export const Bio = {
  name: "Sandesh Patankar",
  roles: [
    "Full Stack Developer",
    "UI/UX Designer",
    "Graphic Designer",
  ],
  description:
    "Passionate CDAC certified Fullstack Software Developer with advanced skills in web design and development. Currently working as Software Developer at Macleods Pharmaceuticals Pvt Ltd. Seeking new opportunities in the IT sector to contribute to impactful software development projects.",
  github: "https://gitlab.com/72273_Sandesh/portfolio-showcase",//"https://github.com/pratiks1010",
  resume:"https://bit.ly/409iEJp",
  linkedin: "https://www.linkedin.com/in/sandesh-patankar-b48b76218/",
  email: "sandeshpatankar1509@gmail.com",
  insta: "https://www.instagram.com/monk_mind_?igsh=MXRpOXR5c2E1NXN5Yw==",

};

export const skills = [
  {
    title: "Frontend",
    skills: [
      {
        name: "React Js",
        image:
          "https://s2.svgbox.net/files.svg?ic=reactjs",
      },
      {
        name: "Redux",
        image:
          "https://img.icons8.com/?size=100&id=b6vIINYN0kfW&format=png&color=000000",
      },
      {
        name: "TypeScript",
        image:
          "https://s2.svgbox.net/files.svg?ic=typescript-official",
      },
      {
        name: "HTML",
        image: "https://s2.svgbox.net/files.svg?ic=html",
      },
      {
        name: "CSS",
        image:
          "https://s2.svgbox.net/files.svg?ic=css",
      },
      {
        name: "JavaScript",
        image:
          "https://s2.svgbox.net/files.svg?ic=node",
      },
      {
        name: "Bootstrap",
        image:
          "https://img.icons8.com/?size=100&id=EzPCiQUqWWEa&format=png&color=000000",
      },


    ],
  },
  {
    title: "Backend",
    skills: [
      {
        name: "Node Js",
        image:"https://img.icons8.com/?size=100&id=54087&format=png&color=000000" ,
      },
      {
        name: "Express Js",
        image:
          "https://img.icons8.com/?size=100&id=B4UIxQoMYIGD&format=png&color=000000",
      },
      {
        name: "Spring Boot",
        image:
          "https://img.icons8.com/?size=100&id=90519&format=png&color=000000",
      },
      {
        name: "ASP.NET Core",
        image:
          "https://img.icons8.com/?size=100&id=o0pte3XTuPWi&format=png&color=000000",
      },

    ],
  },
  {
    title: "Database",
    skills: [
      {
        name: "MySQL",
        image:
          "https://s2.svgbox.net/files.svg?ic=mysql",
      },
      {
        name: "MSSQL",
        image:
          "https://img.icons8.com/?size=100&id=laYYF3dV0Iew&format=png&color=000000",
      },
      {
        name: "MongoDB",
        image:
          "https://s2.svgbox.net/files.svg?ic=mongo",
      },

    ],
  },
  {
    title: "Operating Systems",
    skills: [
      {
        name: "Windows",
        image:
          "https://img.icons8.com/?size=100&id=108792&format=png&color=000000",
      },

      {
        name: "Linux",
        image:
          "https://img.icons8.com/?size=100&id=17842&format=png&color=000000",
      },

    ],
  },
  {
    title: "UI/UX Design",
    skills: [
      {
        name: "Adobe Xd",
        image:
          "https://img.icons8.com/?size=100&id=TF2CQcYTNqU2&format=png&color=000000",
      },
      {
        name: "Figma",
        image:
          "https://img.icons8.com/?size=100&id=zfHRZ6i1Wg0U&format=png&color=000000",
      },
      {
        name: "Canva",
        image:
          "https://img.icons8.com/?size=100&id=iWw83PVcBpLw&format=png&color=000000",
      },
      {
        name: "Photoshop",
        image:
          "https://s2.svgbox.net/files.svg?ic=photoshop",
      },
      // {
      //   name: "Adobe Illustrator",
      //   image:
      //     "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/illustrator.png?alt=media&token=8464281d-7555-4da3-ad1b-7ee6d9a4df0d",
      // },
      // {
      //   name: "Wordpress",
      //   image:
      //     "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/wordpress.png?alt=media&token=6908c51d-71b6-4edb-8145-8318e03ce00e",
      // },
    ],
  },
  {
    title: "Tools",
    skills: [
      {
        name: "Git",
        image:
          "https://s2.svgbox.net/files.svg?ic=git",
      },
      {
        name: "GitLab",
        image:
          "https://s2.svgbox.net/files.svg?ic=gitlab",
      },
      {
        name: "Docker",
        image:
          "https://s2.svgbox.net/files.svg?ic=docker",
      },

      {
        name: "VS Code",
        image:
          "https://s2.svgbox.net/files.svg?ic=vscode",
      },
      {
        name: "Visual Studio",
        image:
          "https://s2.svgbox.net/files.svg?ic=vscode3",
      },
      {
        name: "Postman",
        image:
          "https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg",
      },
      {
        name: "Swagger",
        image:
          "https://s2.svgbox.net/files.svg?ic=swagger",
      },
      {
        name: "NPM",
        image:
          "https://img.icons8.com/?size=100&id=24895&format=png&color=000000",
      },


    ],
  },
];

export const experiences = [
  {
    id: 0,
    img: "https://www.macleodspharma.com/wp-content/uploads/2020/06/logo.jpeg",
    role: "Software Developer",
    company: "Macleods Pharmaceuticals Pvt. Ltd.",
    date: "Current",
    desc: `Working on "HRMS Development Project" where I am contributing with my skills as below`,
    skills: [
      "React-Js",
      "HTML",
      "CSS",
      "Bootstrap",
      "JavaScript",
      "Typescript",
      "Dot Net Core 8",
      "Photoshop",
      "Figma",
      "Adobe XD",
      "Next Js",
      "Git",
    ],

  },





];

export const education = [
  {
    id: 0,
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRGewqO9TWSByJagZKnvpyf82x_JzOj1ENPAg&s",
    school: "Sunbeam Institute of Information Technology, Pune-Karad",
    date: "March 2023 - Sep 2023",
    grade: "68.38%",
    desc: " completion of the Advanced Computing program and Software Development at #Sunbeam Institute of Information-Technology, Pune-Karad, mastering Java, .NET (C#), SQL, MySQL, React, and MongoDB. Equipped with practical hands-on experience and a thirst for innovation, I'm prepared to tackle new challenges and contribute to cutting-edge projects. Grateful for the transformative journey at #Sunbeam, I'm eagerly anticipating the next chapter of professional growth.",
    degree: "PG Diploma in Advanced Computing",
  },
  {
    id: 1,
    img: "https://dbatu.ac.in/wp-content/uploads/2022/07/cropped-University-logo.jpg",
    school: "Sanjeevan Engineering and Technology Institute, Panhala, Kohlapur",
    date: "June 2017 - Aug 2021",
    grade: "79.99%",
    desc: "completion of my Bachelor of Technology degree in Mechanical Engineering. With a solid foundation in Mechanical Engineering principles",
    degree: "B.Tech (Mechanical Engineering)",
  },
  {
    id: 2,
    img: "https://www.mahahsscboard.in/LogoBord.png",
    school: "Shripatrao Chougule Arts and Science College, Kolhapur, Maharashtra 413004",
    date: "Jun 2014 - Mar 2016",
    grade: "68.00%",
    desc: "I completed my class 12th education at Shripatrao Chougule Arts and Science College where I studied in Science(PCMB + IT).",
    degree: "HSC (Science)",
  },
  {
    id: 3,
    img: "https://www.mahahsscboard.in/LogoBord.png",
    school: "Shri Lal-Bahadur Shastri Vidyalaya Waghave, Panhala, Kolhapur.",
    date: "Jun 2013 - Mar 2014",
    grade: "80.80%",
    desc: "I completed my class 10 education at Shri Lal-Bahadur Shastri Vidyalaya Waghave, Panhala, Kolhapur where I Completed 10th std Education.",
    degree: "SSC (General)",
  },
];

export const projects = [

  {
    id: 11,
    title: "Personal Portfolio Website  ",
    date: "16 May 2024",
    description:
      "👋 Excited to share my latest project - a Personal Portfolio Website built with React and CSS! 💻",
    image:
      "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/5.jpg?alt=media&token=5e39b6ef-da5b-48c8-a4b7-f2302ea6a702",
    tags: [
      "Reactjs",
      "CSS",
      "React-icons",
      "Firebase",

    ],
    category: "Frentend Technologies",
    github: "https://github.com/pratiks1010/Personal-Portfolio-Website",
    webapp: "https://www.linkedin.com/posts/pratik-sawant-web-developer_react-css-webdevelopment-activity-7195775369537363968-x97H?utm_source=share&utm_medium=member_desktop",
  },
  {
    id: 10,
    title: "A Voting Application Back-end API ",
    date: "10 May 2024",
    description:
      "This voting application allows users to cast their votes for a predefined set of candidates, ensuring a smooth and efficient voting process.",
    image:
      "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/4.jpg?alt=media&token=6345c248-6b36-4aef-97c7-e7233408a64e",
    tags: [
      "Nodejs",
      "Expressjs",
      "mongodb Atlas",
      "JWT",
      "Postman",

    ],
    category: "Full Stack Application",
    github: "https://www.linkedin.com/posts/pratik-sawant-726672223_carrental-uiuxdesign-websitelaunch-activity-7162324224957706240-bazw?utm_source=share&utm_medium=member_desktop",
    webapp: "https://www.behance.net/gallery/191247765/Rental-car-website",
  },
  {
    id: 9,
    title: "Ui UX Design of Rental Car Platform ",
    date: "06 february 2024",
    description:
      "latest project: a sleek and user-friendly Ui UX Design of rental car platform designed with your convenience in mind. Browse through our diverse selection of luxury vehicles, enjoy seamless booking, and benefit from 24/7 customer support. Your next adventure starts here Explore our user-friendly website now for hassle-free booking and unparalleled service. Visit my Behance profile for an in-depth look at our innovative UI/UX design.",
    image:
      "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/Rental%20Car%20WebSite%20Page%201.jpg?alt=media&token=486f765b-6b5f-43fc-a2d5-9645d23fdb7a",
    tags: [
      "Adobe XD",
      "Photoshop",
      "Canva",
      "Figma",
      "flaticon",

    ],
    category: "UI UX",
    github: "https://github.com/pratiks1010/Voting-Application-REST-API-",
    webapp: "https://www.linkedin.com/pulse/introducing-my-latest-project-voting-application-pratik-sawant-wcgcf/?trackingId=8thPc5c7QteQnlx6kw9QbA%3D%3D",
  },
  {
    id: 0,
    title: "Food Delivery App",
    date: "16 february 2024",
    description:
      "latest UI/UX project 🎨✨- a sleek and user-friendly design for a food delivery app! 🚀 From seamless navigation to enticing visuals, every detail is crafted for a delightful dining experience. ",
    image:
      "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/CoverImage.jpg?alt=media&token=4948d075-7694-4cd6-a7ac-fc4ee71db9a4",
    tags: ["Adobe XD", "Photoshop", "Canva", "Figma", "flaticon"],
    category: "UI UX",
    github: "https://www.linkedin.com/posts/pratik-sawant-726672223_uiuxdesign-fooddeliveryapp-designexcellence-activity-7157235327600934912-Co9n?utm_source=share&utm_medium=member_desktop",
    webapp: "https://www.behance.net/gallery/190103003/food-delivery-app",
    
  },
  {
    id: 1,
    title: "Sneaker Store Website Home Page",
    date: "24 January 2024",
    description:
      "Excited to share my new sneaker store website! UI/UX Project 🚀 It's easy to use, looks great, and has cool features. You can easily find what you're looking for with the simple layout and search function. I added awesome pictures and videos with interesting info to make it more appealing. ",
    image:
      "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/shoeWebSite%20(page1).jpg?alt=media&token=97f87f83-1590-4a43-8b31-3fe96c670b39",
    tags: [
      "Adobe XD",
      "Photoshop",
      "Canva",
      "Figma",
      "flaticon",
      "Clean png",
    ],
    category: "UI UX",
    github: "https://www.linkedin.com/posts/pratik-sawant-726672223_ui-ux-webdesign-activity-7155602075245043712-dEE_?utm_source=share&utm_medium=member_desktop",
    webapp: "https://www.behance.net/gallery/189733061/sneaker-store-website",
  },
  {
    id: 2,
    title: "Pet Paradise (Pet Shop Web Application)",
    date: "10 Aug 2023 - 15 Sep 2023",
    description:
      "Pet Shop web application is to provide a digital platform designed to facilitate the buying, selling, and management of pets and pet-related products. It serves as an online marketplace where pet enthusiasts, pet owners, breeders, and pet product vendors can interact, showcase their offerings, and make transactions.The objective of the Pet-Shop Web Application is to create a user-friendly and effective platform for customers to easily browse and buy products related to pets. The application strives to ensure a smooth and hassle-free shopping journey, allowing users to discover an extensive variety of pet products and complete their purchases online with ease",
    image:
      "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/homepahe.png?alt=media&token=0df8aac5-b95d-42fa-9fb4-dc2a2f82b783",
    tags: ["ReactJs ", "Javascript", "Axios", "HTML5", "CSS", "Bootstrap 5", "Java", "Spring Boot", "REST Api", "JWT", "Spring Security"],
    category: "Full Stack Application",
    github: "https://github.com/pratiks1010/PetShop_WebApplication.git",

    member: [
      {
        name: "Pratik Sawant",
        img: "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/logo8.png?alt=media&token=a6ba294c-1b44-414b-b769-e3217ff7feda",
        linkedin: "https://www.linkedin.com/in/pratik-sawant-726672223/",
        github: "https://github.com/pratiks1010",
      },
      {
        name: "Uddesh Deshmukh",
        img: "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/uddesh.jpg?alt=media&token=5ea0d760-375b-4afc-a9ff-2779888baf03",
        linkedin: "https://www.linkedin.com/in/uddesh-deshmukh-073b97204",
        github: "http://github.com/UddeshDeshmukh21",
      },
    ],
  },

  {
    id: 10,
    title: "Crypto Currency App",
    date: "15 January 2024",
    description:
      "Crypto Currency App! 🌐💼 I've built a React app that provides real-time information about various cryptocurrencies. 📊💰 👉 Features:Search and explore different cryptocurrencies. Details include rank, name, symbol, market capitalization, and more.Clean and user-friendly interface.",
    image:
      "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/2.jpg?alt=media&token=09e783ff-64b8-48c0-82b7-3aba987a23c8",
    tags: ["ReactJS", "JavaScript", "Axios", "HTML5 ", "CSS", " Bootstrap 5", "Rest API"],
    category: "Frentend Technologies",
    github: "https://lnkd.in/dZBf76B2",
    webapp: "https://www.linkedin.com/posts/pratik-sawant-726672223_react-cryptocurrency-programming-activity-7153743121254604804-AiEM?utm_source=share&utm_medium=member_desktop",
  },

  {
    id: 7,
    title: "Pizza Paradise",
    date: "12 january 2024",
    description:
      "introduce my latest web project, Pizza Paradise! 🍕. Built with React.js and Bootstrap 5, Pizza Paradise is a responsive and delicious way to explore a mouthwatering pizza menu and place your order online. It's been a labor of love, and I'm excited to share it with you all.",
    image:
      "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/pizza%20paradise.png?alt=media&token=d4677a3d-aa73-4022-8f49-f9b70f3dbadf",
    tags: ["Reactjs", "JavaScript", "HTML5 ", "CSS", " Bootstrap 5"],
    category: "Frentend Technologies",
    github: "https://lnkd.in/dycJDdpu",
    webapp: "https://www.linkedin.com/posts/pratik-sawant-726672223_webdevelopment-reactjs-bootstrap5-activity-7117082962185105408-8raz?utm_source=share&utm_medium=member_desktop",
  },
  {
    id: 8,
    title: " Zomato-inspired Restaurant Web Application",
    date: "18 September 2023",
    description:
      " project: a Zomato-inspired restaurant web application built with React.js! 🍽️ 🔍 Project Highlights: Modern UI: Designed with user-friendly features and a clean interface for a seamless dining experience. Search and Filter: Easily discover restaurants by cuisine, location, and price range. Detailed Listings: Get all the information you need - from menu options to reviews. User Reviews: Leave your thoughts and read what others are saying about their dining experiences.Responsive: Accessible on all devices, so you can find your next meal on the go!",
    image:
      "https://firebasestorage.googleapis.com/v0/b/images-18f5e.appspot.com/o/zomato.png?alt=media&token=232bd934-39e3-44d3-8358-787147433d36",
    tags: ["Reactjs", "JavaScript", "HTML5 ", "CSS", " Bootstrap 5"],
    category: "Frentend Technologies",
    github: "https://lnkd.in/d29e_Nad",
    webapp: "https://www.linkedin.com/posts/pratik-sawant-726672223_reactjs-webdevelopment-restaurantapp-activity-7109811572176252928-4zYy?utm_source=share&utm_medium=member_desktop",
  },
];

export const TimeLineData = [
  { year: 2017, text: "Started my journey" },
  { year: 2018, text: "Worked as a freelance developer" },
  { year: 2019, text: "Founded JavaScript Mastery" },
  { year: 2020, text: "Shared my projects with the world" },
  { year: 2021, text: "Started my own platform" },
];
