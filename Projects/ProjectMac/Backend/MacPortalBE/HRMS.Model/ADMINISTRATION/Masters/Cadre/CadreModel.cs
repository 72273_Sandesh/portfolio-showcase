﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.ADMINISTRATION.Masters.Cadre
{
    public class CadreModel
    {
        [Key]
        [Required]  
        public required string CADRE_CODE { get; set; }
        public string DESCR { get; set; }
        public string? SH_DESCR { get; set; }
        public string? LVE_TBLNO { get; set; }
        public DateTime? CHG_DATE { get; set; }
        public string? CHG_USER { get; set; }
        public string? CHG_TERM { get; set; }
        public int? UDF_NUM1 { get; set; }
        public string? CADRE_ID { get; set; }
        public int? CADRE_ORDER { get; set; }
        public string? BAND_CODE { get; set; }
        public decimal? MKIT { get; set; }
        public decimal? MMED { get; set; }
        public string? CHG_REASON { get; set; }

        [Column("STATUS")]
        public string? STATUS { get; set; }

    }
}
