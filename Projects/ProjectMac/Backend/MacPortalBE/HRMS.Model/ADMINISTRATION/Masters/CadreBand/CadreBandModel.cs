﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HRMS.ModelCL.ADMINISTRATION.Masters.CadreBand
{
    public class CadreBandModel
    {
        [Key]

        public string BAND_CODE { get; set; }
        public string? DESCR { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string? SH_DESCR { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string BAND_ID { get; set; }
    }
}
