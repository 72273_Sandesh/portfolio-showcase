﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.ADMINISTRATION.Masters.Designation
{
    public class DesignationModel
    {
        [Key]
        [Required]
        [Column("DESIGN_CODE")]
        public string Id { get; set; }

        [Column("DESIGNATION")]
        public string name { get; set; }

        [Column("CHG_DATE")]
        public DateTime? ChangeDate { get; set; }

        [Column("CHG_USER")]
        public string? ChangeUser { get; set; }


        [Column("CHG_TERM")]
        public string? ChangeTerm { get; set; }

        [Column("LONG_DESIGNATION")]
        public string LongDesignation { get; set; }

        [Column("STATUS")]
        public string? Status { get; set; }

        [Column("UDF_NUM1")]
        public int? UdfNum1 { get; set; }

        [Column("UDF_STR1")]
        public string? UdfStr1 { get; set; }


        [Column("GRADE_CODE")]
        public string? GradeCode { get; set; }

        [Column("SAP_DESIGN_CODE")]
        public string? SapDesignCode { get; set; }

        [Column("CHG_REASON")]
        public string? ChangeReason { get; set; }
    }

    


}
