﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.ADMINISTRATION.Masters.Employee
{
    public class Location
    {
        public string CHG_USER { get; set; }
        public DateTime CHG_DATE { get; set; }
        public string CHG_TERM { get; set; }
    }
}
