﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.ADMINISTRATION.Masters.Qualification
{
    public class Qualification
    {
        [Key]
        [Required]
        [Column("QLF_CODE")]
        public string Id { get; set; }

        [Required]
        [Column("DESCR")]
        public string name { get; set; }

        [Column("SH_DESCR")]
        public string? Shortdescr { get; set; }

        [Column("EQIVAL")]
        public string? Eqival { get; set; }

        [Column("TYPE")]
        public string? Type { get; set; }

        [Column("CHG_DATE")]
        public DateTime? ChangeDate { get; set; }

        [Column("CHG_USER")]
        public string? ChangeUser { get; set; }

        [Column("CHG_TERM")]
        public string? ChangeTerm { get; set; }

        [Column("CHG_REASON")]
        public string? ChangeReason { get; set; }

        [Column("STATUS")]
        public string? Status { get; set; }
    }
}
