﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL
{
    public class ErrorModel
    {
        public string ResponseStatus { get; set; }
        //public string Message { get; set; }
        public List<string> Message { get; set; }
    }

    public class ResponseStatusModel
    {
        public string? ResponseStatus { get; set; }
        //public string Message { get; set; }
        public List<string?> Message { get; set; }
        public string? Code { get; set; }

    }
}
