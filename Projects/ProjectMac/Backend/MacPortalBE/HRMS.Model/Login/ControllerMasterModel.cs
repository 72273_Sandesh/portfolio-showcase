﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Login
{
    public class ControllerMasterModel
    {
        [Key]
        [Column("END_POINTS_ID")]
        public string EndPointsId { get; set; }

        [Column("CTRL_ID")]
        public string CtrlId { get; set; }

        [Column("CTRL_NAME")]
        public string? CtrlName { get; set; }

        [Column("END_POINTS")]
        public string? EndPoints { get; set; }

        [Column("STATUS")]
        public string? Status { get; set; }

        [Column("CHG_REASON")]
        public string? ChangeReason { get; set; }

        [Column("DESCRIPTION")]
        public string? Description { get; set; }

        [Column("TRAN_DATE")]
        public DateTime? TranDate { get; set; }

        [Column("CHG_USER")]
        public string? ChangeUser { get; set; }

        [Column("CHG_TERM")]
        public string? ChangeTerminal { get; set; }

        [Column("CONFIRMED")]
        public string? Confirmed { get; set; }

        [Column("HTTP_TYPE")]
        public string? HttpType { get; set; }

    }
}
