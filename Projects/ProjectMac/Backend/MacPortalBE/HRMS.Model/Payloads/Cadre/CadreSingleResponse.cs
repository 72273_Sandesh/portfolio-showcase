﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Cadre
{
    public class CadreSingleResponse
    {
        public CadreResponse data { get; set; }
    }
}
