﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Department
{
    public class DepartmentGetResponse
    {
        //public IEnumerable<DepartmentGetResponse> data { get; set; }
        public List<DeptResponse> data { get; set; }
        public Payload payload { get; set; }
    }
}
