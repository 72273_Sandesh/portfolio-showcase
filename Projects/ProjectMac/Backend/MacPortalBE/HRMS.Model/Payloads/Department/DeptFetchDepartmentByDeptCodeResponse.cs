﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Department
{
    public class DeptFetchDepartmentByDeptCodeResponse : DeptResponse
    {
        public string DeptHeadName { get; set; }
    }
}
