﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Designation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Designation
{
    public class DesignationGetAllResponse : DesignationModel
    {

        [Column("GRADE_ID")]
        public string? GradeId { get; set; }


        [Column("GRADE_CODE")]
        public string? GradeCode { get; set; }

        [Column("GRADE_NAME")]
        public string? GradeName { get; set; }


        [Column("CADRE_CODE")]
        public string? CadreCode { get; set; }


        [Column("CADRE_NAME")]
        public string? CadreName { get; set; }



    }
}
