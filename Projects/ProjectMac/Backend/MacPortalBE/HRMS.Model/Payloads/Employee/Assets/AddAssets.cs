﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Assets
{
    public class AddAssets
    {
        [Key]
        public string EMP_CODE { get; set; }
        public string INDUCT_CODE { get; set; }
        public DateTime? DATE_FORMALITY { get; set; }
        public string? REMARKS {  get; set; }
        public DateTime CHG_DATE { get; set; }
        public string CHG_USER { get; set; }
        public string CHG_TERM { get; set; }
        public string? TRAN_ID { get; set; }
        public char? STATUS { get; set; }
        public DateTime? STATUS_DATE { get; set; }
        public decimal? TOT_AMT { get; set; }
        public decimal? ADJ_AMT { get; set; }
    }
}
