﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Assets
{
    public class EmployeeAssetsUpdateRequest
    {
        public DateTime? DATE_FORMALITY { get; set; }
        public string REMARKS { get; set; }
        public string CHG_USER { get; set; }
        public DateTime? CHG_DATE { get; set; }
        public string CHG_TERM { get; set; }
        public string STATUS { get; set; }
        public DateTime? STATUS_DATE { get; set; }
        public decimal? TOT_AMT { get; set; }
        public decimal? ADJ_AMT { get; set; }
    }
}
