﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Assets
{
    public class EmployeeGetAssetsResponse
    {
        public string INDUCT_CODE { get; set; }
        public string DESCR {  get; set; }
    }
}
