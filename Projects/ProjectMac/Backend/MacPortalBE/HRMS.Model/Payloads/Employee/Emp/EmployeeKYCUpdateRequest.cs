﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Emp
{
    public class EmployeeKYCUpdateRequest
    {
        public string? AADHAR_EMP_NAME {  get; set; }
        public string? PAN_EMP_NAME { get; set; }
        public DateTime? PASSPORT_ISS_DATE { get; set; }
        public DateTime? PASSPORT_EXP_DATE { get; set; }
    }
}

