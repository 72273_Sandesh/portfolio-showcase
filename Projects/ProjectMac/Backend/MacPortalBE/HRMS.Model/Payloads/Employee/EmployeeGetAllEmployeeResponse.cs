﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeGetAllEmployeeResponse
    {
        [Column("EMP_CODE")]
        public string id { get; set; } //Not Null = EMP_CODE
        public string EMP_FNAME { get; set; } //Not Null = EMP_FNAME
        public string EMP_MNAME { get; set; } //Not Null = EMP_MNAME
        public string EMP_LNAME { get; set; } //Not Null = EMP_LNAME
        public string? DESIGNATION { get; set; }
        public DateTime DOB { get; set; } //Not Null = BIRTH_DATE
        public DateTime DATE_JOIN { get; set; }  //Not Null = DATE_JOIN
        public decimal? BASIC { get; set; }
        public decimal? GROSS { get; set; }
        public string DESIGN_CODE { get; set; }
        public string? EMP_SITE { get; set; }
        public string? DEPT_CODE { get; set; }
        public string? GRADE { get; set; }
        public string? CADRE { get; set; }
        public DateTime? CHG_DATE { get; set; }
        public string? CHG_USER { get; set; }
        public string? CHG_TERM { get; set; }
        public string LOCATION { get; set; }
        public string DEPARTMENT { get; set; }
        public string STATUS { get; set; }

    }
}
