﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeGetCountryResponse
    {
        public string COUNT_CODE { get; set; }
        public string DESCR { get; set; }
    }
}
