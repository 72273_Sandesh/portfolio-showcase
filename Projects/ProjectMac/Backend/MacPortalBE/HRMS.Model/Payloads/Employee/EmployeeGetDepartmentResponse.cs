﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    //Dropdown for Department
    public class EmployeeGetDepartmentResponse
    {
        [Column("DEPT_CODE")]
        public string DeptCode { get; set; }

        [Column("DESCR")]
        public string Description { get; set; }

        [Column("DEPT_HEAD")]
        public string? DepartmentHead { get; set; }

        [Column("STATUS")]
        public string? Status { get; set; }

        [Column("CHG_DATE")]
        public DateTime? ChangeDate { get; set; }

        [Column("CHG_USER")]
        public string ChangeUser { get; set; }

        [Column("CHG_TERM")]
        public string ChangeTerm { get; set; }
    }
}


