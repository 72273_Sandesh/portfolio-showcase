﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeGetDesignationResponse
    {
        public string DESIGN_CODE { get; set; }
        public string DESIGNATION { get; set; }
        public string LONG_DESIGNATION { get; set; }
        public char? STATUS { get; set; }
        public string GRADE_NAME { get; set; }
        public string CADRE_NAME { get; set; }
        public string GRADE_ID { get; set; }
        public string CADRE_CODE { get; set; }
        public string BAND_CODE { get; set; }
        public string GRADE_CODE { get; set; }
        public DateTime CHG_DATE { get; set; }
        public string CHG_USER { get; set; }
        public string CHG_TERM { get; set; }
    }
}
