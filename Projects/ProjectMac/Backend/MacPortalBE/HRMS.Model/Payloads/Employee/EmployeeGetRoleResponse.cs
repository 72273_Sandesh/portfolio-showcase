﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeGetRoleResponse
    {
        public string TRAN_ID { get; set; }
        public string MENU_DESCR { get; set; }
        public string DEPT_CODE { get; set; }
    }
}
