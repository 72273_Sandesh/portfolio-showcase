﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeGetSeperationDetailResponse
    {
        public string EMP_CODE { get; set; }
        public string EMP_NAME { get; set; }
        public DateTime DOJ { get; set; }
        public DateTime RESIGN_DATE { get; set; }
        public DateTime? RELIEVE_DATE { get; set; }
        public DateTime? HR_RECD_DATE { get; set; }
        public string SEP_MODE { get; set; }
        public string STATUS { get; set; }
        public string? RESIGN_REASON { get; set; }
        public decimal NET_AMT { get; set; }
        public string REMARKS { get; set; }
    }
}
