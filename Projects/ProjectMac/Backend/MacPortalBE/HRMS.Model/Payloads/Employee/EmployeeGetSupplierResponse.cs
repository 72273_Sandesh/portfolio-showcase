﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeGetSupplierResponse
    {
        public string SUPP_NAME { get; set; }
        public string SUPP_CODE { get; set; }
        public string SH_NAME { get; set; }
    }
}
