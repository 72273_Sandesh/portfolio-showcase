﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeSP_GetEmployeeAssetsDetailResponse
    {
        public string INDUCT_CODE { get; set; }
        public string INDUCTION { get; set; }
        public DateTime? DATE_FORMALITY { get; set; }
        public string? REMARKS { get; set; }
        public string STATUS { get; set; }
        public DateTime? STATUS_DATE { get; set; }
        public decimal TOT_AMT { get; set; }
        public decimal ADJ_AMT { get; set; }
        public string TRAN_ID { get; set; }
    }
}
