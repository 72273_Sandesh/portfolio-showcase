﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeSP_GetEmployeeEducationalDetailResponse
    {
        public string EMP_CODE { get; set; }
        public string SEQ_NO { get; set; }
        public string QLF_CODE { get; set; }
        public string? INSTITUTE { get; set; }
        public string? PASS_YEAR { get; set; }
        public string? CLASS { get; set; }
        public decimal? PERC { get; set; }
        public string? UNIVERSITY { get; set; }
        public string? DEGREE_NAME { get; set; }
        public string? REMARKS { get; set; }
        public char? STATUS_TYPE { get; set; }
        public DateTime? EXP_COMP_DATE { get; set; }
        public DateTime? FROM_DATE { get; set; }
        public DateTime? TO_DATE { get; set; }
        public string? QUAL_STATUS { get; set; }
        public string? QUALIFICATION { get; set; }
        //-----------------------Extra Added
        public string SUBJECT1 { get; set; }
        public string SUBJECT2 { get; set; }
        public string SUBJECT3 { get; set; }
        public string SUBJECT4 { get; set; }

    }
}
