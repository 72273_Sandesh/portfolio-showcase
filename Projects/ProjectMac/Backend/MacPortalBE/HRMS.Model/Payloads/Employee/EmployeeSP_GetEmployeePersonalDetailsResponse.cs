﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeSP_GetEmployeePersonalDetailsResponse
    {
       
            public string EMP_CODE { get; set; }
            public string EMP_NAME { get; set; }
            public string EMP_FNAME { get; set; }
            public string EMP_MNAME { get; set; }
            public string EMP_LNAME { get; set; }
            public DateTime BIRTH_DATE { get; set; }
            public string SHORT_NAME { get; set; }
            public string DESIGNATION { get; set; }
            public string GRADE { get; set; }
            public string GRADE_NAME { get; set; }
            public string   SITE_NAME { get; set; }
            public string DEPT_CODE { get; set; }
            public string DEPT_NAME { get; set; }
            public DateTime DATE_JOIN { get; set; }
            public DateTime? RESI_DATE { get; set; }
            public DateTime? RELIEVE_DATE { get; set; }
            public string QUAL { get; set; }
            public decimal TOT_EXP { get; set; }
            public decimal MAC_EXP { get; set; }
            public string MOBILE_NO { get; set; }
            public string MOBILE_NO_OFF { get; set; }
            public string EMPLOYMENT_TYPE { get; set; }
            public string GENDER { get; set; } // SEX
            public string GENDER_DESCR { get; set; }
            public string EMP_SITE { get; set; }
            public string EMP_TYPE { get; set; }
            public string? EMP_IMAGE { get; set; }  // Image
            public string CONTACT_CODE { get; set; }
            public string NAME_PREFIX { get; set; }
            public DateTime? M_STATUS_DATE { get; set; }
            public string M_STATUS { get; set; }
            public string M_STATUS_DESCR { get; set; }
            public string CONTACT_CITY { get; set; }
            public string CONTACT_STATE { get; set; }
            public string CONTACT_PIN { get; set; }
            public string RELIGION { get; set; }
            public string IT_NO { get; set; }
            public string EMAIL_ID_OFF { get; set; }
            public string PER_ADD1 { get; set; }
            public string PER_ADD2 { get; set; }
            public string PER_ADD3 { get; set; }
            public string CUR_ADD1 { get; set; }
            public string CUR_ADD2 { get; set; }
            public string CUR_PIN { get; set; }
            public string CUR_ADD3 { get; set; }
            public string CUR_TEL1 { get; set; }
            public string CUR_CITY { get; set; }
            public string CUR_TEL2 { get; set; }
            public string CUR_TEL3 { get; set; }
            public string EMAIL_ID_PER { get; set; }
            public string CONTACT_TEL { get; set; }
            public string CONTACT_PERS { get; set; }
            public string BLOOD_GRP { get; set; }
            public string SECTION { get; set; }
            public string PROBATION_PRD { get; set; }
            public string TRAINING_PRD { get; set; }
            public DateTime? PROBATION_DATE { get; set; }
            public DateTime? DATE_CONF { get; set; }
            public string PROBATION { get; set; }
            public string CONFIRMED { get; set; }
            public string BANK_ACCT { get; set; }
            public string BANK_CODE { get; set; }
            public string BANK_NAME { get; set; }
            public string BANK_IFSC { get; set; }
            public string CADRE { get; set; }
            public string CONTACT_ADD1 { get; set; }
            public string CONTACT_ADD2 { get; set; }
            public string CONTACT_ADD3 { get; set; }
            //public string CONTACT_CITY_1 { get; set; }
            public string PER_PIN { get; set; }
            //public string CONTACT_STATE_1 { get; set; }
            public string PER_TEL { get; set; }
            public string PAY_MODE { get; set; }
            public string PAYMENT_MODE { get; set; }
            public string CUR_STATE { get; set; }
            public string PER_STATE { get; set; }
            public string PER_CITY { get; set; }
            public string CREDIT_CARD_NO { get; set; }
            public string PF_UN_NO { get; set; }
            public string AADHAAR_NO { get; set; }
            public string DRIV_LIC_NO { get; set; }
            public string ELECTION_CARD_NO { get; set; }
            //public string IT_NO_1 { get; set; }
            public string FATHER_NAME { get; set; }
            public string MOTHER_NAME { get; set; }
            public string PASSPORT_NO { get; set; }
            public string PASSPORT_ISS_PLACE { get; set; }
            public DateTime? PASSPORT_ISS_DATE { get; set; }
            public DateTime? PASSPORT_EXP_DATE { get; set; }
            public string CUR_STATE_DESCR { get; set; }
            public string PER_STATE_DESCR { get; set; }
            public string CONTACT_STATE_DESCR { get; set; }
            public int PROB_EXT { get; set; }
            public int CONF_EXT { get; set; }
            public decimal CURR_CTC { get; set; }
            public decimal JOINING_CTC { get; set; }

        //------------
        public string PAN_EMP_NAME { get; set; }
        public string AADHAR_EMP_NAME { get; set; }
        public string JOINED_AS { get; set; }
        public string REPORT_TO { get; set; }
        public int? NOTICE_PRD { get; set; }
        public string WORK_SITE {  get; set; }
        public DateTime GRATUITY_DATE {  get; set; }
        public decimal BASIC {  get; set; }
        public char? PAY_TYPE { get; set; }
        public string? DD_PAYABLE_BANK { get; set; }
        public string? PF_NO { get; set; }
        public string? FPF_NO { get; set; }
        public string? ESIC_NO { get; set; }
        public string PAY_SITE {  get; set; }
        public string GROSS {  get; set; }
        public char? WITH_HELD {  get; set; }
        public string STAN_CODE {  get; set; }

    }
}
