﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeSP_GetEmployeeStatutoryDetailResponse
    {
        public string SEQ_NO { get; set; }
        public string MEMBER_NAME { get; set; }
        public string STATUTORY_TYPE { get; set; }
        public decimal? PERCENTAGE { get; set; }
        public string RELATION { get; set; }
        public string GENDER_DESC { get; set; }
    }
}