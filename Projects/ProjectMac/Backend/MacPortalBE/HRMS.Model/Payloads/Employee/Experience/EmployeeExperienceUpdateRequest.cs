﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Experience
{
    public class EmployeeExperienceUpdateRequest
    {
        public string ORGANISATION { get; set; }
        public string DESIGNATION { get; set; }
        public DateTime? FROM_DATE { get; set; }
        public DateTime? TO_DATE { get; set; }
        public decimal? GROSS_AMT { get; set; }
        public string? CURR_CODE { get; set; }
        public string? CHG_USER { get; set; }
        public DateTime? CHG_DATE { get; set; }
        public string? CHG_TERM { get; set; }
        public string? DEPARTMENT { get; set; }
        //public string? REMARKS { get; set; }
        public string? COUNTRY { get; set; }
        public string? STATE { get; set; }
        //public string? JOIN_DESIGNATION { get; set; }
        //public string? JOIN_GROSS_AMT { get; set; }
        //public string? SEP_REASON { get; set; }
        public string? WORK_LOCATION { get; set; }
    }
}
