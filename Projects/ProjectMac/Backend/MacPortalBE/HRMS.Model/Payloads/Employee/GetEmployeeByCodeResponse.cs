﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.ModelCL.ADMINISTRATION.Masters;
using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class GetEmployeeByCodeResponse 
    {
        [Key]
        [Required]
        public string EMP_CODE { get; set; } //Not Null = EMP_CODE
        public string? CONTACT_CODE { get; set; }
        public string EMP_FNAME { get; set; } //Not Null = EMP_FNAME
        public string EMP_MNAME { get; set; } //Not Null = EMP_MNAME
        public string EMP_LNAME { get; set; } //Not Null = EMP_LNAME
        public string? NAME_PREFIX { get; set; }
        public char? EMP_TYPE { get; set; }
        public string? DESIGNATION { get; set; }
        public string? DEPT_CODE { get; set; }
        public string? GRADE { get; set; }
        public string? CADRE { get; set; }
        public string? REPORT_TO { get; set; }
        public string? CUR_ADD1 { get; set; }
        public string? CUR_ADD2 { get; set; }
        public string? CUR_ADD3 { get; set; }
        public string? CUR_CITY { get; set; }
        public string? CUR_STATE { get; set; }
        public string? CUR_PIN { get; set; }
        public string? CUR_TEL1 { get; set; }
        public string? CUR_TEL2 { get; set; }
        public string? CUR_TEL3 { get; set; }
        public string? PER_ADD1 { get; set; }
        public string? PER_ADD2 { get; set; }
        public string? PER_ADD3 { get; set; }
        public string? PER_CITY { get; set; }
        public string? PER_STATE { get; set; }
        public string? PER_PIN { get; set; }
        public string? PER_TEL { get; set; }
        public string SEX { get; set; }     //Not Null = SEX(char)
        public string? NATIONALITY { get; set; }
        public char? CITIZEN { get; set; }
        public string? STATE_CODE__DOMICILE { get; set; }
        public char? M_STATUS { get; set; }
        public DateTime? M_STATUS_DATE { get; set; }
        public DateTime BIRTH_DATE { get; set; } //Not Null = BIRTH_DATE
        public string? BLOOD_GRP { get; set; }
        public string? CONTACT_PERS { get; set; }
        public string? RELIGION { get; set; }
        public string? JOINED_AS { get; set; }
        public DateTime DATE_JOIN { get; set; }  //Not Null = DATE_JOIN
        public string? MODE_APP { get; set; }
        public string? WORK_SHIFT { get; set; }
        public char? SHIFT_ROT { get; set; }
        public string? HOL_TBLNO { get; set; }
        public char? PAY_TYPE { get; set; }
        public char? PAY_MODE { get; set; }
        public string? EMP_SITE { get; set; }
        public string? PAY_SITE { get; set; }
        public string? WORK_SITE { get; set; }
        public string? BANK_CODE { get; set; }
        public string? BANK_ACCT { get; set; }
        public string? STAN_CODE { get; set; }
        public string? DD_PAYABLE_BANK { get; set; }
        public int? TOT_EXP { get; set; }
        public string? PF_NO { get; set; }
        public string? FPF_NO { get; set; }
        public string? ESIC_NO { get; set; }
        public string? IT_NO { get; set; }
        public decimal? BASIC { get; set; }
        public decimal? GROSS { get; set; }
        public string? HOBBY1 { get; set; }
        public string? HOBBY2 { get; set; }
        public string? HOBBY3 { get; set; }
        public string? HOBBY4 { get; set; }
        public string? OTHER_INFO { get; set; }
        public string? PROJ_CODE { get; set; }
        public string? DEPUTE_NO { get; set; }
        public int? RATING { get; set; }
        public int? TRAINING_PRD { get; set; }
        public DateTime? PROBATION_DATE { get; set; }
        public int? PROBATION_PRD { get; set; }
        public char? PROBATION { get; set; }
        public DateTime? DATE_CONF { get; set; }
        public char? CONFIRMED { get; set; }
        public int? NOTICE_PRD { get; set; }
        public DateTime? RESI_DATE { get; set; }
        public DateTime? RELIEVE_DATE { get; set; }
        public string? EMP_CODE__LINK { get; set; }
        public string? ACCT_CODE__SAL { get; set; }
        public string? CCTR_CODE__SAL { get; set; }
        public string? ALTER_CODE { get; set; }
        public char? WITH_HELD { get; set; }
        public string? APPR_NO__LAST { get; set; }
        public string? STATUS { get; set; } // changed datatype
        public DateTime? CHG_DATE { get; set; }
        public string? CHG_USER { get; set; }
        public string? CHG_TERM { get; set; }
        public string? SHORT_NAME { get; set; }
        public string? STAN_CODE__HQ { get; set; }
        public string PROC_GROUP { get; set; } //Not Null = PROC_GROUP
        public string? CONTACT_ADD1 { get; set; }
        public string? CONTACT_ADD2 { get; set; }
        public string? CONTACT_ADD3 { get; set; }
        public string? CONTACT_CITY { get; set; }
        public string? CONTACT_STATE { get; set; }
        public string? CONTACT_TEL { get; set; }
        public string? CONTACT_PIN { get; set; }
        public byte[]? IMAGE { get; set; }
        public byte[]? SIGN { get; set; }
        public int? NOTICE_PERIOD { get; set; }
        public string? DESIGN_CODE { get; set; }
        public string? SITE_CODE__IBCA { get; set; }
        public string? SITE_CODE__PAYMENT { get; set; }
        public DateTime? INCR_DATE { get; set; }
        public string? ESI_DISPENSARY { get; set; }
        public string? MOBILE_NO { get; set; }
        public string? SMS_DOMAIN { get; set; }
        public string? EMAIL_ID_OFF { get; set; }
        public string? EMAIL_ID_PER { get; set; }
        public string? FUNC_GROUP { get; set; }
        public char? AUTO_ADV { get; set; }
        public string? REPORT_TO__ADMIN { get; set; }
        public decimal? NORMAL_RATE { get; set; }
        public decimal? OT_RATE { get; set; }
        public string? SH_NAME { get; set; }
        public string? MAIDEN_NAME { get; set; }
        public string? BIRTH_PLACE { get; set; }
        public string? RESI_PERMIT_NO { get; set; }
        public DateTime? RESI_PERMIT_UPTO { get; set; }
        public string? SOCIAL_SECURITY_NO { get; set; }
        public string? PATTERN_CODE { get; set; }
        public string? PERIOD_CODE { get; set; }
        public char? ELIGIBLE_OT { get; set; }
        public char? STANDBY_MODE { get; set; }
        public DateTime? GRATUITY_DATE { get; set; }
        public string? WORK_TBLNO { get; set; }
        public string? SUPP_CODE__CON { get; set; }
        public DateTime? OFFER_DATE { get; set; }
        public string? EMP_CODE__OLD { get; set; }
        public string? IME_NO { get; set; }
        public string? EMP_CLASS { get; set; }
        public string? OCCUPATION_CODE { get; set; }
        public int? SHIFT_INDEX { get; set; }
        public decimal? GROSS_JOIN { get; set; }
        public string? WORK_CTR { get; set; }
        public string? EMP_PWD { get; set; }
        public string? SECTION_CODE { get; set; }
        public string? OFFER_ID { get; set; }
        public char? DOMICILE { get; set; }
        public char? BENEV_FLAG { get; set; }
        public string? MOBILE_NO_OFF { get; set; }
        public string? CREDIT_CARD_NO { get; set; }
        public DateTime? HR_REC_DATE { get; set; }
        public char? PHY_SAMPLE { get; set; }
        public string? PF_UN_NO { get; set; }
        public string? AADHAAR_NO { get; set; }
        public string? BANK_IFSC { get; set; }
        public string? EMP_CODE__HOD { get; set; }
        public string? POS_CODE { get; set; }
        public string? INSIDER_TRADING_OPT { get; set; }
        public int? BOND_PRD { get; set; }
        public decimal? BOND_AMT { get; set; }
        public string? PENSION_SCHEME { get; set; }
        public string? UAN_NO { get; set; }
        public string? PARENT_DEPT { get; set; }
        public string? WORK_LOC_CODE { get; set; }
        public string? USER_ID { get; set; }
        public string? EXT_NO { get; set; }
        public string? AADHAR_CARD_NO { get; set; }
        public string? ELECTION_CARD_NO { get; set; }
        public string? DRIV_LIC_NO { get; set; }
        public DateTime? DRIV_LIC_VALID_UPTO { get; set; }
        public char? PHYSICAL_HANDICAP { get; set; }
        public string? HANDICAP_CATEGORY { get; set; }
        public string? PF_UAN { get; set; }
        public char? GROUP_MEDICLAIM { get; set; }
        public decimal? BOND_AMT__RECO { get; set; }
        public string? FATHER_NAME { get; set; }
        public char? ESIC_MEDICLAIM { get; set; }
        public string? AADHAR_EMP_NAME { get; set; }
        public string? MOTHER_NAME { get; set; }
        public string? PASSPORT_NO { get; set; }
        public string? PASSPORT_ISS_PLACE { get; set; }
        public DateTime? PASSPORT_ISS_DATE { get; set; }
        public DateTime? PASSPORT_EXP_DATE { get; set; }
        public string? SALES_PERS { get; set; }
        public string? ROLE_ID { get; set; }
        public string? AADHAR_MIDDLE_NAME { get; set; }
        public DateTime? AADHAR_BIRTH_DATE { get; set; }
        public string? PAN_EMP_NAME { get; set; }
        public string? PAN_MIDDLE_NAME { get; set; }
        public DateTime? PAN_BIRTH_DATE { get; set; }
        public char? PENSION_APPL { get; set; }
        public string? IN_REPLACE_OF_EMP { get; set; }
        public string? SAP_SALES_HIER { get; set; }
    }
}


