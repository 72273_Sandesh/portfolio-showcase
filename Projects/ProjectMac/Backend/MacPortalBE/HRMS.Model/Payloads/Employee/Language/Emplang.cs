﻿using HRMS.ModelCL.Payloads.Employee.Language;

namespace HRMS.RepositoryCL.Concrete.Masters
{
    public class Emplang : AddLanguage
    {
        public string EmpCode { get; set; }
        public string Language { get; set; }
        public char? READ { get; set; }
        public char? WRITE { get; set; }
        public char? SPEAK { get; set; }
        public char? WORK_PROFICIENCY { get; set; }
    }
}