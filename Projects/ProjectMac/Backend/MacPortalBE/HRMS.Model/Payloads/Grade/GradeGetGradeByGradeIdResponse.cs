﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Grade
{
    public class GradeGetGradeByGradeIdResponse : GradeResponse
    {
        public string? CadreName { get; set; }
    }
}
