﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Qualifcation
{
    public  class GetQualificationByCodeResponse: Qualification
    {
        public string? QualSalStrucDesc { get; set; }
    }
}
