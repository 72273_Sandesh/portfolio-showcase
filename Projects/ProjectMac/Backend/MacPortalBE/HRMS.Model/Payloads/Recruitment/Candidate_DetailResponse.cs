﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Recruitment
{
    public class Candidate_DetailResponse
    {
        public string CANDIDATE_CODE { get; set; }
        public string CANDIDATE_NAME { get; set; }
        public string DEPARTMENT { get; set; }
        public string DESIGNATION { get; set; }
        public string SITE_DESCR { get; set; }
        public string OFFER_ID { get; set; }
        public decimal CURR_CTC { get; set; }
        public decimal OFFERED_CTC { get; set; }
        public DateTime AL_DATE { get; set; }
        public DateTime JOIN_DATE__EXP { get; set; }
        public string SECTION_NAME { get; set; }
        public string SECTION_CODE { get; set; }
        public decimal TOT_EXP { get; set; }
        public DateTime RECEIVED_DATE { get; set; }
        public string CURR_COMPANY { get; set; }
        public string CURR_DEPARTMENT { get; set; }
        public string CURR_DESIGNATION { get; set; }
        public string VAC_NO { get; set; }
        public string SITE_CODE_SUITABLE { get; set; }
        public string DEPT_CODE_SUITABLE { get; set; }
        public string DESIGN_CODE_SUITABLE { get; set; }
        public string GRADE_CODE { get; set; }
        public char CONFIRMED { get; set; }
        public string REMARKS { get; set; }
        public string GRADE_DESCR { get; set; }
        public string QLF_DESCR1 { get; set; }
        public string QLF_TYPE1 { get; set; }
        public string QLF_DESCR2 { get; set; }
        public string QLF_TYPE2 { get; set; }
        public string QLF_DESCR3 { get; set; }
        public string QLF_TYPE3 { get; set; }
        public string CADRE_DESCR { get; set; }
        public string PANEL_TRAN_ID { get; set; }
    }
}
