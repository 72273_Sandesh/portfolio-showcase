﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Recruitment
{
    public class Offer_DetailResponse
    {
        public string CAND_CODE { get; set; }
        public string EMP_CODE { get; set; }
        public DateTime EXP_DOJ { get; set; }
        public string OFFERED_DESIGNATION { get; set; }
        public decimal OFFERED_CTC { get; set; }
        public string CURR_DESIGNATION { get; set; }
        public decimal CURR_CTC { get; set; }
        public int DEV_JOIN_DAY { get; set; }

        //----------------------------
        public string OFFER_ID { get; set; }
        public string VAC_NO { get; set; }
        public DateTime OFFER_DATE { get; set; }

    }
}
