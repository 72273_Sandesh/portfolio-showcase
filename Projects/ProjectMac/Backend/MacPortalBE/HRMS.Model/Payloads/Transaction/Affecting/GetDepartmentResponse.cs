﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting
{
    public class GetDepartmentResponse
    {
        public string DeptCode { get; set; }
        public string Descr { get; set; }
    }
}
