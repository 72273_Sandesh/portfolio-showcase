﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting
{
    public class GetSectionResponse
    {
        public string SectionCode { get; set; }
        public string MenuDescr { get; set; }
        public string DeptCode { get; set; }
        public string SiteCode { get; set; }  // Add this property
    }
}
