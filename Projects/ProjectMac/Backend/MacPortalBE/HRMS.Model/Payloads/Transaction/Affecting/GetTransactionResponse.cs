﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting
{
    public class GetTransactionResponse
    {
        public string FLD_NAME { get; set; }
        public string MOD_NAME { get; set; }
        public string FLD_VALUE { get; set; }
        public string DESCR { get; set; }
        public string? SH_DESCR { get; set; }
        public string? UDF_STR1 { get; set; }
        public string? UDF_STR2 { get; set; }
        public char? ACTIVE { get; set; }
        public string? UDF_STR3 { get; set; }
        public string? UDF_STR4 { get; set; }
    }
}

