﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting
{
    public class Section
    {
        [Key]
        public string SECTION_CODE { get; set; }
        public string? FIN_ENTITY { get; set; }
        public string? SITE_CODE { get; set; }       
        public string? HOD { get; set; }
        public string? EMP_CODE { get; set; }
        public string? DEPT_CODE { get; set; }
        public int? MENU_ID { get; set; }
        public string? MENU_DESCR { get; set; }
        public string? URL_TEXT { get; set; }
        public int? PARENT_ID { get; set; }
        public string? SEC_PARENT { get; set; }
        public string? IS_PARENT { get; set; }
        public string? ACTIVE { get; set; }
        public string? IS_NEGATIVE { get; set; }
        public DateTime? CHG_DATE { get; set; }
        public string? CHG_USER { get; set; }
        public string? CHG_TERM { get; set; }
        public string? CCTR_CODE { get; set; }
    }

}



