﻿using HRMS.ModelCL.Payloads.Department;
using HRMS.RepositoryCL.Concrete.Masters;
using HRMS_Core.Data.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Comman
{
    public class AutoGenerateCodeRepository: IAutoGenerateCodeRepository
    {
        
        private readonly AppDbContext _appDbContext;
        private readonly ILogger<AutoGenerateCodeRepository> _logger;

        public AutoGenerateCodeRepository(AppDbContext appContext, IConfiguration configuration, ILogger<AutoGenerateCodeRepository> logger)
        {
            _appDbContext = appContext;
            _logger = logger;
        }


        public string GenerateTranIDManualAsync(string strPrefixSeq, string strTranIDCol, string strTable)
        {
            try
            {
                string strTranID = string.Empty;

                if (!string.IsNullOrEmpty(strPrefixSeq))
                {
                    string strPrefix = strPrefixSeq.Split(',')[0];
                    int strLen = int.Parse(strPrefixSeq.Split(',')[1].Substring(3));

                    string query = $"SELECT LPAD(MAX(SUBSTR({strTranIDCol}, {strPrefix.Length + 1}, {strLen})) + 1, {strLen}, 0) FROM {strTable} ";

                    if (!string.IsNullOrEmpty(strPrefix))
                    {

                        if (strTranIDCol == "DESIGN_CODE")
                        {
                            query += $"WHERE SUBSTR({strTranIDCol}, 0, {strPrefix.Length}) = '{strPrefix}' AND {strTranIDCol} NOT LIKE '9999%' AND DESIGN_CODE NOT LIKE 'DE999%' ";
                        }
                        else 
                        {
                            query += $"WHERE SUBSTR({strTranIDCol}, 0, {strPrefix.Length}) = '{strPrefix}' AND {strTranIDCol} NOT LIKE '9999%'";
                        }
                    }
                    else
                    {

                        if (strTranIDCol == "DESIGN_CODE")
                        {
                            query += $"WHERE {strTranIDCol} NOT LIKE '9999%' AND DESIGN_CODE NOT LIKE 'DE999%'";
                        }
                        else
                        {
                            query += $"WHERE {strTranIDCol} NOT LIKE '9999%'";
                        }
                    }
                    object result = _appDbContext.ExecuteScalar(query);
                    strTranID = strPrefix + Convert.ToString(result);

                    return strTranID;
                }

                return strTranID;
            }
            catch (Exception ex)
            {
                // Handle the exception appropriately
                _logger.LogError(ex, "An error occurred while generating department code.");
                throw;
            }
        }

        public string GenerateCode(string tableName, string tranWindowCol)
        {
            try
            {
                string keyString = "";
                string tranIdCol = "";
                string deptCode = "";

                string query = $"SELECT KEY_STRING, TRAN_ID_COL FROM TRANSETUP WHERE TRAN_WINDOW='{tranWindowCol}'";

                using (OracleConnection con = _appDbContext.GetConn())
                {
                    con.Open();
                    using (OracleCommand command = _appDbContext.GetCommand())
                    {
                        command.CommandText = query;

                        using (OracleDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                keyString = reader["KEY_STRING"].ToString();
                                tranIdCol = reader["TRAN_ID_COL"].ToString();
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(keyString) && !string.IsNullOrEmpty(tranIdCol))
                {
                    deptCode = GenerateTranIDManualAsync(keyString, tranIdCol, tableName);
                }
                return deptCode;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while generating {tableName} code for {tranWindowCol}.");
                throw;
            }
        }


    }
}
