﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Comman
{
    public class Enums
    {
        public enum TranWindowCol
        {
            SLA__DEPARTMENT,
            SLA__DESIGNATIONLIST,
            SLA__GRADELIST,
            SLA__CADRELIST,
            SLA__QUALIFICATIONLIST
        }

        public enum TableName
        {
            DEPARTMENT,
            DESIGNATION,
            GRADE,
            CADRE,
            QUALIFICATION
        }
    }
}
