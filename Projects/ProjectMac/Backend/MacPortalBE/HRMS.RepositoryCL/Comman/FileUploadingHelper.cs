﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Comman
{
    public class FileUploadingHelper
    {
        private readonly IConfiguration _configuration;
        private readonly IDbConnection _dbConnection;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public FileUploadingHelper(IConfiguration configuration, IDbConnection dbConnection, IHttpContextAccessor httpContextAccessor)
        {
            _configuration = configuration;
            _dbConnection = dbConnection;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<string> SaveDataAndFileUpload(List<string> lstData, List<IFormFile> arrFile, string strFilePath, List<string> strFileName = null, Dictionary<string, Image> dictImages = null)
        {
            string[] strTranID = new string[999999];
            try
            {
                int iRec = 0, iRecTranID = 0, iQryExecCnt = 0;
                string strErrMsg = "";

                if (lstData.Count > 0)
                {
                    using (var conn = new OracleConnection(_configuration.GetConnectionString("DefaultConnection")))
                    {
                        await conn.OpenAsync();
                        using (var transaction = conn.BeginTransaction())
                        {
                            try
                            {
                                foreach (var item in lstData)
                                {
                                    string strQry = "";
                                    string qry = item;

                                    if (qry.Contains("fnGenerateTranID") || qry.Contains("fnUpdateTranId"))
                                    {
                                        string strMethodWithParam = qry.Replace("'", "");
                                        string strMethod = strMethodWithParam.Split(':')[0];
                                        string strParams = strMethodWithParam.Split(':')[1];
                                        string[] arrParams = strParams.Split(',');

                                        if (qry.Contains("fnGenerateTranID"))
                                        {
                                            strTranID[iRecTranID] = fnGenerateTranID(arrParams[0], arrParams[1], arrParams[2], arrParams[3], Convert.ToInt32(arrParams[4]));
                                            iRec = iRec + 1;
                                            iRecTranID++;
                                        }
                                        else if (qry.Contains("fnUpdateTranId"))
                                        {
                                            if (fnUpdateTranId(arrParams[0], arrParams[1], arrParams[2], arrParams[3], arrParams[4], Convert.ToInt32(arrParams[5]), strTranID[iRecTranID - 1]))
                                            {
                                                iRec = iRec + 1;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (qry.Contains("<<TRAN_ID>>") && !string.IsNullOrEmpty(strTranID[iRecTranID - 1]))
                                        {
                                            strQry = qry.Replace("<<TRAN_ID>>", strTranID[iRecTranID - 1]);
                                            if (qry.Contains("<<TRAN_ID_REF_"))
                                            {
                                                Int16 iTranSeq = Convert.ToInt16(qry.Substring(qry.IndexOf("<<TRAN_ID_REF_", 0) + 14, (qry.IndexOf("_>>", 0) - (qry.IndexOf("<<TRAN_ID_REF_", 0) + 14))));
                                                strQry = strQry.Replace("<<TRAN_ID_REF_" + iTranSeq + "_>>", strTranID[iTranSeq]);
                                            }
                                        }
                                        else if (qry.Contains("<<TRAN_ID_REF_"))
                                        {
                                            strQry = qry;
                                            for (int i = 0; i < iRecTranID; i++)
                                            {
                                                if (strQry.Contains("<<TRAN_ID_REF_" + i + "_>>"))
                                                {
                                                    strQry = strQry.Replace("<<TRAN_ID_REF_" + i + "_>>", strTranID[i]);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            strQry = qry;
                                        }

                                        if (strQry.Contains("<<XMLFilePath>>"))
                                        {
                                            strQry = strQry.Replace("<<XMLFilePath>>", _configuration["XmlFilePath"].Remove(0, 3));
                                        }

                                        using (var cmd = new OracleCommand(strQry, conn))
                                        {
                                            cmd.Transaction = transaction;

                                            int iRowExists = 0;
                                            if (qry.StartsWith("DELETE"))
                                            {
                                                cmd.CommandText = strQry.Replace("DELETE FROM", "SELECT COUNT(1) FROM");
                                                iRowExists = Convert.ToInt16(await cmd.ExecuteScalarAsync());
                                                if (iRowExists > 0)
                                                {
                                                    iRowExists = 0;
                                                }
                                                else
                                                {
                                                    iRowExists = 1;
                                                }
                                            }
                                            else if (qry.StartsWith("UPDATE"))
                                            {
                                                string s_WhereCond = strQry.Substring(strQry.IndexOf("WHERE"));
                                                cmd.CommandText = strQry.Replace("UPDATE", "SELECT COUNT(1) FROM ").Remove(strQry.Replace("UPDATE", "SELECT COUNT(1) FROM ").IndexOf("SET")) + s_WhereCond;
                                                iRowExists = Convert.ToInt16(await cmd.ExecuteScalarAsync());
                                                if (iRowExists > 0)
                                                {
                                                    iRowExists = 0;
                                                }
                                                else
                                                {
                                                    iRowExists = 1;
                                                }
                                            }

                                            cmd.CommandText = strQry;
                                            AddImageParameters(cmd, strQry, dictImages);

                                            int iRowsAffected = await cmd.ExecuteNonQueryAsync();
                                            cmd.Parameters.Clear();
                                            if (iRowsAffected > 0)
                                            {
                                                iRec = iRec + 1;
                                            }
                                            else
                                            {
                                                strErrMsg += " Index: " + iQryExecCnt;
                                                iRec = iRec + iRowExists;
                                                LogFile("SAVE (fnSaveDataAndFileUpload)", "Rows not affected", " Index: " + iQryExecCnt + Environment.NewLine + strQry, _configuration["LogFilePath"]);
                                            }
                                            iQryExecCnt = iQryExecCnt + 1;
                                        }
                                    }
                                }
                                if (iRec == lstData.Count)
                                {
                                    if (arrFile != null)
                                    {
                                        if (string.IsNullOrEmpty(strFilePath))
                                        {
                                            strFilePath = _configuration["XmlFilePath"];
                                        }

                                        for (int k = 0; k < arrFile.Count; k++)
                                        {
                                            if (arrFile[k] != null && arrFile[k].Length > 0)
                                            {
                                                if (strFileName[k].Contains("AUTO_FILE_NAME"))
                                                {
                                                    if (!string.IsNullOrEmpty(strTranID[0]))
                                                        strFileName[k] = strFileName[k].Replace("AUTO_FILE_NAME", strTranID[0]);
                                                    else
                                                    {
                                                        transaction.Rollback();
                                                        return "";
                                                    }
                                                }
                                                if (strFileName[k].Contains("<<TRAN_ID_REF_0_>>"))
                                                {
                                                    strFileName[k] = strFileName[k].Replace("<<TRAN_ID_REF_0_>>", strTranID[0]);
                                                }
                                                else if (strFileName[k].Contains("<<TRAN_ID>>"))
                                                {
                                                    strFileName[k] = strFileName[k].Replace("<<TRAN_ID>>", strTranID[0]);
                                                }
                                                var path = Path.Combine(strFilePath, strFileName[k]);
                                                using (var stream = new FileStream(path, FileMode.Create))
                                                {
                                                    await arrFile[k].CopyToAsync(stream);
                                                }
                                            }
                                        }
                                    }
                                    if (string.IsNullOrEmpty(strTranID[0]))
                                    {
                                        strTranID[0] = iRec.ToString();
                                    }
                                    transaction.Commit();
                                    return strTranID[0];
                                }
                                else
                                {
                                    transaction.Rollback();
                                    LogFile("SAVE (fnSaveDataAndFileUpload)", "Error", "Transaction Failure at " + strErrMsg, _configuration["LogFilePath"]);
                                    return "Error: Transaction Failure at " + strErrMsg;
                                }
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                LogFile("SAVE (fnSaveDataAndFileUpload)", "Error", ex.Message, _configuration["LogFilePath"]);
                                return "Error: " + ex.Message;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
            return strTranID[0];
        }

        public string fnGenerateTranID(string strSiteCode, string strDate, string strRefSer, string strRefPrefix, int strSeqLength = 5)
        {
            try
            {
                int strSeqLen = Convert.ToInt32(strSeqLength);
                string strTranId = "";
                string Ls_Site_Doc_Prefix = "";
                string Ls_Prd_Doc_Prefix = "";
                string Ls_Ref_Prefix = "";
                string Ls_New_Seq_No = "";
                string Qry = "";

                using (var conn = new OracleConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();

                    if (!string.IsNullOrEmpty(strSiteCode))
                    {
                        Qry = "SELECT Doc_prefix FROM Site WHERE site_code = :siteCode";
                        using (var cmd = new OracleCommand(Qry, conn))
                        {
                            cmd.Parameters.Add("siteCode", OracleDbType.Varchar2).Value = strSiteCode;
                            Ls_Site_Doc_Prefix = Convert.ToString(cmd.ExecuteScalar());
                        }
                    }

                    if (!string.IsNullOrEmpty(strDate))
                    {
                        Qry = "SELECT Trim(Doc_prefix) FROM Period WHERE fr_date <= SYSDATE AND to_date >= SYSDATE";
                        using (var cmd = new OracleCommand(Qry, conn))
                        {
                            Ls_Prd_Doc_Prefix = Convert.ToString(cmd.ExecuteScalar());
                        }
                    }

                    Ls_Ref_Prefix = Ls_Site_Doc_Prefix + strRefPrefix + Ls_Prd_Doc_Prefix;

                    if (!string.IsNullOrEmpty(strSiteCode) && !string.IsNullOrEmpty(strDate) && (Ls_Ref_Prefix.Length + strSeqLen > 10))
                    {
                        return "Invalid Site Period And Sequence Combination!!!";
                    }

                    Qry = "SELECT Count(1) FROM RefSeq WHERE Ref_Ser = :refSer";
                    if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                    {
                        Qry += " AND Trim(Ref_Prefix) = :refPrefix";
                    }
                    using (var cmd = new OracleCommand(Qry, conn))
                    {
                        cmd.Parameters.Add("refSer", OracleDbType.Varchar2).Value = strRefSer;
                        if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                        {
                            cmd.Parameters.Add("refPrefix", OracleDbType.Varchar2).Value = Ls_Ref_Prefix;
                        }

                        int iRowsAffected = Convert.ToInt32(cmd.ExecuteScalar() ?? 0);
                        if (iRowsAffected == 0)
                        {
                            Qry = "INSERT INTO Refseq VALUES (:refSer, :refPrefix, LPAD('0', :seqLen, '0'), SYSDATE, 'MAC', :remoteAddr)";
                            using (var cmdInsert = new OracleCommand(Qry, conn))
                            {
                                cmdInsert.Parameters.Add("refSer", OracleDbType.Varchar2).Value = strRefSer;
                                cmdInsert.Parameters.Add("refPrefix", OracleDbType.Varchar2).Value = Ls_Ref_Prefix;
                                cmdInsert.Parameters.Add("seqLen", OracleDbType.Int32).Value = strSeqLen;
                                cmdInsert.Parameters.Add("remoteAddr", OracleDbType.Varchar2).Value = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

                                iRowsAffected = cmdInsert.ExecuteNonQuery();
                                if (iRowsAffected == 0)
                                {
                                    return "Error Inserting TranID!!!";
                                }
                            }
                        }
                        else if (iRowsAffected > 1)
                        {
                            return "Please Mention Ref Prefix!!!";
                        }
                    }

                    Qry = "SELECT Length(Trim(Seq_No)) AS LEN, LPad(Seq_No + 1, :seqLen, '0') AS SEQ FROM RefSeq WHERE Trim(Ref_Ser) = :refSer";
                    if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                    {
                        Qry += " AND Trim(Ref_Prefix) = :refPrefix";
                    }
                    using (var cmd = new OracleCommand(Qry, conn))
                    {
                        cmd.Parameters.Add("refSer", OracleDbType.Varchar2).Value = strRefSer;
                        if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                        {
                            cmd.Parameters.Add("refPrefix", OracleDbType.Varchar2).Value = Ls_Ref_Prefix;
                        }
                        cmd.Parameters.Add("seqLen", OracleDbType.Int32).Value = strSeqLen;

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                if (Convert.ToInt32(reader["LEN"]) != strSeqLength)
                                {
                                    return "Existing Sequence Length Is Not Equal To Provided Length!!!";
                                }
                                else
                                {
                                    Ls_New_Seq_No = Convert.ToString(reader["SEQ"]).Trim();
                                }
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(Ls_New_Seq_No))
                    {
                        return "No Next Sequence Found!!!";
                    }

                    strTranId = Ls_Ref_Prefix + Ls_New_Seq_No;
                    return strTranId;
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
                throw;
            }
        }


        public bool fnUpdateTranId(string strSiteCode, string strEmpCode, string strDate, string strRefSer, string strRefPrefix, int strSeqLen, string strTranID)
        {
            try
            {
                string Qry = "";
                using (var conn = new OracleConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();
                    if (strSiteCode != "" && strDate != "" && strSeqLen == 5)
                    {
                        Qry = "UPDATE REFSEQ SET SEQ_NO = '" + strTranID.Substring(5, 5) + "', CHG_DATE = SYSDATE, CHG_USER = '" + strEmpCode + "', " +
                            "CHG_TERM='" + _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() + "' WHERE REF_SER = '" + strRefSer + "' And Trim(REF_PREFIX) = '" + strTranID.Substring(0, 5) + "' ";
                    }
                    else if (strRefPrefix == "")
                    {
                        Qry = "UPDATE REFSEQ SET SEQ_NO = '" + strTranID + "', CHG_DATE = SYSDATE, CHG_USER = '" + strEmpCode + "', " +
                            "CHG_TERM='" + _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() + "' WHERE REF_SER = '" + strRefSer + "' ";
                    }
                    else if (strRefPrefix != "")
                    {
                        Qry = "UPDATE REFSEQ SET SEQ_NO = '" + strTranID.Substring(strTranID.Length - strSeqLen) + "', CHG_DATE = SYSDATE, CHG_USER = '" + strEmpCode + "', " +
                            "CHG_TERM='" + _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() + "' WHERE REF_SER = '" + strRefSer + "' And Trim(REF_PREFIX) = '" + strTranID.Substring(0, (strTranID.Length - strSeqLen)) + "' ";
                    }

                    if (Qry != "")
                    {
                        using (var cmd = new OracleCommand(Qry, conn))
                        {
                            int iRowsAffected = cmd.ExecuteNonQuery();
                            if (iRowsAffected == 0)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        private void LogFile(string sLogTitle, string sLogType, string sLogContent, string sLogFilePath)
        {
            string sLogFileName = "Log" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
            using (StreamWriter writer = new StreamWriter(Path.Combine(sLogFilePath, sLogFileName), true))
            {
                writer.WriteLine(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + " | " + sLogType + " | " + sLogTitle + " | " + sLogContent);
            }
        }

        private void AddImageParameters(OracleCommand cmd, string query, Dictionary<string, Image> dictImages)
        {
            if (dictImages != null)
            {
                foreach (var imageKey in dictImages.Keys)
                {
                    if (query.Contains(imageKey))
                    {
                        byte[] imageBytes = ConvertImageToByteArray(dictImages[imageKey]);
                        OracleParameter param = new OracleParameter(imageKey, OracleDbType.Blob);
                        param.Value = imageBytes;
                        cmd.Parameters.Add(param);
                    }
                }
            }
        }

        private byte[] ConvertImageToByteArray(Image image)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Jpeg); // You can change the format based on your requirements
                return ms.ToArray();
            }
        }
    }
}
