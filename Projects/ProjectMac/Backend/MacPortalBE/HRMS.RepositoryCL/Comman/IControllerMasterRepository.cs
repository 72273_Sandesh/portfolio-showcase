﻿using HRMS.ModelCL.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Comman
{
    public interface IControllerMasterRepository
    {
        Task<(List<string> ExistingEndpoints, List<string> InsertedEndpoints)> InsertControllerMaster(List<ControllerMasterModel> records);
    }
}
