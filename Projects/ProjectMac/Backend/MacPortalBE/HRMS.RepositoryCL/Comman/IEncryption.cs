﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Comman
{
    public interface IEncryption
    {
        public string Encrypt(string stingToencrypt, bool useHashing);
        public string Decrypt(string cipherString, bool useHashing);
    }
}
