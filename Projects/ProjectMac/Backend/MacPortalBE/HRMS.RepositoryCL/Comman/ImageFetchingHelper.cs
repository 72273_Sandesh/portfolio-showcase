﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Comman
{
    public class ImageFetchingHelper
    {
        public async Task<byte[]> fnGetImage(string s_CODE, string strTABLE_NAME = "EMPLOYEE")
        {
            try
            {
                byte[] empPic = null;
                //long seq = 0;

                //Since BLOB column can not be read from UAT 10G oracle due to ODP.net hence hardcore to get the image
                string connectionString = "DATA SOURCE=172.16.0.126:1730/macleods;USER ID=mactest;PASSWORD=Str1ker$#45deR";

                using (OracleConnection con = new OracleConnection(connectionString))
                {
                    await con.OpenAsync();

                    string Qry = "";

                    if (strTABLE_NAME == "EMPLOYEE" || strTABLE_NAME == "CANDIDATE")
                        Qry = "SELECT IMAGE FROM " + strTABLE_NAME + " WHERE TRIM(EMP_CODE) = :s_CODE";
                    else if (strTABLE_NAME.StartsWith("CANDIDATE_DATA_"))
                        Qry = "SELECT " + strTABLE_NAME.Replace("CANDIDATE_DATA_", "CAND_IMG") + " FROM CANDIDATE_DATA WHERE CANDIDATE_CODE = :s_CODE";
                    else if (strTABLE_NAME == "RECRSL" || strTABLE_NAME == "DIGI_OFFER" || strTABLE_NAME == "EMP_RELIEVED")
                        Qry = "SELECT QR_IMAGE FROM " + strTABLE_NAME + " WHERE OFFER_ID = :s_CODE";
                    else if (strTABLE_NAME.StartsWith("CANDIDATE_DATA_FLD_"))
                        Qry = "SELECT " + strTABLE_NAME.Replace("CANDIDATE_DATA_FLD_", "CAND_IMG") + " FROM CANDIDATE_DATA_FLD WHERE CANDIDATE_CODE = :s_CODE";
                    else if (strTABLE_NAME.StartsWith("CANDIDATE_DATA_SIGN_"))
                        Qry = "SELECT " + strTABLE_NAME.Replace("CANDIDATE_DATA_SIGN_", "CAND_SIGN") + " FROM CANDIDATE_DATA WHERE CANDIDATE_CODE = :s_CODE";

                    using (OracleCommand cmd = new OracleCommand(Qry, con))
                    {
                        cmd.Parameters.Add("s_CODE", OracleDbType.Varchar2).Value = s_CODE.Trim();
                        //cmd.Parameters.Add(new OracleParameter("s_CODE", s_CODE));

                        using (OracleDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                if (reader.Read())
                                {
                                    if (!reader.IsDBNull(0))
                                    {
                                        OracleBlob blob = reader.GetOracleBlob(0);
                                        empPic = new byte[blob.Length];
                                        blob.Read(empPic, 0, empPic.Length);
                                        //seq = reader.GetBytes(0, 0, null, 0, int.MaxValue) - 1;
                                        //empPic = new byte[seq + 1];
                                        //reader.GetBytes(0, 0, empPic, 0, Convert.ToInt32(seq));
                                    }
                                }
                            }
                        }
                    }
                }
                return empPic;
            }
            catch (Exception ex)
            {
                throw RepositoryExceptionHandler.HandleException(ex, "Error while fetching the employee image data-");
            }
        }

    }
}
