﻿using HRMS.ModelCL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace HRMS.RepositoryCL.Comman
{
    public static class ValidationHelper
    {
        public static (bool isValid, object errorResponse) ValidateModel<TModel>(TModel model)
        {
            try
            {
                var validationContext = new ValidationContext(model);
                var validationResults = new List<ValidationResult>();

                // Validate the model using Data Annotations
                bool isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

                // If validation fails, create a detailed error response
                if (!isValid)
                {
                    var errors = validationResults.Select(validationResult => new
                    {
                        Field = validationResult.MemberNames.FirstOrDefault() ?? "Model",
                        Message = validationResult.ErrorMessage
                    }).ToList();

                    return (false, new { Status = "Error", Message = "Validation failed", Errors = errors });
                }

                return (true, null);
            }
            catch (Exception ex)
            {
                // Log the exception for debugging purposes
                Console.WriteLine($"Unexpected error during validation: {ex}");

                return (false, new { Status = "Error", Message = "Unexpected error during validation", ExceptionMessage = ex.Message });
            }
        }

        public static IActionResult GetUpdateResponse(int rowsAffected, string successMessage, string infoMessage, object payload = null)
        {
            if (rowsAffected > 0)
            {
                // Rows were updated successfully
                return new OkObjectResult(new { Status = "S", Message = successMessage, Payload = payload });
            }
            else if (rowsAffected == 0)
            {
                // No rows were updated (zero rows affected)
                return new OkObjectResult(new { Status = "I", Message = infoMessage, Payload = payload });
            }
            else
            {
                // Handle other cases or errors if needed
                return new BadRequestObjectResult(new { Status = "E", Message = "Update operation failed" });
            }
        }

        //public static ErrorStatus ErrorResponse(string arg1, string arg2)
        //{
        //    // Add your processing logic here
        //    //if (string.IsNullOrEmpty(arg1) || arg2 <= 0)
        //    //{
        //    //    return new ErrorStatus
        //    //    {
        //    //        StatusCode = 400, // Bad Request
        //    //        Message = "Invalid arguments. Please provide valid values."
        //    //    };
        //    //}

        //    // Additional processing logic can be added as needed

        //    // If all checks pass, return success
        //    return new ErrorStatus
        //    {
        //        Status= "E", // OK
        //        Message = arg2
        //    };
        //}

      

        public static (string customMessage, string originalMessage) HandleOracleException(OracleException ex)
        {
            string customMessage;
            switch (ex.Number)
            {
                case 1: // ORA-00001 - Duplicate key or unique constraint violation
                    customMessage = "Duplicate key or unique constraint violation";
                    break;

                case 2290: // ORA-02290 - Check constraint violation (e.g., length constraint)
                    customMessage = "Check constraint violation";
                    break;

                case 2291: // ORA-02291 - Foreign key constraint violation
                    customMessage = "Foreign key constraint violation";
                    break;

                case 1400: // ORA-01400 - NOT NULL constraint violation
                    customMessage = "NOT NULL constraint violation";
                    break;

                // Add more cases as needed for other specific Oracle error codes
                default:
                    customMessage = "Other Oracle exception";
                    break;
            }

            return (customMessage, ex.Message);
        }

        public static string GetPropertyNameFromOracleException<TModel>(Exception ex, TModel model)
        {
            // Extract the field name causing the error from the OracleException
            string errorMessage = ex.InnerException.Message;

            // Extracting the property name from the error message (you may need to adjust this logic)
            // Example: "ORA-12899: value too large for column \"MAC\".\"GRADE\".\"DESCR\" (actual: 75, maximum: 40)"
            // This logic assumes that the field name is the part between the last set of double quotes
            int lastQuoteIndex = errorMessage.LastIndexOf('"');
            int secondLastQuoteIndex = errorMessage.LastIndexOf('"', lastQuoteIndex - 1);

            string columnName = errorMessage.Substring(secondLastQuoteIndex + 1, lastQuoteIndex - secondLastQuoteIndex - 1);

            // Use reflection to find the property name based on the Column attribute
            var properties = typeof(TModel).GetProperties()
                .Where(p => Attribute.IsDefined(p, typeof(ColumnAttribute)))
                .ToList();

            var matchingProperty = properties.FirstOrDefault(p =>
            {
                var columnAttribute = (ColumnAttribute)Attribute.GetCustomAttribute(p, typeof(ColumnAttribute));
                return columnAttribute != null && columnAttribute.Name == columnName;
            });

            if (matchingProperty != null)
            {
                return matchingProperty.Name;
            }
            else
            {
                // If the property name is not found in the model, return a generic value
                return "UnknownPropertyName";
            }
        }

        public static (string PropertyName, string ErrorMessage) GetPropertyNameAndErrorFromOracleException<TModel>(Exception ex, TModel model)
        {
            // Check if the exception is an OracleException with an inner exception
            //if (ex is OracleException oracleException && oracleException.InnerException != null)
            try
            {
                // Extract the error message from the inner exception
                string errorMessage = ex.InnerException.Message;

                // Extracting the column name from the error message (you may need to adjust this logic)
                int lastQuoteIndex = errorMessage.LastIndexOf('"');
                int secondLastQuoteIndex = errorMessage.LastIndexOf('"', lastQuoteIndex - 1);

                string columnName = errorMessage.Substring(secondLastQuoteIndex + 1, lastQuoteIndex - secondLastQuoteIndex - 1);

                // Use reflection to find the property name based on the Column attribute
                var matchingProperty = typeof(TModel).GetProperties()
                    .Where(p => Attribute.IsDefined(p, typeof(ColumnAttribute)))
                    .FirstOrDefault(p =>
                    {
                        var columnAttribute = (ColumnAttribute)Attribute.GetCustomAttribute(p, typeof(ColumnAttribute));
                        return columnAttribute != null && columnAttribute.Name == columnName;
                    });

                if (matchingProperty != null)
                {
                    // Replace the column name in the error message with the property name
                    string replacedErrorMessage = errorMessage.Replace(columnName, matchingProperty.Name);
                    return (matchingProperty.Name, replacedErrorMessage);
                }


                // If the property name is not found or the exception structure is different, return generic values
                return ("UnknownPropertyName", ex.Message);
            }
            catch (Exception )
            {
                return ("UnknownPropertyName", ex.Message);
            }
        }

        public static string GetDbErrorMessage(Exception ex)
        {
            return ex.InnerException.Message;

        }



        

    }
}
