﻿using HRMS.ModelCL.Payloads;
using HRMS.RepositoryCL.Interface.Login;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Concrete.Login
{
    public class TokenVerificationRepository:ITokenVerificationRepository
    {
        private readonly IConfiguration _configuration;

        public TokenVerificationRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /* public bool VerifyToken(string token)
         {
             try
             {
                 var tokenHandler = new JwtSecurityTokenHandler();
                 var key = Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]);
                 var validationParameters = new TokenValidationParameters
                 {
                     ValidateIssuer = true,
                     ValidateAudience = true,
                     ValidateLifetime = true,
                     ValidateIssuerSigningKey = true,
                     ValidIssuer = _configuration["Jwt:Issuer"],
                     ValidAudience = _configuration["Jwt:Audiance"],
                     IssuerSigningKey = new SymmetricSecurityKey(key)
                 };

                 SecurityToken validatedToken;
                 var principal = tokenHandler.ValidateToken(token, validationParameters, out validatedToken);

                 // If the token is successfully validated, return true
                 return true;
             }
             catch (Exception ex)
             {
                 // If there is an exception during token validation, return false
                 return false;
             }
         }*/


        public TokenVerifyResponse VerifyToken(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]);
                var validationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = _configuration["Jwt:Issuer"],
                    ValidAudience = _configuration["Jwt:Audiance"],
                    IssuerSigningKey = new SymmetricSecurityKey(key)
                };

                SecurityToken validatedToken;
                var principal = tokenHandler.ValidateToken(token, validationParameters, out validatedToken);

                // Extract user data from claims

                var userId = principal?.Claims.FirstOrDefault(c => c.Type == "userId")?.Value;
                var email = principal?.Claims.FirstOrDefault(c => c.Type == "emailId")?.Value;
                var firstName = principal?.Claims.FirstOrDefault(c => c.Type == "_empFirstName")?.Value;
                var lastName = principal?.Claims.FirstOrDefault(c => c.Type == "_empLastName")?.Value;


                var response = new TokenVerifyResponse
                {
                    api_token = token,
                    created_at = DateTime.Now,
                    email = email,
                    email_verified_at = DateTime.Now,
                    first_name = firstName,
                    id = userId,
                    last_name = lastName,
                    updated_at = DateTime.Now
                };

                return response;
            }
            catch (Exception ex)
            {
                // Log or handle the exception appropriately
                return null; // Return null if token validation fails
            }
        }

    }
}
