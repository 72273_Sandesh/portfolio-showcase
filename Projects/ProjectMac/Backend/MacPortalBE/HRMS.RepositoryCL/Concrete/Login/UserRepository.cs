﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using HRMS.ModelCL.Login;
using HRMS.RepositoryCL.Interface.Login;
using Oracle.ManagedDataAccess.Client;
using HRMS.RepositoryCL.Comman;
using HRMS_Core.Data.Data;
using RMS.ModelCL.Login;
using Microsoft.EntityFrameworkCore;
using HRMS.ModelCL.Payloads;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Microsoft.AspNetCore.Http;
using System.Data.OracleClient;
using Microsoft.AspNetCore.Hosting.Server;
using System.Data;


namespace HRMS.RepositoryCL.Concrete.Login
{
    public class UserRepository : IUserRepository
    {
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        private readonly IEncryption _encryption;

        public UserRepository(IConfiguration configuration, AppDbContext context, IEncryption encryption)
        {
            _appDbContext = context;
            _configuration = configuration;
            _encryption = encryption;
        }

        public async Task<Role> AddRole(Role role)
        {
            var addedRole = _appDbContext.Roles.Add(role);
            await _appDbContext.SaveChangesAsync();
            return addedRole.Entity;
        }

        public async Task<users> AddUser(users user)
        {
            var addedUser = _appDbContext.users.Add(user);
            await _appDbContext.SaveChangesAsync();
            return addedUser.Entity;
        }

        public async Task<bool> AssignRoleToUser(AddUserRole obj)
        {
            try
            {
                var addRoles = new List<UserRole>();
                var user = await _appDbContext.UserRoles.SingleOrDefaultAsync(s => s.UserId == obj.UserId);

                if (user == null)
                {
                    throw new Exception("User is not valid");
                }

                foreach (int role in obj.RoleIds)
                {
                    var userRole = new UserRole
                    {
                        RoleId = role,
                        UserId = user.Id
                    };
                    addRoles.Add(userRole);
                }

                await _appDbContext.UserRoles.AddRangeAsync(addRoles);
                await _appDbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<IActionResult> Authenticate(LoginRequest users)
        {
            (string retVal, string empFirstName, string empLastName, string empCode, string empEmail, string profileId) = await GetUsersAsync(users);

            if (retVal == "true")
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenKey = Encoding.UTF8.GetBytes(_configuration["JWT:Key"]);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]{
                    new Claim(ClaimTypes.Name, "MacleodsWebAPI"),
                    new Claim("userId",empCode),
                    new Claim("profileId",profileId),
                    new Claim("emailId",empEmail),
                    new Claim("_empFirstName",empFirstName),
                    new Claim("_empLastName",empLastName)}
                    ),
                    Expires = DateTime.UtcNow.AddMinutes(30),
                    Issuer = _configuration["JWT:Issuer"], // Replace with your issuer
                    Audience = _configuration["JWT:Audiance"], // Replace with your audience
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);

                TokenVerifyResponse response = new TokenVerifyResponse
                {
                    api_token = tokenHandler.WriteToken(token),
                    first_name = empFirstName,
                    last_name = empLastName,
                    email = empEmail,
                    id = empCode,
                    email_verified_at = DateTime.Now,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now
                };
                return new ObjectResult(response)
                {
                    StatusCode = (int)HttpStatusCode.OK
                };
            }
            else
            {
                return new ObjectResult(new { Message = "Invalid credentials", Status = "Unauthorized" })
                {
                    StatusCode = (int)HttpStatusCode.Unauthorized
                };
            }
        }


        /* public async Task<IActionResult> Authenticate(LoginRequest users)
         {
             (string retVal, string empFirstName, string empLastName, string empCode, string empEmail, string profileId) = await GetUsersAsync(users);

             if (retVal == "true")
             {
                 var tokenHandler = new JwtSecurityTokenHandler();
                 var tokenKey = Encoding.UTF8.GetBytes(_configuration["JWT:Key"]);

                 var tokenDescriptor = new SecurityTokenDescriptor
                 {
                     Subject = new ClaimsIdentity(new Claim[]{
                 new Claim(ClaimTypes.Name, "MacleodsWebAPI"),
                 new Claim("userId", empCode),
                 new Claim("profileId", profileId),
                 new Claim("emailId", empEmail),
                 new Claim("_empFirstName", empFirstName),
                 new Claim("_empLastName", empLastName)
             }),
                     Expires = DateTime.UtcNow.AddMinutes(5),
                     Issuer = _configuration["JWT:Issuer"], // Replace with your issuer
                     Audience = _configuration["JWT:Audience"], // Replace with your audience
                     SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature)
                 };

                 var token = tokenHandler.CreateToken(tokenDescriptor);
                 var tokenString = tokenHandler.WriteToken(token);
                 var expirationTime = tokenDescriptor.Expires;

                 TokenVerifyResponse response = new TokenVerifyResponse
                 {
                     api_token = tokenString,
                     first_name = empFirstName,
                     last_name = empLastName,
                     email = empEmail,
                     id = empCode,
                     email_verified_at = DateTime.Now,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     expires_at = expirationTime // Add this field to the response
                 };

                 return new ObjectResult(response)
                 {
                     StatusCode = (int)HttpStatusCode.OK
                 };
             }
             else
             {
                 return new ObjectResult(new { Message = "Invalid credentials", Status = "Unauthorized" })
                 {
                     StatusCode = (int)HttpStatusCode.Unauthorized
                 };
             }
         }*/


        public async Task<(string, string, string, string, string, string)> GetUsersAsync(LoginRequest users)
        {
            string retVal = "false";
            string? empFirstName = string.Empty;
            string? empLastName = string.Empty;
            string? empCode = string.Empty;
            string? empEmail = string.Empty;
            string? profileId = string.Empty;

            using (OracleConnection con = _appDbContext.GetConn())
            {
                using (OracleCommand cmd = _appDbContext.GetCommand())
                {
                    try
                    {
                        con.Open();
                        cmd.BindByName = true;
                        string encryptPass = _encryption.Encrypt(users.password, true);
                        string Qry = "SELECT A.*,B.DEPT_CODE,B.EMAIL_ID_OFF,B.RELIEVE_DATE,C.DESCR AS DEPT,NVL(LOGIN_ATTEMPTS,0) LOGIN_ATTEMPTS, B.EMP_FNAME AS EMP_FNAME,B.EMP_LNAME AS EMP_LNAME," +
                                     "TRUNC(TO_DATE(SYSDATE)-NVL(TO_DATE(LAST_LOGIN_DATE),TO_DATE(SYSDATE))) AS LOG_DAYS,90-TRUNC(TO_DATE(SYSDATE)-NVL(TO_DATE(LAST_PWD_CHG_DATE),TO_DATE(SYSDATE))) AS PWD_DAYS " +
                                     "FROM SCM_RIGHTS A " +
                                     "LEFT OUTER JOIN EMPLOYEE B ON A.EMP_CODE=B.EMP_CODE LEFT OUTER JOIN DEPARTMENT C ON B.DEPT_CODE=C.DEPT_CODE WHERE A.EMP_CODE='" + users.empid + "'  ";
                        cmd.CommandText = Qry;
                        OracleDataReader reader = cmd.ExecuteReader();
                        while (await reader.ReadAsync())
                        {
                            if (Convert.ToString(reader["USER_LOCK"]) == "1")
                            {
                                //dtSet.Tables[0].Rows[0]["REMARKS"] = "This Login is Locked.";
                                //return DataSetData.FromDataSet(dtSet);
                                return ("This Login is Locked.", empFirstName, empLastName, empCode, empEmail, profileId);
                            }
                            if (Convert.ToString(reader["RELIEVE_DATE"]) != "")
                            {
                                //dtSet.Tables[0].Rows[0]["REMARKS"] = "This Login is Expired.";
                                //return DataSetData.FromDataSet(dtSet);
                                return ("This Login is Expired.", empFirstName, empLastName, empCode, empEmail, profileId);
                            }
                            if (Convert.ToInt32(reader["LOG_DAYS"]) > 90)
                            {
                                //dtSet.Tables[0].Rows[0]["REMARKS"] = "You Have Not Loggin To The Portal From Last " + Convert.ToString(reader["LOG_DAYS"]) + " Days.\nPlease Contact IT Department.";
                                //return DataSetData.FromDataSet(dtSet);
                                return ("You Have Not Loggin To The Portal From Last " + Convert.ToString(reader["LOG_DAYS"]) + " Days.\nPlease Contact IT Department.", empFirstName, empLastName, empCode, empEmail, profileId);
                            }
                            if (_encryption.Decrypt(Convert.ToString(reader["PWD_ENCRYPT"]), true) == users.password)
                            {
                                retVal = "true";
                                empFirstName = Convert.ToString(reader["EMP_FNAME"]);
                                empLastName = Convert.ToString(reader["EMP_LNAME"]);
                                empCode = Convert.ToString(reader["EMP_CODE"]);
                                empEmail = Convert.ToString(reader["EMAIL_ID_OFF"]);
                                profileId = Convert.ToString(reader["PROFILE_ID"]);

                            }
                            else
                            {
                                retVal = "Invalid password!!";
                            }
                        }
                        reader.Dispose();
                        return (retVal, empFirstName, empLastName, empCode, empEmail, profileId);
                    }
                    catch (Exception ex)
                    {
                        retVal = "false";
                        throw (ex);
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

    }
}


