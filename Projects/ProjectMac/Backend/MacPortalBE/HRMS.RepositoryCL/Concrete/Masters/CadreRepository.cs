﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Cadre;
using HRMS.ModelCL.ADMINISTRATION.Masters.CadreBand;
using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Cadre;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.Data.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using static System.Runtime.InteropServices.JavaScript.JSType;



namespace HRMS.RepositoryCL.Concrete.Masters
{
    public class CadreRepository : ICadreRepository
    {
        private readonly IAutoGenerateCodeRepository _autoGenerateCodeRepository;
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        public CadreRepository(AppDbContext appContext, IConfiguration configuration, IAutoGenerateCodeRepository autoGenerateCodeRepository)
        {
            _appDbContext = appContext;
            _configuration = configuration;
            _autoGenerateCodeRepository = autoGenerateCodeRepository;
        }



        public async Task<(List<CadreGetAllResponse>, int)> GetAllCadres(int page, int items_per_page, string search)
        {
            // Calculate the starting row number based on the page number and page size
            int startRow = (page - 1) * items_per_page;
            int endRow = page * items_per_page;

            try
            {
                var result = from cadre in _appDbContext.CADRE
                             join cadreBand in _appDbContext.CADRE_BAND
                             on cadre.BAND_CODE equals cadreBand.BAND_CODE into joinedCadreBand
                             from band in joinedCadreBand.DefaultIfEmpty()
                             where cadre.CADRE_ID != null
                             select new CadreGetAllResponse
                             {
                                 id = cadre.CADRE_CODE,
                                 CADRE_CODE = cadre.CADRE_CODE,
                                 DESCR = cadre.DESCR,
                                 SH_DESCR = cadre.SH_DESCR,
                                 CHG_DATE = cadre.CHG_DATE,
                                 CHG_USER = cadre.CHG_USER,
                                 CHG_TERM = cadre.CHG_TERM,
                                 UDF_NUM1 = cadre.UDF_NUM1,
                                 CADRE_ID = cadre.CADRE_ID,
                                 CADRE_ORDER = cadre.CADRE_ORDER,
                                 BAND_CODE = cadre.BAND_CODE,
                                 MKIT = cadre.MKIT,
                                 MMED = cadre.MMED,
                                 CHG_REASON = cadre.CHG_REASON,
                                 STATUS = cadre.STATUS,
                                 CadreCategory = band != null ? band.DESCR : null
                             };

                // Apply search filter if search string is provided (case-insensitive for DESCR)
                if (!string.IsNullOrEmpty(search))
                {
                    string searchUpper = search.ToUpper();
                    result = result.Where(c =>
                        c.DESCR.ToUpper().Contains(searchUpper) ||
                        c.CADRE_CODE.ToUpper().Contains(searchUpper) ||
                        c.SH_DESCR.ToUpper().Contains(searchUpper) ||
                        c.CadreCategory.ToUpper().Contains(searchUpper) ||
                        c.CHG_USER.ToUpper().Contains(searchUpper) ||
                        c.CHG_DATE.ToString().Contains(searchUpper) ||
                        (c.STATUS.ToUpper() == "A" && searchUpper == "ACTIVE") || // Map "ACTIVE" to "A"
                        (c.STATUS.ToUpper() == "N" && searchUpper == "INACTIVE")   // Map "INACTIVE" to "N"
                    );
                }

                // Materialize the query result to a list
                var resultList = await result.ToListAsync();

                // Filter the paged cadres based on pagination
                var pagedCadres = resultList.Skip(startRow).Take(items_per_page).ToList();

                // Get the total count of cadres
                int totalCadreCount = resultList.Count;

                return (pagedCadres, totalCadreCount);
            }
            catch (Exception ex)
            {
                // Handle exceptions
                string errorMessage = $"Error getting Cadres: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }
                throw new Exception(errorMessage, ex.InnerException);
            }
        }





        public int CountAllCadres()
        {
            try
            {
                int count = _appDbContext.CADRE.Count();

                return count;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting Cadre: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }

        }


        public async Task<List<CadreGetAllResponse>> FetchCadreByCadreCode(string cadreCode)
        {
            try
            {

                var result = from cadre in _appDbContext.CADRE
                             join cadreBand in _appDbContext.CADRE_BAND
                             on cadre.BAND_CODE equals cadreBand.BAND_CODE into joinedCadreBand
                             from band in joinedCadreBand.DefaultIfEmpty()
                             where cadre.CADRE_CODE.Equals(cadreCode)
                             select new CadreGetAllResponse
                             {
                                 id = cadre.CADRE_CODE,
                                 CADRE_CODE = cadre.CADRE_CODE,
                                 DESCR = cadre.DESCR,
                                 SH_DESCR = cadre.SH_DESCR,
                                 CHG_DATE = cadre.CHG_DATE,
                                 CHG_USER = cadre.CHG_USER,
                                 CHG_TERM = cadre.CHG_TERM,
                                 UDF_NUM1 = cadre.UDF_NUM1,
                                 CADRE_ID = cadre.CADRE_ID,
                                 CADRE_ORDER = cadre.CADRE_ORDER,
                                 BAND_CODE = cadre.BAND_CODE,
                                 MKIT = cadre.MKIT,
                                 MMED = cadre.MMED,
                                 CHG_REASON = cadre.CHG_REASON,
                                 STATUS = cadre.STATUS,
                                 //BandCode = band != null ? band.BAND_CODE : null,
                                 CadreCategory = band != null ? band.DESCR : null
                             };
                var resultList = result.ToList();

                return resultList;
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting Cadre: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }

        public async Task<bool> CreateCadre(CadreModel cadre)
        {
            try
            {
                bool codeExists = _appDbContext.CADRE.Any(g => (g.DESCR).Trim().ToUpper() == (cadre.DESCR).Trim().ToUpper());
                if (codeExists)
                {
                    return false;
                }
                else
                {
                    Enums.TableName tableNameValue = Enums.TableName.CADRE;
                    Enums.TranWindowCol tranWindowColValue = Enums.TranWindowCol.SLA__CADRELIST;

                    string tableNameString = tableNameValue.ToString();
                    string tranWindowColString = tranWindowColValue.ToString();

                    string cadreCode = _autoGenerateCodeRepository.GenerateCode(tableNameString, tranWindowColString);
                    cadre.CADRE_CODE = cadreCode;

                    if (cadre.STATUS == "ACTIVE")
                    {
                        cadre.STATUS = "A";
                    }
                    if (cadre.STATUS == "INACTIVE")
                    {
                        cadre.STATUS = "N";
                    }
                    // Add the new cadre to the context
                    _appDbContext.CADRE.Add(cadre);

                    await _appDbContext.SaveChangesAsync();
                    return true;
                }
                
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting Cadre: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }

        public UpdateFlagResponse UpdateCadre(CadreModel cadre)
        {
            try
            {
                UpdateFlagResponse enum_flags = new UpdateFlagResponse();
                bool codeExists = _appDbContext.CADRE.Any(g => (g.CADRE_CODE).Trim().ToUpper() == (cadre.CADRE_CODE).Trim().ToUpper());
                if (codeExists)
                {
                    enum_flags.codeExists = true;
                    bool descrExists = _appDbContext.CADRE.Any(g => g.DESCR.Trim().ToUpper() == cadre.DESCR.Trim().ToUpper() && g.CADRE_CODE.Trim().ToUpper() != cadre.CADRE_CODE.Trim().ToUpper());
                    if (descrExists)
                    {
                        enum_flags.descrExists = true;
                    }
                    else
                    {
                        if (cadre.STATUS == "ACTIVE")
                        {
                            cadre.STATUS = "A";
                        }
                        if (cadre.STATUS == "INACTIVE")
                        {
                            cadre.STATUS = "N";
                        }
                        _appDbContext.Entry(cadre).State = EntityState.Modified;
                        _appDbContext.SaveChanges();
                        enum_flags.Success = true;
                    }
                }
                else 
                {
                    enum_flags.codeExists = false;
                }
                return enum_flags;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException) || ex.GetType() == typeof(DbUpdateConcurrencyException))
                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting Cadre: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }


        public async Task<List<CadreBandModel>> GetAllCadre_Category()
        {
            try
            {
                //return await _appDbContext.CADRE_BAND.ToListAsync();
                var bandCodes = new List<string> { "B0001", "B0002", "B0003" };

                var query = await _appDbContext.CADRE_BAND
                    .Where(b => bandCodes.Contains(b.BAND_CODE))
                    .Select(b => new CadreBandModel { BAND_CODE = b.BAND_CODE, DESCR = b.DESCR })
                    .OrderBy(b => b.DESCR)
                    .ToListAsync();
                return query;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting Cadre: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }

        }

    }
}