﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Designation;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Department;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.Data.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;




namespace HRMS.RepositoryCL.Concrete.Masters
{
    public class DepartmentRepository : IDepartmentRepository
    {

        private readonly IAutoGenerateCodeRepository _autoGenerateCodeRepository;
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        private readonly ILogger<DepartmentRepository> _logger;

        DepartmentComboResponse lstResponse = new DepartmentComboResponse();
        public DepartmentRepository(AppDbContext appContext, IConfiguration configuration, ILogger<DepartmentRepository> logger, IAutoGenerateCodeRepository autoGenerateCode)
        {
            _appDbContext = appContext;
            _configuration = configuration;
            _logger = logger;
            _autoGenerateCodeRepository = autoGenerateCode;
        }


        public async Task<(List<Department>, int)> GetAllDepartments(int page, int items_per_page, string search)
        {
            try
            {
                // Calculate the starting row number based on the page number and page size
                int startRow = (page - 1) * items_per_page + 1; // Start row index for Oracle starts from 1
                int endRow = page * items_per_page;

                List<Department> departments = new List<Department>();
                int totalCount = 0;

                string countQuery = @"
            SELECT 
                COUNT(*) 
            FROM 
                DEPARTMENT A 
                LEFT OUTER JOIN EMPLOYEE B ON A.DEPT_HEAD = B.EMP_CODE 
            WHERE 
                A.DEPT_CODE LIKE 'D%'
                AND (:search IS NULL 
                    OR UPPER(A.DESCR) LIKE '%' || :searchUpper || '%'
                    OR UPPER(A.DEPT_CODE) LIKE '%' || :searchUpper || '%'
                    OR UPPER(A.CHG_USER) LIKE '%' || :searchUpper || '%'                                                           
                    OR UPPER(B.SHORT_NAME) LIKE '%' || :searchUpper || '%'  
                    OR UPPER(A.CHG_DATE) LIKE '%' || :searchUpper || '%'
                    OR UPPER(A.STATUS) LIKE '%' || :searchUpper || '%' 
                    OR (:searchUpper = 'ACTIVE' AND UPPER(A.STATUS) = 'A')
                    OR (:searchUpper = 'INACTIVE' AND UPPER(A.STATUS) = 'N')
                )";

                using (OracleConnection connection = _appDbContext.GetConn())
                using (OracleCommand command = _appDbContext.GetCommand())
                {
                    try
                    {
                        await connection.OpenAsync();
                        command.BindByName = true;
                        command.CommandText = countQuery;
                        command.Parameters.Add(new OracleParameter("search", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search));
                        command.Parameters.Add(new OracleParameter("searchUpper", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search.ToUpper()));

                        // Execute the count query to get the total number of departments
                        totalCount = Convert.ToInt32(await command.ExecuteScalarAsync());

                        string query = @"
                    SELECT *
                    FROM (
                        SELECT 
                            A.DEPT_CODE, 
                            A.DESCR, 
                            B.SHORT_NAME AS DEPT_HEAD, 
                            A.STATUS, 
                            A.CHG_DATE, 
                            A.CHG_USER, 
                            A.CHG_TERM, 
                            A.CHG_REASON, 
                            A.ACCT_CODE, 
                            A.PAY_TABLE, 
                            A.CCTR_CODE, 
                            A.SH_DESCR, 
                            A.ACCT_CODE__AP, 
                            A.CCTR_CODE__AP, 
                            A.STATUS AS INNER_STATUS, 
                            A.UDF_NUM1, 
                            A.SH_DEPT_CODE, 
                            ROW_NUMBER() OVER (ORDER BY A.DEPT_CODE) AS rn
                        FROM 
                            DEPARTMENT A 
                            LEFT OUTER JOIN EMPLOYEE B ON A.DEPT_HEAD = B.EMP_CODE 
                        WHERE 
                            A.DEPT_CODE LIKE 'D%'
                            AND (:search IS NULL 
                                OR UPPER(A.DESCR) LIKE '%' || :searchUpper || '%'
                                OR UPPER(A.DEPT_CODE) LIKE '%' || :searchUpper || '%'
                                OR UPPER(A.CHG_USER) LIKE '%' || :searchUpper || '%'                                                           
                                OR UPPER(B.SHORT_NAME) LIKE '%' || :searchUpper || '%'  
                                OR UPPER(A.CHG_DATE) LIKE '%' || :searchUpper || '%'
                                OR UPPER(A.STATUS) LIKE '%' || :searchUpper || '%' 
                                OR (:searchUpper = 'ACTIVE' AND UPPER(A.STATUS) = 'A')
                                OR (:searchUpper = 'INACTIVE' AND UPPER(A.STATUS) = 'N')
                            )
                    )
                    WHERE rn BETWEEN :startRow AND :endRow";

                        command.CommandText = query;
                        command.Parameters.Clear(); // Clear previous parameters
                        command.Parameters.Add(new OracleParameter("startRow", startRow));
                        command.Parameters.Add(new OracleParameter("endRow", endRow));
                        command.Parameters.Add(new OracleParameter("search", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search));
                        command.Parameters.Add(new OracleParameter("searchUpper", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search.ToUpper()));

                        using (OracleDataReader reader = (OracleDataReader)await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                Department department = new Department
                                {
                                    id = reader["DEPT_CODE"].ToString(),
                                    name = reader["DESCR"].ToString(),
                                    DeptHead = reader["DEPT_HEAD"].ToString(),
                                    Status = reader["STATUS"].ToString(),
                                    ChangeDate = (DateTime)(reader["CHG_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["CHG_DATE"])),
                                    ChangeUser = reader["CHG_USER"].ToString(),
                                    ChangeTerm = reader["CHG_TERM"].ToString(),
                                    ChangeReason = reader["CHG_REASON"].ToString(),
                                    AcctCode = reader["ACCT_CODE"].ToString(),
                                    PayTable = reader["PAY_TABLE"].ToString(),
                                    ShortDescription = reader["SH_DESCR"].ToString(),
                                    AcctCodeAp = reader["ACCT_CODE__AP"].ToString(),
                                    CctrCodeAp = reader["CCTR_CODE__AP"].ToString(),
                                    UdfNumber1 = Convert.ToInt32(reader["UDF_NUM1"]),
                                    ShDeptCode = reader["SH_DEPT_CODE"].ToString(),
                                };

                                departments.Add(department);
                            }
                        }

                        return (departments, totalCount);
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving departments", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                // Log or handle the exception appropriately
                throw new Exception("General error in GetAllDepartments", ex);
            }
        }

        public int CountAllDepartments()
        {
            try
            {
                // Get the count of all departments using LINQ
                int count = _appDbContext.DEPARTMENT.Count();

                return count;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                Console.WriteLine("Error counting departments: " + ex.Message);
                return 0; // Return 0 if an exception occurs
            }
        }


        public async Task<List<DeptFetchDepartmentByDeptCodeResponse>> FetchDepartmentByDeptCode(string deptCode)
        {
            try
            {
                var departments = await _appDbContext.DEPARTMENT
                    .Where(d => d.id.Trim() == deptCode.Trim())
                    .ToListAsync();

                var deptHeads = GetAll_DepartmentHead().Result;
                // Convert Department objects to DeptResponse objects
                var deptResponses = departments.Select(department => new DeptFetchDepartmentByDeptCodeResponse
                {
                    id = department.id,
                    name = department.name,
                    ChangeDate = department.ChangeDate,
                    ChangeUser = department.ChangeUser,
                    ChangeTerm = department.ChangeTerm,
                    DeptHead = department.DeptHead,
                    DeptHeadName = deptHeads.Find(p => p.EmpCode == department.DeptHead)?.ShortName ?? "",
                    AcctCode = department.AcctCode,
                    PayTable = department.PayTable,
                    CctrCode = department.CctrCode,
                    ShortDescription = department.ShortDescription,
                    AcctCodeAp = department.AcctCodeAp,
                    CctrCodeAp = department.CctrCodeAp,
                    Status = department.Status,
                    UdfNumber1 = department.UdfNumber1,
                    ShDeptCode = department.ShDeptCode,
                    ChangeReason = department.ChangeReason
                }).ToList();

              

                return deptResponses;
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting department: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }

        public async Task<bool> CreateDepartment(Department department)
        {
            try
            {

                bool codeExists = _appDbContext.DEPARTMENT.Any(g => (g.name).Trim().ToUpper() == (department.name).Trim().ToUpper() );
                if (codeExists)
                {
                    return false;
                }
                else
                {
                    Enums.TableName tableNameValue = Enums.TableName.DEPARTMENT;
                    Enums.TranWindowCol tranWindowColValue = Enums.TranWindowCol.SLA__DEPARTMENT;

                    string tableNameString = tableNameValue.ToString();
                    string tranWindowColString = tranWindowColValue.ToString();

                    string deptCode = _autoGenerateCodeRepository.GenerateCode(tableNameString, tranWindowColString);

                    // Add the department entity to the context and save changes
                    if (department.Status == "ACTIVE")
                    {
                        department.Status = "A";
                    }
                    if (department.Status == "INACTIVE")
                    {
                        department.Status = "N";
                    }

                    department.id = deptCode;
                    _appDbContext.DEPARTMENT.Add(department);
                    await _appDbContext.SaveChangesAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {

                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting department: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }

        public UpdateFlagResponse UpdateDepartment(Department department)
        {
            try
            {
                UpdateFlagResponse enum_flags = new UpdateFlagResponse();
                bool codeExists = _appDbContext.DEPARTMENT.Any(g => (g.id).Trim() == (department.id).Trim());
                if (codeExists)
                {
                    enum_flags.codeExists = true;
                    bool descrExists = _appDbContext.DEPARTMENT.Any(g => (g.id.Trim().ToUpper() != department.id.Trim().ToUpper()) && (g.name.Trim().ToUpper() == department.name.Trim().ToUpper()));
                    if (descrExists)
                    {
                        enum_flags.descrExists = true;
                    }
                    else
                    {
                        if (department.Status == "ACTIVE")
                        {
                            department.Status = "A";
                        }
                        if (department.Status == "INACTIVE")
                        {
                            department.Status = "N";
                        }
                        _appDbContext.Entry(department).State = EntityState.Modified;
                        _appDbContext.SaveChanges();
                        enum_flags.Success = true;
                    }
                }
                else
                {
                    enum_flags.codeExists = false;
                }
                return enum_flags;
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException) || ex.GetType() == typeof(DbUpdateConcurrencyException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting department: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }



        public async Task<DepartmentComboResponse> GetAllDepartmentcombo()
        {

            // Define queries and corresponding models
            List<(string query, Type model)> queryModelPairs = new List<(string, Type)>
            {
                ($@"SELECT EMP_CODE, SHORT_NAME FROM EMPLOYEE WHERE EMP_TYPE='H' AND RELIEVE_DATE IS NULL ORDER BY EMP_FNAME", typeof(DepartHead)),
                ($@"SELECT LONG_DESCR AS DESCRIPTION ,ACCT_CODE, DESCR FROM  ACCOUNTS WHERE LENGTH(TRIM(ACCT_CODE)) = 10 AND ACTIVE = 'Y' AND ACCT_CODE <>  ACCT_CODE__PAR ORDER BY DESCR", typeof(Acct_Code)),
                ($@"SELECT DESCR, CCTR_CODE FROM COSTCTR", typeof(Cctr_Code)),
                ($@"SELECT PAY_TABLE, DESCR FROM PAYTABLE", typeof(Pay_Table))
                // Add more query-model pairs as needed...
            };

            using (OracleConnection con = _appDbContext.GetConn())
            {
                using (OracleCommand cmd = _appDbContext.GetCommand())
                {
                    try
                    {
                        await con.OpenAsync();
                        cmd.BindByName = true;

                        foreach (var pair in queryModelPairs)
                        {
                            // Execute each query
                            cmd.CommandText = pair.query;

                            List<object> resultList = new List<object>();

                            using (OracleDataReader reader = cmd.ExecuteReader())
                            {
                                while (await reader.ReadAsync())
                                {
                                    // Map results based on the model type
                                    MapModel(reader, pair.model);
                                    //resultList.Add(result);
                                }
                            }

                        }

                        return lstResponse;
                    }
                    catch (Exception ex)
                    {
                        string str = ex.GetType().ToString();
                        if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                        {
                            //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                            //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                            string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                            throw new RepositoryException(error);
                        }
                        // Log or handle the exception appropriately
                        throw ex.InnerException;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        private DepartmentComboResponse MapModel(OracleDataReader reader, Type modelType)
        {

            List<DepartHead> lst_DeptHead = new List<DepartHead>();
            List<Acct_Code> lst_AccCode = new List<Acct_Code>();
            List<Cctr_Code> lst_CcrtCode = new List<Cctr_Code>();
            List<Pay_Table> lst_PayTable = new List<Pay_Table>();

            // Check if the modelType is DepartHead
            if (modelType == typeof(DepartHead))
            {
                while (reader.Read())
                {
                    // Create a new instance of DepartHead and populate its properties
                    DepartHead obj1 = new DepartHead
                    {
                        EmpCode = reader["EMP_CODE"].ToString(),
                        ShortName = reader["SHORT_NAME"].ToString()
                        // Map other properties of DepartHead as needed...
                    };
                    lst_DeptHead.Add(obj1);
                }
                lstResponse.DEPART_HEAD = lst_DeptHead;
            }

            if (modelType == typeof(Acct_Code))
            {
                while (reader.Read())
                {
                    // Create a new instance of DepartHead and populate its properties
                    Acct_Code obj2 = new Acct_Code
                    {
                        LONG_DESCR = reader["DESCRIPTION"].ToString(),
                        ACCT_CODE = reader["ACCT_CODE"].ToString(),
                        DESCR = reader["DESCR"].ToString()
                        // Map other properties of DepartHead as needed...
                    };
                    lst_AccCode.Add(obj2);
                }
                lstResponse.ACCT_CODE = lst_AccCode;
            }

            if (modelType == typeof(Cctr_Code))
            {
                while (reader.Read())
                {
                    // Create a new instance of DepartHead and populate its properties
                    Cctr_Code obj3 = new Cctr_Code
                    {
                        CCTR_CODE = reader["CCTR_CODE"].ToString(),
                        DESCR = reader["DESCR"].ToString()
                    };
                    lst_CcrtCode.Add(obj3);
                }
                lstResponse.CCTR_CODE = lst_CcrtCode;
            }

            if (modelType == typeof(Pay_Table))
            {
                while (reader.Read())
                {
                    // Create a new instance of DepartHead and populate its properties
                    Pay_Table obj4 = new Pay_Table
                    {
                        PAY_TABLE = reader["PAY_TABLE"].ToString(),
                        DESCR = reader["DESCR"].ToString()
                    };
                    lst_PayTable.Add(obj4);
                }
                lstResponse.PAY_TABLE = lst_PayTable;
            }

            return lstResponse;
        }

        public async Task<List<AcountsModel>> GetAll_Accounts()
        {
            try
            {
                return await _appDbContext.ACCOUNTS.ToListAsync();
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                _logger.LogError(ex, "An error occurred while retrieving accounts.");
                throw;
            }
        }

        public async Task<List<CostCenterModel>> GetAll_CostCentre()
        {
            try
            {
                return await _appDbContext.COSTCTR.ToListAsync();
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                _logger.LogError(ex, "An error occurred while retrieving while retriving cost center.");
                throw;
            }
        }


        public async Task<List<PayTableModel>> GetAll_PayTables()
        {
            try
            {
                return await _appDbContext.PAYTABLE.ToListAsync();
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                _logger.LogError(ex, "An error occurred while retrieving while retriving pay table.");
                throw;
            }
        }


        public async Task<List<DepartHead>> GetAll_DepartmentHead()
        {
            try
            {
                var query = @"SELECT emp_code, short_name
                                FROM employee
                                WHERE emp_type = 'H'
                                    AND RELIEVE_DATE IS NULL
                                     AND cadre IN ('L0004', 'L0005', 'L0006', 'L0007', 'F0004', 'F0005', 'F0006')
                                    ORDER BY short_name";

                var result = await _appDbContext.EMPLOYEE.FromSqlRaw(query)
                    .Select(employee => new DepartHead
                    {
                        EmpCode = employee.EMP_CODE,
                        ShortName = employee.SHORT_NAME

                    }).ToListAsync();
                return result;

            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                _logger.LogError(ex, "An error occurred while retrieving while retriving department head");
                throw;
            }
        }


    }
}
