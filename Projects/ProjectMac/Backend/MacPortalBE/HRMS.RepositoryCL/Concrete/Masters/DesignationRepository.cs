﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Designation;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.ADMINISTRATION.Masters.Grade;
using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Designation;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.Data.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace HRMS.RepositoryCL.Concrete.Masters
{
    public class DesignationRepository : IDesignationRepository
    {

        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        private readonly IAutoGenerateCodeRepository _autoGenerateCodeRepository;
        public DesignationRepository(AppDbContext appContext, IConfiguration configuration, IAutoGenerateCodeRepository autoGenerateCodeRepository)
        {   
            _appDbContext = appContext;
            _configuration = configuration;
            _autoGenerateCodeRepository = autoGenerateCodeRepository;
        }



        public async Task<(List<DesignationGetAllResponse>, int)> GetAllDesignations(int page, int items_per_page, string search)
        {
            List<DesignationGetAllResponse> designations = new List<DesignationGetAllResponse>();
            int totalCount = 0;

            try
            {
                int startRow = (page - 1) * items_per_page + 1;
                int endRow = page * items_per_page;

                using (OracleConnection con = _appDbContext.GetConn())
                {
                    using (OracleCommand cmd = _appDbContext.GetCommand())
                    {
                        await con.OpenAsync();

                        // Common parameters for both queries
                        cmd.Parameters.Add(new OracleParameter("search", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search));
                        cmd.Parameters.Add(new OracleParameter("searchUpper", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search.ToUpper()));

                        // Count query
                        string countQuery = @"SELECT COUNT(*) FROM DESIGNATION A LEFT OUTER JOIN GRADE B ON A.GRADE_CODE = B.GRADE_CODE LEFT OUTER JOIN CADRE C ON C.CADRE_CODE = B.CADRE_CODE WHERE A.UDF_STR1 = 'H' AND (:search IS NULL OR UPPER(DESIGNATION) LIKE '%' || :searchUpper || '%' OR UPPER(DESIGN_CODE) LIKE '%' || :searchUpper || '%' OR UPPER(LONG_DESIGNATION) LIKE '%' || :searchUpper || '%' OR UPPER(C.DESCR) LIKE '%' || :searchUpper || '%' OR UPPER(A.CHG_USER) LIKE '%' || :searchUpper || '%' OR UPPER(A.CHG_DATE) LIKE '%' || :searchUpper || '%' OR UPPER(A.STATUS) LIKE '%' || :searchUpper || '%' OR (:searchUpper = 'ACTIVE' AND UPPER(A.STATUS) = 'A') OR (:searchUpper = 'INACTIVE' AND UPPER(A.STATUS) = 'N'))";
                        cmd.CommandText = countQuery;
                        totalCount = Convert.ToInt32(await cmd.ExecuteScalarAsync());

                        // Main query
                        string desgQuery = $@"SELECT * FROM (SELECT A.DESIGN_CODE, A.DESIGNATION, A.LONG_DESIGNATION, A.STATUS, B.DESCR AS GRADE_NAME, C.DESCR AS CADRE_NAME, B.GRADE_ID, C.CADRE_CODE, A.GRADE_CODE, A.CHG_DATE, A.CHG_USER, A.CHG_TERM, A.CHG_REASON, ROW_NUMBER() OVER (ORDER BY DESIGN_CODE) seq FROM DESIGNATION A LEFT OUTER JOIN GRADE B ON A.GRADE_CODE = B.GRADE_CODE LEFT OUTER JOIN CADRE C ON C.CADRE_CODE = B.CADRE_CODE WHERE A.UDF_STR1 = 'H' AND (:search IS NULL OR UPPER(DESIGNATION) LIKE '%' || :searchUpper || '%' OR UPPER(DESIGN_CODE) LIKE '%' || :searchUpper || '%' OR UPPER(LONG_DESIGNATION) LIKE '%' || :searchUpper || '%' OR UPPER(C.DESCR) LIKE '%' || :searchUpper || '%' OR UPPER(A.CHG_USER) LIKE '%' || :searchUpper || '%' OR UPPER(A.CHG_DATE) LIKE '%' || :searchUpper || '%' OR UPPER(A.STATUS) LIKE '%' || :searchUpper || '%' OR (:searchUpper = 'ACTIVE' AND UPPER(A.STATUS) = 'A') OR (:searchUpper = 'INACTIVE' AND UPPER(A.STATUS) = 'N'))) WHERE seq BETWEEN :startRow AND :endRow";

                        cmd.BindByName = true;
                        cmd.CommandText = desgQuery;
                        cmd.Parameters.Add(new OracleParameter("startRow", startRow));
                        cmd.Parameters.Add(new OracleParameter("endRow", endRow));

                        using (OracleDataReader reader = (OracleDataReader)await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                DesignationGetAllResponse designation = new DesignationGetAllResponse
                                {
                                    Id = reader["DESIGN_CODE"].ToString(),
                                    name = reader["DESIGNATION"].ToString(),
                                    ChangeDate = reader["CHG_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["CHG_DATE"]),
                                    ChangeUser = reader["CHG_USER"].ToString(),
                                    ChangeTerm = reader["CHG_TERM"].ToString(),
                                    LongDesignation = reader["LONG_DESIGNATION"].ToString(),
                                    Status = reader["STATUS"].ToString(),
                                    ChangeReason = reader["GRADE_NAME"].ToString(),
                                    GradeId = reader["GRADE_ID"].ToString(),
                                    GradeCode = reader["GRADE_CODE"].ToString(),
                                    GradeName = reader["GRADE_NAME"].ToString(),
                                    CadreCode = reader["CADRE_CODE"].ToString(),
                                    CadreName = reader["CADRE_NAME"].ToString(),
                                };

                                designations.Add(designation);
                            }
                        }
                    }
                }

                return (designations, totalCount);
            }
            catch (Exception ex)
            {
                // Log or handle the exception appropriately
                string errorMessage = $"Error getting designations: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }


        public int CountAllDesignation()
        {
            int count = 0;
            using (OracleConnection con = _appDbContext.GetConn())
            {
                try
                {
                    con.Open();
                    string query = "SELECT COUNT(*) FROM Designation"; // Adjust table name as per your database schema
                    using (OracleCommand command = new OracleCommand(query, con))
                    {
                        // ExecuteScalar returns object, so we need to cast to decimal first
                        var result = command.ExecuteScalar();

                        // Then we can safely convert to int
                        count = Convert.ToInt32(result); ;
                    }
                    con.Close();

                }
                catch (Exception ex)
                {

                    Console.WriteLine("Error counting designation: " + ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
            return count;
        }



        public async Task<IEnumerable<DesignationGetDesignationByCodeResponse>> GetDesignationByCode(string code)
        {
            try
            {

                var designation = await _appDbContext.DESIGNATION
                                    .Where(d => d.Id.Trim() == code.Trim())
                                    .ToListAsync();

                var ddl_Names = await GetGrades();

                var designationResponse = designation.Select(designation => new DesignationGetDesignationByCodeResponse
                {
                    Id = designation.Id,
                    name = designation.name,
                    ChangeDate = designation.ChangeDate,
                    ChangeUser = designation.ChangeUser,
                    ChangeTerm = designation.ChangeTerm,
                    LongDesignation = designation.LongDesignation,
                    Status = designation.Status,
                    UdfNum1 = designation.UdfNum1,
                    UdfStr1 = designation.UdfStr1,
                    GradeCode = designation.GradeCode,
                    GradeName = ddl_Names.Find(p => p.id.Trim() == designation.GradeCode?.Trim())?.Description,
                    SapDesignCode = designation.SapDesignCode,
                    ChangeReason = designation.ChangeReason

                });

                return designationResponse;


            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                // Log the exception or handle it as needed
                throw new Exception($"Error getting designation with Id {code}", ex);
            }
        }


        public async Task<bool> CreateDesignation (DesignationModel designation)
        {
            try
            {
                bool codeExists = _appDbContext.DESIGNATION.Any(g => (g.name).Trim().ToUpper() == (designation.name).Trim().ToUpper());
                if (codeExists)
                {
                    return false;
                }
                else
                {
                    Enums.TableName tableNameValue = Enums.TableName.DESIGNATION;
                    Enums.TranWindowCol tranWindowColValue = Enums.TranWindowCol.SLA__DESIGNATIONLIST;

                    string tableNameString = tableNameValue.ToString();
                    string tranWindowColString = tranWindowColValue.ToString();

                    string Code = _autoGenerateCodeRepository.GenerateCode(tableNameString, tranWindowColString);

                    if (designation.Status == "ACTIVE")
                    {
                        designation.Status = "A";
                    }
                    if (designation.Status == "INACTIVE")
                    {
                        designation.Status = "N";
                    }
                    designation.Id = Code;
                    designation.ChangeDate = DateTime.Now;
                    _appDbContext.DESIGNATION.Add(designation);
                    await _appDbContext.SaveChangesAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {

                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting designation: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }

        public async Task<UpdateFlagResponse> UpdateDesignation(DesignationModel designation)
        {
            try
            {
                UpdateFlagResponse enum_flags = new UpdateFlagResponse();
                bool codeExists = _appDbContext.DESIGNATION.Any(g => (g.Id).Trim() == (designation.Id).Trim());
                if (codeExists)
                {
                    enum_flags.codeExists = true;
                    bool descrExists = _appDbContext.DESIGNATION.Any(g => (g.name.Trim().ToUpper() == designation.name.Trim().ToUpper()) && (g.Id.Trim().ToUpper() != designation.Id.Trim().ToUpper()));
                    if (descrExists)
                    {
                        enum_flags.descrExists = true;
                    }
                    else
                    {
                        if (designation.Status == "ACTIVE")
                        {
                            designation.Status = "A";
                        }
                        if (designation.Status == "INACTIVE")
                        {
                            designation.Status = "N";
                        }
                        _appDbContext.Entry(designation).State = EntityState.Modified;
                        await _appDbContext.SaveChangesAsync();
                        enum_flags.Success = true;
                        enum_flags.Success = true;
                    }
                }
                else
                {
                    enum_flags.codeExists = false;
                }

                return enum_flags;
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException) || ex.GetType() == typeof(DbUpdateConcurrencyException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while update designation: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        public async Task<List<GradeModel>> GetGrades()
        {
            try
            {
                var grades = await _appDbContext.GRADE
                    .Where(g => g.GradeType == "H")
                    .OrderBy(g => g.Description)
                    .Select(g => new GradeModel
                    {
                        id = g.id,
                        Description = g.Description.ToUpper(),
                        ShortDescription = g.ShortDescription.ToUpper(),
                        CadreCode = g.CadreCode
                    })
                    .ToListAsync();

                return grades;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting grades: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

    }
}
