﻿using HRMS.RepositoryCL.Interface.Masters;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.RepositoryCL.Comman;
using Microsoft.EntityFrameworkCore;
using HRMS.ModelCL.Payloads.Employee;
using HRMS_Core.Data.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore.Internal;
using HRMS.ModelCL.ADMINISTRATION.Masters.Site;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using HRMS.ModelCL;
using System.Globalization;
using HRMS.ModelCL.Payloads;
using System.Net.NetworkInformation;
using Microsoft.AspNetCore.Http;
using static HRMS.RepositoryCL.Comman.Enums;
using System.Reflection.Emit;
using System.Reflection.PortableExecutable;
using System.Diagnostics.CodeAnalysis;
using Oracle.ManagedDataAccess.Types;
using System.Diagnostics;
using HRMS.ModelCL.Payloads.Recruitment;
using Microsoft.AspNetCore.Http.HttpResults;
using System.Collections;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using HRMS.ModelCL.Payloads.Employee.Language;
using HRMS.ModelCL.ADMINISTRATION.Masters.Designation;
using Microsoft.EntityFrameworkCore.Storage;
using HRMS.ModelCL.Payloads.Employee.Family;
using HRMS.ModelCL.Payloads.Employee.Qualification;
using HRMS.ModelCL.Payloads.Employee.Experience;
using HRMS.ModelCL.Payloads.Employee.Assets;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using System.Text.Json;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing;
using HRMS.ModelCL.Payloads.Employee.Emp;
using System.Data.Common;



namespace HRMS.RepositoryCL.Concrete.Masters
{
    public class EmployeeRepository : Controller,  IEmployeeRepository 
    {

        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        private readonly IAutoGenerateCodeRepository _autoGenerateCodeRepository;
        private readonly ImageFetchingHelper _imageFetchingHelper;
        private readonly IDbConnection _dbConnection;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public EmployeeRepository(AppDbContext appContext, IConfiguration configuration, IAutoGenerateCodeRepository autoGenerateCodeRepository, ImageFetchingHelper imageFetchingHelper, IDbConnection dbConnection, IHttpContextAccessor httpContextAccessor)
        {
            _appDbContext = appContext;
            _configuration = configuration;
            _autoGenerateCodeRepository = autoGenerateCodeRepository;
            _imageFetchingHelper = imageFetchingHelper;
            _dbConnection = dbConnection;
            _httpContextAccessor = httpContextAccessor;
        }

       

        public async Task<(List<EmployeeGetAllEmployeeResponse>, int)> FetchEmployees(int page, int items_per_page, string location, string department, string status, string search)
        {
            // Map status values "Active" and "Separated" to database values "A" and "S"
            string mappedStatus = status == "ACTIVE" ? "A" : (status == "SEPERATED" ? "S" : null);

            // Calculate the starting row number based on the page number and page size
            int startRow = (page - 1) * items_per_page;
            int endRow = page * items_per_page;

            List<EmployeeGetAllEmployeeResponse> employees = new List<EmployeeGetAllEmployeeResponse>();

            string query = @"
SELECT 
    A.EMP_CODE, 
    A.EMP_FNAME, 
    A.EMP_MNAME, 
    A.EMP_LNAME, 
    A.STATUS,
    CASE WHEN G.FLD_VALUE = 'Y' THEN '' ELSE A.DESIGNATION END AS DESIGNATION, 
    C.DESCR LOCATION, 
    D.DESCR AS DEPARTMENT, 
    TO_CHAR(A.BIRTH_DATE, 'DD/MM/YYYY') AS DOB, 
    TO_CHAR(A.DATE_JOIN, 'DD/MM/YYYY') AS DATE_JOIN, 
    CASE WHEN G.FLD_VALUE = 'Y' THEN 0 ELSE A.BASIC END AS BASIC, 
    CASE WHEN G.FLD_VALUE = 'Y' THEN 0 ELSE A.GROSS END AS GROSS, 
    CASE WHEN G.FLD_VALUE = 'Y' THEN '' ELSE A.DESIGN_CODE END AS DESIGN_CODE, 
    A.EMP_SITE, 
    A.DEPT_CODE, 
    CASE WHEN G.FLD_VALUE = 'Y' THEN '' ELSE A.GRADE END AS GRADE, 
    CASE WHEN G.FLD_VALUE = 'Y' THEN '' ELSE A.CADRE END AS CADRE, 
    A.CHG_USER, 
    A.CHG_DATE, 
    A.CHG_TERM 
FROM 
    EMPLOYEE A 
    LEFT OUTER JOIN DESIGNATION B ON A.DESIGNATION = B.DESIGN_CODE 
    LEFT OUTER JOIN SITE C ON C.SITE_CODE = A.EMP_SITE 
    LEFT OUTER JOIN DEPARTMENT D ON D.DEPT_CODE = A.DEPT_CODE 
    LEFT OUTER JOIN GENCODES G ON G.MOD_NAME = 'W_APPR' 
    AND G.FLD_NAME = 'APPR_LOCK' 
    AND G.ACTIVE = 'Y' 
WHERE 
    A.EMP_TYPE = 'H' ";

            if (!string.IsNullOrEmpty(location))
            {
                query += "AND A.EMP_SITE = :location ";
            }

            if (!string.IsNullOrEmpty(department))
            {
                query += "AND A.DEPT_CODE = :department ";
            }

            if (!string.IsNullOrEmpty(mappedStatus))
            {
                // Assuming 'status' field is in EMPLOYEE table
                query += "AND A.STATUS = :status ";
            }

            if (!string.IsNullOrEmpty(search))
            {
                query += @"AND (
                    UPPER(A.EMP_CODE) LIKE '%' || :searchUpper || '%' 
                    OR UPPER(A.EMP_FNAME) LIKE '%' || :searchUpper || '%' 
                    OR UPPER(A.EMP_MNAME) LIKE '%' || :searchUpper || '%' 
                    OR UPPER(A.EMP_LNAME) LIKE '%' || :searchUpper || '%' 
                    OR UPPER(A.CHG_USER) LIKE '%' || :searchUpper || '%' 
                    OR TO_CHAR(A.CHG_DATE, 'DD/MM/YYYY') LIKE '%' || :searchUpper || '%'
                ) ";
            }

            query += "ORDER BY A.EMP_FNAME";

            int count = 0;

            using (OracleConnection connection = _appDbContext.GetConn())
            {
                using (OracleCommand command = _appDbContext.GetCommand())
                {
                    try
                    {
                        await connection.OpenAsync();
                        command.BindByName = true;
                        command.CommandText = query;

                        command.Parameters.Add(new OracleParameter(":startRow", OracleDbType.Int32, startRow, ParameterDirection.Input));
                        command.Parameters.Add(new OracleParameter(":endRow", OracleDbType.Int32, endRow, ParameterDirection.Input));
                        command.Parameters.Add(new OracleParameter(":location", string.IsNullOrEmpty(location) ? (object)DBNull.Value : location));
                        command.Parameters.Add(new OracleParameter(":department", string.IsNullOrEmpty(department) ? (object)DBNull.Value : department));
                        command.Parameters.Add(new OracleParameter(":status", string.IsNullOrEmpty(mappedStatus) ? (object)DBNull.Value : mappedStatus));
                        command.Parameters.Add(new OracleParameter(":searchUpper", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search.ToUpper()));

                        using (OracleDataReader reader = (OracleDataReader)await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var employee = new EmployeeGetAllEmployeeResponse();

                                employee.id = reader["EMP_CODE"].ToString();
                                employee.EMP_FNAME = reader["EMP_FNAME"].ToString();
                                employee.EMP_MNAME = reader["EMP_MNAME"].ToString();
                                employee.EMP_LNAME = reader["EMP_LNAME"].ToString();
                                employee.DESIGNATION = reader["DESIGNATION"].ToString();
                                employee.LOCATION = reader["LOCATION"].ToString();
                                employee.DEPARTMENT = reader["DEPARTMENT"].ToString();
                                employee.DOB = (DateTime)GetDateOrNull(reader, "DOB");
                                employee.DATE_JOIN = (DateTime)GetDateOrNull(reader, "DATE_JOIN");
                                employee.BASIC = Convert.ToDecimal(reader["BASIC"]);
                                employee.GROSS = Convert.ToDecimal(reader["GROSS"]);
                                employee.DESIGN_CODE = reader["DESIGN_CODE"].ToString();
                                employee.EMP_SITE = reader["EMP_SITE"].ToString();
                                employee.DEPT_CODE = reader["DEPT_CODE"].ToString();
                                employee.GRADE = reader["GRADE"].ToString();
                                employee.CADRE = reader["CADRE"].ToString();
                                employee.CHG_USER = reader["CHG_USER"].ToString();
                                employee.CHG_DATE = reader["CHG_DATE"] != DBNull.Value ? reader.GetDateTime("CHG_DATE").Date : default(DateTime);
                                employee.CHG_TERM = reader["CHG_TERM"].ToString();
                                employee.STATUS = reader["STATUS"].ToString();

                                employees.Add(employee);
                            }
                        }

                        count = employees.Count;

                        employees = employees.Skip((page - 1) * items_per_page)
                             .Take(items_per_page)
                             .ToList();

                        return (employees, count);
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = $"Error fetching employees: {ex.Message}";

                        if (ex.InnerException != null)
                        {
                            errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                        }

                        throw new Exception(errorMessage, ex.InnerException);
                    }
                }
            }
        }



        public async Task<(List<Employee>, int)> FetchFirstTenEmployees(int page, int items_per_page)
        {
            // Calculate the starting row number based on the page number and page size
            int startRow = (page - 1) * items_per_page;
            int endRow = page * items_per_page;

            List<Employee> employees = new List<Employee>();

            /*string query = @"
        SELECT * 
        FROM (
            SELECT * FROM EMPLOYEE
        ) 
        WHERE ROWNUM <= 10"; // Limit to the first 10 records
*/

            string query = $@"
        SELECT * FROM (
            SELECT e.*, ROWNUM AS seq FROM EMPLOYEE e
        ) WHERE seq BETWEEN :startRow AND :endRow";

            int count = 0; // Variable to hold the count of fetched employees

            using (OracleConnection connection = _appDbContext.GetConn())
            {
                using (OracleCommand cmd = _appDbContext.GetCommand())
                {
                    try
                    {
                        await connection.OpenAsync();
                        cmd.BindByName = true;
                        cmd.CommandText = query;
                        cmd.Parameters.Add(new OracleParameter("startRow", startRow));
                        cmd.Parameters.Add(new OracleParameter("endRow", endRow));

                        using (OracleDataReader reader = (OracleDataReader)await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                Employee employee = new Employee
                                {
                                    EMP_CODE = GetStringOrNull(reader, "EMP_CODE"),
                                    EMP_FNAME = GetStringOrNull(reader, "EMP_FNAME"),
                                    EMP_MNAME = GetStringOrNull(reader, "EMP_MNAME"),
                                    EMP_LNAME = GetStringOrNull(reader, "EMP_LNAME"),
                                    DESIGNATION = GetStringOrNull(reader, "DESIGNATION"),
                                    BIRTH_DATE = reader["BIRTH_DATE"] != DBNull.Value ? reader.GetDateTime("BIRTH_DATE").Date : default(DateTime),
                                    DATE_JOIN = reader["DATE_JOIN"] != DBNull.Value ? reader.GetDateTime("DATE_JOIN").Date : default(DateTime),
                                    BASIC = GetIntOrDefault(reader, "BASIC"),
                                    GROSS = GetIntOrDefault(reader, "GROSS"),
                                    DESIGN_CODE = GetStringOrNull(reader, "DESIGN_CODE"),
                                    EMP_SITE = GetStringOrNull(reader, "EMP_SITE"),
                                    DEPT_CODE = GetStringOrNull(reader, "DEPT_CODE"),
                                    GRADE = GetStringOrNull(reader, "GRADE"),
                                    CADRE = GetStringOrNull(reader, "CADRE"),
                                    PER_STATE = GetStringOrNull(reader, "PER_STATE")
                                };

                                employees.Add(employee);
                            }
                        }

                        // Now count the fetched employees
                        count = employees.Count;

                        return (employees, count);
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = $"Error fetching employees: {ex.Message}";

                        if (ex.InnerException != null)
                        {
                            errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                        }

                        throw new Exception(errorMessage, ex.InnerException);
                    }
                }
            }
        }






        public int CountAllEmployees()
        {
            try
            {
                // Assuming _dbContext is your Entity Framework DbContext
                return _appDbContext.EMPLOYEE.Count();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error counting employees: " + ex.Message);
                // Handle the exception or log it
                // For now, I'm just printing the error message to the console
                return 0; // Return 0 indicating failure
            }
        }

        public int CountSearchedEmployees(string search)
        {
            try
            {
                using (var connection = _appDbContext.GetConn())
                {
                    connection.Open();
                    var command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(*) FROM EMPLOYEE WHERE UPPER(EMP_FNAME) LIKE '%' || :searchUpper || '%' OR UPPER(EMP_LNAME) LIKE '%' || :searchUpper || '%'";
                    command.Parameters.Add(new OracleParameter("searchUpper", search.ToUpper()));
                    return Convert.ToInt32(command.ExecuteScalar());
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions
                throw new RepositoryException("Error counting searched employees.", ex);
            }
        }


        public Employee FetchEmployeeByEmpCode(OracleConnection connection, string empCode)
        {
            using (var command = new OracleCommand("SELECT * FROM EMPLOYEE WHERE EMP_CODE = :empCode", connection))
            {
                command.Parameters.Add(new OracleParameter("empCode", empCode));

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var employee = new Employee
                        {
                            EMP_CODE = GetStringOrNull(reader, "EMP_CODE"),
                            EMP_FNAME = GetStringOrNull(reader, "EMP_FNAME"),
                            EMP_MNAME = GetStringOrNull(reader, "EMP_MNAME"),
                            EMP_LNAME = GetStringOrNull(reader, "EMP_LNAME"),
                            DESIGNATION = GetStringOrNull(reader, "DESIGNATION"),
                            // BIRTH_DATE = GetDateTime(reader, "BIRTH_DATE"),
                            //DATE_JOIN = GetDateTime(reader, "DATE_JOIN"),
                            BIRTH_DATE = reader["BIRTH_DATE"] != DBNull.Value ? reader.GetDateTime("BIRTH_DATE").Date : default(DateTime),
                            DATE_JOIN = reader["DATE_JOIN"] != DBNull.Value ? reader.GetDateTime("DATE_JOIN").Date : default(DateTime),
                            BASIC = GetIntOrDefault(reader, "BASIC"),
                            GROSS = GetIntOrDefault(reader, "GROSS"),
                            DESIGN_CODE = GetStringOrNull(reader, "DESIGN_CODE"),
                            EMP_SITE = GetStringOrNull(reader, "EMP_SITE"),
                            DEPT_CODE = GetStringOrNull(reader, "DEPT_CODE"),
                            GRADE = GetStringOrNull(reader, "GRADE"),
                            CADRE = GetStringOrNull(reader, "CADRE"),
                            PER_STATE = GetStringOrNull(reader, "PER_STATE"),
                            SEX = GetStringOrNull(reader, "SEX"),
                            PROC_GROUP = GetStringOrNull(reader, "PROC_GROUP"),
                            //IMAGE = reader["IMAGE"] != DBNull.Value ? (byte[])reader["IMAGE"] : null,
                        };

                        return employee;
                    }
                }
            }

            return null; // Return null if the employee with the given EMP_CODE is not found
        }

        // complete controller tomorrow

        public async Task<IEnumerable<Employee>> GetEmployeeByCode(string empCode)
        {
            try
            {
                // Validate input
                if (string.IsNullOrEmpty(empCode))
                {
                    throw new ArgumentException("Employee code cannot be null or empty.");
                }

                // LINQ query to find the employee by EMP_CODE
                var employees = await _appDbContext.EMPLOYEE
                                     .Where(emp => emp.EMP_CODE == empCode)
                                     .ToListAsync();

                if (employees.Any())
                {
                    return employees;
                }
                else
                {
                    throw new Exception(" Employee not found ");
                }
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException) || ex.GetType() == typeof(DbUpdateConcurrencyException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }
                // Log the exception or handle it as needed
                throw new Exception($"Error getting employee with code {empCode}", ex);
            }
        }


        public void InsertEmployee(OracleConnection connection, Employee newEmployee)
        {
            using (var command = new OracleCommand("INSERT INTO EMPLOYEE (EMP_CODE, EMP_FNAME, EMP_MNAME, EMP_LNAME, DESIGNATION, BIRTH_DATE, DATE_JOIN, BASIC, GROSS, DESIGN_CODE, EMP_SITE, DEPT_CODE, GRADE, CADRE, PER_STATE, SEX, PROC_GROUP) " +
                                                    "VALUES (:empCode, :empFName, :empMName, :empLName, :designation, :birthDate, :dateJoin, :basic, :gross, :designCode, :empSite, :deptCode, :grade, :cadre, :perState, :sex, :procgroup)", connection))
            {
                command.Parameters.Add(new OracleParameter("empCode", newEmployee.EMP_CODE));
                command.Parameters.Add(new OracleParameter("empFName", newEmployee.EMP_FNAME));
                command.Parameters.Add(new OracleParameter("empMName", newEmployee.EMP_MNAME));
                command.Parameters.Add(new OracleParameter("empLName", newEmployee.EMP_LNAME));
                command.Parameters.Add(new OracleParameter("designation", newEmployee.DESIGNATION));
                command.Parameters.Add(new OracleParameter("birthDate", newEmployee.BIRTH_DATE));
                command.Parameters.Add(new OracleParameter("dateJoin", newEmployee.DATE_JOIN));
                command.Parameters.Add(new OracleParameter("basic", newEmployee.BASIC));
                command.Parameters.Add(new OracleParameter("gross", newEmployee.GROSS));
                command.Parameters.Add(new OracleParameter("designCode", newEmployee.DESIGN_CODE));
                command.Parameters.Add(new OracleParameter("empSite", newEmployee.EMP_SITE));
                command.Parameters.Add(new OracleParameter("deptCode", newEmployee.DEPT_CODE));
                command.Parameters.Add(new OracleParameter("grade", newEmployee.GRADE));
                command.Parameters.Add(new OracleParameter("cadre", newEmployee.CADRE));
                command.Parameters.Add(new OracleParameter("perState", newEmployee.PER_STATE));
                command.Parameters.Add(new OracleParameter("sex", newEmployee.SEX));
                command.Parameters.Add(new OracleParameter("procGroup", newEmployee.PROC_GROUP));

                command.ExecuteNonQuery();
            }
        }

        public void UpdateEmployeeDetails(OracleConnection connection, string empCode, Employee updatedEmployee)
        {
            OracleTransaction transaction = null;

            try
            {
                transaction = connection.BeginTransaction();

                using (var command = new OracleCommand("UPDATE EMPLOYEE SET EMP_FNAME = :empFName, EMP_MNAME = :empMName, EMP_LNAME = :empLName, DESIGNATION = :designation, " +
                                                       "BIRTH_DATE = TO_DATE(:birthDate, 'YYYY-MM-DD'), " +
                                                       "DATE_JOIN = TO_DATE(:dateJoin, 'YYYY-MM-DD'), " +
                                                       "BASIC = :basic, GROSS = :gross, DESIGN_CODE = :designCode, " +
                                                       "EMP_SITE = :empSite, DEPT_CODE = :deptCode, GRADE = :grade, CADRE = :cadre, " +
                                                       "PER_STATE = :perState, SEX = :sex, PROC_GROUP = :procGroup WHERE EMP_CODE = :empCode", connection))
                {
                    // Log SQL query
                    Console.WriteLine($"Executing SQL: {command.CommandText}");

                    // Set parameters with explicit data type conversion
                    command.Parameters.Add(new OracleParameter("empCode", empCode));
                    command.Parameters.Add(new OracleParameter("empFName", updatedEmployee.EMP_FNAME));
                    command.Parameters.Add(new OracleParameter("empMName", updatedEmployee.EMP_MNAME));
                    command.Parameters.Add(new OracleParameter("empLName", updatedEmployee.EMP_LNAME));
                    command.Parameters.Add(new OracleParameter("designation", updatedEmployee.DESIGNATION));
                    command.Parameters.Add(new OracleParameter("birthDate", updatedEmployee.BIRTH_DATE.ToString("yyyy-MM-dd")));
                    command.Parameters.Add(new OracleParameter("dateJoin", updatedEmployee.DATE_JOIN.ToString("yyyy-MM-dd")));
                    command.Parameters.Add(new OracleParameter("basic", updatedEmployee.BASIC));
                    command.Parameters.Add(new OracleParameter("gross", updatedEmployee.GROSS));
                    command.Parameters.Add(new OracleParameter("designCode", updatedEmployee.DESIGN_CODE));
                    command.Parameters.Add(new OracleParameter("empSite", updatedEmployee.EMP_SITE));
                    command.Parameters.Add(new OracleParameter("deptCode", updatedEmployee.DEPT_CODE));
                    command.Parameters.Add(new OracleParameter("grade", updatedEmployee.GRADE));
                    command.Parameters.Add(new OracleParameter("cadre", updatedEmployee.CADRE));
                    command.Parameters.Add(new OracleParameter("perState", updatedEmployee.PER_STATE));
                    command.Parameters.Add(new OracleParameter("sex", updatedEmployee.SEX));
                    command.Parameters.Add(new OracleParameter("procGroup", updatedEmployee.PROC_GROUP));

                    // Log parameter values
                    foreach (OracleParameter parameter in command.Parameters)
                    {
                        Console.WriteLine($"{parameter.ParameterName}: {parameter.Value}");
                    }
                    // Execute the update
                    command.ExecuteNonQuery();

                    transaction.Commit();

                    // Log success
                    Console.WriteLine($"Transaction committed successfully");
                }
            }
            catch (Exception ex)
            {
                // Rollback the transaction on exception
                if (transaction != null)
                {
                    transaction.Rollback();
                }

                throw; // Re-throw the exception to propagate it up the stack
            }
            finally
            {
                // Ensure the transaction is disposed
                transaction?.Dispose();
            }
        }


        public bool EmployeeExists(OracleConnection connection, string empCode)
        {
            using (var command = new OracleCommand("SELECT COUNT(*) FROM EMPLOYEE WHERE EMP_CODE = :empCode", connection))
            {
                command.Parameters.Add(new OracleParameter("empCode", empCode));

                var count = Convert.ToInt32(command.ExecuteScalar());

                return count > 0;
            }
        }


        // To handle null from DataBase-----------------------------------------------------------------
        private string GetStringOrNull(OracleDataReader reader, string columnName)
        {
            return reader[columnName] != DBNull.Value ? reader[columnName].ToString() : null;
        }



        private DateTime? GetDateOrNull(OracleDataReader reader, string columnName)
        {
            int columnIndex = reader.GetOrdinal(columnName);

            if (!reader.IsDBNull(columnIndex))
            {
                string dateString = reader.GetString(columnIndex).Trim(); // Trim leading and trailing whitespace

                if (!string.IsNullOrEmpty(dateString))
                {
                    string[] formats = { "dd-MM-yy", "dd/MM/yyyy" }; // Specify the possible date formats

                    if (DateTime.TryParseExact(dateString, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime result))
                    {
                        return result;
                    }
                    else
                    {
                        // Handle invalid date format here if needed
                        return null;
                    }
                }
            }
            return null;
        }



        private int GetIntOrDefault(OracleDataReader reader, string columnName)
        {
            return reader[columnName] != DBNull.Value ? Convert.ToInt32(reader[columnName]) : 0;
        }

        private decimal GetDecimalOrDefault(OracleDataReader reader, string columnName)
        {
            return reader[columnName] != DBNull.Value ? Convert.ToDecimal(reader[columnName]) : 0m;
        }


        //-------------------------------------------------------------------------------------------------


        // DropDowns

        // site codes
        /* public async Task<List<EmployeeGetSiteCodesResponse>> ddl_GetSiteCodes(string empcode)
         {
             var site_code = new List<EmployeeGetSiteCodesResponse>();

             try
             {

                 *//*var sqlQuery = @"SELECT A.SITE_CODE, B.DESCR, NVL(B.DIV_ID__OLD,'0') AS DIV_ID__OLD, NVL(A.LOC_GROUP, 'X') AS LOC_GROUP 
                       FROM USER_SITE A, SITE B 
                       WHERE A.SITE_CODE = B.SITE_CODE AND A.USER_ID = :empcode 
                       ORDER BY 1";*//*

                 var sqlQuery = @$"SELECT A.SITE_CODE, B.DESCR, NVL(B.DIV_ID__OLD,'0') AS DIV_ID__OLD, NVL(A.LOC_GROUP, 'X') AS LOC_GROUP 
                        FROM USER_SITE A, SITE B 
                        WHERE A.SITE_CODE = B.SITE_CODE AND A.USER_ID = '{empcode}' 
                        ORDER BY 1";

                 using (var connection = _appDbContext.GetConn())
                 using (var command = new OracleCommand(sqlQuery, connection))
                 {
                     // Capture the empcode parameter in a local variable
                     command.Parameters.Add(new OracleParameter("empcode", empcode));

                     try
                     {
                         await connection.OpenAsync();

                         // Execute the query asynchronously
                         using (var reader = await command.ExecuteReaderAsync())
                         {
                             while (await reader.ReadAsync())
                             {
                                 var site = new EmployeeGetSiteCodesResponse
                                 {
                                     SITE_CODE = reader["SITE_CODE"].ToString(),
                                     DESCR = reader["DESCR"].ToString(),
                                     DIV_ID__OLD = reader["DIV_ID__OLD"].ToString(),
                                     LOC_GROUP = reader["LOC_GROUP"].ToString()
                                 };

                                 site_code.Add(site);
                             }
                         }
                     }
                     catch (Exception ex)
                     {
                         // Log or handle the exception appropriately
                         throw new Exception("Error retrieving site codes", ex);
                     }
                 }
             }
             catch (Exception ex)
             {

                 string str = ex.GetType().ToString();
                 if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                 {
                     //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                     //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                     string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                     throw new RepositoryException(error);
                 }
                 string errorMessage = $"Error while getting site codes: {ex.Message}";

                 if (ex.InnerException != null)
                 {
                     errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                 }

                 throw new Exception(errorMessage, ex.InnerException);
             }

             return site_code;
         }



         public async Task<List<EmployeeGetLocationResponse>> ddl_GetLocations(string sessionUserSite)
         {
             var sites = new List<EmployeeGetLocationResponse>();

             try
             {

                 var sqlQuery = @"
                     SELECT SITE_CODE, UPPER(DESCR) AS DESCR, AREA_CODE, ADD1 || '' || ADD2 || '' || CITY || '-' || PIN AS ADDRESS, FIN_ENTITY
                     FROM SITE
                     WHERE SITE_CODE IN (:sessionUserSite) ORDER BY SITE_CODE";


                 using (var connection = _appDbContext.GetConn())
                 using (var command = new OracleCommand(sqlQuery, connection))
                 {
                     // Capture the sessionUserSite parameter in a local variable
                     command.Parameters.Add(new OracleParameter("sessionUserSite", sessionUserSite));

                     try
                     {
                         await connection.OpenAsync();

                         // Execute the query asynchronously
                         using (var reader = await command.ExecuteReaderAsync())
                         {
                             while (await reader.ReadAsync())
                             {
                                 var site = new EmployeeGetLocationResponse
                                 {
                                     SiteCode = reader["SITE_CODE"].ToString(),
                                     Description = reader["DESCR"].ToString(),
                                     AreaCode = reader["AREA_CODE"].ToString(),
                                     Address = reader["ADDRESS"].ToString(),
                                     FinancialEntity = reader["FIN_ENTITY"].ToString()
                                 };

                                 sites.Add(site);
                             }
                         }
                     }
                     catch (Exception ex)
                     {
                         // Log or handle the exception appropriately
                         throw new Exception("Error retrieving sites", ex);
                     }
                 }
             }
             catch (Exception ex)
             {

                 string str = ex.GetType().ToString();
                 if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                 {
                     //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                     //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                     string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                     throw new RepositoryException(error);
                 }
                 string errorMessage = $"Error while getting sites: {ex.Message}";

                 if (ex.InnerException != null)
                 {
                     errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                 }

                 throw new Exception(errorMessage, ex.InnerException);
             }

             return sites;
         }*/


        //______________________________________________________________________________________________________________________________________________________

        public async Task<List<EmployeeGetSiteCodesResponse>> ddl_GetSiteCodes(string empcode)
        {
            var site_code = new List<EmployeeGetSiteCodesResponse>();

            try
            {
                var sqlQuery = @$"SELECT A.SITE_CODE, B.DESCR, NVL(B.DIV_ID__OLD,'0') AS DIV_ID__OLD, NVL(A.LOC_GROUP, 'X') AS LOC_GROUP 
                              FROM USER_SITE A, SITE B 
                              WHERE A.SITE_CODE = B.SITE_CODE AND A.USER_ID = '{empcode}' 
                              ORDER BY 1";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {
                    command.Parameters.Add(new OracleParameter("empcode", empcode));

                    try
                    {
                        await connection.OpenAsync();

                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var site = new EmployeeGetSiteCodesResponse
                                {
                                    SITE_CODE = reader["SITE_CODE"].ToString(),
                                    DESCR = reader["DESCR"].ToString(),
                                    DIV_ID__OLD = reader["DIV_ID__OLD"].ToString(),
                                    LOC_GROUP = reader["LOC_GROUP"].ToString()
                                };

                                site_code.Add(site);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error retrieving site codes", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting site codes: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return site_code;
        }

        public async Task<List<EmployeeGetLocationResponse>> ddl_GetLocations(List<string> siteCodes)
        {
            var sites = new List<EmployeeGetLocationResponse>();

            try
            {
                var parameterNames = siteCodes.Select((_, index) => $":siteCode{index}").ToList();
                var parameters = siteCodes.Select((siteCode, index) => new OracleParameter($":siteCode{index}", siteCode)).ToArray();

                var sqlQuery = $@"
                SELECT SITE_CODE, UPPER(DESCR) AS DESCR, AREA_CODE, ADD1 || '' || ADD2 || '' || CITY || '-' || PIN AS ADDRESS, FIN_ENTITY
                FROM SITE
                WHERE SITE_CODE IN ({string.Join(", ", parameterNames)}) ORDER BY SITE_CODE";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {
                    command.Parameters.AddRange(parameters);

                    try
                    {
                        await connection.OpenAsync();

                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var site = new EmployeeGetLocationResponse
                                {
                                    SiteCode = reader["SITE_CODE"].ToString(),
                                    Description = reader["DESCR"].ToString(),
                                    AreaCode = reader["AREA_CODE"].ToString(),
                                    Address = reader["ADDRESS"].ToString(),
                                    FinancialEntity = reader["FIN_ENTITY"].ToString()
                                };

                                sites.Add(site);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error retrieving sites", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting sites: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return sites;
        }

        public async Task<List<EmployeeGetLocationResponse>> GetLocationsForEmployee(string empcode)
        {
            var siteCodesResponse = await ddl_GetSiteCodes(empcode);
            var siteCodes = siteCodesResponse.Select(sc => sc.SITE_CODE).ToList();
            var locations = await ddl_GetLocations(siteCodes);
            return locations;
        }


        //______________________________________________________________________________________________________________________________________________________


        public async Task<List<EmployeeGetDepartmentResponse>> ddl_GetDepartment()
        {
            try
            {
                /*
                 SELECT A.DEPT_CODE, A.DESCR, B.SHORT_NAME AS DEPT_HEAD, A.STATUS, A.CHG_DATE, A.CHG_USER, A.CHG_TERM FROM DEPARTMENT A LEFT OUTER JOIN EMPLOYEE B 
                 ON A.DEPT_HEAD=B.EMP_CODE WHERE A.DEPT_CODE LIKE 'D%' ORDER BY A.DESCR
                */

                var departments = await _appDbContext.DEPARTMENT
                 .Where(d => d.id.StartsWith("D"))
                 .OrderBy(d => d.name)
                 .GroupJoin(_appDbContext.EMPLOYEE,
                    d => d.DeptHead,
                    e => e.EMP_CODE,
                (d, employees) => new { Department = d, Employees = employees })
                .SelectMany(x => x.Employees.DefaultIfEmpty(),
                (d, e) => new EmployeeGetDepartmentResponse
                {
                    DeptCode = d.Department.id,
                    Description = d.Department.name,
                    //DepartmentHead = e != null ? e.SHORT_NAME : null, // SHORT_NAME is the property in Employee representing the department head's name
                    DepartmentHead = e.SHORT_NAME,
                    Status = d.Department.Status,
                    ChangeDate = d.Department.ChangeDate,
                    ChangeUser = d.Department.ChangeUser,
                    ChangeTerm = d.Department.ChangeTerm
                })
                   .ToListAsync();

                return departments;

            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting Department: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }


        public IEnumerable<string> ddl_GetStatus()
        {
            var statuses = new List<string>
        {
            // Assuming you have a mapping for database values to display values
            { GetDisplayStatus("A") }, // Mapping "A" to "Active"
            { GetDisplayStatus("S") }  // Mapping "S" to "Seperated"
        };

            return statuses;
        }

        private string GetDisplayStatus(string databaseValue)
        {
            // Perform mapping here
            switch (databaseValue)
            {
                case "A":
                    return "ACTIVE";
                case "S":
                    return "SEPERATED";
                default:
                    return "UNKONOWN";
            }
        }


        public async Task<List<EmployeeGetDesignationResponse>> ddl_GetDesignation()
        {
            var designations = new List<EmployeeGetDesignationResponse>();

            try
            {

                var sqlQuery = @"SELECT A.DESIGN_CODE,A.DESIGNATION,A.LONG_DESIGNATION,A.STATUS,B.DESCR AS GRADE_NAME,
                                C.DESCR AS CADRE_NAME,B.GRADE_ID,C.CADRE_CODE,C.BAND_CODE,A.GRADE_CODE,A.CHG_DATE,A.CHG_USER,
                                A.CHG_TERM  FROM DESIGNATION A INNER JOIN GRADE B ON A.GRADE_CODE=B.GRADE_CODE 
                                INNER JOIN CADRE C ON C.CADRE_CODE=B.CADRE_CODE WHERE A.UDF_STR1='H' ORDER BY 2";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var designation = new EmployeeGetDesignationResponse
                                {
                                    DESIGN_CODE = reader["DESIGN_CODE"].ToString(),
                                    DESIGNATION = reader["DESIGNATION"].ToString(),
                                    LONG_DESIGNATION = reader["LONG_DESIGNATION"].ToString(),
                                    //STATUS = Convert.ToChar(reader["STATUS"]),
                                    STATUS = reader["STATUS"] == DBNull.Value ? null : Convert.ToChar(reader["STATUS"]),
                                    GRADE_NAME = reader["GRADE_NAME"].ToString(),
                                    CADRE_NAME = reader["CADRE_NAME"].ToString(),
                                    GRADE_ID = reader["GRADE_ID"].ToString(),
                                    CADRE_CODE = reader["CADRE_CODE"].ToString(),
                                    BAND_CODE = reader["BAND_CODE"].ToString(),
                                    GRADE_CODE = reader["GRADE_CODE"].ToString(),
                                    CHG_DATE = reader["CHG_DATE"] != DBNull.Value ? reader.GetDateTime("CHG_DATE").Date : default(DateTime),
                                    CHG_USER = reader["CHG_USER"].ToString(),
                                    CHG_TERM = reader["CHG_TERM"].ToString(),
                                };

                                designations.Add(designation);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving Designation", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting Designation: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return designations;
        }

        public async Task<List<EmployeeGetProcGroupResponse>> ddl_GetProcGroup()
        {
            var groups = new List<EmployeeGetProcGroupResponse>();

            try
            {

                var sqlQuery = @"SELECT RTRIM (FLD_VALUE) PROC_GROUP, DESCR FROM GENCODES WHERE FLD_NAME='PROC_GROUP' 
                                    AND TRIM(MOD_NAME) = 'W_EMP_NITIN'  AND ACTIVE='Y' ORDER BY 1";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var group = new EmployeeGetProcGroupResponse
                                {
                                    PROC_GROUP = reader["PROC_GROUP"].ToString(),
                                    DESCR = reader["DESCR"].ToString()
                                };

                                groups.Add(group);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving Proc_Group", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting Proc_Group: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return groups;
        }

        public async Task<List<EmployeeGetBankResponse>> ddl_GetBank()
        {
            var banks = new List<EmployeeGetBankResponse>();

            try
            {

                var sqlQuery = @"SELECT BANK_CODE, BANK_NAME, BANK_TYPE FROM BANK WHERE BANK_CODE IN 
                                   ('BNK9999','BNK9998','BNK9989','CSH001','BNK9978')";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var bank = new EmployeeGetBankResponse
                                {
                                    BANK_CODE = reader["BANK_CODE"].ToString(),
                                    BANK_NAME = reader["BANK_NAME"].ToString(),
                                    BANK_TYPE = reader["BANK_TYPE"].ToString()
                                };

                                banks.Add(bank);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving banks", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting Bank: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return banks;
        }

        public async Task<List<EmployeeGetQualificationResponse>> ddl_GetQualification()
        {
            var qualifications = new List<EmployeeGetQualificationResponse>();

            try
            {

                var sqlQuery = @"SELECT QLF_CODE, DESCR AS QUALIFICATION, SH_DESCR, TYPE FROM QUALIFICATION ORDER BY QUALIFICATION";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var qualification = new EmployeeGetQualificationResponse
                                {
                                    QLF_CODE = reader["QLF_CODE"].ToString(),
                                    QUALIFICATION = reader["QUALIFICATION"].ToString(),
                                    SH_DESCR = reader["SH_DESCR"].ToString(),
                                    TYPE = reader["TYPE"].ToString()
                                };

                                qualifications.Add(qualification);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving qualifications", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting qualification: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return qualifications;
        }

         public async Task<List<EmployeeGetCurrencyResponse>> ddl_GetCurrency()
         {
             var currencies = new List<EmployeeGetCurrencyResponse>();

             try
             {

                 var sqlQuery = @"SELECT DESCR, CURR_CODE FROM CURRENCY ORDER BY 1";

                 using (var connection = _appDbContext.GetConn())
                 using (var command = new OracleCommand(sqlQuery, connection))
                 {

                     try
                     {
                         await connection.OpenAsync();

                         // Execute the query asynchronously
                         using (var reader = await command.ExecuteReaderAsync())
                         {
                             while (await reader.ReadAsync())
                             {
                                 var currency = new EmployeeGetCurrencyResponse
                                 {
                                     DESCR = reader["DESCR"].ToString(),
                                     CURR_CODE = reader["CURR_CODE"].ToString() 
                                 };

                                 currencies.Add(currency);
                             }
                         }
                     }
                     catch (Exception ex)
                     {
                         // Log or handle the exception appropriately
                         throw new Exception("Error retrieving currency", ex);
                     }
                 }
             }
             catch (Exception ex)
             {

                 string str = ex.GetType().ToString();
                 if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                 {
                     //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                     //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                     string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                     throw new RepositoryException(error);
                 }
                 string errorMessage = $"Error while getting currency: {ex.Message}";

                 if (ex.InnerException != null)
                 {
                     errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                 }

                 throw new Exception(errorMessage, ex.InnerException);
             }

             return currencies;
         }

        public async Task<Dictionary<string, string>> GetCurrencyDescriptionsAsync()
        {
            var currencyDescriptions = new Dictionary<string, string>();

            try
            {
                var sqlQuery = @"SELECT DESCR, CURR_CODE FROM CURRENCY ORDER BY 1";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {
                    await connection.OpenAsync();

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var currCode = reader["CURR_CODE"].ToString().Trim();
                            var descr = reader["DESCR"].ToString();

                            if (!currencyDescriptions.ContainsKey(currCode))
                            {
                                currencyDescriptions.Add(currCode, descr);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = $"Error while getting currency: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return currencyDescriptions;
        }



        public async Task<List<EmployeeGetSupplierResponse>> ddl_GetSupplier()
        {
            var suppliers = new List<EmployeeGetSupplierResponse>();

            try
            {

                var sqlQuery = @"SELECT SUPP_NAME, SUPP_CODE, SH_NAME FROM SUPPLIER WHERE SUPP_TYPE='H' AND NVL(TRIM(BLACK_LIST),'N')='N' ORDER BY SUPP_NAME";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var supplier = new EmployeeGetSupplierResponse
                                {
                                    SUPP_NAME = reader["SUPP_NAME"].ToString(),
                                    SUPP_CODE = reader["SUPP_CODE"].ToString(),
                                    SH_NAME = reader["SH_NAME"].ToString()
                                };

                                suppliers.Add(supplier);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving currency", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting currency: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return suppliers;
        }

        public async Task<List<EmployeeGetStateResponse>> ddl_GetStates()
        {
            var states = new List<EmployeeGetStateResponse>();

            try
            {

                var sqlQuery = @"SELECT STATE_CODE, DESCR, COUNT_CODE FROM STATE WHERE COUNT_CODE='IN' ORDER BY DESCR";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var state = new EmployeeGetStateResponse
                                {
                                    STATE_CODE = reader["STATE_CODE"].ToString(),
                                    DESCR = reader["DESCR"].ToString(),
                                    COUNT_CODE = reader["COUNT_CODE"].ToString()
                                };

                                states.Add(state);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving Department", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting Department: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return states;
        }

        public async Task<List<EmployeeGetCountryResponse>> ddl_GetCountry()
        {
            var countries = new List<EmployeeGetCountryResponse>();

            try
            {

                var sqlQuery = @"SELECT COUNT_CODE, DESCR FROM COUNTRY ORDER BY DESCR";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var country = new EmployeeGetCountryResponse
                                {
                                    COUNT_CODE = reader["COUNT_CODE"].ToString(),
                                    DESCR = reader["DESCR"].ToString()               
                                };

                                countries.Add(country);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving country", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting country: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return countries;
        }

        public async Task<List<EmployeeGetRoleResponse>> ddl_GetEmpRole()
        {
            var roles = new List<EmployeeGetRoleResponse>();

            try
            {

                var sqlQuery = @"SELECT TRAN_ID,MENU_DESCR,DEPT_CODE FROM EMP_ROLES ORDER BY MENU_DESCR";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var role = new EmployeeGetRoleResponse
                                {
                                    TRAN_ID = reader["TRAN_ID"].ToString(),
                                    MENU_DESCR = reader["MENU_DESCR"].ToString(),
                                    DEPT_CODE = reader["DEPT_CODE"].ToString()
                                };

                                roles.Add(role);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving roles", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting roles: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return roles;
        }



        public async Task<List<EmployeeGetLanguageResponse>> ddl_GetLanguages()
        {
            var languages = new List<EmployeeGetLanguageResponse>();

            try
            {

                var sqlQuery = @"SELECT DESCR AS FLD_VALUE, DESCR FROM GENCODES WHERE MOD_NAME = 'SLA__EMPLOYEE_LIST' AND FLD_NAME='LANGUAGE' AND ACTIVE='Y' ORDER BY TO_NUMBER(UDF_STR1)";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var language = new EmployeeGetLanguageResponse
                                {
                                    FLD_VALUE = reader["FLD_VALUE"].ToString(),
                                    DESCR = reader["DESCR"].ToString()
                                };

                                languages.Add(language);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving Languages", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting Languages: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return languages;
        }

        public async Task<List<EmployeeGetMaritalStatusResponse>> ddl_GetMaritalStatus()
        {
            var mstatus = new List<EmployeeGetMaritalStatusResponse>();

            try
            {

                var sqlQuery = @"SELECT TO_CHAR(FLD_VALUE) AS FLD_VALUE,DESCR FROM (
                                  SELECT '0' AS FLD_VALUE ,'-Select-'AS DESCR, '0' AS UDF_STR1 FROM DUAL
                                  UNION ALL
                                  SELECT TRIM(FLD_VALUE) AS FLD_VALUE, DESCR, UDF_STR1 FROM GENCODES WHERE MOD_NAME = 'SLA__EMPLOYEE_LIST' AND FLD_NAME='MARITAL_STATUS' AND ACTIVE='Y'
                                  ) T
                                  ORDER BY TO_NUMBER(UDF_STR1)";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var status = new EmployeeGetMaritalStatusResponse
                                {
                                    FLD_VALUE = reader["FLD_VALUE"].ToString(),
                                    DESCR = reader["DESCR"].ToString()
                                };

                                mstatus.Add(status);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving Marital Status", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting Marital Status: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return mstatus;
        }


        // Stroed Procedure

        // working to chack in data table
        public async Task<DataTable> SP_GetEmployeeDetailsTable(string empCode, string tableName)
        {
            try
            {
                using (OracleConnection con = _appDbContext.GetConn())
                {
                    await con.OpenAsync();

                    using (OracleCommand cmd = _appDbContext.GetCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "PR_GET_EMP_EISDET";
                        cmd.CommandType = CommandType.StoredProcedure;

                        // Input parameters
                        cmd.Parameters.Add("Cursor_PR_GET_EMP_EISDET ", OracleDbType.RefCursor).Direction = ParameterDirection.InputOutput;
                        cmd.Parameters.Add("AS_EMP_CODE", OracleDbType.Char).Value = empCode;
                        cmd.Parameters.Add("AS_TAB_NAME ", OracleDbType.Char).Value = tableName;

                        // Execute the command and retrieve the data
                        using (OracleDataAdapter adapter = new OracleDataAdapter(cmd))
                        {
                            DataTable dataTable = new DataTable();
                            adapter.Fill(dataTable);
                            return dataTable;
                        }
                    }
                }
            }
            catch (OracleException ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))

                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting employee: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting employee: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }



        // working for Employee Trancfer History Details
        public async Task<List<EmployeeSP_GetEmployeeTransferDetailResponse>> SP_GetEmployeeTransferDetails(string asEmpCode, string empcode)
        {

            try
            {
                using (OracleConnection con = _appDbContext.GetConn())
                {
                    await con.OpenAsync();

                    using (OracleCommand cmd = _appDbContext.GetCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "PR_EMPTRANSFERHISTINF";
                        cmd.CommandType = CommandType.StoredProcedure;

                        // Input parameters
                        cmd.Parameters.Add("Cursor_PR_EMPTRANSFERHISTINF", OracleDbType.RefCursor).Direction = ParameterDirection.InputOutput;
                        cmd.Parameters.Add("AS_CANDIDATE_CODE", OracleDbType.Char).Value = asEmpCode;
                        cmd.Parameters.Add("EMPCODE ", OracleDbType.Char).Value = empcode;

                        // Execute the command and retrieve the data
                        using (OracleDataReader reader = (OracleDataReader)await cmd.ExecuteReaderAsync())
                        {
                            List<EmployeeSP_GetEmployeeTransferDetailResponse> emps = new List<EmployeeSP_GetEmployeeTransferDetailResponse>();

                            while (reader.Read())
                            {

                                EmployeeSP_GetEmployeeTransferDetailResponse emp = new EmployeeSP_GetEmployeeTransferDetailResponse();

                                emp.P_EMPID = reader["P_EMPID"].ToString();
                                emp.P_SRNO = Convert.ToInt64(reader["P_SRNO"]);
                                emp.P_TRANSACTION_TYPE = reader["P_TRANSACTION_TYPE"].ToString();
                                emp.P_DIV_ID = reader["P_DIV_ID"].ToString();
                                emp.P_DEPT_ID = reader["P_DEPT_ID"].ToString();                               
                                emp.EMPNAME = reader["EMPNAME"].ToString();
                                emp.P_SECTION_ID = reader["P_SECTION_ID"].ToString();
                                emp.P_DATE_OF_TRANS_PROMO = reader["P_DATE_OF_TRANS_PROMO"] != DBNull.Value ? reader.GetDateTime("P_DATE_OF_TRANS_PROMO").Date : default(DateTime);
                                emp.P_LEVEL_ID = reader["P_LEVEL_ID"].ToString();
                                emp.P_GRADE_ID = reader["P_GRADE_ID"].ToString();
                                emp.P_SECTION_NAME = reader["P_SECTION_NAME"].ToString();
                                emp.P_DEPT_NAME = reader["P_DEPT_NAME"].ToString();
                                emp.P_DESIG = reader["P_DESIG"].ToString();
                                emp.P_CTC = Convert.ToInt64(reader["P_CTC"]);
                                emp.P_REASON = reader["P_REASON"].ToString();
                                emp.P_STRUCTURE_SRNO = Convert.ToInt64(reader["P_STRUCTURE_SRNO"]);
                                emp.P_GRADE_DESC = reader["P_GRADE_DESC"].ToString();
                                emp.P_LEVEL_NAME = reader["P_LEVEL_NAME"].ToString();
                                emp.P_DB = reader["P_DB"].ToString();
                                emp.DOC_PATH = reader["DOC_PATH"].ToString();
                                emp.VISI = reader["VISI"].ToString();

                                emps.Add(emp);
                            }
                            return emps;
                        }
                    }
                }
                // Return the list of employees
                return null;
            }
            catch (OracleException ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting Employee: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }



        // working for PR_GET_EMP_EISDET

        //-----------------------------------------------------------

        public async Task<object> SP_GetEmployeeDetails(string empCode, string tableName)
        {
            try
            {
                using (OracleConnection con = _appDbContext.GetConn())
                {
                    await con.OpenAsync();

                    using (OracleCommand cmd = _appDbContext.GetCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "PR_GET_EMP_EISDET";
                        cmd.CommandType = CommandType.StoredProcedure;

                        // Input parameters
                        cmd.Parameters.Add("Cursor_PR_GET_EMP_EISDET", OracleDbType.RefCursor).Direction = ParameterDirection.InputOutput;
                        cmd.Parameters.Add("AS_EMP_CODE", OracleDbType.Char).Value = empCode;
                        cmd.Parameters.Add("AS_TAB_NAME", OracleDbType.Char).Value = tableName;

                        // Execute the command and retrieve the data
                        using (OracleDataReader reader = (OracleDataReader)await cmd.ExecuteReaderAsync())
                        {
                            if (reader.HasRows)
                            {
                                // Check the tableName and map data accordingly
                                switch (tableName)
                                {

                                    case "EMPPER":
                                        List<EmployeeSP_GetEmployeePersonalDetailsResponse> emps = await MapEmployeePersonalDetailsAsync(reader);
                                        return emps;
                                    case "EMPFAM":
                                        List<EmployeeSP_GetEmployeeFamilyDetailResponse> familyMembers = MapFamilyMembers(reader);
                                        return familyMembers;
                                    case "EMPEDU":
                                        List<EmployeeSP_GetEmployeeEducationalDetailResponse> quals = MapEducationDetails(reader);
                                        return quals;
                                    case "EMPLANG":
                                        List<EmployeeSP_GetEmployeeLanguageDetailResponse> langs = MapLanguageDetails(reader);
                                        return langs;
                                    case "EMPEXP":
                                        List<EmployeeSP_GetEmployeeExperienceDetailResponse> exps = await MapExperienceDetailsAsync(reader);
                                        return exps;
                                    case "ASSETS":
                                        List<EmployeeSP_GetEmployeeAssetsDetailResponse> assets = MapAssetsDetails(reader);
                                        return assets;
                                    case "STATU":
                                        List<EmployeeSP_GetEmployeeStatutoryDetailResponse> statutory = MapStatutoryDetails(reader);
                                        return statutory;
                                    case "MEDICLAIM":
                                        List<EmployeeSP_GetEmployeeMediclaimDetailResponse> mediclaims = MapMediclaimsDetails(reader);
                                        return mediclaims;
                                    case "EMPOFF":
                                        List<EmployeeSP_GetEmployeeOfficialDetailResponse> officials = MapOfficialDetails(reader);
                                        return officials;
                                    case "CANDIDATE_DET":
                                        List<Candidate_DetailResponse> candidates = MapCandidateDetails(reader);
                                        return candidates;
                                    case "INTERVIEW_DET":
                                        List<Interview_DetailResponse> interviews = MapInterviewDetails(reader);
                                        return interviews;
                                    case "eMp0F2r":
                                        List<Offer_DetailResponse> offers = MapOfferDetails(reader);
                                        return offers;
                                    case "EMP_FAMDET":
                                        List< EmployeeSP_GetEmployeeFamilyStatutoryMediclaimDetailResponse> famdetails = MapFamilyStatutoryMediclaimDetails(reader);
                                        return famdetails;
                                    // Add cases for other tables as needed
                                    default:
                                        throw new NotImplementedException($"Mapping for table '{tableName}' is not implemented.");
                                }
                            }
                            else
                            {
                                //return null; // No data found
                                return Array.Empty<object>();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions
                throw new RepositoryException($"Error fetching data from stored procedure: {ex.Message}", ex);
            }
        }



        private async Task<List<EmployeeSP_GetEmployeePersonalDetailsResponse>> MapEmployeePersonalDetailsAsync(OracleDataReader reader)
        {
            List<EmployeeSP_GetEmployeePersonalDetailsResponse> emps = new List<EmployeeSP_GetEmployeePersonalDetailsResponse>();

            // Map data for EMPPER table
            while (reader.Read())
            {
                // Map data to EmployeeSP_GetEmployeePersonalDetailsResponse object
                EmployeeSP_GetEmployeePersonalDetailsResponse emp = new EmployeeSP_GetEmployeePersonalDetailsResponse();
 
                emp.EMP_CODE = reader["EMP_CODE"].ToString();
                emp.EMP_NAME = reader["EMP_NAME"].ToString();
                emp.EMP_FNAME = GetStringOrNull(reader, "EMP_FNAME");
                emp.EMP_MNAME = GetStringOrNull(reader, "EMP_MNAME");
                emp.EMP_LNAME = GetStringOrNull(reader, "EMP_LNAME");
                emp.SHORT_NAME = GetStringOrNull(reader, "SHORT_NAME");
                emp.BIRTH_DATE = (DateTime)GetDateOrNull(reader, "BIRTH_DATE");
                emp.DESIGNATION = GetStringOrNull(reader, "DESIGNATION");
                emp.GRADE = GetStringOrNull(reader, "GRADE");
                emp.GRADE_NAME = GetStringOrNull(reader, "GRADE_NAME");
                emp.SITE_NAME = GetStringOrNull(reader, "SITE_NAME");
                emp.DEPT_CODE = GetStringOrNull(reader, "DEPT_CODE");
                emp.DEPT_NAME = GetStringOrNull(reader, "DEPT_NAME");
                emp.DATE_JOIN = (DateTime)GetDateOrNull(reader, "DATE_JOIN");
                emp.RESI_DATE = GetDateOrNull(reader, "RESI_DATE");
                emp.RELIEVE_DATE = GetDateOrNull(reader, "RELIEVE_DATE");
                emp.QUAL = reader["QUAL"].ToString();
                emp.TOT_EXP = GetDecimalOrDefault(reader, "TOT_EXP");
                emp.MAC_EXP = GetDecimalOrDefault(reader, "MAC_EXP");
                emp.MOBILE_NO = GetStringOrNull(reader, "MOBILE_NO");
                emp.MOBILE_NO_OFF = reader["MOBILE_NO_OFF"].ToString();
                emp.EMPLOYMENT_TYPE = GetStringOrNull(reader, "EMPLOYMENT_TYPE");
                emp.GENDER = GetStringOrNull(reader, "GENDER");
                emp.GENDER_DESCR = GetStringOrNull(reader, "GENDER_DESCR");
                emp.EMP_SITE = GetStringOrNull(reader, "EMP_SITE");
                emp.EMP_TYPE = Convert.ToString(reader["EMP_TYPE"]);


                byte[] bytes = await _imageFetchingHelper.fnGetImage(emp.EMP_CODE, "EMPLOYEE");
                if (bytes != null)
                {
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    string ImageUrl = "data:image/png;base64," + base64String;
                    emp.EMP_IMAGE = ImageUrl;
                }


                emp.CONTACT_CODE = reader["CONTACT_CODE"].ToString();
                emp.NAME_PREFIX = GetStringOrNull(reader, "NAME_PREFIX");
                emp.M_STATUS_DATE = GetDateOrNull(reader, "M_STATUS_DATE");
                emp.M_STATUS = GetStringOrNull(reader, "M_STATUS");
                emp.M_STATUS_DESCR = GetStringOrNull(reader, "M_STATUS_DESCR");
                emp.CONTACT_CITY = GetStringOrNull(reader, "CONTACT_CITY");
                emp.CONTACT_STATE = GetStringOrNull(reader, "CONTACT_STATE");
                emp.CONTACT_PIN = GetStringOrNull(reader, "CONTACT_PIN");
                emp.RELIGION = reader["RELIGION"].ToString();
                emp.IT_NO = GetStringOrNull(reader, "IT_NO");
                emp.EMAIL_ID_OFF = GetStringOrNull(reader, "EMAIL_ID_OFF");
                emp.PER_ADD1 = GetStringOrNull(reader, "PER_ADD1");
                emp.PER_ADD2 = GetStringOrNull(reader, "PER_ADD2");
                emp.PER_ADD3 = reader["PER_ADD3"].ToString();
                emp.CUR_ADD1 = GetStringOrNull(reader, "CUR_ADD1");
                emp.CUR_ADD2 = GetStringOrNull(reader, "CUR_ADD2");
                emp.CUR_PIN = GetStringOrNull(reader, "CUR_PIN");
                emp.CUR_ADD3 = reader["CUR_ADD3"].ToString();
                emp.CUR_TEL1 = GetStringOrNull(reader, "CUR_TEL1");
                emp.CUR_CITY = GetStringOrNull(reader, "CUR_CITY");
                emp.CUR_TEL2 = reader["CUR_TEL2"].ToString();
                emp.CUR_TEL3 = reader["CUR_TEL3"].ToString();
                emp.EMAIL_ID_PER = GetStringOrNull(reader, "EMAIL_ID_PER");
                emp.CONTACT_TEL = GetStringOrNull(reader, "CONTACT_TEL");
                emp.CONTACT_PERS = GetStringOrNull(reader, "CONTACT_PERS");
                emp.BLOOD_GRP = GetStringOrNull(reader, "BLOOD_GRP");
                emp.SECTION = GetStringOrNull(reader, "SECTION");
                emp.PROBATION_PRD = GetStringOrNull(reader, "PROBATION_PRD");
                emp.TRAINING_PRD = GetStringOrNull(reader, "TRAINING_PRD");
                emp.PROBATION_DATE = reader["PROBATION_DATE"] != DBNull.Value ? reader.GetDateTime("PROBATION_DATE").Date : default(DateTime);
                emp.DATE_CONF = reader["DATE_CONF"] != DBNull.Value ? reader.GetDateTime("DATE_CONF").Date : default(DateTime);
                emp.PROBATION = GetStringOrNull(reader, "PROBATION");
                emp.CONFIRMED = GetStringOrNull(reader, "CONFIRMED");
                emp.BANK_ACCT = GetStringOrNull(reader, "BANK_ACCT");
                emp.BANK_CODE = GetStringOrNull(reader, "BANK_CODE");
                emp.BANK_NAME = GetStringOrNull(reader, "BANK_NAME");
                emp.BANK_IFSC = GetStringOrNull(reader, "BANK_IFSC");
                emp.CADRE = GetStringOrNull(reader, "CADRE");
                emp.CONTACT_ADD1 = GetStringOrNull(reader, "CONTACT_ADD1");
                emp.CONTACT_ADD2 = GetStringOrNull(reader, "CONTACT_ADD2");
                emp.CONTACT_ADD3 = reader["CONTACT_ADD3"].ToString();
                emp.PER_PIN = reader["PER_PIN"].ToString();
                emp.PER_TEL = GetStringOrNull(reader, "PER_TEL");
                emp.PAY_MODE = GetStringOrNull(reader, "PAY_MODE");
                emp.PAYMENT_MODE = GetStringOrNull(reader, "PAYMENT_MODE");
                emp.CUR_STATE = GetStringOrNull(reader, "CUR_STATE");
                emp.PER_STATE = GetStringOrNull(reader, "PER_STATE");
                emp.PER_CITY = GetStringOrNull(reader, "PER_CITY");
                emp.CREDIT_CARD_NO = reader["PER_PIN"].ToString();
                emp.PF_UN_NO = GetStringOrNull(reader, "PF_UN_NO");
                emp.AADHAAR_NO = GetStringOrNull(reader, "AADHAAR_NO");
                emp.DRIV_LIC_NO = reader["DRIV_LIC_NO"].ToString();
                emp.ELECTION_CARD_NO = reader["DRIV_LIC_NO"].ToString();
                emp.FATHER_NAME = GetStringOrNull(reader, "FATHER_NAME");
                emp.MOTHER_NAME = GetStringOrNull(reader, "MOTHER_NAME");
                emp.PASSPORT_NO = reader["PASSPORT_NO"].ToString();
                emp.PASSPORT_ISS_PLACE = reader["PASSPORT_ISS_PLACE"].ToString();
                emp.PASSPORT_ISS_DATE = GetDateOrNull(reader, "PASSPORT_ISS_DATE");
                emp.PASSPORT_EXP_DATE = GetDateOrNull(reader, "PASSPORT_EXP_DATE");
                emp.CUR_STATE_DESCR = GetStringOrNull(reader, "CUR_STATE_DESCR");
                emp.PER_STATE_DESCR = GetStringOrNull(reader, "PER_STATE_DESCR");
                emp.CONTACT_STATE_DESCR = GetStringOrNull(reader, "CONTACT_STATE_DESCR");
                emp.PROB_EXT = GetIntOrDefault(reader, "PROB_EXT");
                emp.CONF_EXT = GetIntOrDefault(reader, "CONF_EXT");
                emp.CURR_CTC = GetDecimalOrDefault(reader, "CURR_CTC");
                emp.JOINING_CTC = GetDecimalOrDefault(reader, "JOINING_CTC");
                //emp.CURR_CTC = Convert.ToDecimal(reader["CURR_CTC"]);
                //emp.JOINING_CTC = Convert.ToDecimal(reader["JOINING_CTC"]);

                emp.PAN_EMP_NAME = reader["PAN_EMP_NAME"].ToString();
                emp.AADHAR_EMP_NAME = reader["AADHAR_EMP_NAME"].ToString();
                emp.JOINED_AS = reader["JOINED_AS"].ToString();
                emp.REPORT_TO = reader["REPORT_TO"].ToString();
                emp.NOTICE_PRD = Convert.ToInt32(reader["NOTICE_PRD"]);
                emp.WORK_SITE = reader["WORK_SITE"].ToString();
                emp.GRATUITY_DATE = reader["GRATUITY_DATE"] != DBNull.Value ? reader.GetDateTime("GRATUITY_DATE").Date : default(DateTime);
                emp.BASIC = GetDecimalOrDefault(reader, "BASIC");
                emp.PAY_TYPE = Convert.ToChar(reader["PAY_TYPE"]);
                emp.DD_PAYABLE_BANK = reader["DD_PAYABLE_BANK"].ToString();
                emp.PF_NO = reader["PF_NO"].ToString();
                emp.FPF_NO = reader["FPF_NO"].ToString();
                emp.ESIC_NO = reader["ESIC_NO"].ToString();
                emp.PAY_SITE = reader["PAY_SITE"].ToString();
                emp.GROSS = reader["GROSS"].ToString();
                emp.WITH_HELD = reader["WITH_HELD"] != DBNull.Value ? Convert.ToChar(reader["WITH_HELD"]) : default(char);
                emp.STAN_CODE = reader["STAN_CODE"].ToString();
                emps.Add(emp);
            }
            return emps;
        }


        

        private List<EmployeeSP_GetEmployeeFamilyDetailResponse> MapFamilyMembers(OracleDataReader reader)
        {
            List<EmployeeSP_GetEmployeeFamilyDetailResponse> familyMembers = new List<EmployeeSP_GetEmployeeFamilyDetailResponse>();

            // Map data for EMPFAM table
            while (reader.Read())
            {
                // Map data to EmployeeSP_GetEmployeeFamilyDetailResponse object
                EmployeeSP_GetEmployeeFamilyDetailResponse familyMember = new EmployeeSP_GetEmployeeFamilyDetailResponse();
                familyMember.EMP_CODE = reader["EMP_CODE"].ToString();
                familyMember.SEQ_NO = Convert.ToInt32(reader["SEQ_NO"]);
                familyMember.MEMBER_NAME = reader["MEMBER_NAME"].ToString();
                familyMember.DATE_BIRTH = GetDateOrNull(reader, "DATE_BIRTH");
                familyMember.SEX = reader["SEX"].ToString();
                familyMember.RELATION = reader["RELATION"].ToString();
                familyMember.OCCUPATION = reader["OCCUPATION"].ToString();
                familyMember.CHG_USER = reader["CHG_USER"].ToString();
                familyMember.CHG_DATE = (DateTime)GetDateOrNull(reader, "CHG_DATE");
                familyMember.CHG_TERM = reader["CHG_TERM"].ToString();
                familyMember.QLF_CODE__FMLY = reader["QLF_CODE__FMLY"].ToString();
                familyMember.GENDER_DESC = reader["GENDER_DESC"].ToString();
                familyMember.DEPENDENT = reader["DEPENDENT"].ToString();
                familyMembers.Add(familyMember);
            }

            return familyMembers;
        }

        private List<EmployeeSP_GetEmployeeEducationalDetailResponse> MapEducationDetails(OracleDataReader reader)
        {
            List<EmployeeSP_GetEmployeeEducationalDetailResponse> quals = new List<EmployeeSP_GetEmployeeEducationalDetailResponse>();

            // Map data for EMPFAM table
            while (reader.Read())
            {

                EmployeeSP_GetEmployeeEducationalDetailResponse qual = new EmployeeSP_GetEmployeeEducationalDetailResponse();

                qual.EMP_CODE = reader["EMP_CODE"].ToString();
                qual.SEQ_NO = reader["SEQ_NO"].ToString();
                qual.QLF_CODE = reader["QLF_CODE"].ToString();
                qual.INSTITUTE = reader["INSTITUTE"].ToString();
                qual.PASS_YEAR = reader["PASS_YEAR"].ToString();
                qual.CLASS = reader["CLASS"].ToString();
                qual.PERC = reader["PERC"] != DBNull.Value ? Convert.ToDecimal(reader["PERC"]) : default(decimal);
                qual.UNIVERSITY = reader["UNIVERSITY"].ToString();
                qual.DEGREE_NAME = reader["DEGREE_NAME"].ToString();
                qual.REMARKS = reader["REMARKS"].ToString();
                //qual.STATUS_TYPE = Convert.ToChar(reader["STATUS_TYPE"]);
                qual.STATUS_TYPE = reader["STATUS_TYPE"] != DBNull.Value ? Convert.ToChar(reader["STATUS_TYPE"]) : default(char);
                qual.QUALIFICATION = reader["QUALIFICATION"].ToString();
                qual.EXP_COMP_DATE = GetDateOrNull(reader, "EXP_COMP_DATE");
                qual.FROM_DATE = GetDateOrNull(reader, "FROM_DATE");
                qual.TO_DATE = GetDateOrNull(reader, "TO_DATE");
                qual.SUBJECT1 = reader["SUBJECT1"].ToString();
                qual.SUBJECT2 = reader["SUBJECT2"].ToString();
                qual.SUBJECT3 = reader["SUBJECT3"].ToString();
                qual.SUBJECT4 = reader["SUBJECT4"].ToString();
                // Calculate QUAL_STATUS based on STATUS_TYPE
                char statusType = reader["STATUS_TYPE"] != DBNull.Value ? Convert.ToChar(reader["STATUS_TYPE"]) : default(char);
                string qualStatus = statusType == 'C' ? "COMPLETED" : (statusType == 'P' ? "PURSUING" : "");
                qual.QUAL_STATUS = qualStatus;

                quals.Add(qual);
            }
            return quals;
        }

        private List<EmployeeSP_GetEmployeeLanguageDetailResponse> MapLanguageDetails(OracleDataReader reader)
        {
            // Map data for EMPFAM table
            List<EmployeeSP_GetEmployeeLanguageDetailResponse> langs = new List<EmployeeSP_GetEmployeeLanguageDetailResponse>();

            while (reader.Read())
            {
                EmployeeSP_GetEmployeeLanguageDetailResponse lang = new EmployeeSP_GetEmployeeLanguageDetailResponse();

                lang.EMP_CODE = reader["EMP_CODE"].ToString();
                lang.LANGUAGE = reader["LANGUAGE"].ToString();
                lang.READ = reader["READ"] != DBNull.Value ? Convert.ToChar(reader["READ"]) : (char?)null;
                lang.WRITE = reader["WRITE"] != DBNull.Value ? Convert.ToChar(reader["WRITE"]) : (char?)null;
                lang.SPEAK = reader["SPEAK"] != DBNull.Value ? Convert.ToChar(reader["SPEAK"]) : (char?)null;
                lang.WORK_PROFICIENCY = reader["WORK_PROFICIENCY"] != DBNull.Value ? Convert.ToChar(reader["WORK_PROFICIENCY"]) : (char?)null;


                langs.Add(lang);
            }
            return langs;
        }


        /*private async Task<List<EmployeeSP_GetEmployeeExperienceDetailResponse>> MapExperienceDetailsAsync(OracleDataReader reader)
        {
            // Fetch currency descriptions
            var currencyDescriptions = await GetCurrencyDescriptionsAsync();

            // Map data for EMPFAM table //working latest
            List<EmployeeSP_GetEmployeeExperienceDetailResponse> exps = new List<EmployeeSP_GetEmployeeExperienceDetailResponse>();

            while (await reader.ReadAsync())
            {
                var currCode = reader["CURR_CODE"].ToString().Trim();

                EmployeeSP_GetEmployeeExperienceDetailResponse exp = new EmployeeSP_GetEmployeeExperienceDetailResponse
                {
                    EMP_CODE = reader["EMP_CODE"].ToString().Trim(),
                    ORGANISATION = reader["ORGANISATION"].ToString().Trim(),
                    LAST_DESIGNATION = reader["LAST_DESIGNATION"].ToString().Trim(),
                    FROM_DATE = (DateTime)GetDateOrNull(reader, "FROM_DATE"),
                    TO_DATE = GetDateOrNull(reader, "TO_DATE"),
                    GROSS_AMT = Convert.ToDecimal(reader["GROSS_AMT"]),
                    CURR_CODE = currCode,
                    CURR_DESCR = currencyDescriptions.TryGetValue(currCode, out var descr) ? descr : null, // Add currency description
                    CHG_USER = reader["CHG_USER"].ToString().Trim(),
                    CHG_DATE = (DateTime)GetDateOrNull(reader, "CHG_DATE"),
                    CHG_TERM = reader["CHG_TERM"].ToString().Trim(),
                    DEPARTMENT = reader["DEPARTMENT"].ToString().Trim(),
                    COUNTRY = reader["COUNTRY"].ToString().Trim(),
                    STATE = reader["STATE"].ToString().Trim(),
                    SEQ_NO = Convert.ToInt32(reader["SEQ_NO"]),
                    WORK_LOCATION = reader["WORK_LOCATION"].ToString().Trim(),
                    SEP_REASON = reader["SEP_REASON"].ToString().Trim()
                };

                exps.Add(exp);
            }

            return exps;
        }*/

        // research
        private async Task<List<EmployeeSP_GetEmployeeExperienceDetailResponse>> MapExperienceDetailsAsync(OracleDataReader reader)
        {
            // Fetch currency descriptions
            var currencyDescriptions = await GetCurrencyDescriptionsAsync();

            // Map data for EMPFAM table
            List<EmployeeSP_GetEmployeeExperienceDetailResponse> exps = new List<EmployeeSP_GetEmployeeExperienceDetailResponse>();

            while (await reader.ReadAsync())
            {
                var currCode = reader["CURR_CODE"].ToString().Trim();

                var exp = new EmployeeSP_GetEmployeeExperienceDetailResponse();

                exp.EMP_CODE = reader["EMP_CODE"].ToString().Trim(); // Step through each assignment
                exp.ORGANISATION = reader["ORGANISATION"].ToString().Trim();
                exp.LAST_DESIGNATION = reader["LAST_DESIGNATION"].ToString().Trim();
                exp.FROM_DATE = (DateTime)GetDateOrNull(reader, "FROM_DATE");
                exp.TO_DATE = GetDateOrNull(reader, "TO_DATE");
                exp.GROSS_AMT = Convert.ToDecimal(reader["GROSS_AMT"]);
                exp.CURR_CODE = currCode;
                exp.CURR_DESCR = currencyDescriptions.TryGetValue(currCode, out var descr) ? descr : null; // Add currency description
                exp.CHG_USER = reader["CHG_USER"].ToString().Trim();
                exp.CHG_DATE = GetDateOrNull(reader, "CHG_DATE");
                exp.CHG_TERM = reader["CHG_TERM"].ToString().Trim();
                exp.DEPARTMENT = reader["DEPARTMENT"].ToString().Trim();
                exp.COUNTRY = reader["COUNTRY"].ToString().Trim();
                exp.STATE = reader["STATE"].ToString().Trim();
                exp.SEQ_NO = Convert.ToInt32(reader["SEQ_NO"]);
                exp.WORK_LOCATION = reader["WORK_LOCATION"].ToString().Trim();
                exp.SEP_REASON = reader["SEP_REASON"].ToString().Trim();

                exps.Add(exp);
            }

            return exps;
        }


        private List<EmployeeSP_GetEmployeeAssetsDetailResponse> MapAssetsDetails(OracleDataReader reader)
        {
            // Map data for EMPFAM table
            List<EmployeeSP_GetEmployeeAssetsDetailResponse> assets = new List<EmployeeSP_GetEmployeeAssetsDetailResponse>();

            while (reader.Read())
            {
                EmployeeSP_GetEmployeeAssetsDetailResponse asset = new EmployeeSP_GetEmployeeAssetsDetailResponse();

                asset.INDUCT_CODE = reader["INDUCT_CODE"].ToString();
                asset.INDUCTION = reader["INDUCTION"].ToString();
                asset.DATE_FORMALITY = GetDateOrNull(reader, "DATE_FORMALITY");
                asset.REMARKS = reader["REMARKS"].ToString();
                //asset.STATUS = Convert.ToChar(reader["STATUS"]);
                asset.STATUS = reader["STATUS"].ToString();
                asset.STATUS_DATE = GetDateOrNull(reader, "STATUS_DATE");
                asset.TOT_AMT = Convert.ToDecimal(reader["TOT_AMT"]);
                asset.ADJ_AMT = Convert.ToDecimal(reader["ADJ_AMT"]);
                asset.TRAN_ID = reader["TRAN_ID"].ToString();

                assets.Add(asset);
            }
            return assets;

        }


        private List<EmployeeSP_GetEmployeeStatutoryDetailResponse> MapStatutoryDetails(OracleDataReader reader)
        {
            // Map data for EMPFAM table
            List<EmployeeSP_GetEmployeeStatutoryDetailResponse> statutory = new List<EmployeeSP_GetEmployeeStatutoryDetailResponse>();

            while (reader.Read())
            {
                EmployeeSP_GetEmployeeStatutoryDetailResponse statu = new EmployeeSP_GetEmployeeStatutoryDetailResponse();

                statu.SEQ_NO = reader["SEQ_NO"].ToString();
                statu.MEMBER_NAME = reader["MEMBER_NAME"].ToString();
                statu.STATUTORY_TYPE = reader["STATUTORY_TYPE"].ToString();
                statu.PERCENTAGE = GetDecimalOrDefault(reader, "PERCENTAGE");
                statu.RELATION = reader["RELATION"].ToString();
                statu.GENDER_DESC = reader["GENDER_DESC"].ToString();

                statutory.Add(statu);
            }
            return statutory;
        }

        private List<EmployeeSP_GetEmployeeMediclaimDetailResponse> MapMediclaimsDetails(OracleDataReader reader)
        {
            // Map data for EMPFAM table

            List<EmployeeSP_GetEmployeeMediclaimDetailResponse> mediclaims = new List<EmployeeSP_GetEmployeeMediclaimDetailResponse>();
            while (reader.Read())
            {
                EmployeeSP_GetEmployeeMediclaimDetailResponse mediclaim = new EmployeeSP_GetEmployeeMediclaimDetailResponse();

                mediclaim.MEMBER_NAME = reader["MEMBER_NAME"].ToString();
                mediclaim.GENDER = reader["GENDER"].ToString();
                mediclaim.RELATION = reader["RELATION"].ToString();
                mediclaim.DATE_BIRTH = (DateTime)GetDateOrNull(reader, "DATE_BIRTH");
                mediclaim.MEDICLAIM_STATUS = reader["MEDICLAIM_STATUS"].ToString();
                mediclaim.GENDER_DESC = reader["GENDER_DESC"].ToString();

                mediclaims.Add(mediclaim);
            }
            return mediclaims;
        }

        private List<EmployeeSP_GetEmployeeOfficialDetailResponse> MapOfficialDetails(OracleDataReader reader)
        {
            // Map data for EMPFAM table

            List<EmployeeSP_GetEmployeeOfficialDetailResponse> officials = new List<EmployeeSP_GetEmployeeOfficialDetailResponse>();
            while (reader.Read())
            {
                EmployeeSP_GetEmployeeOfficialDetailResponse official = new EmployeeSP_GetEmployeeOfficialDetailResponse();

                official.MODULE_NAME = reader["MODULE_NAME"].ToString();
                official.SAN_EMPNAME1 = reader["SAN_EMPNAME1"].ToString();
                official.SAN_EMPNAME2 = reader["SAN_EMPNAME2"].ToString();
                official.SAN_EMPNAME3 = reader["SAN_EMPNAME3"].ToString();
                official.SAN_EMPNAME4 = reader["SAN_EMPNAME4"].ToString();
                official.SAN_EMPNAME5 = reader["SAN_EMPNAME5"].ToString();

                officials.Add(official);
            }
            return officials;
        }

        private List<Candidate_DetailResponse> MapCandidateDetails (OracleDataReader reader)
        {
            List<Candidate_DetailResponse> candidates = new List<Candidate_DetailResponse> ();
            while (reader.Read())
            {
                Candidate_DetailResponse candidate = new Candidate_DetailResponse();

                candidate.CANDIDATE_CODE = reader["CANDIDATE_CODE"].ToString();
                candidate.CANDIDATE_NAME = reader["CANDIDATE_NAME"].ToString();
                candidate.DEPARTMENT = reader["DEPARTMENT"].ToString();
                candidate.DESIGNATION = reader["DESIGNATION"].ToString();
                candidate.SITE_DESCR = reader["SITE_DESCR"].ToString();
                candidate.OFFER_ID = reader["OFFER_ID"].ToString();
                candidate.CURR_CTC = Convert.ToDecimal(reader["CURR_CTC"]);
                candidate.OFFERED_CTC = Convert.ToDecimal(reader["OFFERED_CTC"]);
                candidate.AL_DATE = reader["AL_DATE"] != DBNull.Value ? reader.GetDateTime("AL_DATE").Date : default(DateTime);
                candidate.JOIN_DATE__EXP = reader["JOIN_DATE__EXP"] != DBNull.Value ? reader.GetDateTime("JOIN_DATE__EXP").Date : default(DateTime);
                candidate.SECTION_NAME = reader["SECTION_NAME"].ToString();
                candidate.SECTION_CODE = reader["SECTION_CODE"].ToString();
                candidate.TOT_EXP = Convert.ToDecimal(reader["TOT_EXP"]);
                candidate.RECEIVED_DATE = reader["RECEIVED_DATE"] != DBNull.Value ? reader.GetDateTime("RECEIVED_DATE").Date : default(DateTime);
                candidate.CURR_COMPANY = reader["CURR_COMPANY"].ToString();
                candidate.CURR_DEPARTMENT = reader["CURR_DEPARTMENT"].ToString();
                candidate.CURR_DESIGNATION = reader["CURR_DESIGNATION"].ToString();
                candidate.VAC_NO = reader["VAC_NO"].ToString();
                candidate.SITE_CODE_SUITABLE = reader["SITE_CODE_SUITABLE"].ToString();
                candidate.DEPT_CODE_SUITABLE = reader["DEPT_CODE_SUITABLE"].ToString();
                candidate.DESIGN_CODE_SUITABLE = reader["DESIGN_CODE_SUITABLE"].ToString();
                candidate.GRADE_CODE = reader["GRADE_CODE"].ToString();
                candidate.CONFIRMED = Convert.ToChar(reader["CONFIRMED"]);
                candidate.REMARKS = reader["REMARKS"].ToString();
                candidate.GRADE_DESCR = reader["GRADE_DESCR"].ToString();
                candidate.QLF_DESCR1 = reader["QLF_DESCR1"].ToString();
                candidate.QLF_TYPE1 = reader["QLF_TYPE1"].ToString();
                candidate.QLF_DESCR2 = reader["QLF_DESCR2"].ToString();
                candidate.QLF_TYPE2 = reader["QLF_TYPE2"].ToString();
                candidate.QLF_DESCR3 = reader["QLF_DESCR3"].ToString();
                candidate.QLF_TYPE3 = reader["QLF_TYPE3"].ToString();
                candidate.CADRE_DESCR = reader["CADRE_DESCR"].ToString();
                candidate.PANEL_TRAN_ID = reader["PANEL_TRAN_ID"].ToString();

                candidates.Add(candidate);

            }
            return candidates;
        }

        private List<Interview_DetailResponse> MapInterviewDetails(OracleDataReader reader)
        {
            List<Interview_DetailResponse> interviews = new List<Interview_DetailResponse>();
            while (reader.Read())
            {
                Interview_DetailResponse interview = new Interview_DetailResponse();

                interview.REF_NO = reader["REF_NO"].ToString();
                interview.START_DATE = (DateTime)GetDateOrNull(reader, "START_DATE");
                interview.STATUS = reader["STATUS"].ToString();
                interview.PANEL = reader["PANEL"].ToString();
                interview.COMMENTS_PANEL = reader["COMMENTS_PANEL"].ToString();
                interview.SUPP_CODE__CON = reader["SUPP_CODE__CON"].ToString();
                interview.SOURCE = reader["SOURCE"].ToString();
                interview.REMARKS = reader["REMARKS"].ToString();

                interviews.Add(interview);

            }
            return interviews;
        }

        private List<Offer_DetailResponse> MapOfferDetails(OracleDataReader reader)
        {
            List<Offer_DetailResponse> offers = new List<Offer_DetailResponse>();
            while(reader.Read())
            {
                Offer_DetailResponse  offer = new Offer_DetailResponse();

                offer.CAND_CODE = reader["CAND_CODE"].ToString();
                offer.EMP_CODE = reader["EMP_CODE"].ToString();
                offer.EXP_DOJ = (DateTime)GetDateOrNull(reader, "EXP_DOJ");
                offer.OFFERED_DESIGNATION = reader["OFFERED_DESIGNATION"].ToString();
                offer.OFFERED_CTC = Convert.ToDecimal(reader["OFFERED_CTC"]);
                offer.CURR_DESIGNATION = reader["CURR_DESIGNATION"].ToString();
                offer.CURR_CTC = Convert.ToDecimal(reader["CURR_CTC"]);
                offer.DEV_JOIN_DAY = Convert.ToInt32(reader["DEV_JOIN_DAY"]);
                offer.OFFER_ID = reader["OFFER_ID"].ToString();
                offer.VAC_NO = reader["VAC_NO"].ToString();
                offer.OFFER_DATE = reader["OFFER_DATE"] != DBNull.Value ? reader.GetDateTime("OFFER_DATE").Date : default(DateTime);
                //offer.OFFER_DATE = (DateTime)GetDateOrNull(reader, "OFFER_DATE");

                offers.Add(offer);
            }
            return offers;
        }

        private List<EmployeeSP_GetEmployeeFamilyStatutoryMediclaimDetailResponse> MapFamilyStatutoryMediclaimDetails(OracleDataReader reader)
        {
            List<EmployeeSP_GetEmployeeFamilyStatutoryMediclaimDetailResponse> famdetails = new List<EmployeeSP_GetEmployeeFamilyStatutoryMediclaimDetailResponse>();
            while(reader.Read())
            {
                EmployeeSP_GetEmployeeFamilyStatutoryMediclaimDetailResponse famdeatil = new EmployeeSP_GetEmployeeFamilyStatutoryMediclaimDetailResponse();

                famdeatil.EMP_CODE = reader["EMP_CODE"].ToString();
                famdeatil.SEQ_NO = reader["SEQ_NO"].ToString();
                famdeatil.MEMBER_NAME = reader["MEMBER_NAME"].ToString();
                famdeatil.DATE_BIRTH = GetDateOrNull(reader, "DATE_BIRTH");
                famdeatil.SEX = reader["SEX"].ToString();
                famdeatil.RELATION = reader["RELATION"].ToString();
                famdeatil.OCCUPATION = reader["OCCUPATION"].ToString();
                famdeatil.DEPENDENT = reader["DEPENDENT"].ToString();
                famdeatil.CHG_USER = reader["CHG_USER"].ToString();
                //famdeatil.CHG_DATE = (DateTime)GetDateOrNull(reader, "CHG_DATE");
                famdeatil.CHG_DATE = reader["CHG_DATE"] != DBNull.Value ? reader.GetDateTime("CHG_DATE").Date : default(DateTime);
                famdeatil.CHG_TERM = reader["CHG_TERM"].ToString();
                famdeatil.PF_NOMINEE = reader["PF_NOMINEE"].ToString();
                famdeatil.PF_PERC = GetDecimalOrDefault(reader, "PF_PERC");
                famdeatil.GRAT_NOMINEE = reader["GRAT_NOMINEE"].ToString();
                famdeatil.GRAT_PERC = GetDecimalOrDefault(reader, "GRAT_PERC");
                famdeatil.PEN_NOMINEE = reader["PEN_NOMINEE"].ToString();
                famdeatil.PEN_PERC = GetDecimalOrDefault(reader, "PEN_PERC");
                famdeatil.PHYSICAL_STATUS = reader["PHYSICAL_STATUS"].ToString();
                famdeatil.QLF_CODE = reader["QLF_CODE"].ToString();
                famdeatil.ADOPTED = reader["ADOPTED"].ToString();
                famdeatil.MEDI_NOMINEE = reader["MEDI_NOMINEE"].ToString();
                famdeatil.ACC_INC_NOMINEE = reader["ACC_INC_NOMINEE"].ToString();
                //famdeatil.ACC_INC_PERC = Convert.ToDecimal(reader["ACC_INC_PERC"]);
                famdeatil.ACC_INC_PERC = GetDecimalOrDefault(reader, "ACC_INC_PERC");
                famdeatil.DEATH_NOMINEE = reader["DEATH_NOMINEE"].ToString();
                //famdeatil.DEATH_PERC = Convert.ToDecimal(reader["DEATH_PERC"]);
                famdeatil.DEATH_PERC = GetDecimalOrDefault(reader, "DEATH_PERC");

                famdetails.Add(famdeatil);
            }
            return famdetails;
        }


        // seperation details

        public IEnumerable<EmployeeGetSeperationDetailResponse> GetEmployeesSeperationDetails(string empCode)
        {
            List<EmployeeGetSeperationDetailResponse> employees = new List<EmployeeGetSeperationDetailResponse>();

            using (OracleConnection connection = _appDbContext.GetConn())
            {
                connection.Open();

                string sqlQuery = @"
            SELECT 
                A.EMP_CODE,
                A.SHORT_NAME AS EMP_NAME,
                TO_CHAR(A.DATE_JOIN,'dd/MM/yyyy') AS DOJ,
                TO_CHAR(D.RESI_DATE,'dd/MM/yyyy') AS RESIGN_DATE,
                TO_CHAR(D.RELIEVE_DATE,'dd/MM/yyyy') AS RELIEVE_DATE,
                TO_CHAR(D.HR_REC_DATE,'dd/MM/yyyy') AS HR_RECD_DATE,
                C.DESCR AS SEP_MODE,
                B.DESCR AS STATUS,
                NVL(E.REASON,'') AS RESIGN_REASON,
                NVL(F.AMOUNT,0) AS NET_AMT,
                CASE 
        WHEN (SELECT COUNT(1) AS CNT FROM DOC_CONTENTS A INNER JOIN DOC_TRANSACTION_LINK B ON A.DOC_ID=B.DOC_ID WHERE A.STORAGE_CODE='FFSCHQACK ' AND B.REF_ID=A.EMP_CODE ) > 0 THEN 'FULL AND FINAL IS COMPLETED AND CHEQUE RECEIVED' 
        WHEN D.STATUS = 'U' THEN 'RESIGNATION NOT APPROVED YET' 
        WHEN D.STATUS = 'A' THEN 'RESIGNATION IS IN PROCESS WITH HR' 
        WHEN D.STATUS = 'S' AND E.STATUS='R' THEN 'SEPARATION IS IN PROCESS WITH HR' 
        WHEN E.STATUS = 'C' AND F.CONFIRMED ='N' AND (SELECT COUNT(1) AS CNT FROM FULLFINAL_DET WHERE TRAN_ID = F.TRAN_ID) = 0 THEN 'FULL N FINAL IS IN PROCESS WITH ACCOUNTS' 
        WHEN ((SELECT COUNT(1) AS CNT FROM FULLFINAL_DET WHERE TRAN_ID = F.TRAN_ID)>0 AND (F.CONFIRMED = 'N')) THEN 'FULL N FINAL IS IN PROCESS WITH ACCOUNTS' 
        WHEN F.CONFIRMED = 'F' THEN 'FULL N FINAL IS IN PROCESS WITH TAX' 
        WHEN F.CONFIRMED = 'I' THEN 'FULL N FINAL IS IN PROCESS WITH ACCOUNTS' 
        WHEN F.CONFIRMED = 'X' THEN 'FULL N FINAL IS IN CANCELLED' 
        WHEN ((SELECT COUNT(1) AS CNT2 FROM MISC_PAYMENT WHERE SUNDRY_CODE=D.EMP_CODE AND TRAN_TYPE='FFS')) = 0 AND F.CONFIRMED = 'Y' THEN 'FULL N FINAL IS COMPLETED AND CHEQUE NOT YET PREPARED' 
        WHEN ((SELECT COUNT(1) AS CNT2 FROM MISC_PAYMENT WHERE SUNDRY_CODE=D.EMP_CODE AND TRAN_TYPE='FFS')) > 0 AND F.CONFIRMED = 'Y' THEN 'FULL N FINAL IS COMPLETED AND CHEQUE PREPARED' 
        END AS REMARKS
            FROM 
                SEPARATION_CLOSE D 
            LEFT OUTER JOIN 
                SEPARATION E ON D.EMP_CODE=E.EMP_CODE 
            LEFT OUTER JOIN 
                FULLFINAL_HDR F ON E.EMP_CODE=F.EMP_CODE 
            INNER JOIN 
                EMPLOYEE A ON D.EMP_CODE=A.EMP_CODE 
            LEFT OUTER JOIN 
                GENCODES B ON D.STATUS=B.FLD_VALUE AND B.FLD_NAME='STATUS' AND B.MOD_NAME='SLA__SEPARATION_CLOSE' 
            LEFT OUTER JOIN 
                GENCODES C ON NVL(D.SEPARATION_MODE,'R')=C.FLD_VALUE AND C.FLD_NAME='SEP_MODE' AND C.MOD_NAME='SLA__SEPARATION_CLOSE' 
            WHERE 
                TRIM(D.EMP_CODE) = :empCode";

                using (OracleCommand command = new OracleCommand(sqlQuery, connection))
                {
                    command.Parameters.Add(new OracleParameter(":empCode", empCode));

                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            EmployeeGetSeperationDetailResponse employee = new EmployeeGetSeperationDetailResponse();
                            employee.EMP_CODE = reader.GetString(0);
                            employee.EMP_NAME = reader.GetString(1);
                            employee.DOJ = (DateTime)GetDateOrNull(reader, "DOJ");
                            employee.RESIGN_DATE = (DateTime)GetDateOrNull(reader, "RESIGN_DATE");
                            employee.RELIEVE_DATE = GetDateOrNull(reader, "RELIEVE_DATE");
                            employee.HR_RECD_DATE = GetDateOrNull(reader, "HR_RECD_DATE");
                            employee.SEP_MODE = reader.GetString(6);
                            employee.STATUS = reader.GetString(7);
                            employee.RESIGN_REASON = reader["RESIGN_REASON"].ToString();
                            employee.NET_AMT = reader.GetDecimal(9);
                            employee.REMARKS = reader["REMARKS"].ToString();

                            employees.Add(employee);
                        }
                    }
                }
            }
            return employees;
        }



        // Attachment 

        private DataTable fnGetAttachedDocs(string strUniqueID, string strStorageCode)
        {
            try
            {
                DataTable dtDocs = new DataTable();
                using (OracleConnection connection = _appDbContext.GetConn())
                {
                    connection.Open();

                    string Qry = "SELECT STORAGE_CODE, DESCR AS DOCUMENTS, STORAGE_CODE__PAR, STORAGE_SECURITY, STORAGE_PATH, '' DOC_ID, 'File Not Attached' DOCUMENT_LINK, '' DOC_PATH, '' DOC_TYPE " +
                                 "FROM DOC_STORAGE WHERE STORAGE_CODE='" + strStorageCode + "' " +
                                 "AND STORAGE_CODE__PAR NOT IN " +
                                 "(SELECT C.STORAGE_CODE__PAR FROM DOC_CONTENTS A " +
                                 "INNER JOIN DOC_TRANSACTION_LINK B ON A.DOC_ID=B.DOC_ID " +
                                 "INNER JOIN DOC_STORAGE C ON A.STORAGE_CODE=C.STORAGE_CODE__PAR " +
                                 "WHERE REF_ID IN ('" + strUniqueID + "') AND C.STORAGE_CODE='" + strStorageCode + "' AND NVL(B.CONFIRMED,'Y')='Y') " +
                                 "UNION ALL " +
                                 "SELECT C.STORAGE_CODE, C.DESCR, C.STORAGE_CODE__PAR, C.STORAGE_SECURITY, C.STORAGE_PATH, " +
                                 "A.DOC_ID, NVL(A.DOC_NAME,'File Not Attached') AS DOCUMENT_LINK, " +
                                 "SUBSTR(REPLACE(C.STORAGE_PATH, '\', '/') || '/' || A.DOC_NAME, 4) AS DOC_PATH, " +
                                 "A.DOC_TYPE FROM DOC_CONTENTS A " + "INNER JOIN DOC_TRANSACTION_LINK B ON A.DOC_ID=B.DOC_ID " +
                                 "INNER JOIN DOC_STORAGE C ON A.STORAGE_CODE=C.STORAGE_CODE__PAR " +
                                 "WHERE REF_ID IN ('" + strUniqueID + "') AND C.STORAGE_CODE='" + strStorageCode + "' AND NVL(B.CONFIRMED,'Y')='Y' ";


                    using (OracleCommand command = new OracleCommand(Qry, connection))
                    {
                        command.Parameters.Add(new OracleParameter(":strUniqueID", OracleDbType.Varchar2) { Value = strUniqueID });
                        command.Parameters.Add(new OracleParameter(":strStorageCode", OracleDbType.Varchar2) { Value = strStorageCode });

                        using (OracleDataAdapter adapter = new OracleDataAdapter(command))
                        {
                            adapter.Fill(dtDocs);
                        }
                    }

                }
                return dtDocs;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message); // Log the error message
                throw; // Rethrow the exception
            }

        }


        public string GetAttachmentMode()
        {
            // This could involve querying a database, checking a configuration, etc.
            return "OFFERAPPR"; // Defaulting to "EMPL" for demonstration purposes
        }


        /* public List<Dictionary<string, object>> GetAttachments(string strEmpCode, string strCndCode, string attachMode)
         {
             DataTable dtAttachments = null;
             string strUniqueID = ""; // Initialize strUniqueID
             string strStorageCode = GetAttachmentMode(); // Get the default storage code

             if (attachMode == "CAND" || attachMode == "OFFERAPPR")
             {
                 // Assuming fnExecuteScalarCompleted_CAND_CODE is related to "CAND" mode
                 string candCode = GetCandCode(strEmpCode);
                 if (!string.IsNullOrEmpty(candCode))
                     fnExecuteScalarCompleted_CAND_CODE(candCode);
             }
             else if (attachMode == "EMPL" || attachMode == "RESI" || attachMode == "EIS")
             {
                 // Set strUniqueID based on the mode
                 if (attachMode == "EMPL")
                     strUniqueID = strEmpCode + "','" + strCndCode;
                 else
                     strUniqueID = strEmpCode; // Assuming strEmpCode is unique for RESI and EIS modes

                 // Use fnGetAttachedDocs directly based on the attachment mode
                 dtAttachments = fnGetAttachedDocs(strUniqueID, strStorageCode);
             }

             if (dtAttachments != null)
             {
                 dtAttachments.Columns.Add("HasDocID", typeof(bool));
                 foreach (DataRow row in dtAttachments.Rows)
                 {
                     row["HasDocID"] = !string.IsNullOrEmpty(row["DOC_ID"].ToString());
                 }
             }

             return ConvertDataTableToList(dtAttachments);
         }
 */

        /*public List<Dictionary<string, object>> GetAttachments(string strEmpCode, string attachMode, string strCndCode= null)
        {
            DataTable dtAttachments = null;
            string strUniqueID = ""; // Initialize strUniqueID
            string strStorageCode = GetAttachmentMode(); // Get the default storage code

            if (attachMode == "CAND")
            {
                // Assuming fnExecuteScalarCompleted_CAND_CODE is related to "CAND" mode
                string candCode = GetCandCode(strEmpCode);
                if (!string.IsNullOrEmpty(candCode))
                {
                    dtAttachments = fnGetAttachedDocs(candCode, "CAND");
                }
                 
            }
            else if (attachMode == "OFFERAPPR")
            {
                string candCode = GetCandCode(strEmpCode);
                string candidateCode = GetCandidateCode(candCode);
                if (!string.IsNullOrEmpty(candidateCode))
                {
                    dtAttachments = fnGetAttachedDocs(candidateCode, "OFFERAPPR");
                }
            }
            else if (attachMode == "EMPL" || attachMode == "RESI" || attachMode == "EIS")
            {
                // Set strUniqueID based on the mode
                if (attachMode == "EMPL")
                    strUniqueID = strEmpCode + "','" + strCndCode;
                else
                    strUniqueID = strEmpCode; // Assuming strEmpCode is unique for RESI and EIS modes

                // Use fnGetAttachedDocs directly based on the attachment mode
                dtAttachments = fnGetAttachedDocs(strUniqueID, strStorageCode);
            }

            if (dtAttachments != null)
            {
                dtAttachments.Columns.Add("HasDocID", typeof(bool));
                foreach (DataRow row in dtAttachments.Rows)
                {
                    row["HasDocID"] = !string.IsNullOrEmpty(row["DOC_ID"].ToString());
                }
            }

            return ConvertDataTableToList(dtAttachments);
        }
*/

        public List<Dictionary<string, object>> GetAttachments(string strEmpCode, string strCndCode, string attachMode)
        {
            DataTable dtAttachments = null;
            string strUniqueID = ""; // Initialize strUniqueID
            string strStorageCode = attachMode; // Get the default storage code

            if (attachMode == "CAND")
            {
                // Assuming fnExecuteScalarCompleted_CAND_CODE is related to "CAND" mode
                string candCode = GetCandCode(strEmpCode);
                if (!string.IsNullOrEmpty(candCode))
                {
                    dtAttachments = fnGetAttachedDocs(candCode, "CAND");
                }

            }
            else if (attachMode == "OFFERAPPR")
            {
                string candCode = GetCandCode(strEmpCode);
                string candidateCode = GetCandidateCode(candCode);
                if (!string.IsNullOrEmpty(candidateCode))
                {
                    dtAttachments = fnGetAttachedDocs(candidateCode, "OFFERAPPR");
                }
            }
            else if (attachMode == "EMPL" || attachMode == "RESI" || attachMode == "EIS")
            {
                // Set strUniqueID based on the mode
                if (attachMode == "EMPL")
                    strUniqueID = strEmpCode + "','" + strCndCode;
                else
                    strUniqueID = strEmpCode; // Assuming strEmpCode is unique for RESI and EIS modes

                // Use fnGetAttachedDocs directly based on the attachment mode
                dtAttachments = fnGetAttachedDocs(strUniqueID, strStorageCode);
            }

            if (dtAttachments != null)
            {
                dtAttachments.Columns.Add("HasDocID", typeof(bool));
                foreach (DataRow row in dtAttachments.Rows)
                {
                    row["HasDocID"] = !string.IsNullOrEmpty(row["DOC_ID"].ToString());
                }
            }

            return ConvertDataTableToList(dtAttachments);
        }

        public string GetCandidateCode(string CandCode)
        {
            string candidateCode = null;

            // Database connection
            using (OracleConnection connection = _appDbContext.GetConn())
            {
                connection.Open();

                /*string sqlQuery = @"
                SELECT 
                    CANDIDATE_CODE 
                FROM 
                    CANDIDATE_INT_DET 
                WHERE 
                    OFFER_ID IN (:CandCode)";*/

                //string sqlQuery = "SELECT CANDIDATE_CODE FROM CANDIDATE_INT_DET WHERE OFFER_ID IN(SELECT OFFER_ID FROM RECRSL WHERE CAND_CODE = ('" + CandCode + "')";
                string sqlQuery = "SELECT CANDIDATE_CODE FROM CANDIDATE_INT_DET WHERE OFFER_ID IN (SELECT OFFER_ID FROM RECRSL WHERE CAND_CODE = :CandCode)";


                using (OracleCommand command = new OracleCommand(sqlQuery, connection))
                {
                    // Add parameter for CandCode
                    //command.Parameters.Add(":CandCode", OracleDbType.Varchar2).Value = candCode;
                    command.Parameters.Add(new OracleParameter(":CandCode", OracleDbType.Varchar2) { Value = CandCode });

                    // Execute the query
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        // Check if any rows returned
                        if (reader.Read())
                        {
                            // Retrieve candidate code from the first row
                            candidateCode = reader.GetString(0);
                        }
                        // Optionally handle case when no rows are returned

                    }
                }
            }

            return candidateCode;
        }

        public string GetCandCode(string strEmpCode)
        {
            string candidateCode = null;

            // Database connection
            using (OracleConnection connection = _appDbContext.GetConn())
            {
                connection.Open();

                // Use parameterized query to avoid SQL injection
                string sqlQuery = "select cand_code from recrsl where emp_code='" + strEmpCode + "'";

                using (OracleCommand command = new OracleCommand(sqlQuery, connection))
                {
                    // Add parameter for EMP_CODE
                    command.Parameters.Add(":strEmpCode", OracleDbType.Varchar2).Value = strEmpCode;

                    // Execute the query
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        // Check if any rows returned
                        if (reader.Read())
                        {
                            // Retrieve candidate code from the first row
                            candidateCode = reader.GetString(0);
                        }
                    }
                }
            }

            return candidateCode;
        }


        // convert to list  ----------------------------------------
        private List<Dictionary<string, object>> ConvertDataTableToList(DataTable dt)
        {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();

            if (dt == null)
            {
                return result; // Return an empty list if dt is null
            }

            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> dict = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    dict[col.ColumnName] = row[col];
                }
                result.Add(dict);
            }

            return result;
        }






        // Update Methods
        public async Task<IActionResult> UpdateEmployeeData(string id, EmployeeSP_GetEmployeePersonalDetailsResponse employeeUpdateModel)
        {
            try
            {
                // Call the stored procedure to fetch the employee details
                var employeeData = await SP_GetEmployeeDetails(id, "EMPPER");

                if (employeeData == null)
                {
                    return NotFound("Employee not found");
                }

                // Cast the result to a list of the appropriate type
                var employees = employeeData as List<EmployeeSP_GetEmployeePersonalDetailsResponse>;

                if (employees == null)
                {
                    // Handle the case where the retrieved data is not of the expected type
                    return StatusCode(500, "Error processing employee data");
                }

                // Find the employee to update
                var employeeToUpdate = employees.FirstOrDefault(e => e.EMP_CODE == id);

                if (employeeToUpdate == null)
                {
                    return NotFound("Employee not found");
                }

                // Update the employee data
                employeeToUpdate.EMP_CODE = employeeUpdateModel.EMP_CODE;
                employeeToUpdate.EMP_NAME = employeeUpdateModel.EMP_NAME;
                employeeToUpdate.EMP_FNAME = employeeUpdateModel.EMP_FNAME;
                employeeToUpdate.EMP_MNAME = employeeUpdateModel.EMP_MNAME;
                employeeToUpdate.EMP_LNAME = employeeUpdateModel.EMP_LNAME;
                employeeToUpdate.GRADE = employeeUpdateModel.GRADE;

                // Save the updated employee data
                _appDbContext.Update(employeeToUpdate);
                await _appDbContext.SaveChangesAsync();

                return Ok(employeeToUpdate); // Return the updated employee data
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException) || ex.GetType() == typeof(DbUpdateConcurrencyException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error updating employee: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }


        /* public async Task<IActionResult> UpdateEmployeeData(string id, EmployeeSP_GetEmployeePersonalDetailsResponse employeeUpdateModel)
         {
             try
             {
                 // Call the stored procedure to fetch the employee details
                 var employeeData = await SP_GetEmployeeDetails(id, "EMPPER");

                 if (employeeData == null)
                 {
                     return NotFound("Employee not found");
                 }

                 // Cast the result to the appropriate type
                 var employee = employeeData as EmployeeSP_GetEmployeePersonalDetailsResponse;

                 if (employee == null)
                 {
                     // Handle the case where the retrieved data is not of the expected type
                     return StatusCode(500, "Error processing employee data");
                 }

                 // Update the employee data
                 employee.EMP_CODE = employeeUpdateModel.EMP_CODE;
                 employee.EMP_NAME = employeeUpdateModel.EMP_NAME;
                 employee.EMP_FNAME = employeeUpdateModel.EMP_FNAME;
                 employee.EMP_MNAME = employeeUpdateModel.EMP_MNAME;
                 employee.EMP_LNAME = employeeUpdateModel.EMP_LNAME;
                 employee.GRADE = employeeUpdateModel.GRADE;

                 // Save the updated employee data
                 _appDbContext.Update(employee);
                 await _appDbContext.SaveChangesAsync();

                 return Ok(employee); // Return the updated employee data
             }
             catch (Exception ex)
             {
                 string str = ex.GetType().ToString();
                 if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException) || ex.GetType() == typeof(DbUpdateConcurrencyException))
                 {
                     string error = ex.InnerException?.Message ?? ex.Message;
                     throw new RepositoryException(error);
                 }
                 string errorMessage = $"Error updating employee: {ex.Message}";
                 if (ex.InnerException != null)
                 {
                     errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                 }

                 throw new Exception(errorMessage, ex.InnerException);
             }
         }*/

        private IActionResult StatusCode(int v1, string v2)
        {
            throw new NotImplementedException();
        }

        private IActionResult Ok(EmployeeSP_GetEmployeePersonalDetailsResponse result)
        {
            throw new NotImplementedException();
        }

        private IActionResult Ok()
        {
            throw new NotImplementedException();
        }

        private IActionResult NotFound(string v)
        {
            throw new NotImplementedException();
        }



        /* public async Task<IActionResult> UpdateEmployeeData(string id, EmployeeSP_GetEmployeePersonalDetailsResponse employeeUpdateModel)
         {
             try
             {
                 // Create a new instance of EmployeeSP_GetEmployeePersonalDetailsResponse
                 var employee = new EmployeeSP_GetEmployeePersonalDetailsResponse
                 {
                     EMP_CODE = id,
                     EMP_NAME = employeeUpdateModel.EMP_NAME,
                     EMP_FNAME = employeeUpdateModel.EMP_FNAME,
                     EMP_MNAME = employeeUpdateModel.EMP_MNAME,
                     EMP_LNAME = employeeUpdateModel.EMP_LNAME,
                     GRADE = employeeUpdateModel.GRADE
                     // Add other properties as needed
                 };

                 // Update the employee data
                 _appDbContext.Update(employee);
                 await _appDbContext.SaveChangesAsync();

                 return Ok(employee); // Return the updated employee data
             }
             catch (Exception ex)
             {
                 string str = ex.GetType().ToString();
                 if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException) || ex.GetType() == typeof(DbUpdateConcurrencyException))
                 {
                     string error = ex.InnerException?.Message ?? ex.Message;
                     throw new RepositoryException(error);
                 }
                 string errorMessage = $"Error updating employee: {ex.Message}";
                 if (ex.InnerException != null)
                 {
                     errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                 }

                 throw new Exception(errorMessage, ex.InnerException);
             }
         }
 */

        // Insert Methods

        /* public async Task<IActionResult> InsertEmployeeChange(Employee employee)
         {
             try
             {
                 // Call your stored procedure to get the existing employee details
                 var existingEmployeeDetails = await SP_GetEmployeeDetails(employee.EMP_CODE, "EMPPER");

                 

                 if (existingEmployeeDetails == null)
                 {
                     return NotFound("Employee not found");
                 }

                 // Cast the result to a list of the appropriate type
                 var employees = existingEmployeeDetails as List<Employee>;

                 if (employees == null)
                 {
                     // Handle the case where the retrieved data is not of the expected type
                     return StatusCode(500, "Error processing employee data");
                 }


                 // Retrieve the existing employee details
                 var existingEmployee = existingEmployeeDetails.First(); // Assuming you're interested in the first record

                 // Compare each property of Employee and existingEmployee to detect changes
                 if (existingEmployee.CONTACT_CODE != employee.CONTACT_CODE ||
                     existingEmployee.EMP_FNAME != employee.EMP_FNAME ||
                     existingEmployee.EMP_MNAME != employee.EMP_MNAME ||
                     // Compare all other properties similarly
                     // ...

                     existingEmployee.SAP_SALES_HIER != employee.SAP_SALES_HIER)
                 {
                     // If any property is different, create a new Employee_Det_Change instance and insert it
                     var changeEntry = new Employee_Det_Change
                     {
                         // Map properties from Employee to Employee_Det_Change as needed
                         // Example:
                         EMP_CODE = employee.EMP_CODE,
                         EMP_FNAME = employee.EMP_FNAME,
                         // Map all other properties similarly
                         // ...
                         CHG_DATE = DateTime.Now // Set the change date to current date
                     };

                     _appDbContext.EMPLOYEE_DET_CHANGE.Add(changeEntry);
                     await _appDbContext.SaveChangesAsync();
                 }

                 return Ok(); // Successfully inserted change
             }
             catch (Exception ex)
             {
                 // Handle exception appropriately (log, etc.)
                 return NotFound("Employee not found");
             }
         }*/

        // Working

        public async Task<IActionResult> InsertEmployeeChanges(EmployeeSP_GetEmployeePersonalDetailsResponse employee)
        {
            try
            {
                // Call your stored procedure to get the existing employee details
                var existingEmployee = await SP_GetEmployeeDetails(employee.EMP_CODE, "EMPPER");

                if (existingEmployee == null)
                {
                    return NotFound("Employee not found");
                }

                // Cast the result to a list of the appropriate type
                var employees = existingEmployee as List<EmployeeSP_GetEmployeePersonalDetailsResponse>;

                if (employees == null)
                {
                    // Handle the case where the retrieved data is not of the expected type
                    return StatusCode(500, "Error processing employee data");
                }

                // Find the employee to update
                var employeeToUpdate = employees.FirstOrDefault(e => e.EMP_CODE == employee.EMP_CODE);

                if (employeeToUpdate == null)
                {
                    return NotFound("Employee not found");
                }

                // Compare each property of Employee and existingEmployee to detect changes
                if (
                    employeeToUpdate.CONTACT_CODE != employee.CONTACT_CODE ||
                    employeeToUpdate.EMP_FNAME != employee.EMP_FNAME ||
                    employeeToUpdate.EMP_MNAME != employee.EMP_MNAME

                    )

                // Compare all other properties similarly
                //employeeToUpdate.SAP_SALES_HIER != employee.SAP_SALES_HIER)
                {
                    // If any property is different, create a new Employee_Det_Change instance and insert it
                    var changeEntry = new Employee_Det_Change
                    {
                        // Map properties from Employee to Employee_Det_Change as needed
                        // Example:
                        TRAN_ID = "0000009999",
                        TRAN_DATE = DateTime.Now,
                        EMP_CODE = employee.EMP_CODE,
                        EMP_FNAME_O = employee.EMP_FNAME,
                        EMP_FNAME = employee.EMP_FNAME,
                        EMP_MNAME_O = employee.EMP_MNAME,
                        EMP_MNAME = employee.EMP_MNAME,
                        EMP_LNAME_O = employee.EMP_LNAME,
                        EMP_LNAME = employee.EMP_LNAME,
                        SHORT_NAME_O = employee.SHORT_NAME,
                        NAME_PREFIX_O = employee.NAME_PREFIX,
                        //EMP_TYPE_O = employee.EMP_TYPE,
                        //REPORT_TO_O = employee.
                        SEX_O = employee.GENDER,
                        //NATIONALITY_O = employee.NATIO
                        //CITIZEN_O = employee.
                        //STATE_CODE__DOMICILE_O = employee.
                        BIRTH_DATE_O = employee.BIRTH_DATE,
                        BLOOD_GRP_O = employee.BLOOD_GRP,
                        CONTACT_PERS_O = employee.CONTACT_PERS,
                        CONTACT_ADD1_O = employee.CONTACT_ADD1,
                        CONTACT_ADD2_O = employee.CONTACT_ADD2,
                        CONTACT_ADD3_O = employee.CONTACT_ADD3,
                        CONTACT_CITY_O = employee.CONTACT_CITY,
                        CONTACT_STATE_O = employee.CONTACT_STATE,
                        CONTACT_PIN_O = employee.CONTACT_PIN,
                        CONTACT_TEL_O = employee.CONTACT_TEL,
                        RELIGION_O = employee.RELIGION,
                        //JOINED_AS_O = employee
                        DATE_JOIN_O = employee.DATE_JOIN,
                        //MODE_APP_O = employee.
                        //WORK_SHIFT_O = employee.
                        //SHIFT_ROT_O = employee.
                        //HOL_TBLNO_O = employee.
                        //PAY_TYPE_O = employee.
                        //PAY_MODE_O = employee.PAY_MODE,
                        BANK_CODE_O = employee.BANK_CODE,
                        BANK_ACCT_O = employee.BANK_ACCT,
                        //STAN_CODE_O = employee.
                        //DD_PAYABLE_BANK_O = employee.
                        TOT_EXP_O = employee.TOT_EXP,
                        //PF_NO_O = employee.
                        //FPF_NO_O = employee.
                        //ESIC_NO_O = employee.
                        IT_NO_O = employee.IT_NO,
                        //GROSS_O = employee.
                        //HOBBY1_O = employee.
                        //HOBBY2_O
                        //HOBBY3_O
                        //HOBBY4_O
                        //OTHER_INFO_O = employee.
                        //TRAINING_PRD_O = employee.TRAINING_PRD,



                        // Map all other properties similarly
                        // ...
                        CHG_DATE = DateTime.Now // Set the change date to current date
                    };

                    _appDbContext.EMPLOYEE_DET_CHANGE.Add(changeEntry);
                    await _appDbContext.SaveChangesAsync();
                }

                return Ok(); // Successfully inserted change
            }
            catch (Exception ex)
            {
                // Handle exception appropriately (log, etc.)
                return NotFound("Employee not found");
            }
        }


        public async Task<Employee> InsertEmployeeChange(Employee_Det_Change employee)
        {
            try
            {
                // Call your stored procedure to get the existing employee details
                var existingEmployee = await SP_GetEmployeeDetails(employee.EMP_CODE, "EMPPER");

                if (existingEmployee == null)
                {
                    return (Employee)NotFound("Employee not found");
                }

                // Cast the result to a list of the appropriate type
                var employees = existingEmployee as List<EmployeeSP_GetEmployeePersonalDetailsResponse>;

                if (employees == null)
                {
                    // Handle the case where the retrieved data is not of the expected type
                    return (Employee)StatusCode(500, "Error processing employee data");
                }

                // Find the employee to update
                var employeeToUpdate = employees.FirstOrDefault(e => e.EMP_CODE == employee.EMP_CODE);

                if (employeeToUpdate == null)
                {
                    return (Employee)NotFound("Employee not found");
                }

                // Compare each property of Employee and existingEmployee to detect changes
                if (
                    //employeeToUpdate.CONTACT_CODE != employee.CONTACT_CODE ||
                    //employeeToUpdate.EMP_NAME != 
                    employeeToUpdate.EMP_FNAME != employee.EMP_FNAME ||
                    employeeToUpdate.EMP_MNAME != employee.EMP_MNAME ||
                    employeeToUpdate.EMP_LNAME != employee.EMP_LNAME ||
                    employeeToUpdate.BIRTH_DATE != employee.BIRTH_DATE ||
                    employeeToUpdate.SHORT_NAME != employee.SHORT_NAME 
                    //employeeToUpdate.DESIGNATION != employee. ||
                    //employeeToUpdate.GRADE != employee.GRADE 
                    //employeeToUpdate.GRADE_NAME != employee


                    )

                // Compare all other properties similarly
                //employeeToUpdate.SAP_SALES_HIER != employee.SAP_SALES_HIER)
                {
                    // If any property is different, create a new Employee_Det_Change instance and insert it
                    var changeEntry = new Employee_Det_Change
                    {
                        // Map properties from Employee to Employee_Det_Change as needed
                        // Example:
                        TRAN_ID = "0000009999",
                        TRAN_DATE = DateTime.Now,
                        EMP_CODE = employee.EMP_CODE,
                        EMP_FNAME_O = employee.EMP_FNAME,
                        EMP_MNAME_O = employee.EMP_MNAME,
                        EMP_LNAME_O = employee.EMP_LNAME,
                        SHORT_NAME_O = employee.SHORT_NAME,
                        NAME_PREFIX_O = employee.NAME_PREFIX,
                        EMP_TYPE_O = employee.EMP_TYPE,
                        REPORT_TO_O = employee.REPORT_TO,
                        SEX_O = employee.SEX,
                        NATIONALITY_O = employee.NATIONALITY,
                        CITIZEN_O = employee.CITIZEN,
                        STATE_CODE__DOMICILE_O = employee.STAN_CODE,
                        BIRTH_DATE_O = employee.BIRTH_DATE,
                        BLOOD_GRP_O = employee.BLOOD_GRP,
                        CONTACT_PERS_O = employee.CONTACT_PERS,
                        CONTACT_ADD1_O = employee.CONTACT_ADD1,
                        CONTACT_ADD2_O = employee.CONTACT_ADD2,
                        CONTACT_ADD3_O = employee.CONTACT_ADD3,
                        CONTACT_CITY_O = employee.CONTACT_CITY,
                        CONTACT_STATE_O = employee.CONTACT_STATE,
                        CONTACT_PIN_O = employee.CONTACT_PIN,
                        CONTACT_TEL_O = employee.CONTACT_TEL,
                        RELIGION_O = employee.RELIGION,
                        JOINED_AS_O = employee.JOINED_AS,
                        DATE_JOIN_O = employee.DATE_JOIN,
                        MODE_APP_O = employee.MODE_APP,
                        WORK_SHIFT_O = employee.WORK_SHIFT,
                        SHIFT_ROT_O = employee.SHIFT_ROT,
                        HOL_TBLNO_O = employee.HOL_TBLNO,
                        PAY_TYPE_O = employee.PAY_TYPE,
                        PAY_MODE_O = employee.PAY_MODE,
                        BANK_CODE_O = employee.BANK_CODE,
                        BANK_ACCT_O = employee.BANK_ACCT,
                        STAN_CODE_O = employee.STAN_CODE,
                        DD_PAYABLE_BANK_O = employee.DD_PAYABLE_BANK,
                        TOT_EXP_O = employee.TOT_EXP,
                        PF_NO_O = employee.PF_NO,
                        FPF_NO_O = employee.FPF_NO,
                        ESIC_NO_O = employee.ESIC_NO,
                        IT_NO_O = employee.IT_NO,
                        GROSS_O = employee.GROSS,
                        HOBBY1_O = employee.HOBBY1,
                        HOBBY2_O = employee.HOBBY2,
                        HOBBY3_O = employee.HOBBY3,
                        HOBBY4_O = employee.HOBBY4,
                        OTHER_INFO_O = employee.OTHER_INFO,
                        TRAINING_PRD_O = employee.TRAINING_PRD,
                        PROBATION_DATE_O = employee.PROBATION_DATE,
                        PROBATION_PRD_O = employee.PROBATION_PRD,
                        NOTICE_PRD_O = employee.NOTICE_PRD,
                        EMP_CODE__LINK_O = employee.EMP_CODE__LINK,
                        ACCT_CODE__SAL_O = employee.ACCT_CODE__SAL,
                        CCTR_CODE__SAL_O = employee.CCTR_CODE__SAL,
                        ALTER_CODE_O = employee.ALTER_CODE,
                        WITH_HELD_O = employee.WITH_HELD,
                        PROC_GROUP_O = employee.PROC_GROUP,
                        //--------------------------------------------------------------------------
                        EMP_FNAME = employee.EMP_FNAME,
                        EMP_MNAME = employee.EMP_MNAME,
                        EMP_LNAME = employee.EMP_LNAME,
                        SHORT_NAME = employee.SHORT_NAME,
                        NAME_PREFIX = employee.NAME_PREFIX,
                        EMP_TYPE = employee.EMP_TYPE,
                        REPORT_TO = employee.REPORT_TO,
                        SEX = employee.SEX,
                        NATIONALITY = employee.NATIONALITY,
                        CITIZEN = employee.CITIZEN,
                        STATE_CODE__DOMICILE = employee.STATE_CODE__DOMICILE,
                        BIRTH_DATE = employee.BIRTH_DATE,
                        BLOOD_GRP = employee.BLOOD_GRP,
                        CONTACT_PERS = employee.CONTACT_PERS,
                        CONTACT_ADD1 = employee.CONTACT_ADD1,
                        CONTACT_ADD2 = employee.CONTACT_ADD2,
                        CONTACT_ADD3 = employee.CONTACT_ADD3,
                        CONTACT_CITY = employee.CONTACT_CITY,
                        CONTACT_STATE = employee.CONTACT_STATE,
                        CONTACT_PIN = employee.CONTACT_PIN,
                        CONTACT_TEL = employee.CONTACT_TEL,
                        RELIGION = employee.RELIGION,
                        JOINED_AS = employee.JOINED_AS,
                        DATE_JOIN = employee.DATE_JOIN,
                        MODE_APP = employee.MODE_APP,
                        WORK_SHIFT = employee.WORK_SHIFT,
                        SHIFT_ROT = employee.SHIFT_ROT,
                        HOL_TBLNO = employee.HOL_TBLNO,
                        PAY_TYPE = employee.PAY_TYPE,
                        PAY_MODE = employee.PAY_MODE,
                        BANK_CODE = employee.BANK_CODE,
                        BANK_ACCT = employee.BANK_ACCT,
                        STAN_CODE = employee.STAN_CODE,
                        DD_PAYABLE_BANK = employee.DD_PAYABLE_BANK,
                        TOT_EXP = employee.TOT_EXP,
                        PF_NO = employee.PF_NO,
                        FPF_NO = employee.FPF_NO,
                        ESIC_NO = employee.ESIC_NO,
                        IT_NO = employee.IT_NO,
                        GROSS = employee.GROSS,
                        HOBBY1 = employee.HOBBY1,
                        HOBBY2 = employee.HOBBY2,
                        HOBBY3 = employee.HOBBY3,
                        HOBBY4 = employee.HOBBY4,
                        OTHER_INFO = employee.OTHER_INFO,
                        TRAINING_PRD = employee.TRAINING_PRD,
                        PROBATION_DATE = employee.PROBATION_DATE,
                        PROBATION_PRD = employee.PROBATION_PRD,
                        NOTICE_PRD = employee.NOTICE_PRD,
                        EMP_CODE__LINK = employee.EMP_CODE__LINK,
                        ACCT_CODE__SAL = employee.ACCT_CODE__SAL,
                        CCTR_CODE__SAL = employee.CCTR_CODE__SAL,
                        ALTER_CODE = employee.ALTER_CODE,
                        WITH_HELD = employee.WITH_HELD,
                        PROC_GROUP = employee.PROC_GROUP,
                        //--------------------------------------------------------------------------
                        CHG_DATE = DateTime.Now, // Set the change date to current date
                        CHG_USER = employee.CHG_USER,
                        CHG_TERM = employee.CHG_TERM,
                        CONFIRMED = employee.CONFIRMED,
                        //CONF_DATE = 
                        //EMP_CODE__APRV = employee.EMP_CODE__
                        PROBATION_O = employee.PROBATION,
                        DATE_CONF_O = employee.DATE_CONF,
                        CONFIRMED_O = employee.CONFIRMED,
                        PROBATION = employee.PROBATION,
                        DATE_CONF = employee.DATE_CONF,
                        STATUS = employee.STATUS,
                        //FUNC_GROUP__O = employee.FUNC_GROUP,
                        //FUNC_GROUP__N = employee.
                        REPORT_TO__ADMIN_O = employee.REPORT_TO__ADMIN,
                        REPORT_TO__ADMIN = employee.REPORT_TO__ADMIN,
                        MAIDEN_NAME__O = employee.MAIDEN_NAME,
                        MAIDEN_NAME = employee.MAIDEN_NAME,
                        BIRTH_PLACE__O = employee.BIRTH_PLACE,
                        BIRTH_PLACE = employee.BIRTH_PLACE,
                        RESI_PERMIT_NO__O = employee.RESI_PERMIT_NO,
                        RESI_PERMIT_NO = employee.RESI_PERMIT_NO,
                        RESI_PERMIT_UPTO__O = employee.RESI_PERMIT_UPTO,
                        RESI_PERMIT_UPTO = employee.RESI_PERMIT_UPTO,
                        SOCIAL_SECURITY_NO__O = employee.SOCIAL_SECURITY_NO,
                        SOCIAL_SECURITY_NO = employee.SOCIAL_SECURITY_NO,
                        PATTERN_CODE__O = employee.PATTERN_CODE,
                        PATTERN_CODE = employee.PATTERN_CODE,
                        GRATUITY_DATE__O = employee.GRATUITY_DATE,
                        GRATUITY_DATE = employee.GRATUITY_DATE,
                        SHIFT_INDEX__O = employee.SHIFT_INDEX,
                        SHIFT_INDEX = employee.SHIFT_INDEX,
                        EMP_CLASS__O = employee.EMP_CLASS,
                        EMP_CLASS = employee.EMP_CLASS,
                        OCCUPATION_CODE__O = employee.OCCUPATION_CODE,
                        OCCUPATION_CODE = employee.OCCUPATION_CODE,
                        STAN_CODE__HQ = employee.STAN_CODE__HQ,
                        DEPUTE_NO = employee.DEPUTE_NO,
                        STAN_CODE__HQ__O = employee.STAN_CODE__HQ,
                        DEPUTE_NO__O = employee.DEPUTE_NO,
                        AUTO_ADV__O = employee.AUTO_ADV,
                        AUTO_ADV = employee.AUTO_ADV,
                        CREDIT_CARD_NO__O = employee.CREDIT_CARD_NO,
                        CREDIT_CARD_NO = employee.CREDIT_CARD_NO,
                        BENEV_FLAG__O = employee.BENEV_FLAG,
                        BENEV_FLAG = employee.BENEV_FLAG,
                        M_STATUS__O = employee.M_STATUS,
                        M_STATUS = employee.M_STATUS,
                        M_STATUS_DATE__O = employee.M_STATUS_DATE,
                        M_STATUS_DATE = employee.M_STATUS_DATE,
                        EMAIL_ID_PER__O = employee.EMAIL_ID_PER,
                        EMAIL_ID_PER = employee.EMAIL_ID_PER,
                        //REMARKS = employee.rem
                        //WITH_HELD_REMARKS = employee.wi
                        PF_UN_NO__O = employee.PF_UN_NO,
                        PF_UN_NO = employee.PF_UN_NO,
                        AADHAAR_NO__O = employee.AADHAAR_NO,
                        AADHAAR_NO = employee.AADHAAR_NO,
                        BANK_IFSC__O = employee.BANK_IFSC,
                        BANK_IFSC = employee.BANK_IFSC,
                        POS_CODE_O = employee.POS_CODE,
                        POS_CODE = employee.POS_CODE,
                        ESI_DISPENSARY__O = employee.ESI_DISPENSARY,
                        ESI_DISPENSARY = employee.ESI_DISPENSARY,
                        USER_ID_O = employee.USER_ID,
                        EMP_CODE__HOD = employee.EMP_CODE__HOD,
                        EMP_CODE__HOD_O = employee.EMP_CODE__HOD,
                        STAN_CODE__HQ_O = employee.STAN_CODE__HQ,
                        ELIGIBLE_OT__O = employee.ELIGIBLE_OT,
                        ELIGIBLE_OT = employee.ELIGIBLE_OT,
                        //PARENT_DEPT__N = employee
                        PARENT_DEPT = employee.PARENT_DEPT,
                        //WORK_LOC_CODE__N = employee.
                        WORK_LOC_CODE = employee.WORK_LOC_CODE,
                        USER_ID = employee.USER_ID,
                        EXT_NO = employee.EXT_NO,
                        //EXT_NO_N = employee
                        AADHAR_CARD_NO = employee.AADHAR_CARD_NO,
                        ELECTION_CARD_NO = employee.ELECTION_CARD_NO,
                        DRIV_LIC_NO = employee.DRIV_LIC_NO,
                        DRIV_LIC_VALID_UPTO = employee.DRIV_LIC_VALID_UPTO,
                        PHYSICAL_HANDICAP = employee.PHYSICAL_HANDICAP,
                        HANDICAP_CATEGORY = employee.HANDICAP_CATEGORY,
                        PF_UAN = employee.PF_UAN,
                        GROUP_MEDICLAIM = employee.GROUP_MEDICLAIM,
                        INSIDER_TRADING_OPT = employee.INSIDER_TRADING_OPT,
                        EMAIL_ID_OFF__O = employee.EMAIL_ID_OFF,
                        EMAIL_ID_OFF = employee.EMAIL_ID_OFF,
                        MOBILE_NO__O = employee.MOBILE_NO,
                        MOBILE_NO = employee.MOBILE_NO,
                        MOBILE_NO_OFF__O = employee.MOBILE_NO_OFF,
                        MOBILE_NO_OFF = employee.MOBILE_NO_OFF,
                        ELECTION_CARD_NO__O = employee.ELECTION_CARD_NO,
                        DRIV_LIC_NO__O = employee.DRIV_LIC_NO,
                        FATHER_NAME_O = employee.FATHER_NAME,
                        FATHER_NAME = employee.FATHER_NAME,
                        MOTHER_NAME_O = employee.MOTHER_NAME,
                        MOTHER_NAME  = employee.MOTHER_NAME,
                        PASSPORT_NO = employee.PASSPORT_NO,
                        PASSPORT_NO_O = employee.PASSPORT_NO,
                        PASSPORT_ISS_PLACE_O = employee.PASSPORT_ISS_PLACE,
                        PASSPORT_ISS_PLACE = employee.PASSPORT_ISS_PLACE,
                        PASSPORT_ISS_DATE_O = employee.PASSPORT_ISS_DATE,
                        PASSPORT_ISS_DATE = employee.PASSPORT_ISS_DATE,
                        PASSPORT_EXP_DATE_O = employee.PASSPORT_EXP_DATE,
                        PASSPORT_EXP_DATE = employee.PASSPORT_EXP_DATE,
                        ROLE_ID_O = employee.ROLE_ID,
                        ROLE_ID = employee.ROLE_ID,
                        DOMICILE_O = employee.DOMICILE,
                        DOMICILE = employee.DOMICILE,
                        AADHAR_EMP_NAME_O = employee.AADHAR_EMP_NAME,
                        AADHAR_MIDDLE_NAME_O = employee.AADHAR_MIDDLE_NAME,
                        AADHAR_BIRTH_DATE_O = employee.AADHAR_BIRTH_DATE,
                        PAN_EMP_NAME_O = employee.PAN_EMP_NAME,
                        PAN_MIDDLE_NAME_O = employee.PAN_MIDDLE_NAME,
                        PAN_BIRTH_DATE_O = employee.PAN_BIRTH_DATE,
                        AADHAR_EMP_NAME = employee.AADHAR_EMP_NAME,
                        AADHAR_MIDDLE_NAME = employee.AADHAR_MIDDLE_NAME,
                        AADHAR_BIRTH_DATE = employee.AADHAR_BIRTH_DATE,
                        PAN_EMP_NAME = employee.PAN_EMP_NAME,
                        PAN_MIDDLE_NAME = employee.PAN_MIDDLE_NAME,
                        PAN_BIRTH_DATE = employee.PAN_BIRTH_DATE
                    };

                    _appDbContext.EMPLOYEE_DET_CHANGE.Add(changeEntry);
                    await _appDbContext.SaveChangesAsync();
                }

                return (Employee)Ok(); // Successfully inserted change
            }
            catch (Exception ex)
            {
                // Handle exception appropriately (log, etc.)
                return (Employee)NotFound("Employee not found");
            }
        }

        // Update Employee Details

        public async Task<bool> UpdateEmployeeAsync(Employee employee)
        {
            using var transaction = await _appDbContext.Database.BeginTransactionAsync();
            try
            {
                // Create a SQL command to capture the full query
                var command = _appDbContext.Database.GetDbConnection().CreateCommand();
                _appDbContext.Entry(employee).State = EntityState.Modified;

                // Get the full SQL query with parameters
                string fullQuery = GetFullQueryWithParameters(command);

                // Log the full SQL query before saving changes
                Console.WriteLine($"Full SQL query before SaveChangesAsync: {fullQuery}");

                // Save changes to the database
                int affectedRows = await _appDbContext.SaveChangesAsync();

                // Log the number of affected rows
                Console.WriteLine($"Affected rows: {affectedRows}");

                // Commit the transaction
                await transaction.CommitAsync();

                // Return true if exactly one row was affected
                return affectedRows == 1;
            }
            catch (Exception ex)
            {
                // Rollback the transaction
                await transaction.RollbackAsync();

                // Handle specific database exceptions
                if (ex is DbUpdateException || ex is OracleException || ex is DbUpdateConcurrencyException)
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                // General exception handling
                string errorMessage = $"Error while updating employee: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        private string GetFullQueryWithParameters(DbCommand command)
        {
            // Build the SQL command text with parameters
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.AppendLine(command.CommandText);

            foreach (DbParameter parameter in command.Parameters)
            {
                queryBuilder.AppendLine($"{parameter.ParameterName} = {parameter.Value}");
            }

            return queryBuilder.ToString();
        }




        // Employee Edit Basic Details

        public async Task<bool> UpdateEmployeeBasicDetails(string empCode, EmployeeBasicDetailsUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                var commandText = @"
                UPDATE EMPLOYEE
                SET
                    EMP_FNAME  = :fname,
                    EMP_MNAME  = :mname,
                    EMP_LNAME  = :lname,
                    SEX  = :sex,
                    BIRTH_DATE  = TO_DATE(:BirthDate, 'YYYY-MM-DD'),
                    RELIGION  = :religion,
                    BLOOD_GRP  = :BloodGroup,
                    FATHER_NAME  = :FatherName,
                    MOTHER_NAME  = :MotherName,
                    MOBILE_NO  = :MobNo,
                    EMAIL_ID_PER  = :EmailId
                WHERE TRIM(EMP_CODE) = TRIM(:EmpCode)";

                // Create OracleCommand
                using (var command = new OracleCommand(commandText))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                    // Set parameters   
                    command.Parameters.Add(new OracleParameter("fname", updatedDetails.EMP_FNAME ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("mname", updatedDetails.EMP_MNAME ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("lname", updatedDetails.EMP_LNAME ?? (object)DBNull.Value));
                    //command.Parameters.Add(new OracleParameter("sex", updatedDetails.SEX ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("sex", updatedDetails.SEX != '\0' ? (object)updatedDetails.SEX : DBNull.Value));
                    command.Parameters.Add(new OracleParameter("BirthDate", updatedDetails.BIRTH_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("religion", updatedDetails.RELIGION ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("BloodGroup", updatedDetails.BLOOD_GRP ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("FatherName", updatedDetails.FATHER_NAME ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("MotherName", updatedDetails.MOTHER_NAME ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("MobNo", updatedDetails.MOBILE_NO ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("EmailId", updatedDetails.EMAIL_ID_PER ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));                

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error updating employee: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        // Employee Joining Details

        public async Task<bool> UpdateEmployeejoiningDetails(string empCode, EmployeeBasicDetailsUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                var commandText = @"
                UPDATE EMPLOYEE
                SET
                    EMP_FNAME  = :fname,
                    EMP_MNAME  = :mname,
                    EMP_LNAME  = :lname,
                    SEX  = :sex,
                    BIRTH_DATE  = TO_DATE(:BirthDate, 'YYYY-MM-DD'),
                    RELIGION  = :religion,
                    BLOOD_GRP  = :BloodGroup,
                    FATHER_NAME  = :FatherName,
                    MOTHER_NAME  = :MotherName,
                    MOBILE_NO  = :MobNo,
                    EMAIL_ID_PER  = :EmailId
                WHERE TRIM(EMP_CODE) = TRIM(:EmpCode)";

                // Create OracleCommand
                using (var command = new OracleCommand(commandText))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                    // Set parameters   
                    command.Parameters.Add(new OracleParameter("fname", updatedDetails.EMP_FNAME ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("mname", updatedDetails.EMP_MNAME ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("lname", updatedDetails.EMP_LNAME ?? (object)DBNull.Value));
                    //command.Parameters.Add(new OracleParameter("sex", updatedDetails.SEX ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("sex", updatedDetails.SEX != '\0' ? (object)updatedDetails.SEX : DBNull.Value));
                    command.Parameters.Add(new OracleParameter("BirthDate", updatedDetails.BIRTH_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("religion", updatedDetails.RELIGION ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("BloodGroup", updatedDetails.BLOOD_GRP ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("FatherName", updatedDetails.FATHER_NAME ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("MotherName", updatedDetails.MOTHER_NAME ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("MobNo", updatedDetails.MOBILE_NO ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("EmailId", updatedDetails.EMAIL_ID_PER ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error updating employee: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        // Employee Language
        // Add language API

        public async Task<bool> AddLanguage(AddLanguage newLanguage)
        {
            try
            {
                await _appDbContext.EMPLANG.AddAsync(newLanguage);
                await _appDbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {

                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while adding language: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }



        //error
        /*public async Task<bool> UpdateLanguage(string empCode, string language, UpdateLanguage updatedLanguage)
        {
            try
            {
                // Define the parameters with trimming
                var empCodeParam = new OracleParameter("empCode", empCode);
                var languageParam = new OracleParameter("language", language);

                // Construct the query string
                string query = "SELECT * FROM EMPLANG WHERE TRIM(EMP_CODE) = TRIM(:empCode) AND TRIM(LANGUAGE) = TRIM(:language)";
                //string query = "SELECT TRIM(EMP_CODE) AS EMP_CODE,LANGUAGE,READ,WRITE,SPEAK,WORK_PROFICIENCY FROM EMPLANG WHERE TRIM(EMP_CODE) = TRIM(:empCode) AND TRIM(LANGUAGE) = TRIM(:language)";

                // Log the full SQL query with parameters
                string fullQuery = GetFullQueryWithParameters(query, empCodeParam, languageParam);
                Console.WriteLine($"Full SQL query: {fullQuery}");

                // Fetch the existing record based on EmpCode and Language
                var existingLanguages = await _appDbContext.EMPLANG
                    .FromSqlRaw(query, empCodeParam, languageParam)
                    .ToListAsync();

                var existingLanguage = existingLanguages.FirstOrDefault();
                if (existingLanguage == null)
                {
                    // Log or handle the case where the record is not found
                    Console.WriteLine($"No language record found for EmpCode: {empCode}, Language: {language}");
                    return false;
                }

                // Log existing language details
                Console.WriteLine($"Existing language record: {JsonSerializer.Serialize(existingLanguage)}");

                // Update the properties of the existing record
                existingLanguage.READ = updatedLanguage.READ;
                existingLanguage.WRITE = updatedLanguage.WRITE;
                existingLanguage.SPEAK = updatedLanguage.SPEAK;
                existingLanguage.WORK_PROFICIENCY = updatedLanguage.WORK_PROFICIENCY;

                // Attach the entity to the context and mark it as modified
                _appDbContext.EMPLANG.Attach(existingLanguage);
                _appDbContext.Entry(existingLanguage).State = EntityState.Modified;

                // Save the changes to the database
                int affectedRows = await _appDbContext.SaveChangesAsync();

                // Log the number of affected rows
                Console.WriteLine($"Affected rows: {affectedRows}");

                return affectedRows == 1;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // Handle concurrency exception
                string errorMessage = $"Concurrency error while updating language: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new RepositoryException(errorMessage, ex.InnerException);
            }
            catch (Exception ex)
            {
                // Log or handle the exception
                string errorMessage = $"Error while updating language: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                // Check for specific exception types
                if (ex is DbUpdateException || ex is OracleException)
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }*/


       /* public async Task<bool> UpdateLanguage(string empCode, string language, UpdateLanguage updatedLanguage)
        {
            try
            {
                // Define the parameters with trimming
                var empCodeParam = new OracleParameter("empCode", empCode.Trim());
                var languageParam = new OracleParameter("language", language.Trim());

                // Construct the query string
                string query = "SELECT TRIM(EMP_CODE) AS EMP_CODE, LANGUAGE, READ, WRITE, SPEAK, WORK_PROFICIENCY FROM EMPLANG WHERE TRIM(EMP_CODE) = TRIM(:empCode) AND TRIM(LANGUAGE) = TRIM(:language)";

                // Fetch the existing record based on EmpCode and Language
                var existingLanguages = await _appDbContext.EMPLANG
                    .FromSqlRaw(query, empCodeParam, languageParam)
                    .ToListAsync();

                var existingLanguage = existingLanguages.FirstOrDefault();

                if (existingLanguage == null)
                {
                    // Log or handle the case where the record is not found
                    Console.WriteLine($"No language record found for EmpCode: {empCode}, Language: {language}");
                    return false;
                }

                // Log existing language details
                Console.WriteLine($"Existing language record: {JsonSerializer.Serialize(existingLanguage)}");

                // Detach the existing entity to avoid conflicts
                _appDbContext.Entry(existingLanguage).State = EntityState.Detached;

                // Create a new entity with the updated values and original key
                var updatedEntity = new Emplang
                {
                    EMP_CODE = empCode, 
                    LANGUAGE = language,  
                    READ = updatedLanguage.READ,
                    WRITE = updatedLanguage.WRITE,
                    SPEAK = updatedLanguage.SPEAK,
                    WORK_PROFICIENCY = updatedLanguage.WORK_PROFICIENCY
                };

                // Attach the updated entity to the context and mark it as modified
                _appDbContext.EMPLANG.Attach(updatedEntity);
                _appDbContext.Entry(updatedEntity).State = EntityState.Modified;

                // Save the changes to the database
                int affectedRows = await _appDbContext.SaveChangesAsync();

                // Log the number of affected rows
                Console.WriteLine($"Affected rows: {affectedRows}");

                return affectedRows == 1;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // Handle concurrency exception
                string errorMessage = $"Concurrency error while updating language: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new RepositoryException(errorMessage, ex.InnerException);
            }
            catch (Exception ex)
            {
                // Log or handle the exception
                string errorMessage = $"Error while updating language: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                // Check for specific exception types
                if (ex is DbUpdateException || ex is OracleException)
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }*/




        // Working Correctly Language Edit API
         public async Task<bool> UpdateLanguage(string EmpCode, string Language,  UpdateLanguage updatedLanguage)
         {
             Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

             try
             {
                 // Begin transaction
                 transaction = await _appDbContext.Database.BeginTransactionAsync();

                 var commandText = @"
                         UPDATE EMPLANG
                         SET READ = :Read, 
                         WRITE = :Write,
                         SPEAK = :Speak, 
                         WORK_PROFICIENCY = :WorkProficiency
                         WHERE TRIM(EMP_CODE) = TRIM(:EmpCode) AND TRIM(LANGUAGE) = TRIM(:Language)";

                 // Create OracleCommand
                 using (var command = new OracleCommand(commandText))
                 {
                     command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                     command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                     // Set parameters
                     command.Parameters.Add(new OracleParameter("Read", updatedLanguage.READ));
                     command.Parameters.Add(new OracleParameter("Write", updatedLanguage.WRITE));
                     command.Parameters.Add(new OracleParameter("Speak", updatedLanguage.SPEAK));
                     command.Parameters.Add(new OracleParameter("WorkProficiency", updatedLanguage.WORK_PROFICIENCY));
                     command.Parameters.Add(new OracleParameter("EmpCode", EmpCode));
                     command.Parameters.Add(new OracleParameter("Language", Language));

                     string fullQuery = GetFullQueryWithParameters(command);
                     Console.WriteLine($"Full SQL query: {fullQuery}");

                     // Execute the command
                     int affectedRows = command.ExecuteNonQuery();

                     // Commit transaction
                     transaction.Commit();

                     // Log success
                     Console.WriteLine($"Transaction committed successfully");

                     return affectedRows == 1; // Ensure exactly one row is affected
                 }
             }
             catch (DbUpdateConcurrencyException ex)
             {
                 // Handle concurrency conflict
                 if (transaction != null)
                 {
                     // Rollback transaction
                     transaction.Rollback();
                 }
                 throw new RepositoryException("Concurrency conflict occurred while updating language. The data may have been modified or deleted by another user.", ex);
             }
             catch (DbUpdateException ex)
             {
                 // Handle unique constraint violation
                 if (transaction != null)
                 {
                     // Rollback transaction
                     transaction.Rollback();
                 }
                 if (ex.InnerException != null && ex.InnerException.Message.Contains("ORA-00001"))
                 {
                     throw new RepositoryException("Unique constraint violated. The language record might already exist.", ex);
                 }
                 throw;
             }
             catch (Exception ex)
             {
                 // Handle other exceptions
                 if (transaction != null)
                 {
                     // Rollback transaction
                     transaction.Rollback();
                 }
                 string errorMessage = $"Error while updating language: {ex.Message}";
                 if (ex.InnerException != null)
                 {
                     errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                 }
                 throw new Exception(errorMessage, ex.InnerException);
             }         
         }

        // Contact Details
        // Edit Present Details

        public async Task<bool> UpdateEmployeePresentDetails(string empCode, EmployeePresentDetailsUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                var commandText = @"
                UPDATE EMPLOYEE
                SET
                    CUR_ADD1  = :CurAdd1,
                    CUR_ADD2  = :CurAdd2,
                    CUR_ADD3  = :CurAdd3,
                    CUR_CITY  = :CurCity,
                    CUR_STATE  = :CurState,
                    CUR_PIN  = :CurPin,
                    CUR_TEL1  = :CurTel1,
                    CUR_TEL2  = :CurTel2,
                    CUR_TEL3  = :CurTel3
                WHERE TRIM(EMP_CODE) = TRIM(:EmpCode)";

                // Create OracleCommand
                using (var command = new OracleCommand(commandText))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                    // Set parameters   
                    command.Parameters.Add(new OracleParameter("CurAdd1", updatedDetails.CUR_ADD1 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("CurAdd2", updatedDetails.CUR_ADD2 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("CurAdd3", updatedDetails.CUR_ADD3 ?? (object)DBNull.Value));                     
                    command.Parameters.Add(new OracleParameter("CurCity", updatedDetails.CUR_CITY ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("CurState", updatedDetails.CUR_STATE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("CurPin", updatedDetails.CUR_PIN ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("CurTel1", updatedDetails.CUR_TEL1 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("CurTel2", updatedDetails.CUR_TEL2 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("CurTel3", updatedDetails.CUR_TEL3 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error updating employee present detail: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        // Edit Permanent Details

        public async Task<bool> UpdateEmployeePermanentDetails(string empCode, EmployeePermanentDetailsUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                var commandText = @"
                UPDATE EMPLOYEE
	            SET
		            PER_ADD1  = :PerAdd1,
		            PER_ADD2  = :PerAdd2,
		            PER_ADD3  = :PerAdd3,
		            PER_CITY  = :PerCity,
		            PER_STATE  = :PerState,
		            PER_PIN  = :PerPin,
		            PER_TEL  = :PerTel
	            WHERE TRIM(EMP_CODE) = TRIM(:EmpCode)";

                // Create OracleCommand
                using (var command = new OracleCommand(commandText))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                    // Set parameters   
                    command.Parameters.Add(new OracleParameter("PerAdd1", updatedDetails.PER_ADD1 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PerAdd2", updatedDetails.PER_ADD2 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PerAdd3", updatedDetails.PER_ADD3 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PerCity", updatedDetails.PER_CITY ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PerState", updatedDetails.PER_STATE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PerPin", updatedDetails.PER_PIN ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("perTel", updatedDetails.PER_TEL ?? (object)DBNull.Value));                 
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error updating employee permanent detail: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        // Edit Emergency Details

        public async Task<bool> UpdateEmployeeEmergencyDetails(string empCode, EmployeeEmergencyDetailsUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                var commandText = @"
                UPDATE EMPLOYEE
                SET
                    CONTACT_PERS  = :ContPer,
                    CONTACT_ADD1  = :ContAdd1,
                    CONTACT_ADD2  = :ContAdd2,
                    CONTACT_ADD3  = :ContAdd3,
                    CONTACT_CITY  = :ContCity,
                    CONTACT_STATE  = :ContState,
                    CONTACT_TEL  = :Conttel,
	                CONTACT_PIN = :ContactPin
                WHERE TRIM(EMP_CODE) = TRIM(:EmpCode)";

                // Create OracleCommand
                using (var command = new OracleCommand(commandText))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                    // Set parameters   
                    command.Parameters.Add(new OracleParameter("ContPer", updatedDetails.CONTACT_PERS ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ContAdd1", updatedDetails.CONTACT_ADD1 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ContAdd2", updatedDetails.CONTACT_ADD2 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ContAdd3", updatedDetails.CONTACT_ADD3 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ContCity", updatedDetails.CONTACT_CITY ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ContState", updatedDetails.CONTACT_STATE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("Conttel", updatedDetails.CONTACT_TEL ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ContactPin", updatedDetails.CONTACT_PIN ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error updating employee emergency detail: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        // Employment Details
        // Edit Work and Payroll details

        public async Task<bool> UpdateEmployeeWorkPayrollDetails(string empCode, EmployeeWorkPayrollDetailsUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                var commandText = @"
                UPDATE EMPLOYEE
                SET
                    WORK_SITE  = :WorkSite,
                    PAY_SITE  = :PaySite,
                    GRATUITY_DATE  = TO_DATE(:GratuityDate, 'YYYY-MM-DD'),
                    NOTICE_PRD  = :NoticePrd
                WHERE TRIM(EMP_CODE) = TRIM(:EmpCode)";

                // Create OracleCommand
                using (var command = new OracleCommand(commandText))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                    // Set parameters   
                    command.Parameters.Add(new OracleParameter("WorkSite", updatedDetails.WORK_SITE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PaySite", updatedDetails.PAY_SITE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("GratuityDate", updatedDetails.GRATUITY_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("NoticePrd", updatedDetails.NOTICE_PRD ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error updating employee work and payroll detail: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }


        // KYC Details
        // Edit KYC details

        public async Task<bool> UpdateEmployeeKYCDetails(string empCode, EmployeeKYCUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                var commandText = @"
                UPDATE EMPLOYEE
                SET
                    AADHAR_EMP_NAME  = :AEmpName,
                    PAN_EMP_NAME  = :PEmpName,
                    PASSPORT_ISS_DATE  = TO_DATE(:PassIssDate, 'YYYY-MM-DD'),
                    PASSPORT_EXP_DATE  = TO_DATE(:PassExpDate, 'YYYY-MM-DD')
                WHERE TRIM(EMP_CODE) = TRIM(:EmpCode)";

                // Create OracleCommand
                using (var command = new OracleCommand(commandText))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                    // Set parameters   
                    command.Parameters.Add(new OracleParameter("AEmpName", updatedDetails.AADHAR_EMP_NAME ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PEmpName", updatedDetails.PAN_EMP_NAME ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PassIssDate", updatedDetails.PASSPORT_ISS_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PassExpDate", updatedDetails.PASSPORT_EXP_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error updating employee KYC details: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }


        // Employee Family      
        // Add family API for single member
        /* public async Task<bool> AddFamily(AddFamily newfamily)
         {
             try
             {

                 bool nameExists = _appDbContext.EMPFAMILY.Any(g => (g.MEMBER_NAME).Trim().ToUpper() == (newfamily.MEMBER_NAME).Trim().ToUpper());
                 if (nameExists)
                 {
                     return false;
                 }
                 else
                 {

                     // Get the current maximum SEQ_NO for the given EMP_CODE
                     var maxSeqNoStr = await _appDbContext.EMPFAMILY
                         .Where(f => f.EMP_CODE.Trim() == newfamily.EMP_CODE.Trim())
                         .MaxAsync(f => f.SEQ_NO);


                     int maxSeqNo = 0;
                     if (!string.IsNullOrEmpty(maxSeqNoStr) && int.TryParse(maxSeqNoStr, out int parsedSeqNo))
                     {
                         maxSeqNo = parsedSeqNo;
                     }

                     // Increment the SEQ_NO by 1
                     int newSeqNo = maxSeqNo + 1;
                     string newSeqNoStr = newSeqNo.ToString();


                     //string formattedDate = newfamily.DATE_BIRTH.ToString("MM-dd-yy"); // for not null
                     string formattedBirthDate = newfamily.DATE_BIRTH?.ToString("MM-dd-yy"); // for nullable                  
                     string formattedChangeDate = newfamily.CHG_DATE?.ToString("MM-dd-yy");


                     // Create a new family entity to add to the database, including the formatted date
                     var familyEntity = new AddFamily
                     {
                         EMP_CODE = newfamily.EMP_CODE,
                         SEQ_NO = newSeqNoStr,
                         MEMBER_NAME = newfamily.MEMBER_NAME,
                         //DATE_BIRTH = DateTime.ParseExact(formattedDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                         DATE_BIRTH = string.IsNullOrEmpty(formattedBirthDate)
                         ? (DateTime?)null
                         : DateTime.ParseExact(formattedBirthDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                         SEX = newfamily.SEX,
                         RELATION = newfamily.RELATION,
                         OCCUPATION = newfamily.OCCUPATION,
                         DEPENDENT = newfamily.DEPENDENT,
                         CHG_USER = newfamily.CHG_USER,
                         CHG_DATE = string.IsNullOrEmpty(formattedChangeDate)
                         ? (DateTime?)null
                         : DateTime.ParseExact(formattedChangeDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                         CHG_TERM = newfamily.CHG_TERM,
                         PF_NOMINEE = newfamily.PF_NOMINEE,
                         PF_PERC = newfamily.PF_PERC,
                         GRAT_NOMINEE = newfamily.GRAT_NOMINEE,
                         GRAT_PERC = newfamily.GRAT_PERC,
                         PEN_NOMINEE = newfamily.PEN_NOMINEE,
                         PEN_PERC = newfamily.PEN_PERC,
                         PHYSICAL_STATUS = newfamily.PHYSICAL_STATUS,
                         QLF_CODE = newfamily.QLF_CODE,
                         ADOPTED = newfamily.ADOPTED,
                         COMP_NOMINEE = newfamily.COMP_NOMINEE,
                         COMP_PERC = newfamily.COMP_PERC,
                         MEDI_NOMINEE = newfamily.MEDI_NOMINEE,
                         ACC_INC_NOMINEE = newfamily.ACC_INC_NOMINEE,
                         ACC_INC_PERC = newfamily.ACC_INC_PERC,
                         DEATH_NOMINEE = newfamily.DEATH_NOMINEE,
                         DEATH_PERC = newfamily.DEATH_PERC
                     };

                     // Add the family member to the context and save changes
                     await _appDbContext.EMPFAMILY.AddAsync(familyEntity);
                     await _appDbContext.SaveChangesAsync();

                     return true;
                 }
             }
             catch (Exception ex)
             {
                 string str = ex.GetType().ToString();
                 if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                 {

                     //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                     //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                     string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                     throw new RepositoryException(error);
                 }
                 string errorMessage = $"Error getting family: {ex.Message}";

                 if (ex.InnerException != null)
                 {
                     errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                 }

                 throw new Exception(errorMessage, ex.InnerException);

             }
         }*/

        // Add family API for multiple member
        /*public async Task<bool> AddFamilies(List<AddFamily> newFamilies)
        {
            try
            {
                // Check if any of the new family members already exist
                foreach (var newFamily in newFamilies)
                {
                    bool nameExists = _appDbContext.EMPFAMILY.Any(g => (g.MEMBER_NAME).Trim().ToUpper() == (newFamily.MEMBER_NAME).Trim().ToUpper());
                    if (nameExists)
                    {
                        return false;
                    }
                }

                // Group the new families by EMP_CODE
                var empCodeGroups = newFamilies.GroupBy(f => f.EMP_CODE.Trim());

                foreach (var group in empCodeGroups)
                {
                    var empCode = group.Key;

                    // Get the current maximum SEQ_NO for the given EMP_CODE
                    var maxSeqNoStr = await _appDbContext.EMPFAMILY
                        .Where(f => f.EMP_CODE.Trim() == empCode)
                        .MaxAsync(f => f.SEQ_NO);

                    int maxSeqNo = 0;
                    if (!string.IsNullOrEmpty(maxSeqNoStr) && int.TryParse(maxSeqNoStr, out int parsedSeqNo))
                    {
                        maxSeqNo = parsedSeqNo;
                    }

                    foreach (var newFamily in group)
                    {
                        int newSeqNo = ++maxSeqNo; // Increment the sequence number for each family member
                        string newSeqNoStr = newSeqNo.ToString();

                        string formattedBirthDate = newFamily.DATE_BIRTH?.ToString("MM-dd-yy");
                        string formattedChangeDate = newFamily.CHG_DATE?.ToString("MM-dd-yy");

                        var familyEntity = new EMPFAMILY
                        {
                            EMP_CODE = newFamily.EMP_CODE,
                            SEQ_NO = newSeqNoStr,
                            MEMBER_NAME = newFamily.MEMBER_NAME,
                            DATE_BIRTH = string.IsNullOrEmpty(formattedBirthDate)
                                ? (DateTime?)null
                                : DateTime.ParseExact(formattedBirthDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                            SEX = newFamily.SEX,
                            RELATION = newFamily.RELATION,
                            OCCUPATION = newFamily.OCCUPATION,
                            DEPENDENT = newFamily.DEPENDENT,
                            CHG_USER = newFamily.CHG_USER,
                            CHG_DATE = string.IsNullOrEmpty(formattedChangeDate)
                                ? (DateTime?)null
                                : DateTime.ParseExact(formattedChangeDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                            CHG_TERM = newFamily.CHG_TERM,
                            PF_NOMINEE = newFamily.PF_NOMINEE,
                            PF_PERC = newFamily.PF_PERC,
                            GRAT_NOMINEE = newFamily.GRAT_NOMINEE,
                            GRAT_PERC = newFamily.GRAT_PERC,
                            PEN_NOMINEE = newFamily.PEN_NOMINEE,
                            PEN_PERC = newFamily.PEN_PERC,
                            PHYSICAL_STATUS = newFamily.PHYSICAL_STATUS,
                            QLF_CODE = newFamily.QLF_CODE,
                            ADOPTED = newFamily.ADOPTED,
                            COMP_NOMINEE = newFamily.COMP_NOMINEE,
                            COMP_PERC = newFamily.COMP_PERC,
                            MEDI_NOMINEE = newFamily.MEDI_NOMINEE,
                            ACC_INC_NOMINEE = newFamily.ACC_INC_NOMINEE,
                            ACC_INC_PERC = newFamily.ACC_INC_PERC,
                            DEATH_NOMINEE = newFamily.DEATH_NOMINEE,
                            DEATH_PERC = newFamily.DEATH_PERC
                        };

                        // Add the family member to the context
                        await _appDbContext.EMPFAMILY.AddAsync(familyEntity);
                    }
                }

                // Save all changes to the database
                await _appDbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error adding families: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }*/

        // EMP_CODE Error
        /*public async Task<bool> AddFamilies(List<AddFamily> newFamilies)
        {
            try
            {
                foreach (var newFamily in newFamilies)
                {
                    bool nameExists = await _appDbContext.EMPFAMILY
                        .AsNoTracking()
                        .AnyAsync(g => g.MEMBER_NAME.Trim().ToUpper() == newFamily.MEMBER_NAME.Trim().ToUpper());
                    if (nameExists)
                    {
                        return false;
                    }
                }

                var empCodeGroups = newFamilies.GroupBy(f => f.EMP_CODE.Trim());

                foreach (var group in empCodeGroups)
                {
                    var empCode = group.Key;

                    var maxSeqNoStr = await _appDbContext.EMPFAMILY
                        .AsNoTracking()
                        .Where(f => f.EMP_CODE.Trim() == empCode)
                        .MaxAsync(f => f.SEQ_NO);

                    int maxSeqNo = 0;
                    if (!string.IsNullOrEmpty(maxSeqNoStr) && int.TryParse(maxSeqNoStr, out int parsedSeqNo))
                    {
                        maxSeqNo = parsedSeqNo;
                    }

                    foreach (var newFamily in group)
                    {
                        int newSeqNo = ++maxSeqNo;
                        string newSeqNoStr = newSeqNo.ToString();

                        string formattedBirthDate = newFamily.DATE_BIRTH?.ToString("MM-dd-yy");
                        string formattedChangeDate = newFamily.CHG_DATE?.ToString("MM-dd-yy");

                        var familyEntity = new AddFamily
                        {
                            EMP_CODE = newFamily.EMP_CODE,
                            SEQ_NO = newSeqNoStr,
                            MEMBER_NAME = newFamily.MEMBER_NAME,
                            DATE_BIRTH = string.IsNullOrEmpty(formattedBirthDate)
                                ? (DateTime?)null
                                : DateTime.ParseExact(formattedBirthDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                            SEX = newFamily.SEX,
                            RELATION = newFamily.RELATION,
                            OCCUPATION = newFamily.OCCUPATION,
                            DEPENDENT = newFamily.DEPENDENT,
                            CHG_USER = newFamily.CHG_USER,
                            CHG_DATE = string.IsNullOrEmpty(formattedChangeDate)
                                ? (DateTime?)null
                                : DateTime.ParseExact(formattedChangeDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                            CHG_TERM = newFamily.CHG_TERM,
                            PF_NOMINEE = newFamily.PF_NOMINEE,
                            PF_PERC = newFamily.PF_PERC,
                            GRAT_NOMINEE = newFamily.GRAT_NOMINEE,
                            GRAT_PERC = newFamily.GRAT_PERC,
                            PEN_NOMINEE = newFamily.PEN_NOMINEE,
                            PEN_PERC = newFamily.PEN_PERC,
                            PHYSICAL_STATUS = newFamily.PHYSICAL_STATUS,
                            QLF_CODE = newFamily.QLF_CODE,
                            ADOPTED = newFamily.ADOPTED,
                            COMP_NOMINEE = newFamily.COMP_NOMINEE,
                            COMP_PERC = newFamily.COMP_PERC,
                            MEDI_NOMINEE = newFamily.MEDI_NOMINEE,
                            ACC_INC_NOMINEE = newFamily.ACC_INC_NOMINEE,
                            ACC_INC_PERC = newFamily.ACC_INC_PERC,
                            DEATH_NOMINEE = newFamily.DEATH_NOMINEE,
                            DEATH_PERC = newFamily.DEATH_PERC
                        };

                        // Add the family member to the context
                        await _appDbContext.EMPFAMILY.AddAsync(familyEntity);
                    }

                    // Save changes to the database
                    await _appDbContext.SaveChangesAsync();
                    // Clear tracked entities to avoid tracking conflicts
                    _appDbContext.ChangeTracker.Clear();
                }

                return true;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error adding families: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }*/
        /*
        public async Task<bool> AddFamilies(List<AddFamily> newFamilies)
        {
            try
            {
                // Check if any member name already exists
                foreach (var newFamily in newFamilies)
                {
                    bool nameExists = await _appDbContext.EMPFAMILY
                        .AsNoTracking()
                        .AnyAsync(g => g.MEMBER_NAME.Trim().ToUpper() == newFamily.MEMBER_NAME.Trim().ToUpper());
                    if (nameExists)
                    {
                        return false;
                    }
                }

                var empCodeGroups = newFamilies.GroupBy(f => f.EMP_CODE.Trim());

                foreach (var group in empCodeGroups)
                {
                    var empCode = group.Key;

                    var maxSeqNoStr = await _appDbContext.EMPFAMILY
                        .AsNoTracking()
                        .Where(f => f.EMP_CODE.Trim() == empCode)
                        .MaxAsync(f => f.SEQ_NO);

                    int maxSeqNo = 0;
                    if (!string.IsNullOrEmpty(maxSeqNoStr) && int.TryParse(maxSeqNoStr, out int parsedSeqNo))
                    {
                        maxSeqNo = parsedSeqNo;
                    }

                    var familyEntities = new List<AddFamily>();

                    foreach (var newFamily in group)
                    {
                        int newSeqNo = ++maxSeqNo;
                        string newSeqNoStr = newSeqNo.ToString();

                        string formattedBirthDate = newFamily.DATE_BIRTH?.ToString("MM-dd-yy");
                        string formattedChangeDate = newFamily.CHG_DATE?.ToString("MM-dd-yy");

                        var familyEntity = new AddFamily
                        {
                            EMP_CODE = newFamily.EMP_CODE,
                            SEQ_NO = newSeqNoStr,
                            MEMBER_NAME = newFamily.MEMBER_NAME,
                            DATE_BIRTH = string.IsNullOrEmpty(formattedBirthDate)
                                ? (DateTime?)null
                                : DateTime.ParseExact(formattedBirthDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                            SEX = newFamily.SEX,
                            RELATION = newFamily.RELATION,
                            OCCUPATION = newFamily.OCCUPATION,
                            DEPENDENT = newFamily.DEPENDENT,
                            CHG_USER = newFamily.CHG_USER,
                            CHG_DATE = string.IsNullOrEmpty(formattedChangeDate)
                                ? (DateTime?)null
                                : DateTime.ParseExact(formattedChangeDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                            CHG_TERM = newFamily.CHG_TERM,
                            PF_NOMINEE = newFamily.PF_NOMINEE,
                            PF_PERC = newFamily.PF_PERC,
                            GRAT_NOMINEE = newFamily.GRAT_NOMINEE,
                            GRAT_PERC = newFamily.GRAT_PERC,
                            PEN_NOMINEE = newFamily.PEN_NOMINEE,
                            PEN_PERC = newFamily.PEN_PERC,
                            PHYSICAL_STATUS = newFamily.PHYSICAL_STATUS,
                            QLF_CODE = newFamily.QLF_CODE,
                            ADOPTED = newFamily.ADOPTED,
                            COMP_NOMINEE = newFamily.COMP_NOMINEE,
                            COMP_PERC = newFamily.COMP_PERC,
                            MEDI_NOMINEE = newFamily.MEDI_NOMINEE,
                            ACC_INC_NOMINEE = newFamily.ACC_INC_NOMINEE,
                            ACC_INC_PERC = newFamily.ACC_INC_PERC,
                            DEATH_NOMINEE = newFamily.DEATH_NOMINEE,
                            DEATH_PERC = newFamily.DEATH_PERC
                        };

                        familyEntities.Add(familyEntity);
                    }

                    // Add the family members to the context in bulk
                    _appDbContext.EMPFAMILY.AddRange((familyEntities));

                    // Save changes and clear the tracker for each group to avoid tracking issues
                    await _appDbContext.SaveChangesAsync();
                    _appDbContext.ChangeTracker.Clear();
                }

                return true;
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error adding families: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }
        */

        /*public async Task<bool> AddFamilies(List<AddFamily> newFamilies)
        {
            try
            {
                // Check if any member name already exists
                foreach (var newFamily in newFamilies)
                {
                    bool nameExists = await _appDbContext.EMPFAMILY
                        .AsNoTracking()
                        .AnyAsync(g => g.MEMBER_NAME.Trim().ToUpper() == newFamily.MEMBER_NAME.Trim().ToUpper());
                    if (nameExists)
                    {
                        return false;
                    }
                }

                var empCodeGroups = newFamilies.GroupBy(f => f.EMP_CODE.Trim());

                foreach (var group in empCodeGroups)
                {
                    var empCode = group.Key;

                    var maxSeqNoStr = await _appDbContext.EMPFAMILY
                        .AsNoTracking()
                        .Where(f => f.EMP_CODE.Trim() == empCode)
                        .MaxAsync(f => f.SEQ_NO);

                    int maxSeqNo = 0;
                    if (!string.IsNullOrEmpty(maxSeqNoStr) && int.TryParse(maxSeqNoStr, out int parsedSeqNo))
                    {
                        maxSeqNo = parsedSeqNo;
                    }

                    foreach (var newFamily in group)
                    {
                        int newSeqNo = ++maxSeqNo;
                        string newSeqNoStr = newSeqNo.ToString();

                        string formattedBirthDate = newFamily.DATE_BIRTH?.ToString("MM-dd-yy");
                        string formattedChangeDate = newFamily.CHG_DATE?.ToString("MM-dd-yy");

                        var familyEntity = new EmpFamilyEntity
                        {
                            EMP_CODE = newFamily.EMP_CODE,
                            SEQ_NO = newSeqNoStr,
                            MEMBER_NAME = newFamily.MEMBER_NAME,
                            DATE_BIRTH = string.IsNullOrEmpty(formattedBirthDate)
                                ? (DateTime?)null
                                : DateTime.ParseExact(formattedBirthDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                            SEX = newFamily.SEX,
                            RELATION = newFamily.RELATION,
                            OCCUPATION = newFamily.OCCUPATION,
                            DEPENDENT = newFamily.DEPENDENT,
                            CHG_USER = newFamily.CHG_USER,
                            CHG_DATE = string.IsNullOrEmpty(formattedChangeDate)
                                ? (DateTime?)null
                                : DateTime.ParseExact(formattedChangeDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                            CHG_TERM = newFamily.CHG_TERM,
                            PF_NOMINEE = newFamily.PF_NOMINEE,
                            PF_PERC = newFamily.PF_PERC,
                            GRAT_NOMINEE = newFamily.GRAT_NOMINEE,
                            GRAT_PERC = newFamily.GRAT_PERC,
                            PEN_NOMINEE = newFamily.PEN_NOMINEE,
                            PEN_PERC = newFamily.PEN_PERC,
                            PHYSICAL_STATUS = newFamily.PHYSICAL_STATUS,
                            QLF_CODE = newFamily.QLF_CODE,
                            ADOPTED = newFamily.ADOPTED,
                            COMP_NOMINEE = newFamily.COMP_NOMINEE,
                            COMP_PERC = newFamily.COMP_PERC,
                            MEDI_NOMINEE = newFamily.MEDI_NOMINEE,
                            ACC_INC_NOMINEE = newFamily.ACC_INC_NOMINEE,
                            ACC_INC_PERC = newFamily.ACC_INC_PERC,
                            DEATH_NOMINEE = newFamily.DEATH_NOMINEE,
                            DEATH_PERC = newFamily.DEATH_PERC
                        };

                        // Check if the entity is already being tracked
                        var existingEntity = await _appDbContext.EMPFAMILY
                            .FirstOrDefaultAsync(f => f.EMP_CODE == familyEntity.EMP_CODE && f.SEQ_NO == familyEntity.SEQ_NO);

                        if (existingEntity == null)
                        {
                            // Attach the entity if it's not already tracked
                            _appDbContext.EMPFAMILY.Attach(familyEntity);
                        }
                        else
                        {
                            // Update properties of the existing tracked entity
                            _appDbContext.Entry(existingEntity).CurrentValues.SetValues(familyEntity);
                        }
                    }

                    // Save changes for each group and clear the tracker
                    await _appDbContext.SaveChangesAsync();
                    _appDbContext.ChangeTracker.Clear();
                }

                return true;
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error adding families: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }
        */

        public async Task<bool> AddFamilies(List<AddFamily> newFamilies)
        {
            try
            {
                // Check if any member name already exists
                foreach (var newFamily in newFamilies)
                {
                    bool nameExists = await _appDbContext.EMPFAMILY
                        .AsNoTracking()
                        .AnyAsync(g => g.MEMBER_NAME.Trim().ToUpper() == newFamily.MEMBER_NAME.Trim().ToUpper());
                    if (nameExists)
                    {
                        return false;
                    }
                }

                // Group by EMP_CODE to process families in batches
                var empCodeGroups = newFamilies.GroupBy(f => f.EMP_CODE.Trim());

                foreach (var group in empCodeGroups)
                {
                    var empCode = group.Key;

                    // Retrieve maximum SEQ_NO for the current EMP_CODE
                    var maxSeqNoStr = await _appDbContext.EMPFAMILY
                        .AsNoTracking()
                        .Where(f => f.EMP_CODE.Trim() == empCode)
                        .MaxAsync(f => f.SEQ_NO);

                    int maxSeqNo = 0;
                    if (!string.IsNullOrEmpty(maxSeqNoStr) && int.TryParse(maxSeqNoStr, out int parsedSeqNo))
                    {
                        maxSeqNo = parsedSeqNo;
                    }

                    // Prepare a list to collect entities to add or update
                    var entitiesToAddOrUpdate = new List<AddFamily>();

                    foreach (var newFamily in group)
                    {
                        int newSeqNo = ++maxSeqNo;
                        string newSeqNoStr = newSeqNo.ToString();

                        string formattedBirthDate = newFamily.DATE_BIRTH?.ToString("MM-dd-yy");
                        string formattedChangeDate = newFamily.CHG_DATE?.ToString("MM-dd-yy");

                        var familyEntity = new AddFamily
                        {
                            EMP_CODE = newFamily.EMP_CODE,
                            SEQ_NO = newSeqNoStr,
                            MEMBER_NAME = newFamily.MEMBER_NAME,
                            DATE_BIRTH = string.IsNullOrEmpty(formattedBirthDate)
                                ? (DateTime?)null
                                : DateTime.ParseExact(formattedBirthDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                            SEX = newFamily.SEX,
                            RELATION = newFamily.RELATION,
                            OCCUPATION = newFamily.OCCUPATION,
                            DEPENDENT = newFamily.DEPENDENT,
                            CHG_USER = newFamily.CHG_USER,
                            CHG_DATE = string.IsNullOrEmpty(formattedChangeDate)
                                ? (DateTime?)null
                                : DateTime.ParseExact(formattedChangeDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                            CHG_TERM = newFamily.CHG_TERM,
                            PF_NOMINEE = newFamily.PF_NOMINEE,
                            PF_PERC = newFamily.PF_PERC,
                            GRAT_NOMINEE = newFamily.GRAT_NOMINEE,
                            GRAT_PERC = newFamily.GRAT_PERC,
                            PEN_NOMINEE = newFamily.PEN_NOMINEE,
                            PEN_PERC = newFamily.PEN_PERC,
                            PHYSICAL_STATUS = newFamily.PHYSICAL_STATUS,
                            QLF_CODE = newFamily.QLF_CODE,
                            ADOPTED = newFamily.ADOPTED,
                            COMP_NOMINEE = newFamily.COMP_NOMINEE,
                            COMP_PERC = newFamily.COMP_PERC,
                            MEDI_NOMINEE = newFamily.MEDI_NOMINEE,
                            ACC_INC_NOMINEE = newFamily.ACC_INC_NOMINEE,
                            ACC_INC_PERC = newFamily.ACC_INC_PERC,
                            DEATH_NOMINEE = newFamily.DEATH_NOMINEE,
                            DEATH_PERC = newFamily.DEATH_PERC
                        };

                        entitiesToAddOrUpdate.Add(familyEntity);
                    }

                    // Add or update entities in the database
                  
                    foreach (var entity in entitiesToAddOrUpdate)
                    {
                        // Define the SQL query to check for the existence of the entity
                        string sqlQuery = $"SELECT COUNT(*) FROM EMPFAMILY WHERE TRIM(EMP_CODE) = '{entity.EMP_CODE.Trim()}' AND TRIM(SEQ_NO) = '{entity.SEQ_NO.Trim()}'";

                        // Execute the SQL query to check if the entity already exists
                        int count;
                        using (var command = _appDbContext.Database.GetDbConnection().CreateCommand())
                        {
                            command.CommandText = sqlQuery;
                            _appDbContext.Database.OpenConnection();
                            count = Convert.ToInt32(await command.ExecuteScalarAsync());
                            _appDbContext.Database.CloseConnection();
                        }

                        if (count == 0)
                        {
                            // Add new entity to DbSet
                            _appDbContext.EMPFAMILY.Add(entity);

                            await _appDbContext.SaveChangesAsync();

                            // Immediately detach the entity to avoid tracking conflict
                            _appDbContext.Entry(entity).State = EntityState.Detached;
                        }
                    }

                    //await _appDbContext.SaveChangesAsync(); // Save changes for this group
                    _appDbContext.ChangeTracker.Clear(); // Clear the change tracker
                }

                return true; // All families added successfully
            }
            catch (Exception ex)
            {
                // Handle exceptions
                if (ex is DbUpdateException || ex is OracleException)
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error adding families: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }





        // update employee family API
        public async Task<bool> UpdateEmpFamilyAsync(string empCode, string seqNo, EmployeeFamilyUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                /* // SQL query to fetch existing data
                 var existingDataQuery = @"
                 SELECT *
                 FROM EMPFAMILY
                 WHERE TRIM(EMP_CODE) = TRIM(:EmpCode) AND TRIM(SEQ_NO) = TRIM(:SeqNo)";

                 var empCodeParam = new OracleParameter("EmpCode", empCode);
                 var seqNoParam = new OracleParameter("SeqNo", seqNo);

                 // Log full SQL query for fetching existing data
                 string fullExistingDataQuery = GetFullQueryWithParameters(existingDataQuery, empCodeParam, seqNoParam);
                 Console.WriteLine($"Full SQL query for fetching existing data: {fullExistingDataQuery}");

                 // Execute raw SQL query to get existing data
                 var existingDatas = await _appDbContext.EMPFAMILY
                     .FromSqlRaw(existingDataQuery, empCodeParam, seqNoParam)
                     .ToListAsync();

                 if (existingDatas == null || !existingDatas.Any())
                 {
                     Console.WriteLine($"No record found with EMP_CODE = {empCode} and SEQ_NO = {seqNo}");
                     return false;
                 }
                 else
                 {
                     // Log the existing data using System.Text.Json
                     string existingDataJson = JsonSerializer.Serialize(existingDatas, new JsonSerializerOptions { WriteIndented = true });
                     Console.WriteLine($"Existing data: {existingDataJson}");
                 }*/

                var commandText = @"
                        UPDATE EMPFAMILY
                        SET
                            MEMBER_NAME = :MemberName,               
                            DATE_BIRTH = TO_DATE(:birthDate, 'YYYY-MM-DD'),
                            SEX = :Sex,
                            RELATION = :Relation,
                            OCCUPATION = :Occupation,
                            DEPENDENT = :Dependent,
                            CHG_USER = :ChgUser,
                            CHG_DATE = TO_DATE(:ChgDate, 'YYYY-MM-DD'),
                            CHG_TERM = :ChgTerm,
                            PHYSICAL_STATUS = :PhysicalStatus,
                            QLF_CODE = :QlfCode,
                            ADOPTED = :Adopted                          
                        WHERE TRIM(EMP_CODE) = TRIM(:EmpCode) AND TRIM(SEQ_NO) = TRIM(:SeqNo)";

                /*  PF_NOMINEE = :PfNominee,
                    PF_PERC = :PfPerc,
                    GRAT_NOMINEE = :GratNominee,
                    GRAT_PERC = :GratPerc,
                    PEN_NOMINEE = :PenNominee,
                    PEN_PERC = :PenPerc,
                    COMP_NOMINEE = :CompNominee,
                    COMP_PERC = :CompPerc,
                    MEDI_NOMINEE = :MediNominee,
                    ACC_INC_NOMINEE = :AccIncNominee,
                    ACC_INC_PERC = :AccIncPerc,
                    DEATH_NOMINEE = :DeathNominee,
                    DEATH_PERC = :DeathPerc
                */

                // Create OracleCommand
                using (var command = new OracleCommand(commandText))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                    // Set parameters
                    command.Parameters.Add(new OracleParameter("MemberName", updatedDetails.MEMBER_NAME ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("birthDate", updatedDetails.DATE_BIRTH?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("Sex", updatedDetails.SEX ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("Relation", updatedDetails.RELATION ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("Occupation", updatedDetails.OCCUPATION ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("Dependent", updatedDetails.DEPENDENT ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ChgUser", updatedDetails.CHG_USER ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ChgDate", updatedDetails.CHG_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ChgTerm", updatedDetails.CHG_TERM ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PhysicalStatus", updatedDetails.PHYSICAL_STATUS ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("QlfCode", updatedDetails.QLF_CODE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("Adopted", updatedDetails.ADOPTED ?? (object)DBNull.Value));
                    /*command.Parameters.Add(new OracleParameter("PfNominee", updatedDetails.PF_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PfPerc", updatedDetails.PF_PERC ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("GratNominee", updatedDetails.GRAT_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("GratPerc", updatedDetails.GRAT_PERC ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PenNominee", updatedDetails.PEN_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PenPerc", updatedDetails.PEN_PERC ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("CompNominee", updatedDetails.COMP_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("CompPerc", updatedDetails.COMP_PERC ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("MediNominee", updatedDetails.MEDI_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("AccIncNominee", updatedDetails.ACC_INC_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("AccIncPerc", updatedDetails.ACC_INC_PERC ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("DeathNominee", updatedDetails.DEATH_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("DeathPerc", updatedDetails.DEATH_PERC ?? (object)DBNull.Value));*/
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));
                    command.Parameters.Add(new OracleParameter("SeqNo", seqNo));

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error updating employee family: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }


        // update employee family pension, pf, gratuity API
        /*public async Task<bool> UpdateEmpFamilyPensionPfGrat (string empCode, string seqNo, EmployeeFamilyNomineeUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                var commandText = @"
                        UPDATE EMPFAMILY
                        SET
                            PF_NOMINEE = :PfNominee,
                            PF_PERC = :PfPerc,
                            GRAT_NOMINEE = :GratNominee,
                            GRAT_PERC = :GratPerc,
                            PEN_NOMINEE = :PenNominee,
                            PEN_PERC = :PenPerc,
                            COMP_NOMINEE = :CompNominee,
                            COMP_PERC = :CompPerc,
                            MEDI_NOMINEE = :MediNominee,
                            ACC_INC_NOMINEE = :AccIncNominee,
                            ACC_INC_PERC = :AccIncPerc,
                            DEATH_NOMINEE = :DeathNominee,
                            DEATH_PERC = :DeathPerc                       
                        WHERE TRIM(EMP_CODE) = TRIM(:EmpCode) AND TRIM(SEQ_NO) = TRIM(:SeqNo)";

                // Create OracleCommand
                using (var command = new OracleCommand(commandText))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                    // Set parameters   
                    command.Parameters.Add(new OracleParameter("PfNominee", updatedDetails.PF_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PfPerc", updatedDetails.PF_PERC ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("GratNominee", updatedDetails.GRAT_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("GratPerc", updatedDetails.GRAT_PERC ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PenNominee", updatedDetails.PEN_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PenPerc", updatedDetails.PEN_PERC ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("CompNominee", updatedDetails.COMP_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("CompPerc", updatedDetails.COMP_PERC ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("MediNominee", updatedDetails.MEDI_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("AccIncNominee", updatedDetails.ACC_INC_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("AccIncPerc", updatedDetails.ACC_INC_PERC ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("DeathNominee", updatedDetails.DEATH_NOMINEE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("DeathPerc", updatedDetails.DEATH_PERC ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));
                    command.Parameters.Add(new OracleParameter("SeqNo", seqNo));

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error updating employee family: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }
*/


        public async Task<bool> UpdateEmpFamilyPensionPfGrat(string empCode, string seqNo, EmployeeFamilyNomineeUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                // Construct the base update command
                var commandText = new StringBuilder();
                commandText.Append("UPDATE EMPFAMILY SET ");

                // Collect parameters to update
                var parametersToUpdate = new List<OracleParameter>();

                // Append each parameter if updatedDetails contains a non-null value
                if (updatedDetails.PF_NOMINEE != null)
                {
                    commandText.Append("PF_NOMINEE = :PfNominee, ");
                    parametersToUpdate.Add(new OracleParameter("PfNominee", updatedDetails.PF_NOMINEE));
                }
                if (updatedDetails.PF_PERC != null)
                {
                    commandText.Append("PF_PERC = :PfPerc, ");
                    parametersToUpdate.Add(new OracleParameter("PfPerc", updatedDetails.PF_PERC));
                }
                if (updatedDetails.GRAT_NOMINEE != null)
                {
                    commandText.Append("GRAT_NOMINEE = :GratNominee, ");
                    parametersToUpdate.Add(new OracleParameter("GratNominee", updatedDetails.GRAT_NOMINEE));
                }
                if (updatedDetails.GRAT_PERC != null)
                {
                    commandText.Append("GRAT_PERC = :GratPerc, ");
                    parametersToUpdate.Add(new OracleParameter("GratPerc", updatedDetails.GRAT_PERC));
                }
                if (updatedDetails.PEN_NOMINEE != null)
                {
                    commandText.Append("PEN_NOMINEE = :PenNominee, ");
                    parametersToUpdate.Add(new OracleParameter("PenNominee", updatedDetails.PEN_NOMINEE));
                }
                if (updatedDetails.PEN_PERC != null)
                {
                    commandText.Append("PEN_PERC = :PenPerc, ");
                    parametersToUpdate.Add(new OracleParameter("PenPerc", updatedDetails.PEN_PERC));
                }
                if (updatedDetails.COMP_NOMINEE != null)
                {
                    commandText.Append("COMP_NOMINEE = :CompNominee, ");
                    parametersToUpdate.Add(new OracleParameter("CompNominee", updatedDetails.COMP_NOMINEE));
                }
                if (updatedDetails.COMP_PERC != null)
                {
                    commandText.Append("COMP_PERC = :CompPerc, ");
                    parametersToUpdate.Add(new OracleParameter("CompPerc", updatedDetails.COMP_PERC));
                }
                if (updatedDetails.MEDI_NOMINEE != null)
                {
                    commandText.Append("MEDI_NOMINEE = :MediNominee, ");
                    parametersToUpdate.Add(new OracleParameter("MediNominee", updatedDetails.MEDI_NOMINEE));
                }
                if (updatedDetails.ACC_INC_NOMINEE != null)
                {
                    commandText.Append("ACC_INC_NOMINEE = :AccIncNominee, ");
                    parametersToUpdate.Add(new OracleParameter("AccIncNominee", updatedDetails.ACC_INC_NOMINEE));
                }
                if (updatedDetails.ACC_INC_PERC != null)
                {
                    commandText.Append("ACC_INC_PERC = :AccIncPerc, ");
                    parametersToUpdate.Add(new OracleParameter("AccIncPerc", updatedDetails.ACC_INC_PERC));
                }
                if (updatedDetails.DEATH_NOMINEE != null)
                {
                    commandText.Append("DEATH_NOMINEE = :DeathNominee, ");
                    parametersToUpdate.Add(new OracleParameter("DeathNominee", updatedDetails.DEATH_NOMINEE));
                }
                if (updatedDetails.DEATH_PERC != null)
                {
                    commandText.Append("DEATH_PERC = :DeathPerc, ");
                    parametersToUpdate.Add(new OracleParameter("DeathPerc", updatedDetails.DEATH_PERC));
                }

                // Remove trailing comma and space
                if (commandText.Length > 0)
                {
                    commandText.Length -= 2; // Remove last ", "
                }

                // Append WHERE clause
                commandText.Append(" WHERE TRIM(EMP_CODE) = TRIM(:EmpCode) AND TRIM(SEQ_NO) = TRIM(:SeqNo)");

                // Create OracleCommand
                using (var command = new OracleCommand(commandText.ToString()))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                    // Set parameters
                    command.Parameters.AddRange(parametersToUpdate.ToArray());
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));
                    command.Parameters.Add(new OracleParameter("SeqNo", seqNo));

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error while updating family nominee: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        // Update employee family multiple request
        public async Task<bool> UpdateEmpFamilyPensionPfGrat(List<EmployeeFamilyNomineeUpdateMultipleRequest> updatedDetailsList)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                foreach (var updatedDetails in updatedDetailsList)
                {
                    // Construct the base update command
                    var commandText = new StringBuilder();
                    commandText.Append("UPDATE EMPFAMILY SET ");

                    // Collect parameters to update
                    var parametersToUpdate = new List<OracleParameter>();

                    // Append each parameter if updatedDetails contains a non-null value
                    if (updatedDetails.PF_NOMINEE != null)
                    {
                        commandText.Append("PF_NOMINEE = :PfNominee, ");
                        parametersToUpdate.Add(new OracleParameter("PfNominee", updatedDetails.PF_NOMINEE));
                    }
                    if (updatedDetails.PF_PERC != null)
                    {
                        commandText.Append("PF_PERC = :PfPerc, ");
                        parametersToUpdate.Add(new OracleParameter("PfPerc", updatedDetails.PF_PERC));
                    }
                    if (updatedDetails.GRAT_NOMINEE != null)
                    {
                        commandText.Append("GRAT_NOMINEE = :GratNominee, ");
                        parametersToUpdate.Add(new OracleParameter("GratNominee", updatedDetails.GRAT_NOMINEE));
                    }
                    if (updatedDetails.GRAT_PERC != null)
                    {
                        commandText.Append("GRAT_PERC = :GratPerc, ");
                        parametersToUpdate.Add(new OracleParameter("GratPerc", updatedDetails.GRAT_PERC));
                    }
                    if (updatedDetails.PEN_NOMINEE != null)
                    {
                        commandText.Append("PEN_NOMINEE = :PenNominee, ");
                        parametersToUpdate.Add(new OracleParameter("PenNominee", updatedDetails.PEN_NOMINEE));
                    }
                    if (updatedDetails.PEN_PERC != null)
                    {
                        commandText.Append("PEN_PERC = :PenPerc, ");
                        parametersToUpdate.Add(new OracleParameter("PenPerc", updatedDetails.PEN_PERC));
                    }
                    if (updatedDetails.COMP_NOMINEE != null)
                    {
                        commandText.Append("COMP_NOMINEE = :CompNominee, ");
                        parametersToUpdate.Add(new OracleParameter("CompNominee", updatedDetails.COMP_NOMINEE));
                    }
                    if (updatedDetails.COMP_PERC != null)
                    {
                        commandText.Append("COMP_PERC = :CompPerc, ");
                        parametersToUpdate.Add(new OracleParameter("CompPerc", updatedDetails.COMP_PERC));
                    }
                    if (updatedDetails.MEDI_NOMINEE != null)
                    {
                        commandText.Append("MEDI_NOMINEE = :MediNominee, ");
                        parametersToUpdate.Add(new OracleParameter("MediNominee", updatedDetails.MEDI_NOMINEE));
                    }
                    if (updatedDetails.ACC_INC_NOMINEE != null)
                    {
                        commandText.Append("ACC_INC_NOMINEE = :AccIncNominee, ");
                        parametersToUpdate.Add(new OracleParameter("AccIncNominee", updatedDetails.ACC_INC_NOMINEE));
                    }
                    if (updatedDetails.ACC_INC_PERC != null)
                    {
                        commandText.Append("ACC_INC_PERC = :AccIncPerc, ");
                        parametersToUpdate.Add(new OracleParameter("AccIncPerc", updatedDetails.ACC_INC_PERC));
                    }
                    if (updatedDetails.DEATH_NOMINEE != null)
                    {
                        commandText.Append("DEATH_NOMINEE = :DeathNominee, ");
                        parametersToUpdate.Add(new OracleParameter("DeathNominee", updatedDetails.DEATH_NOMINEE));
                    }
                    if (updatedDetails.DEATH_PERC != null)
                    {
                        commandText.Append("DEATH_PERC = :DeathPerc, ");
                        parametersToUpdate.Add(new OracleParameter("DeathPerc", updatedDetails.DEATH_PERC));
                    }
                    if (updatedDetails.CHG_USER != null)
                    {
                        commandText.Append("CHG_USER = :ChgUser, ");
                        parametersToUpdate.Add(new OracleParameter("ChgUser", updatedDetails.CHG_USER));
                    }
                    if (updatedDetails.CHG_TERM != null)
                    {
                        commandText.Append("CHG_TERM = :ChgTerm, ");
                        parametersToUpdate.Add(new OracleParameter("ChgTerm", updatedDetails.CHG_TERM));
                    }
                    if (updatedDetails.CHG_DATE != null)
                    {
                        commandText.Append("CHG_DATE = TO_DATE(:ChgDate, 'YYYY-MM-DD'), ");
                        parametersToUpdate.Add(new OracleParameter("ChgDate", updatedDetails.CHG_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));                     
                    }                  

                    // Remove trailing comma and space
                    if (commandText.Length > 0)
                    {
                        commandText.Length -= 2; // Remove last ", "
                    }

                    // Append WHERE clause
                    commandText.Append(" WHERE TRIM(EMP_CODE) = TRIM(:EmpCode) AND TRIM(SEQ_NO) = TRIM(:SeqNo)");

                    // Create OracleCommand
                    using (var command = new OracleCommand(commandText.ToString()))
                    {
                        command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                        command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                        // Set parameters
                        command.Parameters.AddRange(parametersToUpdate.ToArray());
                        command.Parameters.Add(new OracleParameter("EmpCode", updatedDetails.EMP_CODE));
                        command.Parameters.Add(new OracleParameter("SeqNo", updatedDetails.SEQ_NO));

                        // Log full SQL query with parameters
                        string fullQuery = GetFullQueryWithParameters(command);
                        Console.WriteLine($"Full SQL query: {fullQuery}");

                        // Execute the command
                        int affectedRows = await command.ExecuteNonQueryAsync();
                        Console.WriteLine($"Affected rows: {affectedRows}");

                        if (affectedRows != 1)
                        {
                            throw new Exception($"Expected to update one row, but {affectedRows} rows were affected.");
                        }
                    }
                }

                // Commit transaction
                await transaction.CommitAsync();

                // Log success
                Console.WriteLine("Transaction committed successfully");

                return true;
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error while updating family nominee: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }


        // Employee Qualification
        public async Task<bool> AddQualification(AddQualification newQualification)
        {
            try
            {
                       
                    // Get the current maximum SEQ_NO for the given EMP_CODE
                    var maxSeqNoStr = await _appDbContext.EMPQUAL
                        .Where(f => f.EMP_CODE.Trim() == newQualification.EMP_CODE.Trim())
                        .MaxAsync(f => f.SEQ_NO);


                    int maxSeqNo = 0;
                    if (!string.IsNullOrEmpty(maxSeqNoStr) && int.TryParse(maxSeqNoStr, out int parsedSeqNo))
                    {
                        maxSeqNo = parsedSeqNo;
                    }

                    // Increment the SEQ_NO by 1
                    int newSeqNo = maxSeqNo + 1;
                    string newSeqNoStr = newSeqNo.ToString();


                    string formattedExpCompDate = newQualification.EXP_COMP_DATE?.ToString("MM-dd-yy");
                    string formattedFromDate = newQualification.FROM_DATE?.ToString("MM-dd-yy");
                    string formattedToDate = newQualification.TO_DATE?.ToString("MM-dd-yy");
                    // if not nullable =>  string formattedDate2 = newQualification.TO_DATE.ToString("MM-dd-yy");

                    var QualificationEntity = new AddQualification
                    {
                        EMP_CODE = newQualification.EMP_CODE,
                        SEQ_NO = newSeqNoStr,
                        QLF_CODE = newQualification.QLF_CODE,
                        INSTITUTE = newQualification.INSTITUTE,
                        SUBJECT1 = newQualification.SUBJECT1,
                        SUBJECT2 = newQualification.SUBJECT2,
                        SUBJECT3 = newQualification.SUBJECT3,
                        SUBJECT4 = newQualification.SUBJECT4,
                        PASS_YEAR = newQualification.PASS_YEAR,
                        CLASS = newQualification.CLASS,
                        PERC = newQualification.PERC,
                        UNIVERSITY = newQualification.UNIVERSITY,
                        DEGREE_NAME = newQualification.DEGREE_NAME,
                        REMARKS = newQualification.REMARKS,
                        STATUS_TYPE = newQualification.STATUS_TYPE,
                        EXP_COMP_DATE = string.IsNullOrEmpty(formattedExpCompDate)
                            ? (DateTime?)null
                            : DateTime.ParseExact(formattedExpCompDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                        FROM_DATE = string.IsNullOrEmpty(formattedFromDate)
                            ? (DateTime?)null
                            : DateTime.ParseExact(formattedFromDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                        TO_DATE = string.IsNullOrEmpty(formattedToDate)
                            ? (DateTime?)null
                            : DateTime.ParseExact(formattedToDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                        //TO_DATE = DateTime.ParseExact(formattedDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                    };

                    // Add the qualification to the context and save changes
                    await _appDbContext.EMPQUAL.AddAsync(QualificationEntity);
                    await _appDbContext.SaveChangesAsync();

                    return true;
                
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error getting qualification: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        //  Update employee qualification API
        public async Task<bool> UpdateEmpQualification(string empCode, string seqNo, EmployeeQualificationUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                var commandText = @"
                UPDATE EMPQUAL
                SET
                    QLF_CODE  = :QlfCode,
                    INSTITUTE  = :institute,
                    SUBJECT1  = :sub1,
                    SUBJECT2  = :sub2,
                    SUBJECT3  = :sub3,
                    SUBJECT4  = :sub4,
                    PASS_YEAR  = :PassYear,
                    CLASS  = :class,
                    PERC  = :perc,
                    UNIVERSITY  = :university,
                    DEGREE_NAME  = :DegreeName,
                    REMARKS  = :Remarks,
                    STATUS_TYPE  = :StatusType,
                    EXP_COMP_DATE = TO_DATE(:ExpCompletionDate, 'YYYY-MM-DD'),
                    FROM_DATE = TO_DATE(:FromDate, 'YYYY-MM-DD'),
                    TO_DATE = TO_DATE(:ToDate, 'YYYY-MM-DD')
                WHERE TRIM(EMP_CODE) = TRIM(:EmpCode) AND TRIM(SEQ_NO) = TRIM(:SeqNo)";

                // Create OracleCommand
                using (var command = new OracleCommand(commandText))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                    // Set parameters   
                    command.Parameters.Add(new OracleParameter("QlfCode", updatedDetails.QLF_CODE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("institute", updatedDetails.INSTITUTE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("sub1", updatedDetails.SUBJECT1 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("sub2", updatedDetails.SUBJECT2 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("sub3", updatedDetails.SUBJECT3 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("sub4", updatedDetails.SUBJECT4 ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("PassYear", updatedDetails.PASS_YEAR ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("class", updatedDetails.CLASS ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("perc", updatedDetails.PERC ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("university", updatedDetails.UNIVERSITY ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("DegreeName", updatedDetails.DEGREE_NAME ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("Remarks", updatedDetails.REMARKS ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("StatusType", updatedDetails.STATUS_TYPE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ExpCompletionDate", updatedDetails.EXP_COMP_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("FromDate", updatedDetails.FROM_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ToDate", updatedDetails.TO_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));
                    command.Parameters.Add(new OracleParameter("SeqNo", seqNo));

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error updating employee family: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }



        // Employee Experience
        public async Task<bool> AddExperience(AddExperience newexperience)
        {
            try
            {
                string formattedFromDate = newexperience.FROM_DATE.ToString("MM-dd-yy");
                string formattedToDate = newexperience.TO_DATE?.ToString("MM-dd-yy");
                string formattedChangeDate = newexperience.CHG_DATE?.ToString("MM-dd-yy");
                // if not nullable =>  string formattedDate2 = newQualification.TO_DATE.ToString("MM-dd-yy");

                var ExperienceEntity = new AddExperience
                {
                    EMP_CODE = newexperience.EMP_CODE,
                    ORGANISATION = newexperience.ORGANISATION,
                    DESIGNATION = newexperience.DESIGNATION,
                    FROM_DATE = DateTime.ParseExact(formattedFromDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                    TO_DATE = string.IsNullOrEmpty(formattedToDate)
                        ? (DateTime?)null
                        : DateTime.ParseExact(formattedToDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                    GROSS_AMT = newexperience.GROSS_AMT,
                    CURR_CODE = newexperience.CURR_CODE,
                    CHG_USER = newexperience.CHG_USER,
                    CHG_DATE = string.IsNullOrEmpty(formattedChangeDate)
                        ? (DateTime?)null
                        : DateTime.ParseExact(formattedChangeDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                    CHG_TERM = newexperience.CHG_TERM,
                    DEPARTMENT = newexperience.DEPARTMENT,
                    REMARKS = newexperience.REMARKS,
                    COUNTRY = newexperience.COUNTRY,
                    STATE = newexperience.STATE,
                    JOIN_DESIGNATION = newexperience.JOIN_DESIGNATION,
                    JOIN_GROSS_AMT = newexperience.JOIN_GROSS_AMT,
                    SEP_REASON = newexperience.SEP_REASON,
                    WORK_LOCATION = newexperience.WORK_LOCATION,
                };

                // Add the qualification to the context and save changes
                await _appDbContext.EMPEXPERIENCE.AddAsync(ExperienceEntity);
                await _appDbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error getting experience: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        //  Update employee Experience API
        public async Task<bool> UpdateEmpExperience(string empCode, DateTime fromDate, EmployeeExperienceUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                var commandText = @"
                UPDATE EMPEXPERIENCE
                SET
                    ORGANISATION = :organisation,
                    DESIGNATION = :designation,
                    FROM_DATE= TO_DATE(:FromDate, 'YYYY-MM-DD'),
                    TO_DATE= TO_DATE(:ToDate, 'YYYY-MM-DD'),
                    GROSS_AMT = :GrossAmt,
                    CURR_CODE = :CurrCode,
                    CHG_USER = :ChgUser,
                    CHG_DATE = TO_DATE(:ChgDate, 'YYYY-MM-DD'),
                    CHG_TERM = :ChgTerm,
                    DEPARTMENT = :dept,                  
                    COUNTRY = :country,
                    STATE = :state,
                    WORK_LOCATION = :WorkLocation
                WHERE TRIM(EMP_CODE) = TRIM(:EmpCode) AND TRIM(FROM_DATE) = TRIM(TO_DATE(:FromDateCondition, 'YYYY-MM-DD'))";
                    
                   /* REMARKS = :remarks,
                    JOIN_DESIGNATION = :JoinDesg,
                    JOIN_GROSS_AMT = :JoinGross,
                    SEP_REASON = :SepReason,*/


                // Create OracleCommand
                using (var command = new OracleCommand(commandText))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction(); // Corrected method to get the underlying OracleTransaction

                    // Set parameters   
                    command.Parameters.Add(new OracleParameter("organisation", updatedDetails.ORGANISATION ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("designation", updatedDetails.DESIGNATION ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("FromDate", updatedDetails.FROM_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ToDate", updatedDetails.TO_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("GrossAmt", updatedDetails.GROSS_AMT ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("CurrCode", updatedDetails.CURR_CODE ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ChgUser", updatedDetails.CHG_USER ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ChgDate", updatedDetails.CHG_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));                 
                    command.Parameters.Add(new OracleParameter("ChgTerm", updatedDetails.CHG_TERM ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("dept", updatedDetails.DEPARTMENT ?? (object)DBNull.Value));
                    //command.Parameters.Add(new OracleParameter("remarks", updatedDetails.REMARKS ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("country", updatedDetails.COUNTRY ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("state", updatedDetails.STATE ?? (object)DBNull.Value));
                    //command.Parameters.Add(new OracleParameter("JoinDesg", updatedDetails.JOIN_DESIGNATION ?? (object)DBNull.Value));
                    //command.Parameters.Add(new OracleParameter("JoinGross", updatedDetails.JOIN_GROSS_AMT ?? (object)DBNull.Value));
                    //command.Parameters.Add(new OracleParameter("SepReason", updatedDetails.SEP_REASON ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("WorkLocation", updatedDetails.WORK_LOCATION ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));
                    command.Parameters.Add(new OracleParameter("FromDateCondition", fromDate.ToString("yyyy-MM-dd")));

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error updating employee family: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }


        // Employee Assets
        // Dropdown for asset

        public async Task<List<EmployeeGetAssetsResponse>> ddl_GetAsstes()
        {
            var assets = new List<EmployeeGetAssetsResponse>();

            try
            {

                var sqlQuery = @" select * from INDUCTION where INDUCT_TBLNO='A0001'";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var asset = new EmployeeGetAssetsResponse
                                {
                                    INDUCT_CODE = reader["INDUCT_CODE"].ToString(),
                                    DESCR = reader["DESCR"].ToString()
                                };

                                assets.Add(asset);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving Proc_Group", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting assets: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return assets;
        }

        // Add Employee Assets
        /* public async Task<bool> AddAssets(AddAssets newassets)
         {
             try
             {
                 string formattedChangeDate = newassets.CHG_DATE.ToString("MM-dd-yy");
                 string formattedDateFormality = newassets.DATE_FORMALITY?.ToString("MM-dd-yy");
                 string formattedStatusDate = newassets.STATUS_DATE?.ToString("MM-dd-yy");
                 // if not nullable =>  string formattedDate2 = newQualification.TO_DATE.ToString("MM-dd-yy");

                 var AssetsEntity = new AddAssets
                 {
                     EMP_CODE = newassets.EMP_CODE,
                     INDUCT_CODE = newassets.INDUCT_CODE,
                     DATE_FORMALITY = string.IsNullOrEmpty(formattedDateFormality)
                         ? (DateTime?)null
                         : DateTime.ParseExact(formattedDateFormality, "MM-dd-yy", CultureInfo.InvariantCulture),
                     REMARKS = newassets.REMARKS,
                     CHG_DATE = DateTime.ParseExact(formattedChangeDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                     CHG_USER = newassets.CHG_USER,
                     CHG_TERM = newassets.CHG_TERM,
                     TRAN_ID = newassets.TRAN_ID,
                     STATUS = newassets.STATUS,
                     STATUS_DATE = string.IsNullOrEmpty(formattedStatusDate)
                         ? (DateTime?)null
                         : DateTime.ParseExact(formattedStatusDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                     TOT_AMT = newassets.TOT_AMT,
                     ADJ_AMT = newassets.ADJ_AMT,
                 };

                 // Add the qualification to the context and save changes
                 await _appDbContext.EMPINDUCTION.AddAsync(AssetsEntity);
                 await _appDbContext.SaveChangesAsync();

                 return true;
             }
             catch (Exception ex)
             {
                 string str = ex.GetType().ToString();
                 if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                 {
                     string error = ex.InnerException?.Message ?? ex.Message;
                     throw new RepositoryException(error);
                 }

                 string errorMessage = $"Error getting assets: {ex.Message}";
                 if (ex.InnerException != null)
                 {
                     errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                 }

                 throw new Exception(errorMessage, ex.InnerException);
             }
         }*/




        public async Task<bool> AddAssets(AddAssets newassets)
        {
            try
            {
                string formattedChangeDate = newassets.CHG_DATE.ToString("MM-dd-yy");
                string formattedDateFormality = newassets.DATE_FORMALITY?.ToString("MM-dd-yy");
                string formattedStatusDate = newassets.STATUS_DATE?.ToString("MM-dd-yy");

                using (var connection = _appDbContext.GetConn())
                {
                    await connection.OpenAsync();

                    // Check for duplicate INDUCT_CODE
                    //var isDuplicate = await CheckDuplicateInductCode(connection, newassets.INDUCT_CODE, newassets.EMP_CODE);
                 
                        // Get the current maximum TRAN_ID for the given EMP_CODE
                        var maxtranidStr = await _appDbContext.EMPINDUCTION
                            .Where(f => f.EMP_CODE.Trim() == newassets.EMP_CODE.Trim() &&
                                   f.INDUCT_CODE.Trim() == newassets.INDUCT_CODE.Trim())
                            .MaxAsync(f => f.TRAN_ID);

                        int maxTranId = 0000000000;
                        if (!string.IsNullOrEmpty(maxtranidStr) && int.TryParse(maxtranidStr, out int parsedSeqNo))
                        {
                            maxTranId = parsedSeqNo;
                        }

                        // Increment the TRAN_ID by 1
                        int newTranId = maxTranId + 1;
                        string newTranIdStr = newTranId.ToString("D10");

                    var AssetsEntity = new AddAssets
                    {
                        EMP_CODE = newassets.EMP_CODE,
                        INDUCT_CODE = newassets.INDUCT_CODE,
                        DATE_FORMALITY = string.IsNullOrEmpty(formattedDateFormality)
                            ? (DateTime?)null
                            : DateTime.ParseExact(formattedDateFormality, "MM-dd-yy", CultureInfo.InvariantCulture),
                        REMARKS = newassets.REMARKS,
                        CHG_DATE = DateTime.ParseExact(formattedChangeDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                        CHG_USER = newassets.CHG_USER,
                        CHG_TERM = newassets.CHG_TERM,
                        TRAN_ID = newTranIdStr,
                        STATUS = newassets.STATUS,
                        STATUS_DATE = string.IsNullOrEmpty(formattedStatusDate)
                            ? (DateTime?)null
                            : DateTime.ParseExact(formattedStatusDate, "MM-dd-yy", CultureInfo.InvariantCulture),
                        TOT_AMT = newassets.TOT_AMT,
                        ADJ_AMT = newassets.ADJ_AMT,
                    };

                    // Add the asset to the context and save changes
                    await _appDbContext.EMPINDUCTION.AddAsync(AssetsEntity);
                    await _appDbContext.SaveChangesAsync();
                }

                return true;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error getting assets: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        private async Task<bool> CheckDuplicateInductCode(OracleConnection connection, string inductCode, string empCode)
        {
            string sqlQuery = "SELECT COUNT(*) FROM EMPINDUCTION WHERE INDUCT_CODE = :inductCode AND EMP_CODE = :empCode";
            using (var command = new OracleCommand(sqlQuery, connection))
            {
                command.Parameters.Add("inductCode", OracleDbType.Char).Value = inductCode;
                command.Parameters.Add("empCode", OracleDbType.Char).Value = empCode;
                var count = Convert.ToInt32(await command.ExecuteScalarAsync());
                return count > 0;
            }
        }

        private async Task<string> GenerateTranId(OracleConnection connection, bool isDuplicate)
        {
            string sqlQuery = isDuplicate
                ? "SELECT LPAD(tran_id_seq.NEXTVAL, 10, '0') FROM dual"
                : "SELECT LPAD(tran_id_seq.CURRVAL, 10, '0') FROM dual";

            using (var command = new OracleCommand(sqlQuery, connection))
            {
                return (await command.ExecuteScalarAsync()).ToString();
            }
        }


        // Update Employee Assets
        public async Task<bool> UpdateEmpInduction(string empCode, string inductCode, string tranId, EmployeeAssetsUpdateRequest updatedDetails)
        {
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = null;

            try
            {
                // Begin transaction
                transaction = await _appDbContext.Database.BeginTransactionAsync();

                var commandText = @"
            UPDATE EMPINDUCTION
            SET
                DATE_FORMALITY = TO_DATE(:DateFormality, 'YYYY-MM-DD'),
                REMARKS = :Remarks,
                CHG_USER = :ChgUser,
                CHG_DATE = TO_DATE(:ChgDate, 'YYYY-MM-DD'),
                CHG_TERM = :ChgTerm,
                STATUS = :Status,
                STATUS_DATE = TO_DATE(:StatDate, 'YYYY-MM-DD'),
                TOT_AMT = :TotAmt,
                ADJ_AMT = :AdjAmt
            WHERE 
                TRIM(EMP_CODE) = TRIM(:EmpCode) AND 
                TRIM(INDUCT_CODE) = TRIM(:InductCode) AND
                TRIM(TRAN_ID) = TRIM(:TranId)
                ";
                /*(
                    (SELECT COUNT(*) FROM EMPINDUCTION WHERE TRIM(INDUCT_CODE) = TRIM(:InductCode) AND TRIM(EMP_CODE) = TRIM(:EmpCode)) = 1
                    OR TRIM(TRAN_ID) = TRIM(:TranId)
                )*/

                // Create OracleCommand
                using (var command = new OracleCommand(commandText))
                {
                    command.Connection = (OracleConnection)_appDbContext.Database.GetDbConnection();
                    command.Transaction = (OracleTransaction)transaction.GetDbTransaction();

                    // Set parameters
                    command.Parameters.Add(new OracleParameter("DateFormality", updatedDetails.DATE_FORMALITY?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("Remarks", updatedDetails.REMARKS ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ChgUser", updatedDetails.CHG_USER ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ChgDate", updatedDetails.CHG_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("ChgTerm", updatedDetails.CHG_TERM ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("Status", updatedDetails.STATUS ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("StatDate", updatedDetails.STATUS_DATE?.ToString("yyyy-MM-dd") ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("TotAmt", updatedDetails.TOT_AMT ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("AdjAmt", updatedDetails.ADJ_AMT ?? (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("EmpCode", empCode));
                    command.Parameters.Add(new OracleParameter("InductCode", inductCode));
                    command.Parameters.Add(new OracleParameter("TranId", tranId));

                    // Log full SQL query with parameters
                    string fullQuery = GetFullQueryWithParameters(command);
                    Console.WriteLine($"Full SQL query: {fullQuery}");

                    // Execute the command
                    int affectedRows = await command.ExecuteNonQueryAsync();
                    Console.WriteLine($"Affected rows: {affectedRows}");

                    // Commit transaction
                    await transaction.CommitAsync();

                    // Log success
                    Console.WriteLine("Transaction committed successfully");

                    return affectedRows == 1; // Ensure exactly one row is affected
                }
            }
            catch (Exception ex)
            {
                // Rollback transaction if any error occurs
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error updating employee induction: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }



        // Update Employee 

        /*public async Task<bool> AddEmployyeDetChange(Employee_Det_Change newemployee)
        {
            try
            {
                var EmployeeEntity = new Employee_Det_Change
                {
                    TRAN_ID = newemployee.TRAN_ID,
                    EMP_CODE = newemployee.EMP_CODE,

                   
                };

                // Add the qualification to the context and save changes
                await _appDbContext.EMPLOYEE_DET_CHANGE.AddAsync(EmployeeEntity);
                await _appDbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error getting assets: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }*/



        // To see what getting passed in the sql query (full query)
        public static string GetFullQueryWithParameters(string query, params OracleParameter[] parameters)
        {
            foreach (var parameter in parameters)
            {
                string parameterValue = GetParameterFormattedValue(parameter);
                query = query.Replace(':' + parameter.ParameterName, parameterValue);
            }
            return query;
        }

        public static string GetFullQueryWithParameters(OracleCommand command)
        {
            string fullQuery = command.CommandText;

            foreach (OracleParameter parameter in command.Parameters)
            {
                string parameterValue = GetParameterFormattedValue(parameter);
                fullQuery = fullQuery.Replace(':' + parameter.ParameterName, parameterValue);
            }
            return fullQuery;
        }

        private static string GetParameterFormattedValue(OracleParameter parameter)
        {
            if (parameter.Value == DBNull.Value)
                return "NULL";

            if (parameter.DbType == System.Data.DbType.String || parameter.DbType == System.Data.DbType.Date)
                return $"'{parameter.Value}'";

            return parameter.Value.ToString();
        }



        // -------------------------------------------------------------------------------------------

        public async Task<string> SaveDataAndFileUpload(List<string> lstData, List<IFormFile> arrFile, string strFilePath, List<string> strFileName = null, Dictionary<string, Image> dictImages = null)
        {
            string[] strTranID = new string[999999];
            try
            {
                int iRec = 0, iRecTranID = 0, iQryExecCnt = 0;
                string strErrMsg = "";

                if (lstData.Count > 0)
                {
                    using (var conn = new OracleConnection(_configuration.GetConnectionString("DefaultConnection")))
                    {
                        await conn.OpenAsync();
                        using (var transaction = conn.BeginTransaction())
                        {
                            try
                            {
                                foreach (var item in lstData)
                                {
                                    string strQry = "";
                                    string qry = item;

                                    if (qry.Contains("fnGenerateTranID") || qry.Contains("fnUpdateTranId"))
                                    {
                                        string strMethodWithParam = qry.Replace("'", "");
                                        string strMethod = strMethodWithParam.Split(':')[0];
                                        string strParams = strMethodWithParam.Split(':')[1];
                                        string[] arrParams = strParams.Split(',');

                                        if (qry.Contains("fnGenerateTranID"))
                                        {
                                            strTranID[iRecTranID] = fnGenerateTranID(arrParams[0], arrParams[1], arrParams[2], arrParams[3], Convert.ToInt32(arrParams[4]));
                                            iRec = iRec + 1;
                                            iRecTranID++;
                                        }
                                        else if (qry.Contains("fnUpdateTranId"))
                                        {
                                            if (fnUpdateTranId(arrParams[0], arrParams[1], arrParams[2], arrParams[3], arrParams[4], Convert.ToInt32(arrParams[5]), strTranID[iRecTranID - 1]))
                                            {
                                                iRec = iRec + 1;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (qry.Contains("<<TRAN_ID>>") && !string.IsNullOrEmpty(strTranID[iRecTranID - 1]))
                                        {
                                            strQry = qry.Replace("<<TRAN_ID>>", strTranID[iRecTranID - 1]);
                                            if (qry.Contains("<<TRAN_ID_REF_"))
                                            {
                                                Int16 iTranSeq = Convert.ToInt16(qry.Substring(qry.IndexOf("<<TRAN_ID_REF_", 0) + 14, (qry.IndexOf("_>>", 0) - (qry.IndexOf("<<TRAN_ID_REF_", 0) + 14))));
                                                strQry = strQry.Replace("<<TRAN_ID_REF_" + iTranSeq + "_>>", strTranID[iTranSeq]);
                                            }
                                        }
                                        else if (qry.Contains("<<TRAN_ID_REF_"))
                                        {
                                            strQry = qry;
                                            for (int i = 0; i < iRecTranID; i++)
                                            {
                                                if (strQry.Contains("<<TRAN_ID_REF_" + i + "_>>"))
                                                {
                                                    strQry = strQry.Replace("<<TRAN_ID_REF_" + i + "_>>", strTranID[i]);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            strQry = qry;
                                        }

                                        if (strQry.Contains("<<XMLFilePath>>"))
                                        {
                                            strQry = strQry.Replace("<<XMLFilePath>>", _configuration["XmlFilePath"].Remove(0, 3));
                                        }

                                        using (var cmd = new OracleCommand(strQry, conn))
                                        {
                                            cmd.Transaction = transaction;

                                            int iRowExists = 0;
                                            if (qry.StartsWith("DELETE"))
                                            {
                                                cmd.CommandText = strQry.Replace("DELETE FROM", "SELECT COUNT(1) FROM");
                                                iRowExists = Convert.ToInt16(await cmd.ExecuteScalarAsync());
                                                if (iRowExists > 0)
                                                {
                                                    iRowExists = 0;
                                                }
                                                else
                                                {
                                                    iRowExists = 1;
                                                }
                                            }
                                            else if (qry.StartsWith("UPDATE"))
                                            {
                                                string s_WhereCond = strQry.Substring(strQry.IndexOf("WHERE"));
                                                cmd.CommandText = strQry.Replace("UPDATE", "SELECT COUNT(1) FROM ").Remove(strQry.Replace("UPDATE", "SELECT COUNT(1) FROM ").IndexOf("SET")) + s_WhereCond;
                                                iRowExists = Convert.ToInt16(await cmd.ExecuteScalarAsync());
                                                if (iRowExists > 0)
                                                {
                                                    iRowExists = 0;
                                                }
                                                else
                                                {
                                                    iRowExists = 1;
                                                }
                                            }

                                            cmd.CommandText = strQry;
                                            AddImageParameters(cmd, strQry, dictImages);

                                            int iRowsAffected = await cmd.ExecuteNonQueryAsync();
                                            cmd.Parameters.Clear();
                                            if (iRowsAffected > 0)
                                            {
                                                iRec = iRec + 1;
                                            }
                                            else
                                            {
                                                strErrMsg += " Index: " + iQryExecCnt;
                                                iRec = iRec + iRowExists;
                                                LogFile("SAVE (fnSaveDataAndFileUpload)", "Rows not affected", " Index: " + iQryExecCnt + Environment.NewLine + strQry, _configuration["LogFilePath"]);
                                            }
                                            iQryExecCnt = iQryExecCnt + 1;
                                        }
                                    }
                                }
                                if (iRec == lstData.Count)
                                {
                                    if (arrFile != null)
                                    {
                                        if (string.IsNullOrEmpty(strFilePath))
                                        {
                                            strFilePath = _configuration["XmlFilePath"];
                                        }

                                        for (int k = 0; k < arrFile.Count; k++)
                                        {
                                            if (arrFile[k] != null && arrFile[k].Length > 0)
                                            {
                                                if (strFileName[k].Contains("AUTO_FILE_NAME"))
                                                {
                                                    if (!string.IsNullOrEmpty(strTranID[0]))
                                                        strFileName[k] = strFileName[k].Replace("AUTO_FILE_NAME", strTranID[0]);
                                                    else
                                                    {
                                                        transaction.Rollback();
                                                        return "";
                                                    }
                                                }
                                                if (strFileName[k].Contains("<<TRAN_ID_REF_0_>>"))
                                                {
                                                    strFileName[k] = strFileName[k].Replace("<<TRAN_ID_REF_0_>>", strTranID[0]);
                                                }
                                                else if (strFileName[k].Contains("<<TRAN_ID>>"))
                                                {
                                                    strFileName[k] = strFileName[k].Replace("<<TRAN_ID>>", strTranID[0]);
                                                }
                                                var path = Path.Combine(strFilePath, strFileName[k]);
                                                using (var stream = new FileStream(path, FileMode.Create))
                                                {
                                                    await arrFile[k].CopyToAsync(stream);
                                                }
                                            }
                                        }
                                    }
                                    if (string.IsNullOrEmpty(strTranID[0]))
                                    {
                                        strTranID[0] = iRec.ToString();
                                    }
                                    transaction.Commit();
                                    return strTranID[0];
                                }
                                else
                                {
                                    transaction.Rollback();
                                    LogFile("SAVE (fnSaveDataAndFileUpload)", "Error", "Transaction Failure at " + strErrMsg, _configuration["LogFilePath"]);
                                    return "Error: Transaction Failure at " + strErrMsg;
                                }
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                LogFile("SAVE (fnSaveDataAndFileUpload)", "Error", ex.Message, _configuration["LogFilePath"]);
                                return "Error: " + ex.Message;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
            return strTranID[0];
        }

        public string fnGenerateTranID(string strSiteCode, string strDate, string strRefSer, string strRefPrefix, int strSeqLength = 5)
        {
            try
            {
                int strSeqLen = Convert.ToInt32(strSeqLength);
                string strTranId = "";
                string Ls_Site_Doc_Prefix = "";
                string Ls_Prd_Doc_Prefix = "";
                string Ls_Ref_Prefix = "";
                string Ls_New_Seq_No = "";
                string Qry = "";

                using (var conn = new OracleConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();

                    if (!string.IsNullOrEmpty(strSiteCode))
                    {
                        Qry = "SELECT Doc_prefix FROM Site WHERE site_code = :siteCode";
                        using (var cmd = new OracleCommand(Qry, conn))
                        {
                            cmd.Parameters.Add("siteCode", OracleDbType.Varchar2).Value = strSiteCode;
                            Ls_Site_Doc_Prefix = Convert.ToString(cmd.ExecuteScalar());
                        }
                    }

                    if (!string.IsNullOrEmpty(strDate))
                    {
                        Qry = "SELECT Trim(Doc_prefix) FROM Period WHERE fr_date <= SYSDATE AND to_date >= SYSDATE";
                        using (var cmd = new OracleCommand(Qry, conn))
                        {
                            Ls_Prd_Doc_Prefix = Convert.ToString(cmd.ExecuteScalar());
                        }
                    }

                    Ls_Ref_Prefix = Ls_Site_Doc_Prefix + strRefPrefix + Ls_Prd_Doc_Prefix;

                    if (!string.IsNullOrEmpty(strSiteCode) && !string.IsNullOrEmpty(strDate) && (Ls_Ref_Prefix.Length + strSeqLen > 10))
                    {
                        return "Invalid Site Period And Sequence Combination!!!";
                    }

                    Qry = "SELECT Count(1) FROM RefSeq WHERE Ref_Ser = :refSer";
                    if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                    {
                        Qry += " AND Trim(Ref_Prefix) = :refPrefix";
                    }
                    using (var cmd = new OracleCommand(Qry, conn))
                    {
                        cmd.Parameters.Add("refSer", OracleDbType.Varchar2).Value = strRefSer;
                        if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                        {
                            cmd.Parameters.Add("refPrefix", OracleDbType.Varchar2).Value = Ls_Ref_Prefix;
                        }

                        int iRowsAffected = Convert.ToInt32(cmd.ExecuteScalar() ?? 0);
                        if (iRowsAffected == 0)
                        {
                            Qry = "INSERT INTO Refseq VALUES (:refSer, :refPrefix, LPAD('0', :seqLen, '0'), SYSDATE, 'MAC', :remoteAddr)";
                            using (var cmdInsert = new OracleCommand(Qry, conn))
                            {
                                cmdInsert.Parameters.Add("refSer", OracleDbType.Varchar2).Value = strRefSer;
                                cmdInsert.Parameters.Add("refPrefix", OracleDbType.Varchar2).Value = Ls_Ref_Prefix;
                                cmdInsert.Parameters.Add("seqLen", OracleDbType.Int32).Value = strSeqLen;
                                cmdInsert.Parameters.Add("remoteAddr", OracleDbType.Varchar2).Value = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

                                iRowsAffected = cmdInsert.ExecuteNonQuery();
                                if (iRowsAffected == 0)
                                {
                                    return "Error Inserting TranID!!!";
                                }
                            }
                        }
                        else if (iRowsAffected > 1)
                        {
                            return "Please Mention Ref Prefix!!!";
                        }
                    }

                    Qry = "SELECT Length(Trim(Seq_No)) AS LEN, LPad(Seq_No + 1, :seqLen, '0') AS SEQ FROM RefSeq WHERE Trim(Ref_Ser) = :refSer";
                    if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                    {
                        Qry += " AND Trim(Ref_Prefix) = :refPrefix";
                    }
                    using (var cmd = new OracleCommand(Qry, conn))
                    {
                        cmd.Parameters.Add("refSer", OracleDbType.Varchar2).Value = strRefSer;
                        if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                        {
                            cmd.Parameters.Add("refPrefix", OracleDbType.Varchar2).Value = Ls_Ref_Prefix;
                        }
                        cmd.Parameters.Add("seqLen", OracleDbType.Int32).Value = strSeqLen;

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                if (Convert.ToInt32(reader["LEN"]) != strSeqLength)
                                {
                                    return "Existing Sequence Length Is Not Equal To Provided Length!!!";
                                }
                                else
                                {
                                    Ls_New_Seq_No = Convert.ToString(reader["SEQ"]).Trim();
                                }
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(Ls_New_Seq_No))
                    {
                        return "No Next Sequence Found!!!";
                    }

                    strTranId = Ls_Ref_Prefix + Ls_New_Seq_No;
                    return strTranId;
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
                throw;
            }
        }


        public bool fnUpdateTranId(string strSiteCode, string strEmpCode, string strDate, string strRefSer, string strRefPrefix, int strSeqLen, string strTranID)
        {
            try
            {
                string Qry = "";
                using (var conn = new OracleConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();
                    if (strSiteCode != "" && strDate != "" && strSeqLen == 5)
                    {
                        Qry = "UPDATE REFSEQ SET SEQ_NO = '" + strTranID.Substring(5, 5) + "', CHG_DATE = SYSDATE, CHG_USER = '" + strEmpCode + "', " +
                            "CHG_TERM='" + _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() + "' WHERE REF_SER = '" + strRefSer + "' And Trim(REF_PREFIX) = '" + strTranID.Substring(0, 5) + "' ";
                    }
                    else if (strRefPrefix == "")
                    {
                        Qry = "UPDATE REFSEQ SET SEQ_NO = '" + strTranID + "', CHG_DATE = SYSDATE, CHG_USER = '" + strEmpCode + "', " +
                            "CHG_TERM='" + _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() + "' WHERE REF_SER = '" + strRefSer + "' ";
                    }
                    else if (strRefPrefix != "")
                    {
                        Qry = "UPDATE REFSEQ SET SEQ_NO = '" + strTranID.Substring(strTranID.Length - strSeqLen) + "', CHG_DATE = SYSDATE, CHG_USER = '" + strEmpCode + "', " +
                            "CHG_TERM='" + _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() + "' WHERE REF_SER = '" + strRefSer + "' And Trim(REF_PREFIX) = '" + strTranID.Substring(0, (strTranID.Length - strSeqLen)) + "' ";
                    }

                    if (Qry != "")
                    {
                        using (var cmd = new OracleCommand(Qry, conn))
                        {
                            int iRowsAffected = cmd.ExecuteNonQuery();
                            if (iRowsAffected == 0)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        private void LogFile(string sLogTitle, string sLogType, string sLogContent, string sLogFilePath)
        {
            string sLogFileName = "Log" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
            using (StreamWriter writer = new StreamWriter(Path.Combine(sLogFilePath, sLogFileName), true))
            {
                writer.WriteLine(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + " | " + sLogType + " | " + sLogTitle + " | " + sLogContent);
            }
        }

        private void AddImageParameters(OracleCommand cmd, string query, Dictionary<string, Image> dictImages)
        {
            if (dictImages != null)
            {
                foreach (var imageKey in dictImages.Keys)
                {
                    if (query.Contains(imageKey))
                    {
                        byte[] imageBytes = ConvertImageToByteArray(dictImages[imageKey]);
                        OracleParameter param = new OracleParameter(imageKey, OracleDbType.Blob);
                        param.Value = imageBytes;
                        cmd.Parameters.Add(param);
                    }
                }
            }
        }

        private byte[] ConvertImageToByteArray(Image image)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Jpeg); // You can change the format based on your requirements
                return ms.ToArray();
            }
        }
    }
}


