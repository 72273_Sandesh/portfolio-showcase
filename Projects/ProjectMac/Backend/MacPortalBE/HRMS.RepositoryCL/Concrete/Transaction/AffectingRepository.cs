﻿using HRMS.ModelCL.Payloads.Employee;
using HRMS.ModelCL.Payloads.Transaction.Affecting;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Interface.Transaction;
using HRMS_Core.Data.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Collections.Specialized.BitVector32;

namespace HRMS.RepositoryCL.Concrete.Transaction
{
    public class AffectingRepository : IAffectingRepository
    {
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        private readonly IAutoGenerateCodeRepository _autoGenerateCodeRepository;
        private readonly ImageFetchingHelper _imageFetchingHelper;
        private readonly IDbConnection _dbConnection;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AffectingRepository(AppDbContext appContext, IConfiguration configuration, IAutoGenerateCodeRepository autoGenerateCodeRepository, ImageFetchingHelper imageFetchingHelper, IDbConnection dbConnection, IHttpContextAccessor httpContextAccessor)
        {
            _appDbContext = appContext;
            _configuration = configuration;
            _autoGenerateCodeRepository = autoGenerateCodeRepository;
            _imageFetchingHelper = imageFetchingHelper;
            _dbConnection = dbConnection;
            _httpContextAccessor = httpContextAccessor;
        }
        // Dropdowns
        public async Task<List<GetTransactionResponse>> ddl_GetTransaction()
        {
            var transactions = new List<GetTransactionResponse>();

            try
            {

                var sqlQuery = @"SELECT 
                                    TRIM(FLD_NAME) AS FLD_NAME,
                                    MOD_NAME,
                                    FLD_VALUE,
                                    DESCR,
                                    SH_DESCR,
                                    NVL(UDF_STR1, 'XX') AS UDF_STR1,
                                    NVL(UDF_STR2, '0') AS UDF_STR2,
                                    ACTIVE,
                                    NVL(UDF_STR3, 'XX') AS UDF_STR3,
                                    NVL(UDF_STR4, 'XX') AS UDF_STR4
                                    FROM 
                                    GENCODES
                                WHERE 
                                    TRIM(FLD_NAME) = 'TRANS_TYPE' 
                                    AND TRIM(MOD_NAME) = 'W_EMP_EMPLOY_EVENTS' 
                                    AND ACTIVE = 'Y'
                                ORDER BY 
                                    DESCR";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var transaction = new GetTransactionResponse
                                {
                                    FLD_NAME = reader["FLD_NAME"].ToString(),
                                    MOD_NAME = reader["MOD_NAME"].ToString(),
                                    FLD_VALUE = reader["FLD_VALUE"].ToString(), 
                                    DESCR = reader["DESCR"].ToString(),
                                    SH_DESCR = reader["SH_DESCR"].ToString(),
                                    UDF_STR1 = reader["UDF_STR1"].ToString(),
                                    UDF_STR2 = reader["UDF_STR2"].ToString(),
                                    //ACTIVE = reader["ACTIVE"] == DBNull.Value ? null : Convert.ToChar(reader["ACTIVE"]),
                                    ACTIVE = reader["ACTIVE"] == DBNull.Value ? (char?)null : Convert.ToChar(reader["ACTIVE"]),
                                    UDF_STR3 = reader["UDF_STR3"].ToString(),
                                    UDF_STR4 = reader["UDF_STR4"].ToString(),
                                };

                                transactions.Add(transaction);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving transactions", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting transaction: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return transactions;
        }

        // Location, Department, Site
        public async Task<List<GetSectionResponse>> ddl_GetSections(string siteCode, string deptCode = null)
        {
            try
            {
                var query = _appDbContext.SECTION_ORG
                    .Where(s => s.SITE_CODE == siteCode);

                if (!string.IsNullOrEmpty(deptCode))
                {
                    query = query.Where(s => s.DEPT_CODE == deptCode);
                }

                var sections = await query.OrderBy(s => s.MENU_DESCR).ToListAsync();

                // Map Section entities to GetSectionResponse DTOs
                return sections.Select(s => new GetSectionResponse
                {
                    SectionCode = s.SECTION_CODE,
                    MenuDescr = s.MENU_DESCR,
                    DeptCode = s.DEPT_CODE,
                    SiteCode = s.SITE_CODE
                }).ToList();
            }          
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting sections: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        public async Task<List<GetLocationResponse>> ddl_GetLocations()
        {
            var transactions = new List<GetLocationResponse>();

            try
            {

                var sqlQuery = @"SELECT DISTINCT
                                    A.SITE_CODE,
                                    UPPER(A.DESCR) AS DESCR,
                                    A.AREA_CODE,
                                    A.ADD1 || '' || A.ADD2 || '' || A.CITY || '-' || A.PIN AS ADDRESS,
                                    A.FIN_ENTITY
                                FROM
                                    SITE A
                                INNER JOIN
                                    EMPLOYEE B ON A.SITE_CODE = B.EMP_SITE
                                WHERE
                                    A.AREA_CODE IN ('H', 'F')
                                    AND B.EMP_TYPE = 'H'
                                ORDER BY
                                    A.SITE_CODE";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var transaction = new GetLocationResponse
                                {
                                    SITE_CODE = reader["SITE_CODE"].ToString(),
                                    DESCR = reader["DESCR"].ToString(),
                                    AREA_CODE = reader["AREA_CODE"].ToString(),
                                    ADDRESS = reader["ADDRESS"].ToString(),
                                    FIN_ENTITY = reader["FIN_ENTITY"].ToString(),                             
                                };

                                transactions.Add(transaction);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving transactions", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting locations: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return transactions;
        }

        public async Task<List<GetDepartmentResponse>> ddl_GetDepartments()
        {
            //select * from department WHERE DEPT_CODE LIKE 'D%' and status = 'A' order by DEPT_CODE
            try
            {
                return await _appDbContext.DEPARTMENT
                    .Where(d => d.id.StartsWith("D") && d.Status == "A")
                    .OrderBy(d => d.id)
                    .Select(d => new GetDepartmentResponse
                    {
                        DeptCode = d.id,
                        Descr = d.name
                    })
                    .ToListAsync();
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting departments: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }
    }
}
