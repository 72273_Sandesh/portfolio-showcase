﻿using HRMS.ModelCL.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Interface.Login
{
    public  interface ITokenVerificationRepository
    {
        // bool VerifyToken(string token);

        TokenVerifyResponse VerifyToken(string token);
    }
}
