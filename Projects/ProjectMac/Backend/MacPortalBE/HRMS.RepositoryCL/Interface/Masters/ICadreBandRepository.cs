﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Cadre;
using HRMS.ModelCL.ADMINISTRATION.Masters.CadreBand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Interface.Masters
{
    public interface ICadreBandRepository
    {
        Task<List<CadreBandModel>> GetAllCadreBand(int pageCount, int item_per_page);
        int CountAllCadreBand();

    }
}
