﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Department;
using HRMS.RepositoryCL.Comman;
using Oracle.ManagedDataAccess.Client;


namespace HRMS.RepositoryCL.Interface.Masters
{
    public interface IDepartmentRepository

    {
        /*Task<List<Department>> GetAllDepartments(int pageCount, int item_per_page);
        int CountAllDepartments();
        Task<List<DeptResponse>> FetchDepartmentByDeptCode(string deptCode);
        Task<int> InsertDepartment(DeptResponse department);
        void UpdateDepartmentDetails(string deptCode, Department updatedDepartment, OracleConnection connection);
        bool DepartmentExists(string deptCode, OracleConnection connection);*/

        Task<(List<Department>, int)> GetAllDepartments(int page, int items_per_page, string search);
        int CountAllDepartments();
        Task<List<DeptFetchDepartmentByDeptCodeResponse>> FetchDepartmentByDeptCode(string deptCode);
        Task<bool> CreateDepartment(Department department);
        UpdateFlagResponse UpdateDepartment(Department department);
        //bool DepartmentExists(string deptCode, OracleConnection connection);
        Task<DepartmentComboResponse> GetAllDepartmentcombo();

        Task<List<AcountsModel>> GetAll_Accounts();
        Task<List<CostCenterModel>> GetAll_CostCentre();
        Task<List<PayTableModel>> GetAll_PayTables();

        Task<List<DepartHead>> GetAll_DepartmentHead();


    }
}
