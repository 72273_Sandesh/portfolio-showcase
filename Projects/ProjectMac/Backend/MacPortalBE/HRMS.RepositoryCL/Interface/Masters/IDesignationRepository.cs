﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Designation;
using HRMS.ModelCL.ADMINISTRATION.Masters.Grade;
using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Designation;
using HRMS.RepositoryCL.Comman;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Interface.Masters
{
    public interface IDesignationRepository
    {
        Task<(List<DesignationGetAllResponse>, int)> GetAllDesignations(int page, int items_per_page, string search);
        

        int CountAllDesignation();

        //Task<Designation> GetDesignationById(long id);
        Task<bool> CreateDesignation(DesignationModel qualification);
        Task<IEnumerable<DesignationGetDesignationByCodeResponse>> GetDesignationByCode(string code);
        
        Task<UpdateFlagResponse> UpdateDesignation(DesignationModel designation);

        // DropDown
        Task<List<GradeModel>> GetGrades();



    }
}
