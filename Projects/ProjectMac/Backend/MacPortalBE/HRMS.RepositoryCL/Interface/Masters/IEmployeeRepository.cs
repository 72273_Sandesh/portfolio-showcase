﻿using HRMS.ModelCL;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.Payloads.Employee;
using HRMS.ModelCL.Payloads.Employee.Assets;
using HRMS.ModelCL.Payloads.Employee.Emp;
using HRMS.ModelCL.Payloads.Employee.Experience;
using HRMS.ModelCL.Payloads.Employee.Family;
using HRMS.ModelCL.Payloads.Employee.Language;
using HRMS.ModelCL.Payloads.Employee.Qualification;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Interface.Masters
{
    public interface IEmployeeRepository
    {

        //--------------

        Task<(List<EmployeeGetAllEmployeeResponse>, int)> FetchEmployees(int page, int items_per_page, string location, string department, string status, string search);

        //--------------
        int CountAllEmployees();
        int CountSearchedEmployees(string search);

        Task<(List<Employee>, int)> FetchFirstTenEmployees(int page, int items_per_page);

        //------------
        Task<IEnumerable<Employee>> GetEmployeeByCode(string empCode);
        Employee FetchEmployeeByEmpCode(OracleConnection connection, string empCode);
        void InsertEmployee(OracleConnection connection, Employee newEmployee);
        void UpdateEmployeeDetails(OracleConnection connection, string empCode, Employee updatedEmployee);
        bool EmployeeExists(OracleConnection connection, string empCode);

        // site codes
        Task<List<EmployeeGetSiteCodesResponse>> ddl_GetSiteCodes(string empcode);

        //Dropdown
        Task<List<EmployeeGetDepartmentResponse>> ddl_GetDepartment();
        //-------------------------------Task<List<EmployeeGetLocationResponse>> ddl_GetLocations(string sessionUserSite);

        Task<List<EmployeeGetLocationResponse>> ddl_GetLocations(List<string> siteCodes);
        Task<List<EmployeeGetLocationResponse>> GetLocationsForEmployee(string empcode);
        //______________________________________________________________________________________________
        IEnumerable<string> ddl_GetStatus();
        Task<List<EmployeeGetDesignationResponse>> ddl_GetDesignation();
        Task<List<EmployeeGetProcGroupResponse>> ddl_GetProcGroup();
        Task<List<EmployeeGetBankResponse>> ddl_GetBank();
        Task<List<EmployeeGetQualificationResponse>> ddl_GetQualification();
        Task<List<EmployeeGetCurrencyResponse>> ddl_GetCurrency();
        Task<Dictionary<string, string>> GetCurrencyDescriptionsAsync();
        Task<List<EmployeeGetSupplierResponse>> ddl_GetSupplier();
        Task<List<EmployeeGetStateResponse>> ddl_GetStates();
        Task<List<EmployeeGetCountryResponse>> ddl_GetCountry();
        Task<List<EmployeeGetRoleResponse>> ddl_GetEmpRole();
        Task<List<EmployeeGetLanguageResponse>> ddl_GetLanguages();
        Task<List<EmployeeGetMaritalStatusResponse>> ddl_GetMaritalStatus();

        // Stored Procedure

        Task<DataTable> SP_GetEmployeeDetailsTable(string empCode, string tableName);

        Task<List<EmployeeSP_GetEmployeeTransferDetailResponse>> SP_GetEmployeeTransferDetails(string asEmpCode, string empcode);
        Task<object> SP_GetEmployeeDetails(string empCode, string tableName);

        // Attachment
        List<Dictionary<string, object>> GetAttachments(string strEmpCode, string strCndCode, string attachMode);
        Task<string> SaveDataAndFileUpload(List<string> lstData, List<IFormFile> arrFile, string strFilePath, List<string> strFileName = null, Dictionary<string, Image> dictImages = null);

        string GetAttachmentMode();
        // Seperation Details
        IEnumerable<EmployeeGetSeperationDetailResponse> GetEmployeesSeperationDetails(string empCode);

        // Update methods
        Task<IActionResult> UpdateEmployeeData(string id, EmployeeSP_GetEmployeePersonalDetailsResponse employeeUpdateModel);

        // Insert methods

        Task<IActionResult> InsertEmployeeChanges(EmployeeSP_GetEmployeePersonalDetailsResponse employee);
        Task<Employee> InsertEmployeeChange(Employee_Det_Change employee);

        //Edit Employee details at once
        Task<bool> UpdateEmployeeAsync(Employee employee);

        //  Employee Edit Personal Details
        Task<bool> UpdateEmployeeBasicDetails(string empCode, EmployeeBasicDetailsUpdateRequest updatedDetails);

        //  Employee Edit Contact Details
        Task<bool> UpdateEmployeePresentDetails(string empCode, EmployeePresentDetailsUpdateRequest updatedDetails);
        Task<bool> UpdateEmployeePermanentDetails(string empCode, EmployeePermanentDetailsUpdateRequest updatedDetails);
        Task<bool> UpdateEmployeeEmergencyDetails(string empCode, EmployeeEmergencyDetailsUpdateRequest updatedDetails);

        // Employee Edit Work and Payroll Details
        Task<bool> UpdateEmployeeWorkPayrollDetails(string empCode, EmployeeWorkPayrollDetailsUpdateRequest updatedDetails);

        // Edit KYC details
        Task<bool> UpdateEmployeeKYCDetails(string empCode, EmployeeKYCUpdateRequest updatedDetails);

        // Add Employee Language
        Task<bool> AddLanguage(AddLanguage newLanguage);
        Task<bool> UpdateLanguage(string EmpCode, string Language, UpdateLanguage updatedLanguage);


        // Add Employee Family
        //Task<bool> AddFamily(AddFamily newfamily);
        Task<bool> AddFamilies(List<AddFamily> newFamilies);
        Task<bool> UpdateEmpFamilyAsync(string empCode, string seqNo, EmployeeFamilyUpdateRequest updatedDetails);
        Task<bool> UpdateEmpFamilyPensionPfGrat(string empCode, string seqNo, EmployeeFamilyNomineeUpdateRequest updatedDetails);
        Task<bool> UpdateEmpFamilyPensionPfGrat(List<EmployeeFamilyNomineeUpdateMultipleRequest> updatedDetailsList);

        // Add Employee Qualification
        Task<bool> AddQualification(AddQualification newQualification);
        Task<bool> UpdateEmpQualification(string empCode, string seqNo, EmployeeQualificationUpdateRequest updatedDetails);

        // Add Employee Experience
        Task<bool> AddExperience(AddExperience newexperience);
        Task<bool> UpdateEmpExperience(string empCode, DateTime fromDate, EmployeeExperienceUpdateRequest updatedDetails);

        // Add Employee Assets
        Task<List<EmployeeGetAssetsResponse>> ddl_GetAsstes();
        Task<bool> AddAssets(AddAssets newassets);
        Task<bool> UpdateEmpInduction(string empCode, string inductCode, string tranId, EmployeeAssetsUpdateRequest updatedDetails);

    }
}

