﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Cadre;
using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Grade;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Grade;
using HRMS.RepositoryCL.Comman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Interface.Masters
{
    public interface IGradeRepository
    {
        Task<(List<GradeGetGradeByGradeIdResponse>, int)> GetAllGrades(int page, int items_per_page, string search);
        int CountAllGrades();
        Task<List<GradeGetGradeByGradeIdResponse>> FetchGradeByIdAsync(string gradeId);
        Task<bool> CreateGrade(GradeModel grade);
        Task<UpdateFlagResponse> UpdateGrade(GradeModel grade);

        // Cadre Dropdown
        Task<List<CadreResp>> GetAll_Cadres();
    }
}
