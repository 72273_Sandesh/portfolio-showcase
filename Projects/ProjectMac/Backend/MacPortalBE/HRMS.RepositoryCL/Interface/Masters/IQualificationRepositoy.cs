﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Qualifcation;
using HRMS.RepositoryCL.Comman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Interface.Masters
{
    public interface IQualificationRepositoy
    {
        Task<(List<Qualification>, int)> GetAllQualifications(int page, int items_per_page, string search);
        Task<bool> CreateQualification(Qualification qualification);
        Task<Qualification> GetQualificationById(long id);

        Task<IEnumerable<GetQualificationByCodeResponse>> GetQualificationByCode(string code);
        Task<UpdateFlagResponse> UpdateQualification(Qualification qualification);

        int CountAllQualifications();

        Task<List<QualwiseStruc>> ddl_GetDistictSalaryStruc();
        //Task DeleteQualificationAsync(long id);

    }
}
