﻿using HRMS.ModelCL.Payloads.Transaction.Affecting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Interface.Transaction
{
    public interface IAffectingRepository
    {
        Task<List<GetTransactionResponse>> ddl_GetTransaction();
        Task<List<GetSectionResponse>> ddl_GetSections(string siteCode, string deptCode = null);
        Task<List<GetLocationResponse>> ddl_GetLocations();
        Task<List<GetDepartmentResponse>> ddl_GetDepartments();
    }
}
