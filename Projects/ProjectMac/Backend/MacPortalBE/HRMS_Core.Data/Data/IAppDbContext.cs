﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS_Core.Data.Data
{
    public interface IAppDbContext
    {
        //Oracle Context
        OracleCommand GetCommand();
        OracleConnection GetConn();

        //SQL Context
        SqlCommand GetSQLCommand();
        SqlConnection GetSQLConnection();
    }
}
