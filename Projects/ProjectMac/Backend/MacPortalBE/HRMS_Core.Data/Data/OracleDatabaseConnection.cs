﻿using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System.Data;

namespace HRMS_Core.Data.Data
{
    public class OracleDatabaseConnection
    {
        private readonly string _connectionString;

        public OracleDatabaseConnection(IConfiguration configuration)
        {
            _connectionString = configuration?.GetConnectionString("oracleConnection")
                                ?? throw new ArgumentNullException(nameof(configuration), "Configuration cannot be null");
        }

        public IDbConnection Connection => new OracleConnection(_connectionString);

    }
}
