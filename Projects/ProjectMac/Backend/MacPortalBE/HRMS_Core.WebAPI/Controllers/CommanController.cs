﻿using HRMS.ModelCL.Login;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.WebAPI.Controllers.Masters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace HRMS_Core.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommanController : ControllerBase
    {
        private readonly ILogger<QualificationController> _logger;
        private readonly ControllerMasterRepository _controllerMasterRepository;

        public CommanController(ILogger<QualificationController> logger, ControllerMasterRepository ControllerMasterRepository)
        {
            _logger = logger;
            _controllerMasterRepository = ControllerMasterRepository;
        }

        [HttpGet("CleintIpAddress")]
        public IActionResult GetClientIpAddress()
        {
            string clientIpAddress = HttpContext.Connection.RemoteIpAddress.ToString();
            // Get the remote IP address from the HttpContext
            var ipAddress = Request.HttpContext.Connection.RemoteIpAddress;

            // Convert to IPv4 format if it's in IPv6 format
            if (ipAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
            {
                ipAddress = ipAddress.MapToIPv4();
            }

            return Ok($"Client IP Address: {ipAddress}");
        }

    }
}
