﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using System;
using System.Reflection;


using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using HRMS.ModelCL.Login;
using HRMS.RepositoryCL.Comman;
using HRMS_Core.WebAPI.Controllers.Masters;
using System.Reflection.Emit;

namespace HRMS_Core.WebAPI.Controllers
{

    [Route("api/[controller]")]
    public class CtrlDataController : ControllerBase
    {
        private readonly ILogger<CtrlDataController> _logger;
        private readonly IControllerMasterRepository _controllerMasterRepository;
        private readonly IActionDescriptorCollectionProvider _actionDescriptorCollectionProvider;
        

        private readonly List<ControllerMasterModel> _routeInfoList = new List<ControllerMasterModel>();


        public CtrlDataController(IActionDescriptorCollectionProvider actionDescriptorCollectionProvider, ILogger<CtrlDataController> logger, IControllerMasterRepository ControllerMaster)
        {
            _actionDescriptorCollectionProvider = actionDescriptorCollectionProvider;
            _logger = logger;
            _controllerMasterRepository = ControllerMaster;
        }

        [HttpGet("GetAllControllerDetails")]
        public IEnumerable<ControllerMasterModel> GetRouteInfo()
        {
            var actionDescriptors = _actionDescriptorCollectionProvider.ActionDescriptors.Items;

            foreach (var actionDescriptor in actionDescriptors)
            {
                if (actionDescriptor is ControllerActionDescriptor controllerActionDescriptor)
                {
                    var controllerName = controllerActionDescriptor.ControllerName;
                    var httpMethods = controllerActionDescriptor.ActionConstraints
                        .OfType<HttpMethodActionConstraint>()
                        .SelectMany(constraint => constraint.HttpMethods);

                    foreach (var httpMethod in httpMethods)
                    {
                        var endpoint = $"/{controllerActionDescriptor.AttributeRouteInfo.Template}".Replace("//", "/");
                        _routeInfoList.Add(new ControllerMasterModel { CtrlName = controllerName, HttpType = httpMethod, EndPoints = endpoint });
                    }
                }
            }

            return _routeInfoList;
        }


        [HttpPost("InsertIntoControllerMaster")]
        public async Task<IActionResult> InsertControllerMaster([FromBody] List<ControllerMasterModel> records)
        {
            try
            {
                var (lstEndpointsExist, lstEndpointsInserted) = await _controllerMasterRepository.InsertControllerMaster(records);

                // Create response object with count information
                var response = new
                {
                    //InsertedRecords = $"Records inserted successfully.Count-{lstEndpointsInserted.Count}, Endpoints-{string.Join(", ", lstEndpointsInserted)}",
                    //ExistingRecords = $"Records Already exist.Count-{lstEndpointsExist.Count}, Endpoints-{string.Join(", ", lstEndpointsExist)}"
                    InsertedRecords = new
                    {
                        Count = lstEndpointsInserted.Count,
                        Endpoints = lstEndpointsInserted
                    },
                    ExistingRecords = new
                    {
                        Count = lstEndpointsExist.Count,
                        Endpoints = lstEndpointsExist
                    }
                };
                return Ok(response);
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

    }

    


}
