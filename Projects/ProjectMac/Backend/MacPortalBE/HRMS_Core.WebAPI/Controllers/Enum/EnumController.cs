﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;


namespace HRMS_Core.WebAPI.Controllers.Enum
{
    [Route("api/[controller]")]
    [ApiController]
    public class EnumController : ControllerBase
    {

        [HttpGet("status")]
        public IActionResult GetEmployeeStatus()
        {
            var enumNames = HRMS.ModelCL.ADMINISTRATION.Masters.Employee.Enum.GetNames(typeof(HRMS.ModelCL.ADMINISTRATION.Masters.Employee.Enum));

            return Ok(enumNames);
        }

        [HttpGet("status/{name}")]
        public IActionResult GetEnumValue(string name)
        {
            // Convert input name to lowercase for case-insensitive comparison
            name = name.ToLower();

            // Check if the provided name exists in the enum
            if (HRMS.ModelCL.ADMINISTRATION.Masters.Employee.Enum.TryParse(name, true, out HRMS.ModelCL.ADMINISTRATION.Masters.Employee.Enum enumValue))
            {
                // Return the enum value if it exists
                return Ok(enumValue.ToString());
            }
            else
            {
                // Return error if the provided name does not exist in the enum
                return NotFound($"Enum value corresponding to name '{name}' not found.");
            }
        }

    }
}
