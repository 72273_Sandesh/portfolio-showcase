﻿using HRMS.ModelCL.Login;
using Microsoft.AspNetCore.Mvc;
using HRMS.RepositoryCL.Interface.Login;
using Newtonsoft.Json;
using RMS.ModelCL.Login;
using Newtonsoft.Json.Linq;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using HRMS.RepositoryCL.Concrete.Login;
using HRMS.ModelCL.Payloads;


namespace HRMS_Core.WebAPI.Controllers.Login
{
    [ApiController]
    [Route("api/Auth")]
    public class AuthController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly ILogger<AuthController> _logger;
      

        public AuthController(IUserRepository userRepository, ILogger<AuthController> logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        [HttpPost("AddRole")]
        public async Task<IActionResult> AddRole([FromBody] Role role)
        {
            try
            {
                var addedRole = await _userRepository.AddRole(role);
                return Ok(addedRole);
            }
            catch (Exception ex)
            {
                _logger.LogError($"An error occurred during login. Request details: {JsonConvert.SerializeObject(role)}, Error: {ex.Message}");
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpPost("AddUser")]
        public async Task<IActionResult> AddUser([FromBody] users user)
        {
            try
            {
                var addedUser = await _userRepository.AddUser(user);
                return Ok(addedUser);
            }
            catch (Exception ex)
            {
                _logger.LogError($"An error occurred during login. Request details: {JsonConvert.SerializeObject(user)}, Error: {ex.Message}");
                return BadRequest("Error adding user");
            }
        }

        [HttpPost("AssignRoleToUser")]
        public async Task<IActionResult> AssignRoleToUser([FromBody] AddUserRole obj)
        {
            try
            {
                var success = await _userRepository.AssignRoleToUser(obj);
                if (success)
                    return Ok(new { Message = "Roles assigned successfully" });
                else
                    return BadRequest(new { Message = "Error assigning roles" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"An error occurred during login. Request details: {JsonConvert.SerializeObject(obj)}, Error: {ex.Message}");
                return BadRequest(new { Message = "Error assigning roles" });
            }
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest loginRequest)
        {
            try
            {

                var response = await _userRepository.Authenticate(loginRequest);
                if (response != null && ((ObjectResult)response).Value !=null )
                {
                    //var tokenResponse = token;

                    //TokenVerifyResponse resp = new TokenVerifyResponse()
                    //{
                    //    api_token = token.api_token,
                    //    created_at = DateTime.Now,
                    //    email = "abc123@gmail.com",
                    //    email_verified_at = DateTime.Now,
                    //    first_name = "Omkar",
                    //    id = 1,
                    //    last_name = "Varambale",
                    //    updated_at = DateTime.Now
                    //};

                    var tokenResponse = ((ObjectResult)response).Value as TokenVerifyResponse;

                    if (tokenResponse != null)
                    {
                        // Successfully received TokenVerifyResponse
                        return Ok(tokenResponse);
                    }
                    else
                    {
                        return StatusCode(200, new { Message = "Invalid Credentials !!!", Status = "UnAuthorized" });
                    }
                }
                else
                {
                    _logger.LogError("Error during login");
                    return StatusCode(200, new { Message = "Invalid Credentials !!!", Status = "UnAuthorized" });
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"An error occurred during login. Request details: {JsonConvert.SerializeObject(loginRequest)}, Error: {ex.Message}");
                return StatusCode(500, "Internal Server Error");
            }
        }

        //[HttpPost("verify_token")]
        //public IActionResult VerifyToken([FromBody] string  token)
        //{
        //    // Extract the token from the Authorization header
        //    //var token = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

        //    if (string.IsNullOrEmpty(token))
        //    {
        //        return Unauthorized("Token is missing");
        //    }

        //    try
        //    {
        //        // Manually validate the token
        //        var tokenHandler = new JwtSecurityTokenHandler();
        //        var key = Encoding.UTF8.GetBytes("your_secret_key");
        //        var validationParameters = new TokenValidationParameters
        //        {
        //            ValidateIssuer = true,
        //            ValidateAudience = true,
        //            ValidateLifetime = true,
        //            ValidateIssuerSigningKey = true,
        //            ValidIssuer = Configuration["Jwt:Issuer"],
        //            ValidAudience = "your_audience",
        //            IssuerSigningKey = new SymmetricSecurityKey(key)
        //        };

        //        SecurityToken validatedToken;
        //        var principal = tokenHandler.ValidateToken(token, validationParameters, out validatedToken);

        //        // Your secure logic here
        //        var userId = principal.FindFirst(ClaimTypes.NameIdentifier)?.Value;

        //        LoginRespDTO resp = new LoginRespDTO
        //       (
        //       token,
        //       DateTime.Now,
        //       "abc123@gmail.com",
        //       DateTime.Now,
        //       "Omkar",
        //       2,
        //       "Varambale",
        //       DateTime.Now
        //       );
        //        return Ok(resp);
        //        //return Ok(new { Message = "This is a secure action", UserId = userId });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Unauthorized("Invalid token: " + ex.Message);
        //    }

        //}



    }

}
