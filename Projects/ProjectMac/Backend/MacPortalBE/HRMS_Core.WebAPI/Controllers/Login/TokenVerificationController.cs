﻿using HRMS.ModelCL.Login;
using HRMS.ModelCL.Payloads;
using HRMS.RepositoryCL.Concrete.Login;
using HRMS.RepositoryCL.Interface.Login;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace HRMS_Core.WebAPI.Controllers.Login
{
    [Route("api/Auth")]
    [ApiController]
    public class TokenVerificationController : ControllerBase
    {
        /* private readonly ITokenVerificationRepository _tokenVerificationRepository;

         public TokenVerificationController(ITokenVerificationRepository tokenVerificationRepository)
         {
             _tokenVerificationRepository = tokenVerificationRepository;
         }

         //Token verification Manual for front end template as per keentheme

         [HttpPost("verify_token")]
         public IActionResult VerifyToken([FromBody] Tokens token)
         {
             if (string.IsNullOrEmpty(token.api_token))
             {
                 return Unauthorized("Token is missing");
             }

             // Use the repository to verify the token
             if (!_tokenVerificationRepository.VerifyToken(token.api_token))
             {
                 return Unauthorized("Invalid token");
             }
             TokenVerifyResponse resp = new TokenVerifyResponse()
             {
                 api_token = token.api_token,
                 created_at = DateTime.Now,
                 email = "",
                 email_verified_at = DateTime.Now,
                 first_name = "",
                 id = "",
                 last_name = "",
                 updated_at = DateTime.Now
             };

             return Ok(resp);
         }*/


        private readonly ITokenVerificationRepository _tokenVerificationRepository;
        private readonly IUserRepository _userRepository;

        public TokenVerificationController(ITokenVerificationRepository tokenVerificationRepository, IUserRepository userRepository)
        {
            _tokenVerificationRepository = tokenVerificationRepository;
            _userRepository = userRepository;
        }

        [HttpPost("verify_token")]
        public IActionResult VerifyToken([FromBody] Tokens token)
        {
            if (string.IsNullOrEmpty(token.api_token))
            {
                return Unauthorized("Token is missing");
            }

            // Use the repository to verify the token
            var userResponse = _tokenVerificationRepository.VerifyToken(token.api_token);
            if (userResponse == null)
            {
                return Unauthorized("Invalid token");
            }

            // Token is valid, return the response with user data
            return Ok(userResponse);
        }

    }
}
