﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL;
using HRMS.RepositoryCL.Concrete.Masters;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.Data.Data;
using HRMS_Core.WebAPI.RequestHelpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HRMS.ModelCL.ADMINISTRATION.Masters.CadreBand;

namespace HRMS_Core.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [ApiController]
    public class CadreBandController : ControllerBase
    {
        private readonly AppDbContext _appDbContext;
        private readonly ILogger<CadreBandController> _logger;
        private readonly ICadreBandRepository _CadreBandRepository;

        public CadreBandController(AppDbContext appDbContext, ILogger<CadreBandController> logger, ICadreBandRepository cadreBand)
        {
            _appDbContext = appDbContext;
            _logger = logger;
            _CadreBandRepository = cadreBand;
        }

            [HttpGet("GetAll")]
            public async Task<IActionResult> GetAllCadreBands([FromQuery] PaginationParams paginationParams)
            {
                try
                {
                    var cadreBands = await _CadreBandRepository.GetAllCadreBand(paginationParams.page, paginationParams.items_per_page);

                    var totalCadreBandCount =  _CadreBandRepository.CountAllCadreBand();

                    var payload = GenPageURL_Helper.GeneratePagination(totalCadreBandCount, paginationParams.page, paginationParams.items_per_page, "CadreBand/GetAll");

                        return Ok(new { data = cadreBands, payload });
                }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


    }
}
