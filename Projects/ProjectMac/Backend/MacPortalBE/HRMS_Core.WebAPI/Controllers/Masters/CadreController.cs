﻿using HRMS.ModelCL;
using HRMS.ModelCL.ADMINISTRATION.Masters.Cadre;
using HRMS.ModelCL.ADMINISTRATION.Masters.CadreBand;
using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Cadre;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Concrete.Masters;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.Data.Data;
using HRMS_Core.WebAPI.RequestHelpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.NetworkInformation;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HRMS_Core.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CadreController : ControllerBase
    {
        private readonly AppDbContext _appDbContext;
        private readonly ILogger<CadreController> _logger;
        private readonly ICadreRepository _CadreRepository;

        public CadreController(AppDbContext appDbContext, ILogger<CadreController> logger, ICadreRepository cadre)
        {
            _appDbContext = appDbContext;
            _logger = logger;
            _CadreRepository = cadre;
        }


        /*[HttpGet("GetAllCadres/query")]
        public async Task<ActionResult<List<CadreGetAllResponse>>> GetAllCadres([FromQuery] PaginationParams paginationParams, string? search = null)
        {
            try
            {
                // Retrieve cadres
                var cadres = await _CadreRepository.GetAllCadres(paginationParams.page, paginationParams.items_per_page, search);

                var totalCadreCount = _CadreRepository.CountAllCadres();

                Payload payload = GenPageURL_Helper.GeneratePagination(totalCadreCount, paginationParams.page, paginationParams.items_per_page, "Cadre/GetAllCadres/query");

                return Ok(new { data = cadres, payload });
            }
            catch (RepositoryException ex)
            {
                return Ok(new { Status = 'E', Message = ex.Message });
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }*/


        [HttpGet("GetAllCadres/query")]
        public async Task<IActionResult> GetAllCadres([FromQuery] PaginationParams paginationParams, string? search = null)
        {
            try
            {
                // Retrieve cadres and total cadre count
                var (cadres, totalCadreCount) = await _CadreRepository.GetAllCadres(paginationParams.page, paginationParams.items_per_page, search);

                // Generate pagination payload
                Payload payload = GenPageURL_Helper.GeneratePagination(totalCadreCount, paginationParams.page, paginationParams.items_per_page, "Cadre/GetAllCadres/query");

                // Return response with cadres and payload
                return Ok(new { data = cadres, payload });
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.Message }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("GetCadreByCadreCode/{cadreCode}")]
        public async Task<IActionResult> GetCadreByCadreCode(string cadreCode)
        {
            try
            {
                List<CadreGetAllResponse> cadres = await _CadreRepository.FetchCadreByCadreCode(cadreCode);

                if (cadres == null || cadres.Count == 0)
                {
                    return NotFound();
                }

                //Take the first cadre as a single object for the response
                //CadreResponse cadre = cadres[0];

                var dataWrapper = new
                {
                    data = cadres.Find(p => p.CADRE_CODE == cadreCode),
                };
                return Ok(dataWrapper);

            }
            catch (RepositoryException ex)
            {
                //return Ok(new {data=new { responseStatus = 'E', Message = ex.Message }});
                var messages = new List<string?> { ex.Message };
                return ResponseGen_Helper.CreateResponse("E", messages, cadreCode);
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }




        [HttpPost("createCadre")]
        public async Task<IActionResult> CreateCadre([FromBody] CadreModel cadre)
        {
            try
            {
                if (cadre == null)
                {
                    var messages = new List<string?> { "Cadre object is null." };
                    return ResponseGen_Helper.CreateResponse("E", messages, cadre?.CADRE_CODE);
                }
                bool Create = await _CadreRepository.CreateCadre(cadre);
                if (Create == true)
                {
                    var messages = new List<string?> { "Cadre created successfully." };
                    return ResponseGen_Helper.CreateResponse("S", messages, cadre.CADRE_CODE );
                }
                else
                {
                    var messages = new List<string?> { "Cadre name already exist." };
                    return ResponseGen_Helper.CreateResponse("E", messages, cadre.CADRE_CODE );
                }
            }
            catch (RepositoryException ex)
            {
                var messages = new List<string?> { ex.Message };
                return ResponseGen_Helper.CreateResponse("E", messages, cadre.CADRE_CODE);
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpPut("EditCadre/{Code}")]
        public async Task<IActionResult> EditCadre([FromBody] CadreModel cadre, string Code)
        {
            try
            {
                if (cadre == null)
                {
                    var messages = new List<string?> { "Cadre object is null." };
                    return ResponseGen_Helper.CreateResponse("E", messages, Code);
                }
                else
                {
                    if (Code != cadre.CADRE_CODE.Trim())
                    {
                        var errorMessage = "Cadre code should be same in request body.";
                        return ResponseGen_Helper.CreateResponse("E", new List<string?> { errorMessage }, cadre.CADRE_CODE);
                    }
                }

                UpdateFlagResponse update = _CadreRepository.UpdateCadre(cadre);

                if (!update.Success)
                {
                    if (!update.codeExists)
                    {
                        return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Cadre code not found." }, cadre.CADRE_CODE);
                    }
                    if (update.descrExists)
                    {
                        return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Cadre name already exists." }, cadre.CADRE_CODE);
                    }
                }

                var successMessage = "Cadre updated successfully.";
                return ResponseGen_Helper.CreateResponse("S", new List<string?> { successMessage }, Code);
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.Message },Code);
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred in {controllerName} - {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request: {ex.Message}");
            }
        }


        [HttpGet]
        [Route("GetAll_CadreCategory")]
        public async Task<IActionResult> GetAll_CadreCategory()
        {
            try
            {

                var lstcadreBand = await _CadreRepository.GetAllCadre_Category();
                if (lstcadreBand == null)
                {
                    return NotFound();
                }

                // Exclude null fields
                lstcadreBand = lstcadreBand.Where(item => item != null).ToList();
                // Select only the desired fields
                var filteredData = lstcadreBand.Select(item => new CadreBandModel
                {
                    BAND_CODE = item.BAND_CODE,
                    DESCR = item.DESCR
                }).ToList();

                var dataWrapper = new DataWrapper<CadreBandModel>
                {
                    data = filteredData
                };

                return Ok(dataWrapper);
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { data = new { responsrStatus = 'E', Message = ex.Message } });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.Message }, "");
            }
            catch (Exception ex) 
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


    }
}
