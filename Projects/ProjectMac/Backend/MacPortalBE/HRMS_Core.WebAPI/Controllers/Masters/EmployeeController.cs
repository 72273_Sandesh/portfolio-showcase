﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Oracle.ManagedDataAccess.Client;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using System.Data;
using Microsoft.EntityFrameworkCore;
using HRMS.RepositoryCL.Concrete.Masters;
using Microsoft.AspNetCore.Authorization;
using HRMS.ModelCL.ADMINISTRATION.Masters.Grade;
using HRMS.ModelCL;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS.ModelCL.Payloads.Employee;
using HRMS.ModelCL.Payloads;
using HRMS_Core.WebAPI.RequestHelpers;
using Microsoft.AspNetCore.Mvc.RazorPages;
using HRMS.ModelCL.Payloads.Employee.Language;
using System.Reflection.Emit;
using Microsoft.AspNetCore.Http.HttpResults;
using HRMS.ModelCL.ADMINISTRATION.Masters.Designation;
using HRMS.ModelCL.Payloads.Employee.Family;
using HRMS.ModelCL.Payloads.Employee.Qualification;
using HRMS.ModelCL.Payloads.Employee.Experience;
using HRMS.ModelCL.Payloads.Employee.Assets;
using HRMS.ModelCL.Payloads.Employee.Emp;

namespace HRMS_Core.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EmployeeController : ControllerBase
    {

        // Oracle connection string
        private readonly string _connectionString = "DATA SOURCE=192.168.33.70:1521/UAT10G;USER ID = MAC;PASSWORD = ocean";


        private readonly ILogger<EmployeeController> _logger;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IDesignationRepository _designationRepository;

        public EmployeeController(ILogger<EmployeeController> logger, IEmployeeRepository employee, IDesignationRepository designationRepository)
        {
            _logger = logger;
            _employeeRepository = employee;
            _designationRepository = designationRepository;
        }


        [HttpGet("GetAllEmployees")]
        public async Task<IActionResult> GetAllEmployees([FromQuery] PaginationParams paginationParams, string? location = null , string? department = null, string? status = null, string? search = null)
        {
            try
            {
                var (employees, fetchedCount) = await _employeeRepository.FetchEmployees(paginationParams.page, paginationParams.items_per_page, location, department, status, search);

                Payload payload = GenPageURL_Helper.GeneratePagination(fetchedCount, paginationParams.page, paginationParams.items_per_page, $"Employee/GetAllEmployees?location={location}&department={department}&status={status}&search={search}");

                return Ok(new { data = employees, payload });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred in {controllerName}: {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request: {ex.Message}");
            }
        }



        [HttpGet("GetAllEmployees/10")]
        public async Task<IActionResult> GetAllEmployees([FromQuery] PaginationParams paginationParams)
        {
            try
            {
                var (employees, fetchedCount) = await _employeeRepository.FetchFirstTenEmployees(paginationParams.page, paginationParams.items_per_page);

                // Calculate the total number of employee
                var totalEmpCount = _employeeRepository.CountAllEmployees();

                Payload payload = GenPageURL_Helper.GeneratePagination(fetchedCount, paginationParams.page, paginationParams.items_per_page, "Employee/GetAllEmployees/query");


                return Ok(new { data = employees, payload});
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }




        [HttpGet("GetEmployeeByEmpCode/{empCode}")]
        public IActionResult GetEmployeeByEmpCode(string empCode)
        {
            try
            {
                using (var connection = new OracleConnection(_connectionString))
                {
                    connection.Open();

                    var employee = _employeeRepository.FetchEmployeeByEmpCode(connection, empCode);

                    if (employee != null)
                    {
                        return Ok(employee);
                    }
                    else
                    {
                        return NotFound($"Employee with EMP_CODE '{empCode}' not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Error: {ex.Message}");
            }
        }

        [HttpGet("GetEmpByCode/{empCode}")]
        public async Task<IActionResult> GetEmployeeBYEMPCODE(string empCode)
        {
            try
            {
                var employees = await _employeeRepository.GetEmployeeByCode(empCode);

                if (employees == null || !employees.Any())
                {
                    return NotFound();
                }
                else
                {
                    var dataWrapper = new DataWrapperObj<IEnumerable<Employee>>
                    {
                        data = employees
                    };

                    return Ok(dataWrapper); // Return the dataWrapper instance
                }
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpPost("InsertEmployee")]
        public IActionResult InsertEmployee([FromBody] Employee newEmployee)
        {
            try
            {
                using (var connection = new OracleConnection(_connectionString))
                {
                    connection.Open();

                    // Assuming EMP_CODE is a unique identifier, you might want to check if the employee already exists
                    if (_employeeRepository.EmployeeExists(connection, newEmployee.EMP_CODE))
                    {
                        return BadRequest($"Employee with EMP_CODE '{newEmployee.EMP_CODE}' already exists.");
                    }

                    // If not, proceed with the insertion
                    _employeeRepository.InsertEmployee(connection, newEmployee);

                    return Ok($"Employee with EMP_CODE '{newEmployee.EMP_CODE}' inserted successfully.");
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Error: {ex.Message}");
            }
        }


        [HttpPut("UpdateEmployee/{empCode}")]
        public IActionResult UpdateEmployee(string empCode, [FromBody] Employee updatedEmployee)
        {
            try
            {
                using (var connection = new OracleConnection(_connectionString))
                {
                    connection.Open();

                    // Check if the employee with the given EMP_CODE exists
                    if (!_employeeRepository.EmployeeExists(connection, empCode))
                    {
                        return NotFound($"Employee with EMP_CODE '{empCode}' not found.");
                    }

                    // Proceed with the update
                    _employeeRepository.UpdateEmployeeDetails(connection, empCode, updatedEmployee);

                    return Ok($"Employee with EMP_CODE '{empCode}' updated successfully.");
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Error: {ex.Message}");
            }
        }

        // site codes 
        [HttpGet("ddl_GetSiteCodes/{empcode}")]
        public async Task<IActionResult> ddl_GetSiteCodes(string empcode)
        {
            try
            {
                List<EmployeeGetSiteCodesResponse> lstsitecodes = await _employeeRepository.ddl_GetSiteCodes(empcode);
                if (lstsitecodes == null)
                {
                    return NotFound();
                }

                return Ok(new { site_codes = lstsitecodes });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetDepartment")]
        public async Task<IActionResult> ddl_GetDepartment()
        {
            try
            {
                List<EmployeeGetDepartmentResponse> lstdepartments = await _employeeRepository.ddl_GetDepartment();

                if (lstdepartments == null)
                {
                    return NotFound();
                }


                return Ok(new {department = lstdepartments});
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }



        /*[HttpGet("GetAll_Locations/{sessionUserSite}")]
        public async Task<IActionResult> GetAll_Locations(string sessionUserSite)
        {
            try
            {
                List<EmployeeGetLocationResponse> lstlocations = await _employeeRepository.ddl_GetLocations(sessionUserSite);
                if (lstlocations == null)
                {
                    return NotFound();
                }
                
                return Ok(new { location = lstlocations });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }*/


        [HttpGet("EMP_WORK_PAYROLL_site/{empcode}")]
        public async Task<IActionResult> GetLocationsForEmployee(string empcode)
        {
            try
            {
                var locations = await _employeeRepository.GetLocationsForEmployee(empcode);
                return Ok(locations);
            }
            catch (RepositoryException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = ex.Message });
            }
        }

        [HttpGet("ddl_GetStatus")]
        public IActionResult GetStatus()
        {
            var dropdownItems = _employeeRepository.ddl_GetStatus();

            var response = new
            {
                status = dropdownItems.Select(item => item.ToUpper()).ToList()
            };

            return Ok(response);
        }

        [HttpGet("ddl_combinedEmployeeResponse")]
        public async Task<IActionResult> GetCombinedData(string empcode)
        {
            try
            {
                // Call each endpoint asynchronously
                var departmentsTask = _employeeRepository.ddl_GetDepartment();
                var locationsTask = _employeeRepository.ddl_GetSiteCodes(empcode);

                // Await departments and locations tasks
                await Task.WhenAll(departmentsTask, locationsTask);

                // Extract the results from the tasks
                var departments = await departmentsTask;
                var locations = await locationsTask;

                // Fetch the status synchronously
                var status = _employeeRepository.ddl_GetStatus();

                // Construct the combined response object
                var combinedResponse = new
                {
                    department = departments,
                    location = locations,
                    status = status
                };

                // Return the combined response
                return Ok(combinedResponse);
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpGet("ddl_Employee_Grade")]
        public async Task<IActionResult> ddl_Designation_Grade()
        {
            try
            {
                List<GradeModel> lstgrades = await _designationRepository.GetGrades();

                if (lstgrades == null)
                {
                    return NotFound();
                }

                var DataWrapperObj = new DataWrapperObj<List<GradeModel>>
                {
                    data = lstgrades
                };

                return Ok(DataWrapperObj);
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetDesignation")]
        public async Task<IActionResult> ddl_GetDesignation()
        {
            try
            {
                List<EmployeeGetDesignationResponse> lstdesignations = await _employeeRepository.ddl_GetDesignation();

                if (lstdesignations == null)
                {
                    return NotFound();
                }


                return Ok(new { designation = lstdesignations });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetProcGroup")]
        public async Task<IActionResult> ddl_GetProcGroup()
        {
            try
            {
                List<EmployeeGetProcGroupResponse> lstgroups = await _employeeRepository.ddl_GetProcGroup();

                if (lstgroups == null)
                {
                    return NotFound();
                }


                return Ok(new { proc_groups = lstgroups });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetBank")]
        public async Task<IActionResult> ddl_GetBank()
        {
            try
            {
                List<EmployeeGetBankResponse> lstbanks = await _employeeRepository.ddl_GetBank();

                if (lstbanks == null)
                {
                    return NotFound();
                }


                return Ok(new { banks = lstbanks });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetQualification")]
        public async Task<IActionResult> ddl_GetQualification()
        {
            try
            {
                List<EmployeeGetQualificationResponse> lstqualifications = await _employeeRepository.ddl_GetQualification();

                if (lstqualifications == null)
                {
                    return NotFound();
                }


                return Ok(new { qualifications = lstqualifications });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetCurrency")]
        public async Task<IActionResult> ddl_GetCurrency()
        {
            try
            {
                List<EmployeeGetCurrencyResponse> lstcurrency = await _employeeRepository.ddl_GetCurrency();

                if (lstcurrency == null)
                {
                    return NotFound();
                }


                return Ok(new { currency = lstcurrency });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetCurrencyDescriptions")]
        public async Task<IActionResult> GetCurrencyDescriptions()
        {
            try
            {
                Dictionary<string, string> currencyDescriptions = await _employeeRepository.GetCurrencyDescriptionsAsync();
                return Ok(currencyDescriptions);
            }
            catch (Exception ex)
            {
                // Log the exception
                return StatusCode(500, new { message = ex.Message });
            }
        }

        [HttpGet("ddl_GetSupplier")]
        public async Task<IActionResult> ddl_GetSupplier()
        {
            try
            {
                List<EmployeeGetSupplierResponse> lstsupplier = await _employeeRepository.ddl_GetSupplier();

                if (lstsupplier == null)
                {
                    return NotFound();
                }


                return Ok(new { supplier = lstsupplier });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetStates")]
        public async Task<IActionResult> ddl_GetStates()
        {
            try
            {
                List<EmployeeGetStateResponse> lststates = await _employeeRepository.ddl_GetStates();

                if (lststates == null)
                {
                    return NotFound();
                }


                return Ok(new { states = lststates });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpGet("ddl_GetCountry")]
        public async Task<IActionResult> ddl_GetCountry()
        {
            try
            {
                List<EmployeeGetCountryResponse> lstcountry  = await _employeeRepository.ddl_GetCountry();
                if (lstcountry == null)
                {
                    return NotFound();
                }

                return Ok(new { country = lstcountry });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetEmpRole")]
        public async Task<IActionResult> ddl_GetEmpRole()
        {
            try
            {
                List<EmployeeGetRoleResponse> lstroles = await _employeeRepository.ddl_GetEmpRole();
                if (lstroles == null)
                {
                    return NotFound();
                }

                return Ok(new { country = lstroles });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpGet("ddl_GetLanguages")]
        public async Task<IActionResult> ddl_GetLanguages()
        {
            try
            {
                List<EmployeeGetLanguageResponse> lstlanguages = await _employeeRepository.ddl_GetLanguages();
                if (lstlanguages == null)
                {
                    return NotFound();
                }

                return Ok(new { states = lstlanguages });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetMaritalStatus")]
        public async Task<IActionResult> ddl_GetMaritalStatus()
        {
            try
            {
                List<EmployeeGetMaritalStatusResponse> lstmstatus = await _employeeRepository.ddl_GetMaritalStatus();
                if (lstmstatus == null)
                {
                    return NotFound();
                }

                return Ok(new { marital_status = lstmstatus });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }




        [HttpGet("ddl_Gender")]
        public IEnumerable<string> GetGenders()
        {
            var genders = new List<string>
        {
            "Male",
            "Female",
            "Other"
        };

            return genders;
        }

        [HttpGet("ddl_Blood_group")]
        public IEnumerable<string> GetBloodGroup()
        {
            var group = new List<string>
            {
                "A+",
                "A-",
                "B+",
                "B-",
                "AB+",
                "AB-",
                "O+",
                "O-",
                "Not Available"
            };
            return group;
        }

 

        // Stored Procedure
        // working to see data in data table 
        [HttpGet("SP_GetEmployeeDetails")]
        public async Task<IActionResult> GetEmployeePersonalDetails(string empCode, string tableName)
        {
            try
            {
                var data = await _employeeRepository.SP_GetEmployeeDetailsTable(empCode, tableName);
                return Ok(data);
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        // working for Employee Trancfer History Details
        [HttpGet("GetEmployeeTransferDetails")]
        public async Task<IActionResult> GetEmployeeTransferDetails(string asEmpCode, string empcode)
        {
            try
            {
                // Call the repository method to get employee transfer details
                List<EmployeeSP_GetEmployeeTransferDetailResponse> employees = await _employeeRepository.SP_GetEmployeeTransferDetails(asEmpCode, empcode);
                return Ok(employees); // Return the list of employees
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }


        // working for PR_GET_EMP_EISDET
        [HttpGet("{empCode}/{tableName}")]
        public async Task<IActionResult> GetEmployeeDetails(string empCode, string tableName)
        {
            try
            {
                var result = await _employeeRepository.SP_GetEmployeeDetails(empCode, tableName);


                // Check if result is an empty array and return it
                if (result is Array array && array.Length == 0)
                {
                    return Ok(array); // Return empty array
                }
                else
                {
                    return Ok(result); // Return actual data
                }
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        // seperation details

        [HttpGet("GetEmployeeSeperationDetails/{empCode}")]
        public IActionResult GetEmployeeSeperationDetails(string empCode)
        {
            var employeeSeperationDetails = _employeeRepository.GetEmployeesSeperationDetails(empCode);

            if (employeeSeperationDetails == null || !employeeSeperationDetails.Any())
            {
                return NotFound();
            }

            return Ok(employeeSeperationDetails);
        }

        // Attachment
        [HttpGet("Attachment/{strEmpCode}")]
        public IActionResult GetCandAttachments(string strEmpCode, string strCndCode, string attachMode)
        {
            try
            {
                //string attachMode = _employeeRepository.GetAttachmentMode();

                List<Dictionary<string, object>> attachments = _employeeRepository.GetAttachments(strEmpCode, strCndCode, attachMode);

                if (attachments != null && attachments.Count > 0)
                {
                    return Ok(attachments);
                }
                else
                {
                    return NotFound("No attachments found.");
                }
            }
            catch (Exception ex)
            {
                // Log the exception
                return StatusCode(500, "An error occurred while retrieving attachments.");
                Console.WriteLine("Error: " + ex.Message); // Log the error message
                throw; // Rethrow the exception
            }
        }

        // Upload

        [HttpPost("upload")]
        public async Task<IActionResult> UploadFiles(List<IFormFile> files, [FromForm] List<string> data, [FromForm] string filePath)
        {
            try
            {
                List<string> fileNames = new List<string>();
                foreach (var file in files)
                {
                    fileNames.Add(file.FileName);
                }

                string transactionId = await _employeeRepository.SaveDataAndFileUpload(data, files, filePath, fileNames);

                if (transactionId.StartsWith("Error"))
                {
                    return BadRequest(transactionId);
                }

                return Ok(new { TransactionId = transactionId });
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        // working for update

        [HttpPut("UpdateEmployeePersonalData/{id}")]
        public async Task<IActionResult> UpdateEmployeeData(string id, EmployeeSP_GetEmployeePersonalDetailsResponse employeeUpdateModel)
        {
            try
            {
                var result = await _employeeRepository.UpdateEmployeeData(id, employeeUpdateModel);

                return result;
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }


        // Insert 

        [HttpPost("Insert_SP")]
        public async Task<IActionResult> InsertEmployeeChange(EmployeeSP_GetEmployeePersonalDetailsResponse employee)
        {
            try
            {
                // Call the repository method to insert employee change
                Employee result = (Employee)await _employeeRepository.InsertEmployeeChanges(employee);

                if (result is OkResult)
                {
                    return Ok("Employee change inserted successfully");
                }
                else if (result is NotFoundObjectResult)
                {
                    return NotFound("Employee not found");
                }
                else
                {
                    // Handle other possible IActionResult results
                    return StatusCode(500, "Internal server error");
                }
            }
            catch (Exception ex)
            {
                // Log the exception
                return StatusCode(500, "Internal server error");
            }
        }



        [HttpPost("Insert")]
        public async Task<IActionResult> InsertEmployeeChange(Employee_Det_Change employee)
        {
            try
            {
                // Call the repository method to insert employee change
                Employee result = await _employeeRepository.InsertEmployeeChange(employee);

                if (result is OkResult)
                {
                    return Ok("Employee change inserted successfully");
                }
                else if (result is NotFoundObjectResult)
                {
                    return NotFound("Employee not found");
                }
                else
                {
                    // Handle other possible IActionResult results
                    return StatusCode(500, "Internal server error");
                }
            }
            catch (Exception ex)
            {
                // Log the exception
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("UpdateEmployeeDetails/{id}")]
        public async Task<IActionResult> UpdateEmployeeDetails(string id, [FromBody] Employee employee)
        {
            if (id != employee.EMP_CODE)
            {
                return BadRequest("Employee ID in the URL does not match the ID in the request body.");
            }

            try
            {
                bool success = await _employeeRepository.UpdateEmployeeAsync(employee);

                if (success)
                {
                    return Ok("Employee updated successfully.");
                }
                else
                {
                    return NotFound("Employee not found or update failed.");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        //  Employee Edit Basic Details

        [HttpPut("UpdateEmployeeBasicDetails/{empCode}")]
        public async Task<IActionResult> UpdateEmployeeBasicDetails(string empCode, [FromBody] EmployeeBasicDetailsUpdateRequest updatedDetails)
        {
            try
            {
                bool result = await _employeeRepository.UpdateEmployeeBasicDetails(empCode, updatedDetails);
                if (result)
                {
                    var successMessage = new List<string?> { "Employee details updated successfully." };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Employee details not found for the provided empCode and seqNo." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        // Add Employee Language

        [HttpPost("AddLanguage")]
        public async Task<IActionResult> AddLanguage([FromBody] AddLanguage newLanguage)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                bool Create = await _employeeRepository.AddLanguage(newLanguage);
                if (Create == true)
                {
                    //return Ok(new { Status = "S", Message = "Designation created successfully", DesignationCode = designation.Id });

                    var successMessage = new List<string?> { "Language added successfully" };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    //return Ok(new { Status = "E", Message = "Designation name already exist.", gradeCode = designation.Id});
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Language name already exist." }, newLanguage?.EMP_CODE);
                }
            }
            catch (RepositoryException ex)
            {
                //return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        // Update Language

        [HttpPut("UpdateLanguage/{EmpCode}/{Language}")]
        public async Task<IActionResult> UpdateLanguage(string EmpCode, string Language, [FromBody] UpdateLanguage updatedLanguage)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
          

                bool isUpdated = await _employeeRepository.UpdateLanguage(EmpCode, Language,updatedLanguage);
                if (isUpdated)
                {
                    var successMessage = new List<string?> { "Language updated successfully" };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "The specified record could not be found." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogError(ex, $"An error occurred in {controllerName}: {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request: {ex.Message}");
            }
        }


        // Contact Details
        // Edit Present Details

        [HttpPut("UpdateEmployeePresentAddressDetails/{empCode}")]
        public async Task<IActionResult> UpdateEmployeePresentDetails(string empCode, [FromBody] EmployeePresentDetailsUpdateRequest updatedDetails)
        {
            try
            {
                bool result = await _employeeRepository.UpdateEmployeePresentDetails(empCode, updatedDetails);
                if (result)
                {
                    var successMessage = new List<string?> { "Employee present details updated successfully." };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Employee present details not found for the provided empCode and seqNo." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        // Edit Permanent Details

        [HttpPut("UpdateEmployeePermanentAddressDetails/{empCode}")]
        public async Task<IActionResult> UpdateEmployeePermanentDetails(string empCode, [FromBody] EmployeePermanentDetailsUpdateRequest updatedDetails)
        {
            try
            {
                bool result = await _employeeRepository.UpdateEmployeePermanentDetails(empCode, updatedDetails);
                if (result)
                {
                    var successMessage = new List<string?> { "Employee permanent details updated successfully." };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Employee permanent details not found for the provided empCode and seqNo." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        // Edit Emergency Details

        [HttpPut("UpdateEmployeeEmergencyAddressDetails/{empCode}")]
        public async Task<IActionResult> UpdateEmployeeEmergencyDetails(string empCode, [FromBody] EmployeeEmergencyDetailsUpdateRequest updatedDetails)
        {
            try
            {
                bool result = await _employeeRepository.UpdateEmployeeEmergencyDetails(empCode, updatedDetails);
                if (result)
                {
                    var successMessage = new List<string?> { "Employee emergency details updated successfully." };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Employee emergency details not found for the provided empCode and seqNo." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        // Employment Details
        // Edit Work and Payroll details

        [HttpPut("UpdateEmployeeWorkPayrollDetails/{empCode}")]
        public async Task<IActionResult> UpdateEmployeeWorkPayrollDetails(string empCode, [FromBody] EmployeeWorkPayrollDetailsUpdateRequest updatedDetails)
        {
            try
            {
                bool result = await _employeeRepository.UpdateEmployeeWorkPayrollDetails(empCode, updatedDetails);
                if (result)
                {
                    var successMessage = new List<string?> { "Employee work and payroll details updated successfully." };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Employee work and payroll details not found for the provided empCode and seqNo." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        // KYC Details
        // Edit KYC details

        [HttpPut("UpdateEmployeeKYCDetails/{empCode}")]
        public async Task<IActionResult> UpdateEmployeeKYCDetails(string empCode, [FromBody] EmployeeKYCUpdateRequest updatedDetails)
        {
            try
            {
                bool result = await _employeeRepository.UpdateEmployeeKYCDetails(empCode, updatedDetails);
                if (result)
                {
                    var successMessage = new List<string?> { "Employee KYC details updated successfully." };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Employee KYC details not found for the provided empCode and seqNo." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        // Add Employee Family

        [HttpPost("AddFamily")]
        public async Task<IActionResult> AddFamily([FromBody] List<AddFamily> newFamilies)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                bool Create = await _employeeRepository.AddFamilies(newFamilies);
                if (Create == true)
                {
                    var successMessage = new List<string?> { "Family member added successfully" };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Family member name already exist." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        // Edit Employee Family

        [HttpPut("UpdateFamilyMembers{empCode}/{seqNo}")]
        public async Task<IActionResult> UpdateEmployeeFamily(string empCode, string seqNo, [FromBody] EmployeeFamilyUpdateRequest updateRequest)
        {
            try { 
            if (updateRequest == null)
            {
                return BadRequest("Update details cannot be null.");
            }

            // Ensure required fields are not null
            if (string.IsNullOrEmpty(updateRequest.MEMBER_NAME) ||
                string.IsNullOrEmpty(updateRequest.DEPENDENT))
            {
                return BadRequest("Some required fields are missing.");
            }

            bool updateSuccessful = await _employeeRepository.UpdateEmpFamilyAsync(empCode, seqNo, updateRequest);

                if (updateSuccessful == true)
                {
                    var successMessage = new List<string?> { "Family member updated successfully" };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {                   
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "The specified record could not be found." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpPut("update-emp-family-pension-pf-grat")]
        public async Task<IActionResult> UpdateEmpFamilyPensionPfGrat(string empCode, string seqNo, [FromBody] EmployeeFamilyNomineeUpdateRequest updatedDetails)
        {
            try
            {
                bool result = await _employeeRepository.UpdateEmpFamilyPensionPfGrat(empCode, seqNo, updatedDetails);
                if (result)
                {
                    var successMessage = new List<string?> { "Employee family details updated successfully." };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Employee family details not found for the provided empCode and seqNo." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        // Upadte employee family nominee multiple request
        [HttpPost("updateFamilyNominee")]
        public async Task<IActionResult> UpdateEmpFamilyPensionPfGrat([FromBody] List<EmployeeFamilyNomineeUpdateMultipleRequest> updatedDetailsList)
        {
            if (updatedDetailsList == null || updatedDetailsList.Count == 0)
            {
                return BadRequest("The request body cannot be null or empty.");
            }

            try
            {
                bool result = await _employeeRepository.UpdateEmpFamilyPensionPfGrat(updatedDetailsList);
                if (result)
                {
                    var successMessage = new List<string?> { "Employee family details updated successfully." };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Employee family details not found for the provided empCode and seqNo." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        // Add Employee Qualification

        [HttpPost("AddQualifiaction")]
        public async Task<IActionResult> AddQualifiaction([FromBody] AddQualification newqualification)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                bool Create = await _employeeRepository.AddQualification(newqualification);
                if (Create == true)
                {
                    var successMessage = new List<string?> { "Qualification added successfully" };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Sequence number already exist." }, newqualification?.EMP_CODE);
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpPut("UpdateEmpQualification/{empCode}/{seqNo}")]
        public async Task<IActionResult> UpdateEmpQualification(string empCode, string seqNo, [FromBody] EmployeeQualificationUpdateRequest updatedDetails)
        {
            try
            {
                bool result = await _employeeRepository.UpdateEmpQualification(empCode, seqNo, updatedDetails);            
                if (result)
                {
                    var successMessage = new List<string?> { "Employee qualification details updated successfully." };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Employee qualification details not found for the provided empCode and seqNo." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

       

        // Add Employee Experience

        [HttpPost("AddExperience")]
        public async Task<IActionResult> AddExperience([FromBody] AddExperience newexperience)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                bool Create = await _employeeRepository.AddExperience(newexperience);
                if (Create == true)
                {
                    var successMessage = new List<string?> { "Experience added successfully" };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Experience name already exist." }, newexperience?.EMP_CODE);
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpPut("UpdateEmpExperience/{empCode}/{fromDate}")]
        public async Task<IActionResult> UpdateEmpExperience(string empCode, DateTime fromDate, [FromBody] EmployeeExperienceUpdateRequest updatedDetails)
        {
            try
            {
                bool result = await _employeeRepository.UpdateEmpExperience(empCode, fromDate, updatedDetails);
                if (result)
                {                  
                    var successMessage = new List<string?> { "Employee experience details updated successfully." };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Employee experience details not found for the provided empCode and seqNo." },"");                  
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        // Employee Assets
        // dropdown Assets

        [HttpGet("ddl_GetAsstes")]
        public async Task<IActionResult> ddl_GetAsstes()
        {
            try
            {
                List<EmployeeGetAssetsResponse> lstassets = await _employeeRepository.ddl_GetAsstes();

                if (lstassets == null)
                {
                    return NotFound();
                }


                return Ok(new { assets = lstassets });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        // Add Employee Assets

        [HttpPost("AddAssets")]
        public async Task<IActionResult> AddAssets([FromBody] AddAssets newassets)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                bool Create = await _employeeRepository.AddAssets(newassets);
                if (Create == true)
                {
                    var successMessage = new List<string?> { "Assets added successfully" };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Assets name already exist." }, newassets?.EMP_CODE);
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpPut("UpdateAssets")]
        public async Task<IActionResult> UpdateEmpInduction([FromQuery] string empCode, [FromQuery] string inductCode, [FromQuery] string tranId, [FromBody] EmployeeAssetsUpdateRequest updatedDetails)
        {
            if (string.IsNullOrEmpty(empCode) || string.IsNullOrEmpty(inductCode))
            {
                return BadRequest("Employee code and induction code are required.");
            }

            try
            {
                bool isUpdated = await _employeeRepository.UpdateEmpInduction(empCode, inductCode, tranId, updatedDetails);

                if (isUpdated == true)
                {
                    var successMessage = new List<string?> { "Employee assets record updated successfully." };
                    return ResponseGen_Helper.CreateResponse("S", successMessage, "");
                }
                else
                {
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Employee induction record not found." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }
    }
}
