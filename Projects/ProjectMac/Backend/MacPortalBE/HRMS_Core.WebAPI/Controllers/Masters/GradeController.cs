﻿using HRMS.ModelCL;
using HRMS.ModelCL.ADMINISTRATION.Masters.Cadre;
using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.ADMINISTRATION.Masters.Grade;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Cadre;
using HRMS.ModelCL.Payloads.Department;
using HRMS.ModelCL.Payloads.Grade;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Concrete.Masters;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.Data.Data;
using HRMS_Core.WebAPI.RequestHelpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;
using System.Net;
using static HRMS_Core.WebAPI.Controllers.CommanController;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace HRMS_Core.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GradeController : ControllerBase
    {
        private readonly AppDbContext _appDbContext;
        private readonly ILogger<GradeController> _logger;
        private readonly IGradeRepository _GradeRepository;
      

        public GradeController(AppDbContext appDbContext, ILogger<GradeController> logger, IGradeRepository grade)
        {
            _appDbContext = appDbContext;
            _logger = logger;
            _GradeRepository = grade;
        }

        [HttpGet("GetAllGrades/query")]
        public async Task<IActionResult> GetAllGrades([FromQuery] PaginationParams paginationParams, string? search = null)
        {
            try
            {  // Retrieve grades
                var (grades, fetchedCount) = await _GradeRepository.GetAllGrades(paginationParams.page, paginationParams.items_per_page, search);

                //var totalGradeCount = _GradeRepository.CountAllGrades();

                Payload payload = GenPageURL_Helper.GeneratePagination(fetchedCount, paginationParams.page, paginationParams.items_per_page, "Grade/GetAllGrades/query");

                // Return the paginated list of grades along with pagination metadata
                return Ok(new { data = grades, payload });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }



        [HttpGet("GetGradeByGradeId/{gradeId}")]
        public async Task<IActionResult> GetGradeByGradeId(string gradeId)
        {
            try
            {
                List<GradeGetGradeByGradeIdResponse> grades = await _GradeRepository.FetchGradeByIdAsync(gradeId);

                if (grades == null || grades.Count == 0)
                {
                    return NotFound();
                }

                // Take the first cadre as a single object for the response
                GradeGetGradeByGradeIdResponse grade = grades[0];

                GradeSingleResponse response = new GradeSingleResponse
                {
                    data = grade,
                };

                return Ok(response);
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpPost("createGrade")]
        public async Task<IActionResult> CreateGrade([FromBody] GradeModel grade)
        {
            try
            {
                if (grade == null)
                {
                    //return Ok(new { Status = "E", Message = "Grade object is null."}); 
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Grade object is null." }, "");
                }
               bool Create = await _GradeRepository.CreateGrade(grade);
                if (Create == true)
                {
                    //return Ok(new { Status = "S", Message = "Grade created successfully", GradeId = grade.id });
                    return ResponseGen_Helper.CreateResponse("S", new List<string?> { "Grade created successfully" }, "");
                }
                else
                {
                    //return Ok(new { Status = "E", Message = "Grade name already exist.", gradeCode = grade.id });
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Grade name already exist." }, "");

                }
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                  string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpPut("UpdateGrade/{gradeCode}")]
        public async Task<IActionResult> UpdateGrade([FromBody] GradeModel grade, string gradeCode)
        {
            try
            {
                if (grade == null)
                {

                    //return Ok(new { Status = "E", Message = "Grade object is null." });
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Grade object is null." }, "");
                }
                else
                {
                    if (gradeCode != grade.id.Trim())
                    {
                        //return Ok(new { Status = "E", Message = "Grade code should be macth in request body." });
                        return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Grade code should be match in request body." }, "");
                    }
                }
                UpdateFlagResponse updated = await _GradeRepository.UpdateGrade(grade);

                if (updated.codeExists == false)
                {
                    //return Ok(new { Status = "E", Message = "Grade code not found", gradeCode = grade.id });
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Grade code not found." }, "");
                }
                if (updated.descrExists == true)
                {
                    //return Ok(new { Status = "E", Message = "Grade name already exist.", gradeName = grade.Description });
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Grade name already exist" }, "");
                }

                //return Ok(new { Status = "S", Message = "Grade Updated successfully", GradeId = grade.id });
                return ResponseGen_Helper.CreateResponse("S", new List<string?> { "Grade Updated successfully" }, "");

            }
            catch (RepositoryException ex)
            {
                //return Ok(new {Status='E',Message=ex.ErrorMessage});
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }

        }


        [HttpGet]
        [Route("GetAll_Cadre")]
        public async Task<IActionResult> GetAll_Cadres()
        {
            try
            {
                List<CadreResp> lstCadres = await _GradeRepository.GetAll_Cadres();
                if (lstCadres == null)
                {
                    return NotFound();
                }
                var datawrapper = new DataWrapper<CadreResp>
                {
                    data = lstCadres
                };
                return Ok(datawrapper);
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

    }
}
