﻿using HRMS.ModelCL.ADMINISTRATION.Masters.CadreBand;
using HRMS.ModelCL;
using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using HRMS.ModelCL.Login;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Department;
using HRMS.RepositoryCL.Concrete.Masters;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.Data.Data;
using HRMS_Core.WebAPI.RequestHelpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using Newtonsoft.Json;
using System.Net.Quic;
using HRMS.ModelCL.Payloads.Qualifcation;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using Microsoft.AspNetCore.Http.HttpResults;
using HRMS.RepositoryCL.Comman;
using System.Diagnostics;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using HRMS.ModelCL.ADMINISTRATION.Masters.Grade;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HRMS_Core.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class QualificationController : ControllerBase
    {
        private readonly ILogger<QualificationController> _logger;
        private readonly IQualificationRepositoy _qualificationRepository;

        public QualificationController(ILogger<QualificationController> logger, IQualificationRepositoy qualification)
        {
            _logger = logger;
            _qualificationRepository = qualification;
        }

        // GET: api/<QualificationController>
        [HttpGet]
        [Route("GetAllQualifications/query")]
        public async Task<IActionResult> GetAllQualifications([FromQuery] PaginationParams paginationParams, string? search = null)
        {
            try
            {
                var (qualifications, fetchedCount) = await _qualificationRepository.GetAllQualifications(paginationParams.page, paginationParams.items_per_page, search);
                      
                var dataList = qualifications.Select(p => new
                {
                    p.Id,
                    p.name,
                    p.Shortdescr,
                    p.Eqival,
                    p.Type,
                    p.ChangeDate,
                    p.ChangeUser,
                    p.ChangeTerm,
                    p.Status,
                    p.ChangeReason

                }).ToList();


                // Populate payload
                Payload payload = GenPageURL_Helper.GeneratePagination(fetchedCount, paginationParams.page, paginationParams.items_per_page, "Qualification/GetAllQualifications/query");
               
                return Ok(new { data = dataList, payload });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ ex.Message}");
            }
        }

        [HttpGet("GetQualificationByCode/{code}")]
        public async Task<IActionResult> GetQualificationByCode(string code)
        {
            try
            {
                //var  qual = await _qualificationRepository.GetQualificationByCode(code);
                //if (qual == null || qual.Count() == 0)
                //{
                //    return NotFound();
                //}
                //else 
                //{
                //    return Ok(new { qual });
                //}

                var qual = await _qualificationRepository.GetQualificationByCode(code);

                if (qual == null || !qual.Any())
                {
                    return NotFound();
                }
                else
                {
                    var qualification = qual.FirstOrDefault();
                    var dataWrapper = new DataWrapperObj<GetQualificationByCodeResponse>
                    {
                        data = qualification
                    };

                    return Ok(dataWrapper); // Return the dataWrapper instance
                }


            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }

        }

        [HttpPost("CreateQualification")]
        public async Task<IActionResult> CreateQualification(Qualification qualification)
        {
            try
            {
                if (qualification == null)
                {
                    //return Ok(new { Status = "E", Message = "Qualification object is null." });
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Qualification object is null." }, "");
                }
                bool Create = await _qualificationRepository.CreateQualification(qualification);

                if (Create == true)
                {
                    //return Ok(new { Status = "S", Message = "Qualification created successfully", QualificationCode = qualification.Id });
                    return ResponseGen_Helper.CreateResponse("S", new List<string?> { "Qualification created successfully." }, "");
                }
                else
                {
                    //return Ok(new { Status = "E", Message = "Qualification name already exist.", QualificationCode = qualification.Id });
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Qualification name already exist." }, "");
                }
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage}, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpPut("UpdateQualification/{Code}")]
        public  async Task<IActionResult> UpdateQualification([FromBody] Qualification qualification,string Code)
        {
            try
            {
                if (qualification == null)
                {
                    //return Ok(new { Status = "E", Message = "Qualification object is null." });
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Qualification object is null" }, "");
                }
                else
                {
                    if (Code != qualification.Id.Trim())
                    {
                        //return Ok(new { Status = "E", Message = "Qualification code should be macth in request body." });
                        return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Qualification code should be macth in request body." }, Code);
                    }
                }
                UpdateFlagResponse updated = await _qualificationRepository.UpdateQualification(qualification);

                if (updated.Success != true)
                {
                    if (updated.codeExists == false)
                    {
                        //return Ok(new { Status = "E", Message = "Qualification code not found", QualificationCode = qualification.Id });
                        return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Qualification code not found." }, Code);
                    }
                    if (updated.descrExists == true)
                    {
                        //return Ok(new { Status = "E", Message = "Qualification name already exist.", QualificationName = qualification.name });
                        return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Qualification name already exist." }, "");
                    }
                }
                //return Ok(new { Status = "S", Message = "Qualification Updated successfully", QualificationCode = qualification.Id });
                return ResponseGen_Helper.CreateResponse("S", new List<string?> { "Qualification Updated successfully." }, Code);

            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpGet]
        [Route("ddl_GetSalaryStruc")]
        public async Task<IActionResult> ddl_GetSalaryStruc()
        {
            try
            {
                List<QualwiseStruc> lstQualwiseStruc = await _qualificationRepository.ddl_GetDistictSalaryStruc();
                if (lstQualwiseStruc == null)
                {
                    return NotFound();
                }
                
                var datawrapper = new DataWrapper<QualwiseStruc>
                {
                    data = lstQualwiseStruc
                };
                return Ok(datawrapper);
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

    }
}
