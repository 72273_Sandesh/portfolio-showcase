﻿using HRMS.ModelCL.Payloads.Employee;
using HRMS.ModelCL.Payloads.Transaction.Affecting;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Concrete.Masters;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS.RepositoryCL.Interface.Transaction;
using HRMS_Core.WebAPI.Controllers.Masters;
using HRMS_Core.WebAPI.RequestHelpers;
using Microsoft.AspNetCore.Mvc;

namespace HRMS_Core.WebAPI.Controllers.Transaction
{
    public class AffectingController : Controller
    {
        private readonly ILogger<AffectingController> _logger;
        private readonly IAffectingRepository _affectingRepository;
     

        public AffectingController(ILogger<AffectingController> logger, IAffectingRepository affecting)
        {
            _logger = logger;
            _affectingRepository = affecting;
        }


        [HttpGet("ddl_GetTransaction")]
        public async Task<IActionResult> ddl_GetTransaction()
        {
            try
            {
                List<GetTransactionResponse> lsttransactions = await _affectingRepository.ddl_GetTransaction();

                if (lsttransactions == null)
                {
                    return NotFound();
                }


                return Ok(new { Transaction = lsttransactions });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetSections")]
        //public async Task<ActionResult<IEnumerable<GetSectionResponse>>> ddl_GetSections([FromQuery] string siteCode, [FromQuery] string deptCode = null)
        public async Task<IActionResult> ddl_GetSections([FromQuery] string siteCode, [FromQuery] string deptCode = null)
        {
            if (string.IsNullOrEmpty(siteCode))
            {
                return BadRequest("SiteCode is required.");
            }

            try
            {
                List<GetSectionResponse> sections = await _affectingRepository.ddl_GetSections(siteCode, deptCode);
                if (sections == null)
                {
                    return NotFound();
                }
                return Ok (new { Sections = sections });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetLocations")]
        //public async Task<ActionResult<IEnumerable<GetSectionResponse>>> ddl_GetSections([FromQuery] string siteCode, [FromQuery] string deptCode = null)
        public async Task<IActionResult> ddl_GetLocations()
        {
            try
            {
                List<GetLocationResponse> sections = await _affectingRepository.ddl_GetLocations();
                if (sections == null)
                {
                    return NotFound();
                }
                return Ok(new { Sections = sections });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetDepartments")]
        public async Task<IActionResult> GetDepartments()
        {
            try
            {
                List< GetDepartmentResponse> departments = await _affectingRepository.ddl_GetDepartments();
                if (departments == null)
                {
                    return NotFound();
                }
                return Ok(new { Departments = departments });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }
    }
}
