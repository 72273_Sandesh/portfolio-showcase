using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Concrete.Login;
using HRMS.RepositoryCL.Concrete.Masters;
using HRMS.RepositoryCL.Interface.Login;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.Data.Data;
using HRMS.ModelCL;
using Serilog;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using HRMS_Core.WebAPI;
using HRMS.RepositoryCL;
using Microsoft.AspNetCore.Authorization;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using HRMS.RepositoryCL.Interface.Transaction;
using HRMS.RepositoryCL.Concrete.Transaction;


var builder = WebApplication.CreateBuilder(args);

// Configuration
var configuration = builder.Configuration;
var logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();

// Logging
builder.Logging.ClearProviders();
builder.Logging.AddSerilog(logger);

// Authentication
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidIssuer = configuration["Jwt:Issuer"],
            ValidAudience = configuration["Jwt:Audiance"],
            IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(configuration["Jwt:Key"]))
        };
    });


// Authorization
builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("RequireAuthentication", policy =>
    {
        policy.RequireAuthenticatedUser()
              .RequireAssertion(context => !context.Resource.ToString().StartsWith("api/Auth/Login"));
    });
});

// Swagger
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo { Title = "HRMS API", Version = "v1" });
    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please enter a valid token",
        Name = "Authorization",
        Type = SecuritySchemeType.Http,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    options.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            },
            new string[] { }
        }
    });
});

// Database
builder.Services.AddDbContext<AppDbContext>(options =>
    options.UseOracle(configuration.GetConnectionString("oracleConnection")));

// Register IDbConnection with OracleConnection
builder.Services.AddScoped<IDbConnection>((sp) =>
    new OracleConnection(configuration.GetConnectionString("oracleConnection")));

// Repository Services
builder.Services.AddScoped<IDepartmentRepository, DepartmentRepository>();
builder.Services.AddScoped<IEmployeeRepository, EmployeeRepository>();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IEncryption, Encryption>();
builder.Services.AddScoped<IQualificationRepositoy, QualificationRepositoy>();
builder.Services.AddScoped<IDesignationRepository, DesignationRepository>();
builder.Services.AddScoped<ICadreRepository, CadreRepository>();
builder.Services.AddScoped<ICadreBandRepository, CadreBandRepository>();
builder.Services.AddScoped<IGradeRepository, GradeRepository>();
builder.Services.AddScoped<IAutoGenerateCodeRepository, AutoGenerateCodeRepository>();
builder.Services.AddSingleton<ITokenVerificationRepository, TokenVerificationRepository>();
builder.Services.AddTransient<IControllerMasterRepository, ControllerMasterRepository>();
builder.Services.AddScoped<ImageFetchingHelper>();


builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

// Transaction
builder.Services.AddScoped<IAffectingRepository, AffectingRepository>();
    
//builder.Services.AddScoped<IAuthorizationRepository, AuthorizationRepository>();
// Add repositories
//builder.Services.AddTransient<ControllerMasterRepository>();

// Configure API behavior options
builder.Services.AddControllers().ConfigureApiBehaviorOptions(options =>
{
    options.InvalidModelStateResponseFactory = actionContext =>
    {
        var modelState = actionContext.ModelState.Values;

        var errorModel = new ErrorModel
        {
            ResponseStatus = "E",
            Message = modelState.SelectMany(x => x.Errors, (x, y) => y.ErrorMessage).ToList()
        };
        return new OkObjectResult(new { data = errorModel });
    };
});

// CORS
builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(policy =>
    {
        policy.AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()
            .WithOrigins("http://localhost:5173");
    });
});

//builder.Services.AddIdentity<ApplicationUser, IdentityRole>()
//               .AddEntityFrameworkStores<AppDbContext>()
//               .AddDefaultTokenProviders();


var app = builder.Build();

// Middleware pipeline configuration
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "HRMS API V1");
        // Uncomment the lines below if OAuth settings are required
        // c.OAuthClientId("swagger-ui");
        // c.OAuthAppName("Swagger UI");
        // c.OAuthUsePkce();
        // c.RoutePrefix = string.Empty;
    });
}

app.UseHttpsRedirection();

app.UseRouting();

app.UseCors();

app.UseAuthentication();

//using (var scope = app.Services.CreateScope())
//{
//    var services = scope.ServiceProvider.GetRequiredService<AuthMiddleware>;
//    services.Invoke();
//}
app.UseAuthorization();

//app.UseMiddleware<AuthMiddleware>();

app.Use(async (context, next) =>
{
    // Check if the request is targeting Swagger UI or Swagger JSON endpoint
    if (!context.Request.Path.StartsWithSegments("/api/Auth/Login"))
    {
        // Check if the user is not authenticated
        if (!context.User.Identity.IsAuthenticated)
        {
            // Return unauthorized response
            context.Response.StatusCode = StatusCodes.Status401Unauthorized;
            context.Response.ContentType = "text/plain"; // Set content type as needed
            var message = "Unauthorized. Please authenticate to access this resource.";
            await context.Response.WriteAsync(message);
            return;
        }
    }
    // Allow other requests to pass through the middleware pipeline
    await next();
    });




app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

app.Run();
