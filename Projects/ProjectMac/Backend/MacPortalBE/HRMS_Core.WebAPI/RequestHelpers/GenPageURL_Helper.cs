﻿using HRMS.ModelCL;
using Microsoft.AspNetCore.Mvc;
using HRMS.ModelCL;
using HRMS.ModelCL.Payloads;

namespace HRMS_Core.WebAPI.RequestHelpers
{
    public static class GenPageURL_Helper
    {
        /* public static Payload GeneratePagination(int totalItems, int currentPage, int itemsPerPage, string baseUrl)
        {
            int totalPages = (int)Math.Ceiling((double)totalItems / itemsPerPage);
            int to = currentPage * itemsPerPage;
            if (to > totalItems)
            {
                to = totalItems;
            }

            Pagination pagination = new Pagination
            {
                page = currentPage,
                first_page_url = $"{baseUrl}?page=1&items_per_page={itemsPerPage}",
                from = (currentPage - 1) * itemsPerPage + 1,
                last_page = totalPages,
                links = GenerateLinks(totalPages, currentPage, itemsPerPage, baseUrl),
                next_page_url = currentPage < totalPages ? $"{baseUrl}?page={currentPage + 1}&items_per_page={itemsPerPage}" : null,
                items_per_page = itemsPerPage.ToString(),
                prev_page_url = currentPage > 1 ? $"{baseUrl}?page={currentPage - 1}&items_per_page={itemsPerPage}" : null,
                to = to,
                total = totalItems
            };

            return new Payload { pagination = pagination };
        }

       private static List<Link> GenerateLinks(int totalPages, int currentPage, int itemsPerPage, string baseUrl)
        {
            var links = new List<Link>();

            for (int i = 1; i <= totalPages; i++)
            {
                string pageUrl = $"{baseUrl}?page={i}&items_per_page={itemsPerPage}";
                bool isActive = i == currentPage;

                Link link = new Link
                {
                    url = pageUrl,
                    label = i.ToString(),
                    active = isActive,
                    page = i
                };

                links.Add(link);
            }

            return links;
        }*/

        //--------------------------------------------------------------------------------------------------------------------------------------------

        public static Payload GeneratePagination(int totalItems, int currentPage, int itemsPerPage, string baseUrl)
        {
            int totalPages = (int)Math.Ceiling((double)totalItems / itemsPerPage);
            int to = Math.Min(currentPage * itemsPerPage, totalItems);

            List<Link> links = GenerateLinks(totalPages, currentPage, itemsPerPage, baseUrl);

            string prevPageUrl = currentPage > 1 ? $"{baseUrl}?page={currentPage - 1}&items_per_page={itemsPerPage}" : null;
            string nextPageUrl = currentPage < totalPages ? $"{baseUrl}?page={currentPage + 1}&items_per_page={itemsPerPage}" : null;

            Pagination pagination = new Pagination
            {
                page = currentPage,
                first_page_url = $"{baseUrl}?page=1&items_per_page={itemsPerPage}",
                from = (currentPage - 1) * itemsPerPage + 1,
                last_page = totalPages,
                links = links,
                next_page_url = nextPageUrl,
                items_per_page = itemsPerPage.ToString(),
                prev_page_url = prevPageUrl,
                to = to,
                total = totalItems
            };

            return new Payload { pagination = pagination };
        }


        private static List<Link> GenerateLinks(int totalPages, int currentPage, int itemsPerPage, string baseUrl)
        {
            var links = new List<Link>();

            // Determine the previous page number
            int previousPage = currentPage > 1 ? currentPage - 1 : 1;

            links.Add(new Link
            {
                // Set the URL for the Previous link
                url = previousPage == currentPage ? null : $"{baseUrl}?page={previousPage}&items_per_page={itemsPerPage}",
                label = "Previous",
                active = false,
                page = previousPage
            });

            for (int i = 1; i <= totalPages; i++)
            {
                string pageUrl = $"{baseUrl}?page={i}&items_per_page={itemsPerPage}";
                bool isActive = i == currentPage;

                Link link = new Link
                {
                    url = pageUrl,
                    label = i.ToString(),
                    active = isActive,
                    page = i
                };

                links.Add(link);
            }

            // Determine the next page number
            int nextPage = currentPage < totalPages ? currentPage + 1 : totalPages;

            links.Add(new Link
            {
                // Set the URL for the Next link
                url = nextPage == currentPage ? null : $"{baseUrl}?page={nextPage}&items_per_page={itemsPerPage}",
                label = "Next",
                active = false,
                page = nextPage
            });

            return links;
        }

    }
}
