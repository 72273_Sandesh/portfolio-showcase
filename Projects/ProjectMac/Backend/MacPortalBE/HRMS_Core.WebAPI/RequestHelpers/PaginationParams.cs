﻿namespace HRMS_Core.WebAPI.RequestHelpers
{
   /* public class PaginationParams
    {
        private const int maxPageSize = 50;
        public int PageNumber { get; set; } = 1;
        private int _pageSize = 6;

        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = value > maxPageSize ? maxPageSize : value;
        }
    }*/

    public class PaginationParams
    {
        private const int maxPageSize = 100;
        public int page { get; set; } = 1;
        private int _items_per_page  = 10;

        public int items_per_page
        {
            get => _items_per_page;
            set => _items_per_page = value > maxPageSize ? maxPageSize : value;
        }
    }
}
