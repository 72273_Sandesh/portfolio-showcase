﻿using HRMS.ModelCL;
using Microsoft.AspNetCore.Mvc;

namespace HRMS_Core.WebAPI.RequestHelpers
{
    public static class ResponseGen_Helper
    {
        public static IActionResult CreateOkResponse<T>(T data)
        {
            return new OkObjectResult(new { data });
        }

        public static IActionResult CreateResponse(string responseStatus, List<string?> messages, string? Code)
        {
            var errorModel = new ResponseStatusModel();
            if (responseStatus == "S")
            {
                errorModel = new ResponseStatusModel { ResponseStatus = responseStatus, Message = messages, Code = Code };
            }
            else
            {
                errorModel = new ResponseStatusModel { ResponseStatus = responseStatus, Message = messages };
            }
            return CreateOkResponse(errorModel);
        }

    }
}
