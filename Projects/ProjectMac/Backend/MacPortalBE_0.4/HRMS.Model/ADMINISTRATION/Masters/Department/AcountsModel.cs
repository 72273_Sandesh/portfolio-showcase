﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.ADMINISTRATION.Masters.Department
{
    public class AcountsModel
    {
        [Key]
        public string? ACCT_CODE { get; set; }
        public string? DESCR { get; set; }
        public string? SH_DESCR { get; set; }
        public char? ACCT_TYPE { get; set; }
        public string? SGROUP_CODE { get; set; }
        public char? ACTIVE { get; set; }
        public char? TYP_BAL { get; set; }
        public string? CURR_CODE { get; set; }
        public string? FORMAT_CODE { get; set; }
        public DateTime?  CHG_DATE { get; set; }
        public string? CHG_USER { get; set; }
        public string? CHG_TERM { get; set; }
        public char? LEDG_POST { get; set; }
        public DateTime? ADD_DATE { get; set; }
        public string? ADD_USER { get; set; }
        public string? ADD_TERM { get; set; }
        public string? SGROUP_CODE__ANAL { get; set; }
        public string? ACCT_CODE__NTYP { get; set; }
        public string? REPEATADDDET { get; set; }
        public char? SUNDRY_TYPE { get; set; }
        public string? UDF_STR1 { get; set; }
        public string? ACCT_CODE__PAR { get; set; }
        public char? NTYP_BAL_TYPE { get; set; }
        public string? LONG_DESCR { get; set; }
        public string? SAP_GL_CODE { get; set; }
    }
}
