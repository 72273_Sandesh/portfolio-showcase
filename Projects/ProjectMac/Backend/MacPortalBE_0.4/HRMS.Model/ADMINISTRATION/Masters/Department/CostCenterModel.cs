﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.ADMINISTRATION.Masters.Department
{
    public class CostCenterModel
    {
        [Key]
        public string? CCTR_CODE { get; set; }
        public string? DESCR { get; set; }
        public string? SH_DESCR { get; set; }
        public string? CCTR_CODE__PAR { get; set; }
        public string? TYPE { get; set; }
        public int?  LEVEL_NO { get; set; }
        public DateTime? CHG_DATE { get; set; }
        public string? CHG_USER { get; set; }
        public string? CHG_TERM { get; set; }
        public string? UDF_STR1 { get; set; }
        public string? DEPT_CODE { get; set; }
        public int? UDF_NUM1 { get; set; }
        public string? ACTIVE { get; set; }
        public string? SAP_CCTR_CODE { get; set; }
    }
}
