﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.ADMINISTRATION.Masters.Department
{
    public class Department
    {
        [Key]
        [Required]
        [Column("DEPT_CODE")]
        public string id { get; set; }

        [Required]
        [Column("DESCR")]
        public string name { get; set; }

        [Column("CHG_DATE")]
        public DateTime ChangeDate { get; set; }

        [Column("CHG_USER")]
        public string ChangeUser { get; set; }

        [Column("CHG_TERM")]
        public string ChangeTerm { get; set; }

        [Column("DEPT_HEAD")]
        public string? DeptHead { get; set; }

        [Column("ACCT_CODE")]
        public string? AcctCode { get; set; }

        [Column("PAY_TABLE")]
        public string? PayTable { get; set; }

        [Column("CCTR_CODE")]
        public string? CctrCode { get; set; }

        [Column("SH_DESCR")]
        public string? ShortDescription { get; set; }

        [Column("ACCT_CODE__AP")]
        public string? AcctCodeAp { get; set; }

        [Column("CCTR_CODE__AP")]
        public string? CctrCodeAp { get; set; }

        [Column("STATUS")]
        public string? Status { get; set; }

        [Column("UDF_NUM1")]
        public int? UdfNumber1 { get; set; }

        [Column("SH_DEPT_CODE")]
        public string? ShDeptCode { get; set; }

        [Column("CHG_REASON")]
        public string? ChangeReason { get; set; }


        //public object Employee { get; set; }
    }
}
