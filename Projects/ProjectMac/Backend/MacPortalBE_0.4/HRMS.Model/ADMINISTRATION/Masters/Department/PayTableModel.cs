﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.ADMINISTRATION.Masters.Department
{
    public class PayTableModel
    {
        [Key]
        public string? PAY_TABLE { get; set; }
        public string? DESCR { get; set; }
        public DateTime? CHG_DATE { get; set; }
        public string? CHG_USER { get; set; }
        public string? CHG_TERM { get; set; }
    }
}
