﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.ADMINISTRATION.Masters.Grade
{
    public class GradeModel
    {
        [Key]
        [Required]
        [Column("GRADE_CODE")]
        public string id { get; set; }

        [Required(ErrorMessage = "Description field is Required.")]
        //[StringLength(50, MinimumLength = 3, ErrorMessage = "Description must be between 3 and 50 characters.")]
        [Column("DESCR")]
        public string Description { get; set; }

        [Column("SH_DESCR")]
        public string? ShortDescription { get; set; }

        [Column("CADRE_CODE")]
        public string? CadreCode { get; set; }

        [Column("LVE_TBLNO")]
        public string? LveTableNumber { get; set; }

        [Column("INDUCT_TBLNO")]
        public string? InductionTableNumber { get; set; }

        [Column("NOTICE_PRD")]
        public int? NoticePeriod { get; set; }

        [Column("CHG_DATE")]
        public DateTime? ChangeDate { get; set; }

        [Column("CHG_USER")]
        public string? ChangeUser { get; set; }

        [Column("CHG_TERM")]
        public string? ChangeTerm { get; set; }

        [Column("APPR_TBLNO")]
        public string? ApprovalTableNumber { get; set; }

        [Column("ACCT_CODE__EXP")]
        public string? AccountCodeExp { get; set; }

        [Column("CCTR_CODE__EXP")]
        public string? CostCenterCodeExp { get; set; }

        [Column("GRADE_TYPE")]
        public string? GradeType { get; set; }

        [Column("UDF_NUM1")]
        public int? UdfNumber1 { get; set; }

        [Column("GRADE_ORDER")]
        public int? GradeOrder { get; set; }

        [Column("GRADE_ID")]
        public string? GradeId { get; set; }


        [Column("CHG_REASON")]
        public string? ChangeReason { get; set; }

        [Column("STATUS")]
        public string? Status { get; set; }
        //public string? CadreName { get; set; }
    }

}
