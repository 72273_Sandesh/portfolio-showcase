﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.ADMINISTRATION.Masters.Site
{
    public class SiteModel
    {
        [Required]
        public string SITE_CODE { get; set; } // Not Null
        public string DESCR { get; set; } // Not Null
        public string? GEO_LOC { get; set; }
        public string? AREA_CODE { get; set; }
        public string? SITE_TYPE { get; set; }
        public string? ADD1 { get; set; }
        public string? ADD2 { get; set; }
        public string? CITY { get; set; }
        public string? PIN { get; set; }
        public string STATE_CODE { get; set; } // Not Null
        public string? TELE1 { get; set; }
        public string? TELE2 { get; set; }
        public string? TELE3 { get; set; }
        public string? MODEM { get; set; }
        public string? FAX { get; set; }
        public string? MEMO { get; set; }
        public string? DATABASE { get; set; }
        public string STAN_CODE { get; set; } // Not Null
        public string? PAY_TABLE { get; set; }
        public string? FIN_ENTITY { get; set; }
        public string? BANK_CODE { get; set; }
        public string? CASH_CODE { get; set; }
        public string? HOL_TBLNO { get; set; }
        public string? UDF1 { get; set; }
        public string? UDF2 { get; set; }
        public DateTime CHG_DATE { get; set; } // Not Null
        public string CHG_USER { get; set; } // Not Null
        public string CHG_TERM { get; set; } // Not Null
        public string? EMAIL_ADDR { get; set; }
        public string? EDI_ADDR { get; set; }
        public string? EMP_CODE__HEAD { get; set; }
        public string? SH_DESCR { get; set; }
        public string? ADD3 { get; set; }
        public string? DOC_PREFIX { get; set; }
        public string? SITE_CODE__SUPP { get; set; }
        public string? SITE_CODE__ACCT { get; set; }
        public string? SITE_CODE__PC { get; set; }
        public string? TAX_ENV_PREFIX { get; set; }
        public string? SITE_CODE__CONS { get; set; }
        public string? ABBRV_DESCR { get; set; }
        public string? SITE_CODE__EXC { get; set; }
        public string? INCR_TBLNO { get; set; }
        public string? EDI_DB { get; set; }
        public string? SITE_CODE__OLD { get; set; }
        public string? DATABASE_ID { get; set; }
        public string? SUPP_CODE { get; set; }
        public string? UDF3 { get; set; }
        public string? UDF4 { get; set; }
        public string? UDF5 { get; set; }
        public string? UDF6 { get; set; }
        public string? DIV_ID__OLD { get; set; }
        public decimal? ADHOC_REPL_PERC { get; set; }
        public string? VAT_YEAR { get; set; }
        public string? ACTIVE_U { get; set; }
        public string? ACCT_CODE__SAL_JV { get; set; }
        public string? CCTR_CODE__SAL_JV { get; set; }
        public decimal? FOOD_TAX { get; set; }
        public decimal? MEDICINE_TAX { get; set; }
        public int? SORT_ORD { get; set; }
        public string? BUDGET_ID { get; set; }
        public string? CONTACT_PERSON { get; set; }
        public string? DIRECTOR { get; set; }
        public string? DUNS_NUMBER { get; set; }
        public string? EOU { get; set; }
        public string? FACILITY_CODE { get; set; }
        public string? FDA_ESTABLSMNT_ID { get; set; }
        public string? GEN_MGR { get; set; }
        public string? GRP_CODE { get; set; }
        public string? ITEM_SER__APPL { get; set; }
        public string? NDC_LABLER { get; set; }
        public string? PRICE_LIST { get; set; }
        public string? PRJ_MGR { get; set; }
        public string? PROJ_CODE { get; set; }
        public string? PUR_MGR { get; set; }
        public string? QUALIFY_GRP__SITE { get; set; }
        public string? REGULATORY_DESCR { get; set; }
        public string? SITE_CODE__BUDGET { get; set; }
        public string? SITE_CODE__OWN { get; set; }
        public string? TECH_DIR { get; set; }
        public string? REGULATORY_DESCR1 { get; set; }
        public string? LOT_PREFIX { get; set; }
        public string? ZONE { get; set; }
        public string? SAP_SITE_CODE { get; set; }
        public string? SAP_SPART { get; set; }
        public string? SAP_PROFIT_CENTER { get; set; }
    }
}
