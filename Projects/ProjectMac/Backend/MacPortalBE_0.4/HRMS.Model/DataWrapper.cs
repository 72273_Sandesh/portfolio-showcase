﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL
{
    public class DataWrapper<T>
    {
        public List<T> data { get; set; }
      
    }

    public class DataWrapperObj<T>
    {
        public T data { get; set; }

    }
}
