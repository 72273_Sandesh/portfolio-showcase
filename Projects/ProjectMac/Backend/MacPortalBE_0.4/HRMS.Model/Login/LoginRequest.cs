﻿namespace HRMS.ModelCL.Login
{
    public class LoginRequest
    {
        public string? empid { get; set; }
        public string? password { get; set; }
        public string? server { get; set; }
    }
}
