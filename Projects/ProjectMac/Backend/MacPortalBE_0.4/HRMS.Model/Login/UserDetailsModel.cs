﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Login
{
    public class UserDetailsModel
    {
        public string retVal { get; set; }
        public string? valMsg { get; set; }
        public string? empFirstName { get; set; }
        public string? empLastName { get; set; }
        public string? empCode { get; set; }
        public string? empEmail { get; set; }
        public string? profileId { get; set; }
        public string? token { get; set; }
        public string? refreshToken { get; set; }
        public DateTime? refreshTokenExpiryTime { get; set; }
        public string? imageUrl { get; set; }


    }
}
