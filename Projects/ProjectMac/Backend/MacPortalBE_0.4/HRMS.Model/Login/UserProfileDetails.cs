﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Login
{
    public class UserProfileDetails
    {
        public string Emp_Code { get; set; }
        public string? Emp_Name { get; set; }
        public string? Cand_Code { get; set; }
        public string? Gender { get; set; }
        public string? DOB { get; set; }
        public string? Mobile_no { get; set; }
        public string? Dept_Code { get; set; }
        public string? Dept_Name { get; set; }
        public string? Designation { get; set; }
        public string? Total_Exp { get; set; }
        public string? Email_Id_Off { get; set; }
        public string? Relieve_Date { get; set; }
        public string? Status { get; set; }
        public string? imageUrl { get; set; }


    }
}
