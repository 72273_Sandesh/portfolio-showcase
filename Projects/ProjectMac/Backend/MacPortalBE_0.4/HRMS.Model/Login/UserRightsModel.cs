﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Login
{
    public class UserRightsModel
    {
        [Key]
        [Column("PROFILE_ID")]
        public string ProfileId { get; set; }

        [Column("APPLICATION")]
        public string Application { get; set; }

        [Column("MENU_ROW")]
        public int MenuRow { get; set; }

        [Column("MENU_COL")]
        public int MenuCol { get; set; }

        [Column("MENU_SUBCOL")]
        public int MenuSubcol { get; set; }

        [Column("LEVEL_4")]
        public int Level4 { get; set; }

        [Column("LEVEL_5")]
        public int Level5 { get; set; }

        [Column("MENU_NAME")]
        public string MenuName { get; set; }

        [Column("RIGHTS")]
        public string Rights { get; set; }

        [Column("ACC_FILT")]
        public string AccFilt { get; set; }

        [Column("DEF_FILT")]
        public string DefFilt { get; set; }

        [Column("OBJ_NAME")]
        public string ObjName { get; set; }

        [Column("FAV_OPTION")]
        public char FavOption { get; set; }

        [Column("FAV_ORDER")]
        public long FavOrder { get; set; }

        [Column("CTR_ENDPOINT_IDS")]
        public string CtrEndpointIds { get; set; }
    }
}
