﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMS.ModelCL.ADMINISTRATION.Masters.Cadre;

namespace HRMS.ModelCL.Payloads.Cadre
{
    public class CadreGetAllResponse:CadreModel
    {
        //public string BandCode { get; set; }
        public string id { get; set; }
        public string CadreCategory { get; set; }

    }
}
