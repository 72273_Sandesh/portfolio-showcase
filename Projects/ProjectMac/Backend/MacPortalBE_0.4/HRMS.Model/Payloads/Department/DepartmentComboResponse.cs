﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Department
{
    public class DepartmentComboResponse
    {
        public List<DepartHead> DEPART_HEAD { get; set; }

        public List<Acct_Code> ACCT_CODE { get; set; }
        public List<Cctr_Code> CCTR_CODE { get; set; }

        public List<Pay_Table> PAY_TABLE { get; set; }
    }

    

    public class DepartHead
    {
        [Column("EMP_CODE")]
        public string EmpCode { get; set; }

        [Column("EMP_CODE")]
        public string ShortName { get; set; }
    }

    public class Acct_Code
    {
        public string ACCT_CODE { get; set; }
        public string DESCR { get; set; }
        public string LONG_DESCR { get; set; }
    }

    public class Cctr_Code
    {
        public string CCTR_CODE { get; set; }
        public string DESCR { get; set; }
    }

    public class Pay_Table
    {
        public string PAY_TABLE { get; set; }
        public string DESCR { get; set; }
    }
}
