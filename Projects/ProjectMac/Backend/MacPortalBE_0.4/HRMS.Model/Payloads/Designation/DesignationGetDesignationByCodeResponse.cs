﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Designation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Designation
{
    public class DesignationGetDesignationByCodeResponse : DesignationModel
    {
        public string design_code { get; set; }
        public string GradeName {  get; set; } 
    }
}
