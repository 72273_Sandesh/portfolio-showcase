﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Documents
{
    public class UploadRequest
    {
        public List<string> Data { get; set; }
        public string FilePath { get; set; }
    }

}
