﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Emp
{
    public class EmployeeBasicDetailsUpdateRequest
    {
        //public UpdateEmployeeForm UpdatedDetails 
        public string EMP_FNAME {  get; set; }
        public string EMP_MNAME {  get; set; }
        public string EMP_LNAME { get; set; }
        public char SEX {  get; set; }
        public DateTime? BIRTH_DATE { get; set; }
        public string? RELIGION { get; set; }
        public string? BLOOD_GRP { get; set; }
        public string? FATHER_NAME { get; set; }
        public string? MOTHER_NAME { get; set; }
        public string? MOBILE_NO { get; set; }
        public string? EMAIL_ID_PER {  get; set; }
    }
}
