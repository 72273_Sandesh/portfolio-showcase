﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Emp
{
    public class EmployeeEmergencyDetailsUpdateRequest
    {
        public string? CONTACT_PERS {  get; set; }
        public string? CONTACT_ADD1 { get; set; }
        public string? CONTACT_ADD2 { get; set; }
        public string? CONTACT_ADD3 { get; set; }
        public string? CONTACT_CITY { get; set; }
        public string? CONTACT_STATE { get; set; }
        public string? CONTACT_TEL { get; set; }
        public string? CONTACT_PIN { get; set; }
    }
}
