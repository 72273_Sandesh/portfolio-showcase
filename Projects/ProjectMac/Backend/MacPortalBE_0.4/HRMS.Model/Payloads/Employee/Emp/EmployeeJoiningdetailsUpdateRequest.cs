﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Emp
{
    public class EmployeeJoiningdetailsUpdateRequest
    {
        public string? REPORT_TO { get; set; }
        public int? PROBATION_PRD { get; set; }       
        public int? TRAINING_PRD { get; set; }

    }
}
