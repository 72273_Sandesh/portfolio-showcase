﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Emp
{
    public class EmployeePermanentDetailsUpdateRequest
    {
        public string? PER_ADD1 { get; set; }    
        public string? PER_ADD2 { get; set; }
        public string? PER_ADD3 { get; set; }
        public string? PER_CITY { get; set; }
        public string? PER_STATE { get; set; }
        public string? PER_PIN { get; set; }
        public string? PER_TEL { get; set; }
    }
}
