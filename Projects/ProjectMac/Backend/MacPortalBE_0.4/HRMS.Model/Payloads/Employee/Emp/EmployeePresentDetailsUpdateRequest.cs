﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Emp
{
    public class EmployeePresentDetailsUpdateRequest
    {
        public string? CUR_ADD1 { get; set; }
        public string? CUR_ADD2 { get; set; }
        public string? CUR_ADD3 { get; set; }
        public string? CUR_CITY { get; set; }
        public string? CUR_STATE { get; set; }
        public string? CUR_PIN { get; set; }
        public string? CUR_TEL1 { get; set; }
        public string? CUR_TEL2 { get; set; }
        public string? CUR_TEL3 { get; set; }

    }
}


