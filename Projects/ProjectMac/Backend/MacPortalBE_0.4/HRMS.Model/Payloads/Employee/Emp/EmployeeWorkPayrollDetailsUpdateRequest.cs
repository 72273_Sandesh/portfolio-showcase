﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Emp
{
    public class EmployeeWorkPayrollDetailsUpdateRequest
    {
        public string? WORK_SITE {  get; set; }
        public string? PAY_SITE { get; set; }
        public DateTime? GRATUITY_DATE { get; set; }
        public int? NOTICE_PRD { get; set; }

    }
}
