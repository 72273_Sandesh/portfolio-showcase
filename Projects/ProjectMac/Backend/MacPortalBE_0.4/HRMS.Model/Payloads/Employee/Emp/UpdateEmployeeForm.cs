﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace HRMS.ModelCL.Payloads.Employee.Emp
{
    public class UpdateEmployeeForm
    {
        //public EmployeeBasicDetailsUpdateRequest UpdatedDetails { get; set; }
        public string? userId { get; set; }
        public string? userIpAddress { get; set; }
        public List<IFormFile?> SupportDocs { get; set; }
        public string? FileName { get; set; }
        public string? HidDocsId { get; set; }
        public string? StorageCode { get; set; }

    }
}
