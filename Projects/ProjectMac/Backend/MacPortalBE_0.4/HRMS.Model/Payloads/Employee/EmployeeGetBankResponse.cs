﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeGetBankResponse
    {
        public string BANK_CODE { get; set; }
        public string BANK_NAME { get; set; }
        public string BANK_TYPE { get; set;}
    }
}
