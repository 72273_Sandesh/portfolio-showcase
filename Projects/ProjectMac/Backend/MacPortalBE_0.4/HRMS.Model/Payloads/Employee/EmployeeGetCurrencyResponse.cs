﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeGetCurrencyResponse
    {
        public string DESCR {  get; set; }
        public string CURR_CODE { get; set; }
    }
}
