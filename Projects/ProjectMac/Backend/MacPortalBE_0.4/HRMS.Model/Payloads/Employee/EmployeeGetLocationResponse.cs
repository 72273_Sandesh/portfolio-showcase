﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeGetLocationResponse
    {
        public string SiteCode { get; set; }
        public string Description { get; set; }
        public string AreaCode { get; set; }
        public string Address { get; set; }
        public string FinancialEntity { get; set; }
    }
}
