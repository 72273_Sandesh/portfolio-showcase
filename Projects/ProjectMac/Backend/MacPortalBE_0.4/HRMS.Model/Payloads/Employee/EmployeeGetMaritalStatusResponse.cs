﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeGetMaritalStatusResponse
    {
        public string FLD_VALUE {  get; set; }
        public string DESCR { get; set; }
    }
}
