﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeGetQualificationResponse
    {
        public string QLF_CODE { get; set; }
        public string QUALIFICATION { get; set; }
        public string SH_DESCR { get; set; }
        public string TYPE { get; set; }
    }
}
