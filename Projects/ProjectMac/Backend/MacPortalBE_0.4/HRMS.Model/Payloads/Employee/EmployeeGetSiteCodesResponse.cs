﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeGetSiteCodesResponse
    {
        public string SITE_CODE { get; set; }
        public string DESCR { get; set; }
        public string DIV_ID__OLD { get; set; }
        public string LOC_GROUP { get; set; }
    }
}
