﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeSP_GetEmployeeFamilyDetailResponse
    {
        public string EMP_CODE { get; set; }
        public int SEQ_NO { get; set; }
        public string MEMBER_NAME { get; set; }
        public DateTime? DATE_BIRTH { get; set; }
        public string SEX { get; set; }
        public string RELATION { get; set; }
        public string? OCCUPATION { get; set; }
        public string CHG_USER { get; set; }
        public DateTime CHG_DATE { get; set; }
        public string CHG_TERM { get; set; }
        public string QLF_CODE__FMLY { get; set; }
        public string GENDER_DESC { get; set; }
        public string DEPENDENT { get; set; }
    }
}
