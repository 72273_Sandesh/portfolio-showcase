﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeSP_GetEmployeeMediclaimDetailResponse
    {
        public string MEMBER_NAME { get; set; }
        public string GENDER { get; set; }
        public string RELATION {  get; set; }
        public DateTime? DATE_BIRTH { get; set; }
        public string MEDICLAIM_STATUS { get; set; }
        public string GENDER_DESC { get; set; }
    }
}
