﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeSP_GetEmployeeOfficialDetailResponse
    {
        public string MODULE_NAME { get; set; }
        public string SAN_EMPNAME1 { get; set; }
        public string SAN_EMPNAME2 { get; set; }
        public string SAN_EMPNAME3 { get; set; }
        public string SAN_EMPNAME4 { get; set; }
        public string SAN_EMPNAME5 { get; set; }

    }
}
