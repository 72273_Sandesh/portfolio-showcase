﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee
{
    public class EmployeeSP_GetEmployeeTransferDetailResponse
    {
        public string P_EMPID { get; set; }
        public long P_SRNO { get; set; }
        public string P_TRANSACTION_TYPE { get; set; }
        public string P_DIV_ID { get; set; }
        public string P_DEPT_ID { get; set; }
        public string EMPNAME { get; set; }
        public string P_SECTION_ID { get; set; }
        public DateTime P_DATE_OF_TRANS_PROMO { get; set; }
        public string P_LEVEL_ID { get; set; }
        public string P_GRADE_ID { get; set; }
        public string P_SECTION_NAME { get; set; }
        public string P_DEPT_NAME { get; set; }
        public string P_DESIG { get; set; }
        public long P_CTC { get; set; }
        public string P_REASON { get; set; }
        public long P_STRUCTURE_SRNO { get; set; }
        public string P_GRADE_DESC { get; set; }
        public string P_LEVEL_NAME { get; set; }
        public string P_DB { get; set; } //
        public string DOC_PATH { get; set; } //
        public string VISI { get; set; } //
    }
}
