﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Family
{
    public class EmpFamilyEntity
    {
        [Key]
        [Column(Order = 1)] // Order is important for composite keys
        public string EMP_CODE { get; set; }

        [Key]
        [Column(Order = 2)]
        public string SEQ_NO { get; set; }
        public string MEMBER_NAME { get; set; }
        public DateTime? DATE_BIRTH { get; set; }
        public string SEX { get; set; }
        public string RELATION { get; set; }
        public string OCCUPATION { get; set; }
        public string DEPENDENT { get; set; }
        public string CHG_USER { get; set; }
        public DateTime? CHG_DATE { get; set; }
        public string CHG_TERM { get; set; }
        public string PF_NOMINEE { get; set; }
        public decimal? PF_PERC { get; set; }
        public string? GRAT_NOMINEE { get; set; }
        public decimal? GRAT_PERC { get; set; }
        public string PEN_NOMINEE { get; set; }
        public decimal? PEN_PERC { get; set; }
        public char? PHYSICAL_STATUS { get; set; }
        public char? QLF_CODE { get; set; }
        public char? ADOPTED { get; set; }
        public char? COMP_NOMINEE { get; set; }
        public decimal? COMP_PERC { get; set; }
        public char? MEDI_NOMINEE { get; set; }
        public char? ACC_INC_NOMINEE { get; set; }
        public decimal? ACC_INC_PERC { get; set; }
        public char? DEATH_NOMINEE { get; set; }
        public decimal? DEATH_PERC { get; set; }
    }
    }
