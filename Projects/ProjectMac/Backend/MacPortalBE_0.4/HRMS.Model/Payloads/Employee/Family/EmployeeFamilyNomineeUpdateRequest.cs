﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Family
{
    public class EmployeeFamilyNomineeUpdateRequest
    {
        public string? PF_NOMINEE { get; set; }
        public decimal? PF_PERC { get; set; }
        public string? GRAT_NOMINEE { get; set; }
        public decimal? GRAT_PERC { get; set; }
        public string? PEN_NOMINEE { get; set; }
        public decimal? PEN_PERC { get; set; }
        public string? COMP_NOMINEE { get; set; }
        public decimal? COMP_PERC { get; set; }
        public string? MEDI_NOMINEE { get; set; }
        public string? ACC_INC_NOMINEE { get; set; }
        public decimal? ACC_INC_PERC { get; set; }
        public string? DEATH_NOMINEE { get; set; }
        public decimal? DEATH_PERC { get; set; }
    }
}
