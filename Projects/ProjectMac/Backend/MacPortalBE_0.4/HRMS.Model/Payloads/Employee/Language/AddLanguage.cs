﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Employee.Language
{
    public class AddLanguage
    {
        [Key]
        [Required]
        public string EMP_CODE { get; set; }
        [Required]
        public string LANGUAGE { get; set; }
        public char? READ { get; set; }
        public char? WRITE { get; set; }
        public char? SPEAK { get; set; }
        public char? WORK_PROFICIENCY { get; set; }
    }
}
