﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads
{
    public class GetPayloads_Page
    {
        public Payload GetPayload()
        {
            Payload payobj = new Payload()
            {
                pagination = new Pagination()
                {
                    page = 1,
                    first_page_url = "/?page=1",
                    from = 1,
                    last_page = 5,
                    next_page_url = null,
                    links = new List<Link>()
                {
                    new Link()
                    {
                        url = null,
                        label = "&laquo; Previous",
                        active = false,
                        page = null
                    },
                    new Link()
                    {
                        url = "/?page=1",
                        label = "1",
                        active = true,
                        page = 1
                    },
                    new Link()
                    {
                        url = null,
                        label = "Next &raquo;",
                        active = false,
                        page = null
                    }
                },
                    items_per_page = "100",
                    prev_page_url = "",
                    to = 20,
                    total = 21
                }
            };

            return payobj;
        }
    }
}
