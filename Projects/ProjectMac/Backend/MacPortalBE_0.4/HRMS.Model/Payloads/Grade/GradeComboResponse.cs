﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Grade
{
    public class GradeComboResponse
    {
        public List<CadreResp> CADRE {  get; set; }
    }

    public class CadreResp
    {
        [Column("CADRE_CODE")]
        public string CadreCode { get; set; }

        [Column("DESCR")]
        public string Cadre { get; set; }

        [Column("SH_DESCR")]
        public string ShortDescription { get; set; }

        [Column("BAND_CODE")]
        public string BandCode { get; set; }
    }
}
