﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Recruitment
{
    public class Interview_DetailResponse
    {
        public string REF_NO {  get; set; }
        public DateTime START_DATE { get; set; }
        public string STATUS { get; set; }
        public string PANEL { get; set; }
        public string COMMENTS_PANEL { get; set; }
        public string SUPP_CODE__CON { get; set; }
        public string SOURCE { get; set; }
        public string REMARKS { get; set; }

    }
}
