﻿namespace HRMS.ModelCL.Payloads
{
    public class TokenVerifyResponse
    {
        public string api_token { get; set; }
        public string? Emp_Name { get; set; }
        public string Emp_Code { get; set; }
        public string? Cand_Code { get; set; }
        public string? Gender { get; set; }
        public string? DOB { get; set; }
        public string? Mobile_no { get; set; }
        public string? Dept_Code { get; set; }
        public string? Dept_Name { get; set; }
        public string? Total_Exp { get; set; }
        public string? Email_Id_Off { get; set; }
        public string? Relieve_Date { get; set; }
        public string? imageUrl { get; set; }
        public string? Status { get; set; }
        public string? Designation { get; set; }
        public DateTime created_at { get; set; }
        // public string email { get; set; }
        public DateTime email_verified_at { get; set; }
        public string first_name { get; set; }
        public string id { get; set; }
        public string last_name { get; set; }
        public DateTime updated_at { get; set; }
        public string? refreshToken { get; set; }
        public DateTime? refreshToken_ExpiryTime { get; set; }



        //public LoginRespDTO(String api_token, DateTime created_at, String email, DateTime email_verified_at, String firstname, int id, String lastname, DateTime updated_at)
        //{
        //    this.api_token = api_token;
        //    this.created_at = created_at;
        //    this.email = email;
        //    this.email_verified_at = email_verified_at;
        //    this.first_name = firstname;
        //    this.id = id;
        //    this.last_name = lastname;
        //    this.updated_at = updated_at;
        //}
    }
}
