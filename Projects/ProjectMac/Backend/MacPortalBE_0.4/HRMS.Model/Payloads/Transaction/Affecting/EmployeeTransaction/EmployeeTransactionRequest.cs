﻿using HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction;
using System;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting.employeeTransaction
{
    public class EmployeeTransactionRequest
    {
        //public EmployeeBankDetRequest employeeBankDetRequests {  get; set; }
    }

    public class EmployeeBankDetRequest
    {
        public string EMP_CODE { get; set; }
        public string TRAN_ID { get; set; }
        public string TABLE_NAME { get; set; }
        public string FORM_NAME { get; set; }
        public string KEY_VAR { get; set; }
        public int WF_LEVEL { get; set; }
        public string CONF { get; set; }
        public string SITE_CODE { get; set; }
        public string EMP { get; set; }
        public string FILE_PATH { get; set; }
        public string FILE { get; set; }
        public string FOLDER_PATH { get; set; }
        public string CHG_USER { get; set; }
        public string CHG_TERM { get; set; }
        public string CHG_DATE { get; set; }
        public string REMARKS { get; set; }
        public string WORK_SITE { get; set; }
        public string GRATUITY_DATE { get; set; }
        public int NOTICE_PRD { get; set; }
        public decimal BASIC { get; set; }
        public decimal CURR_CTC { get; set; }
        public string PAY_MODE { get; set; }
        public string PAY_TYPE { get; set; }
        public string BANK_CODE { get; set; }
        public string BANK_IFSC { get; set; }
        public string BANK_NAME { get; set; }
        public string BANK_ACCT { get; set; }
        public string DD_PAYABLE_BANK { get; set; }
        public string PDF { get; set; }
        public string EMP_IMAGE { get; set; }
    }
}
