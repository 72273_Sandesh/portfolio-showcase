﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting
{
    public class GetLocationResponse
    {
        public string SITE_CODE { get; set; }
        public string DESCR { get; set; }
        public string AREA_CODE { get; set; }
        public string ADDRESS { get; set; }
        public string FIN_ENTITY { get; set; }
    }
}
