﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction
{
    public class FieldUpdateRequest
    {
        public string FIELD_NAME { get; set; }
        public string FIELD_DESCRIPTION { get; set; }
        public string OLD_KEY { get; set; }
        public string NEW_KEY { get; set; }
        public string OLD_VALUE { get; set; }
        public string NEW_VALUE { get; set; }
        public int SEQ_NO { get; set; } 
        public char VISIBILITY { get; set; }
    }
}
