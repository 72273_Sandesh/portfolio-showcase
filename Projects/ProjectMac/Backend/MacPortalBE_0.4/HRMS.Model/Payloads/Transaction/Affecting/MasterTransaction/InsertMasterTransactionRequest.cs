﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction
{
    public class InsertMasterTransactionRequest
    {
        public string TableName { get; set; }
        public string? KeyVar { get; set; }
        public int WFLevel { get; set; }
        public char Conf { get; set; }
        public string SiteCode { get; set; }
        public string Emp { get; set; }
        public string? FilePath { get; set; }
        public IFormFile? File { get; set; }
        public string? FolderPath { get; set; }
        public string? strTranId { get; set; }
        public string? remarks { get; set; }
        public string tx_chg_Term { get; set; }
        public List<FieldUpdateRequest> FieldUpdates { get; set; }
    }
}
