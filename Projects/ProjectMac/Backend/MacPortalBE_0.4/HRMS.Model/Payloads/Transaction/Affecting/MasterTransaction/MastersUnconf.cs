﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction
{
    public class MastersUnconf
    {
        [Key]
        public string TRAN_ID { get; set; }
        public DateTime TRAN_DATE { get; set; }
        public string TABLE_NAME { get; set; }
        public string? KEY_VAR { get; set; }
        public int WF_PROCESS_LEVEL { get; set; }
        public string SQL_QRY { get; set; }
        public char CONFIRMED { get; set; }
        public DateTime CHG_DATE { get; set; }
        public string CHG_USER { get; set; }
        public string CHG_TERM { get; set; }
        public string? EMP_CODE { get; set; }
        public string? REMARKS { get; set; }
        public string? FILE_PATH { get; set; }

        
        public ICollection<MastersUnconfDet> Details { get; set; }
    }
}




