﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction
{
    public class MastersUnconfDet
    {
        [Key]
        public string TRAN_ID { get; set; }
        public string FIELD_NAME { get; set; }
        public string? OLD_VALUE { get; set; }
        public string? NEW_VALUE { get; set; }
        public string? FILE_PATH { get; set; }
        public DateTime? CHG_DATE { get; set; }
        public string CHG_USER { get; set; }
        public string CHG_TERM { get; set; }

       
        public MastersUnconf Master { get; set; }
    }
}
