﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction
{
    public class MastersUnconfWithDetailsForID
    {
        [Column("TRAN_ID")]
        public string id { get; set; }
        public DateTime TRAN_DATE { get; set; }
        public string TABLE_NAME { get; set; }
        public string KEY_VAR { get; set; }
        public int WF_PROCESS_LEVEL { get; set; }
        public string SQL_QRY { get; set; }
        public char CONFIRMED { get; set; }
        public DateTime TX_CHG_DATE { get; set; }
        public string TX_CHG_USER { get; set; }
        public string TX_CHG_TERM { get; set; }
        public string EMP_CODE { get; set; }
        public string REMARKS { get; set; }
        public string FILE_PATH { get; set; }
        public Dictionary<string, FieldValues> FieldNames { get; set; } = new Dictionary<string, FieldValues>();
        //public List<FieldItem> FieldNames { get; set; }
        public List<Reviewer> Reviewers { get; set; } = new List<Reviewer> ();
    }

    public class FieldItem
    {
        public string FieldName { get; set; }
        public FieldValues FieldValues { get; set; }
    }
    public class Reviewer
    {
        public string TRAN_ID { get; set; }
        public int LEVEL_NO { get; set; }
        public string EMP_CODE__REVIEW { get; set; }
        public string SHORT_NAME { get; set; }
        public string CONFIRMED { get; set; }
        public DateTime? CHG_DATE { get; set; }

        [Column("REMARKS")]
        public string REASON { get; set; }
    }

    public class FieldValues
    {
        public string FIELD_DESCRIPTION { get; set; }
        public string OLD_KEY { get; set; }
        public string OLD_VALUE { get; set; }
        public string NEW_KEY { get; set; }
        public string NEW_VALUE { get; set; }
        public int SEQ_NO { get; set; }
    }

}
