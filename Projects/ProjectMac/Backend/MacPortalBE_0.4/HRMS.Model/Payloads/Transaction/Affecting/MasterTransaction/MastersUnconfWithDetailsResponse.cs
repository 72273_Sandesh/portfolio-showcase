﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction
{
    public class MastersUnconfWithDetails
    {       
        [Column("TRAN_ID")]
        public string id { get; set; }
        public DateTime TRAN_DATE { get; set; }
        public string TABLE_NAME { get; set; }
        public string? KEY_VAR { get; set; }
        public int WF_PROCESS_LEVEL { get; set; }
        public string SQL_QRY { get; set; }
        public char CONFIRMED { get; set; }
        public DateTime CHG_DATE { get; set; }
        public string CHG_USER { get; set; }
        public string CHG_TERM { get; set; }
        public string? EMP_CODE { get; set; }
        public string? REMARKS { get; set; }
        public string? FILE_PATH { get; set; }

        public string? NEW_VALUE {  get; set; }

        public string? APPROVER_NAME { get; set; }
        public string? APPROVER_CODE { get; set; }
        public string? INITIATED_AS {  get; set; }
        public string? INITIATED_CODE { get; set; }

        //public List<Details> Details { get; set; } = new List<Details>();
    }

    /*public class Details
    {
        public string TRAN_ID { get; set; }
        public string FIELD_NAME { get; set; }
        public string OLD_VALUE { get; set; }
        public string NEW_VALUE { get; set; }
        public string FILE_PATH_DET { get; set; }
        public DateTime? CHG_DATE_DET { get; set; }
        public string CHG_USER_DET { get; set; }
        public string CHG_TERM_DET { get; set; }
    }*/


}
