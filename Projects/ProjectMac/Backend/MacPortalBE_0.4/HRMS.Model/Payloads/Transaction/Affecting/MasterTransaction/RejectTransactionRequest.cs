﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction
{
    public class RejectTransactionRequest
    {
        public string TransactionId { get; set; }
        public string EmpCode { get; set; }
        public string RemoteAddr { get; set; }
        public string Remarks { get; set; }
    }
}
