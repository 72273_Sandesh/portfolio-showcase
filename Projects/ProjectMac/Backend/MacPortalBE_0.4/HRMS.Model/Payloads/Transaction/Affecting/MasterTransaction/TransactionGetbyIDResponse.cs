﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction
{
   
    public class TransactionGetbyIDResponse
    {
        [Column("TRAN_ID")]
        public string? id { get; set; }
        public DateTime CHG_DATE { get; set; }
        public string? CHG_USER { get; set; }
        public string? CHG_TERM { get; set; }
        public string? FILE_PATH { get; set; }
        public Dictionary<string, FieldNameDetails> FieldNames { get; set; }
    }

    public class FieldNameDetails
    {
        public string OLD_VALUE { get; set; }
        public string NEW_VALUE { get; set; }
    }

}
