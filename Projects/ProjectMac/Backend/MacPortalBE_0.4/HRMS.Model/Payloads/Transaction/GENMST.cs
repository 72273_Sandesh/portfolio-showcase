﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads.Transaction
{
    public class GENMST
    {
        public string FLD_NAME { get; set; }
        
        [Key]
        [Required]
        public string MOD_NAME { get; set; }
        public string? DESCR { get; set; }
        public string? ERROR_CD { get; set; }
        public string?   BLANK_OPT { get; set; }
        public string? FLD_TYPE { get; set; }
        public string? FLD_MIN { get; set; }
        public string? FLD_MAX { get; set; }
        public string? VAL_TYPE { get; set; }
        public DateTime? CHG_DATE { get; set; }
        public string? CHG_USER { get; set; }
        public string? CHG_TERM { get; set; }
        public string? VAL_TABLE { get; set; }
        public string? SQL_INPUT { get; set; }
        public int? FLD_WIDTH { get; set; }
        public string? UDF_USAGE_1 { get; set; }
        public string? UDF_USAGE_2 { get; set; }
        public string? UDF_USAGE_3 { get; set; }
        public string? VAL_STAGE { get; set; }
        public string? OBJ_NAME { get; set; }
        public string? FORM_NO { get; set; }
    }
}
