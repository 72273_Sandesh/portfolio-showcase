﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.ModelCL.Payloads
{
    public class UpdateFlagResponse
    {
        public bool descrExists { get; set; }
        public bool codeExists { get; set; }
        public bool Success { get; set; }
    }
}
