﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace HRMS.ModelCL.Payloads
{
    public class Initials
    {
        public string label { get; set; }
        public string state { get; set; }
    }

    public class Link
    {
        public string url { get; set; }
        public string label { get; set; }
        public bool active { get; set; }
        public int? page { get; set; }
    }

    public class Pagination
    {
        public int page { get; set; }
        public string first_page_url { get; set; }
        public int from { get; set; }
        public int last_page { get; set; }
        public List<Link> links { get; set; }
        public string next_page_url { get; set; }
        public string items_per_page { get; set; }
        public object prev_page_url { get; set; }
        public int to { get; set; }
        public int total { get; set; }
    }

    public class Payload
    {
        public Pagination pagination { get; set; }
    }


    /*public class data
    {
        //public int id { get; set; }
        public string? code { get; set; } 
        *//*public string name { get; set; }
        public string avatar { get; set; }
        public string email { get; set; }
        public string position { get; set; }
        public string role { get; set; }
        public string last_login { get; set; }
        public bool two_steps { get; set; }
        public string joined_day { get; set; }
        public bool online { get; set; }*//*
        // Department Fields

        public int? id { get; set; }
        public string DEPT_CODE { get; set; } //DEPT_CODE
        public string name { get; set; } // DESCR
        public DateTime CHG_DATE { get; set; }
        public string CHG_USER { get; set; }
        public string CHG_TERM { get; set; }

        // Nullable fields
        public string? DEPT_HEAD { get; set; }
        public string? ACCT_CODE { get; set; }
        public string? PAY_TABLE { get; set; }
        public string? CCTR_CODE { get; set; }
        public string? SH_DESCR { get; set; }
        public string? ACCT_CODE__AP { get; set; }
        public string? CCTR_CODE__AP { get; set; }
        public string? STATUS { get; set; }
        public int? UDF_NUM1 { get; set; }
        public string? SH_DEPT_CODE { get; set; }
        public Initials initials { get; set; }
    }*/
}
