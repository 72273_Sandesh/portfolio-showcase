﻿using HRMS_Core.Data.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;

namespace HRMS.RepositoryCL.Comman
{
    public static class ControllerIDGenerator
    {
        private static readonly Dictionary<string, int> controllerCounters = new Dictionary<string, int>();
        private static readonly Dictionary<string, int> endpointCounters = new Dictionary<string, int>();

        public static string GenerateControllerId(string name, AppDbContext appDbContext)
        {
            try
            {
                string prefix = "CT";
                //bool controllerExists = CheckControllerExists(name, appDbContext);

                //if (controllerExists)
                //{
                //    return GetExistingControllerId(name, appDbContext);
                //}
                //else
                //{
                //int counter = GetControllerCounter(prefix);
                //string counterString = (++counter % 1000).ToString("D3");
                //controllerCounters[prefix] = counter;
                //return $"{prefix}{counterString}";

                int nextSequence = GetNextControllerSequence(prefix, appDbContext);
                string counterString = (nextSequence % 1000).ToString("D3");
                controllerCounters[prefix] = nextSequence;
                return $"{prefix}{counterString}";
                //}
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static string GenerateEndpointId(string controllerId, AppDbContext appDbContext)
        {
            try
            {
                string prefix = controllerId.Substring(0, 5);
                bool endpointExists = CheckEndpointExists(controllerId, appDbContext);

                if (endpointExists)
                {
                    var nextSequence = GetNextEndpointSequence(prefix, appDbContext);
                    string counterString = (nextSequence.Result % 100).ToString("D2");
                    endpointCounters[prefix] = nextSequence.Result;
                    return $"{controllerId}_E{counterString}";
                }
                else
                {
                    int counter = 0;//GetEndpointCounter(prefix);
                    string counterString = (++counter % 100).ToString("D2");
                    endpointCounters[prefix] = counter;
                    return $"{controllerId}_E{counterString}";
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private static bool CheckControllerExists(string name, AppDbContext appDbContext)
        {
            return appDbContext.CONTROLLER_MASTER.Any(c => c.CtrlName == name);
        }

        private static bool CheckEndpointExists(string controllerId, AppDbContext appDbContext)
        {
            return appDbContext.CONTROLLER_MASTER.Any(endpoint => endpoint.CtrlId == controllerId);
        }

        private static int GetNextControllerSequence(string prefix, AppDbContext appDbContext)
        {
            try
            {
                int maxSequence = 0;
                string sqlQuery = $"SELECT * FROM CONTROLLER_MASTER WHERE CTRL_ID LIKE '{prefix}%'";

                var controllersWithPrefix = appDbContext.CONTROLLER_MASTER
                    .FromSqlRaw(sqlQuery)
                    .ToListAsync().Result;

                if (controllersWithPrefix.Any())
                {
                    maxSequence = controllersWithPrefix
                        .Select(c => int.Parse(c.CtrlId.Substring(prefix.Length)))
                        .DefaultIfEmpty(0)
                        .Max();
                }

                return maxSequence + 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<int> GetNextEndpointSequence(string prefix, AppDbContext appDbContext)
        {
            try
            {
                int maxSequence = 0;

                string query = $"SELECT MAX(END_POINTS_ID) FROM CONTROLLER_MASTER WHERE  ctrl_id ='{prefix}'";
                //var controllersWithPrefix = appDbContext.CONTROLLER_MASTER
                //    .FromSqlRaw(query)
                //    .ToList();

                string controllersWithPrefix = string.Empty;
                using (var command = appDbContext.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = query;
                    await appDbContext.Database.OpenConnectionAsync();

                    using (var reader = await command.ExecuteReaderAsync()) 
                    {
                        if (await reader.ReadAsync()) 
                        {
                            if (!reader.IsDBNull(0)) 
                            {
                                controllersWithPrefix = reader.GetString(0);
                                maxSequence = int.Parse(controllersWithPrefix.Substring(prefix.Length + 2)); 
                            }
                        }
                    }
                }

                return maxSequence + 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static int GetControllerCounter(string prefix)
        {
            return controllerCounters.ContainsKey(prefix) ? controllerCounters[prefix] : 0;
        }

        private static int GetEndpointCounter(string prefix)
        {
            return endpointCounters.ContainsKey(prefix) ? endpointCounters[prefix] : 0;
        }

        //private static string GetExistingControllerId(string name, AppDbContext appDbContext)
        //{
        //    string query = "SELECT CTRL_ID FROM CONTROLLER_MASTER = @Name";

        //    // Define parameters for the query
        //    SqlParameter nameParameter = new SqlParameter("@Name", name);

        //    // Execute raw SQL query
        //    var controllerId = appDbContext.CONTROLLER_MASTER.FromSqlRaw(query, nameParameter).FirstOrDefault();

        //    if (controllerId != null)
        //    {
        //        return controllerId.CtrlId;
        //    }
        //    else
        //    {
        //        // Handle the case where the controller does not exist in the database
        //        throw new InvalidOperationException("Controller does not exist in the database.");
        //    }
        //}
    }
}
