﻿using HRMS.ModelCL.Login;
using HRMS_Core.Data.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Comman
{
    public class ControllerMasterRepository: IControllerMasterRepository
    {
        private readonly AppDbContext _appDbContext;

        public ControllerMasterRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public async Task<(List<string> ExistingEndpoints, List<string> InsertedEndpoints)> InsertControllerMaster(List<ControllerMasterModel> records)
        {
            try
            {
                List<string> lstEndpointsExist = new List<string>();
                List<string> lstEndpointsInserted = new List<string>();
                foreach (var record in records)
                {
                    string QueryExistEndPoints = $"SELECT COUNT(*) FROM CONTROLLER_MASTER WHERE TRIM(END_POINTS)='{Convert.ToString(record.EndPoints)?.Trim()}' AND TRIM(HTTP_TYPE)='{Convert.ToString(record.HttpType)?.Trim()}'";

                    /*Start-Check if the Endpoint exist in database*/
                    using (var command = _appDbContext.Database.GetDbConnection().CreateCommand())
                    {
                        command.CommandText = QueryExistEndPoints;
                        await _appDbContext.Database.OpenConnectionAsync();
                        var count = await command.ExecuteScalarAsync();
                        if (count != null && Convert.ToInt32(count) > 0)
                        {
                            lstEndpointsExist.Add(record.EndPoints);
                            continue;
                        }
                    }
                    /*End-Check if the Endpoint exist in database*/
                    record.TranDate = DateTime.Now;
                    record.Status = (record.Status == "ACTIVE") ? "A" : "N";
                    string? trimmedCtrlName = record.CtrlName?.Trim();
                    string sqlQueryforEnd = @"SELECT CTRL_ID FROM CONTROLLER_MASTER WHERE TRIM(CTRL_NAME) = TRIM(:CtrnName)";

                    /*Start-Check if the Controller exist in database*/
                    var existingControllerId = await _appDbContext.CONTROLLER_MASTER
                        .FromSqlRaw(sqlQueryforEnd, new OracleParameter(":CtrnName", trimmedCtrlName))
                        .Select(cr => cr.CtrlId)
                        .ToListAsync();

                    string? CtrlId = existingControllerId.Count > 0 ? existingControllerId[0] : null;
                    /*End-Check if the Controller exist in database*/

                    if (!string.IsNullOrEmpty(CtrlId))
                    {
                        record.CtrlId = CtrlId;
                    }
                    else
                    {
                        record.CtrlId = ControllerIDGenerator.GenerateControllerId(trimmedCtrlName, _appDbContext);
                    }
                    record.EndPointsId = ControllerIDGenerator.GenerateEndpointId(record.CtrlId, _appDbContext);
                    await _appDbContext.CONTROLLER_MASTER.AddRangeAsync(record);
                    await _appDbContext.SaveChangesAsync();
                    lstEndpointsInserted.Add(Convert.ToString(record.EndPoints));
                }
                return (lstEndpointsExist, lstEndpointsInserted);
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error getting erro while check user rights: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

    }
}
