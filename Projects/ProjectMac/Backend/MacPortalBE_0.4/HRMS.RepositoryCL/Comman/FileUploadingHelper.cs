﻿using HRMS_Core.Data.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Comman
{
    public class FileUploadingHelper : IFileUploadingHelper
    {
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        private readonly IAutoGenerateCodeRepository _autoGenerateCodeRepository;
        private readonly ImageFetchingHelper _imageFetchingHelper;
        private readonly IDbConnection _dbConnection;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public FileUploadingHelper(AppDbContext appContext, IConfiguration configuration, IAutoGenerateCodeRepository autoGenerateCodeRepository, ImageFetchingHelper imageFetchingHelper, IDbConnection dbConnection, IHttpContextAccessor httpContextAccessor)
        {
            _appDbContext = appContext;
            _configuration = configuration;
            _autoGenerateCodeRepository = autoGenerateCodeRepository;
            _imageFetchingHelper = imageFetchingHelper;
            _dbConnection = dbConnection;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<string> SaveDataAndFileUpload(List<string> lstData, List<IFormFile> arrFile, string strFilePath, List<string> strFileName = null, Dictionary<string, Image> dictImages = null)
        {
            if (lstData == null || lstData.Count == 0)
            {
                throw new ArgumentException("lstData is null or empty.");
            }

            List<string> strTranID = new List<string>();
            try
            {
                int iRec = 0, iRecTranID = 0, iQryExecCnt = 0;
                string strErrMsg = "";

                if (lstData.Count > 0)
                {
                    using (var conn = _appDbContext.GetConn())
                    {
                        await conn.OpenAsync();
                        using (var transaction = conn.BeginTransaction())
                        {
                            try
                            {
                                foreach (var item in lstData)
                                {
                                    string strQry = "";
                                    string qry = item;

                                    if (qry.Contains("fnGenerateTranID") || qry.Contains("fnUpdateTranId"))
                                    {
                                        string strMethodWithParam = qry.Replace("'", "");
                                        string strMethod = strMethodWithParam.Split(':')[0];
                                        string strParams = strMethodWithParam.Split(':')[1];
                                        string[] arrParams = strParams.Split(',');

                                        if (qry.Contains("fnGenerateTranID"))
                                        {
                                            strTranID.Add(fnGenerateTranID(arrParams[0], arrParams[1], arrParams[2], arrParams[3], Convert.ToInt32(arrParams[4])));
                                            iRec = iRec + 1;
                                            iRecTranID++;
                                        }
                                        else if (qry.Contains("fnUpdateTranId"))
                                        {
                                            if (iRecTranID > 0 && fnUpdateTranId(arrParams[0], arrParams[1], arrParams[2], arrParams[3], arrParams[4], Convert.ToInt32(arrParams[5]), strTranID[iRecTranID - 1]))
                                            {
                                                iRec = iRec + 1;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (qry.Contains("<<TRAN_ID>>") && iRecTranID > 0 && !string.IsNullOrEmpty(strTranID[iRecTranID - 1]))
                                        {
                                            strQry = qry.Replace("<<TRAN_ID>>", strTranID[iRecTranID - 1]);
                                            if (qry.Contains("<<TRAN_ID_REF_"))
                                            {
                                                Int16 iTranSeq = Convert.ToInt16(qry.Substring(qry.IndexOf("<<TRAN_ID_REF_", 0) + 14, (qry.IndexOf("_>>", 0) - (qry.IndexOf("<<TRAN_ID_REF_", 0) + 14))));
                                                if (iTranSeq < strTranID.Count)
                                                {
                                                    strQry = strQry.Replace("<<TRAN_ID_REF_" + iTranSeq + "_>>", strTranID[iTranSeq]);
                                                }
                                            }
                                        }
                                        else if (qry.Contains("<<TRAN_ID_REF_"))
                                        {
                                            strQry = qry;
                                            for (int i = 0; i < iRecTranID; i++)
                                            {
                                                if (strQry.Contains("<<TRAN_ID_REF_" + i + "_>>"))
                                                {
                                                    strQry = strQry.Replace("<<TRAN_ID_REF_" + i + "_>>", strTranID[i]);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            strQry = qry;
                                        }

                                        if (strQry.Contains("<<XMLFilePath>>"))
                                        {
                                            strQry = strQry.Replace("<<XMLFilePath>>", _configuration["XmlFilePath"].Remove(0, 3));
                                        }

                                        using (var cmd = new OracleCommand(strQry, conn))
                                        {
                                            cmd.Transaction = transaction;

                                            int iRowExists = 0;
                                            if (qry.StartsWith("DELETE"))
                                            {
                                                cmd.CommandText = strQry.Replace("DELETE FROM", "SELECT COUNT(1) FROM");
                                                iRowExists = Convert.ToInt16(await cmd.ExecuteScalarAsync());
                                                if (iRowExists > 0)
                                                {
                                                    iRowExists = 0;
                                                }
                                                else
                                                {
                                                    iRowExists = 1;
                                                }
                                            }
                                            else if (qry.StartsWith("UPDATE"))
                                            {
                                                string s_WhereCond = strQry.Substring(strQry.IndexOf("WHERE"));
                                                cmd.CommandText = strQry.Replace("UPDATE", "SELECT COUNT(1) FROM ").Remove(strQry.Replace("UPDATE", "SELECT COUNT(1) FROM ").IndexOf("SET")) + s_WhereCond;
                                                iRowExists = Convert.ToInt16(await cmd.ExecuteScalarAsync());
                                                if (iRowExists > 0)
                                                {
                                                    iRowExists = 0;
                                                }
                                                else
                                                {
                                                    iRowExists = 1;
                                                }
                                            }

                                            cmd.CommandText = strQry;
                                            AddImageParameters(cmd, strQry, dictImages);

                                            int iRowsAffected = await cmd.ExecuteNonQueryAsync();
                                            cmd.Parameters.Clear();
                                            if (iRowsAffected > 0)
                                            {
                                                iRec = iRec + 1;
                                            }
                                            else
                                            {
                                                strErrMsg += " Index: " + iQryExecCnt;
                                                iRec = iRec + iRowExists;
                                                LogFile("SAVE (fnSaveDataAndFileUpload)", "Rows not affected", " Index: " + iQryExecCnt + Environment.NewLine + strQry, _configuration["LogFilePath"]);
                                            }
                                            iQryExecCnt = iQryExecCnt + 1;
                                        }
                                    }
                                }
                                if (iRec == lstData.Count)
                                {
                                    if (arrFile != null)
                                    {
                                        if (string.IsNullOrEmpty(strFilePath))
                                        {
                                            strFilePath = _configuration["XmlFilePath"];
                                        }

                                        for (int k = 0; k < arrFile.Count; k++)
                                        {
                                            if (arrFile[k] != null && arrFile[k].Length > 0)
                                            {
                                                if (strFileName != null && k < strFileName.Count)
                                                {
                                                    if (strFileName[k].Contains("AUTO_FILE_NAME"))
                                                    {
                                                        if (!string.IsNullOrEmpty(strTranID[0]))
                                                        {
                                                            strFileName[k] = strFileName[k].Replace("AUTO_FILE_NAME", strTranID[0]);
                                                        }
                                                        else
                                                        {
                                                            transaction.Rollback();
                                                            return "Error: AUTO_FILE_NAME replacement failed due to missing TRAN_ID.";
                                                        }
                                                    }
                                                    if (strFileName[k].Contains("<<TRAN_ID_REF_0_>>"))
                                                    {
                                                        strFileName[k] = strFileName[k].Replace("<<TRAN_ID_REF_0_>>", strTranID[0]);
                                                    }
                                                    else if (strFileName[k].Contains("<<TRAN_ID>>"))
                                                    {
                                                        strFileName[k] = strFileName[k].Replace("<<TRAN_ID>>", strTranID[0]);
                                                    }
                                                    if (string.IsNullOrEmpty(strFilePath))
                                                    {
                                                        return "Error: File path is null or empty.";
                                                    }
                                                    var path = Path.Combine(strFilePath, strFileName[k]);
                                                    using (var stream = new FileStream(path, FileMode.Create))
                                                    {
                                                        await arrFile[k].CopyToAsync(stream);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (strTranID.Count == 0)
                                    {
                                        strTranID.Add(iRec.ToString());
                                    }
                                    transaction.Commit();
                                    return strTranID[0];
                                }
                                else
                                {
                                    transaction.Rollback();
                                    LogFile("SAVE (fnSaveDataAndFileUpload)", "Error", "Transaction Failure at " + strErrMsg, _configuration["LogFilePath"]);
                                    return "Error: Transaction Failure at " + strErrMsg;
                                }
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                LogFile("SAVE (fnSaveDataAndFileUpload)", "Error", ex.Message, _configuration["LogFilePath"]);
                                return "Error: " + ex.Message;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
            return strTranID.Count > 0 ? strTranID[0] : "Error";
        }

        // working
        public string fnGenerateTranID(string strSiteCode, string strDate, string strRefSer, string strRefPrefix, int strSeqLength = 5)
        {
            try
            {
                int strSeqLen = Convert.ToInt32(strSeqLength);
                string strTranId = "";
                string Ls_Site_Doc_Prefix = "";
                string Ls_Prd_Doc_Prefix = "";
                string Ls_Ref_Prefix = "";
                string Ls_New_Seq_No = "";
                string Qry = "";

                using (var conn = _appDbContext.GetConn())
                {
                    conn.Open();

                    if (!string.IsNullOrEmpty(strSiteCode))
                    {
                        Qry = "SELECT Doc_prefix FROM Site WHERE site_code = :siteCode";
                        using (var cmd = new OracleCommand(Qry, conn))
                        {
                            cmd.Parameters.Add("siteCode", OracleDbType.Varchar2).Value = strSiteCode;
                            Ls_Site_Doc_Prefix = Convert.ToString(cmd.ExecuteScalar());
                        }
                    }

                    if (!string.IsNullOrEmpty(strDate))
                    {
                        Qry = "SELECT Trim(Doc_prefix) FROM Period WHERE fr_date <= SYSDATE AND to_date >= SYSDATE";
                        using (var cmd = new OracleCommand(Qry, conn))
                        {
                            Ls_Prd_Doc_Prefix = Convert.ToString(cmd.ExecuteScalar());
                        }
                    }

                    Ls_Ref_Prefix = Ls_Site_Doc_Prefix + strRefPrefix + Ls_Prd_Doc_Prefix;

                    if (!string.IsNullOrEmpty(strSiteCode) && !string.IsNullOrEmpty(strDate) && (Ls_Ref_Prefix.Length + strSeqLen > 10))
                    {
                        return "Invalid Site Period And Sequence Combination!!!";
                    }

                    Qry = "SELECT Count(1) FROM RefSeq WHERE Ref_Ser = :refSer";
                    if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                    {
                        Qry += " AND Trim(Ref_Prefix) = :refPrefix";
                    }
                    using (var cmd = new OracleCommand(Qry, conn))
                    {
                        cmd.Parameters.Add("refSer", OracleDbType.Varchar2).Value = strRefSer;
                        if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                        {
                            cmd.Parameters.Add("refPrefix", OracleDbType.Varchar2).Value = Ls_Ref_Prefix;
                        }

                        int iRowsAffected = Convert.ToInt32(cmd.ExecuteScalar() ?? 0);
                        if (iRowsAffected == 0)
                        {
                            Qry = "INSERT INTO Refseq VALUES (:refSer, :refPrefix, LPAD('0', :seqLen, '0'), SYSDATE, 'MAC', :remoteAddr)";
                            using (var cmdInsert = new OracleCommand(Qry, conn))
                            {
                                cmdInsert.Parameters.Add("refSer", OracleDbType.Varchar2).Value = strRefSer;
                                cmdInsert.Parameters.Add("refPrefix", OracleDbType.Varchar2).Value = Ls_Ref_Prefix;
                                cmdInsert.Parameters.Add("seqLen", OracleDbType.Int32).Value = strSeqLen;
                                cmdInsert.Parameters.Add("remoteAddr", OracleDbType.Varchar2).Value = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

                                iRowsAffected = cmdInsert.ExecuteNonQuery();
                                if (iRowsAffected == 0)
                                {
                                    return "Error Inserting TranID!!!";
                                }
                            }
                        }
                        else if (iRowsAffected > 1)
                        {
                            return "Please Mention Ref Prefix!!!";
                        }
                    }

                    // Use string interpolation to include variables directly in the query
                    Qry = $"SELECT Length(Trim(Seq_No)) AS LEN, LPad(Seq_No + 1, {strSeqLen}, '0') AS SEQ FROM RefSeq WHERE Trim(Ref_Ser) = '{strRefSer}'";
                    if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                    {
                        Qry += $" AND Trim(Ref_Prefix) = '{Ls_Ref_Prefix}'";
                    }

                    using (var cmd = new OracleCommand(Qry, conn))
                    {
                        // Log the query and parameters
                        Console.WriteLine("Executing query: " + Qry);

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                Console.WriteLine("No rows returned by the query.");
                            }

                            while (reader.Read())
                            {
                                int length = reader["LEN"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LEN"]);
                                string seq = reader["SEQ"] == DBNull.Value ? string.Empty : Convert.ToString(reader["SEQ"]);

                                // Log LEN and SEQ values
                                Console.WriteLine($"LEN: {length}, SEQ: {seq}");

                                if (length != strSeqLength)
                                {
                                    return "Existing Sequence Length Is Not Equal To Provided Length!!!";
                                }
                                else
                                {
                                    Ls_New_Seq_No = seq.Trim();
                                }
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(Ls_New_Seq_No))
                    {
                        return "No Next Sequence Found!!!";
                    }

                    strTranId = Ls_Ref_Prefix + Ls_New_Seq_No;
                    return strTranId;
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
        }

        public bool fnUpdateTranId(string strSiteCode, string strEmpCode, string strDate, string strRefSer, string strRefPrefix, int strSeqLen, string strTranID)
        {
            try
            {
                string Qry = "";
                using (var conn = _appDbContext.GetConn())
                {
                    conn.Open();
                    if (strSiteCode != "" && strDate != "" && strSeqLen == 5)
                    {
                        Qry = "UPDATE REFSEQ SET SEQ_NO = '" + strTranID.Substring(5, 5) + "', CHG_DATE = SYSDATE, CHG_USER = '" + strEmpCode + "', " +
                            "CHG_TERM='" + _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() + "' WHERE REF_SER = '" + strRefSer + "' And Trim(REF_PREFIX) = '" + strTranID.Substring(0, 5) + "' ";
                    }
                    else if (strRefPrefix == "")
                    {
                        Qry = "UPDATE REFSEQ SET SEQ_NO = '" + strTranID + "', CHG_DATE = SYSDATE, CHG_USER = '" + strEmpCode + "', " +
                            "CHG_TERM='" + _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() + "' WHERE REF_SER = '" + strRefSer + "' ";
                    }
                    else if (strRefPrefix != "")
                    {
                        Qry = "UPDATE REFSEQ SET SEQ_NO = '" + strTranID.Substring(strTranID.Length - strSeqLen) + "', CHG_DATE = SYSDATE, CHG_USER = '" + strEmpCode + "', " +
                            "CHG_TERM='" + _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() + "' WHERE REF_SER = '" + strRefSer + "' And Trim(REF_PREFIX) = '" + strTranID.Substring(0, (strTranID.Length - strSeqLen)) + "' ";
                    }

                    if (Qry != "")
                    {
                        using (var cmd = new OracleCommand(Qry, conn))
                        {
                            int iRowsAffected = cmd.ExecuteNonQuery();
                            if (iRowsAffected == 0)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        private void LogFile(string sLogTitle, string sLogType, string sLogContent, string sLogFilePath)
        {
            string sLogFileName = "Log" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
            using (StreamWriter writer = new StreamWriter(Path.Combine(sLogFilePath, sLogFileName), true))
            {
                writer.WriteLine(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + " | " + sLogType + " | " + sLogTitle + " | " + sLogContent);
            }
        }

        private void AddImageParameters(OracleCommand cmd, string query, Dictionary<string, Image> dictImages)
        {
            if (dictImages != null)
            {
                foreach (var imageKey in dictImages.Keys)
                {
                    if (query.Contains(imageKey))
                    {
                        byte[] imageBytes = ConvertImageToByteArray1(dictImages[imageKey]);
                        OracleParameter param = new OracleParameter(imageKey, OracleDbType.Blob);
                        param.Value = imageBytes;
                        cmd.Parameters.Add(param);
                    }
                }
            }
        }

        private byte[] ConvertImageToByteArray1(Image image)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Jpeg); // You can change the format based on your requirements
                return ms.ToArray();
            }
        }

        private byte[] ConvertImageToByteArray(Image image, ImageFormat jpeg)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Jpeg); // You can change the format based on your requirements
                return ms.ToArray();
            }
        }

        public async Task<bool> ExecuteNonQueryMultiple(List<string> lstQry, string s_MATCH_EXEC_CNT = "Y")
        {
            using var connection = _appDbContext.GetConn();
            await connection.OpenAsync();

            OracleTransaction transaction = null;

            try
            {
                transaction = connection.BeginTransaction();
                using var command = connection.CreateCommand();
                command.Transaction = transaction;

                string qry = "";
                int iRowsAffectedCnt = 0;

                foreach (var item in lstQry)
                {
                    qry = item.Replace(";", "");
                    int iRowExists = 0;

                    if (qry.StartsWith("DELETE"))
                    {
                        command.CommandText = qry.Replace("DELETE FROM", "SELECT COUNT(1) FROM");
                        iRowExists = Convert.ToInt32(await command.ExecuteScalarAsync());
                        if (iRowExists > 0)
                        {
                            iRowExists = 0;
                        }
                        else
                        {
                            iRowExists = 1;
                        }
                    }

                    command.CommandText = qry;
                    int iRowsAffected = await command.ExecuteNonQueryAsync();
                    if (iRowsAffected > 0)
                    {
                        iRowsAffectedCnt++;
                    }
                    else
                    {
                        LogFile("SAVE (ExecuteNonQueryMultiple)", "Error", qry, _configuration["LogFilePath"]);
                        iRowsAffectedCnt += iRowExists;
                    }
                }

                if (s_MATCH_EXEC_CNT == "Y")
                {
                    if (iRowsAffectedCnt == lstQry.Count)
                    {
                        transaction.Commit();
                        return true;
                    }
                    else
                    {
                        LogFile("SAVE (ExecuteNonQueryMultiple)", "Error", qry, _configuration["LogFilePath"]);
                        transaction.Rollback();
                        return false;
                    }
                }
                else
                {
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogFile("SAVE (ExecuteNonQueryMultiple)", "Error", "SOME ERROR OCCURRED", _configuration["LogFilePath"]);
                LogFile("SAVE (ExecuteNonQueryMultiple)", "Error", fnResetErrorMsg(ex.Message), _configuration["LogFilePath"]);
                transaction?.Rollback();
                return false;
            }
            finally
            {
                await connection.CloseAsync();
            }
        }


        public async Task<bool> ExecuteNonQueryMultipleAndFileUpload(List<string> lstQry, List<IFormFile> lstFile, string strFilePath, List<string> lstFileName = null, Dictionary<string, Image> dictImages = null)
        {
            using var connection = _appDbContext.GetConn();
            await connection.OpenAsync();

            OracleTransaction transaction = null;

            try
            {
                transaction = connection.BeginTransaction();
                using var command = connection.CreateCommand();
                command.Transaction = transaction;

                int iRowsAffectedCnt = 0;

                foreach (var item in lstQry)
                {
                    string qry = item.Replace(";", "");
                    int iRowExists = 0;

                    if (qry.StartsWith("DELETE"))
                    {
                        command.CommandText = qry.Replace("DELETE FROM", "SELECT COUNT(1) FROM");
                        iRowExists = Convert.ToInt32(await command.ExecuteScalarAsync());
                        if (iRowExists > 0)
                        {
                            iRowExists = 0;
                        }
                        else
                        {
                            iRowExists = 1;
                        }
                    }

                    command.CommandText = qry;

                    if (qry.Contains(":Picture1"))
                        command.Parameters.Add("Picture1", OracleDbType.Blob, ConvertImageToByteArray(dictImages.Values.ElementAt(0), ImageFormat.Jpeg), ParameterDirection.Input);

                    if (qry.Contains(":Picture2"))
                    {
                        if (dictImages.Count > 1)
                            command.Parameters.Add("Picture2", OracleDbType.Blob, ConvertImageToByteArray(dictImages.Values.ElementAt(1), ImageFormat.Png), ParameterDirection.Input);
                        else
                            command.Parameters.Add("Picture2", OracleDbType.Blob, ConvertImageToByteArray(dictImages.Values.ElementAt(0), ImageFormat.Png), ParameterDirection.Input);
                    }

                    if (qry.Contains(":Picture5"))
                    {
                        if (dictImages.Count > 1)
                            command.Parameters.Add("Picture5", OracleDbType.Blob, ConvertImageToByteArray(dictImages.Values.ElementAt(1), ImageFormat.Png), ParameterDirection.Input);
                        else
                            command.Parameters.Add("Picture5", OracleDbType.Blob, ConvertImageToByteArray(dictImages.Values.ElementAt(0), ImageFormat.Png), ParameterDirection.Input);
                    }

                    int iRowsAffected = await command.ExecuteNonQueryAsync();
                    command.Parameters.Clear();
                    if (iRowsAffected > 0)
                    {
                        iRowsAffectedCnt++;
                    }
                    else
                    {
                        iRowsAffectedCnt += iRowExists;
                    }
                }

                if (iRowsAffectedCnt == lstQry.Count)
                {
                    if (lstFile != null && lstFileName != null)
                    {
                        if (string.IsNullOrEmpty(strFilePath))
                        {
                            strFilePath = _configuration["XmlFilePath"];
                        }

                        for (int k = 0; k < lstFile.Count; k++)
                        {
                            if (!string.IsNullOrEmpty(lstFile[k]?.FileName))
                            {
                                var filePath = Path.Combine(strFilePath, lstFileName[k]);
                                using (var stream = new FileStream(filePath, FileMode.Create))
                                {
                                    await lstFile[k].CopyToAsync(stream);
                                }
                            }
                        }
                    }

                    transaction.Commit();
                    return true;
                }
                else
                {
                    transaction.Rollback();
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogFile("SAVE (ExecuteNonQueryMultiple)", "Error", "SOME ERROR OCCURRED", _configuration["LogFilePath"]);
                LogFile("SAVE (ExecuteNonQueryMultiple)", "Error", fnResetErrorMsg(ex.Message), _configuration["LogFilePath"]);
                transaction?.Rollback();
                return false;
            }
            finally
            {
                await connection.CloseAsync();
            }
        }

        private string fnResetErrorMsg(string errorMsg)
        {
            if (string.IsNullOrEmpty(errorMsg))
                return string.Empty;

            // You can add custom logic here to format the error message as needed.
            // For now, let's just return the original message.
            return errorMsg;
        }


        // single documents
        /* public async Task<string> UploadAttachmentAsync(IFormFile supportDoc, string fileName,string userId, string userIpAddress, string empCode, string hidDocsId )
         {
             List<string> lstQry = new List<string>();
             int iGenTranIDCnt = 0;
             List<IFormFile> SuppDoc_files = new List<IFormFile>();
             List<string> SuppDoc_files_name = new List<string>();
             string strFILE_PATH = "", strExtension = "", s_UNIQUE_ID = "", strUpdatedFileName = "";

             // Replace with actual values or remove if not needed
             string tranIdHdr = empCode; //Guid.NewGuid().ToString();  // Assuming a new GUID as a placeholder for tranIdHdr
             //string hidDocsId = string.Empty;  // Assuming an empty string for hidDocsId

             if (supportDoc != null)
             {
                 string domainName = _configuration["DomainName"]?.ToUpper();
                 if (string.IsNullOrEmpty(domainName))
                 {
                     throw new ArgumentException("Domain name is not specified in the configuration");
                 }

                 if (domainName == "MACLEODSPHARMA")
                     strFILE_PATH = "Admin\\PMS\\";
                 else if (domainName == "OXALISLABS")
                     strFILE_PATH = "Oxalis\\Admin\\PMS\\";
                 else
                     throw new ArgumentException("Invalid domain name");

                 s_UNIQUE_ID = tranIdHdr;
                 strExtension = Path.GetExtension(supportDoc.FileName);
                 SuppDoc_files.Add(supportDoc);

                 if (string.IsNullOrEmpty(hidDocsId))
                 {
                     strUpdatedFileName = $"{fileName}{s_UNIQUE_ID}{strExtension}";
                     SuppDoc_files_name.Add($"{strFILE_PATH}{strUpdatedFileName}");

                     iGenTranIDCnt += 1;
                     lstQry.Add("fnGenerateTranID:'S0001','" + DateTime.Today.ToString("dd-MMM-yyyy") + "','ATTACH','AT',5");

                     string Qry = $"INSERT INTO DOC_CONTENTS (DOC_ID, DOC_NAME, STORAGE_CODE, DOC_TYPE, KEY_WORDS, CHG_DATE, CHG_USER, CHG_TERM, " +
                                  $"ADD_DATE, ADD_USER, ADD_TERM, DOC_SECURITY, USER_ID__CHKOUT) VALUES " +
                                  $"('<<TRAN_ID>>', '{strUpdatedFileName}', 'PMSDOC2324','{strExtension.Remove(0, 1)}', '', SYSDATE, '{userId}', " +
                                  $"'{userIpAddress}', SYSDATE , '{userId}', '{userIpAddress}','N','')";
                     lstQry.Add(Qry);

                     Qry = $"INSERT INTO DOC_TRANSACTION_LINK (DOC_ID, REF_SER, REF_ID, CONFIRMED) VALUES ('<<TRAN_ID>>', 'ATTACH', '{s_UNIQUE_ID}', 'Y')";
                     lstQry.Add(Qry);

                     lstQry.Add($"fnUpdateTranId:'S0001','{userId}','{DateTime.Today.ToString("dd-MMM-yyyy")}','ATTACH','AT',5,'<<TRAN_ID>>'");
                 }
                 else
                 {
                     strUpdatedFileName = $"{fileName}{s_UNIQUE_ID.Trim()}{strExtension}";
                     SuppDoc_files_name.Add($"{strFILE_PATH}{strUpdatedFileName}");

                     string Qry = $"UPDATE DOC_TRANSACTION_LINK SET CONFIRMED='Y' WHERE DOC_ID='{hidDocsId.Trim()}'";
                     lstQry.Add(Qry);

                     Qry = $"UPDATE DOC_CONTENTS SET DOC_NAME = '{strUpdatedFileName}', DOC_TYPE = '{strExtension.Remove(0, 1)}', " +
                           $"CHG_DATE = SYSDATE, CHG_USER = '{userId}', CHG_TERM = '{userIpAddress}' " +
                           $"WHERE DOC_ID = '{hidDocsId.Trim()}' ";
                     lstQry.Add(Qry);

                     Qry = $"UPDATE DOC_TRANSACTION_LINK SET CONFIRMED='Y' WHERE DOC_ID='{hidDocsId.Trim()}'";
                     lstQry.Add(Qry);
                 }
             }

             if (iGenTranIDCnt > 0)
             {
                 string strResult = await SaveDataAndFileUpload(lstQry, SuppDoc_files, _configuration["XmlFilePath"], SuppDoc_files_name);
                 if (!string.IsNullOrEmpty(strResult))
                 {
                     if (strResult.Contains("Error"))
                     {
                         return "Error: " + strResult;
                     }
                     else
                     {
                         return strResult;
                     }
                 }
             }
             else
             {
                 bool bResult = false;
                 if (SuppDoc_files.Count > 0)
                     bResult = await ExecuteNonQueryMultipleAndFileUpload(lstQry, SuppDoc_files, _configuration["XmlFilePath"], SuppDoc_files_name);
                 else
                     bResult = await ExecuteNonQueryMultiple(lstQry);
                 if (!bResult)
                 {
                     return "Error in uploading supporting documents.";
                 }
             }

             return "Success";
         }*/

        // multiple documents
        /*
        public async Task<string> UploadAttachmentsAsync(List<IFormFile> supportDocs, List<string> fileNames, string userId, string userIpAddress, string empCode, string hidDocsId)
        {
            List<string> lstQry = new List<string>();
            int iGenTranIDCnt = 0;
            List<IFormFile> SuppDoc_files = new List<IFormFile>();
            List<string> SuppDoc_files_name = new List<string>();
            string strFILE_PATH = "", s_UNIQUE_ID = "", strExtension = "";

            // Replace with actual values or remove if not needed
            string tranIdHdr = empCode; // Assuming a new GUID as a placeholder for tranIdHdr

            if (supportDocs != null && supportDocs.Count > 0)
            {
                string domainName = _configuration["DomainName"]?.ToUpper();
                if (string.IsNullOrEmpty(domainName))
                {
                    throw new ArgumentException("Domain name is not specified in the configuration");
                }

                if (domainName == "MACLEODSPHARMA")
                    strFILE_PATH = "Admin\\PMS\\";
                else if (domainName == "OXALISLABS")
                    strFILE_PATH = "Oxalis\\Admin\\PMS\\";
                else
                    throw new ArgumentException("Invalid domain name");

                for (int i = 0; i < supportDocs.Count; i++)
                {
                    var supportDoc = supportDocs[i];
                    var fileName = fileNames[i];
                    s_UNIQUE_ID = tranIdHdr;
                    strExtension = Path.GetExtension(supportDoc.FileName);
                    SuppDoc_files.Add(supportDoc);

                    if (string.IsNullOrEmpty(hidDocsId))
                    {
                        string strUpdatedFileName = $"{fileName}{s_UNIQUE_ID}{strExtension}";
                        SuppDoc_files_name.Add($"{strFILE_PATH}{strUpdatedFileName}");

                        iGenTranIDCnt += 1;
                        lstQry.Add("fnGenerateTranID:'S0001','" + DateTime.Today.ToString("dd-MMM-yyyy") + "','ATTACH','AT',5");

                        string Qry = $"INSERT INTO DOC_CONTENTS (DOC_ID, DOC_NAME, STORAGE_CODE, DOC_TYPE, KEY_WORDS, CHG_DATE, CHG_USER, CHG_TERM, " +
                                     $"ADD_DATE, ADD_USER, ADD_TERM, DOC_SECURITY, USER_ID__CHKOUT) VALUES " +
                                     $"('<<TRAN_ID>>', '{strUpdatedFileName}', 'PMSDOC2324','{strExtension.Remove(0, 1)}', '', SYSDATE, '{userId}', " +
                                     $"'{userIpAddress}', SYSDATE , '{userId}', '{userIpAddress}','N','')";
                        lstQry.Add(Qry);

                        Qry = $"INSERT INTO DOC_TRANSACTION_LINK (DOC_ID, REF_SER, REF_ID, CONFIRMED) VALUES ('<<TRAN_ID>>', 'ATTACH', '{s_UNIQUE_ID}', 'Y')";
                        lstQry.Add(Qry);

                        lstQry.Add($"fnUpdateTranId:'S0001','{userId}','{DateTime.Today.ToString("dd-MMM-yyyy")}','ATTACH','AT',5,'<<TRAN_ID>>'");
                    }
                    else
                    {
                        string strUpdatedFileName = $"{fileName}{s_UNIQUE_ID.Trim()}{strExtension}";
                        SuppDoc_files_name.Add($"{strFILE_PATH}{strUpdatedFileName}");

                        string Qry = $"UPDATE DOC_TRANSACTION_LINK SET CONFIRMED='Y' WHERE DOC_ID='{hidDocsId.Trim()}'";
                        lstQry.Add(Qry);

                        Qry = $"UPDATE DOC_CONTENTS SET DOC_NAME = '{strUpdatedFileName}', DOC_TYPE = '{strExtension.Remove(0, 1)}', " +
                              $"CHG_DATE = SYSDATE, CHG_USER = '{userId}', CHG_TERM = '{userIpAddress}' " +
                              $"WHERE DOC_ID = '{hidDocsId.Trim()}' ";
                        lstQry.Add(Qry);

                        Qry = $"UPDATE DOC_TRANSACTION_LINK SET CONFIRMED='Y' WHERE DOC_ID='{hidDocsId.Trim()}'";
                        lstQry.Add(Qry);
                    }
                }
            }

            if (iGenTranIDCnt > 0)
            {
                string strResult = await SaveDataAndFileUpload(lstQry, SuppDoc_files, _configuration["XmlFilePath"], SuppDoc_files_name);
                if (!string.IsNullOrEmpty(strResult))
                {
                    if (strResult.Contains("Error"))
                    {
                        return "Error: " + strResult;
                    }
                    else
                    {
                        return strResult;
                    }
                }
            }
            else
            {
                bool bResult = false;
                if (SuppDoc_files.Count > 0)
                    bResult = await ExecuteNonQueryMultipleAndFileUpload(lstQry, SuppDoc_files, _configuration["XmlFilePath"], SuppDoc_files_name);
                else
                    bResult = await ExecuteNonQueryMultiple(lstQry);
                if (!bResult)
                {
                    return "Error in uploading supporting documents.";
                }
            }

            return "Success";
        }
        */

        public async Task<string> UploadAttachmentsAsync(List<IFormFile> supportDocs, List<string?> fileNames, string userId, string userIpAddress, string empCode, string hidDocsId, string storageCode_Par)
        {
            List<string> lstQry = new List<string>();
            int iGenTranIDCnt = 0;
            List<IFormFile> SuppDoc_files = new List<IFormFile>();
            List<string> SuppDoc_files_name = new List<string>();
            string strFILE_PATH = "", s_UNIQUE_ID = "", strExtension = "";

            string tranIdHdr = empCode; // Assuming empCode as a placeholder for tranIdHdr

            if (supportDocs != null && supportDocs.Count > 0)
            {
                string domainName = _configuration["DomainName"]?.ToUpper();
                if (string.IsNullOrEmpty(domainName))
                {
                    throw new ArgumentException("Domain name is not specified in the configuration");
                }

                if (domainName == "MACLEODSPHARMA")
                    strFILE_PATH = "Admin\\PMS\\";
                else if (domainName == "OXALISLABS")
                    strFILE_PATH = "Oxalis\\Admin\\PMS\\";
                else
                    throw new ArgumentException("Invalid domain name");

                for (int i = 0; i < supportDocs.Count; i++)
                {
                    var supportDoc = supportDocs[i];
                    var fileName = i < fileNames.Count ? fileNames[i] : null;
                    s_UNIQUE_ID = tranIdHdr;
                    strExtension = Path.GetExtension(supportDoc.FileName);
                    SuppDoc_files.Add(supportDoc);

                    string strUpdatedFileName;
                    if (string.IsNullOrEmpty(fileName))
                    {
                        strUpdatedFileName = $"{Path.GetFileNameWithoutExtension(supportDoc.FileName)}_{s_UNIQUE_ID}{strExtension}";
                    }
                    else
                    {
                        strUpdatedFileName = $"{fileName}_{s_UNIQUE_ID}{strExtension}";
                    }

                    SuppDoc_files_name.Add($"{strFILE_PATH}{strUpdatedFileName}");

                    if (string.IsNullOrEmpty(hidDocsId))
                    {
                        iGenTranIDCnt += 1;
                        lstQry.Add("fnGenerateTranID:'S0001','" + DateTime.Today.ToString("dd-MMM-yyyy") + "','ATTACH','AT',5");

                        string Qry = $"INSERT INTO DOC_CONTENTS (DOC_ID, DOC_NAME, STORAGE_CODE, DOC_TYPE, KEY_WORDS, CHG_DATE, CHG_USER, CHG_TERM, " +
                                      $"ADD_DATE, ADD_USER, ADD_TERM, DOC_SECURITY, USER_ID__CHKOUT) VALUES " +
                                      $"('<<TRAN_ID>>', '{strUpdatedFileName}', '{storageCode_Par}','{strExtension.Remove(0, 1)}', '', SYSDATE, '{userId}', " +
                                      $"'{userIpAddress}', SYSDATE , '{userId}', '{userIpAddress}','N','')";
                        lstQry.Add(Qry);

                        Qry = $"INSERT INTO DOC_TRANSACTION_LINK (DOC_ID, REF_SER, REF_ID, CONFIRMED) VALUES ('<<TRAN_ID>>', 'ATTACH', '{s_UNIQUE_ID}', 'Y')";
                        lstQry.Add(Qry);

                        lstQry.Add($"fnUpdateTranId:'S0001','{userId}','{DateTime.Today.ToString("dd-MMM-yyyy")}','ATTACH','AT',5,'<<TRAN_ID>>'");
                    }
                    else
                    {
                        string Qry = $"UPDATE DOC_TRANSACTION_LINK SET CONFIRMED='Y' WHERE DOC_ID='{hidDocsId.Trim()}'";
                        lstQry.Add(Qry);

                        Qry = $"UPDATE DOC_CONTENTS SET DOC_NAME = '{strUpdatedFileName}', DOC_TYPE = '{strExtension.Remove(0, 1)}', " +
                               $"CHG_DATE = SYSDATE, CHG_USER = '{userId}', CHG_TERM = '{userIpAddress}' " +
                               $"WHERE DOC_ID = '{hidDocsId.Trim()}' ";
                        lstQry.Add(Qry);

                        Qry = $"UPDATE DOC_TRANSACTION_LINK SET CONFIRMED='Y' WHERE DOC_ID='{hidDocsId.Trim()}'";
                        lstQry.Add(Qry);
                    }
                }
            }

            if (iGenTranIDCnt > 0)
            {
                string strResult = await SaveDataAndFileUpload(lstQry, SuppDoc_files, _configuration["XmlFilePath"], SuppDoc_files_name);
                if (!string.IsNullOrEmpty(strResult))
                {
                    if (strResult.Contains("Error"))
                    {
                        return "Error: " + strResult;
                    }
                    else
                    {
                        return strResult;
                    }
                }
            }
            else
            {
                bool bResult = false;
                if (SuppDoc_files.Count > 0)
                    bResult = await ExecuteNonQueryMultipleAndFileUpload(lstQry, SuppDoc_files, _configuration["XmlFilePath"], SuppDoc_files_name);
                else
                    bResult = await ExecuteNonQueryMultiple(lstQry);
                if (!bResult)
                {
                    return "Error in uploading supporting documents.";
                }
            }

            return "Success";
        }
    }
}
