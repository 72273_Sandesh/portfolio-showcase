﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Comman
{
    public interface IAutoGenerateCodeRepository
    {
        public string GenerateCode(string tableName, string tranWindowCol);
    }
}
