﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Comman
{
    public interface IFileUploadingHelper
    {
        
        Task<string> SaveDataAndFileUpload(List<string> lstData, List<IFormFile> arrFile, string strFilePath, List<string> strFileName = null, Dictionary<string, Image> dictImages = null);      
        Task<string> UploadAttachmentsAsync(List<IFormFile> supportDocs, List<string?> fileNames, string userId, string userIpAddress, string empCode, string hidDocsId, string storageCode_Par);


    }
}
