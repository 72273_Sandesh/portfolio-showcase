﻿using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Transaction.Affecting.employeeTransaction;
using HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HRMS.RepositoryCL.Comman.TransactionMasterRepository;

namespace HRMS.RepositoryCL.Comman
{
    public interface ITransactionMasterRepository
    {
        Task<int> InsertOrUpdateMasterTransactionAsync(string strTableName, string strKeyVar, int iWFLevel, char strConf, string strSiteCode, string strEmp, string strFilePath, IFormFile file, string strFolderPath, string strTranId, string remarks, string ipAddress, List<FieldUpdateRequest> fieldUpdates);
        Task<int> InsertOrUpdateEmployeeTransactionAsync(dynamic payload, string formName);
        Task<Dictionary<string, string>> GetExistingValuesAsync(string empCode);
        Task RejectTransaction([FromBody] RejectTransactionRequest request);
        Task ApproveTransaction(string tranId, string ipAddress, string empCode, string SessionempCode, string tableName, int wfProcLevel, string moduleType, string remarks);
        Task<string> fnGetGenMstAsync(string s_MOD_NAME, [FromBody] dynamic fields, string s_TRAN_MODE, string s_KEYFIELDVAL);

        //Task<int> InsertOrUpdateMasterTransactionAsync(MasterTransactionRequest request);
    }
}
