﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Comman
{
    public class RepositoryException: Exception
    {
        public string ErrorMessage { get; }

        public RepositoryException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public RepositoryException(string message, string errorMessage, Exception innerException)
            : base(message, innerException)
        {
            ErrorMessage = errorMessage;
        }

        public RepositoryException(string message, string innerException)
        {
        }

        public RepositoryException(string message):base(message) 
        {
            ErrorMessage = message;
        }
    }
}
