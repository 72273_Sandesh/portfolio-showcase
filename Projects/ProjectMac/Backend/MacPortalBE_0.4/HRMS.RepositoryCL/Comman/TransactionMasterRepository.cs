﻿using Dapper;
using HRMS.ModelCL.ADMINISTRATION.Masters.Cadre;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Transaction;
using HRMS.ModelCL.Payloads.Transaction.Affecting.employeeTransaction;
using HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction;
using HRMS.RepositoryCL.Interface.Transaction;
using HRMS_Core.Data.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json.Nodes;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace HRMS.RepositoryCL.Comman
{

    public class TransactionMasterRepository : ITransactionMasterRepository
    {

        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IFileUploadingHelper _fileUploadingHelper;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IAutoGenerateCodeRepository _autoGenerateCodeRepository;
        public TransactionMasterRepository(AppDbContext appContext, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IFileUploadingHelper fileUploadingRepository, ITransactionRepository affecting, IAutoGenerateCodeRepository autoGenerateCodeRepository)
        {
            _appDbContext = appContext;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _fileUploadingHelper = fileUploadingRepository;
            _transactionRepository = affecting;
            _autoGenerateCodeRepository = autoGenerateCodeRepository;
        }



        public async Task<string> QuryListExecuteTransaction(List<string> lstData)
        {
            if (lstData == null || lstData.Count == 0)
            {
                throw new ArgumentException("lstData is null or empty.");
            }

            List<string> strTranID = new List<string>();
            try
            {
                int iRec = 0, iRecTranID = 0, iQryExecCnt = 0;
                string strErrMsg = "";

                using (var conn = _appDbContext.GetConn())
                {
                    await conn.OpenAsync();
                    using (var transaction = await conn.BeginTransactionAsync())
                    {
                        try
                        {
                            foreach (var item in lstData)
                            {
                                string qry = item.Trim();
                                Console.WriteLine("Processing query: " + qry);

                                if (qry.Contains("fnGenerateTranID") || qry.Contains("fnUpdateTranId"))
                                {
                                    string strMethodWithParam = qry.Replace("'", "");
                                    string strMethod = strMethodWithParam.Split(':')[0];
                                    string strParams = strMethodWithParam.Split(':')[1];
                                    string[] arrParams = strParams.Split(',');

                                    if (strMethod == "fnGenerateTranID")
                                    {
                                        string tranId = await fnGenerateTranID(
                                            arrParams[0].Trim(),
                                            arrParams[1].Trim(),
                                            arrParams[2].Trim(),
                                            arrParams[3].Trim(),
                                            Convert.ToInt32(arrParams[4].Trim())
                                        );
                                        strTranID.Add(tranId);
                                        Console.WriteLine("Generated TRAN_ID: " + tranId);
                                        iRec++;
                                        iRecTranID++;
                                    }
                                    else if (strMethod == "fnUpdateTranId")
                                    {
                                        if (iRecTranID > 0)
                                        {
                                            bool updateSuccess = await fnUpdateTranIdAsync(
                                                arrParams[0].Trim(),
                                                arrParams[1].Trim(),
                                                arrParams[2].Trim(),
                                                arrParams[3].Trim(),
                                                arrParams[4].Trim(),
                                                Convert.ToInt32(arrParams[5].Trim()),
                                                strTranID[iRecTranID - 1]
                                            );
                                            if (updateSuccess)
                                            {
                                                iRec++;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    string strQry = qry;

                                    if (strQry.Contains("<<TRAN_ID>>") && iRecTranID > 0 && !string.IsNullOrEmpty(strTranID[iRecTranID - 1]))
                                    {
                                        strQry = strQry.Replace("<<TRAN_ID>>", strTranID[iRecTranID - 1]);

                                        foreach (var idRef in strTranID.Select((value, index) => new { value, index }))
                                        {
                                            strQry = strQry.Replace($"<<TRAN_ID_REF_{idRef.index}>>", idRef.value);
                                        }
                                    }

                                    if (strQry.Contains("<<XMLFilePath>>"))
                                    {
                                        strQry = strQry.Replace("<<XMLFilePath>>", _configuration["XmlFilePath"].TrimStart('/'));
                                    }

                                    Console.WriteLine("Final query: " + strQry);

                                    using (var cmd = new OracleCommand(strQry, conn))
                                    {
                                        cmd.Transaction = (OracleTransaction)transaction;

                                        if (strQry.StartsWith("DELETE"))
                                        {
                                            cmd.CommandText = strQry.Replace("DELETE FROM", "SELECT COUNT(1) FROM");
                                            int iRowExists = Convert.ToInt16(await cmd.ExecuteScalarAsync());
                                            if (iRowExists > 0)
                                            {
                                                await cmd.ExecuteNonQueryAsync();
                                                iRec++;
                                            }
                                        }
                                        else if (strQry.StartsWith("UPDATE"))
                                        {
                                            //string s_WhereCond = strQry.Substring(strQry.IndexOf("WHERE"));
                                            //cmd.CommandText = strQry.Replace("UPDATE", "SELECT COUNT(1) FROM ").Remove(strQry.IndexOf("SET") + 3) + s_WhereCond;
                                            //int iRowExists = Convert.ToInt16(await cmd.ExecuteScalarAsync());
                                            int iRowExists = await cmd.ExecuteNonQueryAsync();
                                            if (iRowExists > 0)
                                            {
                                                await cmd.ExecuteNonQueryAsync();
                                                iRec++;
                                            }
                                            iQryExecCnt++;
                                        }
                                        else
                                        {
                                            int iRowsAffected = await cmd.ExecuteNonQueryAsync();
                                            if (iRowsAffected > 0)
                                            {
                                                iRec++;
                                            }
                                            else
                                            {
                                                strErrMsg += " Index: " + iQryExecCnt;
                                            }
                                            iQryExecCnt++;
                                        }
                                    }
                                }
                            }

                            if (iRec == lstData.Count)
                            {
                                await transaction.CommitAsync();
                                return strTranID.Count > 0 ? strTranID[0] : "Success";
                            }
                            else
                            {
                                await transaction.RollbackAsync();
                                return "Error: Transaction Failure at " + strErrMsg;
                            }
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            return "Error: " + ex.Message;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
        }

        // working correctly for all masters
        public async Task<int> InsertOrUpdateMasterTransactionAsync(
                    string strTableName,
                    string strKeyVar,
                    int iWFLevel,
                    char strConf,
                    string strSiteCode,
                    string strEmp,
                    string strFilePath,
                    IFormFile file,
                    string strFolderPath,
                    string strTranId,
                    string remarks,
                    string chg_Term,
                    List<FieldUpdateRequest> fieldUpdates)
        {
            int iRowsAffected = 0;
            try
            {
                int strSeqLength = 5; // Assuming default sequence length of 5
                List<string> lstQry = new List<string>();
                string strResult = "";
                using (var conn = _appDbContext.GetConn())
                {
                    await conn.OpenAsync();
                    using (var transaction = await conn.BeginTransactionAsync())
                    {
                        try
                        {
                            if (strTranId != null)
                            {
                                // while updating master_unconf_det primary key has same value so skip it 
                                var fieldsToSkip = new List<string> { "CADRE_CODE", "GRADE_CODE", "DEPT_CODE", "DESIGN_CODE", "QLF_CODE", "EMP_CODE" };
                                var fieldsToRemove = new List<FieldUpdateRequest>();

                                foreach (var update in fieldUpdates)
                                {
                                    // Check if the FIELD_NAME matches any of the fields in the list
                                    if (fieldsToSkip.Contains(update.FIELD_NAME.Trim().ToUpper()))
                                    {
                                        continue;
                                    }

                                    if (update.OLD_KEY?.Trim().ToUpper() == update.NEW_KEY?.Trim().ToUpper())
                                    {
                                        string deleteQuery = $@"
                                            UPDATE masters_unconf_det SET VISIBILITY = 'N'
                                            WHERE TRAN_ID = '{strTranId}' AND FIELD_NAME = '{update.FIELD_NAME}'";
                                        lstQry.Add(deleteQuery);

                                        fieldsToRemove.Add(update);
                                    }
                                }

                                // Remove fields from fieldUpdates where visibility was set to 'N'
                                fieldUpdates = fieldUpdates.Except(fieldsToRemove).ToList();
                            }

                            var descrUpdate = fieldUpdates.FirstOrDefault(f => f.FIELD_NAME.Trim().ToUpper() == "DESCR" || f.FIELD_NAME.Trim().ToUpper() == "DESIGNATION");

                            if (descrUpdate != null)
                            {
                                // Use raw SQL to check if the DESCR value already exists in the specified table
                                string query = $"SELECT COUNT(1) FROM {strTableName} WHERE UPPER(TRIM({descrUpdate.FIELD_NAME})) = UPPER(TRIM(:value))";

                                //string query = $"SELECT COUNT(1) FROM {strTableName} WHERE UPPER(TRIM(DESCR)) = UPPER(TRIM(:descr))";
                                var parameters = new { value = descrUpdate.NEW_VALUE };
                                int count = await conn.QuerySingleAsync<int>(query, parameters, transaction);

                                if (count > 0)
                                {
                                    // Compare old and new DESCR values
                                    if (descrUpdate.OLD_VALUE?.Trim().ToUpper() == descrUpdate.NEW_VALUE?.Trim().ToUpper())
                                    {
                                        // Continue execution if the old and new DESCR values are equal
                                    }
                                    else
                                    {
                                        return -1;
                                    }
                                }
                            }


                            if (string.IsNullOrEmpty(strTranId))
                            {
                                strTranId = "<<TRAN_ID>>";
                                Console.WriteLine("Generated TRAN_ID: " + strTranId); // Debug log
                                lstQry.Add($"fnGenerateTranID:'{strSiteCode}','{DateTime.Today:dd-MMM-yyyy}','MST','MT',{strSeqLength}");

                                // Insert into MASTERS_UNCONF
                                string insertMasterQuery = $@"
                                  INSERT INTO MASTERS_UNCONF (
                                  TRAN_ID, TRAN_DATE, TABLE_NAME, KEY_VAR, WF_PROCESS_LEVEL, SQL_QRY, CONFIRMED, CHG_DATE, CHG_USER, CHG_TERM, EMP_CODE, FILE_PATH, REMARKS ) 
                                  VALUES ( '{strTranId}', SYSDATE, '{strTableName}', '{strKeyVar}', {iWFLevel}, ' ', '{strConf}', SYSDATE, '{strEmp}', '{chg_Term}', '{strEmp}', '{strFilePath}','{remarks}' )";
                                lstQry.Add(insertMasterQuery);

                                // Call the update transaction ID method after commit
                                string updateTranIdQuery = $@"
                                  fnUpdateTranId:'{strSiteCode}','{strEmp}','{DateTime.Today:dd-MMM-yyyy}','MST','MT',{strSeqLength},'{strTranId}'";
                                lstQry.Add(updateTranIdQuery);

                                FnAddMastersUnconfReviewer(strEmp, strTranId, chg_Term, remarks,"", lstQry);
                            }

                            // Insert or update multiple fields in masters_unconf_det
                            foreach (var update in fieldUpdates)
                            {
                                string upsertDetailQuery = $@"
                                          MERGE INTO masters_unconf_det d
                                          USING ( SELECT '{strTranId}' AS TRAN_ID, 
                                                         '{update.FIELD_NAME}' AS FIELD_NAME FROM DUAL ) s
                                          ON (d.TRAN_ID = s.TRAN_ID AND d.FIELD_NAME = s.FIELD_NAME)
                                          WHEN MATCHED THEN
                                              UPDATE SET 
                                                  FIELD_DESCRIPTION = '{update.FIELD_DESCRIPTION}',
                                                  OLD_KEY = '{update.OLD_KEY}',
                                                  OLD_VALUE = '{update.OLD_VALUE}',
                                                  NEW_KEY = '{update.NEW_KEY}',
                                                  NEW_VALUE = '{update.NEW_VALUE}', 
                                                  FILE_PATH = '{strFilePath}', 
                                                  CHG_DATE = SYSDATE, 
                                                  CHG_USER = '{strEmp}', 
                                                  CHG_TERM = '{chg_Term}',
                                                  SEQ_NO = '{update.SEQ_NO}',
                                                  VISIBILITY = 'Y'
                                          WHEN NOT MATCHED THEN
                                              INSERT ( TRAN_ID, FIELD_NAME, FIELD_DESCRIPTION, OLD_KEY, OLD_VALUE, NEW_KEY, NEW_VALUE, FILE_PATH, CHG_DATE, CHG_USER, CHG_TERM, SEQ_NO, VISIBILITY ) 
                                              VALUES ( s.TRAN_ID, s.FIELD_NAME, '{update.FIELD_DESCRIPTION}', '{update.OLD_KEY}', '{update.OLD_VALUE}', '{update.NEW_KEY}', '{update.NEW_VALUE}', '{strFilePath}', SYSDATE, '{strEmp}', '{chg_Term}', '{update.SEQ_NO}','Y')";
                                lstQry.Add(upsertDetailQuery);
                            }

                            //lstQry.AddRange(lst1);
                            strResult = await QuryListExecuteTransaction(lstQry);

                            // Check the result and handle accordingly
                            if (strResult.StartsWith("Error"))
                            {
                                throw new Exception(strResult);
                            }

                            // Commit transaction
                            await transaction.CommitAsync();
                            iRowsAffected = 1; // Assuming at least one row affected if successful
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            throw; // Rethrow to handle upstream
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw; // Rethrow to handle upstream
            }
            return iRowsAffected;
        }



        public async Task<int> InsertOrUpdateEmployeeTransactionAsync(dynamic payload,string formName)
        {
            
            List<FieldUpdateRequest> fieldUpdates = new List<FieldUpdateRequest>();
            int iRowsAffected = 0;
            try
            {
                if (formName.ToUpper() == "BANK_DETAILS")
                {
                    
                    //dynamic jsonObject;
                    //if (formName.ToUpper() == "BANK_DETAILS")
                    //{
                    //    jsonObject = JsonConvert.DeserializeObject<CadreModel>(payload.ToString());
                    //}
                    //else
                    //{
                    //    jsonObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(payload.ToString());

                    //}

                    //EmployeeTransactionRequest payload.employeeBankDetRequests
                    //var Result= fnGetGenMstAsync("", payload.employeeBankDetRequests, "", "");
                    //if (Result.Result != null)
                    //{
                    //    throw new RepositoryException(Result.Result);
                    //}
                }

                List<string> lstQry = new List<string>();
                string strResult = "";

                using (var conn = _appDbContext.GetConn())
                {
                    await conn.OpenAsync();
                    using (var transaction = await conn.BeginTransactionAsync())
                    {
                        try
                        {
                            // Fetch existing values from the respective table
                            var existingValues = await GetExistingValuesAsync(payload.employeeBankDetRequests.EMP_CODE);

                            // Map new values from payload.employeeBankDetRequests
                            var employeeFields = new Dictionary<string, string>
                            {
                                { "EMP_CODE", payload.employeeBankDetRequests.EMP_CODE },
                                { "WORK_SITE", payload.employeeBankDetRequests.WORK_SITE },
                                { "GRATUITY_DATE", payload.employeeBankDetRequests.GRATUITY_DATE },
                                { "NOTICE_PRD", payload.employeeBankDetRequests.NOTICE_PRD.ToString() },
                                { "BASIC", payload.employeeBankDetRequests.BASIC.ToString() },
                                { "CURR_CTC", payload.employeeBankDetRequests.CURR_CTC.ToString() },
                                { "PAY_MODE", payload.employeeBankDetRequests.PAY_MODE },
                                { "PAY_TYPE", payload.employeeBankDetRequests.PAY_TYPE },
                                { "BANK_CODE", payload.employeeBankDetRequests.BANK_CODE?.Trim() },
                                { "BANK_IFSC", payload.employeeBankDetRequests.BANK_IFSC },
                                { "BANK_ACCT", payload.employeeBankDetRequests.BANK_ACCT },
                                { "DD_PAYABLE_BANK", payload.employeeBankDetRequests.DD_PAYABLE_BANK },
                                { "CHG_DATE", payload.employeeBankDetRequests.CHG_DATE },
                                { "CHG_USER", payload.employeeBankDetRequests.CHG_USER?.Trim() },
                                { "CHG_TERM", payload.employeeBankDetRequests.CHG_TERM }
                            };

                            // Create FieldUpdateRequest for each field
                            foreach (var field in employeeFields)
                            {
                                if (string.IsNullOrWhiteSpace(field.Key))
                                {
                                    throw new Exception("Field name cannot be null or empty. Please check the field mapping.");
                                }

                                var oldValue = existingValues.ContainsKey(field.Key) ? existingValues[field.Key] : null;

                                var fieldUpdate = new FieldUpdateRequest
                                {
                                    FIELD_NAME = field.Key.ToUpper(),
                                    OLD_VALUE = oldValue,
                                    NEW_VALUE = field.Value
                                };

                                // Log field update details
                                //Console.WriteLine($"Adding Field Update: {fieldUpdate.FieldName}, OldValue: {fieldUpdate.OldValue}, NewValue: {fieldUpdate.NewValue}");

                                fieldUpdates.Add(fieldUpdate);
                            }

                            // Further processing using payload.employeeBankDetRequests.TransactionInfo and other payload.employeeBankDetRequests properties...
                            string strTranId = payload.employeeBankDetRequests.TRAN_ID;

                            // Insert into MASTERS_UNCONF if needed
                            if (string.IsNullOrEmpty(strTranId))
                            {
                                strTranId = "<<TRAN_ID>>";
                                Console.WriteLine("Generated TRAN_ID: " + strTranId);
                                lstQry.Add($"fnGenerateTranID:'{payload.employeeBankDetRequests.SITE_CODE}','{DateTime.Today:dd-MMM-yyyy}','MST','MT',5");

                                // Insert into MASTERS_UNCONF
                                string insertMasterQuery = $@"
                            INSERT INTO MASTERS_UNCONF (
                            TRAN_ID, TRAN_DATE, TABLE_NAME, KEY_VAR, WF_PROCESS_LEVEL, SQL_QRY, CONFIRMED, CHG_DATE, CHG_USER, CHG_TERM, EMP_CODE, FILE_PATH, REMARKS ) 
                            VALUES ( '{strTranId}', SYSDATE, '{payload.employeeBankDetRequests.TABLE_NAME}', '{payload.employeeBankDetRequests.KEY_VAR}', {payload.employeeBankDetRequests.WF_LEVEL}, ' ', '{payload.employeeBankDetRequests.CONF}', SYSDATE, '{payload.employeeBankDetRequests.EMP}', '{payload.employeeBankDetRequests.CHG_TERM}', '{payload.employeeBankDetRequests.EMP}', '{payload.employeeBankDetRequests.FILE_PATH}', '{payload.employeeBankDetRequests.REMARKS}' )";
                                lstQry.Add(insertMasterQuery);

                                // Call the update transaction ID method after commit
                                string updateTranIdQuery = $@"
                            fnUpdateTranId:'{payload.employeeBankDetRequests.SITE_CODE}','{payload.employeeBankDetRequests.EMP}','{DateTime.Today:dd-MMM-yyyy}','MST','MT',5,'{strTranId}'";
                                lstQry.Add(updateTranIdQuery);

                                //FnAddMastersUnconfReviewer(payload.employeeBankDetRequests.EMP, strTranId, "192.168.0.1", "", lstQry);
                            }

                            // Insert or update multiple fields in masters_unconf_det
                            foreach (var update in fieldUpdates)
                            {
                                Console.WriteLine($"Processing Field: {update.FIELD_NAME}, OldValue: {update.OLD_VALUE}, NewValue: {update.NEW_VALUE}");

                                if (string.IsNullOrEmpty(update.FIELD_NAME))
                                {
                                    throw new Exception("FIELD_NAME cannot be null or empty.");
                                }

                                string upsertDetailQuery = $@"
                            MERGE INTO masters_unconf_det d
                            USING ( SELECT '{strTranId}' AS TRAN_ID, 
                                            '{update.FIELD_NAME}' AS FIELD_NAME FROM DUAL ) s
                            ON (d.TRAN_ID = s.TRAN_ID AND d.FIELD_NAME = s.FIELD_NAME)
                            WHEN MATCHED THEN
                                UPDATE SET 
                                    OLD_VALUE = '{update.OLD_VALUE}', 
                                    NEW_VALUE = '{update.NEW_VALUE}', 
                                    FILE_PATH = '{payload.employeeBankDetRequests.FILE_PATH}', 
                                    CHG_DATE = SYSDATE, 
                                    CHG_USER = '{payload.employeeBankDetRequests.EMP}', 
                                    CHG_TERM = '{payload.employeeBankDetRequests.CHG_TERM}'
                            WHEN NOT MATCHED THEN
                                INSERT ( TRAN_ID, FIELD_NAME, OLD_VALUE, NEW_VALUE, FILE_PATH, CHG_DATE, CHG_USER, CHG_TERM ) 
                                VALUES ( s.TRAN_ID, s.FIELD_NAME, '{update.OLD_VALUE}', '{update.NEW_VALUE}', '{payload.employeeBankDetRequests.FILE_PATH}', SYSDATE, '{payload.employeeBankDetRequests.EMP}', '{payload.employeeBankDetRequests.CHG_TERM}' )";
                                lstQry.Add(upsertDetailQuery);
                            }

                            strResult = await QuryListExecuteTransaction(lstQry);
                            if (strResult.StartsWith("Error"))
                            {
                                throw new Exception(strResult);
                            }

                            await transaction.CommitAsync();
                            iRowsAffected = 1; // Assuming at least one row affected if successful
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            throw; // Rethrow to handle upstream
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw; // Rethrow to handle upstream
            }
            return iRowsAffected;
        }

       

        public async Task<Dictionary<string, string>> GetExistingValuesAsync(string empCode)
        {
            var existingValues = new Dictionary<string, string>();

            var query = @$"
    SELECT EMP_CODE, WORK_SITE, GRATUITY_DATE, NOTICE_PRD, BASIC,GROSS*12 AS CURR_CTC, PAY_MODE, PAY_TYPE, BANK_CODE, BANK_IFSC, BANK_ACCT, DD_PAYABLE_BANK, CHG_DATE, CHG_USER, CHG_TERM
    FROM EMPLOYEE
    WHERE EMP_CODE = '{empCode}'";  // Using : for Oracle parameters

            using (var conn = _appDbContext.GetConn()) // Get connection from AppDbContext
            {
                await conn.OpenAsync();
                using (var transaction = await conn.BeginTransactionAsync())
                {
                    try
                    {
                        // Create OracleCommand
                        using (var command = new OracleCommand(query, conn as OracleConnection))
                        {
                            command.Transaction = transaction as OracleTransaction;
                            //command.Parameters.Add(new OracleParameter("EmpCode", empCode));
                            string fullQuery = GetFullQueryWithParameters(command);
                            using (var reader = await command.ExecuteReaderAsync())
                            {
                                if (await reader.ReadAsync())
                                {
                                    existingValues.Add("EMP_CODE", reader["EMP_CODE"] != DBNull.Value ? reader["EMP_CODE"].ToString() : null);
                                    existingValues.Add("WORK_SITE", reader["WORK_SITE"] != DBNull.Value ? reader["WORK_SITE"].ToString() : null);
                                    existingValues.Add("GRATUITY_DATE", reader["GRATUITY_DATE"] != DBNull.Value ? Convert.ToDateTime(reader["GRATUITY_DATE"]).ToString("yyyy-MM-dd") : null);
                                    existingValues.Add("NOTICE_PRD", reader["NOTICE_PRD"] != DBNull.Value ? Convert.ToInt32(reader["NOTICE_PRD"]).ToString() : null);
                                    existingValues.Add("BASIC", reader["BASIC"] != DBNull.Value ? Convert.ToDecimal(reader["BASIC"]).ToString() : null);
                                    existingValues.Add("CURR_CTC", reader["CURR_CTC"] != DBNull.Value ? Convert.ToDecimal(reader["CURR_CTC"]).ToString() : null);
                                    existingValues.Add("PAY_MODE", reader["PAY_MODE"] != DBNull.Value ? reader["PAY_MODE"].ToString() : null);
                                    existingValues.Add("PAY_TYPE", reader["PAY_TYPE"] != DBNull.Value ? reader["PAY_TYPE"].ToString() : null);
                                    existingValues.Add("BANK_CODE", reader["BANK_CODE"] != DBNull.Value ? reader["BANK_CODE"].ToString().Trim() : null);
                                    existingValues.Add("BANK_IFSC", reader["BANK_IFSC"] != DBNull.Value ? reader["BANK_IFSC"].ToString() : null);
                                    existingValues.Add("BANK_ACCT", reader["BANK_ACCT"] != DBNull.Value ? reader["BANK_ACCT"].ToString() : null);
                                    existingValues.Add("DD_PAYABLE_BANK", reader["DD_PAYABLE_BANK"] != DBNull.Value ? reader["DD_PAYABLE_BANK"].ToString() : null);
                                    existingValues.Add("CHG_DATE", reader["CHG_DATE"] != DBNull.Value ? Convert.ToDateTime(reader["CHG_DATE"]).ToString("yyyy-MM-dd") : null);
                                    existingValues.Add("CHG_USER", reader["CHG_USER"] != DBNull.Value ? reader["CHG_USER"].ToString().Trim() : null);
                                    existingValues.Add("CHG_TERM", reader["CHG_TERM"] != DBNull.Value ? reader["CHG_TERM"].ToString() : null);
                                }
                            }
                        }

                        await transaction.CommitAsync();
                    }
                    catch (Exception ex)
                    {
                        await transaction.RollbackAsync();
                        Console.WriteLine($"Exception: {ex.Message}");
                        throw;
                    }
                }
            }

            return existingValues;
        }


        public async Task<List<string>> FnAddMastersUnconfReviewer(string empCode, string strTranId, string chg_Term, string remarks, string Remarks, List<string> lstQuery)
        {

            try
            {
                string sanEmpCode = "";
                int sequence = 0;
                using (var connection = _appDbContext.GetConn())
                {

                    await connection.OpenAsync();
                    string selectQuery = @"
                    SELECT M_SANEMPCODE, M_SEQUENCE 
                    FROM M_WORKFLOW 
                    WHERE M_EMPCODE = :empCode
                    AND M_MODULETYPE = 'MASTERS' 
                    ORDER BY M_SEQUENCE";



                    using (var cmd = _appDbContext.GetCommand())
                    {
                        cmd.CommandText = selectQuery;
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add(new OracleParameter("empCode", empCode.Trim()));
                        int Line_no = 0;
                        using (var reader = await cmd.ExecuteReaderAsync())
                        {
                            string insertQry = $"INSERT INTO MASTERS_UNCONF_REVIEWER (TRAN_ID, LINE_NO, LEVEL_NO, EMP_CODE__REVIEW, REMARKS, CONFIRMED, CHG_USER, CHG_DATE, CHG_TERM) " +
                                                     $"VALUES ('{strTranId}', 0, 0, '{empCode}', '{remarks}', 'Y', '{empCode}', SYSDATE, '{chg_Term}')";
                            lstQuery.Add(insertQry);
                            //sanEmpCode,empCode = loggenIn user
                            while (await reader.ReadAsync())
                            {
                                Line_no++;
                                sanEmpCode = reader["M_SANEMPCODE"].ToString();
                                sequence = Convert.ToInt32(reader["M_SEQUENCE"]);
                                string insertQuery = $"INSERT INTO MASTERS_UNCONF_REVIEWER (TRAN_ID, LINE_NO, LEVEL_NO, EMP_CODE__REVIEW, REMARKS, CONFIRMED, CHG_USER, CHG_DATE, CHG_TERM) " +
                                                     $"VALUES ('{strTranId}', '{Line_no}', {sequence}, '{sanEmpCode}', '{Remarks}', 'O', '{empCode}', null, '{chg_Term}')";
                                lstQuery.Add(insertQuery);
                            }
                        }
                    }

                    return lstQuery;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task<bool> fnUpdateTranIdAsync(string strSiteCode, string strEmpCode, string strDate, string strRefSer, string strRefPrefix, int strSeqLen, string strTranID)
        {
            try
            {
                string Qry = "";
                using (var conn = _appDbContext.GetConn())
                {
                    conn.Open();
                    if (strSiteCode != "" && strDate != "" && strSeqLen == 5)
                    {
                        Qry = "UPDATE REFSEQ SET SEQ_NO = '" + strTranID.Substring(5, 5) + "', CHG_DATE = SYSDATE, CHG_USER = '" + strEmpCode + "', " +
                            "CHG_TERM='" + _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() + "' WHERE REF_SER = '" + strRefSer + "' And Trim(REF_PREFIX) = '" + strTranID.Substring(0, 5) + "' ";
                    }
                    else if (strRefPrefix == "")
                    {
                        Qry = "UPDATE REFSEQ SET SEQ_NO = '" + strTranID + "', CHG_DATE = SYSDATE, CHG_USER = '" + strEmpCode + "', " +
                            "CHG_TERM='" + _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() + "' WHERE REF_SER = '" + strRefSer + "' ";
                    }
                    else if (strRefPrefix != "")
                    {
                        Qry = "UPDATE REFSEQ SET SEQ_NO = '" + strTranID.Substring(strTranID.Length - strSeqLen) + "', CHG_DATE = SYSDATE, CHG_USER = '" + strEmpCode + "', " +
                            "CHG_TERM='" + _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString() + "' WHERE REF_SER = '" + strRefSer + "' And Trim(REF_PREFIX) = '" + strTranID.Substring(0, (strTranID.Length - strSeqLen)) + "' ";
                    }

                    if (Qry != "")
                    {
                        using (var cmd = new OracleCommand(Qry, conn))
                        {
                            int iRowsAffected = cmd.ExecuteNonQuery();
                            if (iRowsAffected == 0)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public static string GetFullQueryWithParameters(OracleCommand command)
        {
            string fullQuery = command.CommandText;

            foreach (OracleParameter parameter in command.Parameters)
            {
                string parameterValue = GetParameterFormattedValue(parameter);
                fullQuery = fullQuery.Replace(':' + parameter.ParameterName, parameterValue);
            }
            return fullQuery;
        }

        private static string GetParameterFormattedValue(OracleParameter parameter)
        {
            if (parameter.Value == null || parameter.Value == DBNull.Value)
            {
                return "NULL";
            }
            else if (parameter.DbType == DbType.String || parameter.DbType == DbType.AnsiString || parameter.DbType == DbType.DateTime)
            {
                return "'" + parameter.Value.ToString().Replace("'", "''") + "'";
            }
            else
            {
                return parameter.Value.ToString();
            }
        }


        // working
        public async Task<string> fnGenerateTranID(string strSiteCode, string strDate, string strRefSer, string strRefPrefix, int strSeqLength = 5)
        {
            try
            {
                int strSeqLen = strSeqLength;
                string strTranId = "";
                string Ls_Site_Doc_Prefix = "";
                string Ls_Prd_Doc_Prefix = "";
                string Ls_Ref_Prefix = "";
                string Ls_New_Seq_No = "";

                using (var conn = _appDbContext.GetConn())
                {
                    await conn.OpenAsync();

                    // Fetch Site Document Prefix
                    if (!string.IsNullOrEmpty(strSiteCode))
                    {
                        string qry = $"SELECT Doc_prefix FROM Site WHERE site_code = :siteCode";
                        using (var cmd = new OracleCommand(qry, conn))
                        {
                            cmd.Parameters.Add("siteCode", OracleDbType.Varchar2).Value = strSiteCode;
                            Ls_Site_Doc_Prefix = Convert.ToString(await cmd.ExecuteScalarAsync());
                        }
                    }

                    // Fetch Period Document Prefix
                    if (!string.IsNullOrEmpty(strDate))
                    {
                        //string qry = "SELECT Trim(Doc_prefix) FROM Period WHERE fr_date <= SYSDATE AND to_date >= SYSDATE";
                        string qry = $"SELECT Trim(Doc_prefix) FROM Period WHERE fr_date <= '{strDate}' AND to_date >= '{strDate}'";
                        using (var cmd = new OracleCommand(qry, conn))
                        {
                            Ls_Prd_Doc_Prefix = Convert.ToString(await cmd.ExecuteScalarAsync());
                        }
                    }

                    Ls_Ref_Prefix = $"{Ls_Site_Doc_Prefix}{strRefPrefix}{Ls_Prd_Doc_Prefix}";

                    if (!string.IsNullOrEmpty(strSiteCode) && !string.IsNullOrEmpty(strDate) && (Ls_Ref_Prefix.Length + strSeqLen > 10))
                    {
                        return "Invalid Site Period And Sequence Combination!!!";
                    }

                    // Check if RefSeq exists
                    /*string checkQry = $"SELECT Count(1) FROM RefSeq WHERE Ref_Ser = :refSer";
                    if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                    {
                        checkQry += " AND Trim(Ref_Prefix) = :refPrefix";
                    }*/
                    string checkQry = "Select Count(1) From RefSeq where Ref_Ser = '" + strRefSer + "' ";

                    if (Ls_Ref_Prefix != "")
                    {
                        checkQry = checkQry + "And Trim(Ref_Prefix) = '" + Ls_Ref_Prefix + "' ";
                    }


                    using (var cmd = new OracleCommand(checkQry, conn))
                    {
                        /* cmd.Parameters.Add("refSer", OracleDbType.Varchar2).Value = strRefSer;
                         if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                         {
                             cmd.Parameters.Add("refPrefix", OracleDbType.Varchar2).Value = Ls_Ref_Prefix;
                         }*/

                        int iRowsAffected = Convert.ToInt32(await cmd.ExecuteScalarAsync() ?? 0);
                        if (iRowsAffected == 0)
                        {
                            // Insert new RefSeq record
                            string insertQry = $"INSERT INTO RefSeq (Ref_Ser, Ref_Prefix, Seq_No, CHG_DATE, CHG_USER, CHG_TERM) " +
                                               $"VALUES (:refSer, :refPrefix, LPAD('0', :seqLen, '0'), SYSDATE, 'MAC', :remoteAddr)";
                            using (var cmdInsert = new OracleCommand(insertQry, conn))
                            {
                                cmdInsert.Parameters.Add("refSer", OracleDbType.Varchar2).Value = strRefSer;
                                cmdInsert.Parameters.Add("refPrefix", OracleDbType.Varchar2).Value = Ls_Ref_Prefix;
                                cmdInsert.Parameters.Add("seqLen", OracleDbType.Int32).Value = strSeqLen;
                                cmdInsert.Parameters.Add("remoteAddr", OracleDbType.Varchar2).Value = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

                                iRowsAffected = await cmdInsert.ExecuteNonQueryAsync();
                                if (iRowsAffected == 0)
                                {
                                    return "Error Inserting TranID!!!";
                                }
                            }
                        }
                        else if (iRowsAffected > 1)
                        {
                            return "Please Mention Ref Prefix!!!";
                        }
                    }

                    // Fetch next sequence number
                    string seqQry = $"SELECT Length(Trim(Seq_No)) AS LEN, LPad(Seq_No + 1, {strSeqLen}, '0') AS SEQ " +
                                    $"FROM RefSeq WHERE Trim(Ref_Ser) = :refSer";
                    if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                    {
                        seqQry += $" AND Trim(Ref_Prefix) = :refPrefix";
                    }
                    using (var cmd = new OracleCommand(seqQry, conn))
                    {
                        cmd.Parameters.Add("refSer", OracleDbType.Varchar2).Value = strRefSer;
                        if (!string.IsNullOrEmpty(Ls_Ref_Prefix))
                        {
                            cmd.Parameters.Add("refPrefix", OracleDbType.Varchar2).Value = Ls_Ref_Prefix;
                        }

                        using (var reader = await cmd.ExecuteReaderAsync())
                        {
                            if (!reader.HasRows)
                            {
                                return "No Next Sequence Found!!!";
                            }

                            while (await reader.ReadAsync())
                            {
                                int length = reader["LEN"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LEN"]);
                                string seq = reader["SEQ"] == DBNull.Value ? string.Empty : Convert.ToString(reader["SEQ"]);

                                if (length != strSeqLength)
                                {
                                    return "Existing Sequence Length Is Not Equal To Provided Length!!!";
                                }
                                else
                                {
                                    Ls_New_Seq_No = seq.Trim();
                                }
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(Ls_New_Seq_No))
                    {
                        return "No Next Sequence Found!!!";
                    }

                    strTranId = $"{Ls_Ref_Prefix}{Ls_New_Seq_No}";
                    return strTranId;
                }
            }
            catch (Exception ex)
            {
                return $"Error: {ex.Message}";
            }
        }


        // Reject     
        public async Task RejectTransaction([FromBody] RejectTransactionRequest request)
        {
            try
            {
                int rec = 0;
                List<string> lstQuery = new List<string>();
                string query = $"UPDATE MASTERS_UNCONF SET CONFIRMED='R', CHG_DATE=SYSDATE, CHG_USER='{request.EmpCode.Trim()}', CHG_TERM='{request.RemoteAddr.Trim()}' " +
                               $"WHERE TRAN_ID='{request.TransactionId.Trim()}' AND CONFIRMED='N'";
                lstQuery.Add(query);
                rec++;

                string updateQueryrRevi = $"UPDATE MASTERS_UNCONF_REVIEWER SET CONFIRMED = 'R',REMARKS='{request.Remarks}', CHG_TERM = '{request.RemoteAddr}', CHG_DATE = SYSDATE " +
                                          $" WHERE  TRAN_ID = '{request.TransactionId.Trim()}' AND EMP_CODE__REVIEW = '{request.EmpCode.Trim()}'";
                lstQuery.Add(updateQueryrRevi);
                rec++;

                string strResult = await fnExecuteTransaction(lstQuery);

            }
            catch (Exception ex)
            {
                if (ex is DbUpdateException || ex is OracleException)
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while rejecting transaction: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }


        public async Task<string> fnExecuteTransaction(List<string> lstQuery)
        {
            int rec = lstQuery.Count; // Adjust this as per your logic to determine `rec`

            if (lstQuery.Count == rec)
            {
                using (var connection = _appDbContext.GetConn())
                {
                    await connection.OpenAsync();
                    using (var transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            foreach (var query in lstQuery)
                            {
                                using (var command = new OracleCommand(query, connection))
                                {
                                    command.Transaction = transaction;
                                    await command.ExecuteNonQueryAsync();
                                }
                            }

                            // Commit the transaction if all queries execute successfully
                            transaction.Commit();
                            return "Transaction committed successfully";
                        }
                        catch (Exception ex)
                        {
                            // Rollback the transaction if any query fails
                            transaction.Rollback();
                            return $"Transaction failed and rolled back: {ex.Message}";
                        }
                    }
                }
            }
            else
            {
                return "Query count does not match the expected record count";
            }
        }

        //Approve
        public async Task ApproveTransaction(string tranId, string ipAddress, string empCode, string SessionempCode, string tableName, int wfProcLevel, string moduleType, string Remarks)
        {
            int maxLevel = await _transactionRepository.FnGetMaximunSanctionAuthority(SessionempCode, moduleType);

            string s_TRAN_WINDOW;
            int rec = 0;
            List<string> lstQuery = new List<string>();

            try
            {

                // Check if the employee's sanction level is greater than the workflow process level
                if (maxLevel > wfProcLevel)
                {

                    string updateQuery = $@"
                        UPDATE MASTERS_UNCONF 
                        SET CONFIRMED = 'N', 
                            CHG_USER = '{SessionempCode}', 
                            CHG_TERM = '{ipAddress}', 
                            CHG_DATE = SYSDATE, 
                            WF_PROCESS_LEVEL = '{wfProcLevel + 1}'
                        WHERE TABLE_NAME = '{tableName}' 
                            AND TRAN_ID = '{tranId}' 
                            AND CONFIRMED = 'N'";

                    lstQuery.Add(updateQuery);
                    rec++;
                 
                                       
                }
                else
                {
                    // Prepare the query to update the MASTERS_UNCONF table with the workflow process level

                    // Get the transaction details and build the insert queries
                    var transactionDetailsQuery = await _transactionRepository.GetTransactionDetails(tranId, tableName);

                    if (transactionDetailsQuery.Trim().StartsWith("INSERT", StringComparison.OrdinalIgnoreCase))
                    {
                        switch (tableName)
                        {
                            case "DEPARTMENT":
                                s_TRAN_WINDOW = "SLA__DEPARTMENT";
                                break;
                            case "DESIGNATION":
                                s_TRAN_WINDOW = "SLA__DESIGNATIONLIST";
                                break;
                            case "GRADE":
                                s_TRAN_WINDOW = "SLA__GRADELIST";
                                break;
                            case "CADRE":
                                s_TRAN_WINDOW = "SLA__CADRELIST";
                                break;
                            case "QUALIFICATION":
                                s_TRAN_WINDOW = "SLA__QUALIFICATIONLIST";
                                break;
                            case "EMP_ROLES":
                                s_TRAN_WINDOW = "SLA__EMP_ROLES";
                                break;
                            default:
                                throw new ArgumentException($"Unsupported table name: {tableName}");
                        }

                        // Generate the code for the table
                        string code = _autoGenerateCodeRepository.GenerateCode(tableName, s_TRAN_WINDOW);

                        // Replace the placeholder in the INSERT query with the generated code
                        transactionDetailsQuery = transactionDetailsQuery.Replace("NULL", $"'{code}'");

                        // Add the modified query to the list
                        lstQuery.Add(transactionDetailsQuery);
                        rec++;
                    }
                    else
                    {
                        lstQuery.Add(transactionDetailsQuery);
                        rec++;
                    }

                    // Prepare the query to update the MASTERS_UNCONF table
                    string updateQuery = $@"
                        UPDATE MASTERS_UNCONF 
                            SET CONFIRMED = 'Y', 
                            CHG_USER = '{SessionempCode}', 
                            CHG_TERM = '{ipAddress}', 
                            CHG_DATE = SYSDATE , 
                            WF_PROCESS_LEVEL = '{maxLevel}'
                        WHERE TABLE_NAME = '{tableName}' 
                            AND TRAN_ID = '{tranId}' 
                            AND CONFIRMED = 'N'";
                    lstQuery.Add(updateQuery);
                    rec++;
                }

                string updateQueryrRevi = $@"
                        UPDATE MASTERS_UNCONF_REVIEWER 
                            SET CONFIRMED = 'Y',
                            REMARKS='{Remarks}', 
                            CHG_TERM = '{ipAddress}', 
                            CHG_DATE = SYSDATE 
                        WHERE TRAN_ID = '{tranId.Trim()}' 
                        AND EMP_CODE__REVIEW = '{SessionempCode.Trim()}'";
                lstQuery.Add(updateQueryrRevi);
                rec++;
               

                // Execute the queries if the number of queries matches the count
                if (lstQuery.Count == rec)
                {
                    using (var connection = _appDbContext.GetConn())
                    {
                        await connection.OpenAsync();
                        using (var transaction = connection.BeginTransaction())
                        {
                            try
                            {
                                foreach (var query in lstQuery)
                                {
                                    using (var command = new OracleCommand(query, connection))
                                    {
                                        command.Transaction = transaction;
                                        await command.ExecuteNonQueryAsync();
                                    }
                                }

                                // Commit the transaction if all queries execute successfully
                                transaction.Commit();
                            }
                            catch
                            {
                                // Rollback the transaction if any query fails
                                transaction.Rollback();
                                throw;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is DbUpdateException || ex is OracleException)
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while processing transaction: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }


        public async Task<string> fnGetGenMstAsync(string s_MOD_NAME, dynamic jsonData, string s_TRAN_MODE, string s_KEYFIELDVAL)
        {
            try
            {
                dynamic jsonObject;
                if (s_TRAN_MODE == "CADRE")
                {
                    jsonObject = JsonConvert.DeserializeObject<CadreModel>(jsonData.ToString());
                }
                else
                {
                    jsonObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(jsonData.ToString());

                }

                //var items = jsonArray.Select(item => item.ToObject<CadreModel>()).ToList();
                //string DESCR = jsonObject["DESCR"]?.ToString() ?? "";

                List<string> lstQry = new List<string>();
                //DataTable dt = null;
                string query = $"SELECT * FROM GENMST WHERE MOD_NAME = '"+ s_MOD_NAME + "' ";
                // string query = "SELECT * FROM GENMST WHERE MOD_NAME = '" + s_MOD_NAME + "' ";
                using (OracleCommand command = new OracleCommand(query))
                {
                    var genmstdata = await _appDbContext.GENMST.FromSqlRaw(query).ToListAsync();

                    foreach (var row in genmstdata)
                    {
                        var strSqlInputFlds = row.SQL_INPUT.ToString();
                        var strQry = row.FLD_MIN.ToString();
                        var strMsg = row.DESCR.ToString();
                        var strResultVal = row.FLD_MAX.ToString();
                        //var strValMode = row.VAL_STAGE.ToString();

                        var arrSqlInputFlds = strSqlInputFlds.Split(',');
                        var arrFieldVal = new ObservableCollection<string>();

                        foreach (var fld in arrSqlInputFlds)
                        {
                            string flieldName = fld.Replace(":0.", "").ToUpper().Trim();
                            //string? fieldValue = Convert.ToString(fields.Find(p => p.flieldName)?.Value);
                            string? fieldValue = GetPropertyValue(jsonObject, flieldName);
                            //string? fieldValue = jsonObject[flieldName]?.ToString() ?? ""; ;

                            if (!string.IsNullOrEmpty(fieldValue))
                            {
                                arrFieldVal.Add($"{flieldName}={fieldValue.Replace("'", "''")}");
                            }
                        }

                        foreach (var fieldVal in arrFieldVal)
                        {
                            string splitFieldVal = fieldVal.Split('=')[1];
                            string fieldValue = splitFieldVal.Replace("'", "");
                            if (DateTime.TryParseExact(fieldValue, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dtTime))
                            {
                                strQry = strQry.Replace("?", $"'{dtTime:dd-MMM-yyyy}'");
                            }
                            else
                            {
                                strQry = strQry.Replace("?", $"'{fieldValue}'");
                            }
                        }

                        //strQry = fnReplaceSessionVar(strQry, "SessionLoginSite", "SessionLOC_GROUP", "SessionEmpCode", "SessionSiteCode", "SessionDeptCode", "SessionProfileID", "SessionLOC_GROUPType");

                        //if (strQry.Contains("<<STRKEYFIELDVAL>>"))
                        //{
                        //    strQry = strQry.Replace("<<STRKEYFIELDVAL>>", s_KEYFIELDVAL);
                        //}
                    }

                }

                var strResult = await fnDBValidationBulkAsync(lstQry, null);
                return strResult;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static string fnReplaceSessionVar(string strQry, string SessionLoginSite, string SessionLOC_GROUP, string SessionEmpCode, string SessionSiteCode, string SessionDeptCode, string SessionProfileID, string SessionLOC_GROUPType)
        {
            string Qry = "";
            Qry = strQry.ToUpper().Replace("<<SESSIONLOGINSITE>>", SessionLoginSite)
                .Replace("<<SESSIONLOC_GROUP>>", SessionLOC_GROUP)
                .Replace("<<SESSIONEMPCODE>>", SessionEmpCode)
                .Replace("<<SESSIONSITECODE>>", SessionSiteCode)
                .Replace("<<SESSIONDEPTCODE>>", SessionDeptCode)
                .Replace("<<SESSIONPROFILE_ID>>", SessionProfileID)
                .Replace("<<SESSIONLOC_GROUPTYPE>>", SessionLOC_GROUPType);
            return Qry;
        }

        //public string? GetPropertyValue(List<T> model, string propertyName)
        //{
        //    var propertyInfo = typeof(T).GetProperty(propertyName.Trim());
        //    if (propertyInfo != null)
        //    {
        //        var value = propertyInfo.GetValue(model);

        //        return Convert.ToString(value);
        //    }
        //    return null;
        //}

        public string? GetPropertyValue(dynamic jsonObject, string propertyName)
        {
            try
            {
                // Use JObject for CADRE mode
                //if (jsonObject is JObject jObject)
                //{
                //    return jObject[propertyName]?.ToString();
                //}
                //// Use dynamic type for other cases
                //return jsonObject[propertyName]?.ToString();

                // Convert propertyName to uppercase to ensure case insensitivity
                string upperPropertyName = propertyName.ToUpper();

                // Iterate through all properties in jsonObject
                foreach (var property in jsonObject.Properties())
                {
                    // Check if the property name matches the required name, case insensitive
                    if (property.Name.ToUpper() == upperPropertyName)
                    {
                        return property.Value.ToString();
                    }
                }

                // Return null if the property is not found
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<string> fnDBValidationBulkAsync(List<string> arrQry, string DB_Type)
        {
            try
            {
                string strErrorMsg = string.Empty;

                for (int i = 0; i < arrQry.Count; i++)
                {
                    using (var conn = _appDbContext.GetConn())
                    {
                        string strQry = arrQry[i].Split('~')[0];
                        string strMsg = arrQry[i].Split('~')[1];
                        string strResultVal = arrQry[i].Split('~')[2];
                        await conn.OpenAsync();
                        DbCommand cmd = _appDbContext.GetCommand();
                        var strResult = await cmd.ExecuteScalarAsync();
                        if (strResult != null || string.IsNullOrEmpty(Convert.ToString(strResult)))
                        {
                            if (Char.IsNumber(strResult.ToString(), 0))
                            {
                                if (strResult.ToString() != strResultVal)
                                {
                                    return (strMsg);
                                }
                            }
                            else if (strResult.ToString() != strResultVal)
                            {
                                return (strResult.ToString());
                            }
                        }
                    }
                }

                return strErrorMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region commented code fnGenMst
        //public async Task<string> fnGetGenMstAsync(string s_MOD_NAME, [FromBody] List<DynamicField> fields, string s_TRAN_MODE, string s_KEYFIELDVAL)
        //    {
        //        try
        //        {
        //            List<string> lstQry = new List<string>();
        //            //DataTable dt = null;
        //            string query = $"SELECT * FROM GENMST WHERE MOD_NAME = 'W_CUSTOMER' ";
        //           // string query = "SELECT * FROM GENMST WHERE MOD_NAME = '" + s_MOD_NAME + "' ";
        //            using (OracleCommand command = new OracleCommand(query))
        //            {
        //                // var genmstdata = await _appDbContext.ExecuteReaderAsync<GENMST>(query);
        //                //var genmstdata = await _appDbContext.ExecuteReaderAsync<FromBodyAttribute>(query);

        //                var genmstdata = await _appDbContext.GENMST.FromSqlRaw(query).ToListAsync();

        //                foreach (var row in genmstdata)
        //                {

        //                    //var row = obj as Dictionary<string, object>;
        //                    //var strSqlInputFlds = row["SQL_INPUT"].ToString();
        //                    //var strQry = row["FLD_MIN"].ToString();
        //                    //var strMsg = row["MSG_DESCR"].ToString();
        //                    //var strResultVal = row["FLD_MAX"].ToString();
        //                    //var strValMode = row["VAL_MODE"].ToString();

        //                    var strSqlInputFlds = row.SQL_INPUT.ToString();
        //                    var strQry = row.FLD_MIN.ToString();
        //                    var strMsg = row.DESCR.ToString();
        //                    var strResultVal = row.FLD_MAX.ToString();
        //                    //var strValMode = row.VAL_STAGE.ToString();

        //                    var arrSqlInputFlds = strSqlInputFlds.Split(',');
        //                    var arrFieldVal = new ObservableCollection<string>();

        //                    foreach (var fld in arrSqlInputFlds)
        //                    {
        //                        string flieldName = fld.Replace(":0.", "").ToUpper().Trim();
        //                        string? fieldValue = Convert.ToString(fields.Find(p => p.Name == flieldName)?.Value);

        //                        if (!string.IsNullOrEmpty(fieldValue))
        //                        {
        //                            arrFieldVal.Add($"{flieldName}={fieldValue.Replace("'", "''")}");
        //                        }
        //                    }

        //                    foreach (var fieldVal in arrFieldVal)
        //                    {
        //                        string splitFieldVal = fieldVal.Split('=').ToString();
        //                        string fieldValue = splitFieldVal.Replace("'", "");
        //                        if (DateTime.TryParseExact(fieldValue, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dtTime))
        //                        {
        //                            strQry = strQry.Replace("?", $"'{dtTime:dd-MMM-yyyy}'");
        //                        }
        //                        else
        //                        {
        //                            strQry = strQry.Replace("?", $"'{fieldValue}'");
        //                        }
        //                    }

        //                    strQry = fnReplaceSessionVar(strQry, "SessionLoginSite", "SessionLOC_GROUP", "SessionEmpCode", "SessionSiteCode", "SessionDeptCode", "SessionProfileID", "SessionLOC_GROUPType");

        //                    if (strQry.Contains("<<STRKEYFIELDVAL>>"))
        //                    {
        //                        strQry = strQry.Replace("<<STRKEYFIELDVAL>>", s_KEYFIELDVAL);
        //                    }

        //                    //if (strValMode.ToUpper().Contains(s_TRAN_MODE.ToUpper()))
        //                    //{
        //                    //    lstQry.Add($"{strQry}~{strMsg}~{strResultVal}");
        //                    //}
        //                }

        //            }

        //            var strResult = QuryListExecuteTransaction(lstQry);
        //            return strResult.Result;
        //        }
        //        catch (Exception ex)
        //        {
        //            return ex.Message;
        //        }
        //    }

        //public static string fnReplaceSessionVar(string strQry, string SessionLoginSite, string SessionLOC_GROUP, string SessionEmpCode, string SessionSiteCode, string SessionDeptCode, string SessionProfileID, string SessionLOC_GROUPType)
        //    {
        //        string Qry = "";
        //        Qry = strQry.ToUpper().Replace("<<SESSIONLOGINSITE>>", SessionLoginSite)
        //            .Replace("<<SESSIONLOC_GROUP>>", SessionLOC_GROUP)
        //            .Replace("<<SESSIONEMPCODE>>", SessionEmpCode)
        //            .Replace("<<SESSIONSITECODE>>", SessionSiteCode)
        //            .Replace("<<SESSIONDEPTCODE>>", SessionDeptCode)
        //            .Replace("<<SESSIONPROFILE_ID>>", SessionProfileID)
        //            .Replace("<<SESSIONLOC_GROUPTYPE>>", SessionLOC_GROUPType);
        //        return Qry;
        //    }
        #endregion commented code fnGenMst
    }
}
