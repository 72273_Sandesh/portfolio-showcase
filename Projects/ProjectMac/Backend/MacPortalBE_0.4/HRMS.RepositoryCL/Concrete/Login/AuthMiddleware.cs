﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HRMS_Core.Data.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Data.SqlClient;
using System.Data;
using HRMS.RepositoryCL.Comman;
using Oracle.ManagedDataAccess.Client;
using HRMS.ModelCL;
using HRMS.ModelCL.Login;
using Microsoft.AspNetCore.Mvc;

public class AuthMiddleware
{
    private readonly RequestDelegate _next;
    private readonly string[] allowedPaths = {"/api/Auth/Login", "/api/CtrlData/GetAllControllerDetails", "/api/CtrlData/InsertIntoControllerMaster" };
    public AuthMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context)
    {


        if (IsAllowedPath(context.Request.Path))
        {
            // If it is, proceed to the next middleware without authentication
            await _next(context);
            return;
        }

        var dbContextOptions = context.RequestServices.GetRequiredService<DbContextOptions<AppDbContext>>();

        // Get IConfiguration from the service provider
        var configuration = context.RequestServices.GetRequiredService<IConfiguration>();

        // Instantiate AppDbContext within the scope of the HTTP request
        using (var appDbContext = new AppDbContext(dbContextOptions, configuration))
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            if (token == null)
            {
                context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                context.Response.ContentType = "text/plain"; // Set content type as needed
                var message = "Unauthorized.Please authenticate with valid token to access this resource.";
                await context.Response.WriteAsync(message);
                return;
            }
            try
            {
                //var profileId = await GetProfileId(appDbContext, token); // Await GetProfileId method
                var profileId = GetProfileIdFromToken(token);
                if (profileId == null)
                {
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    context.Response.ContentType = "text/plain"; // Set content type as needed
                    var message = "Unauthorized. User does not have the role to access this resource";
                    await context.Response.WriteAsync(message);
                    return;
                }
                bool profileIdFound = await CheckProfileIdRights(appDbContext, context.Request.Path, profileId);
                if (profileIdFound != true )
                {
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    context.Response.ContentType = "text/plain"; // Set content type as needed
                    var message = "Unauthorized.User does not have the role to access this resource.";
                    await context.Response.WriteAsync(message);
                    return;
                }

                await _next(context);
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error getting erro while check user rights: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }
    }

    //private async Task<string> GetProfileId(AppDbContext appDbContext, string token)
    //{
    //    try
    //    {
    //        // Get the userId from the token
    //        var userId = GetUserIdFromToken(token);

    //        // Execute SQL query to retrieve PROFILE_ID based on userId
    //        var query = $"SELECT PROFILE_ID FROM scm_rights WHERE EMP_CODE = '{userId.Trim()}'";

    //        await appDbContext.Database.OpenConnectionAsync(); // Open connection asynchronously

    //        using (var command = appDbContext.Database.GetDbConnection().CreateCommand())
    //        {
    //            command.CommandText = query;

    //            using (var result = await command.ExecuteReaderAsync())
    //            {
    //                if (result != null && await result.ReadAsync())
    //                {
    //                    return result.GetString(0); // Assuming PROFILE_ID is a string
    //                }
    //            }
    //        }

    //        return null; // If no result found
    //    }
    //    finally
    //    {
    //        await appDbContext.Database.CloseConnectionAsync(); // Close connection asynchronously
    //    }
    //}

    private string GetProfileIdFromToken(string token)
    {
        // logic to extract user ID from the JWT token
        try
        {
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(token) as JwtSecurityToken;
            var profileId = jsonToken?.Claims.First(c => c.Type == "profileId").Value;
            return profileId;
        }
        catch (Exception ex)
        {
            string str = ex.GetType().ToString();
            if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
            {
                string error = ex.InnerException?.Message != null ? ex.InnerException.Message : ex.Message;
                throw new RepositoryException(error);
            }

            string errorMessage = $"Error getting erro while check user rights: {ex.Message}";

            if (ex.InnerException != null)
            {
                errorMessage += $", Inner Exception: {ex.InnerException.Message}";
            }

            throw new Exception(errorMessage, ex.InnerException);
        }
    }

    //public async Task<List<string>> GetProfileRoles(AppDbContext dbContext, string profileId)
    //{
    //    var roles = new List<string>();
    //    try
    //    {
    //        //var query = "SELECT DISTINCT profile_id FROM controller_rights WHERE PROFILE_ID = :profileId";
    //        roles = await dbContext.USER_RIGHTS
    //                               .Where(ur => ur.ProfileId == profileId)
    //                               .Select(ur => ur.CtrEndpointIds)
    //                               .ToListAsync();
    //    }
    //    catch (Exception ex)
    //    {
    //        string str = ex.GetType().ToString();
    //        if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
    //        {
    //            string error = ex.InnerException?.Message != null ? ex.InnerException.Message : ex.Message;
    //            throw new RepositoryException(error);
    //        }

    //        string errorMessage = $"Error getting erro while check user rights: {ex.Message}";

    //        if (ex.InnerException != null)
    //        {
    //            errorMessage += $", Inner Exception: {ex.InnerException.Message}";
    //        }

    //        throw new Exception(errorMessage, ex.InnerException);
    //    }
    //    return roles;
    //}


    private async Task<bool> CheckProfileIdRights(AppDbContext dbContext, string endpoint,string profileId)
    {
        // Fetch the PROFILE_ID based on the endpoint
        try
        {
            //string sqlQuery = @"
            //SELECT *
            //FROM (
            //    SELECT *
            //    FROM CONTROLLER_RIGHTS
            //    WHERE ENDPOINTS LIKE '%' || :endpoint || '%'
            //)
            //WHERE ROWNUM = 1";

            //  string sqlQuery = @"
            //      SELECT *
            //      FROM CONTROLLER_MASTER
            //      WHERE ENDPOINTS LIKE '%' || :endpoint || '%'";

            //  //List<ControllerRightsModel> entry = await dbContext.CONTROLLER_RIGHTS.FromSqlRaw(sqlQuery, new OracleParameter(":endpoint", endpoint)).ToListAsync<ControllerRightsModel>();

            //  List<string> EndpointId = await dbContext.CONTROLLER_MASTER
            //.FromSqlRaw(sqlQuery, new OracleParameter(":endpoint", endpoint))
            //.Select(cr => cr.EndPointsId)
            //.ToListAsync();

            //var endpoints_Ids = dbContext.CONTROLLER_MASTER.Where(p => p.EndPoints.Trim()==endpoint.Trim()).ToList();
            //string? ProfileId = endpoints_Ids.Select(p => p.EndPointsId).ToString();


            string sqlQueryforEnd = @"SELECT END_POINTS_ID FROM CONTROLLER_MASTER WHERE TRIM(END_POINTS) = TRIM(:endpoint)";

            var EndPointsFromMaster = await dbContext.CONTROLLER_MASTER
                .FromSqlRaw(sqlQueryforEnd, new OracleParameter(":endpoint", endpoint))
                .Select(cr => cr.EndPointsId)
                .ToListAsync();

            string? EndpointsFrom_Mst= EndPointsFromMaster.Count > 0 ? EndPointsFromMaster[0] : null;


            string sqlQuery = @"SELECT CTR_ENDPOINT_IDS 
                                FROM USER_RIGHTS 
                                WHERE TRIM(PROFILE_ID) = TRIM(:profileId) 
                                AND APPLICATION = 'SLA'
                                AND CTR_ENDPOINT_IDS IS NOT NULL";

            //     var EndPointsFromUserRights = await dbContext.USER_RIGHTS
            //.FromSqlRaw(sqlQuery, new OracleParameter(":profileId", profileId))
            //.Select(cr => cr.CtrEndpointIds)
            //.Where(ctrEndpointIds => ctrEndpointIds != null) // Filter out null values
            //.ToListAsync();


            //     // Check if the provided profileId exists in the retrieved profileIds list
            //     bool  value= EndPointsFromUserRights.Contains(EndpointsFrom_Mst);


            var EndPointsFromUserRights = await dbContext.USER_RIGHTS
                                            .FromSqlRaw(sqlQuery, new OracleParameter(":profileId", profileId))
                                            .Select(cr => cr.CtrEndpointIds)
                                            .ToListAsync();

            // Check if the retrieved CTR_ENDPOINT_IDS contain the EndpointsFrom_Mst
            bool matchFound = EndPointsFromUserRights.Any(ctrEndpointIds => ctrEndpointIds?.Split(',').Contains(EndpointsFrom_Mst) ?? false);

            return matchFound;

        }
        catch (Exception ex)
        {
            string str = ex.GetType().ToString();
            if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
            {
                string error = ex.InnerException?.Message != null ? ex.InnerException.Message : ex.Message;
                throw new RepositoryException(error);
            }
            string errorMessage = $"Error getting erro while check user rights: {ex.Message}";

            if (ex.InnerException != null)
            {
                errorMessage += $", Inner Exception: {ex.InnerException.Message}";
            }
            throw new Exception(errorMessage, ex.InnerException);
        }
    }

    private bool IsAllowedPath(PathString path)
    {
        foreach (var allowedPath in allowedPaths)
        {
            if (path.Equals(allowedPath))
            {
                return true;
            }
        }
        return false;
    }

    //public bool HasRequiredRole(List<int> roles, int requiredRole)
    //{
    //    return roles.Contains(requiredRole);
    //}

}
