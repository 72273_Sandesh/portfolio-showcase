﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using HRMS.ModelCL.Login;
using HRMS.RepositoryCL.Interface.Login;
using Oracle.ManagedDataAccess.Client;
using HRMS.RepositoryCL.Comman;
using HRMS_Core.Data.Data;
using RMS.ModelCL.Login;
using Microsoft.EntityFrameworkCore;
using HRMS.ModelCL.Payloads;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Microsoft.AspNetCore.Http;
using System.Data.OracleClient;
using Microsoft.AspNetCore.Hosting.Server;
using System.Data;
using System.Security.Cryptography;


namespace HRMS.RepositoryCL.Concrete.Login
{
    public class UserRepository : IUserRepository
    {
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        private readonly IEncryption _encryption;
        private readonly ImageFetchingHelper _imageFetchingHelper;
        public UserRepository(IConfiguration configuration, AppDbContext context, IEncryption encryption, ImageFetchingHelper imageFetchingHelper)
        {
            _appDbContext = context;
            _configuration = configuration;
            _encryption = encryption;
            _imageFetchingHelper = imageFetchingHelper;
        }
        public async Task<Role> AddRole(Role role)
        {
            var addedRole = _appDbContext.Roles.Add(role);
            await _appDbContext.SaveChangesAsync();
            return addedRole.Entity;
        }
        public async Task<users> AddUser(users user)
        {
            var addedUser = _appDbContext.users.Add(user);
            await _appDbContext.SaveChangesAsync();
            return addedUser.Entity;
        }
        public async Task<bool> AssignRoleToUser(AddUserRole obj)
        {
            try
            {
                var addRoles = new List<UserRole>();
                var user = await _appDbContext.UserRoles.SingleOrDefaultAsync(s => s.UserId == obj.UserId);

                if (user == null)
                {
                    throw new Exception("User is not valid");
                }

                foreach (int role in obj.RoleIds)
                {
                    var userRole = new UserRole
                    {
                        RoleId = role,
                        UserId = user.Id
                    };
                    addRoles.Add(userRole);
                }

                await _appDbContext.UserRoles.AddRangeAsync(addRoles);
                await _appDbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public async Task<IActionResult> Authenticate(LoginRequest users)
        {
            UserDetailsModel userDet = await GetUsersAsync(users.empid, users.password, "Login");
            UserProfileDetails profile_det = await GetUsersProfileDet(users.empid);

            if (userDet.retVal == "true")
            {
                string accessToken = GenerateAccessToken(userDet.retVal, userDet.empFirstName, userDet.empLastName, userDet.empCode, userDet.empEmail, userDet.profileId);

                //if (string.IsNullOrEmpty(refreshToken) || refreshTokenExpiryTime == null || refreshTokenExpiryTime <= DateTime.UtcNow)
                //{
                DateTime token_UpdatedAt = DateTime.Now;
                userDet.refreshToken = GenerateRefreshToken();
                userDet.refreshTokenExpiryTime = DateTime.UtcNow.AddDays(1);
                await UpdateRefreshTokenDBAsync(userDet.empCode, accessToken, token_UpdatedAt, userDet.refreshToken, (DateTime)userDet.refreshTokenExpiryTime);
                //}

                TokenVerifyResponse response = new TokenVerifyResponse
                {
                    api_token = accessToken,
                    refreshToken = userDet.refreshToken,
                    refreshToken_ExpiryTime = userDet.refreshTokenExpiryTime,
                    first_name = userDet.empFirstName,
                    last_name = userDet.empLastName,
                    id = userDet.empCode,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now
                };

                return new ObjectResult(response)
                {
                    StatusCode = (int)HttpStatusCode.OK
                };
            }
            else
            {
                return new ObjectResult(new { Message = "Invalid credentials", Status = "Unauthorized" })
                {
                    StatusCode = (int)HttpStatusCode.Unauthorized
                };
            }
        }
        public string GenerateAccessToken(string retVal, string empFirstName, string empLastName, string empCode, string empEmail, string profileId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenKey = Encoding.UTF8.GetBytes(_configuration["JWT:Key"]);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]{
                    new Claim(ClaimTypes.Name, "MacleodsWebAPI"),
                    new Claim("userId",empCode),
                    new Claim("profileId",profileId),
                    new Claim("emailId",empEmail),
                    new Claim("_empFirstName",empFirstName),
                    new Claim("_empLastName",empLastName)}
                ),
                Expires = DateTime.UtcNow.AddMinutes(5),
                Issuer = _configuration["JWT:Issuer"],
                Audience = _configuration["JWT:Audiance"],
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        public async Task<(string, string)> RefreshTokensAsync(string apiToken, string str_refreshToken)
        {
            var principal = GetPrincipalFromExpiredToken(apiToken);
            var userId = principal.Claims.FirstOrDefault(c => c.Type == "userId")?.Value;

            UserDetailsModel objUserDet = new UserDetailsModel();
            objUserDet.retVal = "false";
            //string retVal = "false";
            //string? empFirstName = string.Empty;
            //string? empLastName = string.Empty;
            //string? empCode = string.Empty;
            //string? empEmail = string.Empty;
            //string? profileId = string.Empty;
            //string? token = string.Empty;
            //string? refreshToken = string.Empty;
            //DateTime? refreshTokenExpiryTime = null;

            objUserDet = await GetUsersAsync(userId, "", "");


            if (objUserDet.retVal == "false" || objUserDet.refreshToken != str_refreshToken || objUserDet.refreshTokenExpiryTime <= DateTime.UtcNow)
            {
                throw new UnauthorizedAccessException("Invalid refresh token");
            }
            var newAccessToken = GenerateAccessToken(objUserDet.retVal, objUserDet.empFirstName, objUserDet.empFirstName, objUserDet.empCode, objUserDet.empEmail, objUserDet.profileId);
            var newRefreshToken = objUserDet.refreshToken;//GenerateRefreshToken();
            var RefreshTokenExpiryTime = objUserDet.refreshTokenExpiryTime;//DateTime.UtcNow.AddDays(1);
            var TokenUpdatedAt = DateTime.Now;

            await UpdateRefreshTokenDBAsync(userId, newAccessToken, TokenUpdatedAt, newRefreshToken, RefreshTokenExpiryTime);

            return (newAccessToken, newRefreshToken);
        }
        public async Task<UserDetailsModel> GetUsersAsync(string userId, string pwd, string calltype)
        {

            UserDetailsModel objUserDet = new UserDetailsModel();
            objUserDet.retVal = "false";

            using (OracleConnection con = _appDbContext.GetConn())
            {
                using (OracleCommand cmd = _appDbContext.GetCommand())
                {
                    try
                    {
                        con.Open();
                        cmd.BindByName = true;
                        //string encryptPass = _encryption.Encrypt(users.password, true);

                        string Qry = "SELECT A.*, B.DEPT_CODE, B.EMAIL_ID_OFF, B.RELIEVE_DATE, C.DESCR AS DEPT, NVL(LOGIN_ATTEMPTS, 0) LOGIN_ATTEMPTS, B.EMP_FNAME AS EMP_FNAME, B.EMP_LNAME AS EMP_LNAME, A.TOKEN AS TOKEN, A.REFRESH_TOKEN AS REFRESH_TOKEN, A.REFRESH_TOKEN_EXPIRY_TIME AS REFRESH_TOKEN_EXPIRY_TIME, " +
                                     "TRUNC(TO_DATE(SYSDATE) - NVL(TO_DATE(LAST_LOGIN_DATE), TO_DATE(SYSDATE))) AS LOG_DAYS, 90 - TRUNC(TO_DATE(SYSDATE) - NVL(TO_DATE(LAST_PWD_CHG_DATE), TO_DATE(SYSDATE))) AS PWD_DAYS " +
                                     "FROM SCM_RIGHTS A " +
                                     "LEFT OUTER JOIN EMPLOYEE B ON A.EMP_CODE = B.EMP_CODE " +
                                     "LEFT OUTER JOIN DEPARTMENT C ON B.DEPT_CODE = C.DEPT_CODE " +
                                     "WHERE A.EMP_CODE = '" + userId.Trim() + "'";
                        cmd.CommandText = Qry;

                        OracleDataReader reader = cmd.ExecuteReader();
                        while (await reader.ReadAsync())
                        {
                            if (Convert.ToString(reader["USER_LOCK"]) == "1")
                            {
                                objUserDet.valMsg = Convert.ToString("This Login is Locked.");
                            }
                            if (Convert.ToString(reader["RELIEVE_DATE"]) != "")
                            {
                                objUserDet.valMsg = "This Login is Expired.";
                            }
                            if (Convert.ToInt32(reader["LOG_DAYS"]) > 90)
                            {
                                objUserDet.valMsg = $"You Have Not Logged In To The Portal For The Last {Convert.ToString(reader["LOG_DAYS"])} Days. Please Contact IT Department.";
                            }
                            if (calltype == "Login")
                            {
                                string strDecrypt = "";
                                strDecrypt = (Convert.ToString(reader["PWD_ENCRYPT"]).ToUpper() == "A") ? Convert.ToString(reader["PWD_ENCRYPT"]).ToUpper() : _encryption.Decrypt(Convert.ToString(reader["PWD_ENCRYPT"]), true);
                                if (pwd.ToUpper() == "A")
                                    pwd = "A";
                                if (Convert.ToString(pwd).Trim() == strDecrypt)
                                {
                                    objUserDet.retVal = "true";
                                    objUserDet.empFirstName = Convert.ToString(reader["EMP_FNAME"]);
                                    objUserDet.empLastName = Convert.ToString(reader["EMP_LNAME"]);
                                    objUserDet.empCode = Convert.ToString(reader["EMP_CODE"]);
                                    objUserDet.empEmail = Convert.ToString(reader["EMAIL_ID_OFF"]);
                                    objUserDet.profileId = Convert.ToString(reader["PROFILE_ID"]);
                                    objUserDet.token = Convert.ToString(reader["TOKEN"]);
                                    objUserDet.refreshToken = Convert.ToString(reader["REFRESH_TOKEN"]);
                                    objUserDet.refreshTokenExpiryTime = reader["REFRESH_TOKEN_EXPIRY_TIME"] as DateTime?;

                                }
                                else
                                {
                                    objUserDet.retVal = "false";
                                    objUserDet.valMsg = "Invalid Username or Password.";
                                }
                            }
                            else
                            {
                                objUserDet.retVal = "true";
                                objUserDet.empFirstName = Convert.ToString(reader["EMP_FNAME"]);
                                objUserDet.empLastName = Convert.ToString(reader["EMP_LNAME"]);
                                objUserDet.empCode = Convert.ToString(reader["EMP_CODE"]);
                                objUserDet.empEmail = Convert.ToString(reader["EMAIL_ID_OFF"]);
                                objUserDet.profileId = Convert.ToString(reader["PROFILE_ID"]);
                                objUserDet.token = Convert.ToString(reader["TOKEN"]);
                                objUserDet.refreshToken = Convert.ToString(reader["REFRESH_TOKEN"]);
                                objUserDet.refreshTokenExpiryTime = reader["REFRESH_TOKEN_EXPIRY_TIME"] as DateTime?;
                                byte[] bytes = await _imageFetchingHelper.fnGetImage(userId, "EMPLOYEE");
                                if (bytes != null)
                                {
                                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                                    string imageUrl = "data:image/png;base64," + base64String;
                                    objUserDet.imageUrl = imageUrl;
                                }
                            }


                        }
                        reader.Dispose();

                        return objUserDet;
                    }
                    catch (Exception ex)
                    {
                        objUserDet.retVal = "false";
                        throw new Exception("An error occurred while fetching the user details.", ex);
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public async Task UpdateRefreshTokenDBAsync(string userId, string token, DateTime tokenUpdatedAt, string refreshToken, DateTime? refreshTokenExpiryTime)
        {
            using (OracleConnection con = _appDbContext.GetConn())
            {
                using (OracleCommand cmd = con.CreateCommand())
                {
                    try
                    {
                        con.Open();
                        cmd.BindByName = true;
                        cmd.CommandText = "UPDATE SCM_RIGHTS SET TOKEN=:token,TOKEN_UPDATED_AT=TO_TIMESTAMP(:tokenUpdatedAt, 'YYYY-MM-DD HH24:MI:SS'), REFRESH_TOKEN = :refreshToken, " +
                                          "REFRESH_TOKEN_EXPIRY_TIME = TO_TIMESTAMP(:refreshTokenExpiryTime, 'YYYY-MM-DD HH24:MI:SS') " +
                                          "WHERE EMP_CODE = :userId";

                        cmd.Parameters.Add(new OracleParameter("token", token));
                        cmd.Parameters.Add(new OracleParameter("tokenUpdatedAt", tokenUpdatedAt.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.Add(new OracleParameter("refreshToken", refreshToken));
                        cmd.Parameters.Add(new OracleParameter("refreshTokenExpiryTime", refreshTokenExpiryTime?.ToString("yyyy-MM-dd HH:mm:ss")));
                        cmd.Parameters.Add(new OracleParameter("userId", userId));

                        await cmd.ExecuteNonQueryAsync();
                    }
                    catch (Exception ex)
                    {
                        // Handle the exception appropriately
                        throw new Exception("An error occurred while updating the token details.", ex);
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }
        public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Key"])),
                ClockSkew = TimeSpan.Zero
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);

            if (!(securityToken is JwtSecurityToken jwtSecurityToken) ||
                !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new SecurityTokenException("Invalid token");
            }

            return principal;
        }
        public async Task<bool> ValidateRefreshTokenAsync(string apiToken, string refreshToken)
        {
            try
            {
                var principal = GetPrincipalFromExpiredToken(apiToken);
                var userId = principal.Claims.FirstOrDefault(c => c.Type == "userId")?.Value;
                UserDetailsModel user = await GetUsersAsync(userId, "", "");

                if (user.retVal == "false" || user.refreshToken != refreshToken)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public async Task<UserProfileDetails> GetUsersProfileDet(string userId)
        {

            UserProfileDetails objUserDet = new UserProfileDetails();

            using (OracleConnection con = _appDbContext.GetConn())
            {
                using (OracleCommand cmd = _appDbContext.GetCommand())
                {
                    try
                    {
                        con.Open();
                        cmd.BindByName = true;
                        //string encryptPass = _encryption.Encrypt(users.password, true);
                        string Qry = "SELECT B.EMP_CODE, B.SHORT_NAME, D.CAND_CODE, B.SEX AS GENDER, B.BIRTH_DATE, B.MOBILE_NO, B.DESIGNATION, B.STATUS, B.DEPT_CODE, " +
                                     "NVL(SUM(TO_NUMBER(DDF_GET_EXP_TOTAL(B.EMP_CODE))), 0) AS TOTAL_EXP, B.EMAIL_ID_OFF, B.RELIEVE_DATE, C.DESCR AS DEPT, B.STATUS " +
                                     "FROM SCM_RIGHTS A LEFT OUTER JOIN EMPLOYEE B ON A.EMP_CODE = B.EMP_CODE LEFT OUTER JOIN DEPARTMENT C ON B.DEPT_CODE = C.DEPT_CODE " +
                                     "LEFT OUTER JOIN RECRSL D ON B.EMP_CODE=D.EMP_CODE WHERE B.EMP_CODE='" + userId.Trim() + "' " +
                                     "GROUP BY B.EMP_CODE, B.SHORT_NAME, D.CAND_CODE, B.SEX, B.BIRTH_DATE, B.MOBILE_NO, B.DESIGNATION, B.STATUS, B.DEPT_CODE, B.EMAIL_ID_OFF, B.RELIEVE_DATE, C.DESCR, B.STATUS";
                        cmd.CommandText = Qry;

                        OracleDataReader reader = cmd.ExecuteReader();
                        while (await reader.ReadAsync())
                        {
                            objUserDet.Emp_Code = Convert.ToString(reader["EMP_CODE"]);
                            objUserDet.Emp_Name = Convert.ToString(reader["SHORT_NAME"]);
                            objUserDet.Cand_Code = Convert.ToString(reader["CAND_CODE"]);
                            objUserDet.Gender = Convert.ToString(reader["GENDER"]);
                            objUserDet.DOB = Convert.ToString(reader["BIRTH_DATE"]);
                            objUserDet.Mobile_no = Convert.ToString(reader["MOBILE_NO"]);
                            objUserDet.Designation = Convert.ToString(reader["DESIGNATION"]);
                            objUserDet.Dept_Code = Convert.ToString(reader["DEPT_CODE"]);
                            objUserDet.Dept_Name = Convert.ToString(reader["DEPT"]);
                            objUserDet.Total_Exp = Convert.ToString(reader["TOTAL_EXP"]);
                            objUserDet.Email_Id_Off = Convert.ToString(reader["EMAIL_ID_OFF"]);
                            objUserDet.Relieve_Date = Convert.ToString(reader["RELIEVE_DATE"]);
                            objUserDet.Status = Convert.ToString(reader["STATUS"]);

                            /*byte[] bytes = await _imageFetchingHelper.fnGetImage(userId, "EMPLOYEE");
                            if (bytes != null)
                            {
                                string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                                string imageUrl = "data:image/png;base64," + base64String;
                                objUserDet.imageUrl = imageUrl;
                            }*/

                        }
                        reader.Dispose();

                        return objUserDet;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("An error occurred while fetching the user details.", ex);
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public async Task<TokenVerifyResponse> VerifyToken(Tokens token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]);
                var validationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = _configuration["Jwt:Issuer"],
                    ValidAudience = _configuration["Jwt:Audiance"],
                    IssuerSigningKey = new SymmetricSecurityKey(key)
                };

                SecurityToken validatedToken;
                var principal = tokenHandler.ValidateToken(token.api_token, validationParameters, out validatedToken);

                // Extract user data from claims
                var userId = principal?.Claims.FirstOrDefault(c => c.Type == "userId")?.Value.Trim();
                //var email = principal?.Claims.FirstOrDefault(c => c.Type == "emailId")?.Value.Trim();
                var firstName = principal?.Claims.FirstOrDefault(c => c.Type == "_empFirstName")?.Value.Trim();
                var lastName = principal?.Claims.FirstOrDefault(c => c.Type == "_empLastName")?.Value.Trim();
                UserProfileDetails profile_det = await GetUsersProfileDet(userId);
                if (profile_det == null)
                {
                    throw new Exception("User profile details not found.");
                }

                var response = new TokenVerifyResponse
                {
                    id = profile_det.Emp_Code,
                    api_token = token.api_token,
                    Emp_Code = profile_det.Emp_Code,
                    Emp_Name = profile_det.Emp_Name,
                    Cand_Code = profile_det.Cand_Code,
                    Gender = profile_det.Gender,
                    DOB = profile_det.DOB,
                    Mobile_no = profile_det.Mobile_no,
                    Dept_Code = profile_det.Dept_Code,
                    Dept_Name = profile_det.Dept_Name,
                    Designation = profile_det.Designation,
                    Total_Exp = profile_det.Total_Exp,
                    Email_Id_Off = profile_det.Email_Id_Off,
                    Relieve_Date = profile_det.Relieve_Date,
                    Status = profile_det.Status,
                    imageUrl = profile_det.imageUrl,
                    created_at = DateTime.Now,
                    email_verified_at = DateTime.Now,
                    first_name = firstName,
                    last_name = lastName,
                    updated_at = DateTime.Now,
                    refreshToken = token.RefreshToken

                };

                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}


