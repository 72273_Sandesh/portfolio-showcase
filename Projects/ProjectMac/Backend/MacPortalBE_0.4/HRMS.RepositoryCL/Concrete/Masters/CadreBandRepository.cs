﻿using HRMS.ModelCL.ADMINISTRATION.Masters.CadreBand;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.Data.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Concrete.Masters
{
    public class CadreBandRepository : ICadreBandRepository
    {
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        public CadreBandRepository(AppDbContext appContext, IConfiguration configuration)
        {
            _appDbContext = appContext;
            _configuration = configuration;
        }


        public async Task<List<CadreBandModel>> GetAllCadreBand(int page, int items_per_page)
        {
            // Calculate the starting row number based on the page number and page size
            int startRow = (page - 1) * items_per_page;
            int endRow = page * items_per_page;

            try
            {
                var cadreBand = await _appDbContext.CADRE_BAND
                    .OrderBy(d => d.BAND_CODE)
                    .ToListAsync();

                // Filter departments based on pagination
                var pagedDepartments = cadreBand.Skip(startRow).Take(items_per_page).ToList();

                return pagedDepartments;
            }
            catch (Exception ex)
            {
                string errorMessage = $"Error getting CadreBand: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }

        public int CountAllCadreBand()
        {
            try
            {
                int count = _appDbContext.CADRE_BAND.Count();

                return count;
            }
            catch (Exception ex)
            {
                string errorMessage = $"Error getting CadreBand: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }

        }
    }
}
