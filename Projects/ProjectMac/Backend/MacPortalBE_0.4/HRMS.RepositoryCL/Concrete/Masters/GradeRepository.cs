﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Cadre;
using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.ADMINISTRATION.Masters.Grade;
using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Cadre;
using HRMS.ModelCL.Payloads.Department;
using HRMS.ModelCL.Payloads.Grade;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.Data.Data;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace HRMS.RepositoryCL.Concrete.Masters
{
    public class GradeRepository : IGradeRepository
    {
        private readonly IAutoGenerateCodeRepository _autoGenerateCodeRepository;
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        private readonly ILogger<GradeRepository> _logger;
        public GradeRepository(AppDbContext appContext, IConfiguration configuration, IAutoGenerateCodeRepository autoGenerateCodeRepository, ILogger<GradeRepository> logger)
        {
            _appDbContext = appContext;
            _configuration = configuration;
            _autoGenerateCodeRepository = autoGenerateCodeRepository;
            _logger = logger;
        }


        /* public async Task<(List<GradeGetGradeByGradeIdResponse>, int)> GetAllGrades(int page, int items_per_page, string search)
         {
             // Calculate the starting row number based on the page number and page size
             int startRow = (page - 1) * items_per_page;

             // Fetch all cadres asynchronously
             var ddl_Names = await GetAll_Cadres();
             var cadreList = ddl_Names.ToList();

             try
             {
                 // Base query for grades
                 var gradesQuery = _appDbContext.GRADE
                     .Where(g => g.GradeType.Trim() == "C" || g.GradeType.Trim() == "H")
                     .OrderBy(d => d.id)
                     .Select(grade => new
                     {
                         grade.id,
                         grade.Description,
                         grade.ShortDescription,
                         grade.CadreCode,                    
                         grade.LveTableNumber,
                         grade.InductionTableNumber,
                         grade.NoticePeriod,
                         grade.ChangeDate,
                         grade.ChangeUser,
                         grade.ChangeTerm,
                         grade.ApprovalTableNumber,
                         grade.AccountCodeExp,
                         grade.CostCenterCodeExp,
                         grade.GradeType,
                         grade.UdfNumber1,
                         grade.GradeOrder,
                         grade.GradeId,
                         grade.Status,
                         grade.ChangeReason
                     });

                 // Apply search filter if search string is provided (case-insensitive)
                 if (!string.IsNullOrEmpty(search))
                 {
                     string searchUpper = search.ToUpper();
                     gradesQuery = gradesQuery.Where(d =>
                         (d.Description != null && d.Description.ToUpper().Contains(searchUpper)) ||
                         (d.ShortDescription != null && d.ShortDescription.ToUpper().Contains(searchUpper)) ||
                         (d.ChangeUser != null && d.ChangeUser.ToUpper().Contains(searchUpper)) ||
                         (d.ChangeDate != null && d.ChangeDate.ToString().Contains(searchUpper)) ||
                         (d.CadreCode != null && d.CadreCode.ToUpper().Contains(searchUpper)) ||
                         (d.Status != null &&
                             ((d.Status.ToUpper() == "A" && searchUpper == "ACTIVE") ||
                              (d.Status.ToUpper() == "N" && searchUpper == "INACTIVE")))
                     );
                 }

                 // Materialize the query result to a list
                 var intermediateResult = await gradesQuery.ToListAsync();

                 // Map to GradeModel and add CadreName from cadreList
                 var resultList = intermediateResult.Select(grade => new GradeGetGradeByGradeIdResponse
                 {
                     id = grade.id,
                     Grade_code = grade.id,
                     Description = grade.Description,
                     ShortDescription = grade.ShortDescription,
                     CadreName = cadreList.FirstOrDefault(p => p.CadreCode.Trim() == grade.CadreCode.Trim())?.Cadre ?? "",
                     CadreCode = grade.CadreCode,
                     LveTableNumber = grade.LveTableNumber,
                     InductionTableNumber = grade.InductionTableNumber,
                     NoticePeriod = grade.NoticePeriod,
                     ChangeDate = grade.ChangeDate,
                     ChangeUser = grade.ChangeUser,
                     ChangeTerm = grade.ChangeTerm,
                     ApprovalTableNumber = grade.ApprovalTableNumber,
                     AccountCodeExp = grade.AccountCodeExp,
                     CostCenterCodeExp = grade.CostCenterCodeExp,
                     GradeType = grade.GradeType,
                     UdfNumber1 = grade.UdfNumber1,
                     GradeOrder = grade.GradeOrder,
                     GradeId = grade.GradeId,
                     Status = grade.Status,
                     ChangeReason = grade.ChangeReason
                 }).ToList();

                 // Get the total count of grades
                 int totalCount = resultList.Count;

                 // Apply pagination
                 var pagedGrades = resultList.Skip(startRow).Take(items_per_page).ToList();

                 return (pagedGrades, totalCount);
             }
             catch (Exception ex)
             {
                 string errorMessage = $"Error getting grade: {ex.Message}";
                 if (ex.InnerException != null)
                 {
                     errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                 }

                 if (ex is DbUpdateException || ex is OracleException)
                 {
                     string error = ex.InnerException?.Message ?? ex.Message;
                     throw new RepositoryException(error);
                 }

                 throw new Exception(errorMessage, ex.InnerException);
             }
         }
 */

        public async Task<(List<GradeGetGradeByGradeIdResponse>, int)> GetAllGrades(int page, int items_per_page, string search)
        {
            // Calculate the starting row number based on the page number and page size
            int startRow = (page - 1) * items_per_page;
            int endRow = page * items_per_page;

            try
            {
                // Base query for grades
                var result = from grade in _appDbContext.GRADE
                             join cadre in _appDbContext.CADRE
                             on grade.CadreCode equals cadre.CADRE_CODE into joinedCadre
                             from cadreEntity in joinedCadre.DefaultIfEmpty()
                             select new GradeGetGradeByGradeIdResponse
                             {
                                 id = grade.id,
                                 Grade_code = grade.id,
                                 Description = grade.Description,
                                 ShortDescription = grade.ShortDescription,
                                 CadreName = cadreEntity != null ? cadreEntity.DESCR : null,
                                 CadreCode = grade.CadreCode,
                                 LveTableNumber = grade.LveTableNumber,
                                 InductionTableNumber = grade.InductionTableNumber,
                                 NoticePeriod = grade.NoticePeriod,
                                 ChangeDate = grade.ChangeDate,
                                 ChangeUser = grade.ChangeUser,
                                 ChangeTerm = grade.ChangeTerm,
                                 ApprovalTableNumber = grade.ApprovalTableNumber,
                                 AccountCodeExp = grade.AccountCodeExp,
                                 CostCenterCodeExp = grade.CostCenterCodeExp,
                                 GradeType = grade.GradeType,
                                 UdfNumber1 = grade.UdfNumber1,
                                 GradeOrder = grade.GradeOrder,
                                 GradeId = grade.GradeId,
                                 Status = grade.Status,
                                 ChangeReason = grade.ChangeReason
                             };

                // Apply search filter if search string is provided (case-insensitive for Description, ShortDescription, CadreCode)
                if (!string.IsNullOrEmpty(search))
                {
                    string searchUpper = search.ToUpper();

                    // Try to parse different date formats (full date, month/year, or year)
                    DateTime parsedDate;
                    bool isFullDate = DateTime.TryParseExact(search, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out parsedDate);
                    bool isMonthYear = !isFullDate && DateTime.TryParseExact(search, "MM/yyyy", null, System.Globalization.DateTimeStyles.None, out parsedDate);
                    bool isYear = !isFullDate && !isMonthYear && DateTime.TryParseExact(search, "yyyy", null, System.Globalization.DateTimeStyles.None, out parsedDate);

                    result = result.Where(g =>
                        g.Description.ToUpper().Contains(searchUpper) ||
                        g.ShortDescription.ToUpper().Contains(searchUpper) ||
                        g.CadreCode.ToUpper().Contains(searchUpper) ||
                        g.CadreName.ToUpper().Contains(searchUpper) ||
                        g.ChangeUser.ToUpper().Contains(searchUpper) ||
                        (isFullDate && g.ChangeDate.HasValue && g.ChangeDate.Value.Date == parsedDate.Date) || // Full date comparison
                        (isMonthYear && g.ChangeDate.HasValue && g.ChangeDate.Value.Month == parsedDate.Month && g.ChangeDate.Value.Year == parsedDate.Year) || // Month/Year comparison
                        (isYear && g.ChangeDate.HasValue && g.ChangeDate.Value.Year == parsedDate.Year) || // Year comparison
                        (g.Status.ToUpper() == "A" && searchUpper == "ACTIVE") || // Map "ACTIVE" to "A"
                        (g.Status.ToUpper() == "N" && searchUpper == "INACTIVE")  // Map "INACTIVE" to "N"
                    );
                }

                // Materialize the query result to a list
                var resultList = await result.ToListAsync();

                // Filter the paged grades based on pagination
                var pagedGrades = resultList.Skip(startRow).Take(items_per_page).ToList();

                // Get the total count of grades
                int totalGradeCount = resultList.Count;

                return (pagedGrades, totalGradeCount);
            }
            catch (Exception ex)
            {
                // Handle exceptions
                string errorMessage = $"Error getting Grades: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }
                throw new Exception(errorMessage, ex.InnerException);
            }
        }



        public int CountAllGrades()
        {
            try
            {
                int count = _appDbContext.GRADE.Count();

                return count;
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting grade: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }

        }


        /*public async Task<List<GradeGetGradeByGradeIdResponse>> FetchGradeByIdAsync(string gradeId)
        {
            try
            {
                var grades = await _appDbContext.GRADE
                    .Where(c => c.id.Trim() == gradeId.Trim())
                    .ToListAsync();

                // Fetch all cadres asynchronously
                var ddl_Names = await GetAll_Cadres();

                // Map Grade entities to GradeResponse objects
                var gradeResponses = grades.Select(grade => new GradeGetGradeByGradeIdResponse
                {
                    id = grade.id,
                    Description = grade.Description,
                    ShortDescription = grade.ShortDescription,
                    CadreName = ddl_Names.Find(p => p.CadreCode.Trim() == grade.CadreCode?.Trim())?.Cadre,                  
                    CadreCode = grade.CadreCode,
                    LveTableNumber = grade.LveTableNumber,
                    InductionTableNumber = grade.InductionTableNumber,
                    NoticePeriod = grade.NoticePeriod,
                    ChangeDate = grade.ChangeDate,
                    ChangeUser = grade.ChangeUser,
                    ChangeTerm = grade.ChangeTerm,
                    ApprovalTableNumber = grade.ApprovalTableNumber,
                    AccountCodeExp = grade.AccountCodeExp,
                    CostCenterCodeExp = grade.CostCenterCodeExp,
                    GradeType = grade.GradeType,
                    UdfNumber1 = grade.UdfNumber1,
                    GradeOrder = grade.GradeOrder,
                    GradeId = grade.GradeId,
                    Status = grade.Status,
                    ChangeReason = grade.ChangeReason
                }).ToList();

                return gradeResponses;
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting grade: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }*/


        public async Task<List<GradeGetGradeByGradeIdResponse>> FetchGradeByIdAsync(string gradeId)
        {

            // Fetch all cadres asynchronously
            var ddl_Names = await GetAll_Cadres();
            var cadreList = ddl_Names.ToList();

            try
            {
                var grades = _appDbContext.GRADE
                    .Where(c => c.id.Trim() == gradeId.Trim())
                    .Select(grade => new
                    {
                        grade.id,
                        grade.Description,
                        grade.ShortDescription,
                        grade.CadreCode,
                        // grade.CadreName,
                        grade.LveTableNumber,
                        grade.InductionTableNumber,
                        grade.NoticePeriod,
                        grade.ChangeDate,
                        grade.ChangeUser,
                        grade.ChangeTerm,
                        grade.ApprovalTableNumber,
                        grade.AccountCodeExp,
                        grade.CostCenterCodeExp,
                        grade.GradeType,
                        grade.UdfNumber1,
                        grade.GradeOrder,
                        grade.GradeId,
                        grade.Status,
                        grade.ChangeReason
                    });

                // Materialize the query result to a list
                var intermediateResult = await grades.ToListAsync();

                // Map Grade entities to GradeResponse objects
                var gradeResponses = intermediateResult.Select(grade => new GradeGetGradeByGradeIdResponse
                {
                    id = grade.id,
                    Grade_code = grade.id,
                    Description = grade.Description,
                    ShortDescription = grade.ShortDescription,
                    CadreName = cadreList.FirstOrDefault(p => p.CadreCode.Trim() == grade.CadreCode.Trim())?.Cadre ?? "",
                    CadreCode = grade.CadreCode,
                    LveTableNumber = grade.LveTableNumber,
                    InductionTableNumber = grade.InductionTableNumber,
                    NoticePeriod = grade.NoticePeriod,
                    ChangeDate = grade.ChangeDate,
                    ChangeUser = grade.ChangeUser,
                    ChangeTerm = grade.ChangeTerm,
                    ApprovalTableNumber = grade.ApprovalTableNumber,
                    AccountCodeExp = grade.AccountCodeExp,
                    CostCenterCodeExp = grade.CostCenterCodeExp,
                    GradeType = grade.GradeType,
                    UdfNumber1 = grade.UdfNumber1,
                    GradeOrder = grade.GradeOrder,
                    GradeId = grade.GradeId,
                    Status = grade.Status,
                    ChangeReason = grade.ChangeReason
                }).ToList();

                return gradeResponses;
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting grade: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }

        public async Task<bool> CreateGrade(GradeModel grade)
        {
            try
            {
                bool codeExists = _appDbContext.GRADE.Any(g => (g.Description).Trim().ToUpper() == (grade.Description).Trim().ToUpper());
                if (codeExists)
                {
                    return false;
                }
                else
                {
                    Enums.TableName tableNameValue = Enums.TableName.GRADE;
                    Enums.TranWindowCol tranWindowColValue = Enums.TranWindowCol.SLA__GRADELIST;

                    string tableNameString = tableNameValue.ToString();
                    string tranWindowColString = tranWindowColValue.ToString();

                    string gradeCode = _autoGenerateCodeRepository.GenerateCode(tableNameString, tranWindowColString);
                    grade.id = gradeCode;

                    if (grade.Status == "ACTIVE")
                    {
                        grade.Status = "A";
                    }
                    if (grade.Status == "INACTIVE")
                    {
                        grade.Status = "N";
                    }
                    // Add the new cadre to the context
                    _appDbContext.GRADE.Add(grade);

                    await _appDbContext.SaveChangesAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException) || ex.GetType() == typeof(DbUpdateConcurrencyException))
                {

                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message!=null? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting grade: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }
        public async Task<UpdateFlagResponse> UpdateGrade(GradeModel grade)
        {
            try
            {
                UpdateFlagResponse enum_flags = new UpdateFlagResponse();
                bool codeExists = _appDbContext.GRADE.Any(g => (g.id).Trim() == (grade.id).Trim());
                if (codeExists)
                {
                    enum_flags.codeExists = true;
                    bool descrExists = _appDbContext.GRADE.Any(g => (g.Description.Trim().ToUpper() == grade.Description.Trim().ToUpper()) && (g.id.Trim().ToUpper() != grade.id.Trim().ToUpper()));
                    if (descrExists)
                    {
                        enum_flags.descrExists = true;
                    }
                    else
                    {
                        if (grade.Status == "ACTIVE")
                        {
                            grade.Status = "A";
                        }
                        if (grade.Status == "INACTIVE")
                        {
                            grade.Status = "N";
                        }
                        _appDbContext.Entry(grade).State = EntityState.Modified;
                        _appDbContext.SaveChanges();
                        enum_flags.Success = true;
                    }
                }
                else
                {
                    enum_flags.codeExists = false;
                }
                return enum_flags;

            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException) || ex.GetType()== typeof(DbUpdateConcurrencyException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting grade: {ex.Message}";
                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }


        }
       
        public async Task<List<CadreResp>> GetAll_Cadres()
        {
            try
            {
                var query = from cadre in _appDbContext.CADRE
                            where cadre.CADRE_ID != null
                            orderby cadre.DESCR
                            select new CadreResp
                            {
                                CadreCode = cadre.CADRE_CODE,
                                Cadre = cadre.DESCR.ToUpper(),
                                ShortDescription = cadre.SH_DESCR.ToUpper(),
                                BandCode = cadre.BAND_CODE
                            };

                var result = await query.ToListAsync();
                return result;
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting grade: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }


    }
}
