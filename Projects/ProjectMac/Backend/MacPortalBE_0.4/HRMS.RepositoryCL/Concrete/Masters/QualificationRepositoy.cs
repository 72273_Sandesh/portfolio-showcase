﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Qualifcation;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.Data.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System.Diagnostics;


namespace HRMS.RepositoryCL.Concrete.Masters
{
    public class QualificationRepositoy : IQualificationRepositoy
    {
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        private readonly IAutoGenerateCodeRepository _autoGenerateCodeRepository;
        public QualificationRepositoy(AppDbContext appContext, IConfiguration configuration, IAutoGenerateCodeRepository autoGenerateCodeRepository)
        {
            _appDbContext = appContext;
            _configuration = configuration;
            _autoGenerateCodeRepository = autoGenerateCodeRepository;
        }

        /* public async Task<(List<GetQualificationByCodeResponse>, int)> GetAllQualifications(int page, int items_per_page, string search)
         {
             List<GetQualificationByCodeResponse> qualifications = new List<GetQualificationByCodeResponse>();
             int totalCount = 0;

             try
             {
                 int startRow = (page - 1) * items_per_page + 1;
                 int endRow = page * items_per_page;

                 string countQuery = @"
             SELECT 
                 COUNT(*) 
             FROM 
                 Qualification
             WHERE 
                 (:search IS NULL 
                     OR UPPER(DESCR) LIKE '%' || :searchUpper || '%'
                     OR UPPER(SH_DESCR) LIKE '%' || :searchUpper || '%'
                     OR UPPER(QLF_CODE) LIKE '%' || :searchUpper || '%'
                     OR UPPER(CHG_USER) LIKE '%' || :searchUpper || '%' 
                     OR UPPER(CHG_DATE) LIKE '%' || :searchUpper || '%'    
                     OR UPPER(TYPE) LIKE '%' || :searchUpper || '%'
                     OR UPPER(STATUS) LIKE '%' || :searchUpper || '%'
                     OR (:searchUpper = 'ACTIVE' AND UPPER(STATUS) = 'A')
                     OR (:searchUpper = 'INACTIVE' AND UPPER(STATUS) = 'N')
                 )";

                 using (OracleConnection connection = _appDbContext.GetConn())
                 using (OracleCommand command = _appDbContext.GetCommand())
                 {
                     await connection.OpenAsync();
                     command.BindByName = true;
                     command.CommandText = countQuery;
                     command.Parameters.Add(new OracleParameter("search", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search));
                     command.Parameters.Add(new OracleParameter("searchUpper", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search.ToUpper()));

                     // Execute the count query to get the total number of qualifications
                     totalCount = Convert.ToInt32(await command.ExecuteScalarAsync());

                     string query = @"
                 SELECT * FROM (
                     SELECT 
                         QLF_CODE,
                         DESCR AS QUALIFICATION,
                         SH_DESCR,
                         EQIVAL,
                         TYPE AS QUAL_TYPE,
                         CHG_DATE,
                         CHG_USER,
                         CHG_TERM,
                         STATUS,
                         CHG_REASON,
                         ROW_NUMBER() OVER (ORDER BY QLF_CODE) AS rn
                     FROM 
                         Qualification
                     WHERE 
                         (:search IS NULL 
                             OR UPPER(DESCR) LIKE '%' || :searchUpper || '%'
                             OR UPPER(SH_DESCR) LIKE '%' || :searchUpper || '%'
                             OR UPPER(QLF_CODE) LIKE '%' || :searchUpper || '%'
                             OR UPPER(CHG_USER) LIKE '%' || :searchUpper || '%' 
                             OR UPPER(CHG_DATE) LIKE '%' || :searchUpper || '%'    
                             OR UPPER(TYPE) LIKE '%' || :searchUpper || '%'
                             OR UPPER(STATUS) LIKE '%' || :searchUpper || '%'
                             OR (:searchUpper = 'ACTIVE' AND UPPER(STATUS) = 'A')
                             OR (:searchUpper = 'INACTIVE' AND UPPER(STATUS) = 'N')
                         )
                 )
                 WHERE rn BETWEEN :startRow AND :endRow";

                     command.CommandText = query;
                     command.Parameters.Clear(); // Clear previous parameters
                     command.Parameters.Add(new OracleParameter("startRow", startRow));
                     command.Parameters.Add(new OracleParameter("endRow", endRow));
                     command.Parameters.Add(new OracleParameter("search", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search));
                     command.Parameters.Add(new OracleParameter("searchUpper", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search.ToUpper()));

                     using (OracleDataReader reader = (OracleDataReader)await command.ExecuteReaderAsync())
                     {
                         while (await reader.ReadAsync())
                         {
                             GetQualificationByCodeResponse qualification = new GetQualificationByCodeResponse
                             {
                                 Id = reader["QLF_CODE"].ToString(),
                                 Qual_code = reader["QLF_CODE"].ToString(),
                                 name = reader["QUALIFICATION"].ToString(),
                                 Shortdescr = reader["SH_DESCR"].ToString(),
                                 Eqival = reader["EQIVAL"].ToString(),
                                 Type = reader["QUAL_TYPE"].ToString(),
                                 ChangeDate = reader["CHG_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["CHG_DATE"]),
                                 ChangeUser = reader["CHG_USER"].ToString(),
                                 ChangeTerm = reader["CHG_TERM"].ToString(),
                                 Status = reader["STATUS"].ToString(),
                                 ChangeReason = reader["CHG_REASON"].ToString()
                             };

                             qualifications.Add(qualification);
                         }
                     }
                 }

                 return (qualifications, totalCount);
             }
             catch (Exception ex)
             {
                 string str = ex.GetType().ToString();
                 if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                 {
                     //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                     //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                     string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                     throw new RepositoryException(error);
                 }
                 string errorMessage = $"Error getting qualifications: {ex.Message}";

                 if (ex.InnerException != null)
                 {
                     errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                 }

                 throw new Exception(errorMessage, ex.InnerException);
             }
         }*/

        public async Task<(List<GetQualificationByCodeResponse>, int)> GetAllQualifications(int page, int items_per_page, string search)
        {
            try
            {
                // Calculate the starting and ending row numbers based on the page number and page size
                int startRow = (page - 1) * items_per_page + 1;
                int endRow = page * items_per_page;

                List<GetQualificationByCodeResponse> qualifications = new List<GetQualificationByCodeResponse>();
                int totalCount = 0;

                string countQuery = @"
        SELECT 
            COUNT(*) 
        FROM 
            Qualification A
        WHERE 
            (:search IS NULL 
                OR UPPER(A.DESCR) LIKE '%' || :searchUpper || '%'
                OR UPPER(A.SH_DESCR) LIKE '%' || :searchUpper || '%'
                OR UPPER(A.QLF_CODE) LIKE '%' || :searchUpper || '%'
                OR UPPER(A.CHG_USER) LIKE '%' || :searchUpper || '%'
                OR (:isFullDate = 1 AND TO_CHAR(A.CHG_DATE, 'DD/MM/YYYY') = :fullDate)
                OR (:isMonthYear = 1 AND TO_CHAR(A.CHG_DATE, 'MM/YYYY') = :monthYear)
                OR (:isYear = 1 AND TO_CHAR(A.CHG_DATE, 'YYYY') = :year)
                OR UPPER(A.STATUS) LIKE '%' || :searchUpper || '%' 
                OR (:searchUpper = 'ACTIVE' AND UPPER(A.STATUS) = 'A')
                OR (:searchUpper = 'INACTIVE' AND UPPER(A.STATUS) = 'N')
            )";

                using (OracleConnection connection = _appDbContext.GetConn())
                using (OracleCommand command = _appDbContext.GetCommand())
                {
                    try
                    {
                        await connection.OpenAsync();
                        command.BindByName = true;

                        // Parse the search string for date formats
                        DateTime parsedDate;
                        bool isFullDate = DateTime.TryParseExact(search, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out parsedDate);
                        bool isMonthYear = !isFullDate && DateTime.TryParseExact(search, "MM/yyyy", null, System.Globalization.DateTimeStyles.None, out parsedDate);
                        bool isYear = !isFullDate && !isMonthYear && DateTime.TryParseExact(search, "yyyy", null, System.Globalization.DateTimeStyles.None, out parsedDate);

                        // Set parameters for the count query
                        command.CommandText = countQuery;
                        command.Parameters.Add(new OracleParameter("search", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search));
                        command.Parameters.Add(new OracleParameter("searchUpper", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search.ToUpper()));
                        command.Parameters.Add(new OracleParameter("isFullDate", isFullDate ? 1 : 0));
                        command.Parameters.Add(new OracleParameter("fullDate", isFullDate ? parsedDate.ToString("dd/MM/yyyy") : (object)DBNull.Value));
                        command.Parameters.Add(new OracleParameter("isMonthYear", isMonthYear ? 1 : 0));
                        command.Parameters.Add(new OracleParameter("monthYear", isMonthYear ? parsedDate.ToString("MM/yyyy") : (object)DBNull.Value));
                        command.Parameters.Add(new OracleParameter("isYear", isYear ? 1 : 0));
                        command.Parameters.Add(new OracleParameter("year", isYear ? parsedDate.ToString("yyyy") : (object)DBNull.Value));

                        // Execute the count query to get the total number of qualifications
                        totalCount = Convert.ToInt32(await command.ExecuteScalarAsync());

                        string query = @"
                SELECT *
                FROM (
                    SELECT 
                        A.QLF_CODE, 
                        A.DESCR AS QUALIFICATION, 
                        A.SH_DESCR, 
                        A.EQIVAL, 
                        A.TYPE AS QUAL_TYPE, 
                        A.CHG_DATE, 
                        A.CHG_USER, 
                        A.CHG_TERM, 
                        A.STATUS, 
                        A.CHG_REASON, 
                        ROW_NUMBER() OVER (ORDER BY A.QLF_CODE) AS rn
                    FROM 
                        Qualification A
                    WHERE 
                        (:search IS NULL 
                            OR UPPER(A.DESCR) LIKE '%' || :searchUpper || '%'
                            OR UPPER(A.SH_DESCR) LIKE '%' || :searchUpper || '%'
                            OR UPPER(A.QLF_CODE) LIKE '%' || :searchUpper || '%'
                            OR UPPER(A.CHG_USER) LIKE '%' || :searchUpper || '%'
                            OR (:isFullDate = 1 AND TO_CHAR(A.CHG_DATE, 'DD/MM/YYYY') = :fullDate)
                            OR (:isMonthYear = 1 AND TO_CHAR(A.CHG_DATE, 'MM/YYYY') = :monthYear)
                            OR (:isYear = 1 AND TO_CHAR(A.CHG_DATE, 'YYYY') = :year)
                            OR UPPER(A.STATUS) LIKE '%' || :searchUpper || '%'
                            OR (:searchUpper = 'ACTIVE' AND UPPER(A.STATUS) = 'A')
                            OR (:searchUpper = 'INACTIVE' AND UPPER(A.STATUS) = 'N')
                        )
                )
                WHERE rn BETWEEN :startRow AND :endRow";

                        // Set parameters for the query
                        command.CommandText = query;
                        command.Parameters.Clear(); // Clear previous parameters
                        command.Parameters.Add(new OracleParameter("startRow", startRow));
                        command.Parameters.Add(new OracleParameter("endRow", endRow));
                        command.Parameters.Add(new OracleParameter("search", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search));
                        command.Parameters.Add(new OracleParameter("searchUpper", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search.ToUpper()));
                        command.Parameters.Add(new OracleParameter("isFullDate", isFullDate ? 1 : 0));
                        command.Parameters.Add(new OracleParameter("fullDate", isFullDate ? parsedDate.ToString("dd/MM/yyyy") : (object)DBNull.Value));
                        command.Parameters.Add(new OracleParameter("isMonthYear", isMonthYear ? 1 : 0));
                        command.Parameters.Add(new OracleParameter("monthYear", isMonthYear ? parsedDate.ToString("MM/yyyy") : (object)DBNull.Value));
                        command.Parameters.Add(new OracleParameter("isYear", isYear ? 1 : 0));
                        command.Parameters.Add(new OracleParameter("year", isYear ? parsedDate.ToString("yyyy") : (object)DBNull.Value));

                        // Execute the query and fetch the data
                        using (OracleDataReader reader = (OracleDataReader)await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                GetQualificationByCodeResponse qualification = new GetQualificationByCodeResponse
                                {
                                    Id = reader["QLF_CODE"].ToString(),
                                    Qual_code = reader["QLF_CODE"].ToString(),
                                    name = reader["QUALIFICATION"].ToString(),
                                    Shortdescr = reader["SH_DESCR"].ToString(),
                                    Eqival = reader["EQIVAL"].ToString(),
                                    Type = reader["QUAL_TYPE"].ToString(),
                                    ChangeDate = reader["CHG_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["CHG_DATE"]),
                                    ChangeUser = reader["CHG_USER"].ToString(),
                                    ChangeTerm = reader["CHG_TERM"].ToString(),
                                    Status = reader["STATUS"].ToString(),
                                    ChangeReason = reader["CHG_REASON"].ToString()
                                };

                                qualifications.Add(qualification);
                            }
                        }

                        return (qualifications, totalCount);
                    }
                    catch (Exception ex)
                    {
                        // Handle the exception appropriately
                        throw new Exception("Error retrieving qualifications", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }
                // Handle general exception
                throw new Exception("General error in GetAllQualifications", ex);
            }
        }


        public async Task<bool> CreateQualification(Qualification qualification)
        {
            try
            {
                bool codeExists = _appDbContext.QUALIFICATION.Any(g => (g.name).Trim().ToUpper() == (qualification.name).Trim().ToUpper());
                if (codeExists)
                {
                    return false;
                }
                else
                {
                    Enums.TableName tableNameValue = Enums.TableName.QUALIFICATION;
                    Enums.TranWindowCol tranWindowColValue = Enums.TranWindowCol.SLA__QUALIFICATIONLIST;

                    string tableNameString = tableNameValue.ToString();
                    string tranWindowColString = tranWindowColValue.ToString();

                    string Code = _autoGenerateCodeRepository.GenerateCode(tableNameString, tranWindowColString);

                    if (qualification.Status == "ACTIVE")
                    {
                        qualification.Status = "A";
                    }
                    if (qualification.Status == "INACTIVE")
                    {
                        qualification.Status = "N";
                    }
                    qualification.Id = Code;
                    qualification.ChangeDate = DateTime.Now;
                    _appDbContext.QUALIFICATION.Add(qualification);
                    await _appDbContext.SaveChangesAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {

                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting qualification: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);

            }
        }

        public async Task<Qualification> GetQualificationById(long id)
        {
            try
            {
                return await _appDbContext.QUALIFICATION.FindAsync(id);
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error getting qualification: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }


        public async Task<IEnumerable<GetQualificationByCodeResponse>> GetQualificationByCode(string code)
        {
            try
            {

                var qualifications = await _appDbContext.QUALIFICATION
                                    .Where(d => d.Id.Trim() == code.Trim()).ToListAsync();

                var salStruc = ddl_GetDistictSalaryStruc();
                var foundSalStruc = salStruc.Result.Find(p => p.TRAN_ID.Trim() == code.Trim());

                var result = qualifications.Select(q => new GetQualificationByCodeResponse
                {
                    Id = q.Id,
                    Qual_code = q.Id,
                    name = q.name,
                    Shortdescr = q.Shortdescr,
                    Eqival = q.Eqival,
                    Type = q.Type,
                    ChangeDate = q.ChangeDate,
                    ChangeUser = q.ChangeUser,
                    ChangeTerm = q.ChangeTerm,
                    ChangeReason = q.ChangeReason,
                    Status = q.Status,
                    QualSalStrucDesc = salStruc.Result.Find(p => p.TRAN_ID == q.Eqival)?.DESCR
                }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException) || ex.GetType() == typeof(DbUpdateConcurrencyException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                // Log the exception or handle it as needed
                throw new Exception($"Error getting qualification with Id {code}", ex);
            }
        }

        
        public int CountAllQualifications()
        {
            int count = 0;
            using (OracleConnection con = _appDbContext.GetConn())
            {
                try
                {

                    con.Open();
                    string query = "SELECT COUNT(*) FROM Qualification"; // Adjust table name as per your database schema
                    using (OracleCommand command = new OracleCommand(query, con))
                    {
                        // ExecuteScalar returns object, so we need to cast to decimal first
                        var result = command.ExecuteScalar();

                        // Then we can safely convert to int
                        count = Convert.ToInt32(result); ;
                    }
                    con.Close();

                }
                catch (Exception ex)
                {
                    con.Close();

                    string str = ex.GetType().ToString();
                    if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                    {
                        //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                        //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                        string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                        throw new RepositoryException(error);
                    }

                    string errorMessage = $"Error getting count of qualification: {ex.Message}";

                    if (ex.InnerException != null)
                    {
                        errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                    }

                    throw new Exception(errorMessage, ex.InnerException);
                }
                finally
                {
                    con.Close();
                }
                return count;
            }
        }


        public async Task<UpdateFlagResponse> UpdateQualification(Qualification qualification)
        {
            try
            {
                UpdateFlagResponse enum_flags = new UpdateFlagResponse();
                bool codeExists = _appDbContext.QUALIFICATION.Any(g => (g.Id).Trim() == (qualification.Id).Trim());
                if (codeExists)
                {
                    enum_flags.codeExists = true;
                    bool descrExists = _appDbContext.QUALIFICATION.Any(g => (g.name.Trim().ToUpper() == qualification.name.Trim().ToUpper()) &&  (g.Id.Trim().ToUpper() != qualification.Id.Trim().ToUpper()));
                    if (descrExists)
                    {
                        enum_flags.descrExists = true;

                    }
                    else
                    {
                        if (qualification.Status == "ACTIVE")
                        {
                            qualification.Status = "A";
                        }
                        if (qualification.Status == "INACTIVE")
                        {
                            qualification.Status = "N";
                        }
                        _appDbContext.Entry(qualification).State = EntityState.Modified;
                        await _appDbContext.SaveChangesAsync();
                        enum_flags.Success = true;
                    }
                }
                else
                {
                    enum_flags.codeExists = false;
                }

                return enum_flags;
            }
            catch (Exception ex)
            {
                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while update qualification: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        public async Task<List<QualwiseStruc>> ddl_GetDistictSalaryStruc()
        {
            try
            {
                return await _appDbContext.QUALWISESTRUC
                .FromSqlRaw("SELECT DISTINCT TRAN_ID, DESCR FROM QUALWISE_STRUC ORDER BY DESCR")
                .ToListAsync(); 
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getring QualwiseStruc: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
           
        }


    }
}
