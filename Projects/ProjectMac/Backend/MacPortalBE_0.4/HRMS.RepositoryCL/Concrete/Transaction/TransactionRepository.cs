﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.Payloads.Employee;
using HRMS.ModelCL.Payloads.Transaction.Affecting;
using HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Interface.Transaction;
using HRMS_Core.Data.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Drawing.Printing;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using static HRMS.RepositoryCL.Comman.Enums;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using static System.Collections.Specialized.BitVector32;
using static System.Net.Mime.MediaTypeNames;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace HRMS.RepositoryCL.Concrete.Transaction
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDbContext;
        private readonly IAutoGenerateCodeRepository _autoGenerateCodeRepository;
        private readonly ImageFetchingHelper _imageFetchingHelper;
        private readonly IDbConnection _dbConnection;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public TransactionRepository(AppDbContext appContext, IConfiguration configuration, IAutoGenerateCodeRepository autoGenerateCodeRepository, ImageFetchingHelper imageFetchingHelper, IDbConnection dbConnection, IHttpContextAccessor httpContextAccessor)
        {
            _appDbContext = appContext;
            _configuration = configuration;
            _autoGenerateCodeRepository = autoGenerateCodeRepository;
            _imageFetchingHelper = imageFetchingHelper;
            _dbConnection = dbConnection;
            _httpContextAccessor = httpContextAccessor;
        }
        // Dropdowns
        public async Task<List<GetTransactionResponse>> ddl_GetTransaction()
        {
            var transactions = new List<GetTransactionResponse>();

            try
            {

                var sqlQuery = @"SELECT 
                                    TRIM(FLD_NAME) AS FLD_NAME,
                                    MOD_NAME,
                                    FLD_VALUE,
                                    DESCR,
                                    SH_DESCR,
                                    NVL(UDF_STR1, 'XX') AS UDF_STR1,
                                    NVL(UDF_STR2, '0') AS UDF_STR2,
                                    ACTIVE,
                                    NVL(UDF_STR3, 'XX') AS UDF_STR3,
                                    NVL(UDF_STR4, 'XX') AS UDF_STR4
                                    FROM 
                                    GENCODES
                                WHERE 
                                    TRIM(FLD_NAME) = 'TRANS_TYPE' 
                                    AND TRIM(MOD_NAME) = 'W_EMP_EMPLOY_EVENTS' 
                                    AND ACTIVE = 'Y'
                                ORDER BY 
                                    DESCR";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var transaction = new GetTransactionResponse
                                {
                                    FLD_NAME = reader["FLD_NAME"].ToString(),
                                    MOD_NAME = reader["MOD_NAME"].ToString(),
                                    FLD_VALUE = reader["FLD_VALUE"].ToString(),
                                    DESCR = reader["DESCR"].ToString(),
                                    SH_DESCR = reader["SH_DESCR"].ToString(),
                                    UDF_STR1 = reader["UDF_STR1"].ToString(),
                                    UDF_STR2 = reader["UDF_STR2"].ToString(),
                                    //ACTIVE = reader["ACTIVE"] == DBNull.Value ? null : Convert.ToChar(reader["ACTIVE"]),
                                    ACTIVE = reader["ACTIVE"] == DBNull.Value ? (char?)null : Convert.ToChar(reader["ACTIVE"]),
                                    UDF_STR3 = reader["UDF_STR3"].ToString(),
                                    UDF_STR4 = reader["UDF_STR4"].ToString(),
                                };

                                transactions.Add(transaction);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving transactions", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting transaction: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return transactions;
        }

        // Location, Department, Site
        public async Task<List<GetSectionResponse>> ddl_GetSections(string siteCode, string deptCode = null)
        {
            try
            {
                var query = _appDbContext.SECTION_ORG
                    .Where(s => s.SITE_CODE == siteCode);

                if (!string.IsNullOrEmpty(deptCode))
                {
                    query = query.Where(s => s.DEPT_CODE == deptCode);
                }

                var sections = await query.OrderBy(s => s.MENU_DESCR).ToListAsync();

                // Map Section entities to GetSectionResponse DTOs
                return sections.Select(s => new GetSectionResponse
                {
                    SectionCode = s.SECTION_CODE,
                    MenuDescr = s.MENU_DESCR,
                    DeptCode = s.DEPT_CODE,
                    SiteCode = s.SITE_CODE
                }).ToList();
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting sections: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        public async Task<List<GetLocationResponse>> ddl_GetLocations()
        {
            var transactions = new List<GetLocationResponse>();

            try
            {

                var sqlQuery = @"SELECT DISTINCT
                                    A.SITE_CODE,
                                    UPPER(A.DESCR) AS DESCR,
                                    A.AREA_CODE,
                                    A.ADD1 || '' || A.ADD2 || '' || A.CITY || '-' || A.PIN AS ADDRESS,
                                    A.FIN_ENTITY
                                FROM
                                    SITE A
                                INNER JOIN
                                    EMPLOYEE B ON A.SITE_CODE = B.EMP_SITE
                                WHERE
                                    A.AREA_CODE IN ('H', 'F')
                                    AND B.EMP_TYPE = 'H'
                                ORDER BY
                                    A.SITE_CODE";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, connection))
                {

                    try
                    {
                        await connection.OpenAsync();

                        // Execute the query asynchronously
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var transaction = new GetLocationResponse
                                {
                                    SITE_CODE = reader["SITE_CODE"].ToString(),
                                    DESCR = reader["DESCR"].ToString(),
                                    AREA_CODE = reader["AREA_CODE"].ToString(),
                                    ADDRESS = reader["ADDRESS"].ToString(),
                                    FIN_ENTITY = reader["FIN_ENTITY"].ToString(),
                                };

                                transactions.Add(transaction);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log or handle the exception appropriately
                        throw new Exception("Error retrieving transactions", ex);
                    }
                }
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting locations: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return transactions;
        }

        public async Task<List<GetDepartmentResponse>> ddl_GetDepartments()
        {
            //select * from department WHERE DEPT_CODE LIKE 'D%' and status = 'A' order by DEPT_CODE
            try
            {
                return await _appDbContext.DEPARTMENT
                    .Where(d => d.id.StartsWith("D") && d.Status == "A")
                    .OrderBy(d => d.id)
                    .Select(d => new GetDepartmentResponse
                    {
                        DeptCode = d.id,
                        Descr = d.name
                    })
                    .ToListAsync();
            }
            catch (Exception ex)
            {

                string str = ex.GetType().ToString();
                if (ex.GetType() == typeof(DbUpdateException) || ex.GetType() == typeof(OracleException))
                {
                    //var error=  ValidationHelper.GetPropertyNameAndErrorFromOracleException(ex, grade);
                    //throw new RepositoryException(error.PropertyName, error.ErrorMessage, ex);
                    string error = ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while getting departments: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }


        // for txMode working correctly
          public async Task<(List<MastersUnconfWithDetails>, int)> GetAllTransactions(int page, int items_per_page, string search, string sessionEmpCode, DateOnly? period, string category, string status, string txMode)
          {
              // Calculate the starting row number based on the page number and page size
              int startRow = (page - 1) * items_per_page + 1; // Oracle uses 1-based index
              int endRow = page * items_per_page;

              List<MastersUnconfWithDetails> transactions = new List<MastersUnconfWithDetails>();
              int totalCount = 0;

              string countQuery = $@"
                  SELECT COUNT(*)
                  FROM 
                      MASTERS_UNCONF_REVIEWER A 
                      INNER JOIN MASTERS_UNCONF B ON A.TRAN_ID = B.TRAN_ID
                  WHERE 
                      {(txMode == "A" ?                    
                          $@"TRIM(A.EMP_CODE__REVIEW) = '{sessionEmpCode.Trim()}' 
                          AND A.CONFIRMED IN ('O', 'P', 'Y', 'R') AND  B.WF_PROCESS_LEVEL >= A.LEVEL_NO " :
                          $@"TRIM(B.EMP_CODE) = '{sessionEmpCode.Trim()}' AND B.CONFIRMED IN ('N', 'Y', 'R') AND A.CONFIRMED IN ('O', 'P', 'Y', 'R')
                              AND B.WF_PROCESS_LEVEL = A.LEVEL_NO"
                      )}
                      AND (:search IS NULL 
                          OR UPPER(B.TRAN_ID) LIKE '%' || :searchUpper || '%'
                          OR UPPER(B.TABLE_NAME) LIKE '%' || :searchUpper || '%'
                          OR TO_CHAR(B.TRAN_DATE, 'YYYY-MM-DD') LIKE '%' || :searchUpper || '%'
                      )
                      AND ('{period}' IS NULL OR TO_CHAR(B.TRAN_DATE, 'YYYY-MM-DD') = TO_CHAR('{period}', 'YYYY-MM-DD'))
                      AND ('{category}' IS NULL OR B.TABLE_NAME = '{category}')
                      AND ('{status}' IS NULL OR B.CONFIRMED = '{status}')";


              string dataQuery = $@"
                 SELECT *
                 FROM (
                     SELECT 
                         B.TRAN_ID, 
                         B.TRAN_DATE, 
                         B.TABLE_NAME,
                         B.KEY_VAR,
                         B.WF_PROCESS_LEVEL,
                         B.SQL_QRY,
                         B.CONFIRMED,
                         B.CHG_DATE,
                         B.CHG_USER,
                         B.CHG_TERM,
                          CASE 
                           WHEN B.TABLE_NAME = 'EMPLOYEE' THEN  
                               (SELECT D1.NEW_VALUE 
                                FROM MASTERS_UNCONF_DET D1 
                                WHERE D1.TRAN_ID = B.TRAN_ID AND D1.FIELD_NAME = 'EMP_CODE') 
                               || ' ' || 
                               (SELECT D2.NEW_VALUE 
                                FROM MASTERS_UNCONF_DET D2 
                                WHERE D2.TRAN_ID = B.TRAN_ID AND D2.FIELD_NAME = 'SHORT_NAME') 
                           ELSE B.EMP_CODE 
                         END AS EMP_CODE,
                         B.REMARKS,
                         B.FILE_PATH,
                         D.NEW_VALUE,
                         C.SHORT_NAME AS APPROVER_NAME,
                         A.EMP_CODE__REVIEW AS APPROVER_CODE,
                         E.SHORT_NAME AS INITIATED_AS,
                         B.EMP_CODE AS INITIATED_CODE,
                         ROW_NUMBER() OVER (ORDER BY B.TRAN_DATE DESC) AS rn 
                     FROM 
                         MASTERS_UNCONF_REVIEWER A 
                         INNER JOIN MASTERS_UNCONF B ON A.TRAN_ID = B.TRAN_ID
                         LEFT JOIN MASTERS_UNCONF_DET D ON B.TRAN_ID = D.TRAN_ID AND UPPER(D.FIELD_NAME) = 'DESCR'
                         LEFT JOIN EMPLOYEE C ON A.EMP_CODE__REVIEW = C.EMP_CODE
                         LEFT JOIN EMPLOYEE E ON TRIM(B.EMP_CODE) = TRIM(E.EMP_CODE)
                         LEFT JOIN EMPLOYEE F ON A.EMP_CODE__REVIEW = F.EMP_CODE AND  A.LEVEL_NO = B.WF_PROCESS_LEVEL
                     WHERE 
                        {(txMode == "A" ?
                          $@"TRIM(A.EMP_CODE__REVIEW) = '{sessionEmpCode.Trim()}' 
                          AND A.CONFIRMED IN ('O', 'P', 'Y', 'R') AND  B.WF_PROCESS_LEVEL >= A.LEVEL_NO " :
                          $@"TRIM(B.EMP_CODE) = '{sessionEmpCode.Trim()}' 
                          AND B.CONFIRMED IN ('N', 'Y', 'R') AND A.CONFIRMED IN ('O', 'P', 'Y', 'R')  AND B.WF_PROCESS_LEVEL = A.LEVEL_NO"
                      )}                    
                         AND ('{search}' IS NULL 
                             OR UPPER(B.TRAN_ID) LIKE '%' || :searchUpper || '%'
                             OR UPPER(B.TABLE_NAME) LIKE '%' || :searchUpper || '%'
                             OR TO_CHAR(B.TRAN_DATE, 'YYYY-MM-DD') LIKE '%' || :searchUpper || '%'
                         )
                         AND ('{period}' IS NULL OR TO_CHAR(B.TRAN_DATE, 'YYYY-MM-DD') = TO_CHAR('{period}', 'YYYY-MM-DD'))
                         AND ('{category}' IS NULL OR B.TABLE_NAME = '{category}')
                         AND ('{status}' IS NULL OR B.CONFIRMED = '{status}')
                     )
                 WHERE rn BETWEEN '{startRow}' AND '{endRow}'";


              using (OracleConnection connection = _appDbContext.GetConn())
              using (OracleCommand command = _appDbContext.GetCommand())
              {
                  try
                  {
                      await connection.OpenAsync();
                      command.BindByName = true;

                      // Count query to get the total number of transactions
                      command.CommandText = countQuery;
                      command.Parameters.Add(new OracleParameter("search", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search));
                      command.Parameters.Add(new OracleParameter("searchUpper", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search.ToUpper()));

                      // Execute the count query to get the total number of transactions
                      totalCount = Convert.ToInt32(await command.ExecuteScalarAsync());

                      // Data query to get paginated transaction records
                      command.CommandText = dataQuery;
                      command.Parameters.Clear(); // Clear previous parameters                
                      command.Parameters.Add(new OracleParameter("searchUpper", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search.ToUpper()));
                      string fullQuery = GetFullQueryWithParameters(command);
                      using (OracleDataReader reader = (OracleDataReader)await command.ExecuteReaderAsync())
                      {
                          while (await reader.ReadAsync())
                          {
                              var transaction = new MastersUnconfWithDetails();

                              transaction.id = reader["TRAN_ID"].ToString();
                              transaction.TRAN_DATE = Convert.ToDateTime(reader["TRAN_DATE"]);
                              //TRAN_DATE = reader["TRAN_DATE"] != DBNull.Value ? reader.GetDateTime("TRAN_DATE").Date : default(DateTime),
                              transaction.TABLE_NAME = reader["TABLE_NAME"].ToString();
                              transaction.KEY_VAR = reader["KEY_VAR"].ToString();
                              transaction.WF_PROCESS_LEVEL = Convert.ToInt32(reader["WF_PROCESS_LEVEL"]);
                              transaction.SQL_QRY = reader["SQL_QRY"].ToString();
                              transaction.CONFIRMED = Convert.ToChar(reader["CONFIRMED"]);
                              transaction.CHG_DATE = Convert.ToDateTime(reader["CHG_DATE"]);
                              transaction.CHG_USER = reader["CHG_USER"].ToString();
                              transaction.CHG_TERM = reader["CHG_TERM"].ToString();
                              transaction.EMP_CODE = reader["EMP_CODE"].ToString();
                              transaction.REMARKS = reader["REMARKS"].ToString();
                              transaction.FILE_PATH = reader["FILE_PATH"].ToString();
                              transaction.NEW_VALUE = reader["NEW_VALUE"].ToString();
                              transaction.APPROVER_NAME = reader["APPROVER_NAME"].ToString();
                              transaction.APPROVER_CODE = reader["APPROVER_CODE"].ToString();
                              transaction.INITIATED_AS = reader["INITIATED_AS"].ToString();
                              transaction.INITIATED_CODE = reader["INITIATED_CODE"].ToString();

                              transactions.Add(transaction);
                          }

                      }
                      return (transactions, totalCount);
                  }
                  catch (Exception ex)
                  {
                      // Handle or log exceptions appropriately
                      throw new Exception("Error retrieving transactions", ex);
                  }
              }
          }


        /*public async Task<(List<MastersUnconfWithDetails>, int)> GetAllTransactions(int page, int items_per_page, string search, string sessionEmpCode, DateOnly? period, string category, string status, string txMode)
        {
            try
            {
                // Calculate the starting row number based on the page number and page size
                int startRow = (page - 1) * items_per_page + 1; // Oracle uses 1-based index
                int endRow = page * items_per_page;

                List<MastersUnconfWithDetails> transactions = new List<MastersUnconfWithDetails>();
                int totalCount = 0;

                // Parse the search string for date formats
                DateTime parsedDate;
                bool isFullDate = DateTime.TryParseExact(search, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out parsedDate);
                bool isMonthYear = !isFullDate && DateTime.TryParseExact(search, "MM/yyyy", null, System.Globalization.DateTimeStyles.None, out parsedDate);
                bool isYear = !isFullDate && !isMonthYear && DateTime.TryParseExact(search, "yyyy", null, System.Globalization.DateTimeStyles.None, out parsedDate);

                string countQuery = $@"
            SELECT COUNT(*)
            FROM 
                MASTERS_UNCONF_REVIEWER A 
                INNER JOIN MASTERS_UNCONF B ON A.TRAN_ID = B.TRAN_ID
            WHERE 
                {(txMode == "A" ?
                            $@"TRIM(A.EMP_CODE__REVIEW) = :sessionEmpCode 
                    AND A.CONFIRMED IN ('O', 'P', 'Y', 'R') AND B.WF_PROCESS_LEVEL >= A.LEVEL_NO " :
                            $@"TRIM(B.EMP_CODE) = :sessionEmpCode AND B.CONFIRMED IN ('N', 'Y', 'R') AND A.CONFIRMED IN ('O', 'P', 'Y', 'R')
                        AND B.WF_PROCESS_LEVEL = A.LEVEL_NO"
                        )}
                AND (:search IS NULL 
                    OR UPPER(B.TRAN_ID) LIKE '%' || :searchUpper || '%'
                    OR UPPER(B.TABLE_NAME) LIKE '%' || :searchUpper || '%'
                    OR (:isFullDate = 1 AND TO_CHAR(B.TRAN_DATE, 'DD/MM/YYYY') = :fullDate)
                    OR (:isMonthYear = 1 AND TO_CHAR(B.TRAN_DATE, 'MM/YYYY') = :monthYear)
                    OR (:isYear = 1 AND TO_CHAR(B.TRAN_DATE, 'YYYY') = :year)
                )
                AND (:period IS NULL OR TO_CHAR(B.TRAN_DATE, 'YYYY-MM-DD') = TO_CHAR(:period, 'YYYY-MM-DD'))
                AND (:category IS NULL OR B.TABLE_NAME = :category)
                AND (:status IS NULL OR B.CONFIRMED = :status)";

                using (OracleConnection connection = _appDbContext.GetConn())
                using (OracleCommand command = _appDbContext.GetCommand())
                {
                    await connection.OpenAsync();
                    command.BindByName = true;

                    // Set parameters for the count query
                    command.CommandText = countQuery;
                    command.Parameters.Add(new OracleParameter("sessionEmpCode", sessionEmpCode.Trim()));
                    command.Parameters.Add(new OracleParameter("search", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search));
                    command.Parameters.Add(new OracleParameter("searchUpper", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search.ToUpper()));
                    command.Parameters.Add(new OracleParameter("isFullDate", isFullDate ? 1 : 0));
                    command.Parameters.Add(new OracleParameter("fullDate", isFullDate ? parsedDate.ToString("dd/MM/yyyy") : (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("isMonthYear", isMonthYear ? 1 : 0));
                    command.Parameters.Add(new OracleParameter("monthYear", isMonthYear ? parsedDate.ToString("MM/yyyy") : (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("isYear", isYear ? 1 : 0));
                    command.Parameters.Add(new OracleParameter("year", isYear ? parsedDate.ToString("yyyy") : (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("period", period.HasValue ? (object)period.Value : DBNull.Value));
                    command.Parameters.Add(new OracleParameter("category", string.IsNullOrEmpty(category) ? (object)DBNull.Value : category));
                    command.Parameters.Add(new OracleParameter("status", string.IsNullOrEmpty(status) ? (object)DBNull.Value : status));

                    // Execute the count query to get the total number of transactions
                    totalCount = Convert.ToInt32(await command.ExecuteScalarAsync());

                    string dataQuery = $@"
                SELECT *
                FROM (
                    SELECT 
                        B.TRAN_ID, 
                        B.TRAN_DATE, 
                        B.TABLE_NAME,
                        B.KEY_VAR,
                        B.WF_PROCESS_LEVEL,
                        B.SQL_QRY,
                        B.CONFIRMED,
                        B.CHG_DATE,
                        B.CHG_USER,
                        B.CHG_TERM,
                        CASE 
                            WHEN B.TABLE_NAME = 'EMPLOYEE' THEN  
                                (SELECT D1.NEW_VALUE 
                                 FROM MASTERS_UNCONF_DET D1 
                                 WHERE D1.TRAN_ID = B.TRAN_ID AND D1.FIELD_NAME = 'EMP_CODE') 
                                || ' ' || 
                                (SELECT D2.NEW_VALUE 
                                 FROM MASTERS_UNCONF_DET D2 
                                 WHERE D2.TRAN_ID = B.TRAN_ID AND D2.FIELD_NAME = 'SHORT_NAME') 
                            ELSE B.EMP_CODE 
                        END AS EMP_CODE,
                        B.REMARKS,
                        B.FILE_PATH,
                        D.NEW_VALUE,
                        C.SHORT_NAME AS APPROVER_NAME,
                        A.EMP_CODE__REVIEW AS APPROVER_CODE,
                        E.SHORT_NAME AS INITIATED_AS,
                        B.EMP_CODE AS INITIATED_CODE,
                        ROW_NUMBER() OVER (ORDER BY B.TRAN_DATE DESC) AS rn 
                    FROM 
                        MASTERS_UNCONF_REVIEWER A 
                        INNER JOIN MASTERS_UNCONF B ON A.TRAN_ID = B.TRAN_ID
                        LEFT JOIN MASTERS_UNCONF_DET D ON B.TRAN_ID = D.TRAN_ID AND UPPER(D.FIELD_NAME) = 'DESCR'
                        LEFT JOIN EMPLOYEE C ON A.EMP_CODE__REVIEW = C.EMP_CODE
                        LEFT JOIN EMPLOYEE E ON TRIM(B.EMP_CODE) = TRIM(E.EMP_CODE)
                    WHERE 
                        {(txMode == "A" ?
                                $@"TRIM(A.EMP_CODE__REVIEW) = :sessionEmpCode 
                        AND A.CONFIRMED IN ('O', 'P', 'Y', 'R') AND  B.WF_PROCESS_LEVEL >= A.LEVEL_NO " :
                                $@"TRIM(B.EMP_CODE) = :sessionEmpCode 
                        AND B.CONFIRMED IN ('N', 'Y', 'R') AND A.CONFIRMED IN ('O', 'P', 'Y', 'R') AND B.WF_PROCESS_LEVEL = A.LEVEL_NO"
                            )}                    
                        AND (:search IS NULL 
                            OR UPPER(B.TRAN_ID) LIKE '%' || :searchUpper || '%'
                            OR UPPER(B.TABLE_NAME) LIKE '%' || :searchUpper || '%'
                            OR (:isFullDate = 1 AND TO_CHAR(B.TRAN_DATE, 'DD/MM/YYYY') = :fullDate)
                            OR (:isMonthYear = 1 AND TO_CHAR(B.TRAN_DATE, 'MM/YYYY') = :monthYear)
                            OR (:isYear = 1 AND TO_CHAR(B.TRAN_DATE, 'YYYY') = :year)
                        )
                        AND (:period IS NULL OR TO_CHAR(B.TRAN_DATE, 'YYYY-MM-DD') = TO_CHAR(:period, 'YYYY-MM-DD'))
                        AND (:category IS NULL OR B.TABLE_NAME = :category)
                        AND (:status IS NULL OR B.CONFIRMED = :status)
                )
                WHERE rn BETWEEN :startRow AND :endRow";

                    // Set parameters for the data query
                    command.CommandText = dataQuery;
                    command.Parameters.Clear(); // Clear previous parameters
                    command.Parameters.Add(new OracleParameter("sessionEmpCode", sessionEmpCode.Trim()));
                    command.Parameters.Add(new OracleParameter("searchUpper", string.IsNullOrEmpty(search) ? (object)DBNull.Value : search.ToUpper()));
                    command.Parameters.Add(new OracleParameter("isFullDate", isFullDate ? 1 : 0));
                    command.Parameters.Add(new OracleParameter("fullDate", isFullDate ? parsedDate.ToString("dd/MM/yyyy") : (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("isMonthYear", isMonthYear ? 1 : 0));
                    command.Parameters.Add(new OracleParameter("monthYear", isMonthYear ? parsedDate.ToString("MM/yyyy") : (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("isYear", isYear ? 1 : 0));
                    command.Parameters.Add(new OracleParameter("year", isYear ? parsedDate.ToString("yyyy") : (object)DBNull.Value));
                    command.Parameters.Add(new OracleParameter("startRow", startRow));
                    command.Parameters.Add(new OracleParameter("endRow", endRow));
                    command.Parameters.Add(new OracleParameter("period", period.HasValue ? (object)period.Value : DBNull.Value));
                    command.Parameters.Add(new OracleParameter("category", string.IsNullOrEmpty(category) ? (object)DBNull.Value : category));
                    command.Parameters.Add(new OracleParameter("status", string.IsNullOrEmpty(status) ? (object)DBNull.Value : status));

                    // Execute query and fetch the data
                    using (OracleDataReader reader = (OracleDataReader)await command.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var transaction = new MastersUnconfWithDetails
                            {
                                id = reader["TRAN_ID"].ToString(),
                                TRAN_DATE = Convert.ToDateTime(reader["TRAN_DATE"]),
                                TABLE_NAME = reader["TABLE_NAME"].ToString(),
                                KEY_VAR = reader["KEY_VAR"].ToString(),
                                WF_PROCESS_LEVEL = Convert.ToInt32(reader["WF_PROCESS_LEVEL"]),
                                SQL_QRY = reader["SQL_QRY"].ToString(),
                                CONFIRMED = Convert.ToChar(reader["CONFIRMED"]),
                                CHG_DATE = Convert.ToDateTime(reader["CHG_DATE"]),
                                CHG_USER = reader["CHG_USER"].ToString(),
                                CHG_TERM = reader["CHG_TERM"].ToString(),
                                EMP_CODE = reader["EMP_CODE"].ToString(),
                                REMARKS = reader["REMARKS"].ToString(),
                                FILE_PATH = reader["FILE_PATH"].ToString(),
                                NEW_VALUE = reader["NEW_VALUE"].ToString(),
                                APPROVER_NAME = reader["APPROVER_NAME"].ToString(),
                                APPROVER_CODE = reader["APPROVER_CODE"].ToString(),
                                INITIATED_AS = reader["INITIATED_AS"].ToString(),
                                INITIATED_CODE = reader["INITIATED_CODE"].ToString()
                            };

                            transactions.Add(transaction);
                        }
                    }

                    return (transactions, totalCount);
                }
            }
            catch (DbUpdateException dbEx)
            {
                string error = dbEx.InnerException?.Message ?? dbEx.Message;
                throw new RepositoryException(error);
            }
            catch (OracleException oracleEx)
            {
                string error = oracleEx.InnerException?.Message ?? oracleEx.Message;
                throw new RepositoryException(error);
            }
            catch (Exception ex)
            {
                throw new Exception("General error in GetAllTransactions", ex);
            }
        }
        */


        /*   public async Task<List<MastersUnconfWithDetailsForID>> GetTransactionByID(string tranId)
           {
               List<MastersUnconfWithDetailsForID> transactions = new List<MastersUnconfWithDetailsForID>();

               string dataQuery = @"
                   SELECT 
                       A.TRAN_ID, 
                       A.TRAN_DATE, 
                       A.TABLE_NAME,
                       A.KEY_VAR,
                       A.WF_PROCESS_LEVEL,
                       A.SQL_QRY,
                       A.CONFIRMED,
                       A.CHG_DATE,
                       A.CHG_USER,
                       A.CHG_TERM,
                       A.EMP_CODE,
                       A.REMARKS,
                       A.FILE_PATH,
                       D.FIELD_NAME,
                       D.FIELD_DESCRIPTION,
                       D.OLD_KEY,
                       D.OLD_VALUE,
                       D.NEW_KEY,
                       D.NEW_VALUE,
                       D.FILE_PATH AS FILE_PATH_DET,
                       D.CHG_DATE AS CHG_DATE_DET,
                       D.CHG_USER AS CHG_USER_DET,
                       D.CHG_TERM AS CHG_TERM_DET,
                       D.SEQ_NO,
                       R.LEVEL_NO,
                       R.EMP_CODE__REVIEW,
                       E.SHORT_NAME,

                       R.CONFIRMED AS REVIEW_CONFIRMED,
                       R.CHG_DATE AS REVIEW_CHG_DATE
                   FROM 
                       MASTERS_UNCONF A
                       LEFT JOIN MASTERS_UNCONF_DET D ON A.TRAN_ID = D.TRAN_ID
                       LEFT JOIN MASTERS_UNCONF_REVIEWER R ON A.TRAN_ID = R.TRAN_ID
                       LEFT JOIN EMPLOYEE E ON R.EMP_CODE__REVIEW = E.EMP_CODE
                   WHERE 
                       A.TRAN_ID = :tranId AND VISIBILITY = 'Y'
                       Order by D.SEQ_NO ";

               using (OracleConnection connection = _appDbContext.GetConn())
               using (OracleCommand command = new OracleCommand(dataQuery, connection))
               {
                   try
                   {
                       await connection.OpenAsync();
                       command.BindByName = true;

                       // Parameter for transaction ID
                       command.Parameters.Add(new OracleParameter("tranId", tranId));

                       using (OracleDataReader reader = (OracleDataReader)await command.ExecuteReaderAsync())
                       {
                           MastersUnconfWithDetailsForID currentTransaction = null;
                           HashSet<(int LEVEL_NO, string TRAN_ID)> seenReviewers = new HashSet<(int, string)>();

                           while (await reader.ReadAsync())
                           {
                               string currentTranId = reader["TRAN_ID"].ToString();

                               if (currentTransaction == null || currentTransaction.id != currentTranId)
                               {
                                   currentTransaction = new MastersUnconfWithDetailsForID
                                   {
                                       id = reader["TRAN_ID"].ToString(),
                                       TRAN_DATE = Convert.ToDateTime(reader["TRAN_DATE"]),
                                       TABLE_NAME = reader["TABLE_NAME"].ToString(),
                                       KEY_VAR = reader["KEY_VAR"].ToString(),
                                       WF_PROCESS_LEVEL = Convert.ToInt32(reader["WF_PROCESS_LEVEL"]),
                                       SQL_QRY = reader["SQL_QRY"].ToString(),
                                       CONFIRMED = Convert.ToChar(reader["CONFIRMED"]),
                                       TX_CHG_DATE = (DateTime)(reader["CHG_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["CHG_DATE"])),
                                       TX_CHG_USER = reader["CHG_USER"].ToString(),
                                       TX_CHG_TERM = reader["CHG_TERM"].ToString(),
                                       EMP_CODE = reader["EMP_CODE"].ToString(),
                                       REMARKS = reader["REMARKS"].ToString(),
                                       FILE_PATH = reader["FILE_PATH"].ToString(),
                                       FieldNames = new Dictionary<string, FieldValues>(),
                                       Reviewers = new List<Reviewer>()
                                   };
                                   transactions.Add(currentTransaction);
                               }

                               if (!reader.IsDBNull(reader.GetOrdinal("FIELD_NAME")))
                               {
                                   string fieldName = reader["FIELD_NAME"].ToString();
                                   var fieldValues = new FieldValues 
                                   {
                                       FIELD_DESCRIPTION = reader["FIELD_DESCRIPTION"] != DBNull.Value ? reader["FIELD_DESCRIPTION"].ToString().ToUpper() : null,
                                       OLD_KEY = reader["OLD_KEY"] != DBNull.Value ? reader["OLD_KEY"].ToString().ToUpper() : null,
                                       OLD_VALUE = reader["OLD_VALUE"] != DBNull.Value ? reader["OLD_VALUE"].ToString().ToUpper() : null,
                                       NEW_KEY = reader["NEW_KEY"] != DBNull.Value ? reader["NEW_KEY"].ToString().ToUpper() : null,
                                       NEW_VALUE = reader["NEW_VALUE"] != DBNull.Value ? reader["NEW_VALUE"].ToString().ToUpper() : null
                                   };
                                   currentTransaction.FieldNames[fieldName] = fieldValues;
                               }

                               if (!reader.IsDBNull(reader.GetOrdinal("LEVEL_NO")))
                               {
                                   var reviewerKey = (
                                       LEVEL_NO: Convert.ToInt32(reader["LEVEL_NO"]),
                                       TRAN_ID: reader["TRAN_ID"].ToString()
                                   );

                                   if (!seenReviewers.Contains(reviewerKey))
                                   {
                                       var reviewer = new Reviewer
                                       {
                                           LEVEL_NO = reviewerKey.LEVEL_NO,
                                           TRAN_ID = reviewerKey.TRAN_ID,
                                           EMP_CODE__REVIEW = reader["EMP_CODE__REVIEW"].ToString(),
                                           SHORT_NAME = reader["SHORT_NAME"].ToString(),
                                           CONFIRMED = reader["REVIEW_CONFIRMED"].ToString(),
                                           CHG_DATE = reader["REVIEW_CHG_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["REVIEW_CHG_DATE"])
                                       };

                                       currentTransaction.Reviewers.Add(reviewer);
                                       seenReviewers.Add(reviewerKey);
                                   }
                               }
                           }
                       }

                       return transactions;
                   }
                   catch (Exception ex)
                   {
                       // Handle or log exceptions appropriately
                       throw new Exception("Error retrieving transactions", ex);
                   }
               }
           }
   */

        public async Task<List<MastersUnconfWithDetailsForID>> GetTransactionByID(string tranId)
        {
            List<MastersUnconfWithDetailsForID> transactions = new List<MastersUnconfWithDetailsForID>();

            string dataQuery = @"
        SELECT 
            A.TRAN_ID, 
            A.TRAN_DATE, 
            A.TABLE_NAME,
            A.KEY_VAR,
            A.WF_PROCESS_LEVEL,
            A.SQL_QRY,
            A.CONFIRMED,
            A.CHG_DATE,
            A.CHG_USER,
            A.CHG_TERM,
            A.EMP_CODE,
            A.REMARKS,
            A.FILE_PATH,
            D.FIELD_NAME,
            D.FIELD_DESCRIPTION,
            D.OLD_KEY,
            D.OLD_VALUE,
            D.NEW_KEY,
            D.NEW_VALUE,
            D.FILE_PATH AS FILE_PATH_DET,
            D.CHG_DATE AS CHG_DATE_DET,
            D.CHG_USER AS CHG_USER_DET,
            D.CHG_TERM AS CHG_TERM_DET,
            D.SEQ_NO,
            R.LEVEL_NO,
            R.EMP_CODE__REVIEW,
            E.SHORT_NAME,
            R.CONFIRMED AS REVIEW_CONFIRMED,
            R.CHG_DATE AS REVIEW_CHG_DATE,
            R.REMARKS AS REASON
        FROM 
            MASTERS_UNCONF A
            LEFT JOIN MASTERS_UNCONF_DET D ON A.TRAN_ID = D.TRAN_ID
            LEFT JOIN MASTERS_UNCONF_REVIEWER R ON A.TRAN_ID = R.TRAN_ID
            LEFT JOIN EMPLOYEE E ON R.EMP_CODE__REVIEW = E.EMP_CODE
        WHERE 
            A.TRAN_ID = :tranId AND VISIBILITY = 'Y'";

            using (OracleConnection connection = _appDbContext.GetConn())
            using (OracleCommand command = new OracleCommand(dataQuery, connection))
            {
                try
                {
                    await connection.OpenAsync();
                    command.BindByName = true;

                    // Parameter for transaction ID
                    command.Parameters.Add(new OracleParameter("tranId", tranId));

                    using (OracleDataReader reader = (OracleDataReader)await command.ExecuteReaderAsync())
                    {
                        MastersUnconfWithDetailsForID currentTransaction = null;
                        HashSet<(int LEVEL_NO, string TRAN_ID)> seenReviewers = new HashSet<(int, string)>();

                        while (await reader.ReadAsync())
                        {
                            string currentTranId = reader["TRAN_ID"].ToString();

                            if (currentTransaction == null || currentTransaction.id != currentTranId)
                            {
                                currentTransaction = new MastersUnconfWithDetailsForID
                                {
                                    id = reader["TRAN_ID"].ToString(),
                                    TRAN_DATE = Convert.ToDateTime(reader["TRAN_DATE"]),
                                    TABLE_NAME = reader["TABLE_NAME"].ToString(),
                                    KEY_VAR = reader["KEY_VAR"].ToString(),
                                    WF_PROCESS_LEVEL = Convert.ToInt32(reader["WF_PROCESS_LEVEL"]),
                                    SQL_QRY = reader["SQL_QRY"].ToString(),
                                    CONFIRMED = Convert.ToChar(reader["CONFIRMED"]),
                                    TX_CHG_DATE = (DateTime)(reader["CHG_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["CHG_DATE"])),
                                    TX_CHG_USER = reader["CHG_USER"].ToString(),
                                    TX_CHG_TERM = reader["CHG_TERM"].ToString(),
                                    EMP_CODE = reader["EMP_CODE"].ToString(),
                                    REMARKS = reader["REMARKS"].ToString(),
                                    FILE_PATH = reader["FILE_PATH"].ToString(),
                                    FieldNames = new Dictionary<string, FieldValues>(),
                                    Reviewers = new List<Reviewer>()
                                };
                                transactions.Add(currentTransaction);
                            }

                            var fieldList = new List<(string FieldName, FieldValues FieldValues)>();

                            if (!reader.IsDBNull(reader.GetOrdinal("FIELD_NAME")))
                            {
                                string fieldName = reader["FIELD_NAME"].ToString();
                                var fieldValues = new FieldValues
                                {
                                    FIELD_DESCRIPTION = reader["FIELD_DESCRIPTION"] != DBNull.Value ? reader["FIELD_DESCRIPTION"].ToString().ToUpper() : null,
                                    OLD_KEY = reader["OLD_KEY"] != DBNull.Value ? reader["OLD_KEY"].ToString().ToUpper() : null,
                                    OLD_VALUE = reader["OLD_VALUE"] != DBNull.Value ? reader["OLD_VALUE"].ToString().ToUpper() : null,
                                    NEW_KEY = reader["NEW_KEY"] != DBNull.Value ? reader["NEW_KEY"].ToString().ToUpper() : null,
                                    NEW_VALUE = reader["NEW_VALUE"] != DBNull.Value ? reader["NEW_VALUE"].ToString().ToUpper() : null,
                                    SEQ_NO = (int)(reader["SEQ_NO"] != DBNull.Value ? Convert.ToInt32(reader["SEQ_NO"]) : (int?)null)
                                };

                                // Add to list for sorting later
                                fieldList.Add((FieldName: fieldName, FieldValues: fieldValues));
                            }

                            // Order the list by SEQ_NO
                            fieldList = fieldList.OrderBy(fv => fv.FieldValues.SEQ_NO).ToList();

                            // Process the ordered list
                            foreach (var item in fieldList)
                            {
                                currentTransaction.FieldNames[item.FieldName] = item.FieldValues;
                            }

                            if (!reader.IsDBNull(reader.GetOrdinal("LEVEL_NO")))
                            {
                                var reviewerKey = (
                                    LEVEL_NO: Convert.ToInt32(reader["LEVEL_NO"]),
                                    TRAN_ID: reader["TRAN_ID"].ToString()
                                );

                                if (!seenReviewers.Contains(reviewerKey))
                                {
                                    var reviewer = new Reviewer
                                    {
                                        LEVEL_NO = reviewerKey.LEVEL_NO,
                                        TRAN_ID = reviewerKey.TRAN_ID,
                                        EMP_CODE__REVIEW = reader["EMP_CODE__REVIEW"].ToString(),
                                        SHORT_NAME = reader["SHORT_NAME"].ToString(),
                                        CONFIRMED = reader["REVIEW_CONFIRMED"].ToString(),
                                        CHG_DATE = reader["REVIEW_CHG_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["REVIEW_CHG_DATE"]),
                                        REASON = reader["REASON"].ToString()
                                    };

                                    currentTransaction.Reviewers.Add(reviewer);
                                    seenReviewers.Add(reviewerKey);
                                }
                            }
                        }
                    }

                    return transactions;
                }
                catch (Exception ex)
                {
                    // Handle or log exceptions appropriately
                    throw new Exception("Error retrieving transactions", ex);
                }
            }
        }


        // Generate Insert/update Query for masters
        public async Task<string> GetTransactionDetails(string tranId, string tableName)
        {
            var fieldNames = new List<string>();
            var newValues = new List<string>();
            var setClauses = new List<string>();

            string specialCodeField = string.Empty;
            string specialCodeValue = ""; // Default to NULL for INSERT queries

            string selectQuery = @"
            SELECT 
                mu.TABLE_NAME,
                mud.FIELD_NAME,
                mud.NEW_KEY,
                mud.VISIBILITY
            FROM 
                MASTERS_UNCONF mu
            JOIN 
                MASTERS_UNCONF_DET mud ON mu.TRAN_ID = mud.TRAN_ID
            WHERE 
                mu.TRAN_ID = :tranId 
                AND mu.TABLE_NAME = :tableName";

            try
            {
                using (var connection = _appDbContext.GetConn())
                {
                    await connection.OpenAsync();
                    using (var selectCommand = new OracleCommand(selectQuery, connection))
                    {
                        selectCommand.Parameters.Add(new OracleParameter("tranId", tranId));
                        selectCommand.Parameters.Add(new OracleParameter("tableName", tableName));

                        using (var reader = await selectCommand.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                var fieldName = reader["FIELD_NAME"].ToString();
                                var newValue = reader["NEW_KEY"].ToString();
                                var visibility = reader["VISIBILITY"].ToString();

                                if (visibility.Equals("Y",StringComparison.OrdinalIgnoreCase))
                                {
                                    if (tableName.Equals("CADRE", StringComparison.OrdinalIgnoreCase) && fieldName.Equals("cadre_code", StringComparison.OrdinalIgnoreCase))
                                    {
                                        specialCodeField = "cadre_code";
                                        specialCodeValue = string.IsNullOrEmpty(newValue) ? null : $"'{newValue}'";
                                    }
                                    else if (tableName.Equals("GRADE", StringComparison.OrdinalIgnoreCase) && fieldName.Equals("grade_code", StringComparison.OrdinalIgnoreCase))
                                    {
                                        specialCodeField = "grade_code";
                                        specialCodeValue = string.IsNullOrEmpty(newValue) ? null : $"'{newValue}'";
                                    }
                                    else if (tableName.Equals("DEPARTMENT", StringComparison.OrdinalIgnoreCase) && fieldName.Equals("dept_code", StringComparison.OrdinalIgnoreCase))
                                    {
                                        specialCodeField = "dept_code";
                                        specialCodeValue = string.IsNullOrEmpty(newValue) ? null : $"'{newValue}'";
                                    }
                                    else if (tableName.Equals("QUALIFICATION", StringComparison.OrdinalIgnoreCase) && fieldName.Equals("qlf_code", StringComparison.OrdinalIgnoreCase))
                                    {
                                        specialCodeField = "qlf_code";
                                        specialCodeValue = string.IsNullOrEmpty(newValue) ? null : $"'{newValue}'";
                                    }
                                    else if (tableName.Equals("DESIGNATION", StringComparison.OrdinalIgnoreCase) && fieldName.Equals("design_code", StringComparison.OrdinalIgnoreCase))
                                    {
                                        specialCodeField = "design_code";
                                        specialCodeValue = string.IsNullOrEmpty(newValue) ? null : $"'{newValue}'";
                                    }
                                    else if (tableName.Equals("EMPLOYEE", StringComparison.OrdinalIgnoreCase) && fieldName.Equals("emp_code", StringComparison.OrdinalIgnoreCase))
                                    {
                                        specialCodeField = "emp_code";
                                        specialCodeValue = string.IsNullOrEmpty(newValue) ? null : $"'{newValue}'";
                                    }
                                    else
                                    {
                                        // Handling regular fields
                                        if (DateTime.TryParse(newValue, out DateTime _))
                                        {
                                            newValue = FormatDateForOracle(newValue);
                                        }
                                        else
                                        {
                                            newValue = string.IsNullOrEmpty(newValue) ? "''" : $"'{newValue}'";
                                        }

                                        if (string.IsNullOrEmpty(specialCodeValue))
                                        {
                                            fieldNames.Add(fieldName);
                                            newValues.Add(newValue);
                                        }
                                        else
                                        {
                                            setClauses.Add($"{fieldName} = {newValue}");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is DbUpdateException || ex is OracleException)
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }
                string errorMessage = $"Error while processing transaction: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            // Generate INSERT query
            if (string.IsNullOrEmpty(specialCodeValue))
            {
                if (fieldNames.Count > 0)
                {
                    // Include special code field with NULL value in INSERT query if it's null
                    if (!string.IsNullOrEmpty(specialCodeField))
                    {
                        fieldNames.Add(specialCodeField);
                        newValues.Add("NULL");
                    }

                    string insertQuery = $@"
                    INSERT INTO {tableName} (
                                {string.Join(", ", fieldNames)}
                                            ) VALUES (
                                {string.Join(", ", newValues)}
                                            )";

                    return insertQuery;
                }
            }
            // Generate UPDATE query
            else if (setClauses.Count > 0)
            {
                // Exclude special code field from UPDATE query
                if (!string.IsNullOrEmpty(specialCodeField))
                {
                    setClauses.RemoveAll(clause => clause.StartsWith($"{specialCodeField} ="));
                }

                string updateQuery = $@"
                UPDATE {tableName}
                        SET 
                        {string.Join(", ", setClauses)}
                        WHERE {specialCodeField} = {specialCodeValue}";

                return updateQuery;
            }

            return string.Empty;
        }
     

        private string GetFullQueryWithParameters(DbCommand command)
        {
            // Build the SQL command text with parameters
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.AppendLine(command.CommandText);

            foreach (DbParameter parameter in command.Parameters)
            {
                queryBuilder.AppendLine($"{parameter.ParameterName} = {parameter.Value}");
            }

            return queryBuilder.ToString();
        }


        // Work Flow
        public async Task<int> FnGetMinSanctionLevel(string empCode, string moduleType)
        {
            int minSanctionLevel = 0;

            try
            {
                var sqlQuery = @"
                SELECT 
                    NVL(MIN(M_SEQUENCE), 0) 
                FROM 
                    M_WORKFLOW 
                WHERE 
                    TRIM(M_EMPCODE) = :empcode 
                    AND M_MODULETYPE = :moduletype";

                using (var connection = _appDbContext.Database.GetDbConnection())
                using (var command = new OracleCommand(sqlQuery, (OracleConnection)connection))
                {
                    command.Parameters.Add(new OracleParameter("empcode", empCode));
                    command.Parameters.Add(new OracleParameter("moduletype", moduleType));

                    try
                    {
                        await connection.OpenAsync();

                        var result = await command.ExecuteScalarAsync();
                        minSanctionLevel = Convert.ToInt32(result);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error retrieving minimum sanction level", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is DbUpdateException || ex is OracleException)
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error while getting minimum sanction level: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return minSanctionLevel;
        }

        public async Task<int> FnGetCurrentSanctionAuthority(string empCode, string moduleType, double seqNo)
        {
            int currSanctionLevel = 0;

            try
            {
                var sqlQuery = @"
                SELECT 
                    TRIM(M_SANEMPCODE) AS M_SANEMPCODE, 
                    M_SEQUENCE, 
                    TRIM(M_SUBSANEMPCODE) AS M_SUBSANEMPCODE 
                FROM 
                    M_WORKFLOW
                WHERE 
                    TRIM(M_EMPCODE)= :empcode 
                    AND M_MODULETYPE = :moduletype AND M_SEQUENCE = : SeqNo ";


                using (var connection = _appDbContext.Database.GetDbConnection())
                using (var command = new OracleCommand(sqlQuery, (OracleConnection)connection))
                {
                    command.Parameters.Add(new OracleParameter("empcode", empCode));
                    command.Parameters.Add(new OracleParameter("moduletype", moduleType));
                    command.Parameters.Add(new OracleParameter("seqNo", seqNo));

                    try
                    {
                        await connection.OpenAsync();

                        var result = await command.ExecuteScalarAsync();
                        currSanctionLevel = Convert.ToInt32(result);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error retrieving current sanction level", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is DbUpdateException || ex is OracleException)
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error while getting current sanction level: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }

            return currSanctionLevel;
        }

        public async Task<int> FnGetMaximunSanctionAuthority(string SessionempCode, string moduleType)
        {
            moduleType = "MASTERS";
            try
            {
                var sqlQuery = @"
                SELECT 
                    M_SEQUENCE 
                FROM 
                    M_WORKFLOW 
                WHERE 
                    TRIM(M_EMPCODE) = :empcode 
                    AND M_MODULETYPE = :moduletype 
                    AND M_SEQUENCE = (
                        SELECT MAX(M_SEQUENCE) 
                        FROM M_WORKFLOW 
                        WHERE TRIM(M_EMPCODE) = :empcode 
                        AND M_MODULETYPE = :moduletype
                    )";

                using (var connection = _appDbContext.GetConn())
                using (var command = new OracleCommand(sqlQuery, (OracleConnection)connection))
                {
                    command.Parameters.Add(new OracleParameter("empcode", SessionempCode.Trim()));
                    command.Parameters.Add(new OracleParameter("moduletype", moduleType.Trim()));

                    await connection.OpenAsync();

                    var result = await command.ExecuteScalarAsync();

                    if (result != null && int.TryParse(result.ToString(), out int maxSanctionLevel))
                    {
                        return maxSanctionLevel;
                    }
                    else
                    {
                        throw new Exception("Failed to parse maximum sanction level.");
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is DbUpdateException || ex is OracleException)
                {
                    string error = ex.InnerException?.Message ?? ex.Message;
                    throw new RepositoryException(error);
                }

                string errorMessage = $"Error while getting maximum sanction level: {ex.Message}";

                if (ex.InnerException != null)
                {
                    errorMessage += $", Inner Exception: {ex.InnerException.Message}";
                }

                throw new Exception(errorMessage, ex.InnerException);
            }
        }

        public async Task<List<string>> FnAddMastersUnconfReviewer(string empCode, string tranIdEmpDet, string remoteAddr, string Remarks, List<string> lstQuery)
        {

            try
            {
                //var lstQuery = new List<string>();
                //DataTable dt = new DataTable();
                string sanEmpCode = "";
                int sequence = 0;
                using (var connection = _appDbContext.GetConn())
                {

                    await connection.OpenAsync();
                    string selectQuery = @$"
                    SELECT M_SANEMPCODE, M_SEQUENCE 
                    FROM M_WORKFLOW 
                    WHERE M_EMPCODE = '{empCode}' 
                    AND M_MODULETYPE = 'MASTERS' 
                    ORDER BY M_SEQUENCE";


                    using (var cmd = _appDbContext.GetCommand())
                    {
                        cmd.CommandText = selectQuery;
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Clear();
                        int Line_no = 0;
                        using (var reader = await cmd.ExecuteReaderAsync())
                        {

                            while (await reader.ReadAsync())
                            {
                                sanEmpCode = reader["M_SANEMPCODE"].ToString();
                                sequence = Convert.ToInt32(reader["M_SEQUENCE"]);
                                string insertQuery = $"INSERT INTO MASTERS_UNCONF_REVIEWER (TRAN_ID, LINE_NO, LEVEL_NO, EMP_CODE__REVIEW, REMARKS, CONFIRMED, CHG_USER, CHG_DATE, CHG_TERM) VALUES ('{tranIdEmpDet}', '{Line_no}', {sequence}, '{sanEmpCode}', '{Remarks}', 'O', '{empCode}', SYSDATE, '{remoteAddr}')";
                                lstQuery.Add(insertQuery);
                            }
                        }
                    }

                    return lstQuery;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        private string FormatDateForOracle(string dateValue)
        {
            if (DateTime.TryParse(dateValue, out DateTime parsedDate))
            {
                return $"TO_DATE('{parsedDate:yyyy-MM-dd}', 'YYYY-MM-DD')";
            }
            return "SYSDATE";
        }
    }
}
