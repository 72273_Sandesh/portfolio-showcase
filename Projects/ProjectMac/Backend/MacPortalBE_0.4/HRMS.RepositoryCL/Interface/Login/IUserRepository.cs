﻿using HRMS.ModelCL.Login;
using HRMS.ModelCL.Payloads;
using Microsoft.AspNetCore.Identity.Data;
using Microsoft.AspNetCore.Mvc;
using RMS.ModelCL.Login;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using LoginRequest = HRMS.ModelCL.Login.LoginRequest;

namespace HRMS.RepositoryCL.Interface.Login
{
    public interface IUserRepository
    {
        Task<users> AddUser(users user);
        //Task<string> Login(ModelCL.Login.LoginRequest loginRequest);
        Task<Role> AddRole(Role role);
        Task<bool> AssignRoleToUser(AddUserRole obj);

       // Task<(string, string, string, string, string, string)> GetUsersAsync(LoginRequest users);
        
        Task<IActionResult> Authenticate(LoginRequest users);

        string GenerateAccessToken(string retVal, string empFirstName, string empLastName, string empCode, string empEmail, string profileId);
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);

        Task<bool> ValidateRefreshTokenAsync(string apiToken, string refreshToken);
        Task<(string, string)> RefreshTokensAsync(string apiToken, string refreshToken);
        Task<UserProfileDetails> GetUsersProfileDet(string userId);
        Task<TokenVerifyResponse> VerifyToken(Tokens token);
    }
}
