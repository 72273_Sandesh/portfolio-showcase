﻿using HRMS.ModelCL.ADMINISTRATION.Masters.Cadre;
using HRMS.ModelCL.ADMINISTRATION.Masters.CadreBand;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Cadre;
using HRMS.ModelCL.Payloads.Department;
using HRMS.RepositoryCL.Comman;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Interface.Masters
{
    public interface ICadreRepository
    {
        //Task<List<CadreGetAllResponse>> GetAllCadres(int page, int items_per_page, string search);
        Task<(List<CadreGetAllResponse>, int)> GetAllCadres(int page, int items_per_page, string search);
        int CountAllCadres();
        Task<List<CadreGetAllResponse>> FetchCadreByCadreCode(string cadreCode);
        Task<bool> CreateCadre(CadreModel cadre);
        UpdateFlagResponse UpdateCadre(CadreModel cadre);

        Task<List<CadreBandModel>> GetAllCadre_Category();


        //Task <Cadre> GetCadreByCadreCode(string cadreCode);
        //Task<List<Cadre>> GetCadresByCadreCode(string cadreCode);
        //Task<bool> UpdateCadre(Cadre cadre);
    }
}
