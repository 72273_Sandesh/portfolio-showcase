﻿using HRMS.ModelCL.Payloads.Transaction.Affecting;
using HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS.RepositoryCL.Interface.Transaction
{
    public interface ITransactionRepository
    {
        Task<List<GetTransactionResponse>> ddl_GetTransaction();
        Task<List<GetSectionResponse>> ddl_GetSections(string siteCode, string deptCode = null);
        Task<List<GetLocationResponse>> ddl_GetLocations();
        Task<List<GetDepartmentResponse>> ddl_GetDepartments();

        Task<(List<MastersUnconfWithDetails>, int)> GetAllTransactions(int page, int items_per_page, string search, string sessionEmpCode, DateOnly? period, string category, string status, string txMode);

        //Task<List<TransactionGetbyIDResponse>> GetTransactionByID(string tranId);
        Task<List<MastersUnconfWithDetailsForID>> GetTransactionByID(string tranId);
        //Task RejectTransaction([FromBody] RejectTransactionRequest request);

        //Task<IEnumerable<GetTransactionDetailsForInsertResponse>> GetTransactionDetailsAsync(string tranId, string tableName);
        Task<string> GetTransactionDetails(string tranId, string tableName);
        //Task ApproveTransaction(string tranId, string ipAddress, string empCode, string tableName);
        //Task ApproveTransaction(string tranId, string ipAddress, string empCode, string tableName, int wfProcLevel, string moduleType,string remarks);

        Task<int> FnGetMinSanctionLevel(string empCode, string moduleType);
        Task<int> FnGetCurrentSanctionAuthority(string empCode, string moduleType, double seqNo);
        Task<int> FnGetMaximunSanctionAuthority(string empCode, string moduleType);

        Task<List<string>> FnAddMastersUnconfReviewer(string empCode, string tranIdEmpDet, string remoteAddr, string Remarks, List<string> lst);
    }
}
