﻿using Microsoft.EntityFrameworkCore;
using HRMS.ModelCL.Login;
using Microsoft.Extensions.Primitives;
using Oracle.ManagedDataAccess.Client;
using System.Data.SqlClient;
using HRMS_Core.Data;
using Microsoft.Extensions.Configuration;
using RMS.ModelCL.Login;
using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.ADMINISTRATION.Masters.Cadre;
using HRMS.ModelCL.ADMINISTRATION.Masters.CadreBand;
using HRMS.ModelCL.ADMINISTRATION.Masters.Grade;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Data;
using System.Drawing;
using Microsoft.Extensions.Logging;
using HRMS.ModelCL.ADMINISTRATION.Masters.Designation;
using HRMS.ModelCL.Payloads.Employee;
using HRMS.ModelCL.Payloads.Employee.Language;
using HRMS.ModelCL.Payloads.Employee.Family;
using HRMS.ModelCL.Payloads.Employee.Qualification;
using HRMS.ModelCL.Payloads.Employee.Experience;
using HRMS.ModelCL.Payloads.Employee.Assets;
using HRMS.ModelCL.Payloads.Transaction.Affecting;
using HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction;
using HRMS.ModelCL.Payloads.Transaction;


namespace HRMS_Core.Data.Data
{
    public class AppDbContext : DbContext, IAppDbContext
    {

        private IConfiguration _config;
        //Oracle Connection
        private OracleConnection _connection;
        private OracleCommand _cmd;
        private string _connectionString;

        private OracleDataAdapter _daOra = null;
        OracleCommandBuilder _builder = null;
        private OracleDataReader _dtreaderOra;
        private DataTable _dtTab;

        private OracleConnection _oraConnection;
        private OracleTransaction _oraTransaction;

        //SQL Connection
        private SqlConnection _SQLconnection;
        private SqlCommand _SQLcmd;
        private string _SQLconnectionString;

        private SqlDataAdapter _SQLda = null;
        SqlCommandBuilder _SQLbuilder = null;
        private SqlDataAdapter _SQLdtreader;


        private readonly IConfiguration _configuration;
        private SqlConnection _sqlConnection;
        private SqlTransaction _sqlTransaction;


        public AppDbContext(DbContextOptions<AppDbContext> options, IConfiguration configuration) : base(options)
        {
            _config = configuration;
#pragma warning disable CS8601 // Possible null reference assignment.
            _connectionString = _config.GetConnectionString("oracleConnection");
#pragma warning restore CS8601 // Possible null reference assignment.
            _SQLconnectionString = _config.GetConnectionString("SQLConnection");
        }

        public DbSet<users> users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        //---------------------------------------------------
        public DbSet<Employee> EMPLOYEE { get; set; }
        public DbSet<Employee_Det_Change> EMPLOYEE_DET_CHANGE { get; set; }
        public DbSet<AddLanguage> EMPLANG { get; set; }

        public DbSet<AddFamily> EMPFAMILY { get; set; }
        public DbSet<AddQualification> EMPQUAL { get; set; }
        public DbSet<AddExperience> EMPEXPERIENCE { get; set; }
        public DbSet<AddAssets> EMPINDUCTION { get; set; }
        //---------------------------------------------------
        public DbSet<Department> DEPARTMENT { get; set; }

        public DbSet<CadreModel> CADRE { get; set; }
        public DbSet<CadreBandModel> CADRE_BAND { get; set; }
        public DbSet<GradeModel> GRADE { get; set; }

        public DbSet<Qualification> QUALIFICATION { get; set; }

        public DbSet<AcountsModel> ACCOUNTS { get; set; }

        public DbSet<CostCenterModel> COSTCTR { get; set; }
        public DbSet<PayTableModel> PAYTABLE { get; set; }

        public DbSet<QualwiseStruc> QUALWISESTRUC { get; set; }

        public DbSet<DesignationModel> DESIGNATION { get; set; }

        public DbSet<ControllerMasterModel> CONTROLLER_MASTER { get; set; }

        public DbSet<UserRightsModel> USER_RIGHTS { get; set; }

        public DbSet<MastersUnconf> MASTERS_UNCONF { get; set; }
        public DbSet<MastersUnconfDet> MASTERS_UNCONF_DET { get; set; }
        //-------------------------------------------------------------------------
        public DbSet<Section> SECTION_ORG { get; set; }

        public DbSet<GENMST> GENMST { get; set; }

        public OracleConnection GetConn()
        {
            _connection = new OracleConnection(_connectionString);
            return _connection;
        }

        public OracleCommand GetCommand()
        {
            _cmd = _connection.CreateCommand();
            return _cmd;
        }

        public SqlConnection GetSQLConnection()
        {
            _SQLconnection = new SqlConnection(_SQLconnectionString);
            return _SQLconnection;
        }

        public SqlCommand GetSQLCommand()
        {
            _SQLcmd = _SQLconnection.CreateCommand();
            return _SQLcmd;
        }

        public async Task<object> ExecuteScalarAsync(string query)
        {
            try
            {
                using (DbConnection connection = GetConn())
                {
                    await connection.OpenAsync();
                    using (DbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = query;
                        return await command.ExecuteScalarAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                //Handle the exception appropriately
                throw ex;
            }
        }

        public object ExecuteScalar(string query)
        {
            try
            {
                using (DbConnection connection = GetConn())
                {
                    connection.Open();
                    using (DbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = query;
                        return command.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle the exception appropriately
                throw ex;
            }
        }

        //public async Task<OracleDataReader> ExecuteReaderAsync(string query, OracleParameter[] parameters )
        //{
        //    try
        //    {

        //        using (OracleConnection connection = GetConn())
        //        {
        //            await connection.OpenAsync();

        //            using (OracleCommand command = new OracleCommand(query, connection))
        //            {
        //                // Add parameters if provided
        //                if (parameters != null)
        //                {
        //                    command.Parameters.AddRange(parameters);
        //                }

        //                // Execute the query asynchronously and return the OracleDataReader
        //                return await command.ExecuteReaderAsync(CommandBehavior.CloseConnection);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // Handle the exception appropriately
        //        Console.WriteLine(ex);
        //        return null;
        //    }
        //}


        #region Sql Connection function
        public void OpenConnection(string dbType = "SQL")
        {
            try
            {
                if (dbType.ToLower().Contains("sql"))
                {
                    if (_sqlConnection.State == ConnectionState.Closed)
                    {
                        if (!string.IsNullOrEmpty(_SQLconnectionString))
                        {
                            _sqlConnection.ConnectionString = _SQLconnectionString;
                            _sqlConnection.Open();
                        }
                    }
                }
                else
                {
                    if (_oraConnection.State == ConnectionState.Closed)
                    {

                        if (!string.IsNullOrEmpty(_SQLconnectionString))
                        {
                            _oraConnection.ConnectionString = _SQLconnectionString;
                            _oraConnection.Open();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BeginTransaction(string dbType = "ORACLE")
        {
            try
            {
                if (dbType == "SQL")
                {
                    _sqlTransaction = _sqlConnection.BeginTransaction();
                }
                else
                {
                    _oraTransaction = _oraConnection.BeginTransaction();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CommitTransaction(string dbType = "ORACLE")
        {
            try
            {
                if (dbType == "SQL")
                {
                    _sqlTransaction?.Commit();
                }
                else
                {
                    _oraTransaction?.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RollBackTransaction(string dbType = "ORACLE")
        {
            try
            {
                if (dbType == "SQL")
                {
                    _sqlTransaction?.Rollback();
                }
                else
                {
                    _oraTransaction?.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CloseConnection(string dbType = "ORACLE")
        {
            try
            {
                if (dbType.ToLower().Contains("sql"))
                {
                    if (_sqlConnection.State == ConnectionState.Open)
                    {
                        _sqlConnection.Close();
                    }
                }
                else
                {
                    if (_oraConnection.State == ConnectionState.Open)
                    {
                        _oraConnection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Oracle Connection Functions
        public OracleConnection OraConnOpen()
        {
            try
            {
                string connectionString = _connectionString;
                OracleConnection connection = new OracleConnection(connectionString);
                connection.Open();
                return connection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void OraConnClose(OracleConnection connection)
        {
            try
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
            }
        }
        #endregion

        public DataSet FillDataSet(OracleCommand strCmd, string strTableName, DataSet Ds = null/* TODO Change to default(_) if this is not a reference type */)
        {
            try
            {
                _daOra = new OracleDataAdapter(strCmd);
                if (Ds == null)
                    Ds = new DataSet();
                if (Ds.Tables.Contains(strTableName))
                {
                    Ds.Tables[strTableName].Clear();
                }
                _daOra.Fill(Ds, strTableName);
                return Ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public DataTable FillDataThruAdapter(OracleCommand strCmd, object strCol)
        //{
        //    try
        //    {
        //        _daOra = new OracleDataAdapter(strCmd);
        //        _dtTab = new DataTable();
        //        _daOra.Fill(_dtTab);

        //        if (strCol.ToString() != "")
        //        {
        //            string i = string.Empty;
        //            int iCnt;
        //        }
        //        return _dtTab;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public async Task<DataTable> FillDataThruAdapter(OracleCommand strCmd, object strCol)
        {
            try
            {
                using (OracleDataAdapter daOra = new OracleDataAdapter(strCmd))
                {
                    DataTable dtTab = new DataTable();
                    daOra.Fill(dtTab);

                    if (strCol != null && !string.IsNullOrEmpty(strCol.ToString()))
                    {
                        // If there's specific logic for strCol, it can be added here
                        string i = string.Empty;
                        int iCnt;
                    }

                    return dtTab;
                }
           

            }
            catch (Exception ex)
            {
                // Log the exception if necessary and then rethrow it
                throw;
            }
        }


        public DataTable FillDataThruAdapterSql(string Qry)
        {
            _cmd.CommandType = CommandType.Text;
            _cmd.CommandText = Qry;
            _dtTab = new DataTable();
            _SQLda = new SqlDataAdapter(_SQLcmd);
            _SQLda.Fill(_dtTab);

            return _dtTab;
        }



        public async Task<T> ExecuteScalarAsync<T>(string query, Action<OracleCommand> parameterizeCommand = null)
        {
            using (var connection = GetConn())
            {
                await connection.OpenAsync();

                using (var command = new OracleCommand(query, connection))
                {
                    parameterizeCommand?.Invoke(command);  // Only invoke if parameterizeCommand is not null

                    object result = await command.ExecuteScalarAsync();
                    if (result != null && result != DBNull.Value)
                    {
                        return (T)Convert.ChangeType(result, typeof(T));
                    }
                    return default;
                }
            }
        }

        public async Task<List<T>> ExecuteReaderAsync<T>(string query, Action<OracleCommand> parameterizeCommand = null) where T : new()
        {
            var resultList = new List<T>();

            using (var connection = GetConn())
            {
                await connection.OpenAsync();

                using (var command = new OracleCommand(query, connection))
                {
                    parameterizeCommand?.Invoke(command);  // Only invoke if parameterizeCommand is not null

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        var properties = typeof(T).GetProperties();

                        while (await reader.ReadAsync())
                        {
                            var dto = new T();

                            foreach (var property in properties)
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal(property.Name)))
                                {
                                    var value = reader[property.Name];
                                    var propertyType = property.PropertyType;

                                    // Handle nullable types
                                    if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                                    {
                                        propertyType = Nullable.GetUnderlyingType(propertyType);
                                    }

                                    // Handle conversions if necessary
                                    if (value.GetType() != propertyType)
                                    {
                                        value = Convert.ChangeType(value, propertyType);
                                    }

                                    property.SetValue(dto, value);
                                }

                            }

                            resultList.Add(dto);
                        }
                    }
                }
            }

            return resultList;
        }

        public async Task<int> ExecuteNonQueryAsync(string query, Action<OracleCommand> parameterizeCommand = null)
        {
            using (var connection = GetConn())
            {
                await connection.OpenAsync();

                using (var command = new OracleCommand(query, connection))
                {
                    parameterizeCommand?.Invoke(command);  // Only invoke if parameterizeCommand is not null

                    return await command.ExecuteNonQueryAsync();
                }
            }
        }

    }
}
