﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMS_Core.Data.Data
{
    public interface IAppDbContext
    {
        OracleCommand GetCommand();
        OracleConnection GetConn();

        //SQL Context
        SqlCommand GetSQLCommand();
        SqlConnection GetSQLConnection();

        Task<object> ExecuteScalarAsync(string query);
        object ExecuteScalar(string query);

        void OpenConnection(string dbType = "ORACLE");
        void BeginTransaction(string dbType = "ORACLE");
        void CommitTransaction(string dbType = "ORACLE");
        void RollBackTransaction(string dbType = "ORACLE");
        void CloseConnection(string dbType = "ORACLE");
        OracleConnection OraConnOpen();
        void OraConnClose(OracleConnection connection);
        DataSet FillDataSet(OracleCommand strCmd, string strTableName, DataSet Ds = null);
        Task<DataTable> FillDataThruAdapter(OracleCommand strCmd, object strCol);
        DataTable FillDataThruAdapterSql(string Qry);

        Task<T> ExecuteScalarAsync<T>(string query, Action<OracleCommand> parameterizeCommand = null);
        Task<List<T>> ExecuteReaderAsync<T>(string query, Action<OracleCommand> parameterizeCommand = null) where T : new();
        Task<int> ExecuteNonQueryAsync(string query, Action<OracleCommand> parameterizeCommand = null);
    }
}
