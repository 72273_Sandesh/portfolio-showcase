﻿using HRMS.ModelCL.Login;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.WebAPI.Controllers.Masters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace HRMS_Core.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommanController : ControllerBase
    {
       
        [HttpGet("GetClientIpAddress")]
        public IActionResult GetClientIpAddress()
        {
            string ipAddress = HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault();

            if (string.IsNullOrEmpty(ipAddress))
            {
                ipAddress = HttpContext.Connection.RemoteIpAddress?.ToString();
            }

            // Check if the IP address is IPv6 and convert to IPv4 if needed
            if (ipAddress == "::1")
            {
                ipAddress = Dns.GetHostEntry(Dns.GetHostName())
                               .AddressList
                               .FirstOrDefault(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)?
                               .ToString();
            }

            return Ok(ipAddress);
        }


    }
}
