﻿using HRMS.ModelCL.Login;
using Microsoft.AspNetCore.Mvc;
using HRMS.RepositoryCL.Interface.Login;
using Newtonsoft.Json;
using RMS.ModelCL.Login;
using Newtonsoft.Json.Linq;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using HRMS.RepositoryCL.Concrete.Login;
using HRMS.ModelCL.Payloads;
using HRMS.RepositoryCL.Comman;
using HRMS_Core.WebAPI.RequestHelpers;
using Microsoft.AspNetCore.Authentication;


namespace HRMS_Core.WebAPI.Controllers.Login
{
    [ApiController]
    [Route("api/Auth")]
    public class AuthController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly ILogger<AuthController> _logger;


        public AuthController(IUserRepository userRepository, ILogger<AuthController> logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        [HttpPost("AddRole")]
        public async Task<IActionResult> AddRole([FromBody] Role role)
        {
            try
            {
                var addedRole = await _userRepository.AddRole(role);
                return Ok(addedRole);
            }
            catch (Exception ex)
            {
                _logger.LogError($"An error occurred during login. Request details: {JsonConvert.SerializeObject(role)}, Error: {ex.Message}");
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpPost("AddUser")]
        public async Task<IActionResult> AddUser([FromBody] users user)
        {
            try
            {
                var addedUser = await _userRepository.AddUser(user);
                return Ok(addedUser);
            }
            catch (Exception ex)
            {
                _logger.LogError($"An error occurred during login. Request details: {JsonConvert.SerializeObject(user)}, Error: {ex.Message}");
                return BadRequest("Error adding user");
            }
        }

        [HttpPost("AssignRoleToUser")]
        public async Task<IActionResult> AssignRoleToUser([FromBody] AddUserRole obj)
        {
            try
            {
                var success = await _userRepository.AssignRoleToUser(obj);
                if (success)
                    return Ok(new { Message = "Roles assigned successfully" });
                else
                    return BadRequest(new { Message = "Error assigning roles" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"An error occurred during login. Request details: {JsonConvert.SerializeObject(obj)}, Error: {ex.Message}");
                return BadRequest(new { Message = "Error assigning roles" });
            }
        }


        //[HttpPost("verify_token")]
        //public IActionResult VerifyToken([FromBody] string  token)
        //{
        //    // Extract the token from the Authorization header
        //    //var token = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

        //    if (string.IsNullOrEmpty(token))
        //    {
        //        return Unauthorized("Token is missing");
        //    }

        //    try
        //    {
        //        // Manually validate the token
        //        var tokenHandler = new JwtSecurityTokenHandler();
        //        var key = Encoding.UTF8.GetBytes("your_secret_key");
        //        var validationParameters = new TokenValidationParameters
        //        {
        //            ValidateIssuer = true,
        //            ValidateAudience = true,
        //            ValidateLifetime = true,
        //            ValidateIssuerSigningKey = true,
        //            ValidIssuer = Configuration["Jwt:Issuer"],
        //            ValidAudience = "your_audience",
        //            IssuerSigningKey = new SymmetricSecurityKey(key)
        //        };

        //        SecurityToken validatedToken;
        //        var principal = tokenHandler.ValidateToken(token, validationParameters, out validatedToken);

        //        // Your secure logic here
        //        var userId = principal.FindFirst(ClaimTypes.NameIdentifier)?.Value;

        //        LoginRespDTO resp = new LoginRespDTO
        //       (
        //       token,
        //       DateTime.Now,
        //       "abc123@gmail.com",
        //       DateTime.Now,
        //       "Omkar",
        //       2,
        //       "Varambale",
        //       DateTime.Now
        //       );
        //        return Ok(resp);
        //        //return Ok(new { Message = "This is a secure action", UserId = userId });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Unauthorized("Invalid token: " + ex.Message);
        //    }

        //}
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest loginRequest)
        {
            try
            {

                var response = await _userRepository.Authenticate(loginRequest);
                if (response != null && ((ObjectResult)response).Value != null)
                {
                    var tokenResponse = ((ObjectResult)response).Value as TokenVerifyResponse;

                    if (tokenResponse != null)
                    {
                        // Successfully received TokenVerifyResponse
                        return Ok(tokenResponse);
                    }
                    else
                    {
                        return StatusCode(200, new { Message = "Invalid Credentials !!!", Status = "UnAuthorized" });
                    }
                }
                else
                {
                    _logger.LogError("Error during login");
                    return StatusCode(200, new { Message = "Invalid Credentials !!!", Status = "UnAuthorized" });
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"An error occurred during login. Request details: {JsonConvert.SerializeObject(loginRequest)}, Error: {ex.Message}");
                return StatusCode(500, "Internal Server Error");
            }
        }

        //[HttpPost("Logout")]
        //public async Task<IActionResult> Logout()
        //{
        //    try
        //    {
        //        // Sign out the user
        //        await HttpContext.SignOutAsync();
        //        HttpContext.Session.Clear();
        //        return Ok(new { message = "Logged out successfully" });
        //    }
        //    catch (RepositoryException ex)
        //    {
        //        //return Ok(new { Status = 'E', Message = ex.Message });
        //        return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
        //    }
        //    catch (Exception ex)
        //    {
        //        string controllerName = GetType().Name;
        //        _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
        //        return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
        //    }
        //}

        [HttpPost("RefreshToken")]
        public async Task<IActionResult> RefreshToken(Tokens tokenRequest)
        {
            try
            {
                var isValidRefreshToken = await _userRepository.ValidateRefreshTokenAsync(tokenRequest.api_token, tokenRequest.RefreshToken);

                if (!isValidRefreshToken)
                {
                    //return Unauthorized(new { Message = "Invalid refresh token" });
                    return Ok(new { Status = 'E', Message = "Invalid refresh token" });
                }
                var (newAccessToken, newRefreshToken) = await _userRepository.RefreshTokensAsync(tokenRequest.api_token, tokenRequest.RefreshToken);

                return Ok(new
                {
                    Token = newAccessToken,
                    RefreshToken = newRefreshToken
                });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpPost("GetUserProfileDetails")]
        public async Task<IActionResult> GetUserProfileDetails(string userId)
        {
            try
            {
                var userDet = await _userRepository.GetUsersProfileDet(userId.Trim());

                if (string.IsNullOrEmpty(userDet.Emp_Code))
                {
                    //return Unauthorized(new { Message = "Invalid refresh token" });
                    return Ok(new { Status = 'E', Message = "Invalid userId." });
                }

                return Ok(userDet);

            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpPost("verify_token")]
        public IActionResult VerifyToken([FromBody] Tokens token)
        {
            if (string.IsNullOrEmpty(token.api_token) || string.IsNullOrEmpty(token.RefreshToken))
            {
                return Unauthorized("Token or RefreshToken is missing");
            }

            // Use the repository to verify the token
            var userResponse = _userRepository.VerifyToken(token);
            if (userResponse == null || userResponse.Result.api_token == null || userResponse.Result == null)
            {
                return Unauthorized("Invalid token");
            }

            // Token is valid, return the response with user data
            return Ok(userResponse);
        }


    }

}
