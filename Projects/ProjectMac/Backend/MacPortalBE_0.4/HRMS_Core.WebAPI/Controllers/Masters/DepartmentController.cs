﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Oracle.ManagedDataAccess.Client;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS.RepositoryCL.Concrete.Masters;

using System.Data;
using Microsoft.AspNetCore.Authorization;
using HRMS.ModelCL.Payloads;
using HRMS_Core.WebAPI.RequestHelpers;
using System.Net.NetworkInformation;
using HRMS.ModelCL.Payloads.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL;
using HRMS_Core.Data.Data;
using System.Linq.Expressions;
using System.Diagnostics;
using HRMS.RepositoryCL.Comman;

namespace HRMS_Core.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DepartmentController : ControllerBase
    {

        
        //variable declair
        private readonly AppDbContext _appDbContext;
        private readonly ILogger<DepartmentController> _logger;
        private readonly IDepartmentRepository _DepartmentRepository;

        public DepartmentController(AppDbContext appDbContext, ILogger<DepartmentController> logger, IDepartmentRepository department)
        {
            _appDbContext = appDbContext;
            _logger = logger;
            _DepartmentRepository = department;
        }

        [HttpGet]
        [Route("GetAllDepartments/query")]
        public async Task<IActionResult> GetAllDepartments([FromQuery] PaginationParams paginationParams, string? search = null)
        {
            try
            {
                var (departments, fetchedCount) = await _DepartmentRepository.GetAllDepartments(paginationParams.page, paginationParams.items_per_page, search);

                // Populate payload
                Payload payload = GenPageURL_Helper.GeneratePagination(fetchedCount, paginationParams.page, paginationParams.items_per_page, "Department/GetAllDepartments/query");

                return Ok(new { data = departments, payload });
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E",new List<string?> { ex.Message }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpGet("GetDepartmentByDeptCode/{deptCode}")]
        public async Task<IActionResult> GetDepartmentbyDeptCode(string deptCode)
        {
            try
            {
                List<DeptFetchDepartmentByDeptCodeResponse> departments = await _DepartmentRepository.FetchDepartmentByDeptCode(deptCode);

                if (departments == null || departments.Count == 0)
                {
                    return NotFound();
                }
                // Take the first department as a single object for the response
                DeptFetchDepartmentByDeptCodeResponse department = departments[0];

                DepartmentSingleResponse response = new DepartmentSingleResponse
                {
                    data = department,
                };

                return Ok(response);
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.Message }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpPost("InsertDepartment")]
        public async Task<IActionResult> CreateDepartment([FromBody] Department department)
        {
            try
            {
                if (department == null)
                {
                    var errorMessage = new List<string?> { "Department object is null." };
                    return ResponseGen_Helper.CreateResponse("E", errorMessage, department?.id);
                }

                bool Create=await _DepartmentRepository.CreateDepartment(department);
                if (Create == true)
                {
                    //return Ok(new { data = new { responsrStatus = "S", Message = "Department created successfully", DepartmentCode = department.id } });
                    var successMesssage= new List<string?> { "Department created successfully." };
                    return ResponseGen_Helper.CreateResponse("S", successMesssage, department?.id);
                }
                else
                {
                    //return Ok(new { data = new { responsrStatus = "E", Message = "Department code already exist.", DepartmentCode = department.id }});
                    var errorMesssage = new List<string?> { "Department name already exist." };
                    return ResponseGen_Helper.CreateResponse("E", errorMesssage, department?.id);
                }
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.Message }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpPut("UpdateDepartment/{Code}")]
        public async Task<IActionResult> UpdateDepartment([FromBody] Department department, string Code)
        {
            try
            {
                if (department == null)
                {
                    //return Ok(new { Status = "E", Message = "Department object is null." });
                    var errorMesssage = new List<string?> { "Department object is null." };
                    return ResponseGen_Helper.CreateResponse("E", errorMesssage, department?.id);
                }
                else
                {
                    if (Code != department.id.Trim())
                    {
                        //return Ok(new { Status = "E", Message = "Department code should be macth in request body." });
                        var errorMesssage = new List<string?> { "Department code should be macth in request body." };
                        return ResponseGen_Helper.CreateResponse("E", errorMesssage, department?.id);
                    }
                }

                UpdateFlagResponse updated = _DepartmentRepository.UpdateDepartment(department);

                if (updated.Success != true)
                {
                    if (updated.codeExists == false)
                    {
                        //return Ok(new { Status = "E", Message = "Grade code not found", DepartmentCode = department.id });
                        return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Department code not found" }, department?.id);
                    }
                    if (updated.descrExists == true)
                    {
                        //return Ok(new { Status = "E", Message = "Department name already exist.", DepartmentCode = department.name });
                        return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Department name already exist" }, department?.id);
                    }
                }
                var SuccessMessage = new List<string?> { "Department updated successfully" };
                return ResponseGen_Helper.CreateResponse("S", SuccessMessage, department.id);
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, Code);
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        //**//
        [HttpGet]
        [Route("GetAllDepartmentCombo")]
        public async Task<IActionResult> GetAllDepartmentCombo()
        {
            try
            {
                var deptcombData = await _DepartmentRepository.GetAllDepartmentcombo();
                if (deptcombData == null)
                {
                    return NotFound();
                }

                return Ok(deptcombData);
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage },"");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }




        [HttpGet]
        [Route("GetAll_Accounts")]
        public async Task<IActionResult> GetAll_Accounts()
        {
            try
            {

                List<AcountsModel> lstaccounts = await _DepartmentRepository.GetAll_Accounts();
                if (lstaccounts == null)
                {
                    return NotFound(); 
                }

                var dataWrapper = new DataWrapper<AcountsModel>
                {
                    data = lstaccounts
                };
               
                return Ok(dataWrapper);
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpGet]
        [Route("GetAll_CostCenter")]
        public async Task<IActionResult> GetAll_CostCenter()
        {
            try
            {
                List<CostCenterModel> lstCostCenter = await _DepartmentRepository.GetAll_CostCentre();
                if (lstCostCenter == null)
                {
                    return NotFound();
                }

                var datawrapper = new DataWrapper<CostCenterModel>
                {
                    data = lstCostCenter
                };

                return Ok(datawrapper);
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpGet]
        [Route("GetAll_PayTables")]
        public async Task<IActionResult> GetAll_PayTable()
        {
            try
            {
                List<PayTableModel> lstPaytables = await _DepartmentRepository.GetAll_PayTables();
                if (lstPaytables == null)
                {
                    return NotFound();
                }
                var datawrapper = new DataWrapper<PayTableModel>
                {
                  data= lstPaytables
                };

                return Ok(datawrapper);
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet]
        [Route("GetAll_DepartmentHead")]
        public async Task<IActionResult> GetAll_DepartmentHead()
        {
            try
            {
                List<DepartHead> lstDeaprtmentHeads = await _DepartmentRepository.GetAll_DepartmentHead();
                if (lstDeaprtmentHeads == null)
                {
                    return NotFound();
                }
                var datawrapper = new DataWrapper<DepartHead>
                {
                    data = lstDeaprtmentHeads
                };
                return Ok(datawrapper);
            }
            catch (RepositoryException ex)
            {
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }
    }
}


