﻿using HRMS.ModelCL;
using HRMS.ModelCL.ADMINISTRATION.Masters.Department;
using HRMS.ModelCL.ADMINISTRATION.Masters.Designation;
using HRMS.ModelCL.ADMINISTRATION.Masters.Employee;
using HRMS.ModelCL.ADMINISTRATION.Masters.Grade;
using HRMS.ModelCL.ADMINISTRATION.Masters.Qualification;
using HRMS.ModelCL.Login;
using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Cadre;
using HRMS.ModelCL.Payloads.Designation;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Concrete.Masters;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS_Core.Data.Data;
using HRMS_Core.WebAPI.RequestHelpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using Newtonsoft.Json;
using System.Diagnostics;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HRMS_Core.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DesignationController : ControllerBase
    {
        private readonly ILogger<DesignationController> _logger;
        private readonly IDesignationRepository _designationRepository;

        public DesignationController(ILogger<DesignationController> logger, IDesignationRepository designation )
        {
            _logger = logger;
            _designationRepository = designation;
        }

        // GET: api/<DesignationController>
        [HttpGet]
        [Route("GetAllDesignations")]
        public async Task<IActionResult> GetAllDesignations([FromQuery] PaginationParams paginationParams, string? search = null)
        {
            try
            {
                var (designations, fetchedCount) = await _designationRepository.GetAllDesignations(paginationParams.page, paginationParams.items_per_page, search);

                var dataList = designations.Select(p => new
                {
                    p.Id,
                    p.design_code,
                    p.name,
                    p.LongDesignation,
                    p.Status,
                    p.GradeCode,
                    p.GradeName,
                    p.CadreCode,
                    p.CadreName,
                    p.GradeId,
                    p.ChangeUser,
                    p.ChangeTerm,
                    p.ChangeReason,
                    p.ChangeDate

                }).ToList();


                // Populate payload
                Payload payload = GenPageURL_Helper.GeneratePagination(fetchedCount, paginationParams.page, paginationParams.items_per_page, "Designation/GetAllDesignations/query");
                return Ok(new { data = dataList, payload });

            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Department name already exist" }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("GetDesignationByCode/{code}")]
        public async Task<IActionResult> GetDesignationByCode(string code)
        {
            try
            {
                var desig = await _designationRepository.GetDesignationByCode(code);

                if (desig == null || !desig.Any())
                {
                    return NotFound();
                }
                else
                {
                    var designation = desig.FirstOrDefault();
                    var dataWrapper = new DataWrapperObj<DesignationGetDesignationByCodeResponse>
                    {
                        data = designation
                    };

                    return Ok(dataWrapper); // Return the dataWrapper instance
                }


            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }

        }

        [HttpPost("CreateDesignation")]
        public async Task<IActionResult> CreateDesignation(DesignationModel designation)
        {
            try
            {
                if (designation == null)
                {
                    //return Ok(new { Status = "E", Message = "Designation object is null." });
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Designation object is null." }, designation?.Id);
                }

                bool Create= await _designationRepository.CreateDesignation(designation);

                if (Create == true)
                {
                    //return Ok(new { Status = "S", Message = "Designation created successfully", DesignationCode = designation.Id });

                    var successMessage = new List<string?> { "Designation created successfully" };
                    return ResponseGen_Helper.CreateResponse("S", successMessage,"");
                }
                else
                {
                   //return Ok(new { Status = "E", Message = "Designation name already exist.", gradeCode = designation.Id});
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Designation name already exist." }, designation?.Id);
                }
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.Message });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpPut("UpdateDesignation/{Code}")]
        public async Task<IActionResult> UpdateDesignation([FromBody] DesignationModel designation, string Code)
        {
            try
            {
                if (designation == null)
                {
                    //return Ok(new { Status = "E", Message = "Designation object is null." });
                    return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Designation object is null." },"");
                }
                else
                {
                    if (Code != designation.Id.Trim())
                    {
                        //return Ok(new { Status = "E", Message = "Designation code should be macth in request body." });
                        return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Designation code should be macth in request body." }, "");
                    }
                }

                UpdateFlagResponse updated = await _designationRepository.UpdateDesignation(designation);

                if (updated.Success != true)
                {
                    if (updated.codeExists == false)
                    {
                        //return Ok(new { Status = "E", Message = "Grade code not found", DesignatioCode = designation.Id });
                        return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Designation code not found" }, designation.Id);
                    }
                    if (updated.descrExists == true)
                    {
                        //return Ok(new { Status = "E", Message = "Grade name already exist.", DesignationName = designation.name });
                        return ResponseGen_Helper.CreateResponse("E", new List<string?> { "Designation name already exist." }, designation.Id);
                    }
                }
                //return Ok(new { Status = "S", Message = "Designation Updated successfully", DesignationCode = designation.Id });
                return ResponseGen_Helper.CreateResponse("S", new List<string?> { "Designation Updated successfully." }, designation.Id);

            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred  Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpGet]
        [Route("ddl_Designation_Grade")]
        public async Task<IActionResult> ddl_Designation_Grade()
        {
            try
            {
                List<GradeModel> lstgrades = await _designationRepository.GetGrades();

                if (lstgrades == null)
                {
                    return NotFound();
                }

                var DataWrapperObj = new DataWrapperObj<List<GradeModel>>
                {
                    data = lstgrades
                };

                return Ok(DataWrapperObj);
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

    }
}
