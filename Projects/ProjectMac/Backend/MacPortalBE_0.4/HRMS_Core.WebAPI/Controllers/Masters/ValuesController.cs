﻿using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace YourNamespace.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IpController : ControllerBase
    {
        [HttpGet("GetPrivateIpAddress")]
        public IActionResult GetPrivateIpAddress()
        {
            var localIpAddress = GetLocalIPAddress();
            if (localIpAddress == null)
            {
                return NotFound("Local IP Address not found!");
            }
            return Ok(localIpAddress);
        }

        private string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                // IPv4 filter; you can adjust this based on your needs.
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return null;
        }
    }
}