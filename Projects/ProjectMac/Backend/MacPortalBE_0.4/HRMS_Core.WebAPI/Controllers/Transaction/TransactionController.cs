﻿using HRMS.ModelCL.Payloads;
using HRMS.ModelCL.Payloads.Employee;
using HRMS.ModelCL.Payloads.Employee.Assets;
using HRMS.ModelCL.Payloads.Transaction.Affecting;
using HRMS.ModelCL.Payloads.Transaction.Affecting.employeeTransaction;
using HRMS.ModelCL.Payloads.Transaction.Affecting.MasterTransaction;
using HRMS.RepositoryCL.Comman;
using HRMS.RepositoryCL.Concrete.Masters;
using HRMS.RepositoryCL.Interface.Masters;
using HRMS.RepositoryCL.Interface.Transaction;
using HRMS_Core.WebAPI.Controllers.Masters;
using HRMS_Core.WebAPI.RequestHelpers;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace HRMS_Core.WebAPI.Controllers.Transaction
{
    /*[Route("api/[controller]")]
    [ApiController]*/
    [Authorize]
    public class TransactionController : Controller
    {
        private readonly ILogger<TransactionController> _logger;
        //private readonly ITransactionRepository _affectingRepository;
        private readonly ITransactionRepository _transactionRepository;
        private readonly ITransactionMasterRepository _transactionMasterRepository;


        public TransactionController(ILogger<TransactionController> logger, ITransactionRepository affecting, ITransactionMasterRepository transaction)
        {
            _logger = logger;
            _transactionRepository = affecting;
            _transactionMasterRepository = transaction;
        }


        [HttpGet("ddl_GetTransaction")]
        public async Task<IActionResult> ddl_GetTransaction()
        {
            try
            {
                List<GetTransactionResponse> lsttransactions = await _transactionRepository.ddl_GetTransaction();

                if (lsttransactions == null)
                {
                    return NotFound();
                }


                return Ok(new { Transaction = lsttransactions });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetSections")]
        //public async Task<ActionResult<IEnumerable<GetSectionResponse>>> ddl_GetSections([FromQuery] string siteCode, [FromQuery] string deptCode = null)
        public async Task<IActionResult> ddl_GetSections([FromQuery] string siteCode, [FromQuery] string deptCode = null)
        {
            if (string.IsNullOrEmpty(siteCode))
            {
                return BadRequest("SiteCode is required.");
            }

            try
            {
                List<GetSectionResponse> sections = await _transactionRepository.ddl_GetSections(siteCode, deptCode);
                if (sections == null)
                {
                    return NotFound();
                }
                return Ok(new { Sections = sections });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetLocations")]
        //public async Task<ActionResult<IEnumerable<GetSectionResponse>>> ddl_GetSections([FromQuery] string siteCode, [FromQuery] string deptCode = null)
        public async Task<IActionResult> ddl_GetLocations()
        {
            try
            {
                List<GetLocationResponse> sections = await _transactionRepository.ddl_GetLocations();
                if (sections == null)
                {
                    return NotFound();
                }
                return Ok(new { Sections = sections });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("ddl_GetDepartments")]
        public async Task<IActionResult> GetDepartments()
        {
            try
            {
                List<GetDepartmentResponse> departments = await _transactionRepository.ddl_GetDepartments();
                if (departments == null)
                {
                    return NotFound();
                }
                return Ok(new { Departments = departments });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpPost("InsertMasterTransaction")]
        public async Task<IActionResult> InsertMasterTransactionAsync([FromBody] InsertMasterTransactionRequest request)      
        {
            try
            {
                if (request == null || request.FieldUpdates == null || request.FieldUpdates.Count == 0)
                {
                    return BadRequest("Invalid request data");
                }

                int result = await _transactionMasterRepository.InsertOrUpdateMasterTransactionAsync(
                    request.TableName,
                    request.KeyVar,
                    request.WFLevel,
                    request.Conf,
                    request.SiteCode,
                    request.Emp,
                    request.FilePath,
                    request.File,
                    request.FolderPath,
                    request.strTranId,
                    request.remarks,
                    request.tx_chg_Term,
                    request.FieldUpdates
                );
                
                if (result == -1)
                {
                    var messages = new List<string?> { request.TableName + " name already exists." };
                    return ResponseGen_Helper.CreateResponse("E", messages, null);
                }
                if (result > 0)
                {
                    var messages = new List<string?> { "Transaction inserted successfully." };
                    return ResponseGen_Helper.CreateResponse("S", messages, "");
                }
                else
                {
                    var messages = new List<string?> { "failed to insert transaction." };
                    return ResponseGen_Helper.CreateResponse("S", messages, "");
                }
            }
            catch (Exception ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.Message }, "");
            }         
        }


        // Working for document
        /*[HttpPost("InsertMasterTransaction")]
        public async Task<IActionResult> InsertMasterTransactionAsync([FromForm] MasterTransactionRequest request)
        {
            try
            {
                if (request == null || request.FieldUpdates == null || request.FieldUpdates.Count == 0)
                {
                    return BadRequest("Invalid request data");
                }

                int result = await _transactionMasterRepository.InsertOrUpdateMasterTransactionAsync(request);
                if (result == -1)
                {
                    var messages = new List<string?> { request.TableName + " name already exists." };
                    return ResponseGen_Helper.CreateResponse("E", messages, null);
                }
                if (result > 0)
                {
                    var messages = new List<string?> { "Transaction inserted successfully." };
                    return ResponseGen_Helper.CreateResponse("S", messages, "");
                }
                else
                {
                    var messages = new List<string?> { "failed to insert transaction." };
                    return ResponseGen_Helper.CreateResponse("S", messages, "");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }*/


        [HttpPost("InsertOrUpdateEmployeeTransaction")]
        public async Task<IActionResult> InsertOrUpdateEmployeeTransactionAsync([FromBody] dynamic request, string formName)
        {
            try
            {
               

                // Call the service method to handle the transaction
                int result = await _transactionMasterRepository.InsertOrUpdateEmployeeTransactionAsync( request, formName);

                // Handle result and generate appropriate response
                if (result == -1)
                {
                    var messages = new List<string?> { "Failed to update or insert employee transaction." };
                    return ResponseGen_Helper.CreateResponse("E", messages, null);
                }
                if (result > 0)
                {
                    var messages = new List<string?> { "Employee transaction processed successfully." };
                    return ResponseGen_Helper.CreateResponse("S", messages, "");
                }
                else
                {
                    var messages = new List<string?> { "Failed to process employee transaction." };
                    return ResponseGen_Helper.CreateResponse("E", messages, "");
                }
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        // GET: api/Employee/{empCode}/existing-values
        [HttpGet("{empCode}/existing-values")]
        public async Task<IActionResult> GetExistingValues(string empCode)
        {
            if (string.IsNullOrEmpty(empCode))
            {
                return BadRequest("Employee code cannot be null or empty.");
            }

            try
            {
                Dictionary<string, string> existingValues = await _transactionMasterRepository.GetExistingValuesAsync(empCode);

                if (existingValues == null || existingValues.Count == 0)
                {
                    return NotFound("No existing values found for the provided employee code.");
                }

                return Ok(existingValues);
            }
            catch (Exception ex)
            {
                // Log the exception
                Console.WriteLine($"Error: {ex.Message}");
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }

        // Get All Transaction

        [HttpGet("GetAllTransaction")]
        public async Task<IActionResult> GetAll([FromQuery] PaginationParams paginationParams, [FromQuery] string? search, [FromQuery] string sessionEmpCode, [FromQuery] DateOnly? period, [FromQuery] string category, [FromQuery] string status, [FromQuery] string txMode)
        {
            try
            {
                // Retrieve cadres and total cadre count
                var (transactions, totalTransactionCount) = await _transactionRepository.GetAllTransactions(paginationParams.page, paginationParams.items_per_page, search, sessionEmpCode, period, category, status, txMode);

                // Generate pagination payload
                Payload payload = GenPageURL_Helper.GeneratePagination(totalTransactionCount, paginationParams.page, paginationParams.items_per_page, "Transaction/GetAllTransaction/query");

                return Ok(new { data = transactions, payload });

            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }




        [HttpGet("GetTransactionByID")]
        public async Task<IActionResult> GetTransactionByID(string tranId)
        {
            try
            {
                List<MastersUnconfWithDetailsForID> transactions = await _transactionRepository.GetTransactionByID(tranId);

                if (transactions == null)
                {
                    return NotFound();
                }


                return Ok(new { transactions = transactions });
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpPost("reject")]
        public async Task<IActionResult> RejectTransaction([FromBody] RejectTransactionRequest request)
        {
            try
            {
                await _transactionMasterRepository.RejectTransaction(request);
                var messages = new List<string?> { "Transaction rejected successfully." };
                return ResponseGen_Helper.CreateResponse("S", messages, "");
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpPost("approve")]
        public async Task<IActionResult> ApproveTransaction(string tranId, string ipAddress, string empCode, string SessionempCode, string tableName, int wfProcLevel, string moduleType, string remarks)
        {

            try
            {
                // Call the service method to approve the transaction
                await _transactionMasterRepository.ApproveTransaction(tranId, ipAddress, empCode, SessionempCode, tableName, wfProcLevel, moduleType, remarks);

                var messages = new List<string?> { "Transaction approved successfully." };
                return ResponseGen_Helper.CreateResponse("S", messages, "");

            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        // Generate Insert/update Query for masters

        [HttpGet("GetDetailsToInsert")]
        public async Task<IActionResult> GetTransactionDetails(string tranId, string tableName)
        {
            try
            {
                string insertQueries = await _transactionRepository.GetTransactionDetails(tranId, tableName);
                return Ok(insertQueries);

            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("fnGetMinSanctionLevel")]
        public async Task<IActionResult> fnGetMinSanctionLevel([FromQuery] string empCode, [FromQuery] string moduleType)
        {
            try
            {
                if (string.IsNullOrEmpty(empCode) || string.IsNullOrEmpty(moduleType))
                {
                    return BadRequest("Invalid parameters.");
                }

                var result = await _transactionRepository.FnGetMinSanctionLevel(empCode, moduleType);

                return Ok(result);
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("fnGetCurrentSanctionAuthority")]
        public async Task<IActionResult> fnGetCurrentSanctionLevel([FromQuery] string empCode, [FromQuery] string moduleType, [FromQuery] double seqNo)
        {
            try
            {
                if (string.IsNullOrEmpty(empCode) || string.IsNullOrEmpty(moduleType))
                {
                    return BadRequest("Invalid parameters.");
                }

                var result = await _transactionRepository.FnGetCurrentSanctionAuthority(empCode, moduleType, seqNo);

                return Ok(result);
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }


        [HttpGet("fnGetMaximunSanctionAuthority")]
        public async Task<IActionResult> fnGetMaximunSanctionLevel([FromQuery] string empCode, [FromQuery] string moduleType)
        {
            try
            {
                if (string.IsNullOrEmpty(empCode) || string.IsNullOrEmpty(moduleType))
                {
                    return BadRequest("Invalid parameters.");
                }

                var result = _transactionRepository.FnGetMaximunSanctionAuthority(empCode, moduleType);

                return Ok(result);
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpGet("add-masters-unconf")]
        public async Task<IActionResult> AddMastersUnconf(
        [FromQuery] string empCode, [FromQuery] string tranIdEmpDet, [FromQuery] string remoteAddress, [FromQuery] string Remarks)


        {
            if (string.IsNullOrEmpty(empCode) || string.IsNullOrEmpty(tranIdEmpDet))
            {
                return BadRequest("Invalid input data.");
            }

            try
            {
                List<string> lst = new List<string>();
                await _transactionRepository.FnAddMastersUnconfReviewer(empCode, tranIdEmpDet, remoteAddress, Remarks, lst);
                return Ok("Transaction added successfully.");
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }

        [HttpPost("fnGetMstGen")]
        // public async Task<IActionResult> fnGetMstGen([FromBody] List<T> fields)
        public IActionResult ProcessRequest([FromBody] dynamic jsonData)
        {
            try
            {
                //if (string.IsNullOrEmpty(empCode) || string.IsNullOrEmpty(moduleType))
                //{
                //    return BadRequest("Invalid parameters.");
                //}
                //var name = jsonData.name;
                var jsonObject = JsonConvert.DeserializeObject<JObject>(jsonData.ToString());
                string DESCR = jsonObject["DESCR"]?.ToString() ?? "";

                //if array
                //var jsonArray = JsonConvert.DeserializeObject<JArray>(jsonData.ToString());
                //var items = jsonArray.Select(item => item.ToObject<JObject>()).ToList();
                //if array


                //dynamic obj = JsonConvert.DeserializeObject<dynamic>(jsonData);
                //var dynamicObject = JsonConvert.DeserializeObject<object>(jsonData);
                //var exampleValue = jsonData.exampleProperty;
                //var jsonArray = JsonConvert.SerializeObject(fields);
                //var items = JsonConvert.DeserializeObject<List<dynamic>>(jsonArray);

                //string jsonArray = "[{\"cadrE_CODE\": \"C00221\",\"descr\": \"DESR\",\"sH_DESCR\": \"TRST\",\"lvE_TBLNO\": \"string\",\"chG_DATE\": \"2024-08-23T14:20:29.462Z\",\"chG_USER\": \"string\",\"chG_TERM\": \"string\",\"udF_NUM1\": 0,\"cadrE_ID\": \"string\",\"cadrE_ORDER\": 0,\"banD_CODE\": \"string\",\"mkit\": 0,\"mmed\": 0,\"chG_REASON\": \"string\",\"status\": \"string\"}]";
                //List<T> items = JsonSerializer.Deserialize<List<T>>(jsonArray);
                //List<CadreModel> cadreResponses = fields.OfType<CadreModel>().ToList();

                //var fields1=fields;
                string s_MOD_NAME = "";
                string s_TRAN_MODE = ""; string s_KEYFIELDVAL = "";
                var result = _transactionMasterRepository.fnGetGenMstAsync(s_MOD_NAME, jsonObject, s_TRAN_MODE, s_KEYFIELDVAL);
                return Ok();
            }
            catch (RepositoryException ex)
            {
                //return Ok(new { Status = 'E', Message = ex.ErrorMessage });
                return ResponseGen_Helper.CreateResponse("E", new List<string?> { ex.ErrorMessage }, "");
            }
            catch (Exception ex)
            {
                string controllerName = GetType().Name;
                _logger.LogInformation($"An error occurred Error in:{controllerName}- {ex.Message}");
                return StatusCode(500, $"An error occurred while processing the request:{ex.Message}");
            }
        }
    }
}
