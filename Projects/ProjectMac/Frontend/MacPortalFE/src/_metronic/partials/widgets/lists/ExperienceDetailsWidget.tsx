import React, { useState, useEffect } from "react";
import { KTIcon } from "../../../helpers";
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";
import {
  employeeIdForView,
  setFMIdForUpdate,
} from "../../../../app/modules/GlobalQuery";

type Props = {
  className: string;
};

type Qualification = {
  seQ_NO: number;
  froM_DATE: Date;
  tO_DATE: Date;
  organisation: string;
  lasT_DESIGNATION: string;
  curR_CODE: string;
  grosS_AMT: string;
  chG_USER: string;
  chG_DATE: Date | null;
  chG_TERM: string;
  department: string;
  worK_LOCATION: string;
  seP_REASON: string;
  state: string;
  country: string;
};

const ExperienceDetailsWidget: React.FC<Props> = ({ className }) => {
  const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
  const [qualifications, setQualifications] = useState<Qualification[]>([]);
  const navigate = useNavigate();

  const formattedDate = (date: Date) => {
    return new Date(date).toLocaleString("en-GB", {
      day: "2-digit",
      month: "2-digit",
      year: "numeric",
    });
  };

  const getYear = (date: Date) => {
    return new Date(date).toLocaleString("en-GB", {
      year: "numeric",
    });
  };

  const yearsOfExp = (strtDate: Date, endDate: Date): string => {
    const startDate = new Date(strtDate);
    const cmpltDate = new Date(endDate);
    const differenceMs = Math.abs(startDate.getTime() - cmpltDate.getTime());
    const years = Math.floor(differenceMs / (1000 * 60 * 60 * 24 * 365.25));
    const months = Math.floor(
      (differenceMs % (1000 * 60 * 60 * 24 * 365.25)) /
        (1000 * 60 * 60 * 24 * 30.4375)
    );

    if (years === 0) {
      return `${months} MONTH${months !== 1 ? "S" : ""}`;
    } else if (months === 0) {
      return `${years}.${years !== 1 ? "S" : ""}`;
    } else {
      return `${years}.${years !== 1 ? "" : ""} ${months} YEAR${
        months !== 1 ? "S" : ""
      }`;
    }
  };

  let totalExperienceMonths = 0;

  qualifications.forEach((qualification) => {
    const fromDate = new Date(qualification.froM_DATE);
    const toDate = new Date(qualification.tO_DATE);

    const diffYears = toDate.getFullYear() - fromDate.getFullYear();
    const diffMonths = toDate.getMonth() - fromDate.getMonth();

    totalExperienceMonths += diffYears * 12 + diffMonths;
  });

  const totalExperienceYears = Math.floor(totalExperienceMonths / 12);
  const remainingMonths = totalExperienceMonths % 12;

  const formattedTotalExperience = `${totalExperienceYears}.${remainingMonths} YEAR${
    remainingMonths !== 1 ? "S" : ""
  }`;

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPEXP`)
      .then((response) => {
        setQualifications(response.data);
      })
      .catch((error) => {
        console.error("Error fetching qualification data:", error);
      });
  }, []);

  const editExperience = (seqNo: number) => {
    setFMIdForUpdate(seqNo - 1);
    navigate("/crafted/pages/profile/EditExperienceDetailsWrapper");
  };

  return (
    <div className={`card ${className}`}>
      {/* begin::Header */}
      <div className="card-header bg-light-primary align-items-center border-0 mt-0">
        <h3 className="card-title align-items-start flex-column">
          <span className="fw-bold mb-2 text-gray-900">Experience Details</span>
        </h3>
        <div className="d-flex my-2">
          <div className="d-flex align-items-center position-relative me-4">
            <Link
              to="/crafted/pages/profile/AddExperienceDetailsWrapper"
              type="button"
              className="btn btn-primary btn-sm"
              data-kt-menu-trigger="click"
              data-kt-menu-placement="bottom-end"
              data-kt-menu-flip="top-end"
            >
              Add
            </Link>
          </div>
        </div>
      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className="card-body pt-1">
        <div className="card-body position-relative" id="kt_activities_body">
          <div
            id="kt_activities_scroll"
            className="position-relative ms-n10 me-n10 pe-1"
            data-kt-scroll="true"
            data-kt-scroll-height="auto"
            data-kt-scroll-wrappers="#kt_activities_body"
            data-kt-scroll-dependencies="#kt_activities_header, #kt_activities_footer"
            data-kt-scroll-offset="5px"
          >
            <div className="d-flex flex-wrap align-items-center justify-content-center fw-bold mb-1 pe-2">
              <div className="border border-gray-300 rounded bg-light-warning  min-w-50px py-1 px-2 me-3 mb-5">
                <div className="card-toolbar d-flex align-items-center">
                  TOTAL EXPERIENCE : {formattedTotalExperience}
                </div>
              </div>
            </div>
            <div className="timeline">
              {qualifications.map((qualification) => (
                <div key={qualification.seQ_NO} className="timeline-item ">
                  <div
                    className="timeline-line w-40px"
                    style={{ paddingLeft: "100px" }}
                  ></div>
                  <div className="timeline-label fw-bold pt-3 text-gray-800 fs-6">
                    {getYear(qualification.froM_DATE)}
                  </div>
                  <div className="timeline-badge">
                    <div className="timeline-icon symbol symbol-circle symbol-30px">
                      <div className="symbol-label bg-light">
                        <KTIcon
                          iconName="briefcase"
                          className="fs-2 text-primary"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="timeline-content mb-0 mt-n1">
                    {" "}
                    {/* Reduced mb-5 to mb-3 */}
                    <div className="pe-3 mb-0">
                      {" "}
                      {/* Reduced mb-5 to mb-3 */}
                      <div className="fs-5 fw-bold mt-3">
                        {qualification.organisation}
                      </div>
                      <div className="d-flex flex-wrap fw-bold mb-1 pe-1">
                        <div className="border border-gray-300 rounded min-w-50px py-1 px-2 me-3 mb-1 mt-1 ">
                          {" "}
                          <div className="card-toolbar ">
                            <KTIcon
                              iconName="calendar"
                              className="text-success fs-1 me-2"
                            />
                            {formattedDate(qualification.froM_DATE)} {"---> "}
                            {formattedDate(qualification.tO_DATE)}
                          </div>
                        </div>
                        <div className="d-flex align-items-center justify-content-center text-dark-500 flex-grow-1">
                          {qualification.seQ_NO !== 1 && (
                            <div className="border border-gray-300 rounded min-w-50px py-1 px-2 me-3 mb-1 mt-1">
                              <div className="card-toolbar ">
                                <KTIcon
                                  iconName="graph-up"
                                  className="text-success fs-1 me-2"
                                />
                                GROWTH : XX %
                              </div>
                            </div>
                          )}
                        </div>
                        <div className="d-flex justify-content-center text-dark-500 flex-grow-1 ml-auto">
                          <div className="border border-gray-300 rounded min-w-50px py-1 px-2 me-3 mb-1 mt-1">
                            {" "}
                            {/* Reduced mb-5 to mb-3 */}
                            <div className="fw-bold fs-6 text-dark-500">
                              {yearsOfExp(
                                qualification.froM_DATE,
                                qualification.tO_DATE
                              )}
                            </div>
                          </div>
                        </div>
                        <div className="fw-bold mb-1 d-flex  justify-content-between">
                          <button
                            className="btn btn-primary btn-sm"
                            onClick={() => editExperience(qualification.seQ_NO)}
                          >
                            Edit
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="d-flex align-items-center border border-dashed border-gray-300 rounded min-w-100px">
                      <div className="card-body">
                        {/* Begin:first Row */}
                        <div className="row">
                          <div className="col-lg-6 mb-2">
                            <label
                              className="col-lg-5 fw-bold text-muted "
                              style={{ minWidth: "150px" }}
                            >
                              DEPARTMENT :
                            </label>
                            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                              {qualification.department}
                            </span>
                          </div>
                          <div className="col-lg-6 mb-2">
                            <label
                              className="col-lg-5 fw-bold text-muted "
                              style={{ minWidth: "150px" }}
                            >
                              DESIGNATION :
                            </label>
                            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                              {qualification.lasT_DESIGNATION}
                            </span>
                          </div>
                        </div>
                        {/* End:first Row */}
                        {/* Begin: Second Row */}
                        <div className="row">
                          <div className="col-lg-6 mb-2">
                            <label
                              className="col-lg-5 fw-bold text-muted "
                              style={{ minWidth: "150px" }}
                            >
                              WORK LOCATION :
                            </label>
                            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                              {qualification.worK_LOCATION}
                            </span>
                          </div>
                          <div className="col-lg-6 mb-2">
                            <label
                              className="col-lg-5 fw-bold text-muted "
                              style={{ minWidth: "150px" }}
                            >
                              GROSS SALARY :
                            </label>
                            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                              {qualification.curR_CODE}.
                              {qualification.grosS_AMT}/-
                            </span>
                          </div>
                        </div>
                        {/* End: Second Row */}

                        {/* Begin: Second Row */}
                        <div className="row">
                          <div className="col-lg-6 mb-2">
                            <label
                              className="col-lg-5 fw-bold text-muted "
                              style={{ minWidth: "150px" }}
                            >
                              STATE :
                            </label>
                            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                              {qualification.state}
                            </span>
                          </div>
                          <div className="col-lg-6 mb-2">
                            <label
                              className="col-lg-5 fw-bold text-muted "
                              style={{ minWidth: "150px" }}
                            >
                              COUNTRY :
                            </label>
                            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                              {qualification.country}
                            </span>
                          </div>
                        </div>
                        {/* End: Second Row */}

                        {/* Begin: Second Row */}
                        <div className="row">
                          <div className="col-lg-6 mb-2">
                            <label
                              className="col-lg-5 fw-bold text-muted "
                              style={{ minWidth: "150px" }}
                            >
                              JOINING CTC :
                            </label>
                            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                              0000 /-
                            </span>
                          </div>
                          <div className="col-lg-6 mb-2">
                            <label
                              className="col-lg-5 fw-bold text-muted "
                              style={{ minWidth: "150px" }}
                            >
                              JOINING DESIGNATION :
                            </label>
                            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                              0000 /-
                            </span>
                          </div>
                        </div>
                        {/* End: Second Row */}

                        {/* Begin:first Row     */}
                        <div className="row">
                          <div className="col-lg-6 mb-2">
                            <label
                              className="col-lg-5 fw-bold text-muted "
                              style={{ minWidth: "150px" }}
                            >
                              REASON OF SEPARATIONS :
                            </label>
                            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                              {qualification.seP_REASON}
                            </span>
                          </div>
                          <div className="col-lg-6 mb-2">
                            <label
                              className="col-lg-5 fw-bold text-muted "
                              style={{ minWidth: "150px" }}
                            >
                              CHANGE USER :
                            </label>
                            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                              {qualification.chG_USER}
                            </span>
                          </div>
                        </div>
                        {/* End:first Row     */}

                        {/* Begin:first Row     */}
                        <div className="row">
                          <div className="col-lg-6 mb-2">
                            <label
                              className="col-lg-5 fw-bold text-muted "
                              style={{ minWidth: "150px" }}
                            >
                              CHANGE DATE :
                            </label>
                            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                              {qualification.chG_DATE
                                ? formattedDate(qualification.chG_DATE)
                                : "NOT CHANGED"}
                            </span>
                          </div>
                          <div className="col-lg-6 mb-2">
                            <label
                              className="col-lg-5 fw-bold text-muted "
                              style={{ minWidth: "150px" }}
                            >
                              CHANGE TERM :
                            </label>
                            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                              {qualification.chG_TERM}
                            </span>
                          </div>
                        </div>
                        {/* End:first Row     */}
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
      {/* end: Card Body */}
    </div>
  );
};

export { ExperienceDetailsWidget };
