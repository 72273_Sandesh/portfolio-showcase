// import { FC, useEffect, useState } from "react";
// import { KTIcon, toAbsoluteUrl } from "../../../helpers";
// import { useNavigate } from "react-router-dom";
// import { FileUpload } from "./FileUpload";
// import axios from "axios";
// import { employeeIdForView } from "../../../../app/modules/GlobalQuery";

// type Props = {
//   className: string;
// };
// type AttachmentDetails = {
//   DOCUMENTS: string;
//   STORAGE_CODE__PAR: string;
//   STORAGE_SECURITY: string; 
//   STORAGE_PATH: string;
//   DOC_ID: string;
//   DOCUMENT_LINK: string; 
//   DOC_PATH: string;
//   DOC_TYPE: string;
//   HasDocID: boolean; 
  
// };

//  const EISDocs: FC<Props> = ({ className }) => {
//   const navigate = useNavigate();
//   const handleClose = () => {
//     navigate(-1);
//   };
//   const [attachmentDetails, setAttachmentDetails] = useState<AttachmentDetails[]>([]);
//   const EMPLOYEE_URL =import.meta.env.VITE_EMPLOYEE_API_URL;
//   useEffect(() => {
//     axios 
//       .get(`${EMPLOYEE_URL}/Attachment/${employeeIdForView}?strCndCode=CD00031458&attachMode=EIS`)
//       .then((response) => {
//         setAttachmentDetails(response.data);
//       })
//       .catch((error) => {
//         console.error("Error fetching asset details:", error);
//       });
//   }, [EMPLOYEE_URL]);
//   const formattedDate = (date: Date) => {
//     return new Date(date).toLocaleString("en-GB", {
//       day: "2-digit",
//       month: "2-digit",
//       year: "numeric",
//       hour: "2-digit",
//       minute: "2-digit",
//       second: "2-digit",
//     });
//   };
//   return (
//     <div className={`card ${className}`}>
//       {/* begin::Header */}
//       <div className="card-header border-0 pt-5">
//         <h3 className="card-title align-items-start flex-column">
//           <span className="card-label fw-bold fs-3 mb-1">
//             EIS Documents
//           </span>
//           {/* <span className='text-muted mt-1 fw-semibold fs-7'>Over 500 members</span> */}
//         </h3>
//         <div
//           className="card-toolbar"
//           data-bs-toggle="tooltip"
//           data-bs-placement="top"
//           data-bs-trigger="hover"
//           title="Click to add a user"
//         >
//           <div className="d-flex my-2">
//             <div className="d-flex align-items-center position-relative me-4">
//               {/* begin::Menu */}
//               <button
//                 type="button"
//                 className="btn btn-sm btn-primary me-3"
//                 data-kt-menu-trigger="click"
//                 data-kt-menu-placement="bottom-end"
//                 data-kt-menu-flip="top-end"
//               >
//                 <KTIcon iconName="plus" className="fs-2" />
//                 Add
//               </button>
//               <FileUpload />
//               {/* end::Menu */}
//             </div>

//             <a onClick={handleClose} className="btn btn-danger btn-sm">
//               close
//             </a>
//           </div>
//         </div>
//       </div>
//       {/* end::Header */}
//       {/* begin::Body */}
//       <div className="card-body py-3">
//         {/* begin::Table container */}
//         <div className="table-responsive">
//           {/* begin::Table */}
//           <table className="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
//             {/* begin::Table head */}
//             <thead>
//               <tr className="fw-bold text-muted">
//                 <th className="min-w-150px">Document Name</th>
//                 <th className="min-w-140px">Type and Size</th>
//                 <th className="min-w-120px">Last Updated</th>
//                 <th className="min-w-100px text-end">Actions</th>
//               </tr>
//             </thead>
//             {/* end::Table head */}
//             {/* begin::Table body */}
//             <tbody>
//             {attachmentDetails.map((attachment) => (
//               <tr>
//                 <td>
//                   <div className="d-flex align-items-center">
//                     <div className="symbol symbol-45px me-5">
//                       <img
//                         src={toAbsoluteUrl("media/svg/files/doc.svg")}
//                         alt=""
//                       />
//                     </div>
//                     <div className="d-flex justify-content-start flex-column">
//                       <a
//                         href="#"
//                         className="text-gray-900 fw-bold text-hover-primary fs-6"
//                       >
//                         {attachment.DOCUMENTS}
//                       </a>
//                     </div>
//                   </div>
//                 </td>
//                 <td>
//                   <a
//                     href="#"
//                     className="text-gray-900 fw-bold text-hover-primary d-block fs-6"
//                   >
//                     PDF
//                   </a>
//                   <span className="text-muted fw-semibold text-muted d-block fs-7">
//                     Size: 569476 bytes
//                   </span>
//                 </td>
//                 <td className="text-end">
//                   <div className=" fw-bolder d-flex align-items-right">
//                     <div className="badge badge-light fw-bolder d-flex align-items-right">
//                       {formattedDate("2012-04-02T00:00:00")}
//                     </div>
//                   </div>
//                 </td>
//                 <td>
//                   <div className="d-flex justify-content-end flex-shrink-0">
//                     <a
//                       href="#"
//                       className="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1"
//                     >
//                       <KTIcon iconName="pencil" className="fs-3" />
//                     </a>
//                     <a
//                       href="#"
//                       className="btn btn-icon btn-bg-light btn-active-color-primary btn-sm"
//                     >
//                       <KTIcon iconName="trash" className="fs-3" />
//                     </a>
//                   </div>
//                 </td>
//               </tr>))}
              
//             </tbody>
//             {/* end::Table body */}
//           </table>
//           {/* end::Table */}
//         </div>
//         {/* end::Table container */}
//       </div>
//       {/* begin::Body */}
//     </div>
//   );
// };

// export { EISDocs };



// import { FC, useEffect, useState } from "react";
// import { KTIcon, toAbsoluteUrl } from "../../../helpers";
// import { useNavigate } from "react-router-dom";
// import axios from "axios";
// import { employeeIdForView } from "../../../../app/modules/GlobalQuery";

// type Props = {
//   className: string;
// };

// type AttachmentDetails = {
//   DOCUMENTS: string;
//   STORAGE_CODE__PAR: string;
//   STORAGE_SECURITY: string;
//   STORAGE_PATH: string;
//   DOC_ID: string;
//   DOCUMENT_LINK: string;
//   DOC_PATH: string;
//   DOC_TYPE: string;
//   HasDocID: boolean;
// };

// const EISDocs: FC<Props> = ({ className }) => {
//   const navigate = useNavigate();
//   const handleClose = () => {
//     navigate(-1);
//   };

//   const [attachmentDetails, setAttachmentDetails] = useState<AttachmentDetails[]>([]);
//   const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

//   useEffect(() => {
//     axios
//       .get(`${EMPLOYEE_URL}/Attachment/${employeeIdForView}?strCndCode=CD00031458&attachMode=EIS`)
//       .then((response) => {
//         setAttachmentDetails(response.data);
//       })
//       .catch((error) => {
//         console.error("Error fetching asset details:", error);
//       });
//   }, [EMPLOYEE_URL]);

//   const formattedDate = (date: Date) => {
//     return new Date(date).toLocaleString("en-GB", {
//       day: "2-digit",
//       month: "2-digit",
//       year: "numeric",
//       hour: "2-digit",
//       minute: "2-digit",
//       second: "2-digit",
//     });
//   };

//   const handleDownload = (attachment: AttachmentDetails) => {
//     const documentLink = `${attachment.STORAGE_PATH}/${attachment.DOCUMENTS}`;

//     // Create a temporary anchor element to trigger the download
//     const anchorElement = document.createElement("a");
//     anchorElement.href = documentLink;
//     anchorElement.download = attachment.DOCUMENTS;
//     document.body.appendChild(anchorElement);
//     anchorElement.click(); // Trigger the download
//     document.body.removeChild(anchorElement);
//   };

//   return (
//     <div className={`card ${className}`}>
//       {/* begin::Header */}
//       <div className="card-header border-0 pt-5">
//         <h3 className="card-title align-items-start flex-column">
//           <span className="card-label fw-bold fs-3 mb-1">EIS Documents</span>
//         </h3>
//         <div
//           className="card-toolbar"
//           data-bs-toggle="tooltip"
//           data-bs-placement="top"
//           data-bs-trigger="hover"
//           title="Click to add a user"
//         >
//           <div className="d-flex my-2">
//             <div className="d-flex align-items-center position-relative me-4">
//               <button
//                 type="button"
//                 className="btn btn-sm btn-primary me-3"
//                 data-kt-menu-trigger="click"
//                 data-kt-menu-placement="bottom-end"
//                 data-kt-menu-flip="top-end"
//               >
//                 <KTIcon iconName="plus" className="fs-2" />
//                 Add
//               </button>
//               {/* Placeholder for the file upload component */}
//               {/* <FileUpload /> */}
//             </div>
//             <button onClick={handleClose} className="btn btn-danger btn-sm">
//               Close
//             </button>
//           </div>
//         </div>
//       </div>
//       {/* end::Header */}
//       {/* begin::Body */}
//       <div className="card-body py-3">
//         {/* begin::Table container */}
//         <div className="table-responsive">
//           {/* begin::Table */}
//           <table className="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
//             {/* begin::Table head */}
//             <thead>
//               <tr className="fw-bold text-muted">
//                 <th className="min-w-150px">Document Name</th>
//                 <th className="min-w-140px">Type and Size</th>
//                 <th className="min-w-120px">Last Updated</th>
//                 <th className="min-w-100px text-end">Actions</th>
//               </tr>
//             </thead>
//             {/* end::Table head */}
//             {/* begin::Table body */}
//             <tbody>
//               {attachmentDetails.map((attachment, index) => (
//                 <tr key={index}>
//                   <td>
//                     <div className="d-flex align-items-center">
//                       <div className="symbol symbol-45px me-5">
//                         <img
//                           src={toAbsoluteUrl("media/svg/files/doc.svg")}
//                           alt=""
//                         />
//                       </div>
//                       <div className="d-flex justify-content-start flex-column">
//                         <span className="text-gray-900 fw-bold text-hover-primary fs-6">
//                           {attachment.DOCUMENTS}
//                         </span>
//                       </div>
//                     </div>
//                   </td>
//                   <td>
//                     <span className="text-gray-900 fw-bold text-hover-primary d-block fs-6">
//                       PDF
//                     </span>
//                     <span className="text-muted fw-semibold text-muted d-block fs-7">
//                       Size: 569476 bytes
//                     </span>
//                   </td>
//                   <td className="text-end">
//                     <div className="fw-bolder d-flex align-items-right">
//                       <div className="badge badge-light fw-bolder d-flex align-items-right">
//                         {formattedDate(new Date("2012-04-02T00:00:00"))}
//                       </div>
//                     </div>
//                   </td>
//                   <td>
//                     <div className="d-flex justify-content-end flex-shrink-0">
//                       <button
//                         onClick={() => handleDownload(attachment)}
//                         className="btn btn-icon btn-bg-light btn-active-color-primary btn-sm"
//                       >
//                         Download
//                       </button>
//                     </div>
//                   </td>
//                 </tr>
//               ))}
//             </tbody>
//             {/* end::Table body */}
//           </table>
//           {/* end::Table */}
//         </div>
//         {/* end::Table container */}
//       </div>
//       {/* end::Body */}
//     </div>
//   );
// };

// export { EISDocs };


import { FC, useEffect, useState } from "react";
import { KTIcon, toAbsoluteUrl } from "../../../helpers";
import { useNavigate } from "react-router-dom";
import { FileUpload } from "./FileUpload";
import axios from "axios";
import { employeeIdForView } from "../../../../app/modules/GlobalQuery";

type Props = {
  className: string;
};

type AttachmentDetails = {
  DOCUMENTS: string;
  STORAGE_CODE__PAR: string;
  STORAGE_SECURITY: string;
  STORAGE_PATH: string;
  DOC_ID: string;
  DOCUMENT_LINK: string;
  DOC_PATH: string;
  DOC_TYPE: string;
  HasDocID: boolean;
};

const EISDocs: FC<Props> = ({ className }) => {
  const navigate = useNavigate();
  const handleClose = () => {
    navigate(-1);
  };
  const [attachmentDetails, setAttachmentDetails] = useState<AttachmentDetails[]>([]);
  const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/Attachment/${employeeIdForView}?strCndCode=CD00031458&attachMode=EIS`)
      .then((response) => {
        setAttachmentDetails(response.data);
      })
      .catch((error) => {
        console.error("Error fetching asset details:", error);
      });
  }, [EMPLOYEE_URL]);

  const formattedDate = (date: Date) => {
    return new Date(date).toLocaleString("en-GB", {
      day: "2-digit",
      month: "2-digit",
      year: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    });
  };

  return (
    <div className={`card ${className}`}>
      {/* begin::Header */}
      <div className="card-header border-0 pt-5">
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bold fs-3 mb-1">EIS Documents</span>
        </h3>
        <div
          className="card-toolbar"
          data-bs-toggle="tooltip"
          data-bs-placement="top"
          data-bs-trigger="hover"
          title="Click to add a user"
        >
          <div className="d-flex my-2">
            <div className="d-flex align-items-center position-relative me-4">
              <button
                type="button"
                className="btn btn-sm btn-primary me-3"
                data-kt-menu-trigger="click"
                data-kt-menu-placement="bottom-end"
                data-kt-menu-flip="top-end"
              >
                <KTIcon iconName="plus" className="fs-2" />
                Add
              </button>
              <FileUpload />
            </div>
            <a onClick={handleClose} className="btn btn-danger btn-sm">
              close
            </a>
          </div>
        </div>
      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className="card-body py-3">
        {/* begin::Table container */}
        <div className="table-responsive">
          {/* begin::Table */}
          <table className="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
            {/* begin::Table head */}
            <thead>
              <tr className="fw-bold text-muted">
                <th className="min-w-150px">Document Name</th>
                <th className="min-w-140px">Type and Size</th>
                <th className="min-w-120px">Last Updated</th>
                <th className="min-w-100px text-end">Actions</th>
              </tr>
            </thead>
            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
              {attachmentDetails.map((attachment) => (
                <tr key={attachment.DOC_ID}>
                  <td>
                    <div className="d-flex align-items-center">
                      <div className="symbol symbol-45px me-5">
                        <img
                          src={toAbsoluteUrl("media/svg/files/doc.svg")}
                          alt=""
                        />
                      </div>
                      <div className="d-flex justify-content-start flex-column">
                        <a
                          href="#"
                          className="text-gray-900 fw-bold text-hover-primary fs-6"
                        >
                          {attachment.DOCUMENTS}
                        </a>
                      </div>
                    </div>
                  </td>
                  <td>
                    <a
                      href="#"
                      className="text-gray-900 fw-bold text-hover-primary d-block fs-6"
                    >
                      {/* {attachment.DOC_TYPE} */}
                      Type
                    </a>
                    <span className="text-muted fw-semibold text-muted d-block fs-7">
                      Size: 569476 bytes
                    </span>
                  </td>
                  <td className="text-end">
                    <div className=" fw-bolder d-flex align-items-right">
                      <div className="badge badge-light fw-bolder d-flex align-items-right">
                        {/* {formattedDate(new Date(attachment.LAST_UPDATED))} */}
                        Date
                      </div>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex justify-content-end flex-shrink-0">
                      <a
                        href={attachment.DOCUMENT_LINK}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1"
                      >
                        <KTIcon iconName="eye" className="fs-3" />
                      </a>
                      <a
                        href={attachment.DOCUMENT_LINK}
                        download
                        className="btn btn-icon btn-bg-light btn-active-color-primary btn-sm"
                      >
                        <KTIcon iconName="arrow-down" className="fs-3" />
                      </a>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>
        {/* end::Table container */}
      </div>
      {/* end::Body */}
    </div>
  );
};

export { EISDocs };
