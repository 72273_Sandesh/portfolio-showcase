import { FC, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { KTIcon, isNotEmpty } from "../../../../../../_metronic/helpers";
import { initialBankDetails, BankDetails } from "../core/_models";
import { useAuth } from "../../../../auth";
import { useListView } from "../core/ListViewProvider";
import { BankDetailsListLoading } from "../components/loading/BankDetailsListLoading";
import { createBankDetails, updateBankDetails } from "../core/_requests";
import { globalVariable, setGlobalVariable } from "../../GlobalVariable";
import { toast } from "react-toastify";
import axios from "axios";
import { useQueryResponse } from "../core/QueryResponseProvider";
import clsx from "clsx";

type Props = {
  isBankDetailsLoading: boolean;
  bankDetails: BankDetails;
};

interface DropdownEmp {
  id: string;
  emP_FNAME: string;
  emP_LNAME: string;
}

const editBankDetailsSchema = Yup.object().shape({
  paymenT_MODE: Yup.string().required("Mode of Payment is required"),
  banK_NAME: Yup.string().required("Bank Name is required"),
  banK_ACCT: Yup.string().required("Account Number is required"),
  banK_IFSC: Yup.string().required("IFSC Code is required"),
  emP_NAME: Yup.string().required("Name of Employee is required"),
});

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const BankDetailsEditModalForm: FC<Props> = ({
  bankDetails,
  isBankDetailsLoading,
}) => {
  const { currentUser } = useAuth();
  const { setItemIdForUpdate } = useListView();
  const { refetch } = useQueryResponse();

  const [headerName, setHeaderName] = useState(globalVariable);
  const [isEdit, setIsEdit] = useState(false);

  const [searchName, setSearchName] = useState("");
  const [dropDownEmpResp, setDropDownEmpResp] = useState<DropdownEmp[]>([]);

  const handleSearchChange = async (searchString: string) => {
    setSearchName(searchString);

    if (searchString.length > 2) {
      axios
        .get(
          `${EMPLOYEE_URL}/GetAllEmployees?page=1&items_per_page=100&search=${searchString}`
        )
        .then((response) => {
          setDropDownEmpResp(response.data.data);
        })
        .catch((error) =>
          console.error("Error fetching combined data:", error)
        );
    } else {
      setDropDownEmpResp([]);
    }
  };

  const handleSelectEmployee = async (employee: any) => {
    setDropDownEmpResp([]);
    setSearchName(employee.emP_FNAME);

    axios
      .get(`${EMPLOYEE_URL}/${employee.id}/EMPPER`)
      .then((response) => {
        formik.setValues({
          emP_NAME: response.data[0].emP_NAME,
          emP_CODE: response.data[0].emP_CODE,
          sitE_NAME: response.data[0].sitE_NAME,
          depT_NAME: response.data[0].depT_NAME,
          designation: response.data[0].designation,
          datE_JOIN: response.data[0].datE_JOIN,
          paymenT_MODE: response.data[0].paymenT_MODE,
          banK_NAME: response.data[0].banK_NAME,
          banK_CODE: response.data[0].banK_CODE,
          banK_ACCT: response.data[0].banK_ACCT,
          banK_IFSC: response.data[0].banK_IFSC,
        });
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  };

  const closeForm = () => {
    setItemIdForUpdate(undefined);
    setGlobalVariable("Add Bank Details");
  };

  const handleEditClick = () => {
    setIsEdit(true);
    setHeaderName("Edit Bank Details");
  };

  const handleCancelClick = () => {
    setIsEdit(false);
    setHeaderName("View Bank Details");
  };

  const [bankDetailsForEdit] = useState<BankDetails>({
    ...bankDetails,
    emP_NAME: bankDetails.emP_NAME || initialBankDetails.emP_NAME,
    emP_CODE: bankDetails.emP_CODE || initialBankDetails.emP_CODE,
    sitE_NAME: bankDetails.sitE_NAME || initialBankDetails.sitE_NAME,
    depT_NAME: bankDetails.depT_NAME || initialBankDetails.depT_NAME,
    designation: bankDetails.designation || initialBankDetails.designation,
    datE_JOIN: bankDetails.datE_JOIN || initialBankDetails.datE_JOIN,
    paymenT_MODE: bankDetails.paymenT_MODE || initialBankDetails.paymenT_MODE,
    banK_NAME: bankDetails.banK_NAME || initialBankDetails.banK_NAME,
    banK_CODE: bankDetails.banK_CODE || initialBankDetails.banK_CODE,
    banK_ACCT: bankDetails.banK_ACCT || initialBankDetails.banK_ACCT,
    banK_IFSC: bankDetails.banK_IFSC || initialBankDetails.banK_IFSC,
  });

  // function formatDate(dateTime: string) {
  //   const date = new Date(dateTime);
  //   const formattedDate = date.toLocaleDateString("en-GB", {
  //     day: "numeric",
  //     month: "numeric",
  //     year: "numeric",
  //   });
  //   const formattedTime = date.toLocaleTimeString("en-IN", {
  //     hour: "numeric",
  //     minute: "numeric",
  //     second: "numeric",
  //     timeZone: "Asia/Kolkata",
  //   });
  //   return `${formattedDate} ${formattedTime}`;
  // }

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };

  const formik = useFormik({
    initialValues: bankDetailsForEdit,
    validationSchema: editBankDetailsSchema,

    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);
      if (isNotEmpty(values.emP_CODE) && isEdit) {
        values.changeUser = currentUser?.first_name;
        values.changeTerm = "TERM";
        values.changeDate = new Date();
        const response = await updateBankDetails(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("Add Bank Details");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      } else {
        values.id = "null";
        values.changeUser = currentUser?.first_name;
        values.changeTerm = "TERM";
        values.changeDate = new Date();

        const response = await createBankDetails(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("Add Bank Details");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      }
    },
  });

  return (
    <>
      <div className="modal-header">
        {/* begin::Modal title */}
        <h2 className="fw-bolder">{headerName}</h2>
        {/* end::Modal title */}

        {/* begin::Close */}
        <div
          className="btn btn-icon btn-sm btn-active-icon-primary"
          data-kt-users-modal-action="close"
          onClick={closeForm}
          style={{ cursor: "pointer" }}
        >
          <KTIcon iconName="cross" className="fs-1" />
        </div>
        {/* end::Close */}
      </div>
      <div className="modal-body mx-5 mx-xl-5 my-2">
        <form
          id="kt_modal_add_user_form"
          className="form"
          onSubmit={formik.handleSubmit}
          noValidate
        >
          <div className="card-body">
            <div className="bg-light-warning border border-dashed border-gray-300 rounded p-5">
              <div className="row mb-1">
                <div className="position-relative col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="emP_FNAME"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    EMPLOYEE NAME
                  </label>
                  {globalVariable !== "Add Bank Details" ? (
                    <input
                      type="text"
                      id="emP_FNAME"
                      className="form-control form-control-lg form-control-solid "
                      name="emP_FNAME"
                      value={formik.values.emP_NAME}
                      readOnly
                    />
                  ) : (
                    <input
                      type="text"
                      id="emP_FNAME"
                      className={
                        globalVariable !== "Add Bank Details"
                          ? "form-control form-control-lg form-control-solid "
                          : "bg-white border border-dark form-control form-control-lg form-control-solid "
                      }
                      name="emP_FNAME"
                      placeholder={
                        globalVariable === "Add Bank Details"
                          ? "Search employee..."
                          : "Enter Employee Name"
                      }
                      value={searchName}
                      onChange={(e) => handleSearchChange(e.target.value)}
                      readOnly={globalVariable !== "Add Bank Details"}
                    />
                  )}
                  {dropDownEmpResp.length > 0 && (
                    <ul
                      className="position-absolute bg-white border border-secondary
                       rounded overflow-auto top-100 list-style-none p-0 mt-0 z-index-1 mh-300px min-w-300px"
                      style={{ left: 167 }}
                    >
                      {dropDownEmpResp.map((employee) => (
                        <li
                          className="ms-5 p-2 cursor-pointer text-hover-primary"
                          key={employee.id}
                          value={employee.id}
                          onClick={() => handleSelectEmployee(employee)}
                        >
                          {employee.emP_FNAME} {employee.emP_LNAME}
                        </li>
                      ))}
                    </ul>
                  )}
                </div>

                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="emP_CODE"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    EMPLOYEE CODE
                  </label>
                  <input
                    type="text"
                    id="emP_CODE"
                    className="form-control form-control-lg form-control-solid  "
                    name="emP_CODE"
                    placeholder="Enter Employee Code"
                    value={formik.values.emP_CODE}
                    onChange={formik.handleChange}
                    readOnly
                  />
                </div>
              </div>

              {globalVariable !== "Add Bank Details" && (
                <div className="row mb-1">
                  <div className="col-lg-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="trId"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      TRANSACTION ID
                    </label>
                    <input
                      type="text"
                      id="trId"
                      className="form-control form-control-lg form-control-solid "
                      name="trId"
                      placeholder="Enter Transaction Id"
                      value={""}
                      onChange={formik.handleChange}
                      readOnly
                    />
                  </div>

                  <div className="col-lg-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="trDate"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      TRANSACTION DATE
                    </label>
                    <input
                      type="date"
                      id="trDate"
                      className="form-control form-control-lg form-control-solid"
                      name="trDate"
                      placeholder="Enter Date of Transaction"
                      value={""}
                      onChange={formik.handleChange}
                      readOnly
                    />
                  </div>
                </div>
              )}

              <div className="row mb-1">
                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="sitE_NAME"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    SITE NAME
                  </label>
                  <input
                    type="text"
                    id="sitE_NAME"
                    className="form-control form-control-lg form-control-solid "
                    name="sitE_NAME"
                    placeholder="Enter Site Name"
                    value={formik.values.sitE_NAME}
                    onChange={formik.handleChange}
                    readOnly
                  />
                </div>

                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="depT_NAME"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    DEPARTMENT
                  </label>
                  <input
                    type="text"
                    id="depT_NAME"
                    className="form-control form-control-lg form-control-solid  "
                    name="depT_NAME"
                    placeholder="Enter Department Name"
                    value={formik.values.depT_NAME}
                    onChange={formik.handleChange}
                    readOnly
                  />
                </div>
              </div>

              <div className="row mb-1">
                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="designation"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    DESIGNATION
                  </label>
                  <input
                    type="text"
                    id="designation"
                    className="form-control form-control-lg form-control-solid "
                    name="designation"
                    placeholder="Enter Designation"
                    value={formik.values.designation}
                    onChange={formik.handleChange}
                    readOnly
                  />
                </div>

                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="datE_JOIN"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    JOIN DATE
                  </label>
                  <input
                    type="date"
                    id="datE_JOIN"
                    className="form-control form-control-lg form-control-solid  "
                    name="datE_JOIN"
                    placeholder="Enter Designation"
                    value={formik.values.datE_JOIN?.split("T")[0]}
                    onChange={formik.handleChange}
                    readOnly
                  />
                </div>
              </div>
            </div>

            <div className="p-5">
              {(globalVariable === "Add Bank Details" && searchName === ""  && dropDownEmpResp.length===0 ) ? (
                <div className="text-center">
                  <img
                    src="/media/logos/searchEmpWarn.png"
                    style={{ height: "150px" }}
                  />
                  <h1>Please Search Employee Name First...</h1>
                </div>
              ) : (
                <>
                  <div className="row mb-1">
                    <div className="col-lg-6">
                      <div className="d-flex align-items-center mb-1">
                        <label
                          htmlFor="paymenT_MODE"
                          className="col-form-label required fw-bold fs-6 me-2"
                          style={{ minWidth: "150px" }}
                        >
                          MODE OF PAYMENT
                        </label>
                        <select
                          id="paymenT_MODE"
                          className={clsx(
                            "form-select form-select-lg form-select-solid ",
                            {
                              "is-invalid":
                                formik.touched.paymenT_MODE &&
                                formik.errors.paymenT_MODE,
                            },
                            {
                              "is-valid":
                                formik.touched.paymenT_MODE &&
                                !formik.errors.paymenT_MODE,
                            }
                          )}
                          // className="form-select form-select-lg form-select-solid "
                          name="paymenT_MODE"
                          value={formik.values.paymenT_MODE}
                          onChange={formik.handleChange}
                          disabled={
                            isEdit === false &&
                            globalVariable !== "Add Bank Details"
                          }
                        >
                          <option value={formik.values.paymenT_MODE}>
                            {formik.values.paymenT_MODE}
                          </option>
                        </select>
                      </div>
                      {formik.touched.paymenT_MODE &&
                        formik.errors.paymenT_MODE && (
                          <div className="fv-plugins-message-container text-danger">
                            <span role="alert">
                              {formik.errors.paymenT_MODE}
                            </span>
                          </div>
                        )}
                    </div>
                    <div className="col-lg-6">
                      <div className="d-flex align-items-center mb-1">
                        <label
                          htmlFor="banK_NAME"
                          className="col-form-label required fw-bold fs-6 me-2"
                          style={{ minWidth: "150px" }}
                        >
                          BANK NAME
                        </label>
                        <select
                          id="banK_NAME"
                          // className="form-select form-select-lg form-select-solid "
                          className={clsx(
                            "form-select form-select-lg form-select-solid ",
                            {
                              "is-invalid":
                                formik.touched.banK_NAME &&
                                formik.errors.banK_NAME,
                            },
                            {
                              "is-valid":
                                formik.touched.banK_NAME &&
                                !formik.errors.banK_NAME,
                            }
                          )}
                          name="banK_NAME"
                          value={formik.values.banK_NAME}
                          onChange={formik.handleChange}
                          // disabled={globalVariable === "infoBankDetails"}
                          disabled={
                            isEdit === false &&
                            globalVariable !== "Add Bank Details"
                          }
                        >
                          <option value={formik.values.banK_CODE}>
                            {formik.values.banK_NAME}
                          </option>
                        </select>
                      </div>
                      {formik.touched.banK_NAME && formik.errors.banK_NAME && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert">{formik.errors.banK_NAME}</span>
                        </div>
                      )}
                    </div>
                  </div>

                  <div className="row mb-1">
                    <div className="col-lg-6">
                      <div className="d-flex align-items-center mb-1">
                        <label
                          htmlFor="banK_ACCT"
                          className="col-form-label required fw-bold fs-6 me-2"
                          style={{ minWidth: "150px" }}
                        >
                          ACCOUNT NUMBER
                        </label>
                        <input
                          type="number"
                          id="banK_ACCT"
                          // className="form-control form-control-lg form-control-solid "
                          className={clsx(
                            "form-control form-control-lg form-control-solid",
                            {
                              "is-invalid":
                                formik.touched.banK_ACCT &&
                                formik.errors.banK_ACCT,
                            },
                            {
                              "is-valid":
                                formik.touched.banK_ACCT &&
                                !formik.errors.banK_ACCT,
                            }
                          )}
                          name="banK_ACCT"
                          placeholder="Enter Organization Name"
                          value={formik.values.banK_ACCT}
                          onChange={formik.handleChange}
                          // readOnly={globalVariable === "infoBankDetails"}
                          readOnly={
                            isEdit === false &&
                            globalVariable !== "Add Bank Details"
                          }
                        />
                      </div>
                      {formik.touched.banK_ACCT && formik.errors.banK_ACCT && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert">{formik.errors.banK_ACCT}</span>
                        </div>
                      )}
                    </div>
                    <div className="col-lg-6">
                      <div className=" d-flex align-items-center mb-1">
                        <label
                          htmlFor="banK_IFSC"
                          className="col-form-label required fw-bold fs-6 me-2"
                          style={{ minWidth: "150px" }}
                        >
                          IFSC CODE
                        </label>
                        <input
                          type="text"
                          id="banK_IFSC"
                          // className="form-control form-control-lg form-control-solid  "
                          className={clsx(
                            "form-control form-control-lg form-control-solid",
                            {
                              "is-invalid":
                                formik.touched.banK_IFSC &&
                                formik.errors.banK_IFSC,
                            },
                            {
                              "is-valid":
                                formik.touched.banK_IFSC &&
                                !formik.errors.banK_IFSC,
                            }
                          )}
                          name="banK_IFSC"
                          placeholder="Enter IFSC code"
                          value={formik.values.banK_IFSC}
                          onChange={formik.handleChange}
                          // readOnly={globalVariable === "infoBankDetails"}
                          readOnly={
                            isEdit === false &&
                            globalVariable !== "Add Bank Details"
                          }
                        />
                      </div>

                      {formik.touched.banK_IFSC && formik.errors.banK_IFSC && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert">{formik.errors.banK_IFSC}</span>
                        </div>
                      )}
                    </div>
                  </div>

                  <div className="row mb-1">
                    <div className="col-lg-6">
                      <div className="d-flex align-items-center mb-1">
                        <label
                          htmlFor="emP_NAME"
                          className="col-form-label required fw-bold fs-6 me-2"
                          style={{ minWidth: "150px" }}
                        >
                          NAME AS PER BANK PASSBOOK
                        </label>
                        <input
                          type="text"
                          id="emP_NAME"
                          // className="form-control form-control-lg form-control-solid "
                          className={clsx(
                            "form-control form-control-lg form-control-solid",
                            {
                              "is-invalid":
                                formik.touched.emP_NAME &&
                                formik.errors.emP_NAME,
                            },
                            {
                              "is-valid":
                                formik.touched.emP_NAME &&
                                !formik.errors.emP_NAME,
                            }
                          )}
                          name="emP_NAME"
                          placeholder="Enter Name as per Bank"
                          value={formik.values.emP_NAME}
                          onChange={formik.handleChange}
                          // readOnly={globalVariable === "infoBankDetails"}
                          readOnly={
                            isEdit === false &&
                            globalVariable !== "Add Bank Details"
                          }
                        />
                      </div>
                      {formik.touched.emP_NAME && formik.errors.emP_NAME && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert">{formik.errors.emP_NAME}</span>
                        </div>
                      )}
                    </div>
                    <div className="col-lg-6">
                      <div className=" d-flex align-items-center mb-1">
                        <label
                          htmlFor="banK_ACCT"
                          className="col-form-label required fw-bold fs-6 me-2"
                          style={{ minWidth: "150px" }}
                        >
                          ATTACH DOCUMENT
                        </label>
                        <input
                          name="banK_ACCT"
                          // className="form-control form-control-lg flex-grow-1"
                          className={clsx(
                            "form-control form-control-lg flex-grow-1",
                            {
                              "is-invalid":
                                formik.touched.banK_ACCT &&
                                formik.errors.banK_ACCT,
                            },
                            {
                              "is-valid":
                                formik.touched.banK_ACCT &&
                                !formik.errors.banK_ACCT,
                            }
                          )}
                          type="file"
                          id="pdf"
                          // onChange={handlePdfChange}
                          accept="application/pdf"
                          required
                          // disabled={globalVariable === "infoBankDetails"}
                          disabled={
                            isEdit === false &&
                            globalVariable !== "Add Bank Details"
                          }
                        />
                      </div>
                      {formik.touched.banK_IFSC && formik.errors.banK_IFSC && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert">{formik.errors.banK_IFSC}</span>
                        </div>
                      )}
                    </div>
                  </div>

                  <div className="row mb-1">
                    <div className="col-lg-6">
                      <div className="d-flex align-items-center mb-1">
                        <label
                          htmlFor="organisation"
                          className="col-form-label required fw-bold fs-6 me-2"
                          style={{ minWidth: "150px" }}
                        >
                          REMARKS
                        </label>
                        <input
                          type="text"
                          id="organisation"
                          className="form-control form-control-lg form-control-solid "
                          name="organisation"
                          placeholder="Enter Organization Name"
                          value={""}
                          onChange={formik.handleChange}
                          // readOnly={globalVariable === "infoBankDetails"}
                          readOnly={
                            isEdit === false &&
                            globalVariable !== "Add Bank Details"
                          }
                        />
                      </div>
                      {formik.touched.banK_IFSC && formik.errors.banK_IFSC && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert">{formik.errors.banK_IFSC}</span>
                        </div>
                      )}
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
          <div className="text-center pt-15">
            {!isEdit ? (
              <button
                type="reset"
                onClick={closeForm}
                className="btn btn-light me-3"
              >
                Back
              </button>
            ) : (
              <button
                type="reset"
                onClick={handleCancelClick}
                className="btn btn-light me-3"
              >
                Cancel
              </button>
            )}
            {!isEdit && globalVariable !== "Add Bank Details" ? (
              <button
                type="reset"
                onClick={handleEditClick}
                className="btn btn-primary me-3"
                disabled={formik.isSubmitting || isBankDetailsLoading}
              >
                Edit
              </button>
            ) : (
              <button
                type="submit"
                className="btn btn-primary"
                disabled={
                  isBankDetailsLoading ||
                  formik.isSubmitting ||
                  // !formik.isValid ||
                  !formik.touched ||
                  (!isEdit && globalVariable !== "Add Bank Details")
                }
              >
                <span className="indicator-label">Submit</span>
                {(formik.isSubmitting || isBankDetailsLoading) && (
                  <span className="indicator-progress">
                    Please wait...{" "}
                    <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                  </span>
                )}
              </button>
            )}
          </div>
        </form>
      </div>
      {(formik.isSubmitting || isBankDetailsLoading) && (
        <BankDetailsListLoading />
      )}
    </>
  );
};

export { BankDetailsEditModalForm };
