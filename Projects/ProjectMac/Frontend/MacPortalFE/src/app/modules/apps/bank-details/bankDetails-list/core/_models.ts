import {ID, Response} from '../../../../../../_metronic/helpers'
export type BankDetails = {
  id?: ID
  deptCode?:string,
  name?:string,
  changeUser?: string,
  deptHead?: string,
  changeDate?: Date,
  shortDescription?: string,
  changeTerm?: string,
  status?: string,
  acctCode?: string,
  payTable?: string,
  cctrCode?:string,
  acctCodeAp?: string,
  cctrCodeAp?: string,
  udfNumber1?:number,
  shDeptCode?: string,
  changeReason?: string | null,
  newReason?: string | null, 
  deptHeadName?: string,
  responseStatus?: string;
  message?: string;

  emP_NAME?:string,
  emP_CODE?:string,
  sitE_NAME?:string,
  depT_NAME?:string,
  designation?:string,
  datE_JOIN?:string,
  paymenT_MODE?:string,
  banK_NAME?:string,
  banK_ACCT?:string,
  banK_IFSC?:string,
  banK_CODE?:string,
 
}

export type BankDetailsQueryResponse = Response<Array<BankDetails>>

export const initialBankDetails: BankDetails = {
}
