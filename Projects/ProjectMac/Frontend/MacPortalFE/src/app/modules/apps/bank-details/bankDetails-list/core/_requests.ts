import axios, { AxiosResponse } from "axios";
import { ID, Response } from "../../../../../../_metronic/helpers";
import { BankDetails, BankDetailsQueryResponse } from "./_models";

const BANKDETAILS_URL = import.meta.env.VITE_DEPT_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const getBankDetails = (query: string): Promise<BankDetailsQueryResponse> => {
  return axios
    .get(`${BANKDETAILS_URL}/GetAllDepartments/query?${query}`)
    .then((d: AxiosResponse<BankDetailsQueryResponse>) => d.data);
};

const getBankDetailsById = (id:ID): Promise<BankDetails | undefined> => {
  return axios
    .get(`${EMPLOYEE_URL}/H004711/EMPPER`)
    .then((response: AxiosResponse<Response<BankDetails>>) => response.data)
    .then((data: Response<BankDetails>) => {
      console.log("The data is : ", data); 
      console.log("getBankDetailsById called+++++++++++")
      return data[0];
    });
};

const createBankDetails = (bankDetails: BankDetails): Promise<BankDetails | undefined> => {
  return axios
    .post(`${BANKDETAILS_URL}/InsertDepartment`, bankDetails)
    .then((response: AxiosResponse<Response<BankDetails>>) => response.data)
    .then((response: Response<BankDetails>) => response.data);
};

const updateBankDetails = (bankDetails: BankDetails): Promise<BankDetails | undefined> => {
  return axios
    .put(`${BANKDETAILS_URL}/UpdateDepartment/${bankDetails.emP_CODE}`, bankDetails)
    .then((response: AxiosResponse<Response<BankDetails>>) => response.data)
    .then((response: Response<BankDetails>) => response.data);
};

const deleteBankDetails = (bankDetailsId: ID): Promise<void> => {
  return axios.delete(`${BANKDETAILS_URL}/${bankDetailsId}`).then(() => {});
};

const deleteSelectedBankDetails = (bankDetailsIds: Array<ID>): Promise<void> => {
  const requests = bankDetailsIds.map((id) => axios.delete(`${BANKDETAILS_URL}/${id}`));
  return axios.all(requests).then(() => {});
};

export {
  getBankDetails,
  deleteBankDetails,
  deleteSelectedBankDetails,
  getBankDetailsById,
  createBankDetails,
  updateBankDetails,
};
