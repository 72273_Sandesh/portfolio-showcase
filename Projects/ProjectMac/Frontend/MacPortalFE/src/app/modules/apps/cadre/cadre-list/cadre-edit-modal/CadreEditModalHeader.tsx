import {KTIcon} from '../../../../../../_metronic/helpers'
import { globalVariable, setGlobalVariable } from '../../GlobalVariable'
import {useListView} from '../core/ListViewProvider'

const CadreEditModalHeader = () => {
  const {setItemIdForUpdate} = useListView()
  const openAddModal = () => {
    setItemIdForUpdate(undefined)
    setGlobalVariable("addCadre")
  }
  return (
    <div className='modal-header'>
      {/* begin::Modal title */}
      {globalVariable == 'addCadre' && (<h2 className='fw-bolder'>Add New Cadre</h2>)
     || globalVariable == 'editCadre' && (<h2 className='fw-bolder'>Edit Cadre</h2>)
      || globalVariable == 'infoCadre' && (<h2 className='fw-bolder'>Cadre Details</h2> )}
      {/* end::Modal title */}

      {/* begin::Close */}
      <div
        className='btn btn-icon btn-sm btn-active-icon-primary'
        data-kt-users-modal-action='close'
        onClick={openAddModal}
        style={{cursor: 'pointer'}}
      >
        <KTIcon iconName='cross' className='fs-1' />
      </div>
      {/* end::Close */}
    </div>
  )
}

export {CadreEditModalHeader}
