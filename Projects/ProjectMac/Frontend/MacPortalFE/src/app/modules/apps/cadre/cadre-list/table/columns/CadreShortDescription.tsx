// import {FC} from 'react'

// type Props = {
//   chG_DATE:Date
// }

// const DepartmentLastLoginCell: FC<Props> = ({chG_DATE}) => (
  
//   <div className='badge badge-light fw-bolder d-flex align-items-right '>{chG_DATE}</div>
// )

// export {DepartmentLastLoginCell}


import React from 'react';

type Props = {
    sH_DESCR?: string;
};

const CadreShortDescription: React.FC<Props> = ({ sH_DESCR }) => {
//   const formattedDate = new Date(chG_DATE).toLocaleString('en-GB', {
//     day: '2-digit',
//     month: '2-digit',
//     year: 'numeric',
    // hour: '2-digit',
    // minute: '2-digit',
    // second: '2-digit',

  return (
    <div className=' fw-bolder d-flex align-items-right'>
      {sH_DESCR ?(sH_DESCR.toUpperCase()): <div className='badge badge-danger fw-bolder d-flex align-items-right'>NOT ASSSIGNED</div>}
    </div>


  );
};

export { CadreShortDescription };
