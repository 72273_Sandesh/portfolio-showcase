import React from 'react';
import { useQueryResponseData } from '../../core/QueryResponseProvider';

const DepartmentTableFormat = () => {
  const departments = useQueryResponseData(); //<Array<Department>>

    return (
  
        <table id="table-to-export">
        <thead>
          <tr>
            <th>Department Id</th>
            <th>Department Name</th>
            <th>Description</th>
            <th>Department Head</th>
            <th>Status</th>
            <th>Account Code</th>
            <th>Pay Table</th>
            <th>CCTR Code</th>
            <th>Account Code Ap</th>
            <th>CCTR Code Ap</th>
            <th>UDF Number 1</th>
            <th>Sh Dept Code</th>
            <th>Change User</th>
            <th>Change Date</th>
            <th>Change Term</th>
            <th>Change Reason</th>
          </tr>
        </thead>
        <tbody>
          {departments.map(department => (
            <tr key={department.id}>
              <td>{department.id}</td>
              <td>{department.name}</td>
              <td>{department.shortDescription}</td>
              <td>{department.deptHead}</td>
              <td>{department.status}</td>
              <td>{department.acctCode}</td>
              <td>{department.payTable}</td>
              <td>{department.cctrCode}</td>
              <td>{department.acctCodeAp}</td>
              <td>{department.cctrCodeAp}</td>
              <td>{department.udfNumber1}</td>
              <td>{department.shDeptCode}</td>
              <td>{department.changeUser}</td>
              <td>{department.changeDate?.toString()}</td>
              <td>{department.changeTerm}</td>
              <td>{department.changeReason}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  
};

export default DepartmentTableFormat;
