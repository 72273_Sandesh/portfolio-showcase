import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { isNotEmpty } from "../../../../../../_metronic/helpers";
import { initialDepartment, Department } from "../core/_models";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { DepartmentListLoading } from "../components/loading/DepartmentListLoading";
import { createDepartment, updateDepartment } from "../core/_requests";
import { useQueryResponse } from "../core/QueryResponseProvider";
import { globalVariable, setGlobalVariable } from "../../GlobalVariable";
import { toast } from "react-toastify";

import axios from "axios";

type Props = {
  isDepartmentLoading: boolean;
  department: Department;
};

interface deptHead {
  shortName: string;
  empCode: string;
}
const API_URL = import.meta.env.VITE_DEPT_DROPDOWNS_API_URL;

const editDepartmentSchema = Yup.object().shape({
  name: Yup.string().required("Description is required"),
  shortDescription: Yup.string().required("Short Description is required"),
  status: Yup.string().required("Status is required"),
  deptHead: Yup.string().required("Department Head is required"),
  changeReason: Yup.string().min(3, "Minimum 3 characters"),
});

const DepartmentEditModalForm: FC<Props> = ({
  department,
  isDepartmentLoading,
}) => {
  const { setItemIdForUpdate } = useListView();
  const { refetch } = useQueryResponse();

  const [statuses] = useState(["ACTIVE", "INACTIVE"]);
  const [status, setStatus] = useState("");

  const [deptHeads, setDeptHeads] = useState<deptHead[]>([]);
  const [deptHead, setDeptHead] = useState("");

  const [newReason, setNewReason] = useState("");

  // const [ipAddress, setIpAddress] = useState<string>("");
  const { currentUser } = useAuth();

  useEffect(() => {
    fetchFromBackend()
      .then((combinedData) => {
        setDeptHeads(combinedData.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const fetchFromBackend = async () => {
    try {
      const response = await axios.get(API_URL);
      const deptHeadData = await response.data;
      return deptHeadData;
    } catch (error) {
      throw new Error("Failed to fetch Deptartment Head from backend");
    }
  };

  const [departmentForEdit] = useState<Department>({
    ...department,
    name: department.name || initialDepartment.name,
    shortDescription:
      department.shortDescription || initialDepartment.shortDescription,
    deptHead: department.deptHead || initialDepartment.deptHead,
    status: department.status || initialDepartment.status || status,
    changeDate: department.changeDate || initialDepartment.changeDate,
    changeUser: department.changeUser || initialDepartment.changeUser,
    changeTerm: department.changeTerm || initialDepartment.changeTerm,
  });
  const fetchIpAddress = async () => {};

  fetchIpAddress();
  function formatDate(dateTime: string) {
    const date = new Date(dateTime);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata", // Specify the Indian time zone
    });
    return `${formattedDate} ${formattedTime}`;
  }

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };
  const formik = useFormik({
    initialValues: departmentForEdit,
    validationSchema: editDepartmentSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);
      // try {
      if (isNotEmpty(values.id) && globalVariable === "editDept") {
        values.changeUser = currentUser?.first_name;
        values.changeTerm = "TERM";
        values.changeDate = new Date();
        values.deptCode = "null";
        const response = await updateDepartment(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("addDept");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      } else {
        values.id = "null";
        values.changeUser = currentUser?.first_name;
        values.changeTerm = "TERM";
        values.changeDate = new Date();

        const response = await createDepartment(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("addDept");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      }
    },
  });

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column " id="kt_modal_add_user_scroll">
          {globalVariable !== "addDept" && (
            <div className="row mb-3">
              <div className="fv-row">
                <label className=" fw-bold fs-6 mb-2">Department Code :</label>
                <input
                  className="form-control form-control-solid mb-3 mb-lg-0"
                  value={formik.values.id ? formik.values.id : ""}
                  readOnly={
                    globalVariable === "infoDept" ||
                    globalVariable === "editDept"
                  }
                />
              </div>
            </div>
          )}
          <div className="row mb-3">
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Department Name
                </label>
                <input
                  placeholder="Description"
                  {...formik.getFieldProps("name")}
                  onChange={(e) =>
                    formik.setFieldValue("name", e.target.value.toUpperCase())
                  }
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    { "is-invalid": formik.touched.name && formik.errors.name },
                    { "is-valid": formik.touched.name && !formik.errors.name }
                  )}
                  type="name"
                  name="name"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDepartmentLoading ||
                    globalVariable === "infoDept"
                  }
                />
                {formik.touched.name && formik.errors.name && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.name}</span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Short Description
                </label>
                <input
                  placeholder="Short Description"
                  {...formik.getFieldProps("shortDescription")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "shortDescription",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.shortDescription &&
                        formik.errors.shortDescription,
                    },
                    {
                      "is-valid":
                        formik.touched.shortDescription &&
                        !formik.errors.shortDescription,
                    }
                  )}
                  type="text"
                  name="shortDescription"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDepartmentLoading ||
                    globalVariable === "infoDept"
                  }
                />
                {formik.touched.shortDescription &&
                  formik.errors.shortDescription && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.shortDescription}</span>
                    </div>
                  )}
              </div>
            </div>
            {/* data empCode shortName */}
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required form-label fs-6 fw-bold">
                  Department Head
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid fw-bolder",
                    {
                      "is-invalid":
                        formik.touched.deptHead && formik.errors.deptHead,
                    },
                    {
                      "is-valid":
                        formik.touched.deptHead && !formik.errors.deptHead,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("deptHead", e.target.value);
                    setDeptHead(e.target.value);
                  }}
                  value={deptHead}
                  disabled={globalVariable === "infoDept"}
                >
                  <option value="">
                    {formik.values.deptHeadName === ""
                      ? "NOT ASSIGNED"
                      : formik.values.deptHeadName}
                  </option>
                  {deptHeads.map((deptHead) => (
                    <option key={deptHead.empCode} value={deptHead.empCode}>
                      {deptHead.shortName}
                    </option>
                  ))}
                </select>
                {formik.touched.deptHead && formik.errors.deptHead && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.deptHead}</span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required form-label fs-6 fw-bold">
                  Status
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid fw-bolder",
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                    setStatus(e.target.value);
                  }}
                  value={status}
                  disabled={globalVariable === "infoDept"}
                >
                  {globalVariable === "addDept" ? (
                    <option value="">{formik.values.status}</option>
                  ) : (
                    <option value="">
                      {formik.values.status === "A"
                        ? "ACTIVE"
                        : formik.values.status === "N"
                        ? "INACTIVE"
                        : ""}
                    </option>
                  )}
                  {statuses.map((status) => (
                    <option key={status} value={status}>
                      {status}
                    </option>
                  ))}
                </select>
                {formik.touched.status && formik.errors.status && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.status}</span>
                  </div>
                )}
              </div>
            </div>

            {globalVariable === "infoDept" && (
              <>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className="fw-bold fs-6 mb-2">Change User</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      value={formik.values.changeUser?.toUpperCase()}
                      disabled={globalVariable === "infoDept"}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className="fw-bold fs-6 mb-2">Change Term</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      value={formik.values.changeTerm}
                      //value={ipAddress}
                      disabled={globalVariable === "infoDept"}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className=" fw-bold fs-6 mb-2">Change Date</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      // value={formik.values.changeDate}
                      // value={
                      //   formik.values.changeDate
                      //     ? formik.values.changeDate.toString()
                      //     : ""
                      // }
                      value={
                        formik.values.changeDate
                          ? formatDate(formik.values?.changeDate.toString())
                          : ""
                      }
                      disabled={globalVariable === "infoDept"}
                    />
                  </div>
                </div>
              </>
            )}
          </div>
          {globalVariable !== "addDept" && (
            <div className="row mb-3">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Change Reason
                </label>
                <textarea
                  placeholder="Change Reason"
                  //value={{...formik.getFieldProps('changeReason')}}
                  {...(globalVariable === "infoDept"
                    ? { ...formik.getFieldProps("changeReason") }
                    : { newReason })}
                  onChange={(e) => {
                    formik.setFieldValue(
                      "changeReason",
                      e.target.value.toUpperCase()
                    );
                    setNewReason(e.target.value.toUpperCase());
                  }}
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.changeReason &&
                        formik.errors.changeReason,
                    },
                    {
                      "is-valid":
                        formik.touched.changeReason &&
                        !formik.errors.changeReason,
                    }
                  )}
                  // type="text"
                  name="changeReason"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDepartmentLoading ||
                    globalVariable === "infoDept"
                  }
                />
                {formik.touched.changeReason && formik.errors.changeReason && (
                  <div className="fv-plugins-message-container">
                    <span role="alert">{formik.errors.changeReason}</span>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
        <div className="text-center pt-15">
          {/* <button
            type='reset'
            onClick={() => cancel()}
            className='btn btn-light me-3'
            disabled={formik.isSubmitting || isDepartmentLoading}
          >
            Cancel
          </button> */}
          {globalVariable !== "infoDept" && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isDepartmentLoading ||
                formik.isSubmitting ||
                !formik.isValid ||
                !formik.touched ||
                (globalVariable !== "addDept" && newReason === "")
              }
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isDepartmentLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isDepartmentLoading) && (
        <DepartmentListLoading />
      )}
    </>
  );
};

export { DepartmentEditModalForm };
