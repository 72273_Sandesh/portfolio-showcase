import {KTIcon} from '../../../../../../_metronic/helpers'
import { globalVariable, setGlobalVariable } from '../../GlobalVariable'
import {useListView} from '../core/ListViewProvider'

const DepartmentEditModalHeader = () => {
  const {setItemIdForUpdate} = useListView()
  const openAddModal = () => {
    setItemIdForUpdate(undefined)
    setGlobalVariable("addDept")
  }
  return (
    <div className='modal-header'>
      {/* begin::Modal title */}
      {globalVariable == 'addDept' && (<h2 className='fw-bolder'>Add New Department</h2>)
     || globalVariable == 'editDept' && (<h2 className='fw-bolder'>Edit Department</h2>)
      || globalVariable == 'infoDept' && (<h2 className='fw-bolder'>Department Details</h2> )}
      {/* end::Modal title */}

      {/* begin::Close */}
      <div
        className='btn btn-icon btn-sm btn-active-icon-primary'
        data-kt-users-modal-action='close'
        onClick={openAddModal}
        style={{cursor: 'pointer'}}
      >
        <KTIcon iconName='cross' className='fs-1' />
      </div>
      {/* end::Close */}
    </div>
  )
}

export {DepartmentEditModalHeader}
