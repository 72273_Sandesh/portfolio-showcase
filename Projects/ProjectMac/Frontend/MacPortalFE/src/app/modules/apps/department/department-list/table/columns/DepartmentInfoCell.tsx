
import clsx from 'clsx'
import {FC} from 'react'
import {toAbsoluteUrl} from '../../../../../../../_metronic/helpers'
import {Department} from '../../core/_models'

type Props = {
    department: Department
}

const DepartmentInfoCell: FC<Props> = ({department}) => (
  <div className='d-flex align-items-center '>
    {/* begin:: Avatar */}
    {/* <div className='symbol symbol-circle symbol-50px overflow-hidden me-3'>
      <a href='#'>
        {department.avatar ? (
          <div className='symbol-label'>
            <img src={toAbsoluteUrl(`media/${department.avatar}`)} alt={department.name} className='w-100' />
          </div>
        ) : (
          <div
            className={clsx(
              'symbol-label fs-3',
              `bg-light-${department.initials?.state}`,
              `text-${department.initials?.state}`
            )}
          >
            {department.initials?.label}
          </div>
        )}
      </a>
    </div> */}
    <div className='d-flex flex-column'>
      <a className='text-gray-800 text-hover-primary mb-1'>
        {department.name?.toUpperCase()}
      </a>
      <span >
        <div className=' fw-bolder d-flex align-items-right'>
          <div className='badge badge-light-success fw-bolder d-flex align-items-right'>{department.id}</div>
        </div>
    </span>
    </div>
  </div>
)

export {DepartmentInfoCell}


