import { Column } from "react-table";
import { DepartmentInfoCell } from "./DepartmentInfoCell";
import { DepartmentLastLoginCell } from "./DepartmentLastLoginCell";
import { DepartmentTwoStepsCell } from "./DepartmentTwoStepsCell";
import { DepartmentActionsCell } from "./DepartmentActionsCell";
import { DepartmentCustomHeader } from "./DepartmentCustomHeader";
import { Department } from "../../core/_models";
import { DepartmentChangeUser } from "./DepartmentChangeUser";

const departmentColumns: ReadonlyArray<Column<Department>> = [
  {
    Header: (props) => (
      <DepartmentCustomHeader
        tableProps={props}
        title="Department Name"
        className="min-w-125px text-left "
      />
    ),
    id: "name",
    Cell: ({ ...props }) => (
      <DepartmentInfoCell department={props.data[props.row.index]} />
    ),
  },
  {
    Header: (props) => (
      <DepartmentCustomHeader
        tableProps={props}
        title="Department Head"
        className="min-w-125px text-left"
      />
    ),
    id: "deptHead",
    Cell: ({ ...props }) => (
      <DepartmentTwoStepsCell deptHead={props.data[props.row.index].deptHead} />
    ),
  },
  {
    Header: (props) => (
      <DepartmentCustomHeader
        tableProps={props}
        title="Change User"
        className="min-w-125px text-left"
      />
    ),
    accessor: "changeUser",
    Cell: ({ ...props }) => (
      <DepartmentChangeUser
        changeUser={props.data[props.row.index].changeUser}
      />
    ),
  },
  {
    Header: (props) => (
      <DepartmentCustomHeader
        tableProps={props}
        title="Change Date"
        className="min-w-125px text-left"
      />
    ),
    id: "changeDate",
    Cell: ({ ...props }) => (
      <DepartmentLastLoginCell
        changeDate={props.data[props.row.index].changeDate ?? new Date()}
      />
    ),
  },
  {
    Header: (props) => (
      <DepartmentCustomHeader
        tableProps={props}
        title="Status"
        className="min-w-125px text-left"
      />
    ),
    accessor: "status",
    Cell: ({ value }) => (
      <span>
        <div className=" fw-bolder d-flex align-items-right">
          <div className="d-flex align-items-right">
            {
              <div className="d-flex align-items-right">
                {value === "A" ? (
                  <div className="badge badge-light-success fw-bolder d-flex align-items-right">
                    ACTIVE
                  </div>
                ) : value === "N" ? (
                  <div className="badge badge-light-danger fw-bolder d-flex align-items-right">
                    INACTIVE
                  </div>
                ) : (
                  <div className="badge d-flex align-items-right">&nbsp;</div>
                )}
              </div>
            }
          </div>
        </div>
      </span>
    ),
  },
  {
    Header: (props) => (
      <DepartmentCustomHeader
        tableProps={props}
        title="Actions"
        className="min-w-100px text-left"
      />
    ),
    id: "actions",
    Cell: ({ ...props }) => (
      <DepartmentActionsCell id={props.data[props.row.index].id} />
    ),
  },
];

export { departmentColumns };
