import {ListViewProvider, useListView} from './core/ListViewProvider'
import {QueryRequestProvider} from './core/QueryRequestProvider'
import {QueryResponseProvider} from './core/QueryResponseProvider'
import {DesignationsListHeader} from './components/header/DesignationsListHeader'
import {DesignationsTable} from './table/DesignationsTable'
import {DesignationEditModal} from './designation-edit-modal/DesignationEditModal'
import {KTCard} from '../../../../../_metronic/helpers'
import { ToolbarWrapper } from '../../../../../_metronic/layout/components/toolbar'
import { Content } from '../../../../../_metronic/layout/components/content'

const DesignationsList = () => {
  const {itemIdForUpdate} = useListView()
  return (
    <>
      <KTCard>
        <DesignationsListHeader />
        <DesignationsTable />
      </KTCard>
      {itemIdForUpdate !== undefined && <DesignationEditModal />}
    </>
  )
}

const DesignationsListWrapper = () => (
  <QueryRequestProvider>
    <QueryResponseProvider>
      <ListViewProvider>
        <ToolbarWrapper />
        <Content>
          <DesignationsList />
        </Content>
      </ListViewProvider>
    </QueryResponseProvider>
  </QueryRequestProvider>
)

export {DesignationsListWrapper}
