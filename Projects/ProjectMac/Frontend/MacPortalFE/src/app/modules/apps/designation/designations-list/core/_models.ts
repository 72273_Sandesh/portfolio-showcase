import {ID, Response} from '../../../../../../_metronic/helpers'
export type Designation = {
  id?: ID
  name?:string,
  longDesignation?:string,
  status?:string,
  gradeCode?:string,
  gradeName?:string,
  cadreCode?:string,
  cadreName?:string,
  gradeId?:string,
  changeUser?:string,
  changeTerm?:string,
  changeReason?:string,
  changeDate?:Date,
  udfNum1?:number,
  udfStr1?:string,
  sapDesignCode?:string,
  responseStatus?: string;
  message?: string;
      
}

export type DesignationsQueryResponse = Response<Array<Designation>>

export const initialDesignation: Designation = {
 
}
