import {useQuery} from 'react-query'
import {DesignationEditModalForm} from './DesignationEditModalForm'
import {isNotEmpty, QUERIES} from '../../../../../../_metronic/helpers'
import {useListView} from '../core/ListViewProvider'
import {getDesignationById} from '../core/_requests'

const DesignationEditModalFormWrapper = () => {
  const {itemIdForUpdate, setItemIdForUpdate} = useListView()
  const enabledQuery: boolean = isNotEmpty(itemIdForUpdate)
  const {
    isLoading,
    data: designation,
    error,
  } = useQuery(
    `${QUERIES.Designations_LIST}-designation-${itemIdForUpdate}`,
    () => {
      return getDesignationById(itemIdForUpdate)
    },
    {
      cacheTime: 0,
      enabled: enabledQuery,
      onError: (err) => {
        setItemIdForUpdate(undefined)
        console.error(err)
      },
    }
  )

  if (!itemIdForUpdate) {
    return <DesignationEditModalForm isDesignationLoading={isLoading} designation={{id: undefined}} />
  }

  if (!isLoading && !error && designation) {
    return <DesignationEditModalForm isDesignationLoading={isLoading} designation={designation} />
  }

  return null
}

export {DesignationEditModalFormWrapper}
