import {Route, Routes, Outlet, Navigate} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {EmployeeSearchListWrapper} from './employeeSearch-list/EmployeeSearchList'

const employeeSearchBreadcrumbs: Array<PageLink> = [
  {
    title: 'Masters ',
    path: '/apps/employee-search/employeesearch',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const EmployeeSearchPage = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='employeesearch'
          element={
            <>
              <PageTitle breadcrumbs={employeeSearchBreadcrumbs}>Employee Search</PageTitle>
              <EmployeeSearchListWrapper />
            </>
          }
        />
      </Route>
      <Route index element={<Navigate to='/apps/employee-search/employeesearch' />} />
    </Routes>
  )
}

export default EmployeeSearchPage
