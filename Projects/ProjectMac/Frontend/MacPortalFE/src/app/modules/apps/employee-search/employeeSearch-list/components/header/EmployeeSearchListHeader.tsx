import {useListView} from '../../core/ListViewProvider'
import {EmployeeSearchListToolbar} from './EmployeeSearchListToolbar'
import {EmployeeSearchListGrouping} from './EmployeeSearchListGrouping'
import {EmployeeSearchListSearchComponent} from './EmployeeSearchListSearchComponent'

const EmployeeSearchListHeader = () => {
  const {selected} = useListView()
  return (
    <div className='card-header border-0 pt-6'>
      <EmployeeSearchListSearchComponent />
      {/* begin::Card toolbar */}
      <div className='card-toolbar'>
        {/* begin::Group actions */}
        {selected.length > 0 ? <EmployeeSearchListGrouping /> : <EmployeeSearchListToolbar />}
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export {EmployeeSearchListHeader}
