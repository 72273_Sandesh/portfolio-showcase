import {Column} from 'react-table'
import {EmployeeSearchInfoCell} from './EmployeeSearchInfoCell'
import {EmployeeSearchLastLoginCell} from './EmployeeSearchLastLoginCell'
import {EmployeeSearchTwoStepsCell} from './EmployeeSearchTwoStepsCell'
import {EmployeeSearchActionsCell} from './EmployeeSearchActionsCell'
import {EmployeeSearchSelectionCell} from './EmployeeSearchSelectionCell'
import {EmployeeSearchCustomHeader} from './EmployeeSearchCustomHeader'
import {EmployeeSearchSelectionHeader} from './EmployeeSearchSelectionHeader'
import {EmployeeSearch} from '../../core/_models'

const EmployeeSearchColumns: ReadonlyArray<Column<EmployeeSearch>> = [
  // {
  //   Header: (props) => <EmployeeSearchSelectionHeader tableProps={props} />,
  //   id: 'selection',
  //   Cell: ({...props}) => <EmployeeSearchSelectionCell id={props.data[props.row.index].id} />,
  // },
  {
    Header: (props) => <EmployeeSearchCustomHeader tableProps={props} title='Employee ID' className='min-w-125px' />,
    id: 'empid',
    Cell: ({...props}) => <EmployeeSearchInfoCell employeeSearch={props.data[props.row.index]} />,
  },

  {
    Header: (props) => <EmployeeSearchCustomHeader tableProps={props} title='Name' className='min-w-125px' />,
    id: 'name',
    Cell: ({...props}) => <EmployeeSearchInfoCell employeeSearch={props.data[props.row.index]} />,
  },
  {
    Header: (props) => (
      <EmployeeSearchCustomHeader tableProps={props} title='Department' className='min-w-125px' />
    ),
    accessor: 'joined_day',id:'dept',
  },
  {
    Header: (props) => <EmployeeSearchCustomHeader tableProps={props} title='Designation' className='min-w-125px' />,
    accessor: 'role',  id: 'designation',
  },

  {
    Header: (props) => (
      <EmployeeSearchCustomHeader tableProps={props} title='Employee site' className='min-w-125px' />
    ),
    id: 'empsite',
    Cell: ({...props}) => <EmployeeSearchLastLoginCell last_login={props.data[props.row.index].last_login} />,
  },
  {
    Header: (props) => (
      <EmployeeSearchCustomHeader tableProps={props} title='State' className='min-w-125px' />
    ),
    id: 'state',
    Cell: ({...props}) => <EmployeeSearchTwoStepsCell two_steps={props.data[props.row.index].two_steps} />,
  },
 
  {
    Header: (props) => (
      <EmployeeSearchCustomHeader tableProps={props} title='Actions' className=' min-w--125px' />
    ),
    id: 'actions',
    Cell: ({...props}) => <EmployeeSearchActionsCell id={props.data[props.row.index].id} />,
  },
]

export {EmployeeSearchColumns}
