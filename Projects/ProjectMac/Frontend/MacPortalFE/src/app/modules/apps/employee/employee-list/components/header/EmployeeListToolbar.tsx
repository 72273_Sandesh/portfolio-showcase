import { useState, useRef } from "react";
import { KTIcon } from "../../../../../../../_metronic/helpers";
import { useListView } from "../../core/ListViewProvider";
import EmployeeTableFormat from "./EmployeeTableFormat";
import * as XLSX from "xlsx";
import { toast } from "react-toastify";

const EmployeeListToolbar: React.FC = () => {
  const [exporting, setExporting] = useState<boolean>(false);
  const tableRef = useRef<HTMLDivElement>(null);
  const { setItemIdForUpdate } = useListView();
  const openAddEmployeeModal = () => {
    setItemIdForUpdate(null);
  };

  const handleExport = () => {
    setExporting(true);
    const input = tableRef.current as HTMLTableElement;
    if (!input) {
      // toast.error(`Exporting Employee Data Failed...`);
      toast.error(
        <div>
          <h6 className="alert-heading">Error</h6>
          <p className="alert alert-danger" role="alert">
            <h6>Exporting Employee Data Failed...</h6>
          </p>
        </div>
      );
      console.error("Table container element not found.");
      setExporting(false);
      return;
    }
    const tableData = parseTableData(input);
    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.aoa_to_sheet(tableData);
    XLSX.utils.book_append_sheet(wb, ws, "Employees");
    const excelFileName = "Employee.xlsx";
    XLSX.writeFile(wb, excelFileName);
    setExporting(false);
  };

  const parseTableData = (table: HTMLTableElement): string[][] => {
    const data: string[][] = [];

    // Include the header row
    const headerRowData: string[] = [];
    table.querySelectorAll("thead th").forEach((headerCell) => {
      headerRowData.push((headerCell as HTMLTableCellElement).innerText);
    });
    data.push(headerRowData);

    // Include the body rows
    const bodyRows = table.querySelectorAll("tbody tr");
    bodyRows.forEach((row) => {
      const rowData: string[] = [];
      row.querySelectorAll("td").forEach((cell) => {
        rowData.push((cell as HTMLTableCellElement).innerText);
      });
      data.push(rowData);
    });

    return data;
  };

  return (
    <>
      <div style={{ display: "none" }}>
        <div ref={tableRef}>
          <EmployeeTableFormat />
        </div>
      </div>
      <div
        className="d-flex justify-content-end"
        data-kt-user-table-toolbar="base"
      >
        {/* <Employee DataListFilter /> */}

        {/* begin::Export */}
        <button
          type="button"
          className="btn btn-light-primary me-3"
          onClick={handleExport}
          disabled={exporting}
        >
          <KTIcon iconName="exit-up" className="fs-2" />
          {exporting ? "Exporting..." : "Export"}
        </button>
        {/* end::Export */}

        {/* begin::Add user */}
        {/* <button type='button' className='btn btn-primary' onClick={openAddEmployeeModal}>
        <KTIcon iconName='plus' className='fs-2' />
        Add Employee
      </button> */}
        {/* end::Add user */}
      </div>
    </>
  );
};

export { EmployeeListToolbar };
