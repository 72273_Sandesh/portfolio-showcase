import {ID, Response} from '../../../../../../_metronic/helpers'
export type Grade = {
      id?: ID
      description?:string,
      shortDescription?: string,
      cadreCode?: string,
      status?:string,
      lveTableNumber?: string,
      inductionTableNumber?: string,
      noticePeriod?: number,
      changeDate?: Date,
      changeUser?:string,
      changeTerm?: string,
      approvalTableNumber?: string,
      accountCodeExp?: string,
      costCenterCodeExp?: string,
      gradeType?: string,
      udfNumber1?: number,
      gradeOrder?: number,
      gradeId?: string,
      changeReason?:string,
      cadreName?:string
      responseStatus?:string,
      message?:string,
      
}

export type GradeQueryResponse = Response<Array<Grade>>

export const initialGrade: Grade = {

}
