import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { isNotEmpty } from "../../../../../../_metronic/helpers";
import { initialGrade, Grade } from "../core/_models";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { GradeListLoading } from "../components/loading/GradeListLoading";
import { createGrade, updateGrade } from "../core/_requests";
import { useQueryResponse } from "../core/QueryResponseProvider";
import { globalVariable, setGlobalVariable } from "../../GlobalVariable";
import { toast } from "react-toastify";
import axios from "axios";

type Props = {
  isGradeLoading: boolean;
  grade: Grade;
};
const API_URL = import.meta.env.VITE_GRADE_DROPDOWNS_API_URL;

const editGradeSchema = Yup.object().shape({
  description: Yup.string()
    //.min(3, 'Minimum 3 characters')
    //.max(50, 'Maximum 50 characters')
    .required("Grade Name is required"),
  shortDescription: Yup.string()
    //.min(3, 'Minimum 3 characters')
    //.max(500, 'Maximum 500 characters')
    .required("Short Description is required"),
  cadreCode: Yup.string()
    //.min(3, 'Minimum 3 characters')
    //.max(500, 'Maximum 500 characters')
    .required("Cadre is required"),
  noticePeriod: Yup.number().required("Notice Period is required"),
  status: Yup.string().required("Status is required"),
  changeReason: Yup.string().min(3, "Minimum 3 characters"),
});

type states = {
  cadreCode: string;
  cadre: string;
};
const GradeEditModalForm: FC<Props> = ({ grade, isGradeLoading }) => {
  const { setItemIdForUpdate } = useListView();
  const { refetch } = useQueryResponse();

  const [cadreCodes, setCadreCodes] = useState<states[]>([]);
  const [cadreCode, setCadreCode] = useState("");

  const [statuses] = useState(["ACTIVE", "INACTIVE"]);
  const [status, setStatus] = useState("");

  const [newReason, setNewReason] = useState("");
  // const [ipAddress, setIpAddress] = useState<string>('');

  const { currentUser } = useAuth();

  useEffect(() => {
    fetchFromBackend()
      .then((cadreCodeData) => {
        setCadreCodes(cadreCodeData.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const fetchFromBackend = async () => {
    try {
      const response = await axios.get(API_URL);
      const data = await response.data;
      return data;
    } catch (error) {
      throw new Error("Failed to fetch grade from backend");
    }
  };

  const [gradeForEdit] = useState<Grade>({
    ...grade,
    description: grade.description || initialGrade.description,
    shortDescription: grade.shortDescription || initialGrade.shortDescription,
    status: grade.status || initialGrade.status || status,
    cadreCode: grade.cadreCode || initialGrade.cadreCode || cadreCode,
    noticePeriod: grade.noticePeriod || initialGrade.noticePeriod,
    changeDate: grade.changeDate || initialGrade.changeDate,
    changeUser: grade.changeUser || initialGrade.changeUser,
    changeTerm: grade.changeTerm || initialGrade.changeTerm,
  });
  const fetchIpAddress = async () => {};

  fetchIpAddress();
  function formatDate(dateTime: string) {
    const date = new Date(dateTime);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata", // Specify the Indian time zone
    });
    return `${formattedDate} ${formattedTime}`;
  }
  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };

  const formik = useFormik({
    initialValues: gradeForEdit,
    validationSchema: editGradeSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);
      try {
        if (isNotEmpty(values.id) && globalVariable === "editGrade") {
          values.changeUser = currentUser?.first_name;
          values.changeTerm = "TERM";
          values.changeDate = new Date();
          const response = await updateGrade(values);
          if (response?.responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>{response.message}</h6>
                </p>
              </div>
            );
          } else {
            setItemIdForUpdate(undefined);
            setGlobalVariable("addGrade");
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>{response?.message}</h6>
                </p>
              </div>
            );
            cancel(true);
          }
        } else {
          values.id = "null";
          values.changeUser = currentUser?.first_name;
          values.changeTerm = "TERM";
          values.changeDate = new Date();

          const response = await createGrade(values);
          if (response?.responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>{response.message}</h6>
                </p>
              </div>
            );
          } else {
            setItemIdForUpdate(undefined);
            setGlobalVariable("addGrade");
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>{response?.message}</h6>
                </p>
              </div>
            );
            cancel(true);
          }
        }
      } catch (error) {
        console.error("Error posting data:", error);
        toast.error(
          <div>
            <h6 className="alert-heading">Error Posting Data</h6>
            <p className="alert alert-danger" role="alert">
              <h6>{String(error)}</h6>
            </p>
          </div>
        );
      } finally {
        setSubmitting(false);
      }
    },
  });

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column " id="kt_modal_add_user_scroll">
          {globalVariable !== "addGrade" && (
            <div className="row mb-3">
              <div className="fv-row">
                <label className=" fw-bold fs-6 mb-2">Grade Code :</label>
                <input
                  className="form-control form-control-solid mb-3 mb-lg-0"
                  value={formik.values.id ? formik.values.id : ""}
                  disabled={
                    globalVariable === "infoGrade" ||
                    globalVariable === "editGrade"
                  }
                />
              </div>
            </div>
          )}
          <div className="row mb-3">
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">Grade Name</label>
                <input
                  placeholder="Description"
                  {...formik.getFieldProps("description")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "description",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.description && formik.errors.description,
                    },
                    {
                      "is-valid":
                        formik.touched.description &&
                        !formik.errors.description,
                    }
                  )}
                  type="description"
                  name="description"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    globalVariable === "infoDesign"
                  }
                />
                {formik.touched.description && formik.errors.description && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.description}</span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Short Description
                </label>
                <input
                  placeholder="Short Description"
                  {...formik.getFieldProps("shortDescription")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "shortDescription",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.shortDescription &&
                        formik.errors.shortDescription,
                    },
                    {
                      "is-valid":
                        formik.touched.shortDescription &&
                        !formik.errors.shortDescription,
                    }
                  )}
                  type="shortDescription"
                  name="shortDescription"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    globalVariable === "infoDesign"
                  }
                />
                {formik.touched.shortDescription &&
                  formik.errors.shortDescription && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.shortDescription}</span>
                    </div>
                  )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required  fs-6 fw-bold">Cadre</label>
                <select
                  name="cadreCode"
                  // className='form-select form-select-solid fw-bolder'
                  className={clsx(
                    "form-select form-select-solid fw-bolder",
                    {
                      "is-invalid":
                        formik.touched.cadreCode && formik.errors.cadreCode,
                    },
                    {
                      "is-valid":
                        formik.touched.cadreCode && !formik.errors.cadreCode,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("cadreCode", e.target.value);
                    setCadreCode(e.target.value);
                  }}
                  value={cadreCode}
                  disabled={globalVariable === "infoGrade"}
                >
                  <option value="">{formik.values.cadreName}</option>
                  {cadreCodes.map((cadreCode) => (
                    <option
                      key={cadreCode.cadreCode}
                      value={cadreCode.cadreCode}
                    >
                      {cadreCode.cadre.toUpperCase()}
                    </option>
                  ))}
                </select>
                {formik.touched.cadreCode && formik.errors.cadreCode && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.cadreCode}</span>
                  </div>
                )}
              </div>
            </div>

            <div className="col-md-6">
              <div className="fv-row">
                <label className=" fw-bold fs-6 mb-2">
                  Notice Period(Days)
                </label>
                <input
                  placeholder="Notice Period"
                  {...formik.getFieldProps("noticePeriod")}
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.noticePeriod &&
                        formik.errors.noticePeriod,
                    },
                    {
                      "is-valid":
                        formik.touched.noticePeriod &&
                        !formik.errors.noticePeriod,
                    }
                  )}
                  type="number"
                  name="noticePeriod"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    globalVariable === "infoGrade"
                  }
                />
                {formik.touched.noticePeriod && formik.errors.noticePeriod && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.noticePeriod}</span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="fw-bold fs-6 mb-2">Status</label>
                <select
                  // className='form-select form-select-solid fw-bolder'
                  className={clsx(
                    "form-select form-select-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                    setStatus(e.target.value);
                  }}
                  value={status}
                  disabled={globalVariable === "infoGrade"}
                >
                  {globalVariable === "addGrade" ? (
                    <option value="">{formik.values.status}</option>
                  ) : (
                    <option value="">
                      {formik.values.status === "A"
                        ? "ACTIVE"
                        : formik.values.status === "N"
                        ? "INACTIVE"
                        : ""}
                    </option>
                  )}
                  {statuses.map((status) => (
                    <option key={status} value={status}>
                      {status}
                    </option>
                  ))}
                </select>
                {formik.touched.status && formik.errors.status && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.status}</span>
                  </div>
                )}
              </div>
            </div>
            {globalVariable === "infoGrade" && (
              <>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className="fw-bold fs-6 mb-2">Change User</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      value={formik.values.changeUser?.toUpperCase()}
                      disabled={globalVariable === "infoGrade"}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className="fw-bold fs-6 mb-2">Change Term</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      value={formik.values.changeTerm}
                      //value={ipAddress}
                      disabled={globalVariable === "infoGrade"}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className=" fw-bold fs-6 mb-2">Change Date</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      // value={formik.values.changeDate}
                      // value={formik.values.changeDate ? formatDate(formik.values.changeDate) : ''}
                      // value={formik.values.changeDate ? formik.values.changeDate.toISOString() : ''}
                      value={
                        formik.values.changeDate
                          ? formatDate(formik.values?.changeDate.toString())
                          : ""
                      }
                      disabled={globalVariable === "infoGrade"}
                    />
                  </div>
                </div>
              </>
            )}
          </div>
          {globalVariable !== "addGrade" && (
            <div className="row mb-3">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Change Reason
                </label>
                <textarea
                  placeholder="Change Reason"
                  //value={{...formik.getFieldProps('changeReason')}}
                  {...(globalVariable === "infoGrade"
                    ? { ...formik.getFieldProps("changeReason") }
                    : { newReason })}
                  onChange={(e) => {
                    formik.setFieldValue(
                      "changeReason",
                      e.target.value.toUpperCase()
                    );
                    setNewReason(e.target.value.toUpperCase());
                  }}
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.changeReason &&
                        formik.errors.changeReason,
                    },
                    {
                      "is-valid":
                        formik.touched.changeReason &&
                        !formik.errors.changeReason,
                    }
                  )}
                  // type='text'
                  name="changeReason"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    globalVariable === "infoGrade"
                  }
                />
                {formik.touched.changeReason && formik.errors.changeReason && (
                  <div className="fv-plugins-message-container">
                    <span role="alert">{formik.errors.changeReason}</span>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
        <div className="text-center pt-15">
          {/* <button
            type='reset'
            onClick={() => cancel()}
            className='btn btn-light me-3'
            disabled={formik.isSubmitting || isGradeLoading}
          >
            Cancel
          </button> */}
          {globalVariable !== "infoGrade" && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isGradeLoading ||
                formik.isSubmitting ||
                // !formik.isValid ||
                !formik.touched ||
                (globalVariable !== "addGrade" && newReason === "")
              }
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isGradeLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isGradeLoading) && <GradeListLoading />}
    </>
  );
};

export { GradeEditModalForm };
