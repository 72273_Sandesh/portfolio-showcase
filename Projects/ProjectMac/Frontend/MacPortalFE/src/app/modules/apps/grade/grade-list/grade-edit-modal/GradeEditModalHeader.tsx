import {KTIcon} from '../../../../../../_metronic/helpers'
import { globalVariable, setGlobalVariable } from '../../GlobalVariable'
import {useListView} from '../core/ListViewProvider'

const GradeEditModalHeader = () => {
  const {setItemIdForUpdate} = useListView()
  const openAddModal = () => {
    setItemIdForUpdate(undefined)
    setGlobalVariable("addGrade")
  }
  return (
    <div className='modal-header'>
      {/* begin::Modal title */}
      {globalVariable == 'addGrade' && (<h2 className='fw-bolder'>Add New Grade</h2>)
     || globalVariable == 'editGrade' && (<h2 className='fw-bolder'>Edit Grade</h2>)
      || globalVariable == 'infoGrade' && (<h2 className='fw-bolder'>Grade Details</h2> )}
      {/* end::Modal title */}

      {/* begin::Close */}
      <div
        className='btn btn-icon btn-sm btn-active-icon-primary'
        data-kt-users-modal-action='close'
        onClick={openAddModal}
        style={{cursor: 'pointer'}}
      >
        <KTIcon iconName='cross' className='fs-1' />
      </div>
      {/* end::Close */}
    </div>
  )
}

export {GradeEditModalHeader}
