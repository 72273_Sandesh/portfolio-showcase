import {ListViewProvider, useListView} from './core/ListViewProvider'
import {QueryRequestProvider} from './core/QueryRequestProvider'
import {QueryResponseProvider} from './core/QueryResponseProvider'
import {QualificationListHeader} from './components/header/QualificationListHeader'
import {QualificationTable} from './table/QualificationTable'
import {QualificationEditModal} from './qualification-edit-modal/QualificationEditModal'
import {KTCard} from '../../../../../_metronic/helpers'
import { ToolbarWrapper } from '../../../../../_metronic/layout/components/toolbar'
import { Content } from '../../../../../_metronic/layout/components/content'

const QualificationList = () => {
  const {itemIdForUpdate} = useListView()
  return (
    <>
      <KTCard>
        <QualificationListHeader />
        <QualificationTable />
      </KTCard>
      {itemIdForUpdate !== undefined && <QualificationEditModal />}
    </>
  )
}

const QualificationListWrapper = () => (
  <QueryRequestProvider>
    <QueryResponseProvider>
      <ListViewProvider>
        <ToolbarWrapper />
        <Content>
          <QualificationList />
        </Content>
      </ListViewProvider>
    </QueryResponseProvider>
  </QueryRequestProvider>
)

export {QualificationListWrapper}
