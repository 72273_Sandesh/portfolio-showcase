import { useQueryResponseData } from '../../core/QueryResponseProvider';

const QualificationTableFormat = () => {
  const qualifications = useQueryResponseData(); //<Array<Department>>

    return (
  
        <table id="table-to-export">
        <thead>
          <tr>
          <th>id</th>
          <th>name</th>
          <th>shortdescr</th>
          <th>eqival</th>
          <th>type</th>
          <th>changeDate</th>
          <th>changeUser</th>
          <th>changeTerm</th>
          <th>status</th>
          <th>qualSalStrucDesc</th>
          <th>changeReason</th>
           
          </tr>
        </thead>
        <tbody>
          {qualifications.map(qualification => (
            <tr key={qualification.id}>
                <td>{qualification.id}</td>
                <td>{qualification.name}</td>
                <td>{qualification.shortdescr}</td>
                <td>{qualification.eqival}</td>
                <td>{qualification.type}</td> 
                <td>{qualification.changeDate?.toString()}</td>
                <td>{qualification.changeUser}</td>
                <td>{qualification.changeTerm}</td>
                <td>{qualification.status}</td>
                <td>{qualification.qualSalStrucDesc}</td>
                <td>{qualification.changeReason}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  
};

export default QualificationTableFormat;
