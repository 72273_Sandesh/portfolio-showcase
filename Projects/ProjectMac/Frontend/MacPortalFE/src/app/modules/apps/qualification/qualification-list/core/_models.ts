import { ID, Response } from "../../../../../../_metronic/helpers";
export type Qualification = {
  id?: ID;

  name?: string;
  shortdescr?: string;
  eqival?: string;
  type?: string;
  changeDate?: Date;
  changeUser?: string;
  changeTerm?: string;
  status?: string;
  qualSalStrucDesc?: string;
  changeReason?: string;
  responseStatus?: string;
  message?: string;
};

export type QualificationQueryResponse = Response<Array<Qualification>>;

export const initialQualification: Qualification = {};
