import clsx from 'clsx'
import {FC} from 'react'
import {Row} from 'react-table'
import {Qualification} from '../../core/_models'

type Props = {
  row: Row<Qualification>
}

const CustomRow: FC<Props> = ({row}) => (
  <tr {...row.getRowProps()}>
    {row.cells.map((cell) => {
      return (
        <td
          {...cell.getCellProps()}
          className={clsx({'justify-content:left min-w-100px': cell.column.id === 'actions'})}
        >
          {cell.render('Cell')}
        </td>
      )
    })}
  </tr>
)

export {CustomRow}
