import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { KTIcon } from "../../../_metronic/helpers";
import { Link, useLocation } from "react-router-dom";
import { ToolbarWrapper } from "../../../_metronic/layout/components/toolbar";
import { Content } from "../../../_metronic/layout/components/content";
import Confetti from "react-confetti";
import {
  employeeIdForView,
  globalQuery,
  setEmployeeIdForView,
} from "../GlobalQuery";
import axios from "axios";

interface ProfileHeader {
  emP_IMAGE: string;
  emP_NAME:string;
  emP_FNAME:string;
  emP_MNAME:string;
  emP_LNAME:string;
  birtH_DATE:string;
  shorT_NAME:string;
  designation:string;
  grade:string;
  gradE_NAME:string;
  sitE_NAME:string;
  depT_CODE:string;
  depT_NAME:string;
  datE_JOIN:string;
  resI_DATE:string;
  relievE_DATE:string;
  qual:string;
  toT_EXP:string;
  maC_EXP:string;
  mobilE_NO:string;
  mobilE_NO_OFF:string;
  employmenT_TYPE:string;
  gender:string;
  gendeR_DESCR:string;
  emP_SITE:string;
  emP_TYPE:string;
  contacT_CODE:string;
  namE_PREFIX:string;
  m_STATUS_DATE:string;
  m_STATUS:string;
  m_STATUS_DESCR:string;
  contacT_CITY:string;
  contacT_STATE:string;
  contacT_PIN:string;
  religion:string;
  iT_NO:string;
  emaiL_ID_OFF:string;
  peR_ADD1:string;
  peR_ADD2:string;
  peR_ADD3:string;
  cuR_ADD1:string;
  cuR_ADD2:string;
  cuR_PIN:string;
  cuR_ADD3:string;
  cuR_TEL1:string;
  cuR_CITY:string;
  cuR_TEL2:string;
  cuR_TEL3:string;
  emaiL_ID_PER:string;
  contacT_TEL:string;
  contacT_PERS:string;
  blooD_GRP:string;
  section:string;
  probatioN_PRD:string;
  traininG_PRD:string;
  probatioN_DATE:string;
  datE_CONF:string;
  probation:string;
  confirmed:string;
  banK_ACCT:string;
  banK_CODE:string;
  banK_NAME:string;
  banK_IFSC:string;
  cadre:string;
  contacT_ADD1:string;
  contacT_ADD2:string;
  contacT_ADD3:string;
  peR_PIN:string;
  peR_TEL:string;
  paY_MODE:string;
  paymenT_MODE:string;
  cuR_STATE:string;
  peR_STATE:string;
  peR_CITY:string;
  crediT_CARD_NO:string;
  pF_UN_NO:string;
  aadhaaR_NO:string;
  driV_LIC_NO:string;
  electioN_CARD_NO:string;
  fatheR_NAME:string;
  motheR_NAME:string;
  passporT_NO:string;
  passporT_ISS_PLACE:string;
  passporT_ISS_DATE:string;
  passporT_EXP_DATE:string;
  cuR_STATE_DESCR:string;
  peR_STATE_DESCR:string;
  contacT_STATE_DESCR:string;
  proB_EXT:string;
  conF_EXT:string;
  curR_CTC:string;
  joininG_CTC:string;
  paN_EMP_NAME:string;
  aadhaR_EMP_NAME:string;
  joineD_AS:string;
  reporT_TO:string;
  noticE_PRD:string;
  worK_SITE:string;
  gratuitY_DATE:string;
  basic:string;
  paY_TYPE:string;
  dD_PAYABLE_BANK:string;
  pF_NO:string;
  fpF_NO:string;
  esiC_NO:string;
  paY_SITE:string;
  emP_CODE:string;
  
}

const ProfileHeader: React.FC = () => {
  const location = useLocation();
  const [isSubMenuOpen, setIsSubMenuOpen] = useState(false);

  const [completion, setCompletion] = useState(50); // Assuming initial completion is 90%
  const [showConfetti, setShowConfetti] = useState(false);

  const navigate = useNavigate();


  const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
  // const [profileHeader, setProfileHeader] = useState([]);
  const [profileHeader, setProfileHeader] = useState<ProfileHeader | null>(null);


  useEffect(() => {
    console.log("UemployeeIdForView:", employeeIdForView);
    console.log("globalQuery:", globalQuery);

    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setProfileHeader(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });

    const receivedCompletion = 0 | 90 | 100;

    if (receivedCompletion !== 0) {
      if (receivedCompletion === 90) {
        setCompletion(receivedCompletion);
        setShowConfetti(true);
        setTimeout(() => {
          setShowConfetti(false);
        }, 9000);
      }
    }
  }, []);

  const handleClose = () => {
    setEmployeeIdForView("");
    navigate("/apps/employee/employees");
  };
  return (
    <>
      <ToolbarWrapper />

      <Content>
        <div className="card mb-5 mb-xl-2">
          <div className="card-body pt-9 pb-0">
            <div className="d-flex flex-wrap flex-sm-nowrap mb-3">
              <div className="me-7 mb-4">
              <div className="symbol symbol-100px symbol-lg-140px symbol-fixed position-relative">
  {profileHeader?.emP_IMAGE ? (
    <img
      src={profileHeader.emP_IMAGE}
      alt="User"
    />
  ) : (
    <img
      src="/media/avatars/User.png"
      alt="Default User"
    />
  )}
  <div className="position-absolute translate-middle bottom-0 start-100 mb-1 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
</div>

              </div>

              <div className="flex-grow-1">
                <div className="d-flex justify-content-between align-items-start flex-wrap mb-2">
                  <div className="d-flex flex-column">
                    <div className="d-flex align-items-center mb-2">
                      <a
                        href="#"
                        className="text-gray-800 text-hover-primary fs-2 fw-bolder me-1"
                      >
                        {/* Max Smith */}
                        {profileHeader?.emP_FNAME}{" "}
                        {profileHeader?.emP_LNAME}
                      </a>
                      <a href="#">
                        <KTIcon
                          iconName="verify"
                          className="fs-1 text-success"
                        />
                      </a>
                    </div>

                    <div className="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
                      <a
                        href="#"
                        className="d-flex align-items-center text-gray-500 text-hover-primary me-5 mb-2"
                      >
                        <KTIcon
                          iconName="profile-circle"
                          className="fs-4 me-1"
                        />
                        {profileHeader?.emP_CODE}
                        {/* designation  */}
                      </a>
                      <a
                        href="#"
                        className="d-flex align-items-center text-gray-500 text-hover-primary me-5 mb-2"
                      >
                        <KTIcon iconName="geolocation" className="fs-4 me-1" />
                        {profileHeader?.depT_NAME}
                        {/* dept */}
                      </a>
                      <a
                        href="#"
                        className="d-flex align-items-center text-gray-500 text-hover-primary mb-2"
                      >
                        <KTIcon iconName="sms" className="fs-4 me-1" />
                        {profileHeader?.emaiL_ID_OFF}
                        {/* email */}
                      </a>
                    </div>
                  </div>

                  <div className="d-flex align-items-center justify-content-center w-100vh">
                    <div className="d-flex align-items-center w-250px w-sm-295px flex-column ">
                      <div className="d-flex justify-content-between w-100 mt-auto mb-2">
                        <span className="fw-bold fs-6 text-gray-500">
                          Profile Completion
                        </span>
                        <span className="fw-bolder fs-6">{completion}%</span>
                      </div>
                      <div className="h-5px mx-3 w-100 bg-light mb-3">
                        {/* Here we set the completion value dynamically */}
                        <div
                          className="bg-success rounded h-5px"
                          role="progressbar"
                          style={{ width: `${completion}%` }}
                        ></div>
                      </div>
                    </div>
                  </div>
                  <div className="d-flex my-4">
                    <div className="d-flex my-2">
                      {/* <div
                        className="btn btn-icon btn-sm  btn-light-danger position-absolute top-0 end-0 "
                        data-kt-users-modal-action="close"
                        onClick={handleClose}
                        style={{ cursor: "pointer" }}
                      >
                        <KTIcon iconName="cross" className="fs-1" />
                      </div> */}
                      <div
                        className="btn  btn-sm btn-light-danger position-absolute"
                        data-kt-users-modal-action="close"
                        onClick={handleClose}
                        style={{ cursor: "pointer", top: '-50px', right: '0px' }}
                      >
                      Close
                      </div>
                    </div>
                  </div>
                </div>

                <div className="d-flex flex-wrap flex-stack">
                  <div>
                    {showConfetti && (
                      <div
                        style={{
                          position: "fixed",
                          top: 0,
                          left: 0,
                          width: "100vw",
                          height: "100vh",
                          zIndex: 999,
                        }}
                      >
                        <Confetti />
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>

            <div className="d-flex overflow-auto h-55px">
              <ul className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
                <li
                  className="nav-item"
                  onMouseEnter={() => setIsSubMenuOpen(true)}
                  onMouseLeave={() => setIsSubMenuOpen(false)}
                >
                  {/* Link to trigger the submenu */}
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname ===
                        "/crafted/pages/profile/overview" && "active")
                    }
                    to="/crafted/pages/profile/overview"
                  >
                    Personal
                  </Link>
                </li>

                <li className="nav-item">
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname === "/crafted/pages/profile/family" &&
                        "active")
                    }
                    to="/crafted/pages/profile/family"
                  >
                    Family
                  </Link>
                </li>

                <li className="nav-item">
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname ===
                        "/crafted/pages/profile/documents" && "active")
                    }
                    to="/crafted/pages/profile/documents"
                  >
                    Qualification
                  </Link>
                </li>
                <li className="nav-item">
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname ===
                        "/crafted/pages/profile/connections" && "active")
                    }
                    to="/crafted/pages/profile/connections"
                  >
                    Experience
                  </Link>
                </li>
                <li className="nav-item">
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname ===
                        "/crafted/pages/profile/attachments" && "active")
                    }
                    to="/crafted/pages/profile/attachments"
                  >
                    Attachments
                  </Link>
                </li>
                <li className="nav-item">
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname === "/crafted/pages/profile/assets" &&
                        "active")
                    }
                    to="/crafted/pages/profile/assets"
                  >
                    Assets
                  </Link>
                </li>

                <li className="nav-item">
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname ===
                        "/crafted/pages/profile/Separation" && "active")
                    }
                    to="/crafted/pages/profile/Separation"
                  >
                    Separation
                  </Link>
                </li>

                <li className="nav-item">
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname ===
                        "/crafted/pages/profile/transferhistory" && "active")
                    }
                    to="/crafted/pages/profile/transferhistory"
                  >
                    Journey
                  </Link>
                </li>

                <li className="nav-item">
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname ===
                        "/crafted/pages/profile/recruitment" && "active")
                    }
                    to="/crafted/pages/profile/recruitment"
                  >
                    Recruitment
                  </Link>
                </li>

                <li className="nav-item">
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname ===
                        "/crafted/pages/profile/salComparison" && "active")
                    }
                    to="/crafted/pages/profile/salComparison"
                  >
                    Salary Comparison
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </Content>
    </>
  );
};
export { ProfileHeader };
