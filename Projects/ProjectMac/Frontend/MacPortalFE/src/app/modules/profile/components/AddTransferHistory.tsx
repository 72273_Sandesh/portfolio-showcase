import { Content } from "../../../../_metronic/layout/components/content";
import TransferHistoryAdd from "./TransferHistoryAdd";

export function AddTransferHistory() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <TransferHistoryAdd />
      </div>
    </Content>
  );
}
