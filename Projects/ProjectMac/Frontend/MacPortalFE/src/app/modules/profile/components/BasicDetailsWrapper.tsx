import { Content } from "../../../../_metronic/layout/components/content";
import { BasicDetails } from "./BasicDetails";
import { PersonalDetails } from "./PersonalDetails";

export function BasicDetailsWrapper() {
  return (
    <Content>
      <>
        <div className="row">
          <div>
            <PersonalDetails />
          </div>

          <div>
            <BasicDetails />
          </div>
        </div>
      </>
    </Content>
  );
}
