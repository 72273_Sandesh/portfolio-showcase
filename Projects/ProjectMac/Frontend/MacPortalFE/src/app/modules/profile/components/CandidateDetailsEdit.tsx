import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

interface Response {
  responseStatus: string;
  message: string;
}

const CandidateDetailsEdit: React.FC = () => {
  const [employee, setEmployee] = useState<any>({});
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/CANDIDATE_DET`)
      .then((response) => {
        setEmployee(response.data[0]);
        // Update formik's initial values after fetching employee data
        formik.setValues({
          candidatE_NAME: response.data[0].candidatE_NAME || "",
          sitE_DESCR: response.data[0].sitE_DESCR || "",
          toT_EXP: response.data[0].toT_EXP || "",
          receiveD_DATE: response.data[0].receiveD_DATE || "",
          curR_COMPANY: response.data[0].curR_COMPANY || "",
          curR_DEPARTMENT: response.data[0].curR_DEPARTMENT || "",
          curR_DESIGNATION: response.data[0].curR_DESIGNATION || "",
          curR_CTC: response.data[0].curR_CTC || "",
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    candidatE_NAME: Yup.string().required("Candidate Name required"),
    sitE_DESCR: Yup.string().required("Location required"),
    toT_EXP: Yup.string().required("Experience required"),
    receiveD_DATE: Yup.string().required("Resume Received Date required"),
    curR_COMPANY: Yup.string().required("Current Company required"),
    curR_DEPARTMENT: Yup.string().required("Current Department required"),
    curR_DESIGNATION: Yup.string().required("Current Designation required"),
    curR_CTC: Yup.string().required("Current CTC required"),
  });

  const formik = useFormik({
    initialValues: {
      candidatE_NAME: "",
      sitE_DESCR: "",
      toT_EXP: "",
      receiveD_DATE: "",
      curR_COMPANY: "",
      curR_DEPARTMENT: "",
      curR_DESIGNATION: "",
      curR_CTC: "",
    },
    validationSchema: validationSchema,
    // onSubmit: (values) => {
    //   console.log('Submitting form data:', values);
    //   // Implement your submit logic here, e.g., send updated data to the server

    //   setEmployee({ sitE_NAME: '', depT_NAME: '' ,designation:'',section:""});
    //   formik.resetForm();// Upon successful submission, clear the form
    // },

    onSubmit: (values, formik) => {
      console.log("Submitting form data:", values);

      axios
        .post<Response>("your_backend_endpoint", values)
        .then((response) => {
          console.log("Post successful!", response.data);

          if (response.data.responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(response.data.message)
                      ? response.data.message.join("\n")
                      : response.data.message}
                  </h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(response.data.message)
                      ? response.data.message.join("\n")
                      : response.data.message}
                  </h6>
                </p>
              </div>
            );
          }

          setEmployee({
            candidatE_NAME: "",
            sitE_DESCR: "",
            toT_EXP: "",
            receiveD_DATE: "",
            curR_COMPANY: "",
            curR_DEPARTMENT: "",
            curR_DESIGNATION: "",
            curR_CTC: "",
          });
          formik.resetForm();
          navigate(-1);
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );

          formik.resetForm();
        });
    },
  });

  const handleClose = () => {
    navigate(-1);
    // navigate("/crafted/pages/profile/overview");
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-warning card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Candidate Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="candidatE_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CANDIDATE NAME :
                </label>
                <input
                  type="text"
                  id="candidatE_NAME"
                  className="form-control form-control-lg form-control-solid"
                  name="candidatE_NAME"
                  placeholder="Enter location name"
                  value={formik.values?.candidatE_NAME}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sitE_DESCR"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  LOCATION :
                </label>
                <input
                  type="text"
                  id="sitE_DESCR"
                  className="form-control form-control-lg form-control-solid"
                  name="sitE_DESCR"
                  placeholder="Enter Location Name"
                  value={formik.values.sitE_DESCR?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.candidatE_NAME &&
                formik.errors.candidatE_NAME ? (
                  <div className="text-danger">
                    {formik.errors.candidatE_NAME}
                  </div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.sitE_DESCR && formik.errors.sitE_DESCR ? (
                  <div className="text-danger">{formik.errors.sitE_DESCR}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="toT_EXP"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  EXPERIENCE :
                </label>
                <input
                  type="text"
                  id="toT_EXP"
                  className="form-control form-control-lg form-control-solid"
                  name="toT_EXP"
                  placeholder="Enter Experience name"
                  value={formik.values?.toT_EXP}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  RESUME RECEIVED DATE :
                </label>
                <input
                  type="date"
                  className="form-control form-control-lg form-control-solid"
                  name="receiveD_DATE"
                  placeholder="Enter Resume Received Date name"
                  value={
                    formik.values?.receiveD_DATE
                      ? new Date(formik.values?.receiveD_DATE)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.toT_EXP && formik.errors.toT_EXP ? (
                  <div className="text-danger">{formik.errors.toT_EXP}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.receiveD_DATE && formik.errors.receiveD_DATE ? (
                  <div className="text-danger">
                    {formik.errors.receiveD_DATE}
                  </div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="curR_COMPANY"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CURRENT COMPANY:
                </label>
                <input
                  type="text"
                  id="curR_COMPANY"
                  className="form-control form-control-lg form-control-solid"
                  name="curR_COMPANY"
                  placeholder="Enter Current Company name"
                  value={formik.values.curR_COMPANY?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  CURRENT DEPARTMENT:
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="curR_DEPARTMENT"
                  placeholder="Enter Current Department name"
                  value={formik.values.curR_DEPARTMENT?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.curR_COMPANY && formik.errors.curR_COMPANY ? (
                  <div className="text-danger">
                    {formik.errors.curR_COMPANY}
                  </div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.curR_DEPARTMENT &&
                formik.errors.curR_DEPARTMENT ? (
                  <div className="text-danger">
                    {formik.errors.curR_DEPARTMENT}
                  </div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="curR_DESIGNATION"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CURRENT DESIGNATION:
                </label>
                <input
                  type="text"
                  id="curR_DESIGNATION"
                  className="form-control form-control-lg form-control-solid"
                  name="curR_DESIGNATION"
                  placeholder="Enter Current Designation"
                  value={formik.values?.curR_DESIGNATION}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  CURRENT CTC :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="curR_CTC"
                  placeholder="Enter Current CTC"
                  value={formik.values.curR_CTC}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.curR_DESIGNATION &&
                formik.errors.curR_DESIGNATION ? (
                  <div className="text-danger">
                    {formik.errors.curR_DESIGNATION}
                  </div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.curR_CTC && formik.errors.curR_CTC ? (
                  <div className="text-danger">{formik.errors.curR_CTC}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit is HERE */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default CandidateDetailsEdit;
