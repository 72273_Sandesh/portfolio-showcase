import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { KTIcon } from "../../../../_metronic/helpers";
import { employeeIdForView } from "../../GlobalQuery";
import { ToastContainer } from "react-toastify";
import { InternalMode , ThemeModeComponent } from "../../../../_metronic/assets/ts/layout";

export function ContactDetails() {
  const [basicDetails, setBasicDetails] = useState<any>([]);
  const [isMinimized, setIsMinimized] = useState(false); // Default state: card is minimized
  const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
  const currentMode: InternalMode = ThemeModeComponent.getCurrentMode();
  useEffect(() => {
    // Fetch basic details from the backend API
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setBasicDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  // const toggleMinimize = () => {
  //   setIsMinimized(!isMinimized); // Toggle minimized/maximized state
  // };

  return (
    <div
      className={`card`}
      id="Contact Details"
      style={(currentMode === 'dark' ? {background: "#0F1014"} : { background: "#F9F9FA"})}
    >

      <ToastContainer/>
      {/* <div
        className="card-header d-flex justify-content-between align-items-center cursor-pointer"
        // onClick={toggleMinimize}
      > */}
        {/* <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Contact Details</h3>
        </div> */}
        {/* <Link
          to="/crafted/pages/profile/EditBasicDetails"
          className="btn btn-sm btn-primary me-3 align-self-center"
        >
          Edit
        </Link> */}

        {/* <button
          className={`btn btn-sm mt-3 btn-light btn-icon rounded-circle ${
            isMinimized ? "" : "active"
          }`}
          aria-label={isMinimized ? "Expand" : "Collapse"}
        >
          <KTIcon
            iconName={isMinimized ? "arrow-down" : "arrow-up"}
            className={`fs-3 ${isMinimized ? "text-dark" : "text-dark"}`}
          />
        </button> */}
      {/* </div> */}

      {!isMinimized && (
        <div className="cardrow g-2 mb-2 g-xxl-2">
           <div className={"rounded " + (currentMode === 'dark' ? "bg-light" : "bg-white")}>
            <div className="card-header d-flex bg-light-primary justify-content-between align-items-left cursor-pointer">
              <div className="card-title m-0 ">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                  Contact Data
                </h3>
              </div>
              {/* <Link
                to="/crafted/pages/profile/EditUnofficialDetails"
                className="btn btn-sm btn-primary me-3 align-self-center"
              >
                Edit
              </Link> */}
            </div>
            <div className="card-body p-9">
              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">
                    PERSONAL MOBILE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails.mobilE_NO}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">
                    PERSONAL MAIL:{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails.emaiL_ID_PER.toUpperCase()} */}
                    {basicDetails?.emaiL_ID_PER
                      ? basicDetails.emaiL_ID_PER.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">
                    OFFICIAL MOBILE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.mobilE_NO_OFF}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">
                    OFFICIAL MAIL :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails.emaiL_ID_OFF.toUpperCase()} */}
                    {basicDetails?.emaiL_ID_OFF
                      ? basicDetails.emaiL_ID_OFF.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}
            </div>
          </div>
        </div>
      )}
   {/* <hr className="bg-dark" /> */}
      {!isMinimized && (
        <div className="cardrow g-2 mb-2 g-xxl-2">
           <div className={"rounded " + (currentMode === 'dark' ? "bg-light" : "bg-white")}>
            <div className="card-header d-flex bg-light-primary justify-content-between align-items-left cursor-pointer">
              <div className="card-title m-0">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                  Present Details
                </h3>
              </div>
              <Link
                to="/crafted/pages/profile/EditPresentDetails"
                className="btn btn-sm btn-primary me-3 align-self-center"
              >
                Edit
              </Link>
            </div>
            <div className="card-body p-9">
              {/* Begin:first Row     */}
              <div className="row">
                <div
                  className="col-lg-6 mb-6"
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "flex-start",
                  }}
                >
                  <label className="col-lg-4 fw-bold text-muted">
                    PRESENT ADDRESS :
                  </label>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "flex-start",
                    }}
                  >
                    <span
                      className="fw-bold fs-6 text-gray-900 text-hover-primary"
                      style={{ marginBottom: "-14px" }}
                    >
                      {/* {basicDetails.cuR_ADD1} */}
                      {basicDetails?.cuR_ADD1
                        ? basicDetails.cuR_ADD1.toUpperCase()
                        : ""}
                    </span>
                    <br />
                    <span
                      className="fw-bold fs-6 text-gray-900 text-hover-primary"
                      style={{ marginBottom: "-14px" }}
                    >
                      {/* {basicDetails.cuR_ADD2} */}
                      {basicDetails?.cuR_ADD2
                        ? basicDetails.cuR_ADD2.toUpperCase()
                        : ""}
                    </span>
                    <br />
                    <span
                      className="fw-bold fs-6 text-gray-900 text-hover-primary"
                      style={{ marginBottom: "-14px" }}
                    >
                      {/* {basicDetails.cuR_ADD3} */}
                      {basicDetails?.cuR_ADD3
                        ? basicDetails.cuR_ADD3.toUpperCase()
                        : ""}
                    </span>
                    <br />
                  </div>
                </div>

                <div
                  className="col-lg-6 mb-6"
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "flex-start",
                  }}
                >
                  <label className="col-lg-4 fw-bold text-muted">
                    TELEPHONE :
                  </label>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "flex-start",
                    }}
                  >
                    <span
                      className="fw-bold fs-6 text-gray-900 text-hover-primary"
                      style={{ marginBottom: "-14px" }}
                    >
                      {basicDetails.cuR_TEL1}
                    </span>
                    <br />
                    <span
                      className="fw-bold fs-6 text-gray-900 text-hover-primary"
                      style={{ marginBottom: "-14px" }}
                    >
                      {basicDetails.cuR_TEL2}
                    </span>
                    <br />
                    <span
                      className="fw-bold fs-6 text-gray-900 text-hover-primary"
                      style={{ marginBottom: "-14px" }}
                    >
                      {basicDetails.cuR_TEL3}
                    </span>
                    <br />
                  </div>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  {/* <label className="col-lg-4 fw-bold text-muted">
                    MOBILE NO :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails.mobilE_NO}
                  </span> */}

                  <label className="col-lg-4 fw-bold text-muted">CITY : </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails.cuR_CITY} */}
                    {basicDetails?.cuR_CITY
                      ? basicDetails.cuR_CITY.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">
                    STATE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails.cuR_STATE_DESCR} */}
                    {basicDetails?.cuR_STATE_DESCR
                      ? basicDetails.cuR_STATE_DESCR.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">PIN : </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails.cuR_PIN}
                  </span>
                </div>
                <div className="col-lg-6 mb-6"></div>
              </div>
            </div>
          </div>
        </div>
      )}
   {/* <hr className="bg-dark" /> */}
      {!isMinimized && (
        <div className="cardrow g-2 mb-2 g-xxl-2">
           <div className={"rounded " + (currentMode === 'dark' ? "bg-light" : "bg-white")}>
            <div className="card-header bg-light-primary d-flex justify-content-between align-items-left cursor-pointer">
              <div className="card-title m-0">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                  Permenent Details
                </h3>
              </div>
              <Link
                to="/crafted/pages/profile/EditPermamentDetails"
                className="btn btn-sm btn-primary me-3 align-self-center"
              >
                Edit
              </Link>
            </div>
            <div className="card-body p-9">
              <div className="row">
                <div
                  className="col-lg-6 mb-6"
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "flex-start",
                  }}
                >
                  <label className="col-lg-4 fw-bold text-muted">
                    PERMANENT ADDRESS:
                  </label>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "flex-start",
                    }}
                  >
                    <span
                      className="fw-bold fs-6 text-gray-900 text-hover-primary"
                      style={{ marginBottom: "-14px" }}
                    >
                      {/* {basicDetails.peR_ADD1} */}
                      {basicDetails?.peR_ADD1
                        ? basicDetails.peR_ADD1.toUpperCase()
                        : ""}
                    </span>
                    <br />
                    <span
                      className="fw-bold fs-6 text-gray-900 text-hover-primary"
                      style={{ marginBottom: "-14px" }}
                    >
                      {/* {basicDetails.peR_ADD2} */}
                      {basicDetails?.peR_ADD2
                        ? basicDetails.peR_ADD2.toUpperCase()
                        : ""}
                    </span>
                    <br />
                    <span
                      className="fw-bold fs-6 text-gray-900 text-hover-primary"
                      style={{ marginBottom: "-14px" }}
                    >
                      {/* {basicDetails.peR_ADD3} */}
                      {basicDetails?.peR_ADD3
                        ? basicDetails.peR_ADD3.toUpperCase()
                        : ""}
                    </span>
                    <br />
                  </div>
                </div>

                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">
                    TELEPHONE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails.peR_TEL}
                  </span>
                </div>
              </div>

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">CITY : </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails.peR_CITY}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">
                    STATE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails.peR_STATE_DESCR} */}
                    {basicDetails?.peR_STATE_DESCR
                      ? basicDetails.peR_STATE_DESCR.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">PIN : </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails.peR_PIN}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
   {/* <hr className="bg-dark" /> */}
      {!isMinimized && (
        <div className="cardrow g-2 mb-2 g-xxl-2">
           <div className={"rounded " + (currentMode === 'dark' ? "bg-light" : "bg-white")}>
            <div className="card-header bg-light-primary d-flex justify-content-between align-items-left cursor-pointer">
              <div className="card-title m-0 ">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                  Emergency Details
                </h3>
              </div>
              <Link
                to="/crafted/pages/profile/EditEmergencyDetails"
                className="btn btn-sm btn-primary me-3 align-self-center"
              >
                Edit
              </Link>
            </div>
            <div className="card-body p-9">
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">
                    CONTACT PERSON:{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails.contacT_PERS} */}
                    {basicDetails?.contacT_PERS
                      ? basicDetails.contacT_PERS.toUpperCase()
                      : ""}
                  </span>
                </div>

                <div
                  className="col-lg-6 mb-6"
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "flex-start",
                  }}
                >
                  <label className="col-lg-4 fw-bold text-muted">
                    CONTACT ADDRESS :
                  </label>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "flex-start",
                    }}
                  >
                    <span
                      className="fw-bold fs-6 text-gray-900 text-hover-primary"
                      style={{ marginBottom: "-14px" }}
                    >
                      {/* {basicDetails.contacT_ADD1} */}
                      {basicDetails?.contacT_ADD1
                        ? basicDetails.contacT_ADD1.toUpperCase()
                        : ""}
                    </span>
                    <br />
                    <span
                      className="fw-bold fs-6 text-gray-900 text-hover-primary"
                      style={{ marginBottom: "-14px" }}
                    >
                      {/* {basicDetails.contacT_ADD2} */}
                      {basicDetails?.contacT_ADD2
                        ? basicDetails.contacT_ADD2.toUpperCase()
                        : ""}
                    </span>
                    <br />
                    <span
                      className="fw-bold fs-6 text-gray-900 text-hover-primary"
                      style={{ marginBottom: "-14px" }}
                    >
                      {/* {basicDetails.contacT_ADD3} */}
                      {basicDetails?.contacT_ADD3
                        ? basicDetails.contacT_ADD3.toUpperCase()
                        : ""}
                    </span>
                    <br />
                  </div>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">
                    TELEPHONE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails.contacT_TEL}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">
                    STATE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails.contacT_STATE_DESCR} */}
                    {basicDetails?.contacT_STATE_DESCR
                      ? basicDetails.contacT_STATE_DESCR.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">CITY : </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails.contacT_CITY} */}
                    {basicDetails?.contacT_CITY
                      ? basicDetails.contacT_CITY.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label className="col-lg-4 fw-bold text-muted">PIN : </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails.contacT_PIN}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
