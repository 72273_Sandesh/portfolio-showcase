import { Content } from "../../../../_metronic/layout/components/content";
import { InterviewDetailsEdit } from "./InterviewDetailsEdit";

export function EditInterviewDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <InterviewDetailsEdit />
      </div>
    </Content>
  );
}
