import { Content } from '../../../../_metronic/layout/components/content'
import { LanguageDetailsEdit } from './LanguageDetailEdit'


export function EditLanguageDetails() {
  return (
    <Content>
        <div className='mb-5 mb-xl-8'>
        <LanguageDetailsEdit/>
        </div>
        
   </Content>
  )
}
