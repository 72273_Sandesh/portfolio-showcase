import { Content } from "../../../../_metronic/layout/components/content";
import OfferDetailsEdit from "./OfferDetailsEdit";


export function EditOfferDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <OfferDetailsEdit />
      </div>
    </Content>
  );
}
