import { Content } from "../../../../_metronic/layout/components/content";
import EditPensionDetails from "./EditPensionDetails";


export function EditPensionNominationsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <EditPensionDetails />
      </div>
    </Content>
  );
}
