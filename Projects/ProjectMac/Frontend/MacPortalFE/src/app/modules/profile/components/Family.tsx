import { Content } from '../../../../_metronic/layout/components/content'
import FamilyDetailsChart from '../../../../_metronic/partials/widgets/tables/FamilyDetailsChart'
import { FamilyDetailsTable } from '../../../../_metronic/partials/widgets/tables/FamilyDetailsTable'

export function Family() {
  return (
    <Content>
        <>
        <FamilyDetailsTable className='mb-5 mb-xl-2' />
        <FamilyDetailsChart />
        </>
   </Content>
  )
}
