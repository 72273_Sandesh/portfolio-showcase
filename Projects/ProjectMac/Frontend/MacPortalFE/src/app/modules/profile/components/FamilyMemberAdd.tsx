import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// interface Response {
//   responseStatus: string;
//   message: string;
// }

const FamilyMemberAdd: React.FC = () => {
  const [employee, setEmployee] = useState<any>({});
  const navigate = useNavigate();

  const {currentUser} = useAuth()
  // const [image, setImage] = useState<string>("");

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setEmployee(response.data[0]);
        // setImage(response.data[0].emP_IMAGE);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  // const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
  //   const file = e.target.files?.[0];
  //   if (file) {
  //     const reader = new FileReader();
  //     reader.onloadend = () => {
  //       const base64String = reader.result as string;
  //       setImage(base64String);
  //     };
  //     reader.readAsDataURL(file);
  //   }
  // };  logic for convert normal image in the base 64

  const validationSchema = Yup.object().shape({
    // Define validation schema for form fields

    membeR_NAME: Yup.string().required("Member name is required"),
    datE_BIRTH: Yup.string().required("Birth date is required"),
    sex: Yup.string().required("Gender is required"),
    relation: Yup.string().required("Realation with employee is required"),
    occupation: Yup.string().required("Occupation is required"),
    dependent: Yup.string().required("Dependent status is required"),
    pF_NOMINEE: Yup.string().required("PF nominee is required"),
    pF_PERC: Yup.string().required("PF percentage is required"),
    graT_NOMINEE: Yup.string().required("Gratuity status is required"),
    graT_PERC: Yup.string().required("Gratuity percentage is required"),
    peN_NOMINEE: Yup.string().required("Pension Code is required"),
    peN_PERC: Yup.string().required("Pension percentage is required"),
    medI_NOMINEE: Yup.string().required("Mediclaim nominee is required"),
  });

  const formik = useFormik({
    initialValues: {
      // Initialize form fields
      emP_CODE: "",
      seQ_NO: "",
      membeR_NAME: "",
      datE_BIRTH: "",
      sex: "",
      relation: "",
      occupation: "",
      dependent: "",
      pF_NOMINEE: "",
      pF_PERC: "",
      graT_NOMINEE: "",
      graT_PERC: "",
      peN_NOMINEE: "",
      peN_PERC: "",
      medI_NOMINEE: "",
      chG_USER: "",
      chG_DATE: "",
      chG_TERM: "",
    },
    validationSchema: validationSchema,
    // onSubmit: (values) => {
    //   console.log("Submitting form data:", values);

    //   axios
    //     .post("your_backend_endpoint", values)
    //     .then((response) => {
    //       console.log("Post successful!", response.data);
    //       toast.success("Employee Edited Successfully");
    //     })
    //     .catch((error) => {
    //       console.error("Error posting data:", error);
    //       toast.error("Error While Editing Employee");
    //     });

    //   formik.resetForm();
    //   navigate(-1);
    // },

    onSubmit: (values, formik) => {
      console.log("Submitting form data:", values);

      axios
        .post(`${EMPLOYEE_URL}/AddFamily`, {
          ...values, // Include other form values
          // emP_IMAGE: image, // Include the image data
          emP_CODE: employeeIdForView,
          chG_USER: currentUser?.first_name,
          chG_DATE: new Date(),
          chG_TERM: "TERM",
          seQ_NO:"7"
        })
        .then((response) => {
          console.log("Post successful!", response.data);
          const { responseStatus, message } = response.data.data;
          if (responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
          } else {
            
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
            formik.resetForm();
            navigate(-1);
            
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );
        });

      // Ensure form is reset and user is navigated away even if axios call fails
      
    },
  });

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Add New Family Member</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Image Input */}
            {/* <div className="mb-3">
              <label className="form-label">Employee Image</label>
              <input type="file" accept="image/*" onChange={handleImageChange} />
              {image && <img src={image} alt="Employee" style={{ marginTop: "10px", maxWidth: "200px", maxHeight: "200px" }} />}
            </div> */}
            {/* <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative d-flex flex-column align-items-center">
              <div className="d-flex justify-content-center align-items-center mb-3">
                {image && (
                  <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative d-flex position-relative">
                    <img
                      src={image || "/media/avatars/User.png"}
                    
                      alt="/media/avatars/User.png"
                      style={{
                        marginTop: "10px",
                        maxWidth: "200px",
                        maxHeight: "200px",
                        borderRadius: "5%", // Optional: If you want a circular image
                      }}
                    />
                    <div className="position-absolute translate-middle bottom-0 start-100 mb-6 h-20px w-20px"></div>
                  </div>
                )}
              </div>
              <label
                htmlFor="upload-button"
                className="btn btn-sm btn-primary me-3"
              >
                Choose Image
              </label>
              <input
                id="upload-button"
                type="file"
                accept="image/*"
                onChange={handleImageChange}
                style={{ display: "none" }}
              />
            </div> */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="membeR_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Member Name:
                </label>
                <input
                  type="text"
                  id="membeR_NAME"
                  className="form-control form-control-lg form-control-solid"
                  name="membeR_NAME"
                  placeholder="Enter family member name"
                  value={formik.values?.membeR_NAME}
                  onChange={formik.handleChange}
                  // onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  Realation :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="relation"
                  placeholder="Enter realation with employee"
                  value={formik.values?.relation}
                  onChange={formik.handleChange}
                  // onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.membeR_NAME && formik.errors.membeR_NAME ? (
                  <div className="text-danger">{formik.errors.membeR_NAME}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.relation && formik.errors.relation ? (
                  <div className="text-danger">{formik.errors.relation}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="datE_BIRTH"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Date of Birth:
                </label>
                <input
                  type="date"
                  id="datE_BIRTH"
                  className="form-control form-control-lg form-control-solid"
                  name="datE_BIRTH"
                  placeholder="Enter passport issue date"
                  value={
                    formik.values?.datE_BIRTH
                      ? new Date(formik.values?.datE_BIRTH)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sex"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Gender :
                </label>
                <select
                  className="form-select form-select-lg form-select-solid"
                  name="sex"
                  value={formik.values.sex}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value=""></option>
                  <option value="M">MALE</option>
                  <option value="F">FEMALE</option>
                  <option value="O">OTHER</option>
                </select>
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.datE_BIRTH && formik.errors.datE_BIRTH ? (
                  <div className="text-danger">{formik.errors.datE_BIRTH}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.sex && formik.errors.sex ? (
                  <div className="text-danger">{formik.errors.sex}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="occupation"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Occupation:
                </label>
                <input
                  type="text"
                  id="occupation"
                  className="form-control form-control-lg form-control-solid "
                  name="occupation"
                  placeholder="Enter Occupation of member"
                  value={formik.values?.occupation}
                  onChange={formik.handleChange}
                  // onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  Dependent:
                </label>
                <select
                  className="form-select form-select-lg form-select-solid"
                  name="dependent"
                  value={formik.values.dependent}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value="">{formik.values.dependent}</option>
                  <option value="Y">YES</option>
                  <option value="N">NO</option>
                </select>
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.occupation && formik.errors.occupation ? (
                  <div className="text-danger">{formik.errors.occupation}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.dependent && formik.errors.dependent ? (
                  <div className="text-danger">{formik.errors.dependent}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="pF_NOMINEE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  PF Nominee:
                </label>
                <select
                  className="form-select form-select-lg form-select-solid"
                  name="pF_NOMINEE"
                  value={formik.values.pF_NOMINEE}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value="">{formik.values.pF_NOMINEE}</option>
                  <option value="Y">YES</option>
                  <option value="N">NO</option>
                </select>
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  PF Percentage:
                </label>
                <input
                  type="number"
                  id="pF_PERC"
                  className="form-control form-control-lg form-control-solid"
                  name="pF_PERC"
                  placeholder="Enter PF Percentage"
                  value={formik.values?.pF_PERC}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  min={0}
                  max={100}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.pF_NOMINEE && formik.errors.pF_NOMINEE ? (
                  <div className="text-danger">{formik.errors.pF_NOMINEE}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.pF_PERC && formik.errors.pF_PERC ? (
                  <div className="text-danger">{formik.errors.pF_PERC}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="graT_NOMINEE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Gratuity nominee:
                </label>
                <select
                  className="form-select form-select-lg form-select-solid"
                  name="graT_NOMINEE"
                  value={formik.values.graT_NOMINEE}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value="">{formik.values.graT_NOMINEE}</option>
                  <option value="Y">YES</option>
                  <option value="N">NO</option>
                </select>
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  Gratuity Percentage:
                </label>
                <input
                  type="number"
                  id="graT_PERC"
                  className="form-control form-control-lg form-control-solid"
                  name="graT_PERC"
                  placeholder="Enter gratuity percentage"
                  value={formik.values?.graT_PERC}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.graT_NOMINEE && formik.errors.graT_NOMINEE ? (
                  <div className="text-danger">
                    {formik.errors.graT_NOMINEE}
                  </div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.graT_PERC && formik.errors.graT_PERC ? (
                  <div className="text-danger">{formik.errors.graT_PERC}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="peN_NOMINEE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Pension nominee:
                </label>
                <select
                  className="form-select form-select-lg form-select-solid"
                  name="peN_NOMINEE"
                  value={formik.values.peN_NOMINEE}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value="">{formik.values.peN_NOMINEE}</option>
                  <option value="Y">YES</option>
                  <option value="N">NO</option>
                </select>
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  Pension Percentage:
                </label>
                <input
                  type="number"
                  id="peN_PERC"
                  className="form-control form-control-lg form-control-solid"
                  name="peN_PERC"
                  placeholder="Enter pension Percentage"
                  value={formik.values?.peN_PERC}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.peN_NOMINEE && formik.errors.peN_NOMINEE ? (
                  <div className="text-danger">{formik.errors.peN_NOMINEE}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.peN_PERC && formik.errors.peN_PERC ? (
                  <div className="text-danger">{formik.errors.peN_PERC}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="medI_NOMINEE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Mediclaim nominee:
                </label>
                <select
                  className="form-select form-select-lg form-select-solid"
                  name="medI_NOMINEE"
                  value={formik.values.medI_NOMINEE}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value="">{formik.values.medI_NOMINEE}</option>
                  <option value="Y">YES</option>
                  <option value="N">NO</option>
                </select>
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.medI_NOMINEE && formik.errors.medI_NOMINEE ? (
                  <div className="text-danger">
                    {formik.errors.medI_NOMINEE}
                  </div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={//formik.isSubmitting || 
                !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default FamilyMemberAdd;
