// import React, { useState, useEffect } from "react";
// import axios from "axios";
// import { useNavigate } from "react-router-dom";
// import { useFormik } from "formik";
// import * as Yup from "yup";
// import { employeeIdForView } from "../../GlobalQuery";
// import { toast } from "react-toastify";
// import { KTIcon } from "../../../../_metronic/helpers";

// const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// const InterviewDetailsAdd: React.FC = () => {
//     const [employee, setEmployee] = useState<any>({});
//     const navigate = useNavigate();
//     const [image, setImage] = useState<string>("");

//     useEffect(() => {
//         axios
//             .get(`${EMPLOYEE_URL}/${employeeIdForView}/INTERVIEW_DET`)
//             .then((response) => {
//                 setEmployee(response.data[0]);

//             })
//             .catch((error) => {
//                 console.error("Error fetching basic details:", error);
//             });
//     }, []);

//     const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
//         const file = e.target.files?.[0];
//         if (file) {
//             const reader = new FileReader();
//             reader.onloadend = () => {
//                 const base64String = reader.result as string;
//                 setImage(base64String);
//             };
//             reader.readAsDataURL(file);
//         }
//     };

//     const validationSchema = Yup.object().shape({

//         reF_NO: Yup.string(),
//         starT_DATE: Yup.string().required("Start Date is required"),
//         status: Yup.string().required("Status is required"),
//         panel: Yup.string().required("Panel Name is required"),
//         commentS_PANEL: Yup.string().required("Panel Remarks is required"),
//         supP_CODE__CON: Yup.string(),
//         source: Yup.string().required("Source is required"),
//         remarks: Yup.string().required("Remarks is required"),
//     });

//     const formik = useFormik({
//         initialValues: {

//             reF_NO: "",
//             starT_DATE: "",
//             status: "",
//             panel: "",
//             commentS_PANEL: "",
//             supP_CODE__CON: "",
//             source: "",
//             remarks: "",
//         },
//         validationSchema: validationSchema,
//         // onSubmit: (values) => {
//         //   console.log("Submitting form data:", values);

//         //   axios
//         //     .post("your_backend_endpoint", values)
//         //     .then((response) => {
//         //       console.log("Post successful!", response.data);
//         //       toast.success("Employee Edited Successfully");
//         //     })
//         //     .catch((error) => {
//         //       console.error("Error posting data:", error);
//         //       toast.error("Error While Editing Employee");
//         //     });

//         //   formik.resetForm();
//         //   navigate(-1);
//         // },
//         onSubmit: (values) => {
//             console.log("Submitting form data:", values);

//             axios
//                 .post("your_backend_endpoint", {
//                     ...values, // Include other form values
//                     emP_IMAGE: image, // Include the image data
//                 })
//                 .then((response) => {
//                     console.log("Post successful!", response.data);
//                     // toast.success("Interview Details Added Successfully");
//                     if (response.responseStatus === "E") {
//                         toast.error(
//                           <div>
//                             <h4 className="alert-heading">Failed to submit Form...</h4>
//                             <p className="alert alert-danger" role="alert">
//                               <h6>{response.message.join("\n")}</h6>
//                             </p>
//                           </div>
//                         );
//                       } 
//                       else 
//                       {
//                         toast.success(
//                           <div>
//                             <h4 className="alert-heading">Successfully Submitted Form...</h4>
//                             <p className="alert alert-success" role="alert">
//                               <h6>{response.message.join("\n")}</h6>
//                             </p>
//                           </div>
//                         );
//                       }
//                     formik.resetForm();
//                     navigate(-1);
//                 })
//                 // .catch((error) => {
//                 //     console.error("Error posting data:", error);
//                 //     // toast.error("Error While adding Interview Details");
//                 //     toast.error(<div >
//                 //         <h6 className="alert-heading">{error}</h6>
//                 //         <p className="alert alert-danger" role="alert"><h6>Error Message</h6></p>
//                 //       </div>);
//                 // });
//             formik.resetForm();
//             navigate(-1);
//         },
//     });

//     const handleClose = () => {
//         navigate(-1);
//         // navigate("/crafted/pages/profile/overview");
//     };

//     return (
//         <div className="card mb-1 mb-xl-2">
//             <div
//                 className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
//                 role="button"
//                 data-bs-target="#kt_account_profile_details"
//                 aria-expanded="true"
//                 aria-controls="kt_account_profile_details"
//             >
//                 <div className="card-title m-0">
//                     <h3 className="fw-bolder m-0">Add Interview Details</h3>
//                 </div>
//                 <button
//                     type="button"
//                     className="btn-close"
//                     aria-label="Close"
//                     onClick={handleClose}
//                     style={{ marginRight: "10px", cursor: "pointer" }}
//                 ></button>
//             </div>

//             <div id="kt_account_profile_details" className="collapse show">
//                 <form onSubmit={formik.handleSubmit} className="form">
//                     <div className="card-body border-top p-9">




//                         {/* Begin: Edit Row */}
//                         <div className="row mb-1">
//                             <div className="col-lg-6 d-flex align-items-center mb-1">
//                                 <label
//                                     htmlFor="panel"
//                                     className="col-form-label required fw-bold fs-6 me-2"
//                                     style={{ minWidth: "150px" }}
//                                 >
//                                     Panel Name :
//                                 </label>
//                                 <input
//                                     type="text"
//                                     id="panel"
//                                     className="form-control form-control-lg form-control-solid form-control-solid "
//                                     name="panel"
//                                     placeholder="Enter Panel Name"
//                                     value={formik.values?.panel}
//                                     onChange={formik.handleChange}
//                                     onBlur={formik.handleBlur}
//                                 />
//                             </div>
//                             <div className="col-lg-6 d-flex align-items-center mb-1">
//                                 <label
//                                     htmlFor="starT_DATE"
//                                     className="col-form-label required fw-bold fs-6 me-2"
//                                     style={{ minWidth: "150px" }}
//                                 >
//                                     Date of Interview:
//                                 </label>
//                                 <input
//                                     type="date"
//                                     id="starT_DATE"
//                                     className="form-control form-control-lg form-control-solid"
//                                     name="starT_DATE"
//                                     placeholder="Select Date of Interview"
//                                     value={formik.values.starT_DATE?.toUpperCase()}
//                                     onChange={formik.handleChange}
//                                     onBlur={formik.handleBlur}
//                                 />
//                             </div>
//                         </div>
//                         <div className="row mb-1">
//                             <div className="col-lg-6">
//                                 {formik.touched.panel && formik.errors.panel ? (
//                                     <div className="text-danger">{formik.errors.panel}</div>
//                                 ) : null}
//                             </div>
//                             <div className="col-lg-6">
//                                 {formik.touched.starT_DATE && formik.errors.starT_DATE ? (
//                                     <div className="text-danger">{formik.errors.starT_DATE}</div>
//                                 ) : null}
//                             </div>
//                         </div>

//                         {/* End: Edit Row */}



//                         {/* Begin: Edit Row */}
//                         <div className="row mb-1">
//                             <div className="col-lg-6 d-flex align-items-center mb-1">
//                                 <label
//                                     htmlFor="source"
//                                     className="col-form-label required fw-bold fs-6 me-2"
//                                     style={{ minWidth: "150px" }}
//                                 >
//                                     Source :
//                                 </label>
//                                 <input
//                                     type="text"
//                                     id="source"
//                                     className="form-control form-control-lg form-control-solid"
//                                     name="source"
//                                     placeholder="Enter Source"
//                                     value={formik.values?.source}
//                                     onChange={formik.handleChange}
//                                     onBlur={formik.handleBlur}
//                                 />
//                             </div>
//                             <div className="col-lg-6 d-flex align-items-center mb-1">
//                                 <label
//                                     htmlFor="status"
//                                     className="col-form-label required fw-bold fs-6 me-2"
//                                     style={{ minWidth: "150px" }}
//                                 >
//                                     Status :
//                                 </label>




//                                 <select
//                                     id="status"
//                                     className="form-select form-select-lg form-select-solid"
//                                     name="status"
//                                     value={formik.values.status}
//                                     onChange={formik.handleChange}
//                                     onBlur={formik.handleBlur}
//                                 >
//                                     <option value="">{formik.values.status}</option>
//                                     <option value="Offered">Offered</option>
//                                     <option value="Rejected">Rejected</option>


//                                 </select>
//                             </div>
//                         </div>
//                         <div className="row mb-1">
//                             <div className="col-lg-6">
//                                 {formik.touched.source && formik.errors.source ? (
//                                     <div className="text-danger">{formik.errors.source}</div>
//                                 ) : null}
//                             </div>
//                             <div className="col-lg-6">
//                                 {formik.touched.status && formik.errors.status ? (
//                                     <div className="text-danger">{formik.errors.status}</div>
//                                 ) : null}
//                             </div>
//                         </div>

//                         {/* End: Edit Row */}

//                         {/* Begin: Edit Row */}
//                         <div className="row mb-1">
//                             <div className="col-lg-6 d-flex align-items-center mb-1">
//                                 <label
//                                     htmlFor="commentS_PANEL"
//                                     className="col-form-label required fw-bold fs-6 me-2"
//                                     style={{ minWidth: "150px" }}
//                                 >
//                                     Panel Remark :
//                                 </label>
//                                 <input
//                                     type="text"
//                                     id="commentS_PANEL"
//                                     className="form-control form-control-lg form-control-solid"
//                                     name="commentS_PANEL"
//                                     placeholder="Enter middle name"
//                                     value={formik.values?.commentS_PANEL}
//                                     onChange={formik.handleChange}
//                                     onBlur={formik.handleBlur}
//                                 />
//                             </div>
//                             <div className="col-lg-6 d-flex align-items-center mb-1">
//                                 <label
//                                     htmlFor="remarks"
//                                     className="col-form-label required fw-bold fs-6 me-2"
//                                     style={{ minWidth: "150px" }}
//                                 >
//                                     Remark :
//                                 </label>


//                                 <select
//                                     id="remarks"
//                                     className="form-select form-select-lg form-select-solid"
//                                     name="remarks"
//                                     value={formik.values.remarks}
//                                     onChange={formik.handleChange}
//                                     onBlur={formik.handleBlur}
//                                 >
//                                     <option value="">{formik.values.remarks}</option>
//                                     <option value="Selected">Selected</option>
//                                     <option value="Rejected">Rejected</option>
//                                     <option value="Reserved">Reserved</option>

//                                 </select>
//                             </div>
//                         </div>
//                         <div className="row mb-1">
//                             <div className="col-lg-6">
//                                 {formik.touched.commentS_PANEL && formik.errors.commentS_PANEL ? (
//                                     <div className="text-danger">{formik.errors.commentS_PANEL}</div>
//                                 ) : null}
//                             </div>
//                             <div className="col-lg-6">
//                                 {formik.touched.remarks && formik.errors.remarks ? (
//                                     <div className="text-danger">
//                                         {formik.errors.remarks}
//                                     </div>
//                                 ) : null}
//                             </div>
//                         </div>

//                         {/* End: Edit Row */}

//                     </div>

//                     {/* Logic for Submit */}
//                     <div className="card-footer d-flex justify-content-center py-6 px-9">
//                         <button
//                             type="button"
//                             className="btn btn-light me-2"
//                             onClick={handleClose}
//                         >
//                             <span className="indicator-label">Discard</span>
//                         </button>

//                         <button
//                             type="submit"
//                             className="btn btn-primary"
//                             disabled={formik.isSubmitting || !formik.isValid}
//                         >
//                             <span className="indicator-label">Submit</span>
//                         </button>
//                     </div>
//                 </form>
//             </div>
//         </div>
//     );
// };

// export default InterviewDetailsAdd;
import {FC} from 'react'

const InterviewDetailsAdd: FC = () => {
  return (<div >
          <h1>This Page is under Development...</h1>
          <h3>please contact to admin</h3>
          </div>)
}

export {InterviewDetailsAdd}