// import React, { useEffect } from "react";
// import axios from "axios";
// import { useNavigate } from "react-router-dom";
// import { useFormik } from "formik";
// import * as Yup from "yup";
// import { employeeIdForView } from "../../GlobalQuery";
// import { toast } from "react-toastify";

// const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// const InterviewDetailsEdit: React.FC = () => {
//   const navigate = useNavigate();

//   const validationSchema = Yup.object().shape({
//     reF_NO: Yup.string(),
//     starT_DATE: Yup.string().required("Start date is required"),
//     status: Yup.string().required("Status is required"),
//     panel: Yup.string().required("Panel is required"),
//     commentS_PANEL: Yup.string().required("Panels remark is reuired"),
//     supP_CODE__CON: Yup.string(),
//     source: Yup.string().required("Source is required"),
//     remarks: Yup.string().required("Remarks is required"),
//   });

//   const formik = useFormik({
//     initialValues: {
//       InterviewDetails: [
//         {
//           reF_NO: "",
//           starT_DATE: "",
//           status: "",
//           panel: "",
//           commentS_PANEL: "",
//           supP_CODE__CON: "",
//           source: "",
//           remarks: "",
//         },
//       ],
//     },
//     validationSchema: Yup.object().shape({
//       InterviewDetails: Yup.array().of(validationSchema),
//     }),
//     onSubmit: (values) => {
//       console.log("Submitting form data:", values);

//       axios
//         .post("your_backend_endpoint", {
//           ...values,
//         })
//         .then((response) => {
//           console.log("Post successful!", response.data);
//           // toast.success("Interview Details Edited Successfully");
//           if (response.responseStatus === "E") {
//             toast.error(
//               <div>
//                 <h4 className="alert-heading">Failed to submit Form...</h4>
//                 <p className="alert alert-danger" role="alert">
//                   <h6>{response.message.join("\n")}</h6>
//                 </p>
//               </div>
//             );
//           } 
//           else 
//           {
//             toast.success(
//               <div>
//                 <h4 className="alert-heading">Successfully Submitted Form...</h4>
//                 <p className="alert alert-success" role="alert">
//                   <h6>{response.message.join("\n")}</h6>
//                 </p>
//               </div>
//             );
//           }
//           formik.resetForm();
//           navigate(-1);
//         })
//         // .catch((error) => {
//         //   console.error("Error posting data:", error);
//         //   // toast.error("Error While Editing Interview Details");
//         //   toast.error(<div >
//         //     <h6 className="alert-heading">{error}</h6>
//         //     <p className="alert alert-danger" role="alert"><h6>Error Message</h6></p>
//         //   </div>);
//         //   formik.resetForm();
//         //   navigate(-1);
//         // });
//     },
//   });

//   useEffect(() => {
//     axios
//       .get(`${EMPLOYEE_URL}/${employeeIdForView}/INTERVIEW_DET`)
//       .then((response) => {
//         formik.setValues({
//           InterviewDetails: response.data,
//         });
//       })
//       .catch((error) => {
//         console.error("Error fetching familyMember details:", error);
//       });
//   }, []);

//   const handleClose = () => {
//     navigate(-1);
//   };

//   return (
//     <div className="card mb-1 mb-xl-2">
//       <div
//         className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
//         role="button"
//         data-bs-target="#kt_account_profile_details"
//         aria-expanded="true"
//         aria-controls="kt_account_profile_details"
//       >
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Edit InterView Details</h3>
//         </div>
//         <button
//           type="button"
//           className="btn-close"
//           aria-label="Close"
//           onClick={handleClose}
//           style={{ marginRight: "10px", cursor: "pointer" }}
//         ></button>
//       </div>

//       <div id="kt_account_profile_details" className="collapse show">
//         <form onSubmit={formik.handleSubmit} className="form">
//           <div className="card-body border-top p-9">
//             {formik.values.InterviewDetails.map((familyMember, index) => (
//               <div key={index}>
//                 <div className="row mb-1">
//                   <div className="col-lg-6 d-flex align-items-center mb-1">
//                     <label
//                       htmlFor={`InterviewDetails[${index}].panel`}
//                       className="col-form-label required fw-bold fs-6 me-2"
//                       style={{ minWidth: "150px" }}
//                     >
//                       Panel Name :
//                     </label>
//                     <input
//                       type="text"
//                       id={`InterviewDetails[${index}].panel`}
//                       className="form-control form-control-lg form-control-solid"
//                       name={`InterviewDetails[${index}].panel`}
//                       placeholder="Enter Full Name"
//                       value={familyMember.panel?.toUpperCase()}
//                       onChange={formik.handleChange}
//                       onBlur={formik.handleBlur}
//                     />
//                   </div>
//                   <div className="col-lg-6 d-flex align-items-center mb-1">
//                     <label
//                       htmlFor={`InterviewDetails[${index}].starT_DATE`}
//                       className="col-form-label required fw-bold fs-6 me-2"
//                       style={{ minWidth: "150px" }}
//                     >
//                       Date of Interview :
//                     </label>
//                     <input
//                       type="date"
//                       id={`InterviewDetails[${index}].starT_DATE`}
//                       className="form-control form-control-lg form-control-solid"
//                       name={`InterviewDetails[${index}].starT_DATE`}
//                       placeholder="Enter Start Date"
//                       value={
//                         familyMember.starT_DATE
//                           ? familyMember.starT_DATE.split("T")[0]
//                           : ""
//                       }
//                       onChange={formik.handleChange}
//                       onBlur={formik.handleBlur}
//                     />
//                   </div>
//                 </div>
//                 <div className="row mb-1">
//                   <div className="col-lg-6">
//                     {formik.touched.InterviewDetails?.[index]?.panel &&
//                     formik.errors.InterviewDetails?.[index]?.panel ? (
//                       <div className="text-danger">
//                         {formik.errors.InterviewDetails[index].panel}
//                       </div>
//                     ) : null}
//                   </div>
//                   <div className="col-lg-6">
//                     {formik.touched.InterviewDetails?.[index]?.starT_DATE &&
//                     formik.errors.InterviewDetails?.[index]?.starT_DATE ? (
//                       <div className="text-danger">
//                         {formik.errors.InterviewDetails[index].starT_DATE}
//                       </div>
//                     ) : null}
//                   </div>
//                 </div>

//                 <div className="row mb-1">
//                   <div className="col-lg-6 d-flex align-items-center mb-1">
//                     <label
//                       htmlFor={`InterviewDetails[${index}].source`}
//                       className="col-form-label required fw-bold fs-6 me-2"
//                       style={{ minWidth: "150px" }}
//                     >
//                       Source :
//                     </label>
//                     <input
//                       type="text"
//                       id={`InterviewDetails[${index}].source`}
//                       className="form-control form-control-lg form-control-solid"
//                       name={`InterviewDetails[${index}].source`}
//                       placeholder="Enter Full Name"
//                       value={familyMember.source?.toUpperCase()}
//                       onChange={formik.handleChange}
//                       onBlur={formik.handleBlur}
//                     />
//                   </div>
//                   <div className="col-lg-6 d-flex align-items-center mb-1">
//                     <label
//                       htmlFor={`InterviewDetails[${index}].status`}
//                       className="col-form-label required fw-bold fs-6 me-2"
//                       style={{ minWidth: "150px" }}
//                     >
//                       Status :
//                     </label>
//                     <select
//                       className="form-select form-select-lg form-select-solid"
//                       name={`InterviewDetails[${index}].status`}
//                       value={familyMember.status}
//                       onChange={formik.handleChange}
//                       onBlur={formik.handleBlur}
//                     >
//                       <option value={familyMember.status}>
//                         {familyMember.status}
//                       </option>
//                       <option value="Offered">Offered</option>
//                       <option value="Rejected">Rejected</option>
//                     </select>
//                   </div>
//                 </div>
//                 <div className="row mb-1">
//                   <div className="col-lg-6">
//                     {formik.touched.InterviewDetails?.[index]?.source &&
//                     formik.errors.InterviewDetails?.[index]?.source ? (
//                       <div className="text-danger">
//                         {formik.errors.InterviewDetails[index].source}
//                       </div>
//                     ) : null}
//                   </div>
//                   <div className="col-lg-6">
//                     {formik.touched.InterviewDetails?.[index]?.status &&
//                     formik.errors.InterviewDetails?.[index]?.status ? (
//                       <div className="text-danger">
//                         {formik.errors.InterviewDetails[index].status}
//                       </div>
//                     ) : null}
//                   </div>
//                 </div>

//                 <div className="row mb-1">
//                   <div className="col-lg-6 d-flex align-items-center mb-1">
//                     <label
//                       htmlFor={`InterviewDetails[${index}].commentS_PANEL`}
//                       className="col-form-label required fw-bold fs-6 me-2"
//                       style={{ minWidth: "150px" }}
//                     >
//                       Panel Remark :
//                     </label>
//                     <input
//                       type="text"
//                       id={`InterviewDetails[${index}].commentS_PANEL`}
//                       className="form-control form-control-lg form-control-solid"
//                       name={`InterviewDetails[${index}].commentS_PANEL`}
//                       placeholder="Enter commentS_PANEL"
//                       value={familyMember.commentS_PANEL?.toUpperCase()}
//                       onChange={formik.handleChange}
//                       onBlur={formik.handleBlur}
//                     />
//                   </div>
//                   <div className="col-lg-6 d-flex align-items-center mb-1">
//                     <label
//                       htmlFor={`InterviewDetails[${index}].remarks`}
//                       className="col-form-label required fw-bold fs-6 me-2"
//                       style={{ minWidth: "150px" }}
//                     >
//                       Remark :
//                     </label>
//                     <select
//                       className="form-select form-select-lg form-select-solid"
//                       name={`InterviewDetails[${index}].remarks`}
//                       value={familyMember.remarks}
//                       onChange={formik.handleChange}
//                       onBlur={formik.handleBlur}
//                     >
//                       <option value={familyMember.remarks}>
//                         {familyMember.remarks}
//                       </option>
//                       <option value="Selected">Selected</option>
//                       <option value="Rejected">Rejected</option>
//                       <option value="Reserved">Reserved</option>
//                     </select>
//                   </div>
//                 </div>
//                 <div className="row mb-1">
//                   <div className="col-lg-6">
//                     {formik.touched.InterviewDetails?.[index]?.commentS_PANEL &&
//                     formik.errors.InterviewDetails?.[index]?.commentS_PANEL ? (
//                       <div className="text-danger">
//                         {formik.errors.InterviewDetails[index].commentS_PANEL}
//                       </div>
//                     ) : null}
//                   </div>
//                   <div className="col-lg-6">
//                     {formik.touched.InterviewDetails?.[index]?.remarks &&
//                     formik.errors.InterviewDetails?.[index]?.remarks ? (
//                       <div className="text-danger">
//                         {formik.errors.InterviewDetails[index].remarks}
//                       </div>
//                     ) : null}
//                   </div>
//                 </div>

              
//                 <div className="row mb-1">
//                   <div className="col-lg-6">
//                     {formik.touched.InterviewDetails?.[index]?.pF_NOMINEE &&
//                     formik.errors.InterviewDetails?.[index]?.pF_NOMINEE ? (
//                       <div className="text-danger">
//                         {formik.errors.InterviewDetails[index].pF_NOMINEE}
//                       </div>
//                     ) : null}
//                   </div>
//                   <div className="col-lg-6">
//                     {formik.touched.InterviewDetails?.[index]?.pF_PERC &&
//                     formik.errors.InterviewDetails?.[index]?.pF_PERC ? (
//                       <div className="text-danger">
//                         {formik.errors.InterviewDetails[index].pF_PERC}
//                       </div>
//                     ) : null}
//                   </div>
//                 </div>

//                 {index < formik.values.InterviewDetails.length - 1 && <hr />}
//               </div>
//             ))}
//           </div>

//           <div className="card-footer d-flex justify-content-center py-6 px-9">
//           <button
//               type="button"
//               className="btn btn-light me-2"
//               onClick={handleClose}
//             >
//               <span className="indicator-label">Discard</span>
//             </button>

//             <button
//               type="submit"
//               className="btn btn-primary"
//               disabled={formik.isSubmitting}
//             >
//               {formik.isSubmitting ? "Please wait..." : "Save Changes"}
//             </button>
//           </div>
//         </form>
//       </div>
//     </div>
//   );
// };

// export default InterviewDetailsEdit;
import {FC} from 'react'

const InterviewDetailsEdit: FC = () => {
  return (<div >
          <h1>This Page is under Development...</h1>
          <h3>please contact to admin</h3>
          </div>)
}

export {InterviewDetailsEdit}
