import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

interface Response {
  responseStatus: string;
  message: string;
}

const JoiningDetailsEdit: React.FC = () => {
  const [employee, setEmployee] = useState<any>({});
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setEmployee(response.data[0]);
        // Update formik's initial values after fetching employee data
        formik.setValues({
          datE_JOIN: response.data[0].datE_JOIN || "",
          joineD_AS: response.data[0].joineD_AS.toUpperCase() || "",
          joininG_CTC: response.data[0].joininG_CTC || "",
          reporT_TO: response.data[0].reporT_TO.toUpperCase() || "",
          employmenT_TYPE: response.data[0].employmenT_TYPE || "",
          datE_CONF: response.data[0].datE_CONF || "",
          probatioN_PRD: response.data[0].probatioN_PRD || "",
          conF_EXT: response.data[0].conF_EXT || "",
          traininG_PRD: response.data[0].traininG_PRD || "",
          probatioN_DATE: response.data[0].probatioN_DATE || "",
          proB_EXT: response.data[0].proB_EXT || "",
          noticE_PRD: response.data[0].noticE_PRD || "",
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    datE_JOIN: Yup.date().required("Date of join is required"),
    // joineD_AS: Yup.string().required("Joined as is required"),
    // joininG_CTC: Yup.number().required("Joining CTC is required"),
    // reporT_TO: Yup.string().required("Report To is required"),
    // employmenT_TYPE: Yup.string().required("Employment Type is required"),
    // datE_CONF: Yup.date().required("Date of Confirmation is required"),
    // probatioN_PRD: Yup.number()
    //   // .required("Joining CTC is required")
    //   .positive("Joining CTC must be positive"), // Optional: Ensure positive value
    //  // Optional: Ensure non-negative value
    //   conF_EXT: Yup.number()
    //   // .required("Current CTC is required")
    //   .positive("Current CTC must be positive") ,// Optional: Ensure positive value
    //   // Optional: Ensure non-negative value
    //   proB_EXT: Yup.number()
    // .required("Current CTC is required")
    // .positive("Current CTC must be positive") ,
    // traininG_PRD: Yup.number()
    // .required("Macleods Experience is required")
    // .positive("Macleods Experience must be positive"), // Optional: Ensure positive value
    // probatioN_DATE: Yup.date().required("Probation Date is required"),
    // Optional: Ensure positive value
    // emaiL_ID_OFF: Yup.string().email("Invalid email format").required("Official Email is required"),

    probatioN_PRD: Yup.date().required("Confirmation months is required"),
    conF_EXT: Yup.date().required("Confirmation extension is required"),
    traininG_PRD: Yup.date().required("Probation months is required"),
    proB_EXT: Yup.date().required("Probation extension is required"),
  });

  const formik = useFormik({
    initialValues: {
      datE_JOIN: "",
      joineD_AS: "",
      joininG_CTC: "",
      reporT_TO: "",
      employmenT_TYPE: "",
      datE_CONF: "",
      probatioN_PRD: "",
      conF_EXT: "",
      traininG_PRD: "",
      probatioN_DATE: "",
      proB_EXT: "",
      noticE_PRD: "",
    },
    validationSchema: validationSchema,
    // onSubmit: (values) => {
    //   console.log('Submitting form data:', values);
    //   // Implement your submit logic here, e.g., send updated data to the server

    //   setEmployee({ sitE_NAME: '', depT_NAME: '' ,designation:'',section:""});
    //   formik.resetForm();// Upon successful submission, clear the form
    // },

    onSubmit: (values, formik) => {
      console.log("Submitting form data:", values);

      axios
        .post<Response>("your_backend_endpoint", values)
        .then((response) => {
          console.log("Joining Details Posted successfully!", response.data);
          if (response.data.responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(response.data.message)
                      ? response.data.message.join("\n")
                      : response.data.message}
                  </h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(response.data.message)
                      ? response.data.message.join("\n")
                      : response.data.message}
                  </h6>
                </p>
              </div>
            );

            // Clear employee data and reset form
            setEmployee({
              datE_JOIN: "",
              joineD_AS: "",
              joininG_CTC: "",
              reporT_TO: "",
              employmenT_TYPE: "",
              datE_CONF: "",
              probatioN_PRD: "",
              conF_EXT: "",
              traininG_PRD: "",
              probatioN_DATE: "",
              proB_EXT: "",
              noticE_PRD: "",
            });
            formik.resetForm();
            navigate(-1); // Redirect to previous page or desired location
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );
        });
    },
  });

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Joining Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="datE_JOIN"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Joining Date :
                </label>
                <input
                  type="date"
                  id="datE_JOIN"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="datE_JOIN"
                  placeholder="Enter date of joining"
                  value={
                    formik.values?.datE_JOIN
                      ? new Date(formik.values?.datE_JOIN)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  Joined as :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="joineD_AS"
                  placeholder="Enter joinned as"
                  value={formik.values.joineD_AS?.toUpperCase()}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.datE_JOIN && formik.errors.datE_JOIN ? (
                  <div className="text-danger">{formik.errors.datE_JOIN}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.joineD_AS && formik.errors.joineD_AS ? (
                  <div className="text-danger">{formik.errors.joineD_AS}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="joininG_CTC"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Joining CTC :
                </label>
                <input
                  type="text"
                  id="joininG_CTC"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="joininG_CTC"
                  placeholder="Enter date of joining"
                  value={formik.values?.joininG_CTC}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  Report To :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="reporT_TO"
                  placeholder="Enter joinned as"
                  value={formik.values.reporT_TO?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.joininG_CTC && formik.errors.joininG_CTC ? (
                  <div className="text-danger">{formik.errors.joininG_CTC}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.reporT_TO && formik.errors.reporT_TO ? (
                  <div className="text-danger">{formik.errors.reporT_TO}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="employmenT_TYPE"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Employment Type :
                </label>
                <input
                  type="text"
                  id="employmenT_TYPE"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="employmenT_TYPE"
                  placeholder="Enter employment type"
                  value={formik.values?.employmenT_TYPE}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="noticE_PRD"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Notice Period(Days):
                </label>

                <input
                  type="number"
                  id="noticE_PRD"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="noticE_PRD"
                  placeholder="Enter employment type"
                  value={formik.values?.noticE_PRD}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.employmenT_TYPE &&
                formik.errors.employmenT_TYPE ? (
                  <div className="text-danger">
                    {formik.errors.employmenT_TYPE}
                  </div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.noticE_PRD && formik.errors.noticE_PRD ? (
                  <div className="text-danger">{formik.errors.noticE_PRD}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-4 d-flex align-items-center mb-1">
                <label
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Probation Date:
                </label>
                <input
                  type="date"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="probatioN_DATE"
                  placeholder="Enter probation date"
                  value={
                    formik.values?.probatioN_DATE
                      ? new Date(formik.values?.probatioN_DATE)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>

              <div className="col-lg-4 d-flex align-items-center mb-1">
                <label
                  htmlFor="traininG_PRD"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Probation months:
                </label>
                <input
                  type="number"
                  id="traininG_PRD"
                  className="form-control form-control-lg form-control-solid"
                  name="traininG_PRD"
                  placeholder="Enter probation months"
                  value={formik.values?.traininG_PRD}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  min={1}
                  max={12}
                />
              </div>

              <div className="col-lg-4 d-flex align-items-center mb-1">
                <label
                  htmlFor="proB_EXT"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Probation Extension:
                </label>
                <input
                  type="number"
                  id="proB_EXT"
                  className="form-control form-control-lg form-control-solid"
                  name="proB_EXT"
                  placeholder="Enter probation extension"
                  value={formik.values?.proB_EXT}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-4">
                {formik.touched.probatioN_DATE &&
                formik.errors.probatioN_DATE ? (
                  <div className="text-danger">
                    {formik.errors.probatioN_DATE}
                  </div>
                ) : null}
              </div>
              <div className="col-lg-4">
                {formik.touched.traininG_PRD && formik.errors.traininG_PRD ? (
                  <div className="text-danger">
                    {formik.errors.traininG_PRD}
                  </div>
                ) : null}
              </div>
              <div className="col-lg-4">
                {formik.touched.proB_EXT && formik.errors.proB_EXT ? (
                  <div className="text-danger">{formik.errors.proB_EXT}</div>
                ) : null}
              </div>
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-4 d-flex align-items-center mb-1">
                <label
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Confirmation Date:
                </label>
                <input
                  type="date"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="datE_CONF"
                  placeholder="Select confirmation date"
                  value={
                    formik.values?.datE_CONF
                      ? new Date(formik.values?.datE_CONF)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-4 d-flex align-items-center mb-1">
                <label
                  htmlFor="probatioN_PRD"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Confirmation Months:
                </label>
                <input
                  type="number"
                  id="probatioN_PRD"
                  className="form-control form-control-lg form-control-solid"
                  name="probatioN_PRD"
                  placeholder="Enter confirmation months"
                  value={formik.values?.probatioN_PRD}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  min={1}
                  max={12}
                />
              </div>
              <div className="col-lg-4 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Confirmation Extension:
                </label>
                <input
                  type="number"
                  className="form-control form-control-lg form-control-solid"
                  name="conF_EXT"
                  placeholder="Enter confirmation extension"
                  value={formik.values?.conF_EXT}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-4">
                {formik.touched.datE_CONF && formik.errors.datE_CONF ? (
                  <div className="text-danger">{formik.errors.datE_CONF}</div>
                ) : null}
              </div>
              <div className="col-lg-4">
                {formik.touched.probatioN_PRD && formik.errors.probatioN_PRD ? (
                  <div className="text-danger">
                    {formik.errors.probatioN_PRD}
                  </div>
                ) : null}
              </div>
              <div className="col-lg-4">
                {formik.touched.conF_EXT && formik.errors.conF_EXT ? (
                  <div className="text-danger">{formik.errors.conF_EXT}</div>
                ) : null}
              </div>
            </div>
            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit is HERE */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default JoiningDetailsEdit;
