// import { useState, FC } from "react";
// import { toAbsoluteUrl } from "../../../../_metronic/helpers";
// import {
//   IProfileDetails,
//   profileDetailsInitValues as initialValues,
// } from "../../accounts/components/settings/SettingsModel";
// import * as Yup from "yup";
// import { useFormik } from "formik";
// import { Navigate, useNavigate } from "react-router-dom";

// const profileDetailsSchema = Yup.object().shape({
//   fName: Yup.string().required("First name is required"),
//   lName: Yup.string().required("Last name is required"),
//   company: Yup.string().required("Company name is required"),
//   contactPhone: Yup.string().required("Contact phone is required"),
//   companySite: Yup.string().required("Company site is required"),
//   country: Yup.string().required("Country is required"),
//   language: Yup.string().required("Language is required"),
//   timeZone: Yup.string().required("Time zone is required"),
//   currency: Yup.string().required("Currency is required"),
//   bloodGroup: Yup.string()
//     .required("Blood group is required")
//     .oneOf(
//       ["", "A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-"],
//       "Please select a valid blood group"
//     ),
// });

// const LanguageDetailsEdit: FC = () => {
//   const [data, setData] = useState<IProfileDetails>(initialValues);
//   const updateData = (fieldsToUpdate: Partial<IProfileDetails>): void => {
//     const updatedData = Object.assign(data, fieldsToUpdate);
//     setData(updatedData);
//   };

//   const [loading, setLoading] = useState(false);
//   const formik = useFormik<IProfileDetails>({
//     initialValues,
//     validationSchema: profileDetailsSchema,
//     onSubmit: (values) => {
//       setLoading(true);
//       setTimeout(() => {
//         values.communications.email = data.communications.email;
//         values.communications.phone = data.communications.phone;
//         values.allowMarketing = data.allowMarketing;
//         const updatedData = Object.assign(data, values);
//         setData(updatedData);
//         setLoading(false);
//       }, 1000);
//     },
//   });

//   const navigate = useNavigate();

//   const handleClose = () => {
//     navigate(-1);
//   };

//   return (
//     <div className="card mb-1 mb-xl-2">
//       <div
//         className="card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
//         role="button"
//         data-bs-toggle="collapse"
//         data-bs-target="#kt_account_profile_details"
//         aria-expanded="true"
//         aria-controls="kt_account_profile_details"
//       >
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Edit Language Details</h3>
//         </div>
//         <button
//           type="button"
//           className="btn-close"
//           aria-label="Close"
//           onClick={handleClose}
//           style={{ marginRight: "10px", cursor: "pointer" }} // Adjust the margin-right as needed
//         ></button>
//       </div>

//       <div id="kt_account_profile_details" className="collapse show">
//         <form onSubmit={formik.handleSubmit} noValidate className="form">
//           <div className="card-body border-top p-9">
//             <div className="row mb-1">
//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Father Name :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   placeholder="Company name"
//                   {...formik.getFieldProps("company")}
//                 />
//                 {formik.touched.company && formik.errors.company && (
//                   <div className="fv-plugins-message-container">
//                     <div className="fv-help-block">{formik.errors.company}</div>
//                   </div>
//                 )}
//               </div>

//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Mother Name :
//                 </label>
//                 <input
//                   type="tel"
//                   className="form-control form-control-lg form-control-solid"
//                   placeholder="Phone number"
//                   {...formik.getFieldProps("contactPhone")}
//                 />
//                 {formik.touched.contactPhone && formik.errors.contactPhone && (
//                   <div className="fv-plugins-message-container">
//                     <div className="fv-help-block">
//                       {formik.errors.contactPhone}
//                     </div>
//                   </div>
//                 )}
//               </div>
//             </div>

//             <div className="row mb-1">
//               {/* <div className='col-lg-6 mb-1'>
//                 <label className='col-form-label required fw-bold fs-6'>Blood Group :</label>
//                 <input
//                   type='text'
//                   className='form-control form-control-lg form-control-solid'
//                 //   placeholder='Company name'
//                 //   {...formik.getFieldProps('company')}
//                 /> */}
//               {/* {formik.touched.company && formik.errors.company && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.company}</div>
//                   </div>
//                 )} */}
//               {/* </div> */}

//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Blood Group:
//                 </label>
//                 <select
//                   className="form-select form-select-lg form-select-solid"
//                   {...formik.getFieldProps("bloodGroup")}
//                 >
//                   <option value="">Select Blood Group</option>
//                   <option value="A+">A+</option>
//                   <option value="A-">A-</option>
//                   <option value="B+">B+</option>
//                   <option value="B-">B-</option>
//                   <option value="AB+">AB+</option>
//                   <option value="AB-">AB-</option>
//                   <option value="O+">O+</option>
//                   <option value="O-">O-</option>
//                 </select>
//                 {/* Display validation error if touched and error exists */}
//                 {formik.touched.bloodGroup && formik.errors.bloodGroup && (
//                   <div className="fv-plugins-message-container">
//                     <div className="fv-help-block">
//                       {formik.errors.bloodGroup}
//                     </div>
//                   </div>
//                 )}
//               </div>

//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Birth Place :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   //   placeholder='Phone number'
//                   //   {...formik.getFieldProps('contactPhone')}
//                 />
//                 {/* {formik.touched.contactPhone && formik.errors.contactPhone && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.contactPhone}</div>
//                   </div>
//                 )} */}
//               </div>
//             </div>

//             <div className="row mb-1">
//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Marital Status :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   //   placeholder='Company name'
//                   //   {...formik.getFieldProps('company')}
//                 />
//                 {/* {formik.touched.company && formik.errors.company && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.company}</div>
//                   </div>
//                 )} */}
//               </div>

//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Marriage Date :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   //   placeholder='Phone number'
//                   //   {...formik.getFieldProps('contactPhone')}
//                 />
//                 {/* {formik.touched.contactPhone && formik.errors.contactPhone && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.contactPhone}</div>
//                   </div>
//                 )} */}
//               </div>
//             </div>

//             <div className="row mb-1">
//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Citizen :{" "}
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   //   placeholder='Company name'
//                   //   {...formik.getFieldProps('company')}
//                 />
//                 {/* {formik.touched.company && formik.errors.company && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.company}</div>
//                   </div>
//                 )} */}
//               </div>

//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   National of :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   //   placeholder='Phone number'
//                   //   {...formik.getFieldProps('contactPhone')}
//                 />
//                 {/* {formik.touched.contactPhone && formik.errors.contactPhone && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.contactPhone}</div>
//                   </div>
//                 )} */}
//               </div>
//             </div>

//             <div className="row mb-1">
//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Maiden Name :{" "}
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   //   placeholder='Company name'
//                   //   {...formik.getFieldProps('company')}
//                 />
//                 {/* {formik.touched.company && formik.errors.company && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.company}</div>
//                   </div>
//                 )} */}
//               </div>

//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Religion :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   //   placeholder='Phone number'
//                   //   {...formik.getFieldProps('contactPhone')}
//                 />
//                 {/* {formik.touched.contactPhone && formik.errors.contactPhone && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.contactPhone}</div>
//                   </div>
//                 )} */}
//               </div>
//             </div>

//             <div className="row mb-1">
//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Social Sec. NO :{" "}
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   //   placeholder='Company name'
//                   //   {...formik.getFieldProps('company')}
//                 />
//                 {/* {formik.touched.company && formik.errors.company && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.company}</div>
//                   </div>
//                 )} */}
//               </div>

//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Hobby 1 :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   //   placeholder='Phone number'
//                   //   {...formik.getFieldProps('contactPhone')}
//                 />
//                 {/* {formik.touched.contactPhone && formik.errors.contactPhone && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.contactPhone}</div>
//                   </div>
//                 )} */}
//               </div>
//             </div>

//             <div className="row mb-1">
//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Hobby 2 :{" "}
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   //   placeholder='Company name'
//                   //   {...formik.getFieldProps('company')}
//                 />
//                 {/* {formik.touched.company && formik.errors.company && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.company}</div>
//                   </div>
//                 )} */}
//               </div>

//               <div className="col-lg-6 mb-1">
//                 <label className="col-form-label required fw-bold fs-6">
//                   Hobby 3 :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   //   placeholder='Phone number'
//                   //   {...formik.getFieldProps('contactPhone')}
//                 />
//                 {/* {formik.touched.contactPhone && formik.errors.contactPhone && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.contactPhone}</div>
//                   </div>
//                 )} */}
//               </div>
//             </div>

//             <div className="row mb-1">
//               <div>
//                 <label className="col-form-label required fw-bold fs-6">
//                   Hobby 4 :{" "}
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   //   placeholder='Company name'
//                   //   {...formik.getFieldProps('company')}
//                 />
//                 {/* {formik.touched.company && formik.errors.company && (
//                   <div className='fv-plugins-message-container'>
//                     <div className='fv-help-block'>{formik.errors.company}</div>
//                   </div>
//                 )} */}
//               </div>
//             </div>
//           </div>

//           <div className="card-footer d-flex justify-content-center py-6 px-9">
//             <button
//               type="submit"
//               className="btn btn-primary"
//               disabled={loading}
//             >
//               {!loading && "Save Changes"}
//               {loading && (
//                 <span
//                   className="indicator-progress"
//                   style={{ display: "block" }}
//                 >
//                   Please wait...{" "}
//                   <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
//                 </span>
//               )}
//             </button>
//           </div>
//         </form>
//       </div>
//     </div>
//   );
// };

// export { LanguageDetailsEdit };
import {FC} from 'react'

const LanguageDetailsEdit: FC = () => {
  return (<div >
          <h1>This Page is under Development...</h1>
          <h3>please contact to admin</h3>
          </div>)
}

export {LanguageDetailsEdit}
