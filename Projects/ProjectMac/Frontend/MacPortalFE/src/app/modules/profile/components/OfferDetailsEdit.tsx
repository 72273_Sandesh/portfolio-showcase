import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;


interface Response {
  responseStatus: string;
  message: string;
}


const OfferDetailsEdit: React.FC = () => {
  const [employee, setEmployee] = useState<any>({});
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/eMp0F2r`)
      .then((response) => {
        setEmployee(response.data[0]);
        // Update formik's initial values after fetching employee data
        formik.setValues({
          canD_CODE: response.data[0].canD_CODE || "",
          emP_CODE: response.data[0].emP_CODE || "",
          exP_DOJ: response.data[0].exP_DOJ || "",
          offereD_CTC: response.data[0].offereD_CTC || "",
          offereD_DESIGNATION: response.data[0].offereD_DESIGNATION || "",
          curR_CTC: response.data[0].curR_CTC || "",
          curR_DESIGNATION: response.data[0].curR_DESIGNATION || "",
          offeR_ID: response.data[0].offeR_ID || "",
          deV_JOIN_DAY:response.data[0].deV_JOIN_DAY || "",
          offeR_DATE:response.data[0].offeR_DATE || "",
          vaC_NO: response.data[0].vaC_NO || "",
        });
      })
      .catch((error) => {
        console.error("Error fetching candidate details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    canD_CODE: Yup.string().required("Candidate Code required"),
    emP_CODE: Yup.string().required("Employee Code required"),
    exP_DOJ: Yup.string().required("Expected Date of Joining required"),
    offereD_DESIGNATION: Yup.string().required("Offered Designation required"),
    offereD_CTC: Yup.string().required("Offered CTC required"),
    curR_DESIGNATION: Yup.string().required("Current Designation required"),
    curR_CTC: Yup.string().required("Current CTC required"),
    deV_JOIN_DAY: Yup.string().required("Deviation In Joining required"),
    offeR_ID: Yup.string().required("Offer ID required"),
    vaC_NO: Yup.string().required("Vacancy required"),
    offeR_DATE: Yup.string().required("Offer Date required"),
  });

  const formik = useFormik({
    initialValues: {
        canD_CODE: "",
        emP_CODE: "",
        exP_DOJ: "",
        offereD_DESIGNATION: "",
        offereD_CTC: "",
        curR_DESIGNATION: "",
        curR_CTC: "",
        deV_JOIN_DAY:"",
        offeR_ID: "",
        vaC_NO: "",
        offeR_DATE: "",
    },
    validationSchema: validationSchema,
    // onSubmit: (values) => {
    //   console.log('Submitting form data:', values);
    //   // Implement your submit logic here, e.g., send updated data to the server

    //   setEmployee({ sitE_NAME: '', depT_NAME: '' ,designation:'',section:""});
    //   formik.resetForm();// Upon successful submission, clear the form
    // },

    onSubmit: (values, formik) => {
      console.log("Submitting form data:", values);
    
      axios
        .post<Response>("your_backend_endpoint", values)
        .then((response) => {
          console.log("Post successful!", response.data);
          if (response.data.responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>{Array.isArray(response.data.message) ? response.data.message.join("\n") : response.data.message}</h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">Successfully Submitted Form...</h4>
                <p className="alert alert-success" role="alert">
                  <h6>{Array.isArray(response.data.message) ? response.data.message.join("\n") : response.data.message}</h6>
                </p>
              </div>
            );
    
            // Clear employee data and reset form
            setEmployee({
              canD_CODE: "",
              emP_CODE: "",
              exP_DOJ: "",
              offereD_DESIGNATION: "",
              offereD_CTC: "",
              curR_DESIGNATION: "",
              curR_CTC: "",
              deV_JOIN_DAY:"",
              offeR_ID: "",
              vaC_NO: "",
              offeR_DATE: "",
            });
            formik.resetForm();
            navigate(-1); // Redirect to previous page or desired location
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );
        });
    },
    
  });

  const handleClose = () => {
    navigate(-1);
    // navigate("/crafted/pages/profile/overview");
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-warning card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Offer Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="curR_CTC"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  PREVIOUS CTC :
                </label>
                <input
                  type="text"
                  id="curR_CTC"
                  className="form-control form-control-lg form-control-solid"
                  name="curR_CTC"
                  placeholder="Enter Previous CTC"
                  value={formik.values?.curR_CTC}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="curR_DESIGNATION"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  PREVIOUS DESIGNATION:
                </label>
                <input
                  type="text"
                  id="curR_DESIGNATION"
                  className="form-control form-control-lg form-control-solid"
                  name="curR_DESIGNATION"
                  placeholder="Enter Previous Designation"
                  value={formik.values.curR_DESIGNATION?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.curR_CTC && formik.errors.curR_CTC ? (
                  <div className="text-danger">{formik.errors.curR_CTC}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.curR_DESIGNATION && formik.errors.curR_DESIGNATION ? (
                  <div className="text-danger">{formik.errors.curR_DESIGNATION}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="offereD_CTC"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  OFFERED CTC:
                </label>
                <input
                  type="text"
                  id="offereD_CTC"
                  className="form-control form-control-lg form-control-solid"
                  name="offereD_CTC"
                  placeholder="Enter Previous CTC"
                  value={formik.values?.offereD_CTC}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="offereD_DESIGNATION"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  OFFERED DESIGNATION:
                </label>
                <input
                  type="text"
                  id="offereD_DESIGNATION"
                  className="form-control form-control-lg form-control-solid"
                  name="offereD_DESIGNATION"
                  placeholder="Enter Offered CTC"
                  value={formik.values.offereD_DESIGNATION?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.offereD_CTC && formik.errors.offereD_CTC ? (
                  <div className="text-danger">{formik.errors.offereD_CTC}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.offereD_DESIGNATION && formik.errors.offereD_DESIGNATION ? (
                  <div className="text-danger">{formik.errors.offereD_DESIGNATION}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}
            
            {/* Begin: Edit Row */}
            <div className="row mb-1">
            <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  EXPECTED JOIN DATE:
                </label>
                <input
                  type="date"
                  className="form-control form-control-lg form-control-solid"
                  name="exP_DOJ"
                  placeholder="Enter Expected Join Date"
                  value={
                    formik.values?.exP_DOJ
                      ? new Date(formik.values?.exP_DOJ)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  DEVIATION IN JOINING:
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="deV_JOIN_DAY"
                  placeholder="Enter Current Department name"
                  value={formik.values?.deV_JOIN_DAY}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.exP_DOJ && formik.errors.exP_DOJ ? (
                  <div className="text-danger">{formik.errors.exP_DOJ}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.deV_JOIN_DAY && formik.errors.deV_JOIN_DAY ? (
                  <div className="text-danger">{formik.errors.deV_JOIN_DAY}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="vaC_NO"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  VACANCY:
                </label>
                <input
                  type="text"
                  id="vaC_NO"
                  className="form-control form-control-lg form-control-solid"
                  name="vaC_NO"
                  placeholder="Enter Previous CTC"
                  value={formik.values?.vaC_NO}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="offeR_ID"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  OFFER ID:
                </label>
                <input
                  type="text"
                  id="offeR_ID"
                  className="form-control form-control-lg form-control-solid"
                  name="offeR_ID"
                  placeholder="Enter Previous Designation"
                  value={formik.values.offeR_ID?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.vaC_NO && formik.errors.vaC_NO ? (
                  <div className="text-danger">{formik.errors.vaC_NO}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.offeR_ID && formik.errors.offeR_ID ? (
                  <div className="text-danger">{formik.errors.offeR_ID}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}


            {/* Begin: Edit Row */}
          <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="offeR_DATE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                 OFFER DATE:
                </label>
                <input
                  type="date"
                  id="offeR_DATE"
                  className="form-control form-control-lg form-control-solid"
                  name="offeR_DATE"
                  placeholder="Enter date of joining"
                  value={
                    formik.values?.offeR_DATE
                      ? new Date(formik.values?.offeR_DATE)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>             
            </div>

            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.offeR_DATE && formik.errors.offeR_DATE ? (
                  <div className="text-danger">
                    {formik.errors.offeR_DATE}
                  </div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

          </div>

          {/* Logic for Submit is HERE */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default OfferDetailsEdit;
