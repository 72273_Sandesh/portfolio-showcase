// import { Link } from "react-router-dom";
// import { KTIcon } from "../../../../_metronic/helpers";

// export function PersonalDetails() {
//   // const [isCollapsed, setIsCollapsed] = useState(true);

//   // const toggleCollapse = () => {
//   //   setIsCollapsed(!isCollapsed);
//   // };

//   return (
//     <div className="card mb-2 mb-xl-2" id="Personal Details">
//       <div
//         className="card-header cursor-pointer"
//         role="button"
//         // onClick={toggleCollapse}
//         // aria-expanded={!isCollapsed}
//         aria-controls="kt_account_profile_details"
//       >
//         <Link
//           to="/crafted/pages/profile/basicDetails"
//           className="btn btn-sm btn-light-primary me-3 align-self-center"
//         >
//           <KTIcon iconName="profile-circle" className="fs-2x text-dark" />
//           Personal Details
//         </Link>

//         <Link
//           to="/crafted/pages/profile/contactDetails"
//           className="btn btn-sm btn-light-primary me-3 align-self-center"
//         >
//           <KTIcon iconName="whatsapp" className="fs-2x text-dark" />
//           Contact Details
//         </Link>

//         <Link
//           to="/crafted/pages/profile/work"
//           className="btn btn-sm btn-light-primary me-3 align-self-center"
//         >
//           <KTIcon iconName="office-bag" className="fs-2x text-dark" />
//           Employment Details
//         </Link>

//         <Link
//           to="/crafted/pages/profile/kyc"
//           //src/app/modules/profile/components/KYCWrapper.tsx
//           className="btn btn-sm btn-light-primary me-3 align-self-center"
//         >
//           <KTIcon iconName="setting-2" className="fs-2x text-dark" />
//           KYC Details
//         </Link>
//       </div>
//     </div>
//   );
// }
import { Link, useLocation } from "react-router-dom";
import { useState, useEffect } from "react";

export function PersonalDetails() {
  const location = useLocation();
  const [activeButton, setActiveButton] = useState(location.pathname);

  useEffect(() => {
    setActiveButton(location.pathname);
  }, [location]);

  const getButtonClass = (path: string) => {
    return activeButton === path
      ? "btn btn-sm btn-primary me-3 align-self-center"
      : "btn btn-sm btn-light-primary me-3 align-self-center";
  };

  return (
    <div className="card mb-2 mb-xl-2" id="Personal Details">
      <div
        className="card-header cursor-pointer"
        role="button"
        aria-controls="kt_account_profile_details"
      >
        <Link
          to="/crafted/pages/profile/basicDetails"
          className={getButtonClass("/crafted/pages/profile/basicDetails")}
          onClick={() => setActiveButton("/crafted/pages/profile/basicDetails")}
        >
          {/* <KTIcon iconName="profile-circle" className="fs-2x text-dark" /> */}
          <img
            src="/media/avatars/project.png"
            alt="Contact Icon"
            className="fs-2x text-dark"
            style={{
              width: "32px",
              height: "32px",
              marginRight: "8px",
            }}
          />
          Personal Details
        </Link>

        {/* <Link
          to="/crafted/pages/profile/contactDetails"
          className={getButtonClass("/crafted/pages/profile/contactDetails")}
          onClick={() => setActiveButton("/crafted/pages/profile/contactDetails")}
        >
          <KTIcon iconName="whatsapp" className="fs-2x text-dark" />
          Contact Details
        </Link> */}

        <Link
          to="/crafted/pages/profile/contactDetails"
          className={getButtonClass("/crafted/pages/profile/contactDetails")}
          onClick={() =>
            setActiveButton("/crafted/pages/profile/contactDetails")
          }
        >
          <img
            src="/media/avatars/contact.png"
            alt="Contact Icon"
            className="fs-2x text-dark"
            style={{
              width: "32px",
              height: "32px",
              marginRight: "8px",
            }}
          />
          Contact Details
        </Link>

        <Link
          to="/crafted/pages/profile/work"
          className={getButtonClass("/crafted/pages/profile/work")}
          onClick={() => setActiveButton("/crafted/pages/profile/work")}
        >
          {/* <KTIcon iconName="office-bag" className="fs-2x text-dark" /> */}
          <img
            src="/media/avatars/hrm.png"
            alt="Contact Icon"
            className="fs-2x text-dark"
            style={{
              width: "32px",
              height: "32px",
              marginRight: "8px",
            }}
          />
          Employment Details
        </Link>

        <Link
          to="/crafted/pages/profile/kyc"
          className={getButtonClass("/crafted/pages/profile/kyc")}
          onClick={() => setActiveButton("/crafted/pages/profile/kyc")}
        >
          {/* <KTIcon iconName="setting-2" className="fs-2x text-dark" /> */}
          <img
            src="/media/avatars/data.png"
            alt="Contact Icon"
            className="fs-2x text-dark"
            style={{
              width: "32px",
              height: "32px",
              marginRight: "8px",
            }}
          />
          KYC Details
        </Link>
      </div>
    </div>
  );
}
