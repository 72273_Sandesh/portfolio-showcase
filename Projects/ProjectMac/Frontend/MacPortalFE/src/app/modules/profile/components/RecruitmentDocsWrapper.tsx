import { Content } from "../../../../_metronic/layout/components/content";
import {  RecruitmentDocs } from "../../../../_metronic/partials/widgets/tables/RecruitmentDocs";

export function RecruitmentDocsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
      <RecruitmentDocs className='mb-5 mb-xl-8' />
      </div>
    </Content>
  );
}
