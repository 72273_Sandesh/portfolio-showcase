import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;


interface Response {
  responseStatus:string;
  message:string;
}

const SeperationDetailsEdit: React.FC = () => {
  const [employee, setEmployee] = useState<any>({});
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/GetEmployeeSeperationDetails/${employeeIdForView}`)
      .then((response) => {
        setEmployee(response.data[0]);
        // Update formik's initial values after fetching employee data
        formik.setValues({
          //   induction: response.data[0].induction || "",
          emP_CODE: response.data[0].emP_CODE || "",
          emP_NAME: response.data[0].emP_NAME || "",
          doj: response.data[0].doj || "",
          resigN_DATE: response.data[0].resigN_DATE || "",
          relievE_DATE: response.data[0].relievE_DATE || "",
          hR_RECD_DATE: response.data[0].hR_RECD_DATE || "",
          seP_MODE: response.data[0].seP_MODE || "",
          status: response.data[0].status || "",
          resigN_REASON: response.data[0].resigN_REASON || "",
          neT_AMT: response.data[0].neT_AMT || "",
          remarks: response.data[0].remarks || "",
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    emP_CODE: Yup.string(),
    emP_NAME: Yup.string().required("Employee name required"),
    doj: Yup.string().required("Date of join required"),
    resigN_DATE: Yup.string().required("Resign date is required"),
    relievE_DATE: Yup.string().required("Relieve date required"),
    hR_RECD_DATE: Yup.string().required("HR record date is required"),
    seP_MODE: Yup.string().required("Seperation mode required"),
    status: Yup.string().required("Status is required"),
    resigN_REASON: Yup.string().required("Resign date is required"),
    neT_AMT: Yup.string().required("Net amount is required"),
    remarks: Yup.string().required("Remarks required"),
  });

  const formik = useFormik({
    initialValues: {
      emP_CODE: "",
      emP_NAME: "",
      doj: "",
      resigN_DATE: "",
      relievE_DATE: "",
      hR_RECD_DATE: "",
      seP_MODE: "",
      status: "",
      resigN_REASON: "",
      neT_AMT: "",
      remarks: "",
    },
    validationSchema: validationSchema,
    // onSubmit: (values) => {
    //   console.log('Submitting form data:', values);
    //   // Implement your submit logic here, e.g., send updated data to the server

    //   setEmployee({ sitE_NAME: '', depT_NAME: '' ,designation:'',section:""});
    //   formik.resetForm();// Upon successful submission, clear the form
    // },

    onSubmit: (values) => {
      console.log("Submitting form data:", values);
    
      axios
        .post<Response>("your_backend_endpoint", values)
        .then((response) => {
          console.log("Post successful!", response.data);
          if (response.data.responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>{Array.isArray(response.data.message) ? response.data.message.join("\n") : response.data.message}</h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">Successfully Submitted Form...</h4>
                <p className="alert alert-success" role="alert">
                  <h6>{Array.isArray(response.data.message) ? response.data.message.join("\n") : response.data.message}</h6>
                </p>
              </div>
            );
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );
        });
    
      setEmployee({
        emP_CODE: "",
        emP_NAME: "",
        doj: "",
        resigN_DATE: "",
        relievE_DATE: "",
        hR_RECD_DATE: "",
        seP_MODE: "",
        status: "",
        resigN_REASON: "",
        neT_AMT: "",
        remarks: "",
      });
      formik.resetForm();
      navigate(-1);
    },
    
  });

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Separation Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
{/* Begin: Edit Row */}
<div className="row mb-1">
  <div className="col-lg-6 d-flex align-items-center mb-1">
    <label
      htmlFor="emP_CODE"
      className="col-form-label required fw-bold fs-6 me-2"
      style={{ minWidth: "150px" }}
    >
      Emp Code :
    </label>
    <input
      type="text"
      id="emP_CODE"
      className="form-control form-control-lg form-control-solid"
      name="emP_CODE"
      placeholder="Enter employee code"
      value={formik.values?.emP_CODE}
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
    />
  </div>
  <div className="col-lg-6 d-flex align-items-center mb-1">
    <label
      htmlFor="emP_NAME"
      className="col-form-label required fw-bold fs-6 me-2"
      style={{ minWidth: "150px" }}
    >
      Employee Name :
    </label>
    <input
      type="text"
      id="emP_NAME"
      className="form-control form-control-lg form-control-solid"
      name="emP_NAME"
      placeholder="Enter employee name"
      value={formik.values.emP_NAME?.toUpperCase()}
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
    />
  </div>
</div>
<div className="row mb-1">
  <div className="col-lg-6">
    {formik.touched.emP_CODE && formik.errors.emP_CODE ? (
      <div className="text-danger">{formik.errors.emP_CODE}</div>
    ) : null}
  </div>
  <div className="col-lg-6">
    {formik.touched.emP_NAME && formik.errors.emP_NAME ? (
      <div className="text-danger">{formik.errors.emP_NAME}</div>
    ) : null}
  </div>
</div>

{/* End: Edit Row */}


{/* Begin: Edit Row */}
<div className="row mb-1">
  <div className="col-lg-6 d-flex align-items-center mb-1">
    <label
      htmlFor="doj"
      className="col-form-label required fw-bold fs-6 me-2"
      style={{ minWidth: "150px" }}
    >
      Date of Joining :
    </label>
    <input
      type="date"
      id="doj"
      className="form-control form-control-lg form-control-solid"
      name="doj"
      placeholder="Select date of joining"
      value={
        formik.values?.doj
          ? new Date(formik.values?.doj).toISOString().split("T")[0]
          : ""
      }
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
    />
  </div>
  <div className="col-lg-6 d-flex align-items-center mb-1">
    <label
      htmlFor="hR_RECD_DATE"
      className="col-form-label required fw-bold fs-6 me-2"
      style={{ minWidth: "150px" }}
    >
      HR Record Date :
    </label>
    <input
      type="date"
      id="hR_RECD_DATE"
      className="form-control form-control-lg form-control-solid"
      name="hR_RECD_DATE"
      placeholder="Select HR record date"
      value={
        formik.values?.hR_RECD_DATE
          ? new Date(formik.values?.hR_RECD_DATE).toISOString().split("T")[0]
          : ""
      }
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
    />
  </div>
</div>
<div className="row mb-1">
  <div className="col-lg-6">
    {formik.touched.doj && formik.errors.doj ? (
      <div className="text-danger">{formik.errors.doj}</div>
    ) : null}
  </div>
  <div className="col-lg-6">
    {formik.touched.hR_RECD_DATE && formik.errors.hR_RECD_DATE ? (
      <div className="text-danger">{formik.errors.hR_RECD_DATE}</div>
    ) : null}
  </div>
</div>

{/* End: Edit Row */}


{/* Begin: Edit Row */}
<div className="row mb-1">
  <div className="col-lg-6 d-flex align-items-center mb-1">
    <label
      htmlFor="resigN_DATE"
      className="col-form-label required fw-bold fs-6 me-2"
      style={{ minWidth: "150px" }}
    >
      Resign Date :
    </label>
    <input
      type="date"
      id="resigN_DATE"
      className="form-control form-control-lg form-control-solid"
      name="resigN_DATE"
      placeholder="Select resign date"
      value={
        formik.values?.resigN_DATE
          ? new Date(formik.values?.resigN_DATE).toISOString().split("T")[0]
          : ""
      }
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
    />
  </div>
  <div className="col-lg-6 d-flex align-items-center mb-1">
    <label
      htmlFor="relievE_DATE"
      className="col-form-label required fw-bold fs-6 me-2"
      style={{ minWidth: "150px" }}
    >
      Relieve Date :
    </label>
    <input
      type="date"
      id="relievE_DATE"
      className="form-control form-control-lg form-control-solid"
      name="relievE_DATE"
      placeholder="Select relieve date"
      value={
        formik.values?.relievE_DATE
          ? new Date(formik.values?.relievE_DATE).toISOString().split("T")[0]
          : ""
      }
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
    />
  </div>
</div>
<div className="row mb-1">
  <div className="col-lg-6">
    {formik.touched.resigN_DATE && formik.errors.resigN_DATE ? (
      <div className="text-danger">{formik.errors.resigN_DATE}</div>
    ) : null}
  </div>
  <div className="col-lg-6">
    {formik.touched.relievE_DATE && formik.errors.relievE_DATE ? (
      <div className="text-danger">{formik.errors.relievE_DATE}</div>
    ) : null}
  </div>
</div>

{/* End: Edit Row */}


{/* Begin: Edit Row */}
<div className="row mb-1">
  <div className="col-lg-6 d-flex align-items-center mb-1">
    <label
      htmlFor="seP_MODE"
      className="col-form-label required fw-bold fs-6 me-2"
      style={{ minWidth: "150px" }}
    >
      Separation Mode :
    </label>
    <input
      type="text"
      id="seP_MODE"
      className="form-control form-control-lg form-control-solid"
      name="seP_MODE"
      placeholder="Enter separation mode"
      value={formik.values?.seP_MODE}
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
    />
  </div>
  <div className="col-lg-6 d-flex align-items-center mb-1">
    <label
      htmlFor="status"
      className="col-form-label required fw-bold fs-6 me-2"
      style={{ minWidth: "150px" }}
    >
      Status :
    </label>
    <select
      className="form-select form-select-lg form-select-solid"
      name="status"
      value={formik.values.status}
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
    >
      <option value="">{formik.values.status}</option>
      <option value="active">Approved</option>
      <option value="inactive">Un-Approved</option>
      <option value="pending">Pending</option>
    </select>
  </div>
</div>
<div className="row mb-1">
  <div className="col-lg-6">
    {formik.touched.seP_MODE && formik.errors.seP_MODE ? (
      <div className="text-danger">{formik.errors.seP_MODE}</div>
    ) : null}
  </div>
  <div className="col-lg-6">
    {formik.touched.status && formik.errors.status ? (
      <div className="text-danger">{formik.errors.status}</div>
    ) : null}
  </div>
</div>

{/* End: Edit Row */}


{/* Begin: Edit Row */}
<div className="row mb-1">
  <div className="col-lg-6 d-flex align-items-center mb-1">
    <label
      htmlFor="resigN_REASON"
      className="col-form-label required fw-bold fs-6 me-2"
      style={{ minWidth: "150px" }}
    >
      Resign Reason :
    </label>
    <input
      type="text"
      id="resigN_REASON"
      className="form-control form-control-lg form-control-solid"
      name="resigN_REASON"
      placeholder="Enter resign reason"
      value={formik.values?.resigN_REASON}
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
    />
  </div>
  <div className="col-lg-6 d-flex align-items-center mb-1">
    <label
      htmlFor="neT_AMT"
      className="col-form-label required fw-bold fs-6 me-2"
      style={{ minWidth: "150px" }}
    >
      Net Amount :
    </label>
    <input
      type="text"
      id="neT_AMT"
      className="form-control form-control-lg form-control-solid"
      name="neT_AMT"
      placeholder="Enter net amount"
      value={formik.values?.neT_AMT || ""}
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
    />
  </div>
</div>
<div className="row mb-1">
  <div className="col-lg-6">
    {formik.touched.resigN_REASON && formik.errors.resigN_REASON ? (
      <div className="text-danger">{formik.errors.resigN_REASON}</div>
    ) : null}
  </div>
  <div className="col-lg-6">
    {formik.touched.neT_AMT && formik.errors.neT_AMT ? (
      <div className="text-danger">{formik.errors.neT_AMT}</div>
    ) : null}
  </div>
</div>

{/* End: Edit Row */}


{/* Begin: Edit Row */}
<div className="row mb-1">
  <div className="d-flex align-items-center mb-1">
    <label
      htmlFor="remarks"
      className="col-form-label required fw-bold fs-6 me-2"
      style={{ minWidth: "150px" }}
    >
      Remark :
    </label>
    <textarea
      id="remarks"
      className="form-control form-control-lg form-control-solid"
      name="remarks"
      placeholder="Enter Remark"
      value={formik.values?.remarks.toUpperCase()}
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
    />
  </div>
</div>
<div className="row mb-1">
  <div className="col-lg-6">
    {formik.touched.remarks && formik.errors.remarks ? (
      <div className="text-danger">{formik.errors.remarks}</div>
    ) : null}
  </div>
</div>

{/* End: Edit Row */}


 

          </div>
        

         

          {/* Logic for Submit is HERE */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
          <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default SeperationDetailsEdit;
