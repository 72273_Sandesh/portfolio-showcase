import {FC} from 'react'

type Props = {
  depT_HEAD: string 
}

const DesignationTwoStepsCell: FC<Props> = ({depT_HEAD}) => (
  <>
   {/* <div className='badge badge-light-success fw-bolder d-flex align-items-center'>{deptHead ?(deptHead):('Not Assigned')}</div>*/}
   <div className=' fw-bolder d-flex align-items-right'>
      {depT_HEAD ?(depT_HEAD.toUpperCase()): <div className='badge badge-danger fw-bolder d-flex align-items-right'>NOT ASSIGNED</div>}
    </div>
    </> 
)

export {DesignationTwoStepsCell}


