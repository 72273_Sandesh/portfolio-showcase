const QUERIES = {
  EmployeeSearch_LIST: 'employeeSearch-list',
  EMPLOYEE_LIST:'employee-list',
  DEPARTMENT_LIST:'department-list',
  QUALIFICATION_LIST: 'qualification-list',
  CADRE_LIST:'cadre-list',
  GRADE_LIST:'grade-list',
  USERS_LIST: 'users-list',
  BANK_DETAILS_LIST:'bankDetails-list',
  Designations_LIST:'designations-list'
}

export {QUERIES}
