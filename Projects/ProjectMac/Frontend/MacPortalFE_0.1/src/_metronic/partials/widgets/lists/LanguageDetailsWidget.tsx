// import { FC, useEffect, useState } from "react";
// import { KTIcon } from "../../../helpers";
// import axios from "axios";
// import { Link } from "react-router-dom";

// type Language = {
//   name: string;
//   proficiency: number;
// };

// type Props = {
//   className: string;
// };

// const LanguageDetailsWidget: FC<Props> = ({ className }) => {
//   const [languages, setLanguages] = useState<Language[]>([]);
//   const [isCollapsed, setIsCollapsed] = useState(true);

//   useEffect(() => {
//     // Fetch language details from API
//     axios
//       .get("https://localhost:44360/api/Employee/H004711/EMPLANG")
//       .then((response) => {
//         setLanguages(response.data);
//       })
//       .catch((error) => {
//         console.error("Error fetching language details:", error);
//       });
//   }, []);

//   const toggleCollapse = () => {
//     setIsCollapsed(!isCollapsed);
//   };

//   return (
//     <div className={`card ${className}`} id="Language Details">
//       <div
//         className="card-header border-0 cursor-pointer"
//         role="button"
//         onClick={toggleCollapse}
//         aria-expanded={!isCollapsed}
//         aria-controls="kt_language_details"
//       >
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Language Details</h3>
//         </div>
       
//       </div>
//       <div
//         id="kt_language_details"
//         className={`collapse ${isCollapsed ? "" : "show"} card-body pt-0`}
//       >
//         {languages.map((language, index) => (
//           <div
//             key={index}
//             className={`d-flex align-items-center bg-light-${
//               index % 4 === 0
//                 ? "warning"
//                 : index % 4 === 1
//                 ? "success"
//                 : index % 4 === 2
//                 ? "danger"
//                 : "info"
//             } rounded p-5 mb-7`}
//           >
//             <div className="d-flex flex-wrap flex-stack">
//               <div className="d-flex flex-column flex-grow-1 pe-8">
//                 <div className="d-flex flex-wrap">
//                   <div className=" min-w-30px py-0 px-4 me-6 mb-3 d-flex justify-content-center align-items-center">
//                     <span
//                       className={`text-${
//                         index % 4 === 0
//                           ? "warning"
//                           : index % 4 === 1
//                           ? "success"
//                           : index % 4 === 2
//                           ? "danger"
//                           : "info"
//                       } me-5`}
//                     >
//                       <KTIcon
//                         iconName="abstract-26"
//                         className={`text-${
//                           index % 4 === 0
//                             ? "warning"
//                             : index % 4 === 1
//                             ? "success"
//                             : index % 4 === 2
//                             ? "danger"
//                             : "info"
//                         } fs-1 me-5`}
//                       />
//                     </span>
//                   </div>

//                   <div className="min-w-125px py-0 px-4 me-6 mb-3 d-flex justify-content-center align-items-center">
//                     <a
//                       href="#"
//                       className="fw-bold text-gray-800 text-hover-primary fs-6"
//                       style={{ margin: "0" }} // Adjust margin as needed
//                     >
//                       {language.language}
//                     </a>
//                   </div>

//                   <div className=" min-w-125px py-0 px-4 me-6 mb-3">
//                     <div className="d-flex align-items-center">
//                       <div className="d-flex align-items-center mt-3">
//                         <div
//                           className="form-check form-check-custom form-check-solid me-5"
//                           style={{
//                             borderColor:
//                               language.read === "Y"
//                                 ? "green"
//                                 : language.read === "N"
//                                 ? "red"
//                                 : "initial",
//                           }}
//                         >
//                           <input
//                             className="form-check-input"
//                             type="checkbox"
//                             id="readCheckbox"
//                             checked={language.read === "Y"}
//                             readOnly
//                             style={{ width: "16px", height: "16px" }} // Smaller size
//                           />
//                           <label
//                             className="form-check-label fw-bold"
//                             htmlFor="readCheckbox"
//                           >
//                             Read
//                           </label>
//                         </div>
//                         <div
//                           className="form-check form-check-custom form-check-solid me-5"
//                           style={{
//                             borderColor:
//                               language.write === "Y"
//                                 ? "green"
//                                 : language.write === "N"
//                                 ? "red"
//                                 : "initial",
//                           }}
//                         >
//                           <input
//                             className="form-check-input"
//                             type="checkbox"
//                             id="writeCheckbox"
//                             checked={language.write === "Y"}
//                             readOnly
//                             style={{ width: "16px", height: "16px" }} // Smaller size
//                           />
//                           <label
//                             className="form-check-label fw-bold"
//                             htmlFor="writeCheckbox"
//                           >
//                             Write
//                           </label>
//                         </div>
//                         <div
//                           className="form-check form-check-custom form-check-solid"
//                           style={{
//                             borderColor:
//                               language.speak === "Y"
//                                 ? "green"
//                                 : language.speak === "N"
//                                 ? "red"
//                                 : "initial",
//                           }}
//                         >
//                           <input
//                             className="form-check-input"
//                             type="checkbox"
//                             id="speakCheckbox"
//                             checked={language.speak === "Y"}
//                             readOnly
//                             style={{ width: "16px", height: "16px" }} // Smaller size
//                           />
//                           <label
//                             className="form-check-label fw-bold"
//                             htmlFor="speakCheckbox"
//                           >
//                             Speak
//                           </label>
//                         </div>
//                       </div>
//                     </div>
//                   </div>

//                   <div className=" min-w-170px py-0 px-4 me-6 mb-3">
//                     <div className="d-flex align-items-center">
//                       <span
//                         className={`fw-bold text-${
//                           index % 4 === 0
//                             ? "warning"
//                             : index % 4 === 1
//                             ? "success"
//                             : index % 4 === 2
//                             ? "danger"
//                             : "info"
//                         } py-1`}
//                       >
//                         proficiency : {language.worK_PROFICIENCY}%
//                       </span>
//                     </div>
//                     <div className="progress h-6px w-300px">
//                       <div
//                         className={`progress-bar bg-${
//                           index % 4 === 0
//                             ? "warning"
//                             : index % 4 === 1
//                             ? "success"
//                             : index % 4 === 2
//                             ? "danger"
//                             : "info"
//                         }`}
//                         role="progressbar"
//                         style={{ width: "78%" }}
//                       ></div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         ))}
//       </div>
//     </div>
//   );
// };

// export { LanguageDetailsWidget };
import {FC} from 'react'

const LanguageDetailsWidget: FC = () => {
  return (<div >
          <h1>This Page is under Development...</h1>
          <h3>please contact to admin</h3>
          </div>)
}

export {LanguageDetailsWidget}