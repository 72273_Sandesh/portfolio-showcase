import React, { useState, useEffect } from "react";
import { KTIcon } from "../../../helpers";
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";
import {
  employeeIdForView,
  setFMIdForUpdate,
} from "../../../../app/modules/GlobalQuery";

type Props = {
  className: string;
};
interface qualification {
  seQ_NO: string;
  pasS_YEAR: string;
  qualification: string;
  degreE_NAME: string;
  university: string;
  institute: string;
  class: string;
  subjecT1: string;
  subjecT2: string;
  subjecT3: string;
  subjecT4: string;
  quaL_STATUS: string;
  exP_COMP_DATE: string;
  perc: number;
}
const QualificationDetailsWidget: React.FC<Props> = ({ className }) => {
  const [qualifications, setQualifications] = useState<qualification[]>([]);
  const navigate = useNavigate();
  const formattedDate = (date: Date) => {
    return new Date(date).toLocaleString("en-GB", {
      day: "2-digit",
      month: "2-digit",
      year: "numeric",
    });
  };
  const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

  useEffect(() => {
    // Fetch basic details from the backend API
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPEDU`)
      .then((response) => {
        setQualifications(response.data);
      })
      .catch((error) => {
        console.error("Error fetching qualification data:", error);
      });
  }, [EMPLOYEE_URL]);

  const getProgressBarColor = (perc: number) => {
    if (perc < 40) {
      return "bg-danger"; // Red
    } else if (perc >= 40 && perc < 60) {
      return "bg-warning"; // Yellow
    } else {
      return "bg-success"; // Green
    }
  };

  const editQualification = (seqNo: number) => {
    setFMIdForUpdate(seqNo - 1);
    navigate("/crafted/pages/profile/QualificationDetailsModalWrapper");
  };

  return (
    <div className={`card ${className}`}>
      {/* begin::Header */}
      <div className="card-header bg-light-primary align-items-center border-0 mt-0">
        <h3 className="card-title align-items-start flex-column">
          <span className="fw-bold mb-2 text-gray-900">
            Qualification Details
          </span>
        </h3>
        <div className="d-flex my-2">
          <div className="fs-5 fw-bold mb-2 ps-3 d-flex justify-content-between">
            <Link
              to="/crafted/pages/profile/AddQualificationDetailsWrapper"
              type="button"
              className="btn btn-primary btn-sm"
              data-kt-menu-trigger="click"
              data-kt-menu-placement="bottom-end"
              data-kt-menu-flip="top-end"
            >
              Add
            </Link>
          </div>
        </div>
      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className="card-body pt-5">
        {/* begin::Timeline */}
        <div className="timeline-label">
          {/* Render qualification items */}
          {qualifications.map((qualification) => (
            <div key={qualification.seQ_NO} className="timeline-item">
              <div className="timeline-label fw-bold text-gray-800 fs-6">
                {qualification.pasS_YEAR}
              </div>
              <div className="timeline-badge">
                <div className="timeline-icon symbol symbol-circle symbol-30px">
                  <div className="symbol-label bg-light">
                    <KTIcon iconName="teacher" className="fs-2 text-primary" />
                  </div>
                </div>
              </div>
              <div className="fw-mormal timeline-content text-muted min-w-100px">
                <div className="fs-5 fw-bold mb-2 ps-3 d-flex justify-content-between">
                  {qualification.degreE_NAME}
                  <button
                    className="btn btn-primary btn-sm"
                    onClick={() =>
                      editQualification(+qualification.seQ_NO.trim())
                    }
                  >
                    Edit
                  </button>
                </div>
                {/* Begin :: Qualification Card */}
                <div className="card-body p-2 qualification-details">
                  <div className="row">
                    {/* Basic Details */}
                    <div className="col-md-6">
                      <div className="max-w-500px py-3 px-4 me-6 mb-1">
                        <div className="card-body p-1">
                          <div className="row mb-3">
                            <label className="col-lg-4 fw-bold text-muted">
                              QUALIFICATION
                            </label>
                            <div className="col-lg-8">
                              <div className="fw-bold fs-6 text-gray-900 mw-250px">
                                {qualification.qualification}
                              </div>
                            </div>
                          </div>
                          <div className="row mb-3">
                            <label className="col-lg-4 fw-bold text-muted">
                              BOARD / UNIVERSITY
                            </label>
                            <div className="col-lg-8">
                              <div className="fw-bold fs-6 text-gray-900 mw-250px">
                                {qualification.university}
                              </div>
                            </div>
                          </div>
                          <div className="row mb-3">
                            <label className="col-lg-4 fw-bold text-muted">
                              INSTITUTE
                            </label>
                            <div className="col-lg-8">
                              <span className="fw-bold fs-6 text-gray-900">
                                {qualification.institute}
                              </span>
                            </div>
                          </div>
                          <div className="row mb-3">
                            <label className="col-lg-4 fw-bold text-muted">
                              GRADE
                            </label>
                            <div className="col-lg-8 mb-3 mb-md-0 fw-bold">
                              <span className="fw-bold fs-6 text-gray-900">
                                {qualification.class}
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Subject Details */}
                    <div className="col-lg-4">
                      <div className="min-w-400px py-3 px-4 me-6 mb-3">
                        <div className="d-flex flex-wrap flex-stack">
                          <div className="d-flex flex-column flex-grow-1 pe-8">
                            <div className="d-flex flex-wrap">
                              {qualification.subjecT1 && (
                                <div className="border border-gray-300 rounded min-w-50px py-1 px-2 me-3 mb-3">
                                  <div className="fw-bold fs-6 text-gray-600">
                                    {qualification.subjecT1}
                                  </div>
                                </div>
                              )}
                              {qualification.subjecT2 && (
                                <div className="border border-gray-300 rounded min-w-50px py-1 px-2 me-3 mb-3">
                                  <div className="fw-bold fs-6 text-gray-600">
                                    {qualification.subjecT2}
                                  </div>
                                </div>
                              )}
                              {qualification.subjecT3 && (
                                <div className="border border-gray-300 rounded min-w-50px py-1 px-2 me-3 mb-3">
                                  <div className="fw-bold fs-6 text-gray-600">
                                    {qualification.subjecT3}
                                  </div>
                                </div>
                              )}
                              {qualification.subjecT4 && (
                                <div className="border border-gray-300 rounded min-w-50px py-1 px-2 me-3 mb-3">
                                  <div className="fw-bold fs-6 text-gray-600">
                                    {qualification.subjecT4}
                                  </div>
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Status Details */}
                    <div className="col-lg-2 d-flex align-items-center">
                      <div className="min-w-125px py-3 px-4 me-6 mb-3">
                        <div className="card-toolbar d-flex align-items-center">
                          {qualification.quaL_STATUS === "COMPLETED" ||
                          qualification.quaL_STATUS === "C" ? (
                            <>
                              <KTIcon
                                iconName="shield-tick"
                                className="text-success fs-1 me-2"
                              />
                              COMPLETED
                            </>
                          ) : (
                            <>
                              <KTIcon
                                iconName="shield-cross"
                                className="text-danger fs-1 me-2"
                              />
                              PERSUING
                            </>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Percentage Details */}
                  {qualification.quaL_STATUS !== "COMPLETED" ? (
                    // <div className="py-3 px-4 me-6 mb-1">
                    <div className="row py-3 px-5 me-6 mb-3">
                      <label className="col-lg-4 fw-bold text-muted">
                        EXPECTED COMPLETION DATE
                      </label>
                      <div className="col-lg-6 mb-3 mb-md-0 fw-bold">
                        <span className="fw-bold fs-6 text-gray-900">
                          {formattedDate(
                            qualification.exP_COMP_DATE
                              ? new Date(qualification.exP_COMP_DATE)
                              : new Date()
                          )}
                        </span>
                      </div>
                    </div>
                  ) : (
                    // </div>
                    <div className="card-body d-flex align-items-end pt-0 p-5">
                      <div className="d-flex align-items-center justify-content-center w-100 flex-column">
                        <div className="d-flex justify-content-between fw-bold opacity-75 w-100 mt-auto mb-2">
                          <span className="fs-6 text-gray-600">PERCENTAGE</span>
                          <span className="fw-bolder fs-6">
                            {qualification.perc}%
                          </span>
                        </div>
                        <div className="h-5px mx-3 w-100 bg-light mb-3">
                          <div
                            className={`rounded h-5px ${getProgressBarColor(
                              qualification.perc
                            )}`}
                            role="progressbar"
                            style={{ width: `${qualification.perc}%` }}
                          ></div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
                {/* end :: Qualification Card */}
              </div>
            </div>
          ))}
        </div>
      </div>
      {/* end: Card Body */}
    </div>
  );
};

export { QualificationDetailsWidget };
