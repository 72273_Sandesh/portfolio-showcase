// import React from 'react';

// const StepWizard: React.FC = () => {
//   return (
//     <section style={{
//       backgroundColor: '#21d4fd',
//       backgroundImage: 'linear-gradient(19deg, #21d4fd 0%, #b721ff 100%)',
//       height: '100vh',
//       width: '100%',
//       display: 'flex',
//       justifyContent: 'center',
//       alignItems: 'center',
//     }}>
//       <ul style={{
//         background: '#fff',
//         boxShadow: '0 15px 25px rgba(0,0,0,0.1)',
//         color: '#333',
//         listStyleType: 'none',
//         borderRadius: '10px',
//         display: 'flex',
//         padding: '20px 10px',
//         position: 'relative',
//         zIndex: 10,
//       }} className="step-wizard-list">
//         <li style={{
//           padding: '0 20px',
//           flexBasis: 0,
//           WebkitBoxFlex: 1,
//           msFlexPositive: 1,
//           flexGrow: 1,
//           maxWidth: '100%',
//           display: 'flex',
//           flexDirection: 'column',
//           textAlign: 'center',
//           minWidth: '50px',
//           position: 'relative',
//         }} className="step-wizard-item">
//           <span style={{
//             height: '40px',
//             width: '40px',
//             display: 'flex',
//             alignItems: 'center',
//             justifyContent: 'center',
//             borderRadius: '50%',
//             fontWeight: 600,
//             margin: '0 auto',
//             position: 'relative',
//             zIndex: 10,
//             color: 'transparent',
//           }} className="progress-count">1</span>
//           <span style={{
//             content: '""',
//             height: '40px',
//             width: '40px',
//             background: '#21d4fd',
//             position: 'absolute',
//             left: '50%',
//             top: '50%',
//             transform: 'translate(-50%, -50%)',
//             borderRadius: '50%',
//             zIndex: -10,
//           }}></span>
//           <span style={{
//             content: '""',
//             height: '10px',
//             width: '20px',
//             borderLeft: '3px solid #fff',
//             borderBottom: '3px solid #fff',
//             position: 'absolute',
//             left: '50%',
//             top: '50%',
//             transform: 'translate(-50%, -60%) rotate(-45deg)',
//             transformOrigin: 'center center',
//           }}></span>
//           <span style={{
//             fontSize: '14px',
//             fontWeight: 600,
//             marginTop: '10px',
//           }} className="progress-label">Billing Info</span>
//         </li>
//         {/* Repeat the above code for other steps */}
//       </ul>
//     </section>
//   );
// };

// export default StepWizard;




import React from 'react';

const StepWizard: React.FC = () => {
  return (
    <section style={{
      backgroundColor: '#21d4fd',
      backgroundImage: 'linear-gradient(19deg, #21d4fd 0%, #b721ff 100%)',
      height: '100px', // Adjusted height
      width: '400px', // Adjusted width
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      overflow: 'hidden', // Add overflow hidden to contain the content
      borderRadius: '10px', // Add border radius to match the container
    }}>
      <ul style={{
        background: '#fff',
        boxShadow: '0 15px 25px rgba(0,0,0,0.1)',
        color: '#333',
        listStyleType: 'none',
        borderRadius: '10px',
        display: 'flex',
        padding: '20px 10px',
        position: 'relative',
        zIndex: 10,
      }} className="step-wizard-list">
        <li style={{
          padding: '0 10px', // Adjusted padding
          flexBasis: 0,
          WebkitBoxFlex: 1,
          msFlexPositive: 1,
          flexGrow: 1,
          maxWidth: '100%',
          display: 'flex',
          flexDirection: 'column',
          textAlign: 'center',
          position: 'relative',
        }} className="step-wizard-item">
          <span style={{
            height: '40px',
            width: '40px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: '50%',
            fontWeight: 600,
            margin: '0 auto',
            position: 'relative',
            zIndex: 10,
            color: 'transparent',
          }} className="progress-count">1</span>
          <span style={{
            content: '""',
            height: '40px',
            width: '40px',
            background: '#21d4fd',
            position: 'absolute',
            left: '50%',
            top: '50%',
            transform: 'translate(-50%, -50%)',
            borderRadius: '50%',
            zIndex: -10,
          }}></span>
          <span style={{
            content: '""',
            height: '10px',
            width: '20px',
            borderLeft: '3px solid #fff',
            borderBottom: '3px solid #fff',
            position: 'absolute',
            left: '50%',
            top: '50%',
            transform: 'translate(-50%, -60%) rotate(-45deg)',
            transformOrigin: 'center center',
          }}></span>
          <span style={{
            fontSize: '14px',
            fontWeight: 600,
            marginTop: '10px',
          }} className="progress-label">Billing Info</span>
        </li>
        {/* Repeat the above code for other steps */}
      </ul>
    </section>
  );
};

export default StepWizard;
