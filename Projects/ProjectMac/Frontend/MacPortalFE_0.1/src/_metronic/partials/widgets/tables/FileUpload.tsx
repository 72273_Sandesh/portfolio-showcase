import { useState } from "react";

export function FileUpload() {
  const [file, setFile] = useState<File | null>(null);

  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const selectedFile = e.target.files[0];
      if (selectedFile.type === "application/pdf") {
        setFile(selectedFile);
      } else {
        alert("Please select a PDF file.");
      }
    }
  };

  const handleUpload = async () => {
    if (file) {
      console.log("Uploading file...");

      const formData = new FormData();
      formData.append("file", file);

      try {
        const result = await fetch("https://httpbin.org/post", {
          method: "POST",
          body: formData,
        });

        const data = await result.json();

        console.log(data);
      } catch (error) {
        console.error(error);
      }
    }
  };

  return (
    <div className='menu menu-sub menu-sub-dropdown w-250px w-md-300px' data-kt-menu='true'>
      <div className='px-7 py-5'>
        <div className='fs-5 text-gray-900 fw-bolder'>Upload New Document</div>
      </div>

      <div className='separator border-gray-200'></div>

      <div className='px-7 py-5'>
        <div className='mb-10'>
          <label className='form-label fw-bold'>Choose a PDF file from Your Device</label>

          <div className="input-group">
            <input
              className='form-control form-control-white form-control-sm'
              id="file"
              type="file"
              accept=".pdf"
              onChange={handleFileChange}
            />
          </div>
        </div>

        {file && (
          <section>
            File details:
            <ul>
              <li>Name: {file.name}</li>
              <li>Type: {file.type}</li>
              <li>Size: {file.size} bytes</li>
            </ul>
          </section>
        )}

        <div className='d-flex justify-content-end'>
          <button
            type='button'
            className='btn btn-sm btn-light btn-active-light-primary me-2'
            onClick={() => setFile(null)} 
            data-kt-menu-dismiss='true'
          >
            Cancel
          </button>

          <button
            onClick={handleUpload}
            className='btn btn-sm btn-primary'
            disabled={!file}
          >
            Upload a file
          </button>
        </div>
      </div>
    </div>
  );
}
