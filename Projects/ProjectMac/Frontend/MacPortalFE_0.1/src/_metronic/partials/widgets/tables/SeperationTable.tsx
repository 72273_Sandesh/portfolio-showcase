import { useEffect, useState } from "react";
import axios from "axios";
import { employeeIdForView } from "../../../../app/modules/GlobalQuery";

interface BasicDetails {
  emP_CODE: string;
  emP_NAME: string;
  doj: string;
  hR_RECD_DATE: string;
  resigN_DATE: string;
  relievE_DATE: string;
  seP_MODE: string;
  status: string;
  resigN_REASON: string;
  neT_AMT: string;
  remarks: string;
}

export function SeperationTable() {
  const [basicDetails, setBasicDetails] = useState<BasicDetails>({
    emP_CODE: "",
    emP_NAME: "",
    doj: "",
    hR_RECD_DATE: "",
    resigN_DATE: "",
    relievE_DATE: "",
    seP_MODE: "",
    status: "",
    resigN_REASON: "",
    neT_AMT: "",
    remarks: "",
  });
  const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/GetEmployeeSeperationDetails/${employeeIdForView}`)
      .then((response) => {
        if (response.data && response.data.length > 0) {
          setBasicDetails(response.data[0]);
        }
      })
      .catch((error) => {
        console.error("Error fetching Separation details:", error);
      });
  }, []);
  return (
    <div className={`card`}>
      <div className="card-header bg-light-primary cursor-pointer">
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Separation Details</h3>
        </div>
        {/* <Link
          to="/crafted/pages/profile/EditSeperationDetails"
          //src/app/modules/profile/components/EditBasicDetails.tsx
          className="btn btn-sm btn-primary me-3 align-self-center"
        >
          Edit
        </Link> */}
      </div>

      <div className="card-body p-9">
        {/* Begin:first Row     */}
        <div className="row">
          <div className="col-lg-6 mb-6">
            <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">EMP CODE : </label>
            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
              {basicDetails.emP_CODE}
            </span>
          </div>
          <div className="col-lg-6 mb-6">
            <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">EMP NAME : </label>
            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
              {basicDetails.emP_NAME}
            </span>
          </div>
        </div>
        {/* End:first Row     */}

        {/* Begin:first Row     */}
        <div className="row">
          <div className="col-lg-6 mb-6">
            <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
              JOINING DATE :{" "}
            </label>
            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
              {basicDetails.doj
                ? new Date(basicDetails.doj).toLocaleDateString("en-GB")
                : ""}
            </span>
          </div>
          <div className="col-lg-6 mb-6">
            <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
              HR RECORD DATE:{" "}
            </label>
            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
              {basicDetails.hR_RECD_DATE
                ? new Date(basicDetails.hR_RECD_DATE).toLocaleDateString(
                    "en-GB"
                  )
                : ""}
            </span>
          </div>
        </div>
        {/* End:first Row     */}

        {/* Begin:first Row     */}
        <div className="row">
          <div className="col-lg-6 mb-6">
            <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
              RESIGN DATE :{" "}
            </label>
            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
              {basicDetails.resigN_DATE
                ? new Date(basicDetails.resigN_DATE).toLocaleDateString("en-GB")
                : ""}
            </span>
          </div>
          <div className="col-lg-6 mb-6">
            <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
              RELIEVE DATE :{" "}
            </label>
            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
              {/* {new Date(basicDetails?.relievE_DATE || "No data").toLocaleDateString(
                          "en-GB"
                        )} */}
              {basicDetails.relievE_DATE
                ? new Date(basicDetails.relievE_DATE).toLocaleDateString("en-GB"): ""}
            </span>
          </div>
        </div>
        {/* End:first Row     */}

        {/* Begin:first Row     */}
        <div className="row">
          <div className="col-lg-6 mb-6">
            <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
              {" "}
              SEPERATION MODE :{" "}
            </label>
            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
              {basicDetails.seP_MODE}
            </span>
          </div>
          <div className="col-lg-6 mb-6">
            <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">STATUS : </label>
            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
              {basicDetails.status}
            </span>
          </div>
        </div>
        {/* End:first Row     */}
        {/* Begin:first Row     */}
        <div className="row">
          <div className="col-lg-6 mb-6">
            <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
              RESIGN REASON :{" "}
            </label>
            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
              {basicDetails.resigN_REASON}
            </span>
          </div>
          <div className="col-lg-6 mb-6">
            <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">NET AMOUNT : </label>
            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
              {basicDetails.neT_AMT}
            </span>
          </div>
        </div>
        {/* End:first Row     */}

        {/* Begin:first Row     */}
        <div>
          <div className="lg-6 mb-6">
            <label style={{ minWidth: "150px" }} className="col-lg-1 fw-bold text-muted">REMARK : </label>
            <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
              {basicDetails.remarks}
            </span>
          </div>
          <div className="col-lg-6 mb-6">
            {/* <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                  PASSPORT ISSUE DATE :{" "}
                </label>
                <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                  {basicDetails.birtH_DATE}
                </span> */}
          </div>
        </div>
        {/* End:first Row     */}
      </div>
    </div>
  );
}
