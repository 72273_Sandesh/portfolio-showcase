import { FC, useEffect, useState } from "react";
import axios from "axios";
import { employeeIdForView } from "../../../../app/modules/GlobalQuery";
import { Link } from "react-router-dom";

type Props = {
  className: string;
};

type FamilyDetail = {
  name: string;
  proficiency: number;
  p_TRANSACTION_TYPE: string;
  p_CTC: string;
  p_DATE_OF_TRANS_PROMO: string;
  p_DESIG: string;
  p_DIV_ID: string;
  p_DEPT_NAME: string;
  p_SECTION_NAME: string;
  p_GRADE_DESC: string;
  p_LEVEL_NAME: string;
};

const TransferHistoryTable: FC<Props> = ({ className }) => {
  const [familyDetails, setFamilyDetails] = useState<FamilyDetail[]>([]);
  const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

  useEffect(() => {
    // Fetch family details from API
    axios
      .get(
        `${EMPLOYEE_URL}/GetEmployeeTransferDetails?asEmpCode=${employeeIdForView}&empcode=x`
      )
      .then((response) => {
        setFamilyDetails(response.data);
      })
      .catch((error) => {
        console.error("Error fetching family details:", error);
      });
  }, []);

  return (
    <div className={`card ${className}`}>
      {/* begin::Header */}
      <div className="card-header bg-light-primary  border-0 pt-5">
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bold fs-3 mb-1">
            Employee Journey{" "}
          </span>
          <span className="text-muted mt-1 fw-semibold fs-7">
            {familyDetails.length} Records
          </span>
        </h3>
        <div className="card-toolbar">
          {/* begin::Menu */}
          <Link
            to="/crafted/pages/profile/AddTransferHistory"
            className="btn btn-sm btn-primary me-3 align-self-center"
          >
            Add
          </Link>
          <Link
            to="/crafted/pages/profile/AddTransferHistory"
            className="btn btn-sm btn-primary me-3 align-self-center"
          >
            Edit
          </Link>

          {/* end::Menu */}
        </div>
      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className="card-body py-3">
        <div className="table-responsive">
          <table className="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
            {/* begin::Table head */}
            <thead>
              <tr className="fw-bold text-muted">
                <th className="w-25px">Sr.NO.</th>
                <th className="min-w-150px">Description/CTC</th>
                <th className="min-w-120px">Effective Date</th>
                <th className="min-w-140px">Designation</th>
                <th className="min-w-120px">Location </th>
                <th className="min-w-120px text-center">Dept/Section </th>
                <th className="min-w-120px">Grade/Cadre</th>
                {/* <th className="min-w-120px">Pension</th> */}

                {/* <th className="min-w-100px text-end">Actions</th> */}
              </tr>
            </thead>
            {/* end::Table head */}
            <tbody>
              {familyDetails.map((familyDetail, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>
                    <div className="d-flex align-items-center">
                      <div className="d-flex justify-content-start flex-column">
                        <a
                      
                          className="text-gray-900 fw-bold text-hover-primary fs-6"
                        >
                          {familyDetail.p_TRANSACTION_TYPE}
                        </a>
                        <span className="fw-bolder d-flex align-items-right">
                          <div className="badge badge-light-success fw-bolder d-flex align-items-right">
                            {" "}
                            {familyDetail.p_CTC}
                          </div>
                          {/* <div className="badge badge-light-info fw-bolder d-flex align-items-right">
                            {familyDetail.relation}
                          </div> */}
                        </span>
                      </div>
                    </div>
                  </td>
                  {/* <td>
                    <span className='fw-bolder d-flex align-items-right'>
                      <div className='badge badge-light-success fw-bolder d-flex align-items-right'>{familyDetail.datE_BIRTH}</div>
                    </span>
                  </td> */}

                  <td>
                    <span className="fw-bolder d-flex align-items-right">
                      <div className="fw-bolder d-flex align-items-right">
                        {/* Convert date format */}
                        {new Date(
                          familyDetail.p_DATE_OF_TRANS_PROMO
                        ).toLocaleDateString("en-GB")}
                      </div>
                    </span>
                  </td>

                  <td>
                    <a
                  
                      className="text-gray-900 fw-bold text-hover-primary d-block fs-6"
                    >
                      {familyDetail.p_DESIG}
                    </a>
                  </td>

                  <td>
                    <a
                  
                      className="text-gray-900 fw-bold text-hover-primary d-block fs-6"
                    >
                      {familyDetail.p_DIV_ID}
                    </a>
                  </td>
                  <td className="text-center">
                    <div className="d-flex flex-column align-items-center">
                      <a
                    
                        className="text-gray-900 fw-bold text-hover-primary fs-6"
                      >
                        {familyDetail.p_DEPT_NAME}
                      </a>
                      {/* <div className="badge badge-light-success fw-bolder mt-2">
      {familyDetail.p_SECTION_NAME.trim() === "" ? "Not Assigned" : familyDetail.p_SECTION_NAME}
    </div> */}

                      <div className=" fw-bolder d-flex align-items-right">
                        <div className="d-flex align-items-right">
                          {familyDetail.p_SECTION_NAME.trim() === "" ? (
                            <div className="fw-bolder fs-8 d-flex align-items-right text-gray-500">
                              NOT ASSIGNED
                            </div>
                          ) : (
                            <div className="fw-bolder fs-8 d-flex align-items-right text-gray-500">
                              {familyDetail.p_SECTION_NAME}
                            </div>
                          )}
                        </div>
                      </div>
                      {/* You can add more badges or content here as needed */}
                    </div>
                  </td>

                  {/* Gratuity and Pension columns */}
                  <td className="text-end">
                    <div className="d-flex align-items-center">
                      <div className="d-flex justify-content-start flex-column">
                        <a
                         
                          className="text-gray-900 fw-bold text-hover-primary fs-6"
                        >
                          {familyDetail.p_GRADE_DESC}
                        </a>
                        <span className="fw-bolder d-flex align-items-right">
                          <div className=" fw-bolder fs-8 d-flex align-items-right text-gray-500">
                            {" "}
                            {familyDetail.p_LEVEL_NAME}
                          </div>
                          {/* <div className="badge badge-light-info fw-bolder d-flex align-items-right">
                            {familyDetail.relation}
                          </div> */}
                        </span>
                      </div>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>

      {/* end::Body */}
    </div>
  );
};

export { TransferHistoryTable };
