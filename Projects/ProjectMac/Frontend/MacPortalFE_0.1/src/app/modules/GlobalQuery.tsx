import { ID } from "../../_metronic/helpers";

let globalQuery: string = "";
let employeeIdForView: ID | null | string = null;
let filterDebouncedSearchTerm: string | null | undefined = "";
let filterLocation: string | null = "";
let filterDepartment: string | null = "";
let filterStatus: string | null = "";
let pageNo: number | null | string;
let isFiltered: boolean = false;
let fMIdForUpdate: number = 0;
let pfPercentage: number = 100;
let gratPercentage: number = 100;
let penPercentage: number = 100;

export const setGlobalQuery = (newValue: string) => {
  globalQuery = newValue;

  // console.log('newValue:', newValue);
  const params = new URLSearchParams(newValue);
  filterDebouncedSearchTerm = params.get("debouncedSearchTerm");
  filterLocation = params.get("location");
  filterDepartment = params.get("department");
  filterStatus = params.get("status");
  pageNo = params.get("page");
  // console.log('debouncedSearchTerm:', debouncedSearchTerm);
  // console.log('location:', location);
  // console.log('department:', department);
  // console.log('status:', status);
  // console.log('pageNo:', pageNo);
};

export const setEmployeeIdForView = (newValue: ID | null) => {
  employeeIdForView = newValue;
};
export const setIsFiltered = (newValue: boolean) => {
  isFiltered = newValue;
};

export const setFMIdForUpdate = (newValue: number) => {
  fMIdForUpdate = newValue;
};

export const setPfPercentage = (newValue: number) => {
  pfPercentage = newValue;
};
export const setGratPercentage = (newValue: number) => {
  gratPercentage = newValue;
};
export const setPenPercentage = (newValue: number) => {
  penPercentage = newValue;
};

export {
  globalQuery,
  employeeIdForView,
  filterDebouncedSearchTerm,
  filterLocation,
  filterDepartment,
  filterStatus,
  pageNo,
  isFiltered,
  fMIdForUpdate,
  pfPercentage,
  gratPercentage,
  penPercentage,
};