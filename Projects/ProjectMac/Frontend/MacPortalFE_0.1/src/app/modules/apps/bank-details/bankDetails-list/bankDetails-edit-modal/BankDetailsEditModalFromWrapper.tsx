import {useQuery} from 'react-query'
import {BankDetailsEditModalForm} from './BankDetailsEditModalForm'
import {isNotEmpty, QUERIES} from '../../../../../../_metronic/helpers'
import {useListView} from '../core/ListViewProvider'
import {getBankDetailsById} from '../core/_requests'

const BankDetailsEditModalFromWrapper = () => {
  const {itemIdForUpdate, setItemIdForUpdate} = useListView()
  const enabledQuery: boolean = isNotEmpty(itemIdForUpdate)
  const {
    isLoading,
    data: bankDetails,
    error,
  } = useQuery(
    `${QUERIES.BANK_DETAILS_LIST}-bankDetails-${itemIdForUpdate}`,
    () => {
      return getBankDetailsById(itemIdForUpdate)
    },
    {
      cacheTime: 0,
      enabled: enabledQuery,
      onError: (err) => {
        setItemIdForUpdate(undefined)
        console.error(err)
      },
    }
  )

  if (!itemIdForUpdate) {
    return <BankDetailsEditModalForm isBankDetailsLoading={isLoading} bankDetails={{id: undefined}} />
  }

  if (!isLoading && !error && bankDetails) {
    return <BankDetailsEditModalForm isBankDetailsLoading={isLoading} bankDetails={bankDetails} />
  }

  return null
}

export {BankDetailsEditModalFromWrapper}
