import { useEffect, useState } from "react";
import { KTIcon } from "../../../../../../_metronic/helpers";
import { globalVariable, setGlobalVariable } from "../../GlobalVariable";
import { useListView } from "../core/ListViewProvider";

const BankDetailsEditModalHeader = () => {
  const [headerName, setHeaderName] = useState("");

  useEffect(() => {
    const getHeaderName = () => {
      if (globalVariable === "addBankDetails") return "Add New Bank Details";
      else if (globalVariable === "editBankDetails") return "Edit Bank Details";
      else return "View Bank Details";
    };
    
    setHeaderName(getHeaderName);
  }, []);

  

  const { setItemIdForUpdate } = useListView();
  const openAddModal = () => {
    setItemIdForUpdate(undefined);
    setGlobalVariable("addBankDetails");
  };
  return (
    <div className="modal-header">
      {/* begin::Modal title */}
        <h2 className="fw-bolder">{headerName}</h2>
      
      {/* end::Modal title */}

      {/* begin::Close */}
      <div
        className="btn btn-icon btn-sm btn-active-icon-primary"
        data-kt-users-modal-action="close"
        onClick={openAddModal}
        style={{ cursor: "pointer" }}
      >
        <KTIcon iconName="cross" className="fs-1" />
      </div>
      {/* end::Close */}
    </div>
  );
};

export { BankDetailsEditModalHeader };
