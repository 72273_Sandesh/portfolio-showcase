
// import {FC, useEffect} from 'react'
// import {useMutation, useQueryClient} from 'react-query'
// import {MenuComponent} from '../../../../../../../_metronic/assets/ts/components'
// import {ID, KTIcon, QUERIES} from '../../../../../../../_metronic/helpers'
// import {useListView} from '../../core/ListViewProvider'
// import {useQueryResponse} from '../../core/QueryResponseProvider'
// import {deleteBankDetails} from '../../core/_requests'
// import { setGlobalVariable } from '../../../GlobalVariable'

// type Props = {
//   id: ID
// }

// const BankDetailsActionsCell: FC<Props> = ({id}) => {
//   const {setItemIdForUpdate} = useListView()
//   const {query} = useQueryResponse()
//   const queryClient = useQueryClient()

//   useEffect(() => {
//     MenuComponent.reinitialization()
//   }, [])

//   const openEditModal = () => {
//     setItemIdForUpdate(id)
//     setGlobalVariable("Edit Bank Details")
//   }
//   const openInfoModal = () => {
//     setItemIdForUpdate(id)
//     setGlobalVariable("View Bank Details")
//   } //logi for Flag for Info

//   const deleteItem = useMutation(() => deleteBankDetails(id), {
//     // 💡 response of the mutation is passed to onSuccess
//     onSuccess: () => {
//       // ✅ update detail view directly
//       queryClient.invalidateQueries([`${QUERIES.BANK_DETAILS_LIST}-${query}`])
//     },
//   })

//   return (
//     <>
//       <a
//         href='#'
//         className='btn btn-light btn-active-light-primary btn-sm justify-content: flex-center'
//         data-kt-menu-trigger='click'
//         data-kt-menu-placement='bottom-end'
//       >
//         Actions
//         <KTIcon iconName='down' className='fs-5 m-0  justify-content: flex-center'  />
//       </a>
//       {/* begin::Menu */}
//       <div
//         className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-20 w-125px py-4'
//         data-kt-menu='true'
//       >
//         {/* begin::Menu item */}
//         <div className='menu-item px-3'>
//           <a className='menu-link px-3' onClick={openInfoModal}>
//             View 
//           </a>
//         </div>
//         {/* end::Menu item */}
        
//         {/* begin::Menu item */}
//         <div className='menu-item px-3'>
//           <a className='menu-link px-3'>
//             Approve
//           </a>
//         </div>
//         {/* end::Menu item */}

//         {/* begin::Menu item */}
//         <div className='menu-item px-3'>
//           <a className='menu-link px-3'>
//             Reject
//           </a>
//         </div>
//         {/* end::Menu item */}

//         {/* begin::Menu item */}
//         {/* <div className='menu-item px-3'>
//           <a
//             className='menu-link px-3'
//             data-kt-users-table-filter='delete_row'
//             onClick={async () => await deleteItem.mutateAsync()}
//           >
//             Deactivate
//           </a>
//         </div> */}
//         {/* end::Menu item */}
//       </div>
//       {/* end::Menu */}
//     </>
//   )
// }

// export {BankDetailsActionsCell}




import {FC, useEffect} from 'react'
import {useMutation, useQueryClient} from 'react-query'
import {MenuComponent} from '../../../../../../../_metronic/assets/ts/components'
import {ID, KTIcon, QUERIES} from '../../../../../../../_metronic/helpers'
import {useListView} from '../../core/ListViewProvider'
import {useQueryResponse} from '../../core/QueryResponseProvider'
import {deleteBankDetails} from '../../core/_requests'
import { setGlobalVariable } from '../../../GlobalVariable'

type Props = {
  id: ID
}

const BankDetailsActionsCell: FC<Props> = ({id}) => {
  const {setItemIdForUpdate} = useListView()
  const {query} = useQueryResponse()
  const queryClient = useQueryClient()

  useEffect(() => {
    MenuComponent.reinitialization()
  }, [])

  const openEditModal = () => {
    setItemIdForUpdate(id)
    setGlobalVariable("editBankDetails")
  }
  const openInfoModal = () => {
    setItemIdForUpdate(id)
    setGlobalVariable("infoBankDetails")
  } 

  const deleteItem = useMutation(() => deleteBankDetails(id), {
    // 💡 response of the mutation is passed to onSuccess
    onSuccess: () => {
      // ✅ update detail view directly
      queryClient.invalidateQueries([`${QUERIES.BANK_DETAILS_LIST}-${query}`])
    },
  })

  return (
    <>
      <a
        href='#'
        className='btn btn-light btn-active-light-primary btn-sm justify-content: flex-center'
        data-kt-menu-trigger='click'
        data-kt-menu-placement='bottom-end'
      >
        Actions
        <KTIcon iconName='down' className='fs-5 m-0  justify-content: flex-center'  />
      </a>
      {/* begin::Menu */}
      <div
        className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-20 w-125px py-4'
        data-kt-menu='true'
      >
        {/* begin::Menu item */}
        <div className='menu-item px-3'>
          <a className='menu-link px-3' onClick={openInfoModal}>
            View 
          </a>
        </div>
        {/* end::Menu item */}

        {/* begin::Menu item */}
        <div className='menu-item px-3'>
          <a className='menu-link px-3' onClick={openEditModal}>
            Edit 
          </a>
        </div>
        {/* end::Menu item */}
        
        {/* begin::Menu item */}
        <div className='menu-item px-3'>
          <a className='menu-link px-3'>
            Done
          </a>
        </div>
        {/* end::Menu item */}

        
        {/* begin::Menu item */}
        {/* <div className='menu-item px-3'>
          <a
            className='menu-link px-3'
            data-kt-users-table-filter='delete_row'
            onClick={async () => await deleteItem.mutateAsync()}
          >
            Deactivate
          </a>
        </div> */}
        {/* end::Menu item */}
      </div>
      {/* end::Menu */}
    </>
  )
}

export {BankDetailsActionsCell}
