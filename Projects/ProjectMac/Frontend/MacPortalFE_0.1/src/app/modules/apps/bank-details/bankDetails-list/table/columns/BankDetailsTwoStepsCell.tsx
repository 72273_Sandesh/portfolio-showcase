import {FC} from 'react'

type Props = {
  deptHead: string | undefined
}

const BankDetailsTwoStepsCell: FC<Props> = ({deptHead}) => (
  <>
   {/* <div className='badge badge-light-success fw-bolder d-flex align-items-center'>{deptHead ?(deptHead):('Not Assigned')}</div>*/}
   <div className=' fw-bolder d-flex align-items-right'>
      {deptHead ?(deptHead.toUpperCase()): <div className='badge badge-danger fw-bolder d-flex align-items-right'>NOT ASSIGNED</div>}
    </div>
    </> 
)

export {BankDetailsTwoStepsCell}
