import {ID, Response} from '../../../../../../_metronic/helpers'
export type Cadre = {
      id?: ID
  
      cadrE_CODE?:string,
      descr?: string,
      sH_DESCR?: string,
      lvE_TBLNO?: string,
      chG_DATE?: Date,
      chG_USER?:string,
      chG_TERM?: string,
      udF_NUM1?:number | string | null,
      cadrE_ID?:string,
      cadrE_ORDER?:number,
      banD_CODE?: string,
      mkit?: number,
      mmed?: number,
      chG_REASON?:string,
      cadreCategory?:string;
      status?:string;
      responseStatus?: string;
      message?: string;
      
}

export type CadreQueryResponse = Response<Array<Cadre>>

export const initialCadre: Cadre = {
  
}
