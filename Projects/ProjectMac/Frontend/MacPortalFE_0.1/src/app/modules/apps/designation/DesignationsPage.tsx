import {Route, Routes, Outlet, Navigate} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {DesignationsListWrapper} from './designations-list/DesignationsList'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
const designationsBreadcrumbs: Array<PageLink> = [
  {
    title: 'Master',
    path: '/apps/designation/designations',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const DesignationsPage = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='designations'
          element={
            <>
              <PageTitle breadcrumbs={designationsBreadcrumbs}>Designation list</PageTitle>
              <DesignationsListWrapper />
              <ToastContainer/>
            </>
          }
        />
      </Route>
      <Route index element={<Navigate to='/apps/designation/designations' />} />
    </Routes>
  )
}

export default DesignationsPage
