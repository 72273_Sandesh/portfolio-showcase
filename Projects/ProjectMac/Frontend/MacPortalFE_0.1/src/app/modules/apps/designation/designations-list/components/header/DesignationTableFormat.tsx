import React, { FC } from "react";
import { useQueryResponseData } from "../../core/QueryResponseProvider";

const DesignationTableFormat = () => {
  const designations = useQueryResponseData(); 

  return (
    <table id="table-to-export">
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Long Designation</th>
          <th>Status</th>
          <th>Grade Code</th>
          <th>Grade Name</th>
          <th>Cadre Code</th>
          <th>Cadre Name</th>
          <th>Grade Id</th>
          <th>Change User</th>
          <th>Change Term</th>
          <th>Change Reason</th>
          <th>Change Date</th>
          <th>Udf Num1</th>
          <th>Udf Str1</th>
          <th>Sap Design Code</th>
        </tr>
      </thead>
      <tbody>
        {designations.map((designation) => (
          <tr key={designation.id}>
            <td>{designation.id}</td>
            <td>{designation.name}</td>
            <td>{designation.longDesignation}</td>
            <td>{designation.status}</td>
            <td>{designation.gradeCode}</td>
            <td>{designation.gradeName}</td>
            <td>{designation.cadreCode}</td>
            <td>{designation.cadreName}</td>
            <td>{designation.gradeId}</td>
            <td>{designation.changeUser}</td>
            <td>{designation.changeTerm}</td>
            <td>{designation.changeReason}</td>
            <td>{designation.changeDate?.toString()}</td>
            <td>{designation.udfNum1}</td>
            <td>{designation.udfStr1}</td>
            <td>{designation.sapDesignCode}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default DesignationTableFormat;
