import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { isNotEmpty } from "../../../../../../_metronic/helpers";
import { initialDesignation, Designation } from "../core/_models";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { DesignationsListLoading } from "../components/loading/DesignationsListLoading";
import { createDesignation, updateDesignation } from "../core/_requests";
import { useQueryResponse } from "../core/QueryResponseProvider";
import { globalVariable, setGlobalVariable } from "../../GlobalVariable";
import { toast } from "react-toastify";
import axios from "axios";

type Props = {
  isDesignationLoading: boolean;
  designation: Designation;
};

interface grade {
  id: number;
  description: string;
}

const API_URL = import.meta.env.VITE_DESIGN_DROPDOWNS_API_URL;

const editDesignationSchema = Yup.object().shape({
  name: Yup.string().required("Description is required"),
  longDesignation: Yup.string().required("Short Description is required"),
  changeReason: Yup.string().min(3, "Minimum 3 characters"),

  gradeCode: Yup.string().required("Grade is required"),
  status: Yup.string().required("Status is required"),
});

const DesignationEditModalForm: FC<Props> = ({
  designation,
  isDesignationLoading,
}) => {
  const { setItemIdForUpdate } = useListView();
  const { refetch } = useQueryResponse();

  const [statuses] = useState(["ACTIVE", "INACTIVE"]);
  const [status, setStatus] = useState("");

  const [grades, setGrades] = useState<grade[]>([]);
  const [grade, setGrade] = useState("");

  const [newReason, setNewReason] = useState("");

  // const [ipAddress, setIpAddress] = useState<string>('');
  const { currentUser } = useAuth();

  useEffect(() => {
    fetchFromBackend()
      .then((combinedData) => {
        setGrades(combinedData.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const fetchFromBackend = async () => {
    try {
      const response = await axios.get(API_URL);
      const gradeData = await response.data;
      return gradeData;
    } catch (error) {
      throw new Error("Failed to fetch grade from backend");
    }
  };

  const [designationForEdit] = useState<Designation>({
    ...designation,
    name: designation.name || initialDesignation.name,
    longDesignation:
      designation.longDesignation || initialDesignation.longDesignation,
    gradeCode: designation.gradeCode || initialDesignation.gradeCode,
    status: designation.status || initialDesignation.status || status,
    changeDate: designation.changeDate || initialDesignation.changeDate,
    changeUser: designation.changeUser || initialDesignation.changeUser,
    changeTerm: designation.changeTerm || initialDesignation.changeTerm,
  });
  const fetchIpAddress = async () => {};

  fetchIpAddress();
  function formatDate(dateTime: string) {
    const date = new Date(dateTime);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata", // Specify the Indian time zone
    });
    return `${formattedDate} ${formattedTime}`;
  }
  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };
  const formik = useFormik({
    initialValues: designationForEdit,
    validationSchema: editDesignationSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);
      // try {
      if (isNotEmpty(values.id) && globalVariable === "editDesign") {
        values.changeUser = currentUser?.first_name;
        values.changeTerm = "TERM";
        values.changeDate = new Date();
        const response = await updateDesignation(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("addDesign");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      } else {
        values.id = "null";
        values.changeUser = currentUser?.first_name;
        values.changeTerm = "TERM";
        values.changeDate = new Date();

        const response = await createDesignation(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("addDesign");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      }
    },
  });

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column" id="kt_modal_add_user_scroll">
          {globalVariable !== "addDesign" && (
            <div className="row mb-3">
              <div className="fv-row">
                <label className=" fw-bold fs-6 mb-2">Designation Code :</label>
                <input
                  className="form-control form-control-solid mb-3 mb-lg-0"
                  value={formik.values.id ? formik.values.id : ""}
                  disabled={
                    globalVariable === "infoDesign" ||
                    globalVariable === "editDesign"
                  }
                />
              </div>
            </div>
          )}
          <div className="row mb-3">
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Designation Name
                </label>
                <input
                  placeholder="Description"
                  {...formik.getFieldProps("name")}
                  onChange={(e) =>
                    formik.setFieldValue("name", e.target.value.toUpperCase())
                  }
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    { "is-invalid": formik.touched.name && formik.errors.name },
                    { "is-valid": formik.touched.name && !formik.errors.name }
                  )}
                  type="name"
                  name="name"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDesignationLoading ||
                    globalVariable === "infoDesign"
                  }
                />
                {formik.touched.name && formik.errors.name && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.name}</span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Short Description
                </label>
                <input
                  placeholder="Short Description"
                  {...formik.getFieldProps("longDesignation")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "longDesignation",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.longDesignation &&
                        formik.errors.longDesignation,
                    },
                    {
                      "is-valid":
                        formik.touched.longDesignation &&
                        !formik.errors.longDesignation,
                    }
                  )}
                  type="text"
                  name="longDesignation"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDesignationLoading ||
                    globalVariable === "infoDesign"
                  }
                />
                {formik.touched.longDesignation &&
                  formik.errors.longDesignation && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.longDesignation}</span>
                    </div>
                  )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fs-6 fw-bold">Grade</label>
                <select
                  // className='form-select form-select-solid fw-bolder'
                  className={clsx(
                    "form-select form-select-solid fw-bolder",
                    {
                      "is-invalid":
                        formik.touched.gradeCode && formik.errors.gradeCode,
                    },
                    {
                      "is-valid":
                        formik.touched.gradeCode && !formik.errors.gradeCode,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("gradeCode", e.target.value);
                    setGrade(e.target.value);
                  }}
                  value={grade}
                  disabled={globalVariable === "infoDesign"}
                >
                  <option value="">{formik.values.gradeName}</option>
                  {grades.map((grade) => (
                    <option key={grade.id} value={grade.id}>
                      {grade.description.toUpperCase()}
                    </option>
                  ))}
                </select>
                {formik.touched.gradeCode && formik.errors.gradeCode && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.gradeCode}</span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fs-6 fw-bold">Status</label>
                <select
                  // className='form-select form-select-solid fw-bolder'
                  className={clsx(
                    "form-select form-select-solid fw-bolder",
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                    setStatus(e.target.value);
                  }}
                  value={status}
                  disabled={globalVariable === "infoDesign"}
                >
                  {globalVariable === "addDesign" ? (
                    <option value="">{formik.values.status}</option>
                  ) : (
                    <option value="">
                      {formik.values.status === "A"
                        ? "ACTIVE"
                        : formik.values.status === "N"
                        ? "INACTIVE"
                        : ""}
                    </option>
                  )}
                  {statuses.map((status) => (
                    <option key={status} value={status}>
                      {status}
                    </option>
                  ))}
                </select>
                {formik.touched.status && formik.errors.status && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.status}</span>
                  </div>
                )}
              </div>
            </div>

            {globalVariable === "infoDesign" && (
              <>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className="fw-bold fs-6 mb-2">Change User</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      value={formik.values.changeUser?.toUpperCase()}
                      disabled={globalVariable === "infoDesign"}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className="fw-bold fs-6 mb-2">Change Term</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      value={formik.values.changeTerm}
                      //value={ipAddress}
                      disabled={globalVariable === "infoDesign"}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className=" fw-bold fs-6 mb-2">Change Date</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      // value={formik.values.changeDate}
                      // value={formik.values.changeDate ? formatDate(formik.values.changeDate) : ''}
                      // value={
                      //   formik.values.changeDate
                      //     ? formik.values.changeDate.toString()
                      //     : ""
                      // }
                      value={
                        formik.values.changeDate
                          ? formatDate(formik.values?.changeDate.toString())
                          : ""
                      }
                      disabled={globalVariable === "infoDesign"}
                    />
                  </div>
                </div>
              </>
            )}

            {/* <div className='col-md-6'>
              <div className='fv-row'>
                <label className='form-label fs-6 fw-bold'>Salary Account:</label>
                <select
                  className='form-select form-select-solid fw-bolder'
                  onChange={(e) => { formik.setFieldValue('acctCode', e.target.value); setSalaryAcct(e.target.value) }}
                  value={salaryAcct}
                  disabled={globalVariable === 'infoDesign'}
                >
                  <option value=''>{formik.values.acctCode}</option>
                  {salaryAccts.map(salaryAcct => (
                    <option key={salaryAcct} value={salaryAcct}>{salaryAcct}</option>
                  ))}
                </select>
              </div>
            </div>
            <div className='col-md-6'>
              <div className='fv-row'>
                <label className='form-label fs-6 fw-bold'>Account Code AP:</label>
                <select
                  className='form-select form-select-solid fw-bolder'
                  onChange={(e) => { formik.setFieldValue('acctCodeAp', e.target.value); setAcctCodeAP(e.target.value) }}
                  value={acctCodeAP}
                  disabled={globalVariable === 'infoDesign'}
                >
                  <option value=''>{formik.values.acctCodeAp}</option>
                  {acctCodeAPs.map(acctCodeAP => (
                    <option key={acctCodeAP} value={acctCodeAP}>{acctCodeAP}</option>
                  ))}
                </select>
              </div>
            </div>
            <div className='col-md-6'>
              <div className='fv-row'>
                <label className='form-label fs-6 fw-bold'>Pay Table:</label>
                <select
                  className='form-select form-select-solid fw-bolder'
                  onChange={(e) => { formik.setFieldValue('payTable', e.target.value); setPayTable(e.target.value) }}
                  value={payTable}
                  disabled={globalVariable === 'infoDesign'}
                >
                  <option value=''>{formik.values.payTable}</option>
                  {payTables.map(payTable => (
                    <option key={payTable} value={payTable}>{payTable}</option>
                  ))}
                </select>
              </div>
            </div>
            <div className='col-md-6'>
              <div className='fv-row'>
                <label className='form-label fs-6 fw-bold'>Cost Centre:</label>
                <select
                  className='form-select form-select-solid fw-bolder'
                  onChange={(e) => { formik.setFieldValue('cctrCode', e.target.value); setCostCentre(e.target.value) }}
                  value={costCentre}
                  disabled={globalVariable === 'infoDesign'}
                >
                  <option value=''>{formik.values.cctrCode}</option>
                  {costCentres.map(costCentre => (
                    <option key={costCentre} value={costCentre}>{costCentre}</option>
                  ))}
                </select>
              </div>
            </div>
            <div className='col-md-6'>
              <div className='fv-row'>
                <label className='form-label fs-6 fw-bold'>CCTR CODE AP:</label>
                <select
                  className='form-select form-select-solid fw-bolder'
                  onChange={(e) => { formik.setFieldValue('cctrCodeAp', e.target.value); setCCTRCodeAP(e.target.value) }}
                  value={cctrCodeAP}
                  disabled={globalVariable === 'infoDesign'}
                >
                  <option value=''>{formik.values.cctrCodeAp}</option>
                  {cctrCodeAPs.map(cctrCodeAP => (
                    <option key={cctrCodeAP} value={cctrCodeAP}>{cctrCodeAP}</option>
                  ))}
                </select>
              </div>
            </div>*/}
          </div>
          {globalVariable !== "addDesign" && (
            <div className="row mb-3">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Change Reason
                </label>
                <textarea
                  placeholder="Change Reason"
                  //value={{...formik.getFieldProps('changeReason')}}
                  {...(globalVariable === "infoDesign"
                    ? { ...formik.getFieldProps("changeReason") }
                    : { newReason })}
                  onChange={(e) => {
                    formik.setFieldValue(
                      "changeReason",
                      e.target.value.toUpperCase()
                    );
                    setNewReason(e.target.value.toUpperCase());
                  }}
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.changeReason &&
                        formik.errors.changeReason,
                    },
                    {
                      "is-valid":
                        formik.touched.changeReason &&
                        !formik.errors.changeReason,
                    }
                  )}
                  // type='text'
                  name="changeReason"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDesignationLoading ||
                    globalVariable === "infoDesign"
                  }
                />
                {formik.touched.changeReason && formik.errors.changeReason && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.changeReason}</span>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
        <div className="text-center pt-15">
          {/* <button
            type='reset'
            onClick={() => cancel()}
            className='btn btn-light me-3'
            disabled={formik.isSubmitting || isDesignationLoading}
          >
            Cancel
          </button> */}
          {globalVariable !== "infoDesign" && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isDesignationLoading ||
                formik.isSubmitting ||
                !formik.isValid ||
                !formik.touched ||
                (globalVariable !== "addDesign" && newReason === "")
              }
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isDesignationLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isDesignationLoading) && (
        <DesignationsListLoading />
      )}
    </>
  );
};

export { DesignationEditModalForm };
