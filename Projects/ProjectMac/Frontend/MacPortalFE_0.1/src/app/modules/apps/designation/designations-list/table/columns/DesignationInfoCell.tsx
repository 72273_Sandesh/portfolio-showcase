import {FC} from 'react'
import {Designation} from '../../core/_models'

type Props = {
  designation: Designation
}

const DesignationInfoCell: FC<Props> = ({designation}) => (
  <div className='d-flex align-items-center'>
    
    <div className='d-flex flex-column'>
      <a  className='text-gray-800 text-hover-primary mb-1'>
        {designation.name?.toUpperCase()}
      </a>
      <span >
        <div className=' fw-bolder d-flex align-items-right'>
          <div className='badge badge-light-success fw-bolder d-flex align-items-right'>{designation.id}</div>
        </div>
    </span>
    </div>
  </div>
)

export {DesignationInfoCell}
