
import clsx from 'clsx'
import {FC} from 'react'
import {toAbsoluteUrl} from '../../../../../../../_metronic/helpers'
import {EmployeeSearch} from '../../core/_models'

type Props = {
    employeeSearch: EmployeeSearch
}

const EmployeeSearchInfoCell: FC<Props> = ({employeeSearch}) => (
  <div className='d-flex align-items-center'>
    {/* begin:: Avatar */}
    <div className='symbol symbol-circle symbol-50px overflow-hidden me-3'>
      <a href='#'>
        {employeeSearch.avatar ? (
          <div className='symbol-label'>
            <img src={toAbsoluteUrl(`media/${employeeSearch.avatar}`)} alt={employeeSearch.name} className='w-100' />
          </div>
        ) : (
          <div
            className={clsx(
              'symbol-label fs-3',
              `bg-light-${employeeSearch.initials?.state}`,
              `text-${employeeSearch.initials?.state}`
            )}
          >
            {employeeSearch.initials?.label}
          </div>
        )}
      </a>
    </div>
    <div className='d-flex flex-column'>
      <a href='#' className='text-gray-800 text-hover-primary mb-1'>
        {employeeSearch.name}
      </a>
      <span>{employeeSearch.email}</span>
    </div>
  </div>
)

export {EmployeeSearchInfoCell}
