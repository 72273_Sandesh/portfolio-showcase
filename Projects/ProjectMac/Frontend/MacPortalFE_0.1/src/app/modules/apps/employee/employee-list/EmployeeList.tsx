import {ListViewProvider, useListView} from './core/ListViewProvider'
import {QueryRequestProvider} from './core/QueryRequestProvider'
import {QueryResponseProvider} from './core/QueryResponseProvider'
import {EmployeeListHeader} from './components/header/EmployeeListHeader'
import {EmployeeTable} from './table/EmployeeTable'
import {EmployeeEditModal} from './employee-edit-modal/EmployeeEditModal'
import {KTCard} from '../../../../../_metronic/helpers'
import { ToolbarWrapper } from '../../../../../_metronic/layout/components/toolbar'
import { Content } from '../../../../../_metronic/layout/components/content'
import { Toolbar } from '../Toolbar'

const EmployeeList = () => {
  const {itemIdForUpdate} = useListView()
  return (
    <>
      <KTCard>
        <EmployeeListHeader />
        <EmployeeTable />
      </KTCard>
      {itemIdForUpdate !== undefined && <EmployeeEditModal />}
    </>
  )
}

const EmployeeListWrapper = () => (
  <QueryRequestProvider>
    <QueryResponseProvider>
      <ListViewProvider>
        <ToolbarWrapper />
        <Content>
        <Toolbar/>
          <EmployeeList />
        </Content>
      </ListViewProvider>
    </QueryResponseProvider>
  </QueryRequestProvider>
)

export {EmployeeListWrapper}
