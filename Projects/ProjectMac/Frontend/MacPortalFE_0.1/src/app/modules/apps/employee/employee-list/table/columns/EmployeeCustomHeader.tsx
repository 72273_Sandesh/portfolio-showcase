import clsx from 'clsx'
import {FC, PropsWithChildren, useMemo} from 'react'
import {HeaderProps} from 'react-table'
import {initialQueryState} from '../../../../../../../_metronic/helpers'
import {useQueryRequest} from '../../core/QueryRequestProvider'
import {Employee} from '../../core/_models'

type Props = {
  className?: string
  title?: string

  tableProps: PropsWithChildren<HeaderProps<Employee>>
}
const EmployeeCustomHeader: FC<Props> = ({className, title, tableProps}) => {
  const id = tableProps.column.id
  const {state, updateState} = useQueryRequest()

  const isSelectedForSorting = useMemo(() => {
    return state.sort && state.sort === id
  }, [state, id])
  const order: 'asc' | 'desc' | undefined = useMemo(() => state.order, [state])

  const sortColumn = () => {
    // avoid sorting for these columns
    if (id === 'actions' || id === 'selection') {
      return
    }

    if (!isSelectedForSorting) {
      // enable sort asc
      updateState({sort: id, order: 'asc', ...initialQueryState})
      return
    }

    if (isSelectedForSorting && order !== undefined) {
      if (order === 'asc') {
        // enable sort desc
        updateState({sort: id, order: 'desc', ...initialQueryState})
        return
      }

      // disable sort
      updateState({sort: undefined, order: undefined, ...initialQueryState})
    }
  }

  return (
    <th
      {...tableProps.column.getHeaderProps()}
      className={clsx(
        className,
        isSelectedForSorting && order !== undefined && `table-sort-${order}`
      )}
      style={{cursor: 'pointer'}}
      onClick={sortColumn}
    >
      {title}
    </th>
  )
}

export {EmployeeCustomHeader}
// import clsx from 'clsx'
// import { FC, PropsWithChildren, useMemo } from 'react'
// import { HeaderProps } from 'react-table'
// import { initialQueryState } from '../../../../../../../_metronic/helpers'
// import { useQueryRequest } from '../../core/QueryRequestProvider'
// import { Cadre } from '../../core/_models'
//  import clsx from 'clsx'

// type Props = {
//   className?: string
//   title?: string
//   chG_USER?:string
//   tableProps: PropsWithChildren<HeaderProps<Cadre>>
// }

// const CadreCustomHeader: FC<Props> = ({ className,title, tableProps }) => {
//   const id = tableProps.column.id
//   const { state, updateState } = useQueryRequest()

//   const isSelectedForSorting = useMemo(() => {
//     return state.sort && state.sort === id
//   }, [state, id])
//   const order: 'asc' | 'desc' | undefined = useMemo(() => state.order, [state])

//   const sortColumn = () => {
//     if (id === 'actions' || id === 'selection') {
//       return
//     }

//     if (!isSelectedForSorting) {
//       updateState({ sort: id, order: 'asc', ...initialQueryState })
//       return
//     }

//     if (isSelectedForSorting && order !== undefined) {
//       if (order === 'asc') {
//         updateState({ sort: id, order: 'desc', ...initialQueryState })
//         return
//       }
//       updateState({ sort: undefined, order: undefined, ...initialQueryState })
//     }
//   }

//   // Convert title to uppercase
//   const uppercaseTitle = title ? title.toUpperCase() : '';

//   // Convert chG_USER prop to uppercase
//   const uppercaseChgUser = cadre.chG_USER.toUpperCase();

//   return (
//     <th
//       {...tableProps.column.getHeaderProps()}
//       className={clsx(
//         className,
//         isSelectedForSorting && order !== undefined && `table-sort-${order}`
//       )}
//       style={{ cursor: 'pointer' }}
//       onClick={sortColumn}
//     >
//       {uppercaseTitle}
//       <span>{uppercaseChgUser}</span> {/* Displaying the uppercase chG_USER prop */}
//     </th>
//   )
// }

// export { CadreCustomHeader }



