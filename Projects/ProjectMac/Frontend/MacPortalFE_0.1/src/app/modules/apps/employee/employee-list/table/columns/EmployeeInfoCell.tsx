import {FC} from 'react'
import {Employee} from '../../core/_models'
import { Link } from 'react-router-dom'
import { newQuery } from '../../core/QueryResponseProvider'
import { ID } from '../../../../../../../_metronic/helpers'

type Props = {
    employee: Employee
    id:ID
}

const EmployeeInfoCell: FC<Props> = ({employee,id}) => {
  console.log("Current emp Id : ", id)
  const currentUserId = { userid: id };
  const queryData = { query: newQuery };
  return(
  
  <div className='d-flex align-items-center '>
    {/* begin:: Avatar */}
    {/* <div className='symbol symbol-circle symbol-50px overflow-hidden me-3'>
      <a href='#'>
        {department.avatar ? (
          <div className='symbol-label'>
            <img src={toAbsoluteUrl(`media/${department.avatar}`)} alt={department.name} className='w-100' />
          </div>
        ) : (
          <div
            className={clsx(
              'symbol-label fs-3',
              `bg-light-${department.initials?.state}`,
              `text-${department.initials?.state}`
            )}
          >
            {department.initials?.label}
          </div>
        )}
      </a>
    </div> */}
    <div className='d-flex flex-column'>
      <Link
            to="/crafted/pages/profile/overview"
            state={{ userId: currentUserId, queryData: queryData }} className='text-gray-800 text-hover-primary mb-1'>
        {employee.emP_FNAME?.toUpperCase()}
        {' '}
        {employee.emP_LNAME?.toUpperCase()}
      </Link>
      {/* <span className='badge badge-light-success fw-bolder d-flex align-items-right' >{cadre.id}</span> */}
      <span >
        <div className=' fw-bolder d-flex align-items-right'>
          <div className='badge badge-light-success fw-bolder d-flex align-items-right'>{employee.id}</div>
        </div>
    </span>

    </div>
  </div>
)}

export {EmployeeInfoCell}


