


import React from 'react';

type Props = {
    status?: string;
};

const EmployeeStatusCell: React.FC<Props> = ({ status }) => {
  return (
    <div className=' fw-bolder d-flex align-items-right'>
     <span >
        <div className=' fw-bolder d-flex align-items-right'>
          <div className='d-flex align-items-right'>{<div className='d-flex align-items-right'>
  {status === 'A' ? (
    <div className='badge badge-light-success fw-bolder d-flex align-items-right'>ACTIVE</div>
  ) : status === 'N' ? (
    <div className='badge badge-light-danger fw-bolder d-flex align-items-right'>INACTIVE</div>
  ) : (
    <div className='badge d-flex align-items-right'></div>
  )}
</div>
}</div>
        </div>
      </span>
    </div>


  );
};

export { EmployeeStatusCell };
