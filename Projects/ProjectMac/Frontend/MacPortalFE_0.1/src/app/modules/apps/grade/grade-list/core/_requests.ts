import axios, { AxiosResponse } from "axios";
import { ID, Response } from "../../../../../../_metronic/helpers";
import { Grade, GradeQueryResponse } from "./_models";

const GRADE_URL = import.meta.env.VITE_GRADE_API_URL;
//https://localhost:44360/api/Grade/GetAllGrades/query?page=1&items_per_page=10

const getGrade = (query: string): Promise<GradeQueryResponse> => {
  return axios
    .get(`${GRADE_URL}/GetAllGrades/query?${query}`)
    .then((d: AxiosResponse<GradeQueryResponse>) => d.data);
};

// https://localhost:44360/api/Cadre/createCadre
const getGradeById = (id:ID): Promise<Grade | undefined> => {
  return axios
    .get(`${GRADE_URL}/GetGradeByGradeId/${id}`)
    .then((response: AxiosResponse<Response<Grade>>) => response.data)
    .then((response: Response<Grade>) => response.data);
};
//https://localhost:44360/api/Cadre/GetCadreByCadreCode/999

const createGrade = (grade: Grade): Promise<Grade | undefined> => {
  return axios
    .post(`${GRADE_URL}/createGrade`, grade)
    .then((response: AxiosResponse<Response<Grade>>) => response.data)
    .then((response: Response<Grade>) => response.data);
};
//https://localhost:44360/api/Cadre/createCadre
const updateGrade = (grade: Grade): Promise<Grade | undefined> => {
  return axios
    .put(`${GRADE_URL}/UpdateGrade/${grade.id}`, grade)
    .then((response: AxiosResponse<Response<Grade>>) => response.data)
    .then((response: Response<Grade>) => response.data);
};

const deleteGrade = (gradeId: ID): Promise<void> => {
  return axios.delete(`${GRADE_URL}/${gradeId}`).then(() => {});
};

const deleteSelectedGrade = (departmentIds: Array<ID>): Promise<void> => {
  const requests = departmentIds.map((id) => axios.delete(`${GRADE_URL}/${id}`));
  return axios.all(requests).then(() => {});
};

export {
  getGrade,
  deleteGrade,
  deleteSelectedGrade,
  getGradeById,
  createGrade,
  updateGrade,
};
