import {FC} from 'react'

type Props = {
  shortDescription: string | undefined
}

const GradeShortDescription: FC<Props> = ({shortDescription}) => (
  <> <div className=''>{shortDescription ?(shortDescription.toUpperCase()):('Not Assigned')}</div></>
  )

export {GradeShortDescription}
