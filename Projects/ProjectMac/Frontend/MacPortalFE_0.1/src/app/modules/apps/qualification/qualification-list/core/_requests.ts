import axios, { AxiosResponse } from "axios";
import { ID, Response } from "../../../../../../_metronic/helpers";
import { Qualification, QualificationQueryResponse } from "./_models";

const API_URL = import.meta.env.VITE_APP_THEME_API_URL_QUAL;
const QUALIFICATION_URL = `${API_URL}`;
//https://localhost:44360/api/Qualification/GetAllQualifications/query?page=1&items_per_page=10

const getQualification = (query: string): Promise<QualificationQueryResponse> => {
  return axios
    // .get(`${QUALIFICATION_URL}/GetAllCadres/query?${query}`)
    .get(`${QUALIFICATION_URL}/GetAllQualifications/query?${query}`)
    .then((d: AxiosResponse<QualificationQueryResponse>) => d.data);
};

// https://localhost:44360/api/Qualification/GetQualificationByCode/0016%20
const getQualificationById = (id:ID): Promise<Qualification | undefined> => {
  return axios
    .get(`${QUALIFICATION_URL}/GetQualificationByCode/${id}`)
    .then((response: AxiosResponse<Response<Qualification>>) => response.data)
    .then((response: Response<Qualification>) => response.data);
};
//https://localhost:44360/api/Cadre/GetCadreByCadreCode/999

const createQualification = (cadre: Qualification): Promise<Qualification | undefined> => {
  return axios
    .post(`${QUALIFICATION_URL}/CreateQualification`, cadre)
    .then((response: AxiosResponse<Response<Qualification>>) => response.data)
    .then((response: Response<Qualification>) => response.data);
};
//https://localhost:44360/api/Qualification/CreateQualification
const updateQualification = (cadre: Qualification): Promise<Qualification | undefined> => {
  return axios
    .put(`${QUALIFICATION_URL}/UpdateQualification/${cadre.id}`, cadre)
    .then((response: AxiosResponse<Response<Qualification>>) => response.data)
    .then((response: Response<Qualification>) => response.data);

};
//https://localhost:44360/api/Qualification/UpdateQualification/0010

const deleteQualification = (departmentId: ID): Promise<void> => {
  return axios.delete(`${QUALIFICATION_URL}/${departmentId}`).then(() => {});
};

const deleteSelectedQualification = (departmentIds: Array<ID>): Promise<void> => {
  const requests = departmentIds.map((id) => axios.delete(`${QUALIFICATION_URL}/${id}`));
  return axios.all(requests).then(() => {});
};

export {
  getQualification,
  deleteQualification,
  deleteSelectedQualification,
  getQualificationById,
  createQualification,
  updateQualification,
};
