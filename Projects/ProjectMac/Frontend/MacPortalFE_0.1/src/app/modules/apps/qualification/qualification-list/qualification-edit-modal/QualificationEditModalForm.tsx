import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { isNotEmpty } from "../../../../../../_metronic/helpers";
import { initialQualification, Qualification } from "../core/_models";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { QualificationListLoading } from "../components/loading/QualificationListLoading";
import { createQualification, updateQualification } from "../core/_requests";
import { useQueryResponse } from "../core/QueryResponseProvider";
import { globalVariable, setGlobalVariable } from "../../GlobalVariable";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
const API_URL = import.meta.env.VITE_QUAL_DROPDOWNS_API_URL;

type Props = {
  isQualificationLoading: boolean;
  qualification: Qualification;
};

const editQualificationSchema = Yup.object().shape({
  shortdescr: Yup.string().required("Short Description is required"),
  name: Yup.string().required("Description is required"),
  changeReason: Yup.string(),
  qualSalStrucDesc: Yup.string().required("Salary description is required"),
  status: Yup.string().required("Status is required"),
  type: Yup.string().required("Qualification type is required"),
});
type states = {
  traN_ID: string;
  descr: string;
};

const QualificationEditModalForm: FC<Props> = ({
  qualification,
  isQualificationLoading,
}) => {
  const { setItemIdForUpdate } = useListView();
  const { refetch } = useQueryResponse();

  const [qualSalStrucDescs, setQualSalStrucDescs] = useState<states[]>([]);
  const [qualSalStrucDesc, setQualSalStrucDesc] = useState("");

  const [statuses] = useState(["ACTIVE", "INACTIVE"]);
  const [status, setStatus] = useState("");

  const [types] = useState([
    "DIPLOMA",
    "UNDER GRADUATE",
    "GRADUATE",
    "POST GRADUATE",
  ]);
  const [type, setType] = useState("");

  const [changeReason, setChangeReason] = useState<string>("");

  const { currentUser } = useAuth();
  useEffect(() => {
    fetchFromBackend()
      .then((combinedData) => {
        setQualSalStrucDescs(combinedData.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const fetchFromBackend = async () => {
    try {
      const response = await axios.get(API_URL);
      const data = await response.data;
      return data;
    } catch (error) {
      throw new Error("Failed to fetch grade from backend");
    }
  };

  const [qualificationForEdit] = useState<Qualification>({
    ...qualification,
    name: qualification.name || initialQualification.name,
    shortdescr: qualification.shortdescr || initialQualification.shortdescr,
    qualSalStrucDesc:
      qualification.qualSalStrucDesc || initialQualification.qualSalStrucDesc,
    status: qualification.status || initialQualification.status,
    type: qualification.type || initialQualification.type,
    // chG_DATE: qualification.chG_DATE || initialQualification.chG_DATE,
    // chG_USER: qualification.chG_USER || initialQualification.chG_USER,
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };

  const formik = useFormik({
    initialValues: qualificationForEdit,
    validationSchema: editQualificationSchema,

    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      if (isNotEmpty(values.id) && globalVariable === "editQualification") {
        values.changeUser = currentUser?.first_name;
        values.changeTerm = "TERM";

        values.changeDate = new Date();
        const response = await updateQualification(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("addQualification");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      } else {
        values.changeUser = currentUser?.first_name;
        values.changeTerm = "TERM";
        values.id = "null";
        values.changeDate = new Date();
        const response = await createQualification(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("addQualification");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      }
    },
  });

  function formatDate(dateString: string) {
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata", // Specify the Indian time zone
    });
    return `${formattedDate} ${formattedTime}`;
  }

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column" id="kt_modal_add_user_scroll">
          <div className="row mb-3">
            {globalVariable !== "addQualification" && (
              <div className="">
                <div className="fv-row">
                  <label className=" fw-bold fs-6 mb-2">
                    Qualification Code :
                  </label>
                  <input
                    className="form-control form-control-solid mb-3 mb-lg-0"
                    value={formik.values.id ? formik.values.id : ""}
                  />
                </div>
              </div>
            )}

            {/* //here "col-md-6" is for locating two input options aside to each others  */}
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Name of Qualification
                </label>
                <input
                  placeholder="name"
                  value={formik.values.name?.toUpperCase()}
                  // {...formik.getFieldProps('name')}
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    { "is-invalid": formik.touched.name && formik.errors.name },
                    { "is-valid": formik.touched.name && !formik.errors.name }
                  )}
                  type="text"
                  name="name"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isQualificationLoading ||
                    globalVariable === "infoQualification"
                  }
                  onChange={(e) => {
                    formik.setFieldValue("name", e.target.value.toUpperCase());
                  }}
                />
                {formik.touched.name && formik.errors.name && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.name}</span>
                  </div>
                )}
              </div>
            </div>

            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  {" "}
                  Short Description
                </label>
                <input
                  placeholder="Short Description"
                  value={formik.values.shortdescr?.toUpperCase()}
                  // {...formik.getFieldProps('shortdescr')}
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.shortdescr && formik.errors.shortdescr,
                    },
                    {
                      "is-valid":
                        formik.touched.shortdescr && !formik.errors.shortdescr,
                    }
                  )}
                  type="text"
                  name="shortdescr"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isQualificationLoading ||
                    globalVariable === "infoCadre"
                  }
                  onChange={(e) => {
                    formik.setFieldValue(
                      "shortdescr",
                      e.target.value.toUpperCase()
                    );
                  }}
                />
                {formik.touched.shortdescr && formik.errors.shortdescr && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.shortdescr}</span>
                  </div>
                )}
              </div>
            </div>

            {/* {begin : Category} */}
            <div className="col-md-6">
              <div className="fv-row">
                <label className="fs-6 fw-bold">Salary Structure:</label>
                <select
                  // className="form-select form-select-solid fw-bolder"
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.qualSalStrucDesc &&
                        formik.errors.qualSalStrucDesc,
                    },
                    {
                      "is-valid":
                        formik.touched.qualSalStrucDesc &&
                        !formik.errors.qualSalStrucDesc,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("qualSalStrucDesc", e.target.value);
                    setQualSalStrucDesc(e.target.value);
                  }}
                  value={qualSalStrucDesc}
                  disabled={globalVariable === "infoQualification"}
                >
                  <option value="cadreCategory">
                    {formik.values.qualSalStrucDesc}
                  </option>
                  {qualSalStrucDescs.map((qualSalStrucDesc) => (
                    <option
                      key={qualSalStrucDesc.traN_ID}
                      value={qualSalStrucDesc.traN_ID}
                    >
                      {qualSalStrucDesc.descr}
                    </option>
                  ))}
                </select>
                {formik.touched.qualSalStrucDesc &&
                  formik.errors.qualSalStrucDesc && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.qualSalStrucDesc}</span>
                    </div>
                  )}
              </div>
            </div>

            <div className="col-md-6">
              <div className="fv-row">
                <label className="fs-6 fw-bold">Status:</label>
                <select
                  // className="form-select form-select-solid fw-bolder"
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                    setStatus(e.target.value);
                  }}
                  value={status}
                  disabled={globalVariable === "infoQualification"}
                >
                  {globalVariable === "addQualification" ? (
                    <option value="">{formik.values.status}</option>
                  ) : (
                    <option value="">
                      {formik.values.status === "A"
                        ? "ACTIVE"
                        : formik.values.status === "N"
                        ? "INACTIVE"
                        : ""}
                    </option>
                  )}
                  {statuses.map((status) => (
                    <option key={status} value={status}>
                      {status}
                    </option>
                  ))}
                </select>
                {formik.touched.status && formik.errors.status && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.status}</span>
                  </div>
                )}
              </div>
            </div>

            <div className="col-md-6">
              <div className="fv-row">
                <label className="fs-6 fw-bold">Type:</label>
                <select
                  // className="form-select form-select-solid fw-bolder"
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid": formik.touched.type && formik.errors.type,
                    },
                    {
                      "is-valid": formik.touched.type && !formik.errors.type,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("type", e.target.value);
                    setType(e.target.value);
                  }}
                  value={type}
                  disabled={globalVariable === "infoQualification"}
                >
                  {globalVariable === "addQualification" ? (
                    <option value="">{formik.values.type}</option>
                  ) : (
                    <option value="">{formik.values.type}</option>
                  )}
                  {types.map((type) => (
                    <option key={type} value={type}>
                      {type}
                    </option>
                  ))}
                </select>
                {formik.touched.type && formik.errors.type && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.type}</span>
                  </div>
                )}
              </div>
            </div>
            {/* {end : Category} */}

            {globalVariable === "infoQualification" && (
              <div className="col-md-6">
                <div className="fv-row">
                  <label className="fw-bold fs-6 mb-2"> Change Term</label>
                  <input
                    placeholder=""
                    {...formik.getFieldProps("changeTerm")}
                    className={clsx(
                      "form-control form-control-solid mb-3 mb-lg-0",
                      {
                        "is-invalid":
                          formik.touched.changeTerm && formik.errors.changeTerm,
                      },
                      {
                        "is-valid":
                          formik.touched.changeTerm &&
                          !formik.errors.changeTerm,
                      }
                    )}
                    type="text"
                    name="chG_TERM"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isQualificationLoading ||
                      globalVariable === "infoQualification"
                    }
                    //|| globalVariable === 'infoCadre'
                  />
                  {formik.touched.changeTerm && formik.errors.changeTerm && (
                    <div className="fv-plugins-message-container">
                      <span role="alert">{formik.errors.changeTerm}</span>
                    </div>
                  )}
                </div>
              </div>
            )}

            {globalVariable === "infoQualification" && (
              <div className="col-md-6">
                <div className="fv-row">
                  <label className="fw-bold fs-6 mb-2"> Change User</label>
                  <input
                    placeholder=""
                    value={formik.values.changeUser?.toUpperCase()}
                    // {...formik.getFieldProps('changeUser')}
                    className={clsx(
                      "form-control form-control-solid mb-3 mb-lg-0",
                      {
                        "is-invalid":
                          formik.touched.changeUser && formik.errors.changeUser,
                      },
                      {
                        "is-valid":
                          formik.touched.changeUser &&
                          !formik.errors.changeUser,
                      }
                    )}
                    type="text"
                    name="chG_USER"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isQualificationLoading ||
                      globalVariable === "infoQualification"
                    }
                    //|| globalVariable === 'infoCadre'
                  />
                  {formik.touched.changeUser && formik.errors.changeUser && (
                    <div className="fv-plugins-message-container">
                      <span role="alert">{formik.errors.changeUser}</span>
                    </div>
                  )}
                </div>
              </div>
            )}

            {globalVariable === "infoQualification" && (
              <div className="col-md-6">
                <div className="fv-row">
                  <label className="fw-bold fs-6 mb-2"> Change Date</label>
                  <input
                    placeholder=""
                    {...formik.getFieldProps("changeDate")}
                    className={clsx(
                      "form-control form-control-solid mb-3 mb-lg-0",
                      {
                        "is-invalid":
                          formik.touched.changeDate && formik.errors.changeDate,
                      },
                      {
                        "is-valid":
                          formik.touched.changeDate &&
                          !formik.errors.changeDate,
                      }
                    )}
                    type="text"
                    name="changeDate"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isQualificationLoading ||
                      globalVariable === "infoQualification"
                    }
                    // value={formik.values.changeDate? formatDate(formik.values.changeDate : "" }
                    value={
                      formik.values.changeDate
                        ? formatDate(formik.values?.changeDate.toString())
                        : ""
                    }

                    //|| globalVariable === 'infoCadre'
                  />
                  {formik.touched.changeDate && formik.errors.changeDate && (
                    <div className="fv-plugins-message-container">
                      <span role="alert">{formik.errors.changeDate}</span>
                    </div>
                  )}
                </div>
              </div>
            )}

            {globalVariable !== "addQualification" && (
              <div className="row-mb-3">
                <div className="fv-row">
                  <label className="required fw-bold fs-6 mb-2">
                    Reason For Edit
                  </label>
                  <textarea
                    className="required form-control"
                    // value={changeReason}
                    {...(globalVariable === "infoQualification"
                      ? { ...formik.getFieldProps("changeReason") }
                      : { changeReason })}
                    //onChange={(e) => setReason(e.target.value)}
                    onChange={(e) => {
                      formik.setFieldValue(
                        "changeReason",
                        e.target.value.toUpperCase()
                      );
                      setChangeReason(e.target.value.toUpperCase());
                    }}
                    rows={3}
                    {...(globalVariable === "infoQualification"
                      ? { placeholder: "" }
                      : { placeholder: "This is Mandatory Field" })}
                    //  placeholder="This is Mandatory Field"
                    disabled={globalVariable === "infoQualification"}
                  ></textarea>
                  {formik.touched.changeReason &&
                    formik.errors.changeReason && (
                      <div className="fv-plugins-message-container">
                        <span role="alert">{formik.errors.changeReason}</span>
                      </div>
                    )}
                </div>
              </div>
            )}
          </div>
        </div>
        <div className="text-center pt-15">
          {/* <button
            type='reset'
            onClick={() => cancel()}
            className='btn btn-light me-3'
            disabled={formik.isSubmitting || isCadreLoading}
          >
            Cancel
          </button> */}
          {globalVariable !== "infoQualification" && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isQualificationLoading ||
                formik.isSubmitting ||
                !formik.isValid ||
                !formik.touched ||
                (globalVariable !== "addQualification" && changeReason === "")
              }
              // onClick={handleFormSubmit} // Call handleFormSubmit on button click)
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isQualificationLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isQualificationLoading) && (
        <QualificationListLoading />
      )}
    </>
  );
};

export { QualificationEditModalForm };
