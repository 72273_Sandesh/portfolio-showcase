import { Navigate, Routes, Route, Outlet, useLocation } from "react-router-dom";
import { PageLink, PageTitle } from "../../../_metronic/layout/core";
import { LanguageDetails } from "./components/LanguageDetails";
import { ProfileHeader } from "./ProfileHeader";
import { Attachments } from "./components/Attachments";
import { Assets } from "./components/Assets";
import { Family } from "./components/Family";
import React from "react";
import { QualificationDetails } from "./components/QualificationDetails";
import { ExperienceDetails } from "./components/ExperianceDetails";
import { Separation } from "./components/SeparationDetails";
import { Overview } from "./components/Overview";
import { KYCWrapper } from "./components/KYCWrapper";
import { TransferHistoryWrapper } from "./components/TransferHistoryWrapper";
import { RecruitmentWrapper } from "./components/RecruitmentWrapper";
import { EditBasicDetails } from "./components/EditBasicDetails";
import { EditPersonalDetails } from "./components/EditPersonalDetails";
import { AddLanguageDetails } from "./components/AddLanguageDetails";
import { DeptSalaryComparison } from "./components/DeptSalaryComparison";
import { WorkDetailsWrapper } from "./components/WorkDetailsWrapper";
import { BasicDetailsWrapper } from "./components/BasicDetailsWrapper";
import { ContactDetailsWrapper } from "./components/ContactDetailsWrapper";
import { EditJoiningDetails } from "./components/EditJoiningDetails";
import { EditPresentDetails } from "./components/EditPresentDetails";
import { RecruitmentDocsWrapper } from "./components/RecruitmentDocsWrapper";
import { PreJoiningDocsWrapper } from "./components/PreJoiningDocsWrapper";
import { PostJoiningDocsWrapper } from "./components/PostJoiningDocsWrapper";
import { PostResignDocsWrapper } from "./components/PostResignDocsWrapper";
import { EISDocsWrapper } from "./components/EISDocsWrapper";
import { EditQualificationDetailsWrapper } from "./components/EditQualificationDetailsWrapper";
import { EditExperienceDetailsWrapper } from "./components/EditExperienceDetailsWrapper";
import { setEmployeeIdForView, setGlobalQuery } from "../GlobalQuery";
import { EditPermanentDetails } from "./components/EditPermanentDetails";
import { EditEmergencyDetails } from "./components/EditEmergencyDetails";
import { EditKYCDetails } from "./components/EditKYCDetails";
import { AddAssetList } from "./components/AddAssetList";
import { EditLanguagesWrapper } from "./components/EditLanguagesWrapper";
import { AddQualificationDetailsWrapper } from "./components/AddQualificationDetailsWrapper";
import { AddExperienceDetailsWrapper } from "./components/AddExperienceDetailsWrapper";
import { EditStatutoryDetails } from "./components/EditStatutoryDetails";
import { EditWorkPayrollDetails } from "./components/EditWorkPayrollDetails";
import { EditCandidateDetails } from "./components/EditCandidateDetails";
import { AddFamilyMember } from "./components/AddFamilyMember";
import { AddTransferHistory } from "./components/AddTransferHistory";
import { EditAssetList } from "./components/EditAssetList";
import { EditOfferDetails } from "./components/EditOfferDetails";
import { AddInterviewDetails } from "./components/AddInterviewDetails";
import { EditFamilyDetailsWrapper } from "./components/EditFamilyDetailsWrapper";
import { EditInterviewDetails } from "./components/EditInterviewDetails";
import { EditSeperationDetails } from "./components/EditSeperationDetails";
import { ToastContainer } from "react-toastify";
import { EditPensionNominationsWrapper } from "./components/EditPensionNominationsWrapper";
import { EditPFNominationsWrapper } from "./components/EditPFNominationsWrapper";
import { EditGratuityNominationsWrapper } from "./components/EditGratuityNominationsWrapper";

const profileBreadCrumbs: Array<PageLink> = [
  // {
  //   title: "Profile",
  //   path: "/crafted/pages/profile/overview",
  //   isSeparator: false,
  //   isActive: false,
  // },
  // {
  //   title: "",
  //   path: "",
  //   isSeparator: true,
  //   isActive: false,
  // },
];
// const ProfilePage: React.FC = () => {
//   const location = useLocation();
//   const data = location.state as { userId: string; queryData: string };

//   if (data) {
//     const userId = data.userId?.userid;
//     const query = data.queryData?.query;

type DataType = {
  userId?: { userid: string } | null;
  queryData?: { query: string } | null;
};

const ProfilePage: React.FC = () => {
  const location = useLocation();
  const data = location.state as DataType;

  if (data && data.userId && data.queryData) {
    const userId = data.userId.userid;
    const query = data.queryData.query;

    setGlobalQuery(query);
    setEmployeeIdForView(userId);

    console.log("User ID:", userId);
    console.log("Query:", query);
  } else {
    console.error("Data is null or undefined");
  }

  return (
    <>
      <ToastContainer />
      <Routes>
        <Route
          element={
            <>
              <div>
                <ProfileHeader />
              </div>
              <Outlet />
            </>
          }
        >
          <Route
            path="overview"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Employee Details
                </PageTitle>

                <Overview />
              </>
            }
          />
          <Route
            path="family"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Family Details
                </PageTitle>
                <Family />
              </>
            }
          />
          <Route
            path="language"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Language Details
                </PageTitle>
                <LanguageDetails />
              </>
            }
          />
          <Route
            path="documents"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Qualification Details
                </PageTitle>
                <QualificationDetails />
              </>
            }
          />
          <Route
            path="attachments"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Attachments
                </PageTitle>
                <Attachments />
              </>
            }
          />
          <Route
            path="kyc"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  KYC Details
                </PageTitle>
                <KYCWrapper />
              </>
            }
          />
          <Route
            path="basicDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Personal Details
                </PageTitle>
                <BasicDetailsWrapper />
              </>
            }
          />
          <Route
            path="contactDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Contact Details
                </PageTitle>
                <ContactDetailsWrapper />
              </>
            }
          />
          <Route
            path="work"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Employment Details
                </PageTitle>
                <WorkDetailsWrapper />
              </>
            }
          />
          <Route
            path="transferhistory"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Employee Journey
                </PageTitle>
                <TransferHistoryWrapper />
              </>
            }
          />
          <Route
            path="recruitment"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Recruitment
                </PageTitle>
                <RecruitmentWrapper />
              </>
            }
          />
          <Route
            path="assets"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>Assets</PageTitle>
                <Assets />
              </>
            }
          />
          <Route
            path="Separation"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Separation
                </PageTitle>
                <Separation />
              </>
            }
          />
          <Route
            path="EditBasicDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Basic Details
                </PageTitle>
                <EditBasicDetails />
              </>
            }
          />
          <Route
            path="AddAssetList"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Add New Assets
                </PageTitle>
                <AddAssetList />
              </>
            }
          />
          <Route
            path="EditPresentDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Basic Details
                </PageTitle>
                <EditPresentDetails />
              </>
            }
          />
          <Route
            path="EditJoiningDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Joining Details
                </PageTitle>
                <EditJoiningDetails />
              </>
            }
          />
          <Route
            path="EditPersonalDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Personal Details
                </PageTitle>
                <EditPersonalDetails />
              </>
            }
          />
          <Route
            path="EditPermamentDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Permanent Details
                </PageTitle>
                <EditPermanentDetails />
              </>
            }
          />
          <Route
            path="EditEmergencyDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Emergency Details
                </PageTitle>
                <EditEmergencyDetails />
              </>
            }
          />
          <Route
            path="EditKYCDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit KYC Details
                </PageTitle>
                <EditKYCDetails />
              </>
            }
          />
          <Route
            path="EditWorkPayrollDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Work & Payroll Details
                </PageTitle>
                <EditWorkPayrollDetails />
              </>
            }
          />
          <Route
            path="EditStatutoryDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Statutory Details
                </PageTitle>
                <EditStatutoryDetails />
              </>
            }
          />
          <Route
            path="EditCandidateDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Candidate Details
                </PageTitle>
                <EditCandidateDetails />
              </>
            }
          />
          <Route
            path="AddInterviewDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Add Interview Details
                </PageTitle>
                <AddInterviewDetails />
              </>
            }
          />
          <Route
            path="EditInterviewDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Interview Details
                </PageTitle>
                <EditInterviewDetails />
              </>
            }
          />
          <Route
            path="EditOfferDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Offer Details
                </PageTitle>
                <EditOfferDetails />
              </>
            }
          />
          <Route
            path="EditSeperationDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Seperation Details
                </PageTitle>
                <EditSeperationDetails />
              </>
            }
          />
          <Route
            path="FamilyMemberAdd"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Add Family Member
                </PageTitle>
                <AddFamilyMember />
              </>
            }
          />
          <Route
            path="AddTransferHistory"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Add Journey Details
                </PageTitle>
                <AddTransferHistory />
              </>
            }
          />
          <Route
            path="EditLanguagesWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Language Details
                </PageTitle>
                <EditLanguagesWrapper />
              </>
            }
          />
          <Route
            path="EditAssetList"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Asset List
                </PageTitle>
                <EditAssetList />
              </>
            }
          />
          <Route
            path="AddLangaugeDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Add Language Details
                </PageTitle>
                <AddLanguageDetails />
              </>
            }
          />
          salComparison
          <Route
            path="salComparison"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Salary Comparison
                </PageTitle>
                <DeptSalaryComparison />
              </>
            }
          />
          <Route
            path="connections"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Experience Details
                </PageTitle>
                <ExperienceDetails />
              </>
            }
          />
          <Route
            path="RecruitmentDocsWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Attachments
                </PageTitle>
                <RecruitmentDocsWrapper />
              </>
            }
          />
          <Route
            path="PreJoiningDocsWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Attachments
                </PageTitle>
                <PreJoiningDocsWrapper />
              </>
            }
          />
          <Route
            path="PostJoiningDocsWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Attachments
                </PageTitle>
                <PostJoiningDocsWrapper />
              </>
            }
          />
          <Route
            path="PostResignDocsWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Attachments
                </PageTitle>
                <PostResignDocsWrapper />
              </>
            }
          />
          <Route
            path="EISDocsWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Attachments
                </PageTitle>
                <EISDocsWrapper />
              </>
            }
          />
          <Route
            path="QualificationDetailsModalWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Qualification Details
                </PageTitle>
                <EditQualificationDetailsWrapper />
              </>
            }
          />
          <Route
            path="AddQualificationDetailsWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Add Qualification Details
                </PageTitle>
                <AddQualificationDetailsWrapper />
              </>
            }
          />
          <Route
            path="EditExperienceDetailsWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Experience Details
                </PageTitle>
                <EditExperienceDetailsWrapper />
              </>
            }
          />
          <Route
            path="AddExperienceDetailsWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Add Experience Details
                </PageTitle>
                <AddExperienceDetailsWrapper />
              </>
            }
          />
          <Route
            path="EditFamilyDetails"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Family Details
                </PageTitle>
                <EditFamilyDetailsWrapper />
              </>
            }
          />
          <Route
            path="EditPensionNominationsWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Family Nomination Details
                </PageTitle>
                <EditPensionNominationsWrapper />
              </>
            }
          />
          <Route
            path="EditPFNominationsWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Family Nomination Details
                </PageTitle>
                <EditPFNominationsWrapper />
              </>
            }
          />
          <Route
            path="EditGratuityNominationsWrapper"
            element={
              <>
                <PageTitle breadcrumbs={profileBreadCrumbs}>
                  Edit Family Nomination Details
                </PageTitle>
                <EditGratuityNominationsWrapper />
              </>
            }
          />
          <Route
            index
            element={<Navigate to="/crafted/pages/profile/overview" />}
          />
        </Route>
      </Routes>
    </>
  );
};

export default ProfilePage;
