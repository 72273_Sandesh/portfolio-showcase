import { useEffect, useState } from "react";
import { globalVariable } from "../../apps/employee/GlobalVariable";
import { KTIcon } from "../../../../_metronic/helpers";

const BankDetailsEditModalHeader = ({close}) => {
  const [headerName, setHeaderName] = useState("");
  useEffect(() => {
    const getHeaderName = () => {
      if (globalVariable === "addBankDetails") return "Add New Bank Details";
      else if (globalVariable === "editBankDetails") return "Edit Bank Details";
      else return "View Bank Details";
    };
    
    setHeaderName(getHeaderName);
  }, []);

  return (
    <div className="modal-header">
      {/* begin::Modal title */}
        <h2 className="fw-bolder">{headerName}</h2>
      
      {/* end::Modal title */}

      {/* begin::Close */}
      <div
        className="btn btn-icon btn-sm btn-active-icon-primary"
        data-kt-users-modal-action="close"
        onClick={close}
        style={{ cursor: "pointer" }}
      >
        <KTIcon iconName="cross" className="fs-1" />
      </div>
      {/* end::Close */}
    </div>
  );
};

export { BankDetailsEditModalHeader };
