import { Content } from "../../../../_metronic/layout/components/content";
import AddQualificationDetails from "./AddQualificationDetails";

export function AddQualificationDetailsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <AddQualificationDetails/>
      </div>
    </Content>
  );
}
