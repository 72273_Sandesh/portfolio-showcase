import { Content } from "../../../../_metronic/layout/components/content";
import { SalaryComparisonQualification } from "../../../../_metronic/partials/widgets/tables/SalaryComparisonQualification";
import { SalaryComparisonRecruitmentTable } from "../../../../_metronic/partials/widgets/tables/SalaryComparisonRecruitmentTable";
import { SalaryComparisonTable } from "../../../../_metronic/partials/widgets/tables/SalaryComparisonTable";

export function DeptSalaryComparison() {
  return (
    <Content>
      <div className="mb-5 mb-xl-2">
        <div  className="mb-5 mb-xl-2">   <SalaryComparisonTable  className=""/> </div>
        <div  className="mb-5 mb-xl-2">  <SalaryComparisonQualification className=""/> </div>
        <div  className="mb-5 mb-xl-2">  <SalaryComparisonRecruitmentTable className="" /> </div>

      </div>
    </Content>
  );
}
