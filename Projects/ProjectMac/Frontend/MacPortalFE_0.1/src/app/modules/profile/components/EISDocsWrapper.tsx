
import { Content } from "../../../../_metronic/layout/components/content";
import { EISDocs } from "../../../../_metronic/partials/widgets/tables/EISDocs";

export function EISDocsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
      <EISDocs className='mb-5 mb-xl-8' />
      </div>
    </Content>
  );
}
