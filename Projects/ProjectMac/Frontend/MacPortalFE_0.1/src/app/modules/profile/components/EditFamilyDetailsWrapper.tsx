import { Content } from "../../../../_metronic/layout/components/content";
import EditFamilyDetails from "./EditFamilyDetails";

export function EditFamilyDetailsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <EditFamilyDetails/>
      </div>
    </Content>
  );
}
