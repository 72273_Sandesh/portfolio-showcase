import { Content } from "../../../../_metronic/layout/components/content";
import {  } from "./LanguageDetailAdd";
import SeperationDetailsEdit from "./SeperationDetailsEdit";

export function EditSeperationDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <SeperationDetailsEdit />
      </div>
    </Content>
  );
}
