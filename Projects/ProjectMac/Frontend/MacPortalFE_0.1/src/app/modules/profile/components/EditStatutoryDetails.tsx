import { Content } from "../../../../_metronic/layout/components/content";
import {  } from "./LanguageDetailAdd";
import StatutoryDetailsEdit from "./StatutoryDetailsEdit";

export function EditStatutoryDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <StatutoryDetailsEdit />
      </div>
    </Content>
  );
}
