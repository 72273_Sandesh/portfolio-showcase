
import { FC } from 'react'
// import {KTIcon, toAbsoluteUrl} from '../../../../_metronic/helpers'
import {Link, useLocation} from 'react-router-dom'
// import {Dropdown1} from '../../../../_metronic/partials'
// import { ToolbarWrapper } from '../../../../_metronic/layout/components/toolbar'
import { Content } from '../../../../_metronic/layout/components/content'

const EmployeeMenu: FC = () => {
  const location = useLocation()

  return (
    <>
      {/* <ToolbarWrapper /> */}
      <Content>
        <div className='card mb-5 mb-xl-10'>
          <div className='card-body pt-9 pb-0'>
            

            <div className='d-flex overflow-auto h-55px'>
              <ul className='nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap'>
                <li className='nav-item'>
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname === '/crafted/pages/profile/overview' && 'active')
                    }
                    to='/crafted/pages/profile/overview'
                  >
                    Employee
                  </Link>
                </li>
                <li className='nav-item'>
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname === '/crafted/pages/profile/projects' && 'active')
                    }
                    to='/crafted/pages/profile/projects'
                  >
                    Family Details
                  </Link>
                </li>
                <li className='nav-item'>
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname === '/crafted/pages/profile/campaigns' && 'active')
                    }
                    to='/crafted/pages/profile/campaigns'
                  >
                    Language Details
                  </Link>
                </li>
                <li className='nav-item'>
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname === '/crafted/pages/profile/documents' && 'active')
                    }
                    to='/crafted/pages/profile/documents'
                  >
                    Qualification Details
                  </Link>
                </li>
                <li className='nav-item'>
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname === '/crafted/pages/profile/connections' && 'active')
                    }
                    to='/crafted/pages/profile/connections'
                  >
                    Experience Details
                  </Link>
                </li>
                <li className='nav-item'>
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname === '/crafted/pages/profile/attachments' && 'active')
                    }
                    to='/crafted/pages/profile/attachments'
                  >
                    Attachments
                  </Link>
                </li>
                <li className='nav-item'>
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname === '/crafted/pages/profile/assets' && 'active')
                    }
                    to='/crafted/pages/profile/assets'
                  >
                    Assets
                  </Link>
                </li>
                <li className='nav-item'>
                  <Link
                    className={
                      `nav-link text-active-primary me-6 ` +
                      (location.pathname === '/crafted/pages/profile/Separation' && 'active')
                    }
                    to='/crafted/pages/profile/Separation'
                  >
                    Separation
                  </Link>
                </li>
                
              </ul>
            </div>
          </div>
        </div>
      </Content>
    </>
  )
}

export {EmployeeMenu}
