
import { Content } from "../../../../_metronic/layout/components/content";
import { PostResignDocs } from "../../../../_metronic/partials/widgets/tables/PostResignDocs";

export function PostResignDocsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
      <PostResignDocs className='mb-5 mb-xl-8' />
      </div>
    </Content>
  );
}
