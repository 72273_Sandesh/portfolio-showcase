import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

type states = {
  statE_CODE: string;
  descr: string;
};
const PresentDetailsEdit: React.FC = () => {
  const [employee, setEmployee] = useState<any>({});
  const [states, setStates] = useState<states[]>([]);
  const navigate = useNavigate();
  const [pdf, setPdf] = useState<File | null>(null);
  const { currentUser } = useAuth();

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setEmployee(response.data[0]);
        // Update formik's initial values after fetching employee data
        formik.setValues({
          cuR_ADD1: response.data[0].cuR_ADD1 || "",
          cuR_ADD2: response.data[0].cuR_ADD2 || "",
          cuR_ADD3: response.data[0].cuR_ADD3 || "",

          cuR_TEL1: response.data[0].cuR_TEL1 || "",
          cuR_TEL2: response.data[0].cuR_TEL2 || "",
          cuR_TEL3: response.data[0].cuR_TEL3 || "",

          cuR_CITY: response.data[0].cuR_CITY || "",
          cuR_STATE: response.data[0].cuR_STATE || "",
          cuR_STATE_DESCR: response.data[0].cuR_STATE_DESCR || "",
          cuR_PIN: response.data[0].cuR_PIN || "",
          pdf: response.data[0].pdf || pdf,
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });

    axios
      .get(`${EMPLOYEE_URL}/ddl_GetStates`)
      .then((response) => {
        setStates(response.data.states);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    cuR_ADD1: Yup.string().required("Line 1 is required"),
    cuR_ADD2: Yup.string().required("Line 2 is required"),
    cuR_ADD3: Yup.string().required("Line 3 is required"),

    cuR_TEL1: Yup.string().required("Telephone 1 is required"),
    cuR_TEL2: Yup.string().required("Telephone 2  is required"),
    cuR_TEL3: Yup.string().required("Telephone 3  is required"),

    cuR_CITY: Yup.string().required("Current City is required"),
    cuR_STATE: Yup.string().required("Current State is required"),
    cuR_PIN: Yup.string().required("Zip Code is required"),
  });

  const formik = useFormik({
    initialValues: {
      cuR_ADD1: "",
      cuR_ADD2: "",
      cuR_ADD3: "",

      cuR_TEL1: "",
      cuR_TEL2: "",
      cuR_TEL3: "",

      cuR_CITY: "",
      cuR_STATE: "",
      cuR_STATE_DESCR: "",
      cuR_PIN: "",
      pdf: null,
    },
    validationSchema: validationSchema,

    onSubmit: (values, formik) => {
      console.log("Submitting form data:", values);
      // const formData = new FormData();
      // Object.keys(values).forEach((key) => {
      //   if (key === "pdf" && pdf) {
      //     formData.append(key, pdf);
      //   } else {
      //     formData.append(key, values[key as keyof typeof values] as string);
      //   }
      // });

      axios
        .put(
          `${EMPLOYEE_URL}/UpdateEmployeePresentAddressDetails/${employeeIdForView}`,
          {
            ...values,
            chG_DATE: new Date(),
            chG_USER: currentUser?.id,
            chG_TERM: "TERM",
            // pdf: pdf,
          }
        )
        .then((response) => {
          console.log("Basic Details Posted successfully!", response.data);
          const { responseStatus, message } = response.data.data;
          if (responseStatus === "E") {
            formik.setSubmitting(false);
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );

            formik.resetForm();
            navigate(-1);
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );

          formik.setSubmitting(false);
        });
    },
  });
  const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (file) {
      setPdf(file);
    }
  };
  const handleClose = () => {
    navigate(-1);
    // navigate("/crafted/pages/profile/overview");
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Present Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="cuR_ADD1"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CURRENT ADDRESS :
                </label>
                <input
                  type="text"
                  id="cuR_ADD1"
                  className="form-control form-control-lg form-control-solid"
                  name="cuR_ADD1"
                  placeholder="Line 1"
                  value={formik.values.cuR_ADD1}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="cuR_TEL1"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  TELEPHONE :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="cuR_TEL1"
                  placeholder="Telephone 1"
                  value={formik.values.cuR_TEL1}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.cuR_ADD1 && formik.errors.cuR_ADD1 ? (
                  <div className="text-danger">{formik.errors.cuR_ADD1}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.cuR_TEL1 && formik.errors.cuR_TEL1 ? (
                  <div className="text-danger">{formik.errors.cuR_TEL1}</div>
                ) : null}
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="cuR_ADD2"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  &nbsp;
                </label>
                <input
                  type="text"
                  id="cuR_ADD2"
                  className="form-control form-control-lg form-control-solid"
                  name="cuR_ADD2"
                  placeholder="Line 2"
                  value={formik.values.cuR_ADD2}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="cuR_TEL2"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  &nbsp;
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="cuR_TEL2"
                  placeholder="Telephone 2"
                  value={formik.values.cuR_TEL2}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.cuR_ADD2 && formik.errors.cuR_ADD2 ? (
                  <div className="text-danger">{formik.errors.cuR_ADD2}</div>
                ) : null}{" "}
              </div>

              <div className="col-lg-6">
                {formik.touched.cuR_TEL2 && formik.errors.cuR_TEL2 ? (
                  <div className="text-danger">{formik.errors.cuR_TEL2}</div>
                ) : null}
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="cuR_ADD3"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  &nbsp;
                </label>
                <input
                  type="text"
                  id="cuR_ADD3"
                  className="form-control form-control-lg form-control-solid"
                  name="cuR_ADD3"
                  placeholder="Line 3"
                  value={formik.values.cuR_ADD3}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="cuR_TEL3"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  &nbsp;
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="cuR_TEL3"
                  placeholder="Telephone 3"
                  value={formik.values.cuR_TEL3}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.cuR_ADD3 && formik.errors.cuR_ADD3 ? (
                  <div className="text-danger">{formik.errors.cuR_ADD3}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.cuR_TEL3 && formik.errors.cuR_TEL3 ? (
                  <div className="text-danger">{formik.errors.cuR_TEL3}</div>
                ) : null}
              </div>
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="cuR_CITY"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CURRENT CITY :
                </label>
                <input
                  type="text"
                  id="cuR_CITY"
                  className="form-control form-control-lg form-control-solid"
                  name="cuR_CITY"
                  placeholder="Enter current city"
                  value={formik.values?.cuR_CITY}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="cuR_STATE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CURRENT STATE :
                </label>
                <select
                  id="cuR_STATE"
                  className="form-select form-select-lg form-select-solid "
                  name="cuR_STATE"
                  onChange={formik.handleChange}
                >
                  <option value={formik.values?.cuR_STATE}>
                    {formik.values?.cuR_STATE_DESCR}
                  </option>

                  {states.map((state) => (
                    <option key={state.statE_CODE} value={state.statE_CODE}>
                      {state.descr}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.cuR_CITY && formik.errors.cuR_CITY ? (
                  <div className="text-danger">{formik.errors.cuR_CITY}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.cuR_STATE && formik.errors.cuR_STATE ? (
                  <div className="text-danger">{formik.errors.cuR_STATE}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="cuR_PIN"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  ZIP CODE :
                </label>
                <input
                  type="text"
                  id="cuR_PIN"
                  className="form-control form-control-lg form-control-solid"
                  name="cuR_PIN"
                  placeholder="Enter zip code "
                  value={formik.values?.cuR_PIN}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="cuR_PIN"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  ATTACH DOCUMENT :
                </label>
                <input
                  name="birtH_DATE"
                  className="form-control form-control-lg flex-grow-1"
                  type="file"
                  id="pdf"
                  onChange={handlePdfChange}
                  accept="application/pdf"
                  required
                />
                {formik.touched.pdf && formik.errors.pdf ? (
                  <div className="text-danger">{formik.errors.pdf}</div>
                ) : null}
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.cuR_PIN && formik.errors.cuR_PIN ? (
                  <div className="text-danger">{formik.errors.cuR_PIN}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit is HERE */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default PresentDetailsEdit;
