import { Content } from '../../../../_metronic/layout/components/content'
import { QualificationDetailsWidget } from '../../../../_metronic/partials/widgets/lists/QualificationDetailsWidget'


export function QualificationDetails() {
  return (
    <Content>
        <>
        <QualificationDetailsWidget className='mb-5 mb-xl-8' />
        </>
   </Content>
  )
}