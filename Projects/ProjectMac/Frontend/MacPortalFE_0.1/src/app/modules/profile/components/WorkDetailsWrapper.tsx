import { Content } from "../../../../_metronic/layout/components/content";
import { KYC } from "./KYC";
import { PersonalDetails } from "./PersonalDetails";
import { WorkDetails } from "./WorkDetails";


export function WorkDetailsWrapper() {
  return (
    <Content>
      <>
        <div className="row">

        <div>
            <PersonalDetails />
          </div>

          <div>
            <WorkDetails />
          </div>
          </div>
      </>
    </Content>
  );
}
