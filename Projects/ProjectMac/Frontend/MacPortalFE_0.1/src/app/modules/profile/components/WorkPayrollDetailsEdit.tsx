import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
import { BankDetailsEditModal } from "../bankDetails-edit-modal/BankDetailsEditModal";
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
interface EmpSiteDropdownItem {
  siteCode: string;
  description: string;
  areaCode: string;
  address: string;
  financialEntity: string;
}

const WorkPayrollDetailsEdit: React.FC = () => {
  const [employee, setEmployee] = useState<any>({});
  const navigate = useNavigate();
  const [drpdwnEmpSites, setDrpdwnEmpSites] = useState<EmpSiteDropdownItem[]>(
    []
  );
  const { currentUser } = useAuth();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleOpenModal = () => {
    setIsModalOpen(true);
  };
  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/EMP_WORK_PAYROLL_site/${employeeIdForView}`)
      .then((response) => {
        setDrpdwnEmpSites(response.data);
      })
      .catch((error) => {
        console.error("Error fetching EmpSite details:", error);
      });

    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setEmployee(response.data[0]);
        // Update formik's initial values after fetching employee data
        formik.setValues({
          //   induction: response.data[0].induction || "",
          worK_SITE: response.data[0].worK_SITE || "",
          gratuitY_DATE: response.data[0].gratuitY_DATE || "",
          noticE_PRD: response.data[0].noticE_PRD || "",
          basic: response.data[0].basic || "",
          curR_CTC: response.data[0].curR_CTC || "",
          paY_MODE: response.data[0].paY_MODE || "",
          paY_TYPE: response.data[0].paY_TYPE || "",
          banK_CODE: response.data[0].banK_CODE || "",
          banK_IFSC: response.data[0].banK_IFSC || "",
          banK_NAME: response.data[0].banK_NAME || "",
          banK_ACCT: response.data[0].banK_ACCT || "",
          dD_PAYABLE_BANK: response.data[0].dD_PAYABLE_BANK || "",
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    worK_SITE: Yup.string().required("Work Site required"),
    gratuitY_DATE: Yup.string().required("Gratuity Date required"),
    noticE_PRD: Yup.string().required("Notice Peroid required"),
    // basic: Yup.string().required("Basic required"),
    // curR_CTC: Yup.string().required("Current CTC required"),
    // paY_MODE: Yup.string().required("Pay Mode required"),
    // paY_TYPE: Yup.string().required("Pay Type required"),
    // banK_CODE: Yup.string().required("Bank Code required"),
    // banK_IFSC: Yup.string().required("bank IFSC required"),
    // banK_NAME: Yup.string().required("Bank Name required"),
    // banK_ACCT: Yup.string().required("Bank Account required"),
    // dD_PAYABLE_BANK: Yup.string().required("DD Payable Bank required"),
  });

  const formik = useFormik({
    initialValues: {
      worK_SITE: "",
      gratuitY_DATE: "",
      noticE_PRD: "",
      basic: "",
      curR_CTC: "",
      paY_MODE: "",
      paY_TYPE: "",
      banK_CODE: "",
      banK_IFSC: "",
      banK_NAME: "",
      banK_ACCT: "",
      dD_PAYABLE_BANK: "",
    },
    validationSchema: validationSchema,

    onSubmit: (values) => {
      console.log("Submitting form data:", values);

      axios
        .put(
          `${EMPLOYEE_URL}/UpdateEmployeeWorkPayrollDetails/${employeeIdForView}`,
          {
            ...values,
            chG_DATE: new Date(),
            chG_USER: currentUser?.id, //const { currentUser } = useAuth();
            chG_TERM: "TERM",
            // pdf: pdf,
          }
        )
        .then((response) => {
          console.log("Basic Details Posted successfully!", response.data);
          const { responseStatus, message } = response.data.data;
          if (responseStatus === "E") {
            formik.setSubmitting(false);
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
            setEmployee({
              worK_SITE: "",
              gratuitY_DATE: "",
              noticE_PRD: "",
              basic: "",
              curR_CTC: "",
              paY_MODE: "",
              paY_TYPE: "",
              banK_CODE: "",
              banK_IFSC: "",
              banK_NAME: "",
              banK_ACCT: "",
              dD_PAYABLE_BANK: "",
            });
            formik.resetForm();
            navigate(-1);
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );

          formik.setSubmitting(false);
        });
    },
  });

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Work & Payroll Details</h3>
        </div>
        
        <div>
          <button
            type="button"
            className="btn btn-light-danger me-10"
            onClick={handleOpenModal}
          >
            Change Bank Details
          </button>
          {isModalOpen && <BankDetailsEditModal onClose={handleCloseModal}/>}

          <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
        </div>

        
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="worK_SITE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  WORK SITE:
                </label>
                <select
                  name="worK_SITE"
                  className="form-select form-select-lg form-select-solid"
                  value={formik.values?.worK_SITE}
                  onChange={formik.handleChange}
                >
                  <option value={formik.values?.worK_SITE}>
                    {formik.values?.worK_SITE}
                  </option>
                  {drpdwnEmpSites.map((EmpSite) => (
                    <option key={EmpSite.siteCode} value={EmpSite.siteCode}>
                      {EmpSite.description}
                    </option>
                  ))}
                </select>
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="gratuitY_DATE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  GRATUITY DATE:
                </label>
                <input
                  type="date"
                  id="gratuitY_DATE"
                  className="form-control form-control-lg form-control-solid"
                  name="gratuitY_DATE"
                  placeholder="Enter Gratuity Date"
                  // value={formik.values.gratuitY_DATE?.toUpperCase()}
                  value={
                    formik.values?.gratuitY_DATE
                      ? new Date(formik.values?.gratuitY_DATE)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.worK_SITE && formik.errors.worK_SITE ? (
                  <div className="text-danger">{formik.errors.worK_SITE}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.gratuitY_DATE && formik.errors.gratuitY_DATE ? (
                  <div className="text-danger">
                    {formik.errors.gratuitY_DATE}
                  </div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="noticE_PRD"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  NOTICE PERIOD(DAYS):
                </label>
                <input
                  type="number"
                  id="noticE_PRD"
                  className="form-control form-control-lg form-control-solid"
                  name="noticE_PRD"
                  placeholder="Enter Notice Period"
                  value={formik.values?.noticE_PRD}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  min={1}
                  max={180}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="basic"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  BASIC PER MONTH:
                </label>
                <input
                  type="text"
                  id="basic"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="basic"
                  placeholder="Enter Basic Per Month"
                  value={formik.values.basic}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.noticE_PRD && formik.errors.noticE_PRD ? (
                  <div className="text-danger">{formik.errors.noticE_PRD}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.basic && formik.errors.basic ? (
                  <div className="text-danger">{formik.errors.basic}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="curR_CTC"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CTC PER MONTH:
                </label>
                <input
                  type="text"
                  id="curR_CTC"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="curR_CTC"
                  placeholder="Enter CTC PER MONTH"
                  value={formik.values?.curR_CTC}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="paY_MODE"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  PAY MODE:
                </label>
                <input
                  type="text"
                  id="paY_MODE"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="paY_MODE"
                  placeholder="Enter Pay Mode"
                  value={formik.values.paY_MODE?.toUpperCase()}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.curR_CTC && formik.errors.curR_CTC ? (
                  <div className="text-danger">{formik.errors.curR_CTC}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.paY_MODE && formik.errors.paY_MODE ? (
                  <div className="text-danger">{formik.errors.paY_MODE}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="paY_TYPE"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  PAY TYPE:
                </label>
                <input
                  type="text"
                  id="paY_TYPE"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="paY_TYPE"
                  placeholder="Enter Pay Type"
                  value={formik.values?.paY_TYPE}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="banK_CODE"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  BANK CODE:
                </label>
                <input
                  type="text"
                  id="banK_CODE"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="banK_CODE"
                  placeholder="Enter Bank Code"
                  value={formik.values.banK_CODE?.toUpperCase()}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.paY_TYPE && formik.errors.paY_TYPE ? (
                  <div className="text-danger">{formik.errors.paY_TYPE}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.banK_CODE && formik.errors.banK_CODE ? (
                  <div className="text-danger">{formik.errors.banK_CODE}</div>
                ) : null}
              </div>
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="banK_IFSC"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  BANK IFSC CODE:
                </label>
                <input
                  type="text"
                  id="banK_IFSC"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="banK_IFSC"
                  placeholder="Enter Bank IFSC Code"
                  value={formik.values?.banK_IFSC}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="banK_NAME"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  BANK A/C NAME:
                </label>
                <input
                  type="text"
                  id="banK_NAME"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="banK_NAME"
                  placeholder="Enter Bank Name"
                  value={formik.values.banK_NAME?.toUpperCase()}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.banK_IFSC && formik.errors.banK_IFSC ? (
                  <div className="text-danger">{formik.errors.banK_IFSC}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.banK_NAME && formik.errors.banK_NAME ? (
                  <div className="text-danger">{formik.errors.banK_NAME}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="banK_ACCT"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  BANK A/C NO.:
                </label>
                <input
                  type="text"
                  id="banK_ACCT"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="banK_ACCT"
                  placeholder="Enter Bank A/C No"
                  value={formik.values?.banK_ACCT}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="dD_PAYABLE_BANK"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DD PAY BANK:
                </label>
                <input
                  type="text"
                  id="dD_PAYABLE_BANK"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="dD_PAYABLE_BANK"
                  placeholder="Enter DD Pay Bank"
                  value={formik.values.dD_PAYABLE_BANK?.toUpperCase()}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.banK_ACCT && formik.errors.banK_ACCT ? (
                  <div className="text-danger">{formik.errors.banK_ACCT}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.dD_PAYABLE_BANK &&
                formik.errors.dD_PAYABLE_BANK ? (
                  <div className="text-danger">
                    {formik.errors.dD_PAYABLE_BANK}
                  </div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit is HERE */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
        
      </div>
    </div>
  );
};

export default WorkPayrollDetailsEdit;
