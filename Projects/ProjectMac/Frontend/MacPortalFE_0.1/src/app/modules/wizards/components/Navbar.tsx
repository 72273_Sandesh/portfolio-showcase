import React from 'react';

interface NavbarProps {
  sections: string[];
  handleSectionClick: (section: string) => void;
}

const Navbar: React.FC<NavbarProps> = ({ sections, handleSectionClick }) => {
  return (
    <nav>
      <ul>
        {sections.map((section, index) => (
          <li key={index}>
            <button onClick={() => handleSectionClick(section)}>{section}</button>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Navbar;