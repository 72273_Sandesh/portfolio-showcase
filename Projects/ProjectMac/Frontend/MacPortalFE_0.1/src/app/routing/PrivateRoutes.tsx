import { lazy, FC, Suspense } from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import { MasterLayout } from "../../_metronic/layout/MasterLayout";
import TopBarProgress from "react-topbar-progress-indicator";
import { DashboardWrapper } from "../pages/dashboard/DashboardWrapper";
import { MenuTestPage } from "../pages/MenuTestPage";
import { getCSSVariableValue } from "../../_metronic/assets/ts/_utils";
import { WithChildren } from "../../_metronic/helpers";
import BuilderPageWrapper from "../pages/layout-builder/BuilderPageWrapper";
import EmployeesPage from "../modules/apps/employee/EmployeePage";
import DepartmentPage from "../modules/apps/department/DepartmentPage";
import DesignationsPage from "../modules/apps/designation/DesignationsPage";
import DeptPage from "../modules/apps/dept/DeptPage";
import QualificationPage from "../modules/apps/qualification/QualificationPage";
import CadrePage from "../modules/apps/cadre/CadrePage";
import GradePage from "../modules/apps/grade/GradePage";
import BankDetailsPage from "../modules/apps/bank-details/BankDetailsPage";

const PrivateRoutes = () => {
  const ProfilePage = lazy(() => import("../modules/profile/ProfilePage"));
  const WizardsPage = lazy(() => import("../modules/wizards/WizardsPage"));
  const AccountPage = lazy(() => import("../modules/accounts/AccountPage"));
  const WidgetsPage = lazy(() => import("../modules/widgets/WidgetsPage"));
  const ChatPage = lazy(() => import("../modules/apps/chat/ChatPage"));
  const UsersPage = lazy(
    () => import("../modules/apps/user-management/UsersPage")
  );
  const EmployeeSearchPage = lazy(
    () => import("../modules/apps/employee-search/EmployeeSearchPage")
  );

  return (
    <Routes>
      <Route element={<MasterLayout />}>
        {/* Redirect to Dashboard after success login/registartion */}
        <Route path="auth/*" element={<Navigate to="/dashboard" />} />
        {/* Pages */}
        <Route path="dashboard" element={<DashboardWrapper />} />
        <Route path="builder" element={<BuilderPageWrapper />} />
        <Route path="menu-test" element={<MenuTestPage />} />
        {/* Lazy Modules */}
        <Route
          path="crafted/pages/profile/*"
          element={
            <SuspensedView>
              <ProfilePage />
            </SuspensedView>
          }
        />
        <Route
          path="crafted/pages/wizards/*"
          element={
            <SuspensedView>
              <WizardsPage />
            </SuspensedView>
          }
        />
        <Route
          path="crafted/widgets/*"
          element={
            <SuspensedView>
              <WidgetsPage />
            </SuspensedView>
          }
        />
        <Route
          path="crafted/account/*"
          element={
            <SuspensedView>
              <AccountPage />
            </SuspensedView>
          }
        />
        <Route
          path="apps/chat/*"
          element={
            <SuspensedView>
              <ChatPage />
            </SuspensedView>
          }
        />
        <Route
          path="apps/user-management/*"
          element={
            <SuspensedView>
              <UsersPage />
            </SuspensedView>
          }
        />

        <Route
          path="/apps/department/*"
          element={
            <SuspensedView>
              <DepartmentPage />
            </SuspensedView>
          }
        />

        <Route
          path="/apps/qualification/*"
          element={
            <SuspensedView>
              <QualificationPage />
            </SuspensedView>
          }
        />

        <Route
          path="/apps/cadre/*"
          element={
            <SuspensedView>
              <CadrePage />
            </SuspensedView>
          }
        />

        <Route
          path="/apps/bankDetails/*"
          element={
            <SuspensedView>
              <BankDetailsPage />
            </SuspensedView>
          }
        />

        <Route
          path="/apps/employee/*"
          element={
            <SuspensedView>
              <EmployeesPage />
            </SuspensedView>
          }
        />

        <Route
          path="/apps/grade/*"
          element={
            <SuspensedView>
              <GradePage />
            </SuspensedView>
          }
        />
        <Route
          path="/apps/dept/*"
          element={
            <SuspensedView>
              <DeptPage />
            </SuspensedView>
          }
        />

        <Route
          path="/apps/employee-search/*"
          element={
            <SuspensedView>
              <EmployeeSearchPage />
            </SuspensedView>
          }
        />
        <Route
          path="apps/designation/*"
          element={
            <SuspensedView>
              <DesignationsPage />
            </SuspensedView>
          }
        />

        {/* Page Not Found */}
        <Route path="*" element={<Navigate to="/error/404" />} />
      </Route>
    </Routes>
  );
};

const SuspensedView: FC<WithChildren> = ({ children }) => {
  const baseColor = getCSSVariableValue("--bs-primary");
  TopBarProgress.config({
    barColors: {
      "0": baseColor,
    },
    barThickness: 1,
    shadowBlur: 5,
  });
  return <Suspense fallback={<TopBarProgress />}>{children}</Suspense>;
};

export { PrivateRoutes };
