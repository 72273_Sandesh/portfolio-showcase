import React from 'react';

type Props = {
  changeDate: Date;
};

const GradeLastLoginCell: React.FC<Props> = ({ changeDate }) => {
  const formattedDate = new Date(changeDate).toLocaleString('en-GB', {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric',
    // hour: '2-digit',
    // minute: '2-digit',
    // second: '2-digit',
  });

  return (
    <div className=' fw-bolder d-flex align-items-right'>
      {formattedDate ?(formattedDate): <div className='badge badge-danger fw-bolder d-flex align-items-right'>Not Assigned</div>}
    </div>
  );
};

export { GradeLastLoginCell };
