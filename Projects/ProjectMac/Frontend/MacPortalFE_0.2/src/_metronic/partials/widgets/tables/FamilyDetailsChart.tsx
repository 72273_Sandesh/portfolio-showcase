// import axios from "axios";
// import React, { useEffect, useState } from "react";
// import ReactApexChart from "react-apexcharts";
// import { employeeIdForView } from "../../../../app/modules/GlobalQuery";
// import { Link } from "react-router-dom";
// const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// type FamilyDetail = {
//   membeR_NAME: string;
//   statutorY_TYPE: string;
//   percentage: number;
// };

// const FamilyDetailsChart: React.FC = () => {
//   const [statutoryDetails, setStatutoryDetails] = useState<FamilyDetail[]>([]);

//   useEffect(() => {
//     // Fetch family details from API
//     axios
//       .get(`${EMPLOYEE_URL}/${employeeIdForView}/STATU`)
//       .then((response) => {
//         setStatutoryDetails(response.data);
//       })
//       .catch((error) => {
//         console.error("Error fetching family details:", error);
//       });
//   }, []);

//   // Separate the data by statutory type
//   const groupedDetails = {
//     GRATUITY: statutoryDetails.filter(detail => detail.statutorY_TYPE === "GRATUITY"),
//     PF: statutoryDetails.filter(detail => detail.statutorY_TYPE === "PF"),
//     PENSION: statutoryDetails.filter(detail => detail.statutorY_TYPE === "PENSION")
//   };

//   const calculateSeries = (details) => {
//     if (details.length === 0) return [100];
//     const percentages = details.map(detail => detail.percentage);
//     const sum = percentages.reduce((acc, curr) => acc + curr, 0);
//     return [...percentages, 100 - sum];
//   };

//   const chartOptions = {
//     chart: {
//       type: "donut",
//       toolbar: {
//         show: false,
//       },
//     },
//     plotOptions: {
//       pie: {
//         startAngle: -90,
//         endAngle: 270,
//       },
//     },
//     dataLabels: {
//       enabled: false,
//     },
//     fill: {
//       type: "gradient",
//     },
//     legend: {
//       formatter: function (val: string, opts: any) {
//         return val + " - " + opts.w.globals.series[opts.seriesIndex] + "%";
//       },
//       fontSize: "14px",
//       position: "bottom",
//     },
//     responsive: [
//       {
//         breakpoint: 480,
//         options: {
//           chart: {
//             width: "100%",
//           },
//           legend: {
//             position: "bottom",
//           },
//         },
//       },
//     ],
//   };

//   const chartHeight = 250;
//   const chartWidth = "100%";
//   const chartMargin = "20px";

//   return (
//     <div className="card">
//       <div className="card-header bg-light-primary border-0 pt-5">
//         <h3 className="card-title align-items-start flex-column">
//           <span className="card-label fw-bold fs-3 mb-1">
//             Family Nomination Details
//           </span>
//         </h3>
//       </div>
//       <div
//         className="chart-container"
//         style={{
//           display: "flex",
//           justifyContent: "space-around",
//           marginTop: "20px",
//         }}
//       >
//         {["GRATUITY", "PF", "PENSION"].map((type, index) => (
//           <div
//             key={index}
//             className="chart-item"
//             style={{
//               height: chartHeight,
//               width: chartWidth,
//               margin: chartMargin,
//             }}
//           >
//             <h2
//               style={{
//                 fontSize: "20px",
//                 fontWeight: "bold",
//                 textAlign: "center",
//               }}
//             >
//               {type}
//             </h2>
//             <ReactApexChart
//               options={{
//                 ...chartOptions,
//                 labels: groupedDetails[type].length > 0 ? groupedDetails[type].map(detail => detail.membeR_NAME).concat("Remaining") : ["Remaining"]
//               }}
//               series={calculateSeries(groupedDetails[type])}
//               type="donut"
//               height={chartHeight}
//             />
//             {groupedDetails[type].length === 0 && (
//               <div style={{ textAlign: "center" ,color:"red"}}>DATA NOT ASSIGNED YET </div>
//             )}
//           </div>
//         ))}
//       </div>
//     </div>
//   );
// };

// export default FamilyDetailsChart;

import axios from "axios";
import React, { useEffect, useState } from "react";
import ReactApexChart from "react-apexcharts";
import { ApexOptions } from "apexcharts";
import { employeeIdForView } from "../../../../app/modules/GlobalQuery";
import { Link } from "react-router-dom";

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

type FamilyDetail = {
  membeR_NAME: string;
  statutorY_TYPE: string;
  percentage: number;
};

interface Detail {
  percentage: number;
  membeR_NAME: string;
}

type GroupedDetails = {
  [key: string]: Detail[];
};

const calculateSeries = (details: Detail[]): number[] => {
  if (details.length === 0) return [100];

  const percentages = details.map((detail) => detail.percentage);
  const sum = percentages.reduce((acc, curr) => acc + curr, 0);

  return [...percentages, 100 - sum];
};

const trimName = (name: string): string => {
  return name.split(" ")[0];
};

const FamilyDetailsChart: React.FC = () => {
  const [statutoryDetails, setStatutoryDetails] = useState<FamilyDetail[]>([]);

  useEffect(() => {
    // Fetch family details from API
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/STATU`)
      .then((response) => {
        setStatutoryDetails(response.data);
      })
      .catch((error) => {
        console.error("Error fetching family details:", error);
      });
  }, []);

  // Separate the data by statutory type
  const groupedDetails: GroupedDetails = {
    GRATUITY: statutoryDetails.filter(
      (detail) => detail.statutorY_TYPE === "GRATUITY"
    ),
    PF: statutoryDetails.filter((detail) => detail.statutorY_TYPE === "PF"),
    PENSION: statutoryDetails.filter(
      (detail) => detail.statutorY_TYPE === "PENSION"
    ),
  };

  const chartOptions: ApexOptions = {
    chart: {
      type: "donut",
      toolbar: {
        show: false,
      },
    },
    plotOptions: {
      pie: {
        startAngle: -90,
        endAngle: 270,
      },
    },
    dataLabels: {
      enabled: false,
    },
    fill: {
      type: "gradient",
    },
    legend: {
      formatter: function (
        val: string,
        opts: { w: { globals: { series: number[] } }; seriesIndex: number }
      ) {
        return val + " - " + opts.w.globals.series[opts.seriesIndex] + "%";
      },
      fontSize: "14px",
      position: "bottom",
    },
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: "100%",
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  };

  return (
    <div className="card">
      <div className="card-header bg-light-primary border-0 pt-5">
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bold fs-3 mb-1">
            Family Nomination Details
          </span>
        </h3>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          flexWrap: "wrap",
          marginTop: "20px",
        }}
      >
        {["GRATUITY", "PF", "PENSION"].map((type, index) => (
          <div
            key={index}
            style={{
              flex: "1 1 300px",
              margin: "20px",
              maxWidth: "30%",
              boxSizing: "border-box",
            }}
          >
            <h2
              style={{
                fontSize: "20px",
                fontWeight: "bold",
                textAlign: "center",
              }}
            >
              {type}
            </h2>
            <ReactApexChart
              options={{
                ...chartOptions,
                labels:
                  groupedDetails[type].length > 0
                    ? groupedDetails[type]
                        .map((detail) => trimName(detail.membeR_NAME))
                        .concat("Remaining")
                    : ["Remaining"],
              }}
              series={calculateSeries(groupedDetails[type])}
              type="donut"
              height={250}
            />
            {groupedDetails[type].length === 0 && (
              <div style={{ textAlign: "center", color: "red" }}>
                DATA NOT ASSIGNED YET
              </div>
            )}
          </div>
        ))}
      </div>

      <div className="card-header bg-light-primary border-0 pt-5">
        <div className="card-toolbar">
          <Link
            to="/crafted/pages/profile/EditGratuityNominationsWrapper "
            className="btn btn-sm btn-primary me-3 align-self-center min-w-300px"
          >
            Edit Gratuity Nominations
          </Link>
        </div>

        <div className="card-toolbar">
          <Link
            to="/crafted/pages/profile/EditPFNominationsWrapper "
            className="btn btn-sm btn-primary me-3 align-self-center min-w-300px"
          >
            Edit PF Nominations
          </Link>
        </div>

        <div className="card-toolbar">
          <Link
            to="/crafted/pages/profile/EditPensionNominationsWrapper "
            className="btn btn-sm btn-primary me-3 align-self-center min-w-300px "
          >
            Edit Pension Nominations
          </Link>
        </div>
      </div>
    </div>
  );
};

export default FamilyDetailsChart;
