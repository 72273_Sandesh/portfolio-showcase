import { Route, Routes, Outlet, Navigate } from "react-router-dom";
import { PageLink, PageTitle } from "../../../../_metronic/layout/core";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { JourneyDetailsListWrapper } from "./JourneyDetails-list/JourneyDetailsList";

const JourneyDetailsBreadcrumbs: Array<PageLink> = [
  {
    title: "Master",
    path: "/apps/Journey/JourneyDetails",
    isSeparator: false,
    isActive: false,
  },
  {
    title: "",
    path: "",
    isSeparator: true,
    isActive: false,
  },
];

const JourneyDetailsPage = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path="Journey"
          element={
            <>
              <PageTitle breadcrumbs={JourneyDetailsBreadcrumbs}>
                Journey List
              </PageTitle>
              <JourneyDetailsListWrapper />
              <ToastContainer />
            </>
          }
        />
      </Route>
      <Route index element={<Navigate to="/apps/Journey/users" />} />
    </Routes>
  );
};

export default JourneyDetailsPage;
