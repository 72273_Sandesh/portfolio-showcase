import React from 'react';
import { useQueryResponseData } from '../../core/QueryResponseProvider';

const BankDetailsTableFormat = () => {
  const bankDetails = useQueryResponseData();

    return (
  
        <table id="table-to-export">
        <thead>
          <tr>
            <th>BankDetails Id</th>
            <th>BankDetails Name</th>
            <th>Description</th>
            <th>BankDetails Head</th>
            <th>Status</th>
            <th>Account Code</th>
            <th>Pay Table</th>
            <th>CCTR Code</th>
            <th>Account Code Ap</th>
            <th>CCTR Code Ap</th>
            <th>UDF Number 1</th>
            <th>Sh Dept Code</th>
            <th>Change User</th>
            <th>Change Date</th>
            <th>Change Term</th>
            <th>Change Reason</th>
          </tr>
        </thead>
        <tbody>
          {bankDetails.map(bankDetail => (
            <tr key={bankDetail.id}>
              <td>{bankDetail.id}</td>
              <td>{bankDetail.name}</td>
              <td>{bankDetail.shortDescription}</td>
              <td>{bankDetail.deptHead}</td>
              <td>{bankDetail.status}</td>
              <td>{bankDetail.acctCode}</td>
              <td>{bankDetail.payTable}</td>
              <td>{bankDetail.cctrCode}</td>
              <td>{bankDetail.acctCodeAp}</td>
              <td>{bankDetail.cctrCodeAp}</td>
              <td>{bankDetail.udfNumber1}</td>
              <td>{bankDetail.shDeptCode}</td>
              <td>{bankDetail.changeUser}</td>
              <td>{bankDetail.changeDate?.toString()}</td>
              <td>{bankDetail.changeTerm}</td>
              <td>{bankDetail.changeReason}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  
};

export default BankDetailsTableFormat;
