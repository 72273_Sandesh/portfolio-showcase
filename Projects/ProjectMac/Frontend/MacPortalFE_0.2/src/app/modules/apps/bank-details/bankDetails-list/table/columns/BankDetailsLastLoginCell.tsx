import React from 'react';

type Props = {
  changeDate: Date;
};

const BankDetailsLastLoginCell: React.FC<Props> = ({ changeDate }) => {
  const formattedDate = new Date(changeDate).toLocaleString('en-GB', {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric',
    // hour: '2-digit',
    // minute: '2-digit',
    // second: '2-digit',
  });

  return (
    <div className=' fw-bolder d-flex align-items-right'>
          <div className='badge badge-light fw-bolder d-flex align-items-right'>{formattedDate}</div>
        </div>
    // <div className='badge badge-light fw-bolder d-flex align-items-right'>
    //   {formattedDate}
    // </div>
  );
};

export { BankDetailsLastLoginCell };
