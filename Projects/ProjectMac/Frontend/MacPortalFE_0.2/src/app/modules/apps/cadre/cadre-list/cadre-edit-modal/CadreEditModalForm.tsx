import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { isNotEmpty } from "../../../../../../_metronic/helpers";
import { initialCadre, Cadre } from "../core/_models";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { CadreListLoading } from "../components/loading/CadreListLoading";
import { createCadre, updateCadre } from "../core/_requests";
import { useQueryResponse } from "../core/QueryResponseProvider";
import { globalVariable, setGlobalVariable } from "../../GlobalVariable";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
const API_URL = import.meta.env.VITE_CADRE_DROPDOWNS_API_URL;

type Props = {
  isCadreLoading: boolean;
  cadre: Cadre;
};

const editCadreSchema = Yup.object().shape({
  sH_DESCR: Yup.string().required("Short Description is required"),
  descr: Yup.string().required("Description is required"),
  chG_REASON: Yup.string(),
  cadreCategory: Yup.string().required("Category is required"),
  status: Yup.string().required("Status is required"),
});

interface cadreCategory {
  banD_CODE: string;
  descr: string;
}
const CadreEditModalForm: FC<Props> = ({ cadre, isCadreLoading }) => {
  const { setItemIdForUpdate } = useListView();
  const { refetch } = useQueryResponse();

  const [cadreCategorys, setCadreCategorys] = useState<cadreCategory[]>([]);
  const [cadreCategory, setCadreCategory] = useState("");
  const [chG_REASON, setchG_REASON] = useState<string>("");

  const [statuses] = useState(["ACTIVE", "INACTIVE"]);
  const [status, setStatus] = useState("");

  const { currentUser } = useAuth();
  useEffect(() => {
    // const API_URL = `https://localhost:44360/api/Cadre/GetAll_CadreCategory`;

    fetchFromBackend()
      .then((combinedData) => {
        setCadreCategorys(combinedData.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const fetchFromBackend = async () => {
    try {
      const response = await axios.get(API_URL);
      const data = await response.data;
      return data;
    } catch (error) {
      throw new Error("Failed to fetch grade from backend");
    }
  };

  const [cadreForEdit] = useState<Cadre>({
    ...cadre,
    descr: cadre.descr || initialCadre.descr,
    sH_DESCR: cadre.sH_DESCR || initialCadre.sH_DESCR,
    chG_TERM: cadre.chG_TERM || initialCadre.chG_TERM || status,
    chG_DATE: cadre.chG_DATE || initialCadre.chG_DATE,
    chG_USER: cadre.chG_USER || initialCadre.chG_USER,
    cadreCategory: cadre.cadreCategory || initialCadre.cadreCategory,
    status: cadre.status || initialCadre.status,
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };

  const formik = useFormik({
    initialValues: cadreForEdit,
    validationSchema: editCadreSchema,

    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      if (isNotEmpty(values.id) && globalVariable === "editCadre") {
        values.chG_USER = currentUser?.first_name;
        values.chG_TERM = "TERM";
        values.chG_DATE = new Date();
        const response = await updateCadre(values);

        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("addCadre");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      } else {
        values.chG_USER = currentUser?.first_name;
        values.chG_TERM = "TERM";
        values.cadrE_CODE = "null";
        values.chG_DATE = new Date();
        const response = await createCadre(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("addCadre");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      }
    },
  });
  function formatDate(dateString: string) {
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata", // Specify the Indian time zone
    });
    return `${formattedDate} ${formattedTime}`;
  }

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column" id="kt_modal_add_user_scroll">
          <div className="row mb-3">
            {globalVariable !== "addCadre" && (
              <div className="">
                <div className="fv-row">
                  <label className=" fw-bold fs-6 mb-2">Cadre Code :</label>
                  <input
                    className="form-control form-control-solid mb-3 mb-lg-0"
                    value={formik.values.id ? formik.values.id : ""}
                  />
                </div>
              </div>
            )}

            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Name of Cadre
                </label>
                <input
                  placeholder="Name"
                  value={formik.values.descr?.toUpperCase()}
                  // {...formik.getFieldProps('descr')}
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "is-invalid": formik.touched.descr && formik.errors.descr,
                    },
                    { "is-valid": formik.touched.descr && !formik.errors.descr }
                  )}
                  type="text"
                  name="descr"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isCadreLoading ||
                    globalVariable === "infoCadre"
                  }
                  onChange={(e) => {
                    formik.setFieldValue("descr", e.target.value.toUpperCase());
                  }}
                />
                {formik.touched.descr && formik.errors.descr && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.descr}</span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  {" "}
                  Short Description
                </label>
                <input
                  placeholder="Short Description"
                  value={formik.values.sH_DESCR?.toUpperCase()}
                  // {...formik.getFieldProps('sH_DESCR')}
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.sH_DESCR && formik.errors.sH_DESCR,
                    },
                    {
                      "is-valid":
                        formik.touched.sH_DESCR && !formik.errors.sH_DESCR,
                    }
                  )}
                  type="text"
                  name="sH_DESCR"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isCadreLoading ||
                    globalVariable === "infoCadre"
                  }
                  onChange={(e) => {
                    formik.setFieldValue(
                      "sH_DESCR",
                      e.target.value.toUpperCase()
                    );
                  }}
                />
                {formik.touched.sH_DESCR && formik.errors.sH_DESCR && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.sH_DESCR}</span>
                  </div>
                )}
              </div>
            </div>

            {/* {begin : Category} */}
            <div className="col-md-6">
              <div className="fv-row">
                <label className="fw-bold fs-6 mb-2">Category:</label>
                <select
                  // className="form-select form-select-solid fw-bolder"
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "is-invalid":
                        formik.touched.cadreCategory &&
                        formik.errors.cadreCategory,
                    },
                    {
                      "is-valid":
                        formik.touched.cadreCategory &&
                        !formik.errors.cadreCategory,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("cadreCategory", e.target.value);
                    setCadreCategory(e.target.value);
                  }}
                  value={cadreCategory}
                  disabled={globalVariable === "infoCadre"}
                >
                  <option value="cadreCategory">
                    {globalVariable === "editCadre"
                      ? formik.values.cadreCategory
                      : formik.values.cadreCategory}
                  </option>
                  {cadreCategorys.map((cadreCategory) => (
                    <option
                      key={cadreCategory.banD_CODE}
                      value={cadreCategory.banD_CODE}
                    >
                      {cadreCategory.descr}
                    </option>
                  ))}
                </select>
                {formik.touched.cadreCategory &&
                  formik.errors.cadreCategory && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.cadreCategory}</span>
                    </div>
                  )}
              </div>
            </div>
            {/* {end : Category} */}

            <div className="col-md-6">
              <div className="fv-row">
                <label className="fw-bold fs-6 mb-2">Status:</label>
                <select
                  // className="form-select form-select-solid fw-bolder"
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                    setStatus(e.target.value);
                  }}
                  value={status}
                  disabled={globalVariable === "infoCadre"}
                >
                  {globalVariable === "addCadre" ? (
                    <option value="">{formik.values.status}</option>
                  ) : (
                    <option value="">
                      {formik.values.status === "A"
                        ? "ACTIVE"
                        : formik.values.status === "N"
                        ? "INACTIVE"
                        : ""}
                    </option>
                  )}
                  {statuses.map((status) => (
                    <option key={status} value={status}>
                      {status}
                    </option>
                  ))}
                </select>
                {formik.touched.status && formik.errors.status && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.status}</span>
                  </div>
                )}
              </div>
            </div>

            {globalVariable === "infoCadre" && (
              <div className="col-md-6">
                <div className="fv-row">
                  <label className="fw-bold fs-6 mb-2"> Change Term</label>
                  <input
                    placeholder=""
                    {...formik.getFieldProps("chG_TERM")}
                    className={clsx(
                      "form-control form-control-solid mb-3 mb-lg-0",
                      {
                        "is-invalid":
                          formik.touched.chG_TERM && formik.errors.chG_TERM,
                      },
                      {
                        "is-valid":
                          formik.touched.chG_TERM && !formik.errors.chG_TERM,
                      }
                    )}
                    type="text"
                    name="chG_TERM"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isCadreLoading ||
                      globalVariable === "infoCadre"
                    }
                    //|| globalVariable === 'infoCadre'
                  />
                  {formik.touched.chG_TERM && formik.errors.chG_TERM && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.chG_TERM}</span>
                    </div>
                  )}
                </div>
              </div>
            )}

            {globalVariable === "infoCadre" && (
              <div className="col-md-6">
                <div className="fv-row">
                  <label className="fw-bold fs-6 mb-2"> Change User</label>
                  <input
                    placeholder=""
                    {...formik.getFieldProps("chG_USER")}
                    className={clsx(
                      "form-control form-control-solid mb-3 mb-lg-0",
                      {
                        "is-invalid":
                          formik.touched.chG_USER && formik.errors.chG_USER,
                      },
                      {
                        "is-valid":
                          formik.touched.chG_USER && !formik.errors.chG_USER,
                      }
                    )}
                    type="text"
                    name="chG_USER"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isCadreLoading ||
                      globalVariable === "infoCadre"
                    }
                    //|| globalVariable === 'infoCadre'
                  />
                  {formik.touched.chG_USER && formik.errors.chG_USER && (
                    <div className="fv-plugins-message-container">
                      <span role="alert">{formik.errors.chG_USER}</span>
                    </div>
                  )}
                </div>
              </div>
            )}

            {globalVariable === "infoCadre" && (
              <div className="col-md-6">
                <div className="fv-row">
                  <label className="fw-bold fs-6 mb-2"> Change Date</label>
                  <input
                    placeholder=""
                    {...formik.getFieldProps("chG_DATE")}
                    className={clsx(
                      "form-control form-control-solid mb-3 mb-lg-0",
                      {
                        "is-invalid":
                          formik.touched.chG_DATE && formik.errors.chG_DATE,
                      },
                      {
                        "is-valid":
                          formik.touched.chG_DATE && !formik.errors.chG_DATE,
                      }
                    )}
                    type="text"
                    name="chG_DATE"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isCadreLoading ||
                      globalVariable === "infoCadre"
                    }
                    value={
                      formik.values.chG_DATE
                        ? formatDate(formik.values?.chG_DATE.toString())
                        : ""
                    }
                    // value={
                    //   formik.values.chG_DATE
                    //     ? formik.values?.chG_DATE.toISOString()
                    //     : ""
                    // }
                    //|| globalVariable === 'infoCadre'
                  />
                  {formik.touched.chG_DATE && formik.errors.chG_DATE && (
                    <div className="fv-plugins-message-container">
                      <span role="alert">{formik.errors.chG_DATE}</span>
                    </div>
                  )}
                </div>
              </div>
            )}
          </div>
          {globalVariable !== "addCadre" && (
            <div className="row-mb-3">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Reason For Edit
                </label>
                <textarea
                  className="required form-control"
                  // value={chG_REASON}
                  // {...(globalVariable === "infoCadre"
                  //   ? { ...formik.getFieldProps("chG_REASON") }
                  //   : chG_REASON)}
                  {...(globalVariable === "infoCadre"
                    ? { ...formik.getFieldProps("chG_REASON") }
                    : { chG_REASON })}
                  //onChange={(e) => setReason(e.target.value)}
                  onChange={(e) => {
                    formik.setFieldValue(
                      "chG_REASON",
                      e.target.value.toUpperCase()
                    );
                    setchG_REASON(e.target.value.toUpperCase());
                  }}
                  rows={3}
                  {...(globalVariable === "infoCadre"
                    ? { placeholder: "" }
                    : { placeholder: "This is Mandatory Field" })}
                  //  placeholder="This is Mandatory Field"
                  disabled={globalVariable === "infoCadre"}
                ></textarea>
                {formik.touched.chG_REASON && formik.errors.chG_REASON && (
                  <div className="fv-plugins-message-container">
                    <span role="alert">{formik.errors.chG_REASON}</span>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
        <div className="text-center pt-15">
          {globalVariable !== "infoCadre" && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isCadreLoading ||
                formik.isSubmitting ||
                !formik.isValid ||
                !formik.touched ||
                (globalVariable !== "addCadre" && chG_REASON === "")
              }
              // onClick={handleFormSubmit} // Call handleFormSubmit on button click
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isCadreLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isCadreLoading) && <CadreListLoading />}
    </>
  );
};

export { CadreEditModalForm };
