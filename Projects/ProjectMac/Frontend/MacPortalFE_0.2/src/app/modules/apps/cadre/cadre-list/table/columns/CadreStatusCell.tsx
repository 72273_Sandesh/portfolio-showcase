// import {FC} from 'react'

// type Props = {
//   chG_DATE:Date
// }

// const DepartmentLastLoginCell: FC<Props> = ({chG_DATE}) => (

//   <div className='badge badge-light fw-bolder d-flex align-items-right '>{chG_DATE}</div>
// )

// export {DepartmentLastLoginCell}

import React from "react";

type Props = {
  status?: string;
};

const CadreStatusCell: React.FC<Props> = ({ status }) => {
  //   const formattedDate = new Date(chG_DATE).toLocaleString('en-GB', {
  //     day: '2-digit',
  //     month: '2-digit',
  //     year: 'numeric',
  // hour: '2-digit',
  // minute: '2-digit',
  // second: '2-digit',

  return (
    <div className=" fw-bolder d-flex align-items-right">
      <span>
        <div className=" fw-bolder d-flex align-items-right">
          {/* <div className='d-flex align-items-right'>{status === 'A' ? (<div className='badge badge-light-success fw-bolder d-flex align-items-right'>ACTIVE</div>) :(<div className='badge badge-light-danger fw-bolder d-flex align-items-right'>INACTIVE</div>) }</div> */}
          <div className="d-flex align-items-right">
            {status === "A" ? (
              <div className="badge badge-light-success fw-bolder d-flex align-items-right">
                ACTIVE
              </div>
            ) : status === "N" ? (
              <div className="badge badge-light-danger fw-bolder d-flex align-items-right">
                INACTIVE
              </div>
            ) : (
              <div className="badge d-flex align-items-right"></div>
            )}
          </div>
        </div>
      </span>
    </div>
  );
};

export { CadreStatusCell };
