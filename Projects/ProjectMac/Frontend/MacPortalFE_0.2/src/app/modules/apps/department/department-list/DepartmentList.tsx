import {ListViewProvider, useListView} from './core/ListViewProvider'
import {QueryRequestProvider} from './core/QueryRequestProvider'
import {QueryResponseProvider} from './core/QueryResponseProvider'
import {DepartmentListHeader} from './components/header/DepartmentListHeader'
import {DepartmentTable} from './table/DepartmentTable'
import {DepartmentEditModal} from './department-edit-modal/DepartmentEditModal'
import {KTCard} from '../../../../../_metronic/helpers'
import { ToolbarWrapper } from '../../../../../_metronic/layout/components/toolbar'
import { Content } from '../../../../../_metronic/layout/components/content'

const DepartmentList = () => {
  const {itemIdForUpdate} = useListView()
  return (
    <>
      <KTCard>
        <DepartmentListHeader />
        <DepartmentTable />
      </KTCard>
      {itemIdForUpdate !== undefined && <DepartmentEditModal />}
    </>
  )
}

const DepartmentListWrapper = () => (
  <QueryRequestProvider>
    <QueryResponseProvider>
      <ListViewProvider>
        <ToolbarWrapper />
        <Content>
          <DepartmentList />
        </Content>
      </ListViewProvider>
    </QueryResponseProvider>
  </QueryRequestProvider>
)

export {DepartmentListWrapper}
