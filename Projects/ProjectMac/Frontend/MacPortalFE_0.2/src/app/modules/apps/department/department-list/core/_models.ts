import {ID, Response} from '../../../../../../_metronic/helpers'
export type Department = {
  id?: ID
  deptCode?:string,
  name?:string,
  changeUser?: string,
  deptHead?: string,
  changeDate?: Date,
  shortDescription?: string,
  changeTerm?: string,
  status?: string,
  acctCode?: string,
  payTable?: string,
  cctrCode?:string,
  acctCodeAp?: string,
  cctrCodeAp?: string,
  udfNumber1?:number,
  shDeptCode?: string,
  changeReason?: string | null,
  newReason?: string | null, 
  deptHeadName?: string,
  responseStatus?: string;
  message?: string;
}

export type DepartmentQueryResponse = Response<Array<Department>>

export const initialDepartment: Department = {
  
  //sH_DESCR?: string,
  //chG_DATE?: Date,
  //chG_USER?: string,
  //chG_TERM?: string,
  //depT_HEAD?: string,
  // accT_CODE?: string,
  // paY_TABLE?: string,
  // cctR_CODE?:string,
  // sH_DESCR?: string,
  // accT_CODE__AP?: string,
  // cctR_CODE__AP?: string,
  // udF_NUM1?:number,
  // sH_DEPT_CODE?: string
}
