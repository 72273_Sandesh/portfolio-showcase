// import React, { useState, useEffect } from 'react';
// import axios from 'axios';
// import { Link } from 'react-router-dom';

// interface Department {
//   depT_CODE: number;
//   descr: string;
//   isActive: boolean;
// }

// const DeptList: React.FC = () => {
//   const [departments, setDepartments] = useState<Department[]>([]);
//   const [searchTerm, setSearchTerm] = useState<string>('');

//   useEffect(() => {
//     fetchDepartments();
//   }, []);

//   const fetchDepartments = async () => {
//     try {
//       const response = await axios.get<Department[]>(
//         'https://localhost:44360/api/Department/GetAllDepartments'
//       );
//       setDepartments(response.data);
//     } catch (error) {
//       console.error('Error fetching departments:', error);
//     }
//   };

//   const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
//     setSearchTerm(event.target.value);
//   };

//   const filteredDepartments = departments.filter(department =>
//     department.descr.toLowerCase().includes(searchTerm.toLowerCase())
//   );

//   const handleEdit = (depT_CODE: number) => {
//     // Implement edit functionality
//     console.log('Edit department with depT_CODE:', depT_CODE);
//   };

//   const handleDeactivate = (depT_CODE: number) => {
//     // Implement deactivate functionality
//     console.log('Deactivate department with depT_CODE:', depT_CODE);
//   };


 
//   return (
//     <div className="container mt-5">
//       <h1 className="mb-4">Dept List</h1>

//       <table className="mb-10">
//         <tr>
//           <td>
//             <input
//             type="text"
//             className="form-control"
//             placeholder="Search by department name"
//             value={searchTerm}
//             onChange={handleSearchChange}
//           />
//           </td>
//           <td>
//           <Link to="../NewDept">
//                 <button className="btn btn-success">Add New Department</button>
//             </Link>
//           </td>
//         </tr>
        
       
//       </table>
      
      
//       <table className="table">
//         <thead>
//           <tr>
//             <th><h3>Department Id</h3></th>
//             <th><h3>Name</h3></th>
//             <th><h3>Actions</h3></th>
//           </tr>
//         </thead>
//         <tbody>
//           {filteredDepartments.map(department => (
//             <tr key={department.depT_CODE}>
//               <td>{department.depT_CODE}</td>
//               <td>{department.descr}</td>
//               <td>
//                 <button
//                   className="btn btn-primary me-2"
//                   onClick={() => handleEdit(department.depT_CODE)}
//                 >
//                   Edit
//                 </button>
//                 <button
//                   className="btn btn-danger"
//                   onClick={() => handleDeactivate(department.depT_CODE)}
//                 >
//                   Deactivate
//                 </button>
//               </td>
//             </tr>
//           ))}
//         </tbody>
//       </table>
      
//     </div>
//   );
// };

// export default DeptList;



// DeptList.tsx
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

interface Department {
  depT_CODE: number;
  descr: string;
  isActive: boolean;
}

const DeptList: React.FC = () => {
  const [departments, setDepartments] = useState<Department[]>([]);
  const [searchTerm, setSearchTerm] = useState<string>('');

  useEffect(() => {
    fetchDepartments();
  }, []);

  const fetchDepartments = async () => {
    try {
      const response = await axios.get<Department[]>(import.meta.env.VITE_APP_THEME_API_URL);
      setDepartments(response.data);
    } catch (error) {
      console.error('Error fetching departments:', error);
    }
  };

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
  };

  const filteredDepartments = departments.filter((department) =>
    department.descr.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const handleEdit = (depT_CODE: number) => {
    // Implement edit functionality
    console.log('Edit department with depT_CODE:', depT_CODE);
  };

  const handleDeactivate = (depT_CODE: number) => {
    // Implement deactivate functionality
    console.log('Deactivate department with depT_CODE:', depT_CODE);
  };

  return (
    <div className="container mt-5">
      <h1 className="mb-4">Dept List</h1>

      <table className="mb-10">
        <tbody>
          <tr>
            <td colSpan={3}>
              <input
                type="text"
                className="form-control"
                placeholder="Search by department name"
                value={searchTerm}
                onChange={handleSearchChange}
              />
            </td>
            <td colSpan={1}>
              <Link to="../NewDept">
                <button className="btn btn-success">Add New Department</button>
              </Link>
            </td>
          </tr>
        </tbody>
      </table>

      <table className="table">
        <thead>
          <tr>
            <th>
              <h3>Department Id</h3>
            </th>
            <th>
              <h3>Name</h3>
            </th>
            <th>
              <h3>Actions</h3>
            </th>
          </tr>
        </thead>
        <tbody>
          {filteredDepartments.map((department) => (
            <tr key={department.depT_CODE}>
              <td>{department.depT_CODE}</td>
              <td>{department.descr}</td>
              <td>
                <button className="btn btn-primary me-2" onClick={() => handleEdit(department.depT_CODE)}>
                  Edit
                </button>
                <button className="btn btn-danger" onClick={() => handleDeactivate(department.depT_CODE)}>
                  Deactivate
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default DeptList;
