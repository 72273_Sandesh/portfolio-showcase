import {KTIcon} from '../../../../../../_metronic/helpers'
import { globalVariable, setGlobalVariable } from '../../GlobalVariable'
import {useListView} from '../core/ListViewProvider'

const DesignationEditModalHeader = () => {
  const {setItemIdForUpdate} = useListView()
  const openAddModal = () => {
    setItemIdForUpdate(undefined)
    setGlobalVariable('addDesign');
  }
  return (
    <div className='modal-header'>
      <div>
      {globalVariable == 'addDesign' && (<h2 className='fw-bolder'>Add New Designation</h2>)
      ||globalVariable == 'editDesign' && (<h2 className='fw-bolder'>Edit Designation</h2>)
      ||globalVariable == 'infoDesign' && (<h2 className='fw-bolder'>Designation Details</h2>)}
     
    </div>
     
      <div
        className='btn btn-icon btn-sm btn-active-icon-primary'
        data-kt-users-modal-action='close'
        onClick={openAddModal}
        style={{cursor: 'pointer'}}
      >
        <KTIcon iconName='cross' className='fs-1' />
      </div>
      
    </div>
  )
}

export {DesignationEditModalHeader}
