import axios, { AxiosResponse } from "axios";
import { ID, Response } from "../../../../../../_metronic/helpers";
import { EmployeeSearch, EmployeeSearchQueryResponse } from "./_models";

const API_URL = import.meta.env.VITE_APP_THEME_API_URL;
const EmployeeSearch_URL = `${API_URL}/employeeSearch`;
const GET_EmployeeSearch_URL = `${API_URL}/employeeSearch/query`;

const getEmployeeSearch = (query: string): Promise<EmployeeSearchQueryResponse> => {
  return axios
    .get(`${GET_EmployeeSearch_URL}?${query}`)
    .then((d: AxiosResponse<EmployeeSearchQueryResponse>) => d.data);
};

const getEmployeeSearchById = (id: ID): Promise<EmployeeSearch | undefined> => {
  return axios
    .get(`${EmployeeSearch_URL}/${id}`)
    .then((response: AxiosResponse<Response<EmployeeSearch>>) => response.data)
    .then((response: Response<EmployeeSearch>) => response.data);
};

const createEmployeeSearch = (employeeSearch: EmployeeSearch): Promise<EmployeeSearch | undefined> => {
  return axios
    .put(EmployeeSearch_URL, employeeSearch)
    .then((response: AxiosResponse<Response<EmployeeSearch>>) => response.data)
    .then((response: Response<EmployeeSearch>) => response.data);
};

const updateEmployeeSearch = (employeeSearch: EmployeeSearch): Promise<EmployeeSearch | undefined> => {
  return axios
    .post(`${EmployeeSearch_URL}/${employeeSearch.id}`, employeeSearch)
    .then((response: AxiosResponse<Response<EmployeeSearch>>) => response.data)
    .then((response: Response<EmployeeSearch>) => response.data);
};

const  deleteEmployeeSearch = (employeeSearchId: ID): Promise<void> => {
  return axios.delete(`${EmployeeSearch_URL}/${employeeSearchId}`).then(() => {});
};

const deleteSelectedEmployeeSearch = (employeeSearchIds: Array<ID>): Promise<void> => {
  const requests = employeeSearchIds.map((id) => axios.delete(`${EmployeeSearch_URL}/${id}`));
  return axios.all(requests).then(() => {});
};

export {
  getEmployeeSearch,
  deleteEmployeeSearch,
  deleteSelectedEmployeeSearch,
  getEmployeeSearchById,
  createEmployeeSearch,
  updateEmployeeSearch,
};
