import {useListView} from '../../core/ListViewProvider'
import {EmployeeListToolbar} from './EmployeeListToolbar'
import {EmployeeListGrouping} from './EmployeeListGrouping'
import {EmployeeListSearchComponent} from './EmployeeListSearchComponent'


const EmployeeListHeader = () => {
  const {selected} = useListView()
  return (
    <div className='card-header border-0 pt-6'>
      <EmployeeListSearchComponent />
      {/* begin::Card toolbar */}
      <div className='card-toolbar'>
        {/* begin::Group actions */}
        {selected.length > 0 ? <EmployeeListGrouping /> : <EmployeeListToolbar />}
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export {EmployeeListHeader}
