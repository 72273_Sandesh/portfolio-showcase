const EmployeeListLoading = () => {
  const styles = {
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    backgroundColor: "rgba(255, 255, 255, 0.8)",
    backdropFilter: "blur(0.1px)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 1000,
  };

  return (
    <div style={{ ...styles, position: "absolute", textAlign: "center" }}>
      <img
        src="/media/logos/macLoader1.1_gif.gif"
        style={{ height: "100px" }}
      />
    </div>
  );
};

export { EmployeeListLoading };
