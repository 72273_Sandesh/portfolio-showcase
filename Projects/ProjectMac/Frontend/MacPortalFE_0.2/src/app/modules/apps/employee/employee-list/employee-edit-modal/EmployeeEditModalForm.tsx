import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { isNotEmpty } from "../../../../../../_metronic/helpers";
import { initialEmployee, Employee } from "../core/_models";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { EmployeeListLoading } from "../components/loading/EmployeeListLoading";
import { createEmployee, updateEmployee } from "../core/_requests";
import { useQueryResponse } from "../core/QueryResponseProvider";
import { globalVariable } from "../../GlobalVariable";
import { toast } from "react-toastify";

type Props = {
  isEmployeeLoading: boolean;
  employee: Employee;
};

const API_URL = import.meta.env.VITE_DEPT_DROPDOWNS_API_URL;

const editEmployeeSchema = Yup.object().shape({
  emP_FNAME: Yup.string().required("Description is required"),
  shortDescription: Yup.string().required("Short Description is required"),
  changeReason: Yup.string().min(3, "Minimum 3 characters"),
  // .max(50, 'Maximum 50 characters')
  // .required('Change Reason is required'),
});

const EmployeeEditModalForm: FC<Props> = ({ employee, isEmployeeLoading }) => {
  const { setItemIdForUpdate } = useListView();
  const { refetch } = useQueryResponse();

  const [statuses] = useState(["ACTIVE", "INACTIVE"]);
  const [status, setStatus] = useState("");

  const [deptHeads, setDeptHeads] = useState([]);
  const [deptHead, setDeptHead] = useState("");

  const [newReason, setNewReason] = useState("");

  // const [salaryAccts, setSalaryAccts] = useState([]);
  // const [salaryAcct, setSalaryAcct] = useState('');

  // const [acctCodeAPs, setAcctCodeAPs] = useState([]);
  // const [acctCodeAP, setAcctCodeAP] = useState('');

  // const [payTables, setPayTables] = useState([]);
  // const [payTable, setPayTable] = useState('');

  // const [costCentres, setCostCentres] = useState([]);
  // const [costCentre, setCostCentre] = useState('');

  // const [cctrCodeAPs, setCCTRCodeAPs] = useState([]);
  // const [cctrCodeAP, setCCTRCodeAP] = useState('');

  const [ipAddress, setIpAddress] = useState<string>("");
  const { currentUser } = useAuth();

  useEffect(() => {}, []);

  const fetchFromBackend = async (combinedDataEndpoint: string) => {
    try {
      const response = await fetch(combinedDataEndpoint);
      const deptHeadData = await response.json();
      return deptHeadData;
    } catch (error) {
      throw new Error("Failed to fetch Deptartment Head from backend");
    }
  };

  const [employeeForEdit] = useState<Employee>({
    ...employee,
    emP_FNAME: employee.emP_FNAME || initialEmployee.emP_FNAME,
  });
  const fetchIpAddress = async () => {};

  fetchIpAddress();
  function formatDate(dateTime: string) {
    const date = new Date(dateTime);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata", // Specify the Indian time zone
    });
    return `${formattedDate} ${formattedTime}`;
  }

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };
  const formik = useFormik({
    initialValues: employeeForEdit,
    validationSchema: editEmployeeSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);
      try {
        if (isNotEmpty(values.id) && globalVariable === "editEmployee") {
          values.gradE_NAME = currentUser?.first_name;
          // values.changeTerm = "TERM";
          // values.changeDate = new Date();

          await updateEmployee(values);
          toast.success("Employee Edited Successfully");
          cancel(true);
        } else {
          values.id = "null";
          values.gradE_NAME = currentUser?.first_name;
          // values.changeTerm = "TERM";
          // values.changeDate = new Date();

          await createEmployee(values);
          toast.success("Employee Addeded Successfully");
          cancel(true);
        }
      } catch (ex) {
        console.error(ex);
        // toast.error(`Employee Form Submission Failed Because : ${ex}`)
        toast.error(
          <div>
            <h4 className="alert-heading">Failed to submit Form...</h4>
            <p className="alert alert-danger" role="alert">
              <h6>Failed</h6>
            </p>
          </div>
        );
      } finally {
        setSubmitting(false);
        //cancel(true)
      }
    },
  });

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div
          className="d-flex flex-column scroll-y me-n7 pe-7"
          id="kt_modal_add_user_scroll"
        >
          {globalVariable !== "addEmployee" && (
            <div className="row mb-3">
              <div className="fv-row">
                <label className=" fw-bold fs-6 mb-2">Employee Code :</label>
                <input
                  className="form-control form-control-solid mb-3 mb-lg-0"
                  value={formik.values.emP_CODE}
                  disabled={
                    globalVariable === "infoEmployee" ||
                    globalVariable === "editEmployee"
                  }
                />
              </div>
            </div>
          )}
          <div className="row mb-3">
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Employee Name
                </label>
                <input
                  placeholder="Description"
                  {...formik.getFieldProps("emP_FNAME")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "emP_FNAME",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.emP_FNAME && formik.errors.emP_FNAME,
                    },
                    {
                      "is-valid":
                        formik.touched.emP_FNAME && !formik.errors.emP_FNAME,
                    }
                  )}
                  type="emP_FNAME"
                  name="emP_FNAME"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isEmployeeLoading ||
                    globalVariable === "infoEmployee"
                  }
                />
                {formik.touched.emP_FNAME && formik.errors.emP_FNAME && (
                  <div className="fv-plugins-message-container">
                    <span role="alert">{formik.errors.emP_FNAME}</span>
                  </div>
                )}
              </div>
            </div>

            {/* data empCode shortName */}
            <div className="col-md-6">
              <div className="fv-row">
                <label className="form-label fs-6 fw-bold">Employee Head</label>
                <select
                  className="form-select form-select-solid fw-bolder"
                  //className='badge badge-light-success fw-bolder d-flex align-items-right'
                  onChange={(e) => {
                    formik.setFieldValue("deptHead", e.target.value);
                    setDeptHead(e.target.value);
                  }}
                  value={deptHead}
                  disabled={globalVariable === "infoEmployee"}
                >
                  {/* <option value="">
                    {formik.values.deptHeadName === ""
                      ? "NOT ASSIGNED"
                      : formik.values.deptHeadName}
                  </option>
                  {deptHeads.map((deptHead) => (
                    <option key={deptHead} value={deptHead.empCode}>
                      {deptHead.shortName}
                    </option>
                  ))} */}
                </select>
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="form-label fs-6 fw-bold">Status</label>
                <select
                  className="form-select form-select-solid fw-bolder"
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                    setStatus(e.target.value);
                  }}
                  value={status}
                  disabled={globalVariable === "infoEmployee"}
                >
                  {globalVariable === "addEmployee" ? (
                    <option value="">{formik.values.status}</option>
                  ) : (
                    <option value="">
                      {formik.values.status === "A" ? "ACTIVE" : "INACTIVE"}
                    </option>
                  )}
                  {statuses.map((status) => (
                    <option key={status} value={status}>
                      {status}
                    </option>
                  ))}
                </select>
              </div>
            </div>

            {globalVariable === "infoEmployee" && (
              <>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className="fw-bold fs-6 mb-2">Change User</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      value={formik.values.gradE_NAME?.toUpperCase()}
                      disabled={globalVariable === "infoEmployee"}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className="fw-bold fs-6 mb-2">Change Term</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      // value={formik.values.changeTerm}
                      //value={ipAddress}
                      disabled={globalVariable === "infoEmployee"}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className=" fw-bold fs-6 mb-2">Change Date</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      // value={formik.values.changeDate}
                      // value={
                      //   formik.values.changeDate
                      //     ? formatDate(formik.values.changeDate)
                      //     : ""
                      // }
                      disabled={globalVariable === "infoEmployee"}
                    />
                  </div>
                </div>
              </>
            )}
          </div>
          {globalVariable !== "addEmployee" && (
            <div className="row mb-3">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Change Reason
                </label>
                <textarea
                  placeholder="Change Reason"
                  //value={{...formik.getFieldProps('changeReason')}}
                  {...(globalVariable === "infoEmployee"
                    ? { ...formik.getFieldProps("changeReason") }
                    : { newReason })}
                  onChange={(e) => {
                    formik.setFieldValue(
                      "changeReason",
                      e.target.value.toUpperCase()
                    );
                    setNewReason(e.target.value.toUpperCase());
                  }}
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.changeReason &&
                        formik.errors.changeReason,
                    },
                    {
                      "is-valid":
                        formik.touched.changeReason &&
                        !formik.errors.changeReason,
                    }
                  )}
                  // type="text"
                  name="changeReason"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isEmployeeLoading ||
                    globalVariable === "infoEmployee"
                  }
                />
                {formik.touched.changeReason && formik.errors.changeReason && (
                  <div className="fv-plugins-message-container">
                    <span role="alert">{formik.errors.changeReason}</span>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
        <div className="text-center pt-15">
          {/* <button
            type='reset'
            onClick={() => cancel()}
            className='btn btn-light me-3'
            disabled={formik.isSubmitting || isEmployeeLoading}
          >
            Cancel
          </button> */}
          {globalVariable !== "infoEmployee" && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isEmployeeLoading ||
                formik.isSubmitting ||
                !formik.isValid ||
                !formik.touched ||
                (globalVariable !== "addEmployee" && newReason === "")
              }
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isEmployeeLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isEmployeeLoading) && <EmployeeListLoading />}
    </>
  );
};

export { EmployeeEditModalForm };
