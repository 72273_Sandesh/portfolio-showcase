import React from 'react';

type Props = {
  location?: string;
};

const EmployeeLocation: React.FC<Props> = ({ location }) => {


  return (
    <div className=' fw-bolder d-flex align-items-right'>
      {location ?(location.toUpperCase()): <div className='badge badge-danger fw-bolder d-flex align-items-right'>NOT ASSSIGNED</div>}
    </div>


  );
};

export { EmployeeLocation };
