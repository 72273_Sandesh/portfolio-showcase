import {Route, Routes, Outlet, Navigate} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {GradeListWrapper} from './grade-list/GradeList'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';

const GradeBreadcrumbs: Array<PageLink> = [
  {
    title: 'Master',
    path: '/apps/grade/grades',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const GradePage = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='grades'
          element={
            <>
              <PageTitle breadcrumbs={GradeBreadcrumbs}>Grade List</PageTitle>
              <GradeListWrapper />
              <ToastContainer/>
            </>
          }
        />
      </Route>
      <Route index element={<Navigate to='/apps/user-management/users' />} />
    </Routes>
  )
}

export default GradePage
