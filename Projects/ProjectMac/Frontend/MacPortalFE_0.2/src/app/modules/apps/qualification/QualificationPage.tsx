import {Route, Routes, Outlet, Navigate} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {QualificationListWrapper} from './qualification-list/QualificationList'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const QualificationBreadcrumbs: Array<PageLink> = [
  {
    title: 'Master',
    path: '/apps/qualification/qualifications',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const QualificationPage = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='qualifications'
          element={
            <>
              <PageTitle breadcrumbs={QualificationBreadcrumbs}>Qualification List</PageTitle>
              <QualificationListWrapper />
              <ToastContainer/>
            </>
          }
        />
      </Route>
      <Route index element={<Navigate to='/apps/user-management/users' />} />
    </Routes>
  )
}

export default QualificationPage
