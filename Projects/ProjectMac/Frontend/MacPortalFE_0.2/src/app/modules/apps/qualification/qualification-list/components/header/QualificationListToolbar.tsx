import React, { useState, useEffect, useRef } from "react";
import { KTIcon } from "../../../../../../../_metronic/helpers";
import { useListView } from "../../core/ListViewProvider";
import * as XLSX from "xlsx";
import QualificationTableFormat from "./QualificationTableFormat";
import { toast } from "react-toastify";

const QualificationListToolbar: React.FC = () => {
  const { setItemIdForUpdate } = useListView();
  const [exporting, setExporting] = useState<boolean>(false);
  const tableRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    return () => {
      setExporting(false);
    };
  }, []);

  const openAddQualificationModal = () => {
    setItemIdForUpdate(null);
  };
  const handleExport = () => {
    setExporting(true);
    const input = tableRef.current as HTMLTableElement;
    if (!input) {
      // toast.error(`Exporting Qualification Failed...`)
      toast.error(
        <div>
          <h6 className="alert-heading">Error</h6>
          <p className="alert alert-danger" role="alert">
            <h6>Exporting Qualification Failed...</h6>
          </p>
        </div>
      );
      console.error("Table container element not found.");
      setExporting(false);
      return;
    }
    const tableData = parseTableData(input);
    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.aoa_to_sheet(tableData);
    XLSX.utils.book_append_sheet(wb, ws, "Qualifications");
    const excelFileName = "qualifications.xlsx";
    XLSX.writeFile(wb, excelFileName);
    setExporting(false);
  };

  const parseTableData = (table: HTMLTableElement): string[][] => {
    const data: string[][] = [];

    // Include the header row
    const headerRowData: string[] = [];
    table.querySelectorAll("thead th").forEach((headerCell) => {
      headerRowData.push((headerCell as HTMLTableCellElement).innerText);
    });
    data.push(headerRowData);

    // Include the body rows
    const bodyRows = table.querySelectorAll("tbody tr");
    bodyRows.forEach((row) => {
      const rowData: string[] = [];
      row.querySelectorAll("td").forEach((cell) => {
        rowData.push((cell as HTMLTableCellElement).innerText);
      });
      data.push(rowData);
    });

    return data;
  };
  return (
    <>
      {/* <ToastContainer/> */}
      <div style={{ display: "none" }}>
        <div ref={tableRef}>
          {/*style={{...styles}}>  */}
          <QualificationTableFormat />
        </div>
      </div>
      <div
        className="d-flex justify-content-end"
        data-kt-user-table-toolbar="base"
      >
        <button
          type="button"
          className="btn btn-light-primary me-3"
          onClick={handleExport}
          disabled={exporting}
        >
          <KTIcon iconName="exit-up" className="fs-2" />
          {exporting ? "Exporting..." : "Export"}
        </button>
        <button
          type="button"
          className="btn btn-primary"
          onClick={openAddQualificationModal}
        >
          <KTIcon iconName="plus" className="fs-2" />
          Add Qualification
        </button>
      </div>
    </>
  );
};

export { QualificationListToolbar };
