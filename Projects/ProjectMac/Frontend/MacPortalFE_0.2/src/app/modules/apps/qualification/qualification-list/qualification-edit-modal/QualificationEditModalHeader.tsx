import {KTIcon} from '../../../../../../_metronic/helpers'
import { globalVariable, setGlobalVariable } from '../../GlobalVariable'
import {useListView} from '../core/ListViewProvider'

const QualificationEditModalHeader = () => {
  const {setItemIdForUpdate} = useListView()
  const openAddModal = () => {
    setItemIdForUpdate(undefined)
    setGlobalVariable("addQualification")
  }
  return (
    <div className='modal-header'>
      {/* begin::Modal title */}
      {globalVariable == 'addQualification' && (<h2 className='fw-bolder'>Add New Qualification</h2>)
     || globalVariable == 'editQualification' && (<h2 className='fw-bolder'>Edit Qualification</h2>)
      || globalVariable == 'infoQualification' && (<h2 className='fw-bolder'>Qualification Details</h2> )}
      {/* end::Modal title */}

      {/* begin::Close */}
      <div
        className='btn btn-icon btn-sm btn-active-icon-primary'
        data-kt-users-modal-action='close'
        onClick={openAddModal}
        style={{cursor: 'pointer'}}
      >
        <KTIcon iconName='cross' className='fs-1' />
      </div>
      {/* end::Close */}
    </div>
  )
}

export {QualificationEditModalHeader}
