import React from 'react';

type Props = {
  shortdescr?: string;
};

const QualificationShortDescription: React.FC<Props> = ({ shortdescr }) => {


  return (
    <div className=' fw-bolder d-flex align-items-right'>
      {shortdescr ?(shortdescr.toUpperCase()): <div className='badge badge-danger fw-bolder d-flex align-items-right'>NOT ASSSIGNED</div>}
    </div>


  );
};

export { QualificationShortDescription };
