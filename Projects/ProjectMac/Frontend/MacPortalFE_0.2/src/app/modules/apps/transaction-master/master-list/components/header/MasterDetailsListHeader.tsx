import {useListView} from '../../core/ListViewProvider'
import { MasterDetailsListGrouping } from './MasterDetailsListGrouping'
import { MasterDetailsListSearchComponent } from './MasterDetailsListSearchComponent'
import { MasterDetailsListToolbar } from './MasterDetailsListToolbar'

const MasterDetailsListHeader = () => {
  const {selected} = useListView()
  return (
    <div className='card-header border-0 pt-6'>
      <MasterDetailsListSearchComponent />
      {/* begin::Card toolbar */}
      <div className='card-toolbar'>
        {/* begin::Group actions */}
        {selected.length > 0 ? <MasterDetailsListGrouping /> : <MasterDetailsListToolbar />}
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export {MasterDetailsListHeader}
