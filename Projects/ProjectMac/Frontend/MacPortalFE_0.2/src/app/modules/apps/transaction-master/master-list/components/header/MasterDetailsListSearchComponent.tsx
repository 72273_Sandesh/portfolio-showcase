import { useEffect, useState } from "react";
import {
  initialQueryState,
  KTIcon,
  useDebounce,
} from "../../../../../../../_metronic/helpers";
import { useQueryRequest } from "../../core/QueryRequestProvider";
import { toast } from "react-toastify";
import axios from "axios";
import { useQueryResponse } from "../../core/QueryResponseProvider";
import { useAuth } from "../../../../../auth";

const filterAPI_URL = import.meta.env.VITE_EMP_FILTER_DROPDOWNS_API_URL;

type dropdownMenus = {
  sitE_CODE: string;
  descr: string;
  deptCode: string;
  description: string;
};
const MasterDetailsListSearchComponent = () => {
  const { updateState } = useQueryRequest();
  const [searchTerm, setSearchTerm] = useState<string>("");
  const debouncedSearchTerm = useDebounce(searchTerm, 150);
  const [category, setCategory] = useState("");
  const [categories, setCategories] = useState([
    { category_code: 'cat1', descr: 'Cadre' },
    { category_code: 'cat2', descr: 'Department' },
    { category_code: 'cat1', descr: 'Designation' },
    { category_code: 'cat2', descr: 'Grade' },
    { category_code: 'cat1', descr: 'Qualification' },
    
    // Add more categories as needed
  ]);
  const { isLoading } = useQueryResponse();

  const [period, setPeriod] = useState("");
  const [periods, setPeriods] = useState<dropdownMenus[]>([]);
  
  const { currentUser } = useAuth();

  useEffect(() => {
    if (debouncedSearchTerm !== undefined && searchTerm !== undefined) {
      updateState({ search: debouncedSearchTerm, ...initialQueryState });
    }

    axios
      .get(`${filterAPI_URL}?empCode=${currentUser?.id}`)
      .then((response) => {
        setPeriods(response.data.location);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, [debouncedSearchTerm]);

  const filterData = () => {
    try {
      if (period !== "" || category!== "") {
        updateState({
          filter: { period,category },
          ...initialQueryState,
        });
      }
    } catch (ex) {
      console.error(ex);
      toast.error(
        <div>
          <h4 className="alert-heading">oops...</h4>
          <p className="alert alert-danger" role="alert">
            <h6>Failed</h6>
          </p>
        </div>
      );
    }
  };
  return (
    <div className="card-title">
      <div className="d-flex flex-column">
        <div className="d-lg-flex align-lg-items-center">
          <form
            data-kt-user-table-filter="form"
            className="rounded d-flex flex-column flex-lg-row align-items-lg-center bg-light-primary p-1  h-lg-50px "
          >
           <div className="row flex-grow-1 mb-5 mb-lg-0">
  <div className="col-lg-5 d-flex align-items-center mb-3 mb-lg-0">
    <KTIcon iconName="magnifier" className="fs-1 text-gray-500 me-1 ms-3" />
    <input
      type="text"
      className="form-control form-control-flush flex-grow-1"
      name="search"
      value={searchTerm}
      onChange={(e) => setSearchTerm(e.target.value)}
      placeholder="Your Search"
    />
  </div>

  <div className="col-lg-7 d-flex align-items-center">
    <div className="bullet text-gray-800 d-none d-lg-block h-30px w-2px"></div>
    <KTIcon iconName="calendar-8" className="fs-1 text-gray-500 me-1 ms-5" />
    <select
      className="form-select bg-light-primary border-0 flex-grow-1 me-2"
      data-kt-select2="true"
      data-placeholder="period"
      data-allow-clear="true"
      data-kt-user-table-filter="period"
      data-hide-search="true"
      value={period || ""}
      onChange={(e) => setPeriod(e.target.value)}
      aria-placeholder="12345"
    >
      <option value="">All TimeSpan</option>
      {periods.map((period) => (
        <option
          className="dropdown-item bg-light border-0"
          key={period.sitE_CODE}
          value={period.sitE_CODE}
        >
          {period.descr}
        </option>
      ))}
    </select>
    <div className="bullet text-gray-800 d-none d-lg-block h-30px w-2px"></div>
    <KTIcon iconName="filter" className="fs-1 text-gray-500 me-1 ms-5" />
    <select
      className="form-select bg-light-primary border-0 flex-grow-1"
      data-kt-select2="true"
      data-placeholder="category"
      data-allow-clear="true"
      data-kt-user-table-filter="category"
      data-hide-search="true"
      value={category || ""}
      onChange={(e) => setCategory(e.target.value)}
    >
      <option value="">All Masters</option>
      {categories.map((cat) => (
        <option
          className="dropdown-item bg-light border-0"
          key={cat.category_code}
          value={cat.category_code}
        >
          {cat.descr}
        </option>
      ))}
    </select>
  </div>
</div>


            <div className="d-flex ms-2">
              <button
                disabled={isLoading}
                type="button"
                onClick={filterData}
                className="btn btn-dark fw-bold px-6"
                data-kt-menu-dismiss="true"
                data-kt-user-table-filter="filter"
              >
                Apply
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export { MasterDetailsListSearchComponent };
