import { ID, Response } from "../../../../../../_metronic/helpers";
export type MasterDetails = {
  id?: ID;
  deptCode?: string;
  name?: string;
  changeUser?: string;
  deptHead?: string;
  changeDate?: Date;
  shortDescription?: string;
  changeTerm?: string;
  status?: string;
  acctCode?: string;
  payTable?: string;
  cctrCode?: string;
  acctCodeAp?: string;
  cctrCodeAp?: string;
  udfNumber1?: number;
  shDeptCode?: string;
  changeReason?: string;
  newReason?: string | null;
  deptHeadName?: string;
  responseStatus?: string;
  message?: string;

  emP_NAME?: string;
  emP_CODE?: string;
  sitE_NAME?: string;
  depT_NAME?: string;
  designation?: string;
  datE_JOIN?: string;
  paymenT_MODE?: string;
  banK_NAME?: string;
  banK_ACCT?: string;
  banK_IFSC?: string;
  banK_CODE?: string;

  // emP_CODE?: string,
  emP_FNAME?: string;
  emP_MNAME?: string;
  emP_LNAME?: string;
  // designation?: string,
  dob?: Date;
  // datE_JOIN?: Date,
  basic?: number;
  gross?: number;
  desigN_CODE?: string;
  emP_SITE?: string;
  depT_CODE?: string;
  grade?: string;
  cadre?: string;
  chG_DATE?: Date;
  chG_USER?: string;
  chG_TERM?: string;
  location?: string;
  department?: string;
  // status?: string
  // changeReason?:string

  // emP_NAME?: string,
  birtH_DATE?: Date;
  shorT_NAME?: string;
  gradE_NAME?: string;
  // sitE_NAME?: string,
  // depT_NAME?: string,
  resI_DATE?: Date;
  relievE_DATE?: Date;
  qual?: string;
  toT_EXP?: string;
  maC_EXP?: string;
  mobilE_NO?: string;
  mobilE_NO_OFF?: string;
  employmenT_TYPE?: string;
  gender?: string;
  gendeR_DESCR?: string;
  emP_TYPE?: string;
  emP_IMAGE?: string;
  contacT_CODE?: string;
  namE_PREFIX?: string;
  m_STATUS_DATE?: Date;
  m_STATUS?: string;
  m_STATUS_DESCR?: string;
  contacT_CITY?: string;
  contacT_STATE?: string;
  contacT_PIN?: string;
  religion?: string;
  iT_NO?: string;
  emaiL_ID_OFF?: string;
  peR_ADD1?: string;
  peR_ADD2?: string;
  peR_ADD3?: string;
  cuR_ADD1?: string;
  cuR_ADD2?: string;
  cuR_PIN?: string;
  cuR_ADD3?: string;
  cuR_TEL1?: string;
  cuR_CITY?: string;
  cuR_TEL2?: string;
  cuR_TEL3?: string;
  emaiL_ID_PER?: string;
  contacT_TEL?: string;
  contacT_PERS?: string;
  blooD_GRP?: string;
  section?: string;
  probatioN_PRD?: string;
  traininG_PRD?: string;
  probatioN_DATE?: Date;
  datE_CONF?: string;
  probation?: string;
  confirmed?: string;
  // banK_ACCT?: string,
  // banK_CODE?: string,
  // banK_NAME?: string,
  // banK_IFSC?: string,
  contacT_ADD1?: string;
  contacT_ADD2?: string;
  contacT_ADD3?: string;
  contacT_CITY_1?: string;
  peR_PIN?: string;
  contacT_STATE_1?: string;
  peR_TEL?: string;
  paY_MODE?: string;
  // paymenT_MODE?: string,
  cuR_STATE?: string;
  peR_STATE?: string;
  peR_CITY?: string;
  crediT_CARD_NO?: string;
  pF_UN_NO?: string;
  aadhaaR_NO?: string;
  driV_LIC_NO?: string;
  electioN_CARD_NO?: string;
  iT_NO_1?: string;
  fatheR_NAME?: string;
  motheR_NAME?: string;
  passporT_NO?: string;
  passporT_ISS_PLACE?: string;
  passporT_ISS_DATE?: Date;
  passporT_EXP_DATE?: Date;
  cuR_STATE_DESCR?: string;
  peR_STATE_DESCR?: string;
  contacT_STATE_DESCR?: string;
  proB_EXT?: string;
  conF_EXT?: string;
  curR_CTC?: string;
  joininG_CTC?: string;
};

export type MasterDetailsQueryResponse = Response<Array<MasterDetails>>;

export const initialMasterDetails: MasterDetails = {};
