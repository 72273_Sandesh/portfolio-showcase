import axios, { AxiosResponse } from "axios";
import { ID, Response } from "../../../../../../_metronic/helpers";
import { MasterDetails, MasterDetailsQueryResponse } from "./_models";

const MASTERDETAILS_URL = import.meta.env.VITE_DEPT_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const getMasterDetails = (
  query: string
): Promise<MasterDetailsQueryResponse> => {
  return axios
    .get(`${EMPLOYEE_URL}/GetAllEmployees?${query}`)
    .then((d: AxiosResponse<MasterDetailsQueryResponse>) => d.data);
};

const getMasterDetailsById = (id: ID): Promise<MasterDetails | undefined> => {
  return axios
    .get(`${EMPLOYEE_URL}/${id}/EMPPER`)
    .then((response: AxiosResponse<Response<MasterDetails>>) => response.data)
    .then((data: Response<MasterDetails>) => {
      return data[0];
    });
};

const createMasterDetails = (
  bankDetails: MasterDetails
): Promise<MasterDetails | undefined> => {
  return axios
    .post(`${MASTERDETAILS_URL}/InsertDepartment`, bankDetails)
    .then((response: AxiosResponse<Response<MasterDetails>>) => response.data)
    .then((response: Response<MasterDetails>) => response.data);
};

const updateMasterDetails = (
  bankDetails: MasterDetails
): Promise<MasterDetails | undefined> => {
  return axios
    .put(
      `${MASTERDETAILS_URL}/UpdateDepartment/${bankDetails.emP_CODE}`,
      bankDetails
    )
    .then((response: AxiosResponse<Response<MasterDetails>>) => response.data)
    .then((response: Response<MasterDetails>) => response.data);
};

const deleteMasterDetails = (bankDetailsId: ID): Promise<void> => {
  return axios.delete(`${MASTERDETAILS_URL}/${bankDetailsId}`).then(() => {});
};

const deleteSelectedMasterDetails = (
  bankDetailsIds: Array<ID>
): Promise<void> => {
  const requests = bankDetailsIds.map((id) =>
    axios.delete(`${MASTERDETAILS_URL}/${id}`)
  );
  return axios.all(requests).then(() => {});
};

export {
  getMasterDetails,
  deleteMasterDetails,
  deleteSelectedMasterDetails,
  getMasterDetailsById,
  createMasterDetails,
  updateMasterDetails,
};
