import {useQuery} from 'react-query'
import {isNotEmpty, QUERIES} from '../../../../../../_metronic/helpers'
import {useListView} from '../core/ListViewProvider'
import { getMasterDetailsById } from '../core/_requests'
import { MasterDetailsEditModalForm } from './MasterDetailsEditModalForm'

const MasterDetailsEditModalFromWrapper = () => {
  const {itemIdForUpdate, setItemIdForUpdate} = useListView()
  const enabledQuery: boolean = isNotEmpty(itemIdForUpdate)
  const {
    isLoading,
    data: bankDetails,
    error,
  } = useQuery(
    `${QUERIES.MASTER_DETAILS_LIST}-masterDetails-${itemIdForUpdate}`,
    () => {
      return getMasterDetailsById(itemIdForUpdate)
    },
    {
      cacheTime: 0,
      enabled: enabledQuery,
      onError: (err) => {
        setItemIdForUpdate(undefined)
        console.error(err)
      },
    }
  )

  if (!itemIdForUpdate) {
    return <MasterDetailsEditModalForm isBankDetailsLoading={isLoading} bankDetails={{id: undefined}} />
  }

  if (!isLoading && !error && bankDetails) {
    return <MasterDetailsEditModalForm isBankDetailsLoading={isLoading} bankDetails={bankDetails} />
  }

  return null
}

export {MasterDetailsEditModalFromWrapper}
