import { useEffect, useState } from "react";
import { KTIcon } from "../../../../../../_metronic/helpers";
import { globalVariable, setGlobalVariable } from "../../GlobalVariable";
import { useListView } from "../core/ListViewProvider";

const MasterDetailsEditModalHeader = () => {
  const [headerName, setHeaderName] = useState("");

  useEffect(() => {
    const getHeaderName = () => {
      if (globalVariable === "addMasterDetails") return "Add New Master Details";
      else if (globalVariable === "editMasterDetails") return "Edit Master Details";
      else return "View Master Details";
    };
    
    setHeaderName(getHeaderName);
  }, []);

  

  const { setItemIdForUpdate } = useListView();
  const openAddModal = () => {
    setItemIdForUpdate(undefined);
    setGlobalVariable("addMasterDetails");
  };
  return (
    <div className="modal-header">
      {/* begin::Modal title */}
        <h2 className="fw-bolder">{headerName}</h2>
      
      {/* end::Modal title */}

      {/* begin::Close */}
      <div
        className="btn btn-icon btn-sm btn-active-icon-primary"
        data-kt-users-modal-action="close"
        onClick={openAddModal}
        style={{ cursor: "pointer" }}
      >
        <KTIcon iconName="cross" className="fs-1" />
      </div>
      {/* end::Close */}
    </div>
  );
};

export { MasterDetailsEditModalHeader };
