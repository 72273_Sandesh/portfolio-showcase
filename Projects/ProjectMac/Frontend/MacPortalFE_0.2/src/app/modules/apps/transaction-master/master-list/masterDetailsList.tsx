import { KTCard } from "../../../../../_metronic/helpers"
import { Content } from "../../../../../_metronic/layout/components/content"
import { ToolbarWrapper } from "../../../../../_metronic/layout/components/toolbar"
import { MasterDetailsListHeader } from "./components/header/MasterDetailsListHeader"
import { ListViewProvider, useListView } from "./core/ListViewProvider"
import { QueryRequestProvider } from "./core/QueryRequestProvider"
import { QueryResponseProvider } from "./core/QueryResponseProvider"
import { MasterDetailsEditModal } from "./masterDetails-edit-modal/MasterDetailsEditModal"
import { MasterDetailsTable } from "./table/MasterDetailsTable"

const MasterDetailsList = () => {
  const {itemIdForUpdate} = useListView()
  return (
    <>
      <KTCard>
        <MasterDetailsListHeader />
        <MasterDetailsTable />
      </KTCard>
      {itemIdForUpdate !== undefined && <MasterDetailsEditModal />}
    </>
  )
}

const MasterDetailsListWrapper = () => (
  <QueryRequestProvider>
    <QueryResponseProvider>
      <ListViewProvider>
        <ToolbarWrapper />
        <Content>
          <MasterDetailsList />
        </Content>
      </ListViewProvider>
    </QueryResponseProvider>
  </QueryRequestProvider>
)

export {MasterDetailsListWrapper}
