import {FC} from 'react'

type Props = {
  changeUser:string | undefined
}

const MasterDetailsChangeUser: FC<Props> = ({changeUser}) => (
  <>
   <div className=' fw-bolder d-flex align-items-right'>
      {changeUser ?(changeUser.toUpperCase()): <div className='badge badge-danger fw-bolder d-flex align-items-right'>NOT ASSIGNED</div>}
    </div>
    </> 
)

export {MasterDetailsChangeUser}