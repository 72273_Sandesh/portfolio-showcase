import { Column } from "react-table";
import { MasterDetails } from "../../core/_models";
import { MasterDetailsCustomHeader } from "./MasterDetailsCustomHeader";
import { MasterDetailsInfoCell } from "./MasterDetailsInfoCell";
import { MasterDetailsChangeUser } from "./MasterDetailsChangeUser";
import { MasterDetailsLastLoginCell } from "./MasterDetailsLastLoginCell";
import { MasterDetailsTwoStepsCell } from "./MasterDetailsTwoStepsCell";
import { MasterDetailsActionsCell } from "./MasterDetailsActionsCell";
const MasterDetailsColumns: ReadonlyArray<Column<MasterDetails>> = [
  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="Master NAME"
        className="min-w-125px text-left "
      />
    ),
    id: "name",
    Cell: ({ ...props }) => (
      <MasterDetailsInfoCell bankDetails={props.data[props.row.index]} id={props.data[props.row.index].id} />
    ),
  },
    {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="TRANSACTION ID"
        className="min-w-125px text-left"
      />
    ),
    accessor: "changeUser",
    Cell: ({ ...props }) => (
      <MasterDetailsChangeUser
        changeUser={props.data[props.row.index].changeUser}
      />
    ),
  },
  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="TRANSACTION DATE"
        className="min-w-125px text-left"
      />
    ),
    id: "changeDate",
    Cell: ({ ...props }) => (
      <MasterDetailsLastLoginCell
        changeDate={props.data[props.row.index].changeDate ?? new Date()}
      />
    ),
  },
  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="STATUS"
        className="min-w-125px text-left"
      />
    ),
    accessor: "status",
    Cell: ({ value }) => (
      <span>
        <div className=" fw-bolder d-flex align-items-right">
          <div className="d-flex align-items-right">
            {
              <div className="d-flex align-items-right">
                {value === "A" ? (
                  <div className="badge badge-light-success fw-bolder d-flex align-items-right">
                    ACTIVE
                  </div>
                ) : value === "N" ? (
                  <div className="badge badge-light-danger fw-bolder d-flex align-items-right">
                    INACTIVE
                  </div>
                ) : (
                  <div className="badge d-flex align-items-right">&nbsp;</div>
                )}
              </div>
            }
          </div>
        </div>
      </span>
    ),
  },
  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="WF_PROCESS_LEVEL"
        className="min-w-125px text-left"
      />
    ),
    id: "deptHead",
    Cell: ({ ...props }) => (
      <MasterDetailsTwoStepsCell
        deptHead={props.data[props.row.index].deptHead}
      />
    ),
  },
  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="VISIBLITY"
        className="min-w-125px text-left"
      />
    ),
    id: "changeDate1",
    Cell: ({ ...props }) => (
      <MasterDetailsLastLoginCell
        changeDate={props.data[props.row.index].changeDate ?? new Date()}
      />
    ),
  },
  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="STATE"
        className="min-w-125px text-left"
      />
    ),
    id: "changeDate2",
    Cell: ({ ...props }) => (
      <MasterDetailsLastLoginCell
        changeDate={props.data[props.row.index].changeDate ?? new Date()}
      />
    ),
  },
  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="Actions"
        className="min-w-100px text-left"
      />
    ),
    id: "actions",
    Cell: ({ ...props }) => (
      <MasterDetailsActionsCell id={props.data[props.row.index].id} />
    ),
  },
];

export { MasterDetailsColumns };