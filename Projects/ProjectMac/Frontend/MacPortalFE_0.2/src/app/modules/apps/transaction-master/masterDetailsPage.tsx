import {Route, Routes, Outlet, Navigate} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { MasterDetailsListWrapper } from './master-list/masterDetailsList';

const MasterDetailsBreadcrumbs: Array<PageLink> = [
  {
    title: 'Master',
    path: '/apps/masterDetails/masterDetails',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const MasterDetailsPage = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='masterDetails'
          element={
            <>
              <PageTitle breadcrumbs={MasterDetailsBreadcrumbs}>Master Trasnsaction List</PageTitle>
              <MasterDetailsListWrapper />
              <ToastContainer/>
            </>
          }
        />
      </Route>
      <Route index element={<Navigate to='/apps/bank-details/users' />} />
    </Routes>
  )
}

export default MasterDetailsPage
