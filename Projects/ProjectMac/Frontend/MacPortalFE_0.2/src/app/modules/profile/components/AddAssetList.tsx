import { Content } from "../../../../_metronic/layout/components/content";
import AssetListAdd from "./AssetListAdd";
import {  } from "./LanguageDetailAdd";

export function AddAssetList() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <AssetListAdd />
      </div>
    </Content>
  );
}
