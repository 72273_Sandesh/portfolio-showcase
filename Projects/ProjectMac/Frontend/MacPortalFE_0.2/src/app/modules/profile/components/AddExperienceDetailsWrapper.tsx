import { Content } from "../../../../_metronic/layout/components/content";
import AddExperienceDetails from "./AddExperienceDetails";

export function AddExperienceDetailsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <AddExperienceDetails/>
      </div>
    </Content>
  );
}
