import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView, fMIdForUpdate } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
interface AssetDropdownItem {
  inducT_CODE: string;
  descr: string;
}
const AssetListEdit: React.FC = () => {
  const navigate = useNavigate();
  const { currentUser } = useAuth();
  const [drpdwnAssets, setDrpdwnAssets] = useState<AssetDropdownItem[]>([]);

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/ASSETS`)
      .then((response) => {
        formik.setValues({
          inducT_CODE: response.data[fMIdForUpdate].inducT_CODE,
          induction: response.data[fMIdForUpdate].induction,
          datE_FORMALITY: response.data[fMIdForUpdate].datE_FORMALITY,
          remarks: response.data[fMIdForUpdate].remarks,
          status: response.data[fMIdForUpdate].status,
          statuS_DATE: response.data[fMIdForUpdate].statuS_DATE,
          toT_AMT: response.data[fMIdForUpdate].toT_AMT,
          adJ_AMT: response.data[fMIdForUpdate].adJ_AMT,
          traN_ID: response.data[fMIdForUpdate].traN_ID,
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });

    axios
      .get(`${EMPLOYEE_URL}/ddl_GetAsstes`)
      .then((response) => {
        setDrpdwnAssets(response.data.assets);
      })
      .catch((error) => {
        console.error("Error fetching asset details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    induction: Yup.string().required("induction is required"),
    datE_FORMALITY: Yup.string().required("Member name is required"),
    remarks: Yup.string().required("Member name is required"),
    status: Yup.string().required("Member name is required"),
    statuS_DATE: Yup.string().required("Member name is required"),
    toT_AMT: Yup.string().required("Member name is required"),
    adJ_AMT: Yup.string().required("Member name is required"),
  });

  const formik = useFormik({
    initialValues: {
      induction: "",
      inducT_CODE:"",
      datE_FORMALITY: "",
      remarks: "",
      status: "",
      statuS_DATE: "",
      toT_AMT: "",
      adJ_AMT: "",
      traN_ID: "",
    },
    validationSchema: validationSchema,

    onSubmit: (values) => {
      console.log("Submitting form data:", values);

      axios
        .put(`${EMPLOYEE_URL}/UpdateAssets?empCode=${employeeIdForView}
          &inductCode=${values.inducT_CODE}&tranId=${values.traN_ID}`, {
          ...values,
          chG_DATE: new Date(),
          chG_USER: currentUser?.id,
          chG_TERM: "TERM",
        })
        .then((response) => {
          const { responseStatus, message } = response.data;
          console.log("Post successful!", response.data);
          if (responseStatus === "E") {
            toast.error(                  
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
            formik.resetForm();
            navigate(-1); 
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );
        });
    },
  });

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Assets List</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="induction"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  ASSET NAME :
                </label>
                <input
                  type="text"
                  id="induction"
                  className="form-control form-control-lg form-control-solid "
                  name="induction"
                  placeholder="Enter induction Name"
                  value={formik.values?.induction}
                  onChange={formik.handleChange}
                  readOnly
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="relation"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  STATUS :
                </label>
                <select
                  className="form-select form-select-lg form-select-solid"
                  name="status"
                  value={formik.values.status}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value={formik.values.status}>
                  {formik.values.status}
                  </option>
                  <option value="C">COMPLETED</option>
                  <option value="P">PENDING</option>
                </select>
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.induction && formik.errors.induction ? (
                    <div className="text-danger">{formik.errors.induction}</div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.status && formik.errors.status ? (
                    <div className="text-danger">{formik.errors.status}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="datE_FORMALITY"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  EFFECTION DATE :
                </label>
                <input
                  type="date"
                  id="datE_FORMALITY"
                  className="form-control form-control-lg form-control-solid "
                  name="datE_FORMALITY"
                  placeholder="Select date of formality"
                  value={
                    formik.values?.datE_FORMALITY
                      ? new Date(formik.values?.datE_FORMALITY)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="statuS_DATE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CONFIRMATION DATE:
                </label>
                <input
                  type="date"
                  id="statuS_DATE"
                  className="form-control form-control-lg form-control-solid "
                  name="statuS_DATE"
                  placeholder="Select date of status"
                  value={
                    formik.values?.statuS_DATE
                      ? new Date(formik.values?.statuS_DATE)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                />
              </div>

              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.datE_FORMALITY &&
                  formik.errors.datE_FORMALITY ? (
                    <div className="text-danger">
                      {formik.errors.datE_FORMALITY}
                    </div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.statuS_DATE && formik.errors.statuS_DATE ? (
                    <div className="text-danger">
                      {formik.errors.statuS_DATE}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="adJ_AMT"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  ADJUST AMOUNT:
                </label>
                <input
                  type="number"
                  id="adJ_AMT"
                  className="form-control form-control-lg form-control-solid "
                  name="adJ_AMT"
                  placeholder="Enter adJ_AMT"
                  value={formik.values?.adJ_AMT}
                  onChange={formik.handleChange}
                  min={0}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="toT_AMT"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  TOTAL AMOUNT:
                </label>
                <input
                  type="number"
                  id="toT_AMT"
                  className="form-control form-control-lg form-control-solid "
                  name="toT_AMT"
                  placeholder="Enter toT_AMT Name"
                  value={formik.values?.toT_AMT}
                  onChange={formik.handleChange}
                  min={0}
                />
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.adJ_AMT && formik.errors.adJ_AMT ? (
                    <div className="text-danger">{formik.errors.adJ_AMT}</div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.toT_AMT && formik.errors.toT_AMT ? (
                    <div className="text-danger">{formik.errors.toT_AMT}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className=" d-flex align-items-center mb-1">
                <label
                  htmlFor="remarks"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  REMARK:
                </label>
                <textarea
                  className="form-control form-control-lg form-control-solid"
                  rows={3}
                  name="remarks"
                  placeholder="Enter here remark about asset"
                  value={formik.values?.remarks}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.remarks && formik.errors.remarks ? (
                  <div className="text-danger">{formik.errors.remarks}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={//formik.isSubmitting || 
                !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AssetListEdit;
