import { useEffect, useState } from "react";
import { Content } from "../../../../_metronic/layout/components/content";
import { Card4 } from "../../../../_metronic/partials/content/cards/Card4";
import axios from "axios";
import { employeeIdForView } from "../../GlobalQuery";

export function Attachments() {
  const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

  const [numOfData, setNumOfData] = useState([0, 0, 0, 0, 0]);

  const fetchData = async (attachMode : string) => {
    try {
      const response = await axios.get(`${EMPLOYEE_URL}/Attachment/${employeeIdForView}?strCndCode=CD00031458&attachMode=${attachMode}`);
      const data = response.data;
      return Array.isArray(data) ? data.length : 0;
    } catch (error) {
      console.error("Error fetching asset details:", error);
      return 0;
    }
  }

  useEffect(() => {
    const fetchAllData = async () => {
      const numCAND = await fetchData('CAND');
      const numOFFERAPPR = await fetchData('OFFERAPPR');
      const numEMPL = await fetchData('EMPL');
      const numRESI = await fetchData('RESI');
      const numEIS = await fetchData('EIS');
      setNumOfData([numCAND, numOFFERAPPR, numEMPL, numRESI, numEIS]);
    };

    fetchAllData();
  },[]);

  return (
    <Content>
      <div className="row g-6 g-xl-9 mb-6 mb-xl-9">
        <div className="col-12 col-sm-12 col-xl">
          <Card4
            icon='media/svg/files/folder-document.svg'
            title='Recruitment'
            description={`${numOfData[0]} files`}
            path='/crafted/pages/profile/RecruitmentDocsWrapper'
          />
        </div>
        <div className="col-12 col-sm-12 col-xl">
          <Card4
            icon='media/svg/files/folder-document.svg'
            title='Before Joining'
            description={`${numOfData[1]} files`} 
            path='/crafted/pages/profile/PreJoiningDocsWrapper'
          />
        </div>
        <div className="col-12 col-sm-12 col-xl">
          <Card4
            icon='media/svg/files/folder-document.svg'
            title='After Joining'
            description={`${numOfData[2]} files`} 
            path='/crafted/pages/profile/PostJoiningDocsWrapper'
          />
        </div>
        <div className="col-12 col-sm-12 col-xl">
          <Card4
            icon='media/svg/files/folder-document.svg'
            title='After Resignation'
            description={`${numOfData[3]} files`} 
            path='/crafted/pages/profile/PostResignDocsWrapper'
          />
        </div>
        <div className="col-12 col-sm-12 col-xl">
          <Card4
            icon='media/svg/files/folder-document.svg'
            title='EIS Documents'
            description={`${numOfData[4]} files`}
            path='/crafted/pages/profile/EISDocsWrapper'
          />
        </div>
      </div>
    </Content>
  );
}
