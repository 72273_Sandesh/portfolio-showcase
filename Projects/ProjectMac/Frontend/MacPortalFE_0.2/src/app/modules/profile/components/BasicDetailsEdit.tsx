import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { KTIcon } from "../../../../_metronic/helpers";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const BasicDetailsEdit: React.FC = () => {
  const [employee, setEmployee] = useState<any>({});
  const navigate = useNavigate();
  const [image, setImage] = useState<File>();
  const [MaritalStatus, setMaritalStatus] = useState([]);
  const [pdf, setPdf] = useState<string>("");
  const { currentUser } = useAuth();

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setEmployee(response.data[0]);
        setImage(response.data[0].emP_IMAGE || "/media/avatars/User.png");
        formik.setValues({
          emP_CODE: response.data[0].emP_CODE,
          emP_NAME: response.data[0].emP_NAME,
          emP_FNAME: response.data[0].emP_FNAME,
          birtH_DATE: response.data[0].birtH_DATE,
          emP_LNAME: response.data[0].emP_LNAME,
          emP_MNAME: response.data[0].emP_MNAME,
          shorT_NAME: response.data[0].shorT_NAME,
          sex: response.data[0].sex,
          religion: response.data[0].religion,
          m_STATUS_DESCR: response.data[0].m_STATUS_DESCR,
          m_STATUS_DATE: response.data[0].m_STATUS_DATE,
          blooD_GRP: response.data[0].blooD_GRP,
          fatheR_NAME: response.data[0].fatheR_NAME,
          motheR_NAME: response.data[0].motheR_NAME,
          emaiL_ID_PER: response.data[0].emaiL_ID_PER,
          mobilE_NO: response.data[0].mobilE_NO,
          pdf: "",
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });

    axios
      .get(`${EMPLOYEE_URL}/ddl_GetMaritalStatus`)
      .then((response) => {
        setMaritalStatus(response.data.marital_status);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setImage(e.target.files?.[0]);
    const file = e.target.files?.[0];
    if (file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        const base64String = reader.result as string;
        setImage(base64String);
      };
      reader.readAsDataURL(file);
    }
  };

  const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (file) {
      const reader = new FileReader();
      
      reader.onloadend = () => {
        const base64String = reader.result;
        // Now you have the Base64 string of the PDF file
        console.log(base64String);
        // You can set the base64String to your state or perform other operations
        setPdf(base64String);
      };
      
      reader.readAsDataURL(file);
    }
  };
  


  const validationSchema = Yup.object().shape({
    emP_NAME: Yup.string().required("Employee Full Name is required"),
    emP_FNAME: Yup.string().required("Employee First Name is required"),
    emP_LNAME: Yup.string().required("Employee Last Name is required"),
    sex: Yup.string().required("Gender is required"),
    birtH_DATE: Yup.date().required("Date of Birth is required"),
    religion: Yup.string().required("Religion is required"),
    emaiL_ID_PER: Yup.string().required("Personal Email is required"),
    mobilE_NO: Yup.string().required("Personal Mobile No is required"),
  });

  const formik = useFormik({
    initialValues: {
      emP_CODE: "",
      emP_NAME: "",
      emP_FNAME: "",
      emP_LNAME: "",
      emP_MNAME: "",
      shorT_NAME: "",
      sex: "",
      birtH_DATE: "",
      religion: "",
      m_STATUS_DESCR: "",
      m_STATUS_DATE: "",
      blooD_GRP: "",
      fatheR_NAME: "",
      motheR_NAME: "",
      emaiL_ID_PER: "",
      mobilE_NO: "",
      pdf: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values, formik) => {

      axios
        .put(
          `${EMPLOYEE_URL}/UpdateEmployeeBasicDetails/${employeeIdForView}`,
          {
            ...values,
            chG_DATE: new Date(),
            chG_USER: currentUser?.id,
            chG_TERM: "TERM",
            pdf: pdf,
            emP_IMAGE: image,
          },
          // {
          //   headers: {
          //     "Content-Type": "multipart/form-data",
          //   },
          // }
        )
        .then((response) => {
          const {responseStatus,message}=response.data.data;
          console.log("Basic Details Posted successfully!", response.data);

          if (responseStatus === "E") {
            formik.setSubmitting(false);
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(message)
                      ? message.join("\n")
                      : message}
                  </h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(message)
                      ? message.join("\n")
                      : message}
                  </h6>
                </p>
              </div>
            );
            formik.resetForm();
            navigate(-1);
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );

          formik.setSubmitting(false);
        });
    },
  });

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Basic Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Image Input */}
            {/* <div className="mb-3">
              <label className="form-label">Employee Image</label>
              <input type="file" accept="image/*" onChange={handleImageChange} />
              {image && <img src={image} alt="Employee" style={{ marginTop: "10px", maxWidth: "200px", maxHeight: "200px" }} />}
            </div> */}

            <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative d-flex flex-column align-items-center">
              <div className="d-flex justify-content-center align-items-center mb-3 position-relative">
                <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative d-flex position-relative">
                  {image && (
                    <img
                      src={image}
                      alt="Employee"
                      className="faded-image"
                      style={{
                        maxWidth: "100px",
                        maxHeight: "100px",
                        borderRadius: "5%", // Optional: If you want a circular image
                      }}
                      onClick={() =>
                        // document.getElementById("upload-button").click()
                        {
                          const uploadButton =
                            document.getElementById("upload-button");
                          if (uploadButton) {
                            uploadButton.click();
                          } else {
                            console.error("Upload button not found.");
                          }
                        }
                      }
                    />
                  )}
                  <label
                    htmlFor="upload-button"
                    className="btn btn-sm position-absolute top-0 end-0"
                    style={{ transform: "translate(50%, -50%)" }} // Adjust the position slightly
                  >
                    <KTIcon iconName="pencil" className="fs-2x text-dark" />
                  </label>
                  <input
                    id="upload-button"
                    type="file"
                    accept="image/*"
                    onChange={handleImageChange}
                    style={{ display: "none" }}
                  />
                </div>
              </div>
              <style>
                {`
          .faded-image {
            opacity: 0.5;
            transition: opacity 0.3s;
          }
          .faded-image:hover {
            opacity: 1;
          }
        `}
              </style>
            </div>

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="emP_CODE"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  EMPLOYEE CODE :
                </label>
                <input
                  type="text"
                  id="emP_CODE"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="emP_CODE"
                  placeholder="Enter Employee Code"
                  value={formik.values?.emP_CODE}
                  readOnly
                  // onChange={formik.handleChange}
                  // onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="shorT_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SHORT NAME :
                </label>
                <input
                  type="text"
                  id="shorT_NAME"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  name="shorT_NAME"
                  placeholder="Enter Short Name"
                  value={formik.values.shorT_NAME?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  style={{ cursor: "not-allowed" }}
                  readOnly
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.emP_CODE && formik.errors.emP_CODE ? (
                  <div className="text-danger">{formik.errors.emP_CODE}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.shorT_NAME && formik.errors.shorT_NAME ? (
                  <div className="text-danger">{formik.errors.shorT_NAME}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="emP_FNAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  FIRST NAME :
                </label>
                <input
                  type="text"
                  id="emP_FNAME"
                  className="form-control form-control-lg form-control-solid"
                  name="emP_FNAME"
                  placeholder="Enter first name"
                  value={formik.values?.emP_FNAME}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="emP_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  LAST NAME :
                </label>
                <input
                  type="text"
                  id="emP_LNAME"
                  className="form-control form-control-lg form-control-solid"
                  name="emP_LNAME"
                  placeholder="Enter last name"
                  value={formik.values.emP_LNAME?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.emP_FNAME && formik.errors.emP_FNAME ? (
                  <div className="text-danger">{formik.errors.emP_FNAME}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.emP_LNAME && formik.errors.emP_LNAME ? (
                  <div className="text-danger">{formik.errors.emP_LNAME}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="emP_FNAME"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  MIDDLE NAME :
                </label>
                <input
                  type="text"
                  id="emP_MNAME"
                  className="form-control form-control-lg form-control-solid"
                  name="emP_MNAME"
                  placeholder="Enter middle name"
                  value={formik.values?.emP_MNAME}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sex"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  GENDER :
                </label>
                <select
                  className="form-select form-select-lg form-select-solid"
                  name="sex"
                  value={formik.values.sex}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  {/* <option value="">{formik.values.sex}</option> */}
                  <option value="M">MALE</option>
                  <option value="F">FEMALE</option>
                  <option value="Other">Other</option>
                </select>
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.emP_MNAME && formik.errors.emP_MNAME ? (
                  <div className="text-danger">{formik.errors.emP_MNAME}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.sex && formik.errors.sex ? (
                  <div className="text-danger">
                    {formik.errors.sex}
                  </div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="birtH_DATE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  BIRTH DATE :
                </label>
                <input
                  name="birtH_DATE"
                  type="date"
                  // className="form-control form-control-lg form-control-solid flex-grow-1 me-2"
                  className="form-control form-control-lg form-control-solid me-2"
                  placeholder="Enter Date of Birth"
                  value={formik.values?.birtH_DATE.split("T")[0]}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                <input
                  name="birtH_DATE"
                  className="form-control form-control-lg flex-grow-1"
                  type="file"
                  id="pdf"
                  onChange={handlePdfChange}
                  accept="application/pdf"
                  required
                />
                {formik.touched.pdf && formik.errors.pdf ? (
                  <div className="text-danger">{formik.errors.pdf}</div>
                ) : null}
              </div>
              {/* <KTIcon iconName='document' className='fs-1' /> */}
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sex"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  RELIGION :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="religion"
                  placeholder="Enter Religion Name"
                  value={formik.values.religion?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.birtH_DATE && formik.errors.birtH_DATE ? (
                  <div className="text-danger">{formik.errors.birtH_DATE}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.religion && formik.errors.religion ? (
                  <div className="text-danger">{formik.errors.religion}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="blooD_GRP"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  BLOOD GROUP :
                </label>
                <select
                  id="blooD_GRP"
                  className="form-select form-select-lg form-select-solid"
                  name="blooD_GRP"
                  value={formik.values.blooD_GRP}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value="">{formik.values.blooD_GRP}</option>
                  <option value="A+">A+</option>
                  <option value="A-">A-</option>
                  <option value="B+">B+</option>
                  <option value="B-">B-</option>
                  <option value="AB+">AB+</option>
                  <option value="AB-">AB-</option>
                  <option value="O+">O+</option>
                  <option value="O-">O-</option>
                  <option value="not avialable">Not Avialable</option>
                </select>
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.blooD_GRP && formik.errors.blooD_GRP ? (
                  <div className="text-danger">{formik.errors.blooD_GRP}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sex"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  FATHER NAME :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="fatheR_NAME"
                  placeholder="Enter father name"
                  value={formik.values?.fatheR_NAME}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  MOTHER NAME :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="motheR_NAME"
                  placeholder="Enter mother name"
                  value={formik.values.motheR_NAME?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.fatheR_NAME && formik.errors.fatheR_NAME ? (
                  <div className="text-danger">{formik.errors.fatheR_NAME}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.motheR_NAME && formik.errors.motheR_NAME ? (
                  <div className="text-danger">{formik.errors.motheR_NAME}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="emaiL_ID_PER"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  PERSONAL EMAIL :
                </label>
                <input
                  type="text"
                  id="emaiL_ID_PER"
                  className="form-control form-control-lg form-control-solid"
                  name="emaiL_ID_PER"
                  placeholder="Enter personal email"
                  value={formik.values?.emaiL_ID_PER}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  PERSONAL MOBILE :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="mobilE_NO"
                  placeholder="Enter Personal Mobile No"
                  value={formik.values.mobilE_NO?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.emaiL_ID_PER && formik.errors.emaiL_ID_PER ? (
                  <div className="text-danger">
                    {formik.errors.emaiL_ID_PER}
                  </div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.mobilE_NO && formik.errors.mobilE_NO ? (
                  <div className="text-danger">{formik.errors.mobilE_NO}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default BasicDetailsEdit;
