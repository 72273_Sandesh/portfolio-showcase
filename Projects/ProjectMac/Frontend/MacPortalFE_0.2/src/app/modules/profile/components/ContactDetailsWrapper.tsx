import { Content } from "../../../../_metronic/layout/components/content";
import { ContactDetails } from "./ContactDetails";
import { PersonalDetails } from "./PersonalDetails";

export function ContactDetailsWrapper() {
  return (
    <Content>
      <>
        <div className="row">
          <div>
            <PersonalDetails />
          </div>

          <div>
            <ContactDetails />
          </div>
        </div>
      </>
    </Content>
  );
}
