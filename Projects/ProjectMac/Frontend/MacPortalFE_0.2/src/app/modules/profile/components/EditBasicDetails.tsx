import { Content } from '../../../../_metronic/layout/components/content'
import BasicDetailsEdit from './BasicDetailsEdit'


export function EditBasicDetails() {
  return (
    <Content>
        <div className='mb-5 mb-xl-8'>
        <BasicDetailsEdit/>
        </div>
        
   </Content>
  )
}
