import { Content } from "../../../../_metronic/layout/components/content";
import EmergencyDetailsEdit from "./EmergencyDetailsEdit";

export function EditEmergencyDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <EmergencyDetailsEdit />
      </div>
    </Content>
  );
}
