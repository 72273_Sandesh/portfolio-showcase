import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView, fMIdForUpdate } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
interface qualifications {
  qualification: string;
  qlF_CODE: string;
}

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const EditFamilyDetails: React.FC = () => {
  const [qualifications, setQualifications] = useState<qualifications[]>([]);
  const navigate = useNavigate();
  const {currentUser} = useAuth()

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMP_FAMDET`)
      .then((response) => {
        formik.setValues({
          seQ_NO: response.data[fMIdForUpdate].seQ_NO,
          membeR_NAME: response.data[fMIdForUpdate].membeR_NAME,
          datE_BIRTH: response.data[fMIdForUpdate].datE_BIRTH,
          sex: response.data[fMIdForUpdate].sex,
          relation: response.data[fMIdForUpdate].relation,
          occupation: response.data[fMIdForUpdate].occupation,
          dependent: response.data[fMIdForUpdate].dependent,
          physicaL_STATUS:response.data[fMIdForUpdate].physicaL_STATUS,
          qlF_CODE:response.data[fMIdForUpdate].qlF_CODE,
          adopted:response.data[fMIdForUpdate].adopted,
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });

      axios
      .get(`${EMPLOYEE_URL}/ddl_GetQualification`)
      .then((response) => {
        setQualifications(response.data.qualifications);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);
  
  const validationSchema = Yup.object().shape({
    membeR_NAME: Yup.string().required("Member name is required"),
    datE_BIRTH: Yup.string().required("Birth date is required"),
    sex: Yup.string().required("Gender is required"),
    relation: Yup.string().required("Realation with employee is required"),
    occupation: Yup.string().required("Occupation is required"),
    dependent: Yup.string().required("Dependent status is required"),
    physicaL_STATUS:Yup.string().required("Physical status is required"),
    qlF_CODE:Yup.string().required("Qualification is required"),
    adopted:Yup.string().required("Adoption status is required"),
  });

  const formik = useFormik({
    initialValues: {
      seQ_NO: "",
      membeR_NAME: "",
      datE_BIRTH: "",
      sex: "",
      relation: "",
      occupation: "",
      dependent: "",
      physicaL_STATUS:"",
      qlF_CODE:"",
      adopted:"",
    },
    validationSchema: validationSchema,

    onSubmit: (values, formik) => {
      console.log("Submitting form data:", values);

      axios
        .put(`${EMPLOYEE_URL}/UpdateFamilyMembers${employeeIdForView}/${values.seQ_NO}`, {
          ...values,
          chG_USER: currentUser?.first_name,
          chG_DATE: new Date(),
          chG_TERM: "TERM",
        })
        .then((response) => {
          console.log("Post successful!", response.data);
          const { responseStatus, message } = response.data.data;
          if (responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
          } else {
            
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
            formik.resetForm();
            navigate(-1);
            
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );
        });
    },
  });

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Family Member Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="membeR_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  MEMBER NAME
                </label>
                <input
                  type="text"
                  id="membeR_NAME"
                  className="form-control form-control-lg form-control-solid "
                  name="membeR_NAME"
                  placeholder="Enter Member Name"
                  value={formik.values?.membeR_NAME}
                  onChange={formik.handleChange}
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="relation"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  RELATION
                </label>
                <input
                  type="text"
                  id="relation"
                  className="form-control form-control-lg form-control-solid "
                  name="relation"
                  placeholder="Enter Relation"
                  value={formik.values?.relation}
                  onChange={formik.handleChange}
                />
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.membeR_NAME && formik.errors.membeR_NAME ? (
                    <div className="text-danger">
                      {formik.errors.membeR_NAME}
                    </div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.relation && formik.errors.relation ? (
                    <div className="text-danger">{formik.errors.relation}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="dependent"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DEPENDENCY
                </label>
                <select
                  id="dependent"
                  className="form-control form-control-lg form-control-solid "
                  name="dependent"
                  // value={formik.values?.dependent==='Y' ? "YES" : 'NO'}
                  onChange={formik.handleChange}
                >
                  <option value={formik.values?.dependent}>
                    {formik.values?.dependent === "Y" ? "YES" : "NO"}
                  </option>
                  <option value="Y">YES</option>
                  <option value="N">NO</option>
                </select>
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="occupation"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  OCCUPATION
                </label>
                <input
                  type="text"
                  id="occupation"
                  className="form-control form-control-lg form-control-solid "
                  name="occupation"
                  placeholder="Enter Occupation"
                  value={formik.values?.occupation}
                  onChange={formik.handleChange}
                />
              </div>

              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.dependent && formik.errors.dependent ? (
                    <div className="text-danger">{formik.errors.dependent}</div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.occupation && formik.errors.occupation ? (
                    <div className="text-danger">
                      {formik.errors.occupation}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="datE_BIRTH"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  BIRTH DATE
                </label>
                <input
                  type="date"
                  id="datE_BIRTH"
                  className="form-control form-control-lg form-control-solid "
                  name="datE_BIRTH"
                  placeholder="Enter Birth Date"
                  value={
                    formik.values?.datE_BIRTH
                      ? formik.values.datE_BIRTH.split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sex"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  GENDER
                </label>
                <select
                  id="sex"
                  className="form-select form-select-lg form-select-solid"
                  name="sex"
                  onChange={formik.handleChange}
                >
                  <option value={formik.values?.sex}>
                    {formik.values?.sex === "M" ? "MALE" : "FEMALE"}
                  </option>
                  <option value="M">MALE</option>
                  <option value="F">FEMALE</option>
                  <option value="F">OTHER</option>
                </select>
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.datE_BIRTH && formik.errors.datE_BIRTH ? (
                    <div className="text-danger">
                      {formik.errors.datE_BIRTH}
                    </div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.sex && formik.errors.sex ? (
                    <div className="text-danger">{formik.errors.sex}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}
            {/* Begin: Edit Row */}
            <div className="row mb-1">
            <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="physicaL_STATUS"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  PHYSICAL STATUS
                </label>
                <select
                  id="physicaL_STATUS"
                  className="form-select form-select-lg form-select-solid"
                  name="physicaL_STATUS"
                  onChange={formik.handleChange}
                >
                <option value={formik.values?.physicaL_STATUS}>
                  {formik.values?.physicaL_STATUS === "N" ? "ACTIVE" : formik.values?.adopted === "Y" ? "DISABLED" : ""}
                </option>
                <option value="N">ACTIVE</option>
                <option value="Y">DISABLED</option>
                <option value="O">PREFER NOT TO DESCRIBE</option>
                </select>
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="qlF_CODE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  QUALIFICATION
                </label>
                <select
                  id="qlF_CODE"
                  className="form-select form-select-lg form-select-solid"
                  name="qlF_CODE"
                  onChange={formik.handleChange}
                >
                  <option value={formik.values?.qlF_CODE}>
                    {formik.values?.qlF_CODE}
                  </option>
                  {qualifications.map((qual) => (
                    <option value={qual.qlF_CODE}>{qual.qualification}</option>
                  ))}
                </select>
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.physicaL_STATUS && formik.errors.physicaL_STATUS ? (
                    <div className="text-danger">
                      {formik.errors.physicaL_STATUS}
                    </div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.qlF_CODE && formik.errors.qlF_CODE ? (
                    <div className="text-danger">{formik.errors.qlF_CODE}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}
            {/* Begin: Edit Row */}
            <div className="row mb-1">
            <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="adopted"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  ADOPTED ?
                </label>
                <select
                  id="adopted"
                  className="form-select form-select-lg form-select-solid"
                  name="adopted"
                  onChange={formik.handleChange}
                >
                  <option value={formik.values?.adopted}>
                    {formik.values?.adopted === "Y" ? "YES" : formik.values?.adopted === "N" ? "NO" : ""}
                  </option>
                  <option value="Y">YES</option>
                  <option value="N">NO</option>
                </select>
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.adopted && formik.errors.adopted ? (
                    <div className="text-danger">
                      {formik.errors.adopted}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={//formik.isSubmitting ||
                 !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default EditFamilyDetails;
