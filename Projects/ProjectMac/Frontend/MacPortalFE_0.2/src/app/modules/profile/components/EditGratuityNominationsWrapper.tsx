import { Content } from "../../../../_metronic/layout/components/content";
import EditGratuityDetails from "./EditGratuityDetails";


export function EditGratuityNominationsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <EditGratuityDetails />
      </div>
    </Content>
  );
}
