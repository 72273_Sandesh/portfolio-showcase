import { Content } from "../../../../_metronic/layout/components/content";
import EditPFDeatils from "./EditPFDeatils";


export function EditPFNominationsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <EditPFDeatils />
      </div>
    </Content>
  );
}
