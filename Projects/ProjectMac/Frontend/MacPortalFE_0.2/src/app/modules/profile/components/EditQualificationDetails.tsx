import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView, fMIdForUpdate } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

type QualificationStatus = {
  qualification:string;
  qlF_CODE:string;
}

const EditQualificationDetails: React.FC = () => {
  const [QualificationStatus, setQualificationStatus] = useState<QualificationStatus[]>([]);
  const navigate = useNavigate();
  const {currentUser} = useAuth();

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPEDU`)
      .then((response) => {
        formik.setValues({
          seQ_NO: response.data[fMIdForUpdate].seQ_NO,
          emP_CODE: response.data[fMIdForUpdate].emP_CODE,
          qlF_CODE: response.data[fMIdForUpdate].qlF_CODE,
          institute: response.data[fMIdForUpdate].institute,
          pasS_YEAR: response.data[fMIdForUpdate].pasS_YEAR,
          class: response.data[fMIdForUpdate].class,
          perc: response.data[fMIdForUpdate].perc,
          university: response.data[fMIdForUpdate].university,
          degreE_NAME: response.data[fMIdForUpdate].degreE_NAME,
          remarks: response.data[fMIdForUpdate].remarks,
          statuS_TYPE: response.data[fMIdForUpdate].statuS_TYPE,
          exP_COMP_DATE: response.data[fMIdForUpdate].exP_COMP_DATE,
          froM_DATE: response.data[fMIdForUpdate].froM_DATE,
          tO_DATE: response.data[fMIdForUpdate].tO_DATE,
          quaL_STATUS: response.data[fMIdForUpdate].quaL_STATUS,
          qualification: response.data[fMIdForUpdate].qualification,
          subjecT1: response.data[fMIdForUpdate].subjecT1,
          subjecT2: response.data[fMIdForUpdate].subjecT2,
          subjecT3: response.data[fMIdForUpdate].subjecT3,
          subjecT4: response.data[fMIdForUpdate].subjecT4,
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });

    axios
      .get(`${EMPLOYEE_URL}/ddl_GetQualification`)
      .then((response) => {
        setQualificationStatus(response.data.qualifications);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    institute: Yup.string().required("Institution name is required"),
    pasS_YEAR: Yup.string().required("Passing Year is required"),
    class: Yup.string().required("Grade is required"),
    perc: Yup.string().required("Percentage is required"),
    university: Yup.string().required("University name is required"),
    degreE_NAME: Yup.string().required("Degree name is required"),
    froM_DATE: Yup.string().required("From Date is required"),
    tO_DATE: Yup.string().required("To Date is required"),
    qualification: Yup.string().required("Qualification name is required"),
    subjecT1: Yup.string().required("Subject 1 is required"),
    subjecT2: Yup.string().required("Subject 2 is required"),
    subjecT3: Yup.string().required("Subject 3 is required"),
    subjecT4: Yup.string().required("Subject 4 is required"),
  });

  const formik = useFormik({
    initialValues: {
      emP_CODE: "",
      seQ_NO: "",
      qlF_CODE: "",
      institute: "",
      pasS_YEAR: "",
      class: "",
      perc: "",
      university: "",
      degreE_NAME: "",
      remarks: "",
      statuS_TYPE: "",
      exP_COMP_DATE: "",
      froM_DATE: "",
      tO_DATE: "",
      quaL_STATUS: "",
      qualification: "",
      subjecT1: "",
      subjecT2: "",
      subjecT3: "",
      subjecT4: "",
    },
    validationSchema: validationSchema,

    onSubmit: (values, formik) => {
      console.log("Submitting form data:", values);
    
      axios
        .put(`${EMPLOYEE_URL}/UpdateEmpQualification/${employeeIdForView}/${values.seQ_NO}`, {
          ...values,
          chG_USER: currentUser?.first_name,
          chG_DATE: new Date(),
          chG_TERM: "TERM",
        })
        .then((response) => {
          console.log("Post successful!", response.data);
          const {responseStatus,message}=response.data.data;
          if (responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">Successfully Submitted Form...</h4>
                <p className="alert alert-success" role="alert">
                  <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
                </p>
              </div>
            );
    
            // Clear the form and reset Formik
            formik.resetForm();
            navigate(-1); // Redirect to the previous page or desired location
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );
        });
    },
    
  });

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Qualification Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="degreE_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DEGREE
                </label>
                <select
                  className="form-select form-select-lg form-select-solid"
                  name="degreE_NAME"
                  value={formik.values.degreE_NAME}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value={formik.values.degreE_NAME}>
                    {formik.values.degreE_NAME}
                  </option>
                  <option value="SSC">SSC</option>
                  <option value="HSC">HSC</option>
                  <option value="Diploma">Diploma</option>
                  <option value="Graduation">Graduation</option>
                  <option value="Post Graduation">Post Graduation</option>
                  <option value="Other">Other</option>
                </select>
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="qlF_CODE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  QUALIFICATION
                </label>
                <select
                  id="qlF_CODE"
                  className="form-select form-select-lg form-select-solid"
                  name="qlF_CODE"
                  onChange={formik.handleChange}
                >
                  <option value={formik.values?.qlF_CODE}>
                    {formik.values?.qualification}
                  </option>

                  {QualificationStatus.map((state) => (
                    <option value={state.qlF_CODE}>
                      {state.qualification}
                    </option>
                  ))}
                </select>
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.qualification &&
                  formik.errors.qualification ? (
                    <div className="text-danger">
                      {formik.errors.qualification}
                    </div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.degreE_NAME && formik.errors.degreE_NAME ? (
                    <div className="text-danger">
                      {formik.errors.degreE_NAME}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="university"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  BOARD / UNIVERSITY
                </label>
                <input
                  type="text"
                  id="university"
                  className="form-control form-control-lg form-control-solid"
                  name="university"
                  placeholder="Enter Board / University Name"
                  value={formik.values?.university}
                  onChange={formik.handleChange}
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="institute"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  INSTITUTION
                </label>
                <input
                  type="text"
                  id="institute"
                  className="form-control form-control-lg form-control-solid"
                  name="institute"
                  placeholder="Enter Institute Name"
                  value={formik.values?.institute}
                  onChange={formik.handleChange}
                />
              </div>

              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.university && formik.errors.university ? (
                    <div className="text-danger">
                      {formik.errors.university}
                    </div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.institute && formik.errors.institute ? (
                    <div className="text-danger">{formik.errors.institute}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="froM_DATE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  START DATE
                </label>
                <input
                  type="date"
                  id="froM_DATE"
                  className="form-control form-control-lg form-control-solid"
                  name="froM_DATE"
                  placeholder="Enter Start Date"
                  value={
                    formik.values?.froM_DATE
                      ? formik.values.froM_DATE.split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="tO_DATE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  END DATE
                </label>
                <input
                  type="date"
                  id="tO_DATE"
                  className="form-control form-control-lg form-control-solid"
                  name="tO_DATE"
                  placeholder="Enter End Date"
                  value={
                    formik.values?.tO_DATE
                      ? formik.values.tO_DATE.split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                />
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.froM_DATE && formik.errors.froM_DATE ? (
                    <div className="text-danger">{formik.errors.froM_DATE}</div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.tO_DATE && formik.errors.tO_DATE ? (
                    <div className="text-danger">{formik.errors.tO_DATE}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="class"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  GRADE
                </label>
                <input
                  type="text"
                  id="class"
                  className="form-control form-control-lg form-control-solid"
                  name="class"
                  placeholder="Enter Grade"
                  value={formik.values?.class}
                  onChange={formik.handleChange}
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="perc"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  PERCENTAGE
                </label>
                <input
                  type="number"
                  id="perc"
                  className="form-control form-control-lg form-control-solid"
                  name="perc"
                  placeholder="Enter Percentage"
                  value={formik.values?.perc}
                  onChange={formik.handleChange}
                  min={0}
                  max={100}
                />
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.class && formik.errors.class ? (
                    <div className="text-danger">{formik.errors.class}</div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.perc && formik.errors.perc ? (
                    <div className="text-danger">{formik.errors.perc}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="subjecT1"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SUBJECT 1
                </label>
                <input
                  type="text"
                  id="subjecT1"
                  className="form-control form-control-lg form-control-solid"
                  name="subjecT1"
                  placeholder="Enter Subject 1"
                  value={formik.values?.subjecT1}
                  onChange={formik.handleChange}
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="subjecT2"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SUBJECT 2
                </label>
                <input
                  type="text"
                  id="subjecT2"
                  className="form-control form-control-lg form-control-solid"
                  name="subjecT2"
                  placeholder="Enter Subject 2"
                  value={formik.values?.subjecT2}
                  onChange={formik.handleChange}
                  max={100}
                />
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.subjecT1 && formik.errors.subjecT1 ? (
                    <div className="text-danger">{formik.errors.subjecT1}</div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.subjecT3 && formik.errors.subjecT3 ? (
                    <div className="text-danger">{formik.errors.subjecT3}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="subjecT3"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SUBJECT 3
                </label>
                <input
                  type="text"
                  id="subjecT3"
                  className="form-control form-control-lg form-control-solid  "
                  name="subjecT3"
                  placeholder="Enter Subject 3"
                  value={formik.values?.subjecT3}
                  onChange={formik.handleChange}
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="subjecT4"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SUBJECT 4
                </label>
                <input
                  type="text"
                  id="subjecT4"
                  className="form-control form-control-lg form-control-solid "
                  name="subjecT4"
                  placeholder="Enter Subject 4"
                  value={formik.values?.subjecT4}
                  onChange={formik.handleChange}
                  max={100}
                />
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.subjecT3 && formik.errors.subjecT3 ? (
                    <div className="text-danger">{formik.errors.subjecT3}</div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.subjecT4 && formik.errors.subjecT4 ? (
                    <div className="text-danger">{formik.errors.subjecT4}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="quaL_STATUS"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  STATUS
                </label>
                <select
                  name="quaL_STATUS"
                  className="form-select form-select-lg form-select-solid flex-grow-1 me-2"
                  value={formik.values?.quaL_STATUS}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value={formik.values?.quaL_STATUS}>
                    {formik.values?.quaL_STATUS}
                  </option>
                  <option value="COMPLETED">COMPLETED</option>
                  <option value="PERSUING">PERSUING</option>
                </select>
              </div>

              {formik.values?.quaL_STATUS.toLowerCase() === "completed" ? (
                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="pasS_YEAR"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PASSING YEAR
                  </label>
                  <input
                    type="number"
                    id="pasS_YEAR"
                    className="form-control form-control-lg form-control-solid "
                    name="pasS_YEAR"
                    placeholder="Enter Passing Year"
                    value={formik.values?.pasS_YEAR}
                    onChange={formik.handleChange}
                    min={0}
                    max={3000}
                  />
                </div>
              ) : (
                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="exP_COMP_DATE"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    EXPECTED COMPLETION DATE
                  </label>
                  <input
                    type="date"
                    id="exP_COMP_DATE"
                    className="form-control form-control-lg form-control-solid "
                    name="exP_COMP_DATE"
                    placeholder="Enter Expected Completion Date"
                    value={formik.values?.exP_COMP_DATE.split("T")[0]}
                    onChange={formik.handleChange}
                    max={100}
                    required
                  />
                </div>
              )}
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.quaL_STATUS && formik.errors.quaL_STATUS ? (
                    <div className="text-danger">
                      {formik.errors.quaL_STATUS}
                    </div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.pasS_YEAR && formik.errors.pasS_YEAR ? (
                    <div className="text-danger">{formik.errors.pasS_YEAR}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default EditQualificationDetails;
