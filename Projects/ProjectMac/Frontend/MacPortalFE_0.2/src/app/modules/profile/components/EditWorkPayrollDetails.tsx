import { Content } from "../../../../_metronic/layout/components/content";
import {  } from "./LanguageDetailAdd";
import WorkPayrollDetailsEdit from "./WorkPayrollDetailsEdit";

export function EditWorkPayrollDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <WorkPayrollDetailsEdit/>
      </div>
    </Content>
  );
}



