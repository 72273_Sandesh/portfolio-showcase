import { Content } from "../../../../_metronic/layout/components/content";
import { TransferHistoryTable } from "../../../../_metronic/partials/widgets/tables/TransferHistoryTable";



export function TransferHistoryWrapper() {
  return (
    <Content>
      <>
        <div className="row g-10 g-xxl-8">
          <div>
            <TransferHistoryTable className="" />
          </div>
          </div>
      </>
    </Content>
  );
}
