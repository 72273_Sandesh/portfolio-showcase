import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { KTIcon } from "../../../../_metronic/helpers";
import { employeeIdForView } from "../../GlobalQuery";
import { InternalMode , ThemeModeComponent } from "../../../../_metronic/assets/ts/layout";


export function WorkDetails() {
  const [basicDetails, setBasicDetails] = useState<any>([]);
  const [isMinimized, setIsMinimized] = useState(false); // Default state: card is minimized
  const currentMode: InternalMode = ThemeModeComponent.getCurrentMode();

  const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

  useEffect(() => {
    // Fetch basic details from the backend API
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setBasicDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  // const toggleMinimize = () => {
  //   setIsMinimized(!isMinimized); // Toggle minimized/maximized state
  // };

  return (
    <div
      className={`card`}
      id="Employment Details"
      style={(currentMode === 'dark' ? {background: "#0F1014"} : { background: "#F9F9FA"})}
    >
      {/* <div
        className="card-header d-flex justify-content-between align-items-center cursor-pointer"
        // onClick={toggleMinimize}
      > */}
        {/* <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Employment Details</h3>
        </div> */}
        {/* <Link
          to="/crafted/pages/profile/EditBasicDetails"
          className="btn btn-sm btn-primary me-3 align-self-center"
        >
          Edit
        </Link> */}

        {/* <button
          className={`btn btn-sm mt-3 btn-light btn-icon rounded-circle ${
            isMinimized ? "" : "active"
          }`}
          aria-label={isMinimized ? "Expand" : "Collapse"}
        >
          <KTIcon
            iconName={isMinimized ? "arrow-down" : "arrow-up"}
            className={`fs-3 ${isMinimized ? "text-dark" : "text-dark"}`}
          />
        </button> */}
      {/* </div> */}

      {!isMinimized && (
        <div className="cardrow g-2 mb-2 g-xxl-2">
          <div className={"rounded " + (currentMode === 'dark' ? "bg-light" : "bg-white")}>
            <div className="card-header bg-light-primary d-flex justify-content-between align-items-left cursor-pointer">
              <div className="card-title m-0">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                  Work & Payroll Details
                </h3>
              </div>
              <Link
                to="/crafted/pages/profile/EditWorkPayrollDetails"
                className="btn btn-sm btn-primary me-3 align-self-center"
              >
                Edit
              </Link>
            </div>
            <div className="card-body p-9">
              {/* Begin:first Row */}
              <div className="row">
                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      HIRE SOURCE :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.contacT_CODE}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      PREVIOUS EXP. :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.contacT_CODE}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      {" "}
                      WORK SITE :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.worK_SITE}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      PAYROLL SITE :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.paY_SITE}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      GRATUITY DATE :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {/* {basicDetails?.gratuitY_DATE} */}
                      {new Date(basicDetails?.gratuitY_DATE).toLocaleDateString(
                      "en-GB"
                    )}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      NOTICE PERIOD:{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.noticE_PRD ? `${basicDetails.noticE_PRD} days` : ''}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      BASIC PER MONTH :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.basic}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      CTC PER MONTH :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.curR_CTC}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      PAY MODE :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {/* {basicDetails.paymenT_MODE.toUpperCase()} */}
                      {basicDetails?.paY_MODE
                        ? basicDetails.paY_MODE.toUpperCase()
                        : ""}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      PAY TYPE :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.paY_TYPE? basicDetails.paY_TYPE.toUpperCase(): ""}    
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      {" "}
                      BANK CODE :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.banK_CODE}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      BANK IFSC CODE :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.banK_IFSC}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}
                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      BANK A/C NAME :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.banK_NAME}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      BANK A/C NO. :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.banK_ACCT}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      DD PAY BANK :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.dD_PAYABLE_BANK}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      OLD PAYROLL ID :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.contacT_CODE}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      SALARY WITHHELD :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.contacT_CODE}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      STATION CODE :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.contacT_CODE}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}
              </div>
              {/* End:first Row */}
            </div>
          </div>
        </div>
      )}

      {!isMinimized && (
        <div className="cardrow g-2 mb-2 g-xxl-2">
          <div className={"rounded " + (currentMode === 'dark' ? "bg-light" : "bg-white")}>
            <div className="card-header bg-light-primary d-flex justify-content-between align-items-left cursor-pointer">
              <div className="card-title m-0">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                  Statutory Details
                </h3>
              </div>
              <Link
                to="/crafted/pages/profile/EditStatutoryDetails"
                className="btn btn-sm btn-primary me-3 align-self-center"
              >
                Edit
              </Link>
            </div>
            <div className="card-body p-9">
              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                    PF UAN NO. :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.pF_UN_NO}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                    PF NO. :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.pF_NO}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              {/* <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                    PENSION ELIGIBLE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.contacT_CODE}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted"> </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.contacT_CODE}
                  </span>
                </div>
              </div> */}
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                    ESIC NO :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.esiC_NO}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                    FPF NO :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.fpF_NO}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
