import { useEffect } from 'react';
import { useInactivity } from './InactivityContext'; // Adjust the import based on your project structure

const useActivityTracker = (elementRef: React.RefObject<HTMLElement>) => {
  const { updateExpireTime } = useInactivity();

  useEffect(() => {
    const element = elementRef.current;
console.log("inside useActivityTracker++++++++++++++++++")
    if (element) {
      const updateExpireTimeListener = () => updateExpireTime();

      element.addEventListener('click', updateExpireTimeListener);

      return () => {
        element.removeEventListener('click', updateExpireTimeListener);
      };
    }
  }, [elementRef, updateExpireTime]);
};

export default useActivityTracker;
