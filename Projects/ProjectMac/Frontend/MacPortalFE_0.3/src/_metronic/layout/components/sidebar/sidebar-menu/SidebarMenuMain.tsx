import {useIntl} from 'react-intl'
// import {KTIcon} from '../../../../helpers'
import {SidebarMenuItemWithSub} from './SidebarMenuItemWithSub'
import {SidebarMenuItem} from './SidebarMenuItem'
import { useAuth } from '../../../../../app/modules/auth'
import { useState } from 'react'

const SidebarMenuMain = () => {
  const intl = useIntl()

  const handleEmployeeClick = () => {
    window.location.href = '/macleods/macportal/apps/employee/employees';
  };

  const {currentUser} = useAuth();
  const LoggedInUser = currentUser?.id;
  console.log("apla id ahe ",LoggedInUser)
  return (
    <>
      <SidebarMenuItem
        to='/dashboard'
        icon='home'
        title={intl.formatMessage({id: 'MENU.DASHBOARD'})}
        fontIcon='bi-app-indicator'
      />
      {/* <SidebarMenuItem to='/builder' icon='switch' title='You Are Here' fontIcon='bi-layers' /> */}
      <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>Core HR</span>
        </div>
      </div>
      {/* <SidebarMenuItemWithSub
        to='/crafted/pages'
        title='Pages'
        fontIcon='bi-archive'
        icon='element-plus'
      >
        <SidebarMenuItemWithSub to='/crafted/pages/profile' title='Profile' hasBullet={true}>
          <SidebarMenuItem to='/crafted/pages/profile/overview' title='Overview' hasBullet={true} />
          <SidebarMenuItem to='/crafted/pages/profile/projects' title='Projects' hasBullet={true} />
          <SidebarMenuItem
            to='/crafted/pages/profile/campaigns'
            title='Campaigns'
            hasBullet={true}
          />
          <SidebarMenuItem
            to='/crafted/pages/profile/documents'
            title='Documents'
            hasBullet={true}
          />
          <SidebarMenuItem
            to='/crafted/pages/profile/connections'
            title='Connections'
            hasBullet={true}
          />
        </SidebarMenuItemWithSub>

        <SidebarMenuItemWithSub to='/crafted/pages/wizards' title='Wizards' hasBullet={true}>
          <SidebarMenuItem
            to='/crafted/pages/wizards/horizontal'
            title='Horizontal'
            hasBullet={true}
          />
          <SidebarMenuItem to='/crafted/pages/wizards/vertical' title='Vertical' hasBullet={true} />
        </SidebarMenuItemWithSub>
      </SidebarMenuItemWithSub> */}
      <SidebarMenuItemWithSub
        to='/crafted/accounts'
        title='Masters'
        icon='category'
        fontIcon='bi-person'
      >
        
        <SidebarMenuItem to='/apps/cadre/cadres' title='Cadre' hasBullet={true} />
        <SidebarMenuItem to='/apps/department/departments' title='Department' hasBullet={true} />
        <SidebarMenuItem to='/apps/designation/designations' title='Designation' hasBullet={true} />
        <SidebarMenuItem to='/apps/grade/grades' title='Grade' hasBullet={true} />
        <SidebarMenuItem to='/apps/qualification/qualifications' title='Qualification' hasBullet={true} />
        {/* <SidebarMenuItem to='/apps/employee/employees' 
          title='Employee' 
          hasBullet={true}
          onClick={handleEmployeeClick} 
        /> */}
        <SidebarMenuItem to='/apps/employee/employees' title='Employee' hasBullet={true} />
        {/* <SidebarMenuItem to='/apps/dept/departmentMaster' title='Dept' hasBullet={true} /> */}
        {/* <SidebarMenuItem to='/crafted/account/overview' title='Offer Template' hasBullet={true} />
        <SidebarMenuItem to='/crafted/account/settings' title='Employee Roles' hasBullet={true} />
        <SidebarMenuItem to='/crafted/account/overview' title='Institute' hasBullet={true} /> */}
      </SidebarMenuItemWithSub>


      {(LoggedInUser === "H032422   " || LoggedInUser === "H004711   "  || LoggedInUser === "H008567   " )  && (
        <SidebarMenuItemWithSub to='/error' title='Transactions' fontIcon='bi-sticky' icon='arrow-right-left'>
          <SidebarMenuItem to='/apps/bankDetails/bankDetails' title='Employee' fontIcon='bi-sticky' icon='profile-user'>
            {/* <SidebarMenuItem to='/apps/bankDetails/bankDetails' title='Bank Details' hasBullet={true} />
            <SidebarMenuItem to='/apps/Journey/Journey' title='Journey Details' hasBullet={true} /> */}
          </SidebarMenuItem>
          <SidebarMenuItem to='/apps/masterDetails/masterDetails' title='Masters' fontIcon='bi-person' icon='category'/>
        </SidebarMenuItemWithSub>
      )}

           {/* <SidebarMenuItemWithSub to='/error' title='Cadre' fontIcon='bi-sticky' icon='share'>
          <SidebarMenuItem to='/apps/bankDetails/bankDetails' title='Bank Details' hasBullet={true} />
          <SidebarMenuItem to='/crafted/pages/wizards/horizontal' title='Family Details' hasBullet={true} />
        </SidebarMenuItemWithSub>
        <SidebarMenuItemWithSub to='/error' title='Department' fontIcon='bi-sticky' icon='share'>
          <SidebarMenuItem to='/apps/bankDetails/bankDetails' title='Bank Details' hasBullet={true} />
          <SidebarMenuItem to='/crafted/pages/wizards/horizontal' title='Family Details' hasBullet={true} />
        </SidebarMenuItemWithSub>
        <SidebarMenuItemWithSub to='/error' title='Designation' fontIcon='bi-sticky' icon='share'>
          <SidebarMenuItem to='/apps/bankDetails/bankDetails' title='Bank Details' hasBullet={true} />
          <SidebarMenuItem to='/crafted/pages/wizards/horizontal' title='Family Details' hasBullet={true} />
        </SidebarMenuItemWithSub>
        <SidebarMenuItemWithSub to='/error' title='Grade' fontIcon='bi-sticky' icon='share'>
          <SidebarMenuItem to='/apps/bankDetails/bankDetails' title='Bank Details' hasBullet={true} />
          <SidebarMenuItem to='/crafted/pages/wizards/horizontal' title='Family Details' hasBullet={true} />
        </SidebarMenuItemWithSub>
        <SidebarMenuItemWithSub to='/error' title='Qualification' fontIcon='bi-sticky' icon='share'>
          <SidebarMenuItem to='/apps/bankDetails/bankDetails' title='Bank Details' hasBullet={true} />
          <SidebarMenuItem to='/crafted/pages/wizards/horizontal' title='Family Details' hasBullet={true} />
        </SidebarMenuItemWithSub> */}
          {/* <SidebarMenuItem to='/crafted/pages/wizards/horizontal' title='SMS Broadcast' hasBullet={true} />
          <SidebarMenuItem to='/crafted/pages/wizards/horizontal' title='All Dept' hasBullet={true} />
          <SidebarMenuItem to='/apps/bankDetails/bankDetails' title='Bank Details' hasBullet={true} /> */}
      <SidebarMenuItemWithSub
        to='/crafted/widgets'
        title='Reports'
        icon="abstract-26"
         fontIcon="bi-layers"
        
      >

        {/* <SidebarMenuItem to='/crafted/widgets/lists' title='Personal Information' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/statistics' title='Contact Information' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/charts' title='Family Information' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/mixed' title='Education Information' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/tables' title='Experience Information' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/feeds' title='Official Information' hasBullet={true} />

        <SidebarMenuItem to='/crafted/widgets/lists' title='Entry Report' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/statistics' title='Promotion/Increment Report' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/charts' title='Transfer History Report' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/mixed' title='Pending/Regularized Reports' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/tables' title='Comparision Report (Educ.Exp.)' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/feeds' title='Statutory Benefit Information' hasBullet={true} />

        <SidebarMenuItem to='/crafted/widgets/lists' title='Salary Structure History' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/statistics' title='Emp CTC Structure History' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/charts' title='Pay Structure History Deatails' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/mixed' title='Employee Profile' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/tables' title='Employee Details for Insurance' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/feeds' title='candidate attachments' hasBullet={true} />

        <SidebarMenuItem to='/crafted/widgets/feeds' title='Medical Checkup MIS' hasBullet={true} /> */}

      </SidebarMenuItemWithSub>
      {/* <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>Recruitment</span>
        </div>
      </div> */}
      {/* <SidebarMenuItemWithSub
        to='/apps/chat'
        title='Transactions'
        fontIcon='bi-chat-left'
        icon='message-text-2'
      >
        <SidebarMenuItem to='/apps/chat/private-chat' title='Resume' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='Schedule' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Offer Calculator' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/private-chat' title='Approve/Decline Offer ' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='Offer History' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Reimbursement' hasBullet={true} />

        <SidebarMenuItem to='/apps/chat/private-chat' title='Job Posting' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='Approve/Decline Job Posting' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Create Event' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/private-chat' title='Bulk Interview Schedule' hasBullet={true} />
       
      </SidebarMenuItemWithSub> */}

      {/* <SidebarMenuItemWithSub
        to='/apps/chat'
        title='Reports'
        fontIcon='bi-chat-left'
        icon='message-text-2'
      >
        <SidebarMenuItem to='/apps/chat/private-chat' title='Candidate Schedule' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='Candidate Summary' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Reimbursement Summary' hasBullet={true} />

        <SidebarMenuItem to='/apps/chat/private-chat' title='Candidate Information' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='Offer Given Status' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Hire Source Summary' hasBullet={true} />

        <SidebarMenuItem to='/apps/chat/private-chat' title='Hire Source Details' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='Walk-In Event Status' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Internal Employee Transfer' hasBullet={true} />

     </SidebarMenuItemWithSub> */}


      {/* <SidebarMenuItem
        to='/apps/user-management/users'
        icon='abstract-28'
        title='Dashboard'
        fontIcon='bi-layers'
      /> */}
      {/* <div className='menu-item'>
        <a
          target='_blank'
          className='menu-link'
          href={import.meta.env.VITE_APP_PREVIEW_DOCS_URL + '/changelog'}
        >
          <span className='menu-icon'>
            <KTIcon iconName='code' className='fs-2' />
          </span>
          <span className='menu-title'>Changelog {import.meta.env.VITE_APP_VERSION}</span>
        </a>
      </div> */}





      {/* <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>Seperation</span>
        </div>
      </div> */}
      {/* <SidebarMenuItemWithSub
        to='/apps/chat'
        title='Reports'
        fontIcon='bi-chat-left'
        icon='message-text-2'
      >
        <SidebarMenuItem to='/apps/chat/private-chat' title='Pending Status' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='Provisional Status' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Check Issue Letter' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/private-chat' title='Cover Letter ' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='FFS Link Letter' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='FFS Login Trace' hasBullet={true} />

        <SidebarMenuItem to='/apps/chat/private-chat' title='Relieving Letter' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='Experience Letter' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Gratuity Claim Letter' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/private-chat' title='Exit Feedback Analysis' hasBullet={true} />

        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Exit Feedback Analysis Summary' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/private-chat' title='Gratuity Statement' hasBullet={true} />
       
       
      </SidebarMenuItemWithSub> */}

      {/* <SidebarMenuItemWithSub
        to='/apps/chat'
        title='Reports'
        fontIcon='bi-chat-left'
        icon='message-text-2'
      >
        <SidebarMenuItem to='/apps/chat/private-chat' title='Candidate Schedule' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='Candidate Summary' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Reimbursement Summary' hasBullet={true} />

        <SidebarMenuItem to='/apps/chat/private-chat' title='Candidate Information' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='Offer Given Status' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Hire Source Summary' hasBullet={true} />

        <SidebarMenuItem to='/apps/chat/private-chat' title='Hire Source Details' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/group-chat' title='Walk-In Event Status' hasBullet={true} />
        <SidebarMenuItem to='/apps/chat/drawer-chat' title='Internal Employee Transfer' hasBullet={true} />

     </SidebarMenuItemWithSub> */}


      {/* <SidebarMenuItem
        to='/apps/user-management/users'
        icon='abstract-28'
        title='Dashboard'
        fontIcon='bi-layers'
      /> */}
      {/* <div className='menu-item'>
        <a
          target='_blank'
          className='menu-link'
          href={import.meta.env.VITE_APP_PREVIEW_DOCS_URL + '/changelog'}
        >
          <span className='menu-icon'>
            <KTIcon iconName='code' className='fs-2' />
          </span>
          <span className='menu-title'>Changelog {import.meta.env.VITE_APP_VERSION}</span>
        </a>
      </div> */}




{/* <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>Data Analytics</span>
        </div>
      </div> */}
      
{/* 
      <SidebarMenuItem
        to='/apps/user-management/users'
        icon='abstract-28'
        title='Recruitment'
        fontIcon='bi-layers'
      />


      <SidebarMenuItem
        to='/apps/user-management/users'
        icon='abstract-28'
        title='Employee Joined'
        fontIcon='bi-layers'
      /> */}
    </>
  )
}

export {SidebarMenuMain}
