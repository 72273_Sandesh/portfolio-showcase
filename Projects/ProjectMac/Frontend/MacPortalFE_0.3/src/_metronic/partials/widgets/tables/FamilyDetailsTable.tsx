import { FC, useEffect, useState } from "react";
import { KTIcon } from "../../../helpers";
import axios from "axios";
import {
  employeeIdForView,
  setFMIdForUpdate,
} from "../../../../app/modules/GlobalQuery";
import { Link, useNavigate } from "react-router-dom";
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

type Props = {
  className: string;
};

type FamilyDetail = {
  name: string;
  proficiency: number;
  seQ_NO: string;
  image: string;
  src: string;
  membeR_NAME: string;
  sex: string;
  relation: string;
  datE_BIRTH: string;
  occupation: string;
  dependent: string;
  medI_NOMINEE: string;
  pF_PERC: string;
  graT_PERC: string;
  peN_PERC: string;
};

const FamilyDetailsTable: FC<Props> = ({ className }) => {
  const [familyDetails, setFamilyDetails] = useState<FamilyDetail[]>([]);
  const navigate = useNavigate();
  useEffect(() => {
    // Fetch family details from API
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMP_FAMDET`)
      //https://localhost:44360/api/Employee/H008567/EMP_FAMDET

      .then((response) => {
        setFamilyDetails(response.data);
      })
      .catch((error) => {
        console.error("Error fetching family details:", error);
      });
  }, []);
  //https://localhost:44360/api/Employee/H008567/STATU

  const [statutoryDetails, setStatutoryDetails] = useState<FamilyDetail[]>([]);

  useEffect(() => {
    // Fetch family details from API
    axios
      // .get(`${EMPLOYEE_URL}/${employeeIdForView}/STATU`)
      .get("https://localhost:44360/api/Employee/H008567/STATU")
      .then((response) => {
        setStatutoryDetails(response.data);
      })
      .catch((error) => {
        console.error("Error fetching family details:", error);
      });
  }, []);

  const editFamilyMember = (seqNo: number) => {
    setFMIdForUpdate(seqNo - 1);
    navigate("/crafted/pages/profile/EditFamilyDetails");
  };

  return (
    <div className={`card ${className}`}>
      {/* begin::Header */}
      <div className="card-header bg-light-primary border-0 pt-5">
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bold fs-3 mb-1">Family Details</span>
          <span className="text-muted mt-1 fw-semibold fs-7">
            {familyDetails.length} members
          </span>
        </h3>
        <div className="card-toolbar">
          {/* begin::Menu */}
          <Link
            to="/crafted/pages/profile/FamilyMemberAdd"
            className="btn btn-sm btn-primary me-3 align-self-center"
          >
            Add
          </Link>
          {/* <Link
            to="/crafted/pages/profile/EditFamilyDetails"
            className="btn btn-sm btn-primary me-3 align-self-center"
          >
            Edit
          </Link> */}
          {/* end::Menu */}
        </div>
      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className="card-body py-3">
        <div className="table-responsive">
          <table className="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
            {/* begin::Table head */}
            <thead>
              <tr className="fw-bold text-muted">
                <th className="w-25px"></th>
                <th className="min-w-150px">Name of Family Members</th>
                <th className="min-w-120px">Birth Date</th>
                <th className="min-w-140px">Occupation</th>
                <th className="min-w-120px">Mediclaim </th>
                <th className="min-w-120px">PF</th>
                <th className="min-w-120px">Gratuity</th>
                <th className="min-w-120px">Pension</th>

                <th className="min-w-100px text-end">Actions</th>
              </tr>
            </thead>
            {/* end::Table head */}
            <tbody>
              {familyDetails.map((familyDetail, index) => (
                <tr key={index}>
                  <td></td>
                  <td>
                    <div className="d-flex align-items-center">
                      <div className="symbol symbol-45px me-5">
                        <img
                          src={
                            // familyDetail.image?.src || "/media/avatars/User.png"
                            "/media/avatars/User.png"
                            //for backend image mapping
                          }
                          // onError={(e) => {
                          //   e.target.src = "/media/avatars/User.png";
                          //   e.target.style.objectFit = "contain";
                          // }}
                          alt="User Avatar"
                          style={{
                            width: "40px",
                            height: "40px",
                            objectFit: "cover",
                          }}
                        />
                      </div>
                      <div className="d-flex justify-content-start flex-column">
                        <a className="text-gray-900 fw-bold text-hover-primary fs-6">
                          {familyDetail.membeR_NAME}
                        </a>
                        <span className="fw-bolder d-flex align-items-right">
                          <div className="badge fs-8 me-5 fw-bolder d-flex align-items-right text-gray-500">
                            {" "}
                            {familyDetail.sex === "M" ? "MALE" : "FEMALE"}
                          </div>
                          <div className="badge fs-8 fw-bolder d-flex align-items-right text-gray-500">
                            {familyDetail.relation}
                          </div>
                        </span>
                      </div>
                    </div>
                  </td>
                  {/* <td>
                    <span className='fw-bolder d-flex align-items-right'>
                      <div className='badge badge-light-success fw-bolder d-flex align-items-right'>{familyDetail.datE_BIRTH}</div>
                    </span>
                  </td> */}

                  <td>
                    <span className="fw-bolder d-flex align-items-right">
                      {/* <div className="badge badge-light-dark fw-bolder d-flex align-items-right">
                       
                        {new Date(familyDetail.datE_BIRTH).toLocaleDateString(
                          "en-GB"
                        )}
                      </div> */}

                      <div className="fw-bolder d-flex align-items-right">
                        {familyDetail?.datE_BIRTH
                          ? new Date(
                              familyDetail?.datE_BIRTH
                            ).toLocaleDateString("en-GB")
                          : ""}
                      </div>
                    </span>
                  </td>

                  <td>
                    <a className="text-gray-900 fw-bold text-hover-primary d-block fs-6">
                      {familyDetail.occupation}
                    </a>
                    <span className="fw-bolder d-flex align-items-right">
                      <div className="fw-bolder fs-8 d-flex align-items-right text-gray-500">
                        {/* Conditionally render Dependent or Independent */}
                        {familyDetail.dependent === "YES"
                          ? "INDEPENDENT"
                          : "DEPENDENT"}
                      </div>
                    </span>
                  </td>

                  <td className="text-end">
                    <span className="fw-bolder d-flex align-items-end">
                      <div
                        style={{
                          width: "40px",
                          height: "20px",
                          borderRadius: "10px",
                          backgroundColor:
                            familyDetail.medI_NOMINEE === "Y" ? "green" : "red",
                          display: "flex",
                          alignItems: "end",
                          justifyContent:
                            familyDetail.medI_NOMINEE === "Y"
                              ? "flex-end"
                              : "flex-start",
                          padding: "2px",
                          boxSizing: "border-box",
                          position: "relative",
                        }}
                      >
                        <div
                          style={{
                            width: "14px",
                            height: "14px",
                            borderRadius: "50%",
                            backgroundColor: "white",
                            transition: "0.3s",
                            position: "absolute",
                            top: "50%",
                            left:
                              familyDetail.medI_NOMINEE === "Y"
                                ? "calc(100% - 20px)"
                                : "4px",
                            transform: "translateY(-50%)",
                          }}
                        ></div>
                      </div>
                    </span>
                  </td>

                  <td className="text-end">
                    <div className="d-flex flex-column w-120 me-2">
                      <div className="d-flex flex-stack mb-2">
                        <span className="text-muted me-2 fs-7 fw-semibold">
                          {familyDetail.pF_PERC}
                        </span>
                      </div>
                      <div className="progress h-6px w-100">
                        <div
                          className="progress-bar bg-primary"
                          role="progressbar"
                          style={{ width: `${familyDetail.pF_PERC}%` }}
                        ></div>
                      </div>
                    </div>
                  </td>
                  {/* Gratuity and Pension columns */}
                  <td className="text-end">
                    <div className="d-flex flex-column w-100 me-2">
                      <div className="d-flex flex-stack mb-2">
                        <span className="text-muted me-2 fs-7 fw-semibold">
                          {familyDetail.graT_PERC}
                        </span>
                      </div>
                      <div className="progress h-6px w-100">
                        <div
                          className="progress-bar bg-primary"
                          role="progressbar"
                          style={{ width: `${familyDetail.graT_PERC}%` }}
                        ></div>
                      </div>
                    </div>
                  </td>

                  <td className="text-end">
                    <div className="d-flex flex-column w-100 me-2">
                      <div className="d-flex flex-stack mb-2">
                        <span className="text-muted me-2 fs-7 fw-semibold">
                          {familyDetail.peN_PERC}
                        </span>
                      </div>
                      <div className="progress h-6px w-100">
                        <div
                          className="progress-bar bg-primary"
                          role="progressbar"
                          style={{ width: `${familyDetail.peN_PERC}%` }}
                        ></div>
                      </div>
                    </div>
                  </td>

                  <td className="text-end">
                    <div className="d-flex justify-content-end flex-shrink-0">
                      {familyDetail.dependent === "NO" && (
                        <a className="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
                          <KTIcon iconName="user-tick" className="fs-3" />
                        </a>
                      )}
                      <a
                        className="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-2"
                        onClick={() =>
                          editFamilyMember(+familyDetail.seQ_NO.trim())
                        }
                      >
                        <KTIcon iconName="pencil" className="fs-3" />
                      </a>
                      <a
                        className="btn btn-icon btn-bg-light btn-active-color-primary btn-sm"
                        style={{ cursor: "not-allowed" }}
                      >
                        <KTIcon
                          iconName="trash"
                          className="fs-3"
                          // style={{ pointerEvents: "none" }}
                        />
                      </a>

                      <div>
                        {/* <a style="cursor: not-allowed;" data-toggle="modal" data-target="#notAvailable">Not Available</a> */}
                      </div>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
      {/* end::Body */}
    </div>
  );
};

export { FamilyDetailsTable };
