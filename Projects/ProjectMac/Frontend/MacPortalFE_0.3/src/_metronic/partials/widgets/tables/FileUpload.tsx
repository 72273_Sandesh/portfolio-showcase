// import { useState } from "react";

// export function FileUpload() {
//   const [file, setFile] = useState<File | null>(null);

//   const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
//     if (e.target.files) {
//       const selectedFile = e.target.files[0];
//       if (selectedFile.type === "application/pdf") {
//         setFile(selectedFile);
//       } else {
//         alert("Please select a PDF file.");
//       }
//     }
//   };

//   const handleUpload = async () => {
//     if (file) {
//       console.log("Uploading file...");

//       const formData = new FormData();
//       formData.append("file", file);

//       try {
//         const result = await fetch("https://httpbin.org/post", {
//           method: "POST",
//           body: formData,
//         });

//         const data = await result.json();

//         console.log(data);
//       } catch (error) {
//         console.error(error);
//       }
//     }
//   };

//   return (
//     <div className='menu menu-sub menu-sub-dropdown w-250px w-md-300px' data-kt-menu='true'>
//       <div className='px-7 py-5'>
//         <div className='fs-5 text-gray-900 fw-bolder'>Upload New Document</div>
//       </div>

//       <div className='separator border-gray-200'></div>

//       <div className='px-7 py-5'>
//         <div className='mb-10'>
//           <label className='form-label fw-bold'>Choose a PDF file from Your Device</label>

//           <div className="input-group">
//             <input
//               className='form-control form-control-white form-control-sm'
//               id="file"
//               type="file"
//               accept=".pdf"
//               onChange={handleFileChange}
//             />
//           </div>
//         </div>

//         {file && (
//           <section>
//             File details:
//             <ul>
//               <li>Name: {file.name}</li>
//               <li>Type: {file.type}</li>
//               <li>Size: {file.size} bytes</li>
//             </ul>
//           </section>
//         )}

//         <div className='d-flex justify-content-end'>
//           <button
//             type='button'
//             className='btn btn-sm btn-light btn-active-light-primary me-2'
//             onClick={() => setFile(null)}
//             data-kt-menu-dismiss='true'
//           >
//             Cancel
//           </button>

//           <button
//             onClick={handleUpload}
//             className='btn btn-sm btn-primary'
//             disabled={!file}
//           >
//             Upload a file
//           </button>
//         </div>
//       </div>
//     </div>
//   );
// }

// import axios from "axios";
// import { useState } from "react";
// import { toast } from "react-toastify";
// import { employeeIdForView } from "../../../../app/modules/GlobalQuery";

// const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// export function FileUpload() {
//   const [pdf, setPdf] = useState<File | null>(null);

//   const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
//     const file = e.target.files?.[0];
//     if (file) {
//       const reader = new FileReader();

//       reader.onloadend = () => {
//         // const base64String = reader.result;
//         // Now you have the Base64 string of the PDF file
//         // console.log(base64String);
//         // You can set the base64String to your state or perform other operations
//         setPdf(reader.result);
//       };

//       reader.readAsDataURL(file);
//     }
//   };

//   const handleUpload = async () => {
//     // formData.append("supportDoc", pdf);

//     axios
//       .post(`${EMPLOYEE_URL}/upload`, {
//         fileName: pdf?.name,
//         empCode: employeeIdForView,
//         hidDocsId: "",
//         supportDoc: pdf,
//       })
//       .then((response) => {
//         const { responseStatus, message } = response.data.data;
//         console.log("Basic Details Posted successfully!", response.data);

//         if (responseStatus === "E") {
//           toast.error(
//             <div>
//               <h4 className="alert-heading">Failed to submit Form...</h4>
//               <p className="alert alert-danger" role="alert">
//                 <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
//               </p>
//             </div>
//           );
//         } else {
//           toast.success(
//             <div>
//               <h4 className="alert-heading">Successfully Submitted Form...</h4>
//               <p className="alert alert-success" role="alert">
//                 <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
//               </p>
//             </div>
//           );
//         }
//       })
//       .catch((error) => {
//         console.error("Error posting data:", error);
//         toast.error(
//           <div>
//             <h6 className="alert-heading">Error Posting Data</h6>
//             <p className="alert alert-danger" role="alert">
//               <h6>{error.message}</h6>
//             </p>
//           </div>
//         );
//       });
//   };

//   return (
//     <div
//       className="menu menu-sub menu-sub-dropdown w-250px w-md-300px"
//       data-kt-menu="true"
//     >
//       <div className="px-7 py-5">
//         <div className="fs-5 text-gray-900 fw-bolder">Upload New Document</div>
//       </div>

//       <div className="separator border-gray-200"></div>

//       <div className="px-7 py-5">
//         <div className="mb-10">
//           <label className="form-label fw-bold">
//             Choose a PDF file from Your Device
//           </label>

//           <div className="input-group">
//             <input
//               className="form-control form-control-lg flex-grow-1"
//               type="file"
//               id="pdf"
//               onChange={handleFileChange}
//               accept="application/pdf"
//               required
//             />
//           </div>
//         </div>

//         <div className="d-flex justify-content-end">
//           <button
//             type="button"
//             className="btn btn-sm btn-light btn-active-light-primary me-2"
//             onClick={() => setPdf(null)}
//             data-kt-menu-dismiss="true"
//           >
//             Cancel
//           </button>

//           <button
//             onClick={handleUpload}
//             className="btn btn-sm btn-primary"
//             disabled={!pdf}
//           >
//             Upload a file
//           </button>
//         </div>
//       </div>
//     </div>
//   );
// }
//********************************************************************************************************//

import axios from "axios";
import { useState } from "react";
import { toast } from "react-toastify";
import { employeeIdForView } from "../../../../app/modules/GlobalQuery";
import { useAuth } from "../../../../app/modules/auth";

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

export function FileUpload() {
  const [file, setFile] = useState<Blob | File | null>(null);

  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const selectedFile = e.target.files[0];
      setFile(selectedFile);
    }
  };
  const {currentUser} = useAuth();
  const handleUpload = async () => {
    const formData = new FormData();
    formData.append("supportDocs", file);
    formData.append("fileNames", file?.name);
    // formData.append("empCode", employeeIdForView);
    // formData.append("hidDocsId", "");
    // formData.append("userId",currentUser.id);
    // formData.append("userIpAddress","ip");
    // formData.append("storageCode_Par","BOND");

    axios
      .post(`${EMPLOYEE_URL}/upload`, {...formData,
        empCode:employeeIdForView,
        hidDocsId:"",
        userId:currentUser?.id,
        userIpAddress:"ip",
        storageCode_Par:"BOND"
      })
      .then((response) => {
        const { responseStatus, message } = response.data.data;
        console.log("File uploaded successfully!", response.data);

        if (responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
              </p>
            </div>
          );
        } else {
          setFile(null);
          {<div data-kt-menu-dismiss="true"></div>}
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
              </p>
            </div>
          );
        }
      })
      .catch((error) => {
        console.error("Error posting data:", error);
        toast.error(
          <div>
            <h6 className="alert-heading">Error Posting Data</h6>
            <p className="alert alert-danger" role="alert">
              <h6>{error.message}</h6>
            </p>
          </div>
        );
      });
  };

  return (
    <div
      className="menu menu-sub menu-sub-dropdown w-250px w-md-300px"
      data-kt-menu="true"
    >
      <div className="px-7 py-5">
        <div className="fs-5 text-gray-900 fw-bolder">Upload New Document</div>
      </div>

      <div className="separator border-gray-200"></div>

      <div className="px-7 py-5">
        <div className="mb-10">
          <label className="form-label fw-bold">
            Choose a PDF file from Your Device
          </label>

          <div className="input-group">
            <input
              className="form-control form-control-lg flex-grow-1"
              type="file"
              id="pdf"
              onChange={handleFileChange}
              accept="application/pdf"
              required
            />
          </div>
          {file && (
            <section>
              File details:
              <ul>
                <li>Name: {file.name}</li>
                <li>Type: {file.type}</li>
                <li>Size: {file.size} bytes</li>
              </ul>
            </section>
          )}
        </div>

        <div className="d-flex justify-content-end">
          <button
            type="button"
            className="btn btn-sm btn-light btn-active-light-primary me-2"
            onClick={() => setFile(null)}
            data-kt-menu-dismiss="true"
          >
            Cancel
          </button>

          <button
            onClick={handleUpload}
            className="btn btn-sm btn-primary"
            disabled={!file}
          >
            Upload a file
          </button>
        </div>
      </div>
    </div>
  );
}
