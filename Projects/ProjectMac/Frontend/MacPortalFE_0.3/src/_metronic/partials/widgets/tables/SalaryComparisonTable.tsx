import React from "react";
import { KTIcon, toAbsoluteUrl } from "../../../helpers";
import { Dropdown1 } from "../../content/dropdown/Dropdown1";

type Props = {
  className: string;
};

const SalaryComparisonTable: React.FC<Props> = ({ className }) => {
  return (
    <div className={`card ${className}`}>
      {/* begin::Header */}
      <div className="card-header bg-light-primary border-0 pt-5">
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bold fs-3 mb-1">
            Department Wise Salary Comparison
          </span>
          <span className="text-muted mt-1 fw-semibold fs-7">
            Department : Information Systems
          </span>
        </h3>

        <div className="card-toolbar">
          <ul className="nav">
            <li className="nav-item">
              <a
                className="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary active fw-bold px-4 me-1"
                data-bs-toggle="tab"
                href="#kt_table_widget_5_tab_1"
              >
                Current Location
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary fw-bold px-4 me-1"
                data-bs-toggle="tab"
                href="#kt_table_widget_5_tab_2"
              >
                All Location
              </a>
            </li>
          </ul>
        </div>
      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className="card-body py-3">
        {/* begin::Table container */}
        <div className="table-responsive">
          {/* begin::Table */}
          <table className="table align-middle gs-0 gy-4">
            {/* begin::Table head */}
            <thead>
              <tr className="fw-bold text-muted bg-light">
                <th
                  className="ps-4 min-w-325px rounded-center"
                  style={{ color: "#9400D3" }}
                >
                  Index
                </th>
                <th className="min-w-125px" style={{ color: "green" }}>
                  Minimum
                </th>
                <th className="min-w-125px" style={{ color: "red" }}>
                  Maximum
                </th>
                <th className="min-w-200px" style={{ color: "orange" }}>
                  Average
                </th>
                <th className="min-w-150px" style={{ color: "blue" }}>
                  Total
                </th>
                {/* <th className='min-w-200px text-end rounded-end'></th> */}
              </tr>
            </thead>

            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
              <tr>
                <td>
                  <div className="d-flex align-items-center">
                    <div className="symbol symbol-50px me-5">
                      {/* <img
                        src={toAbsoluteUrl('media/stock/600x400/img-26.jpg')}
                        className=''
                        alt=''
                      /> */}
                      <span className="symbol-label bg-light-info">
                        <KTIcon
                          iconName="medal-star"
                          className="fs-2x text-info"
                        />
                      </span>
                    </div>
                    <div className="d-flex justify-content-start flex-column">
                      <a
                        href="#"
                        className="text-gray-900 fw-bold text-hover-primary mb-1 fs-6"
                      >
                        Experiance :
                      </a>
                    </div>
                  </div>
                </td>
                <td>
                  <a
                    href="#"
                    className="text-gray-900 fw-bold text-hover-primary d-block mb-1 fs-6"
                  >
                    0.10
                  </a>
                </td>
                <td>
                  <a
                    href="#"
                    className="text-gray-900 fw-bold text-hover-primary d-block mb-1 fs-6"
                  >
                    28.01
                  </a>
                </td>
                <td>
                  <a
                    href="#"
                    className="text-gray-900 fw-bold text-hover-primary d-block mb-1 fs-6"
                  >
                    31.03
                  </a>
                </td>
                <td>
                  <span className="badge badge-light-danger fs-7 fw-semibold">
                    403.39
                  </span>
                </td>
              </tr>
              <tr>
                <td>
                  <div className="d-flex align-items-center">
                    <div className="symbol symbol-50px me-5">
                      <span className="symbol-label bg-light-info">
                        <KTIcon
                          iconName="delivery-3"
                          className="fs-2x text-info"
                        />
                      </span>
                    </div>
                    <div className="d-flex justify-content-start flex-column">
                      <a
                        href="#"
                        className="text-gray-900 fw-bold text-hover-primary mb-1 fs-6"
                      >
                        Salary :
                      </a>
                    </div>
                  </div>
                </td>
                <td>
                  <a
                    href="#"
                    className="text-gray-900 fw-bold text-hover-primary d-block mb-1 fs-6"
                  >
                    263904
                  </a>
                </td>
                <td>
                  <a
                    href="#"
                    className="text-gray-900 fw-bold text-hover-primary d-block mb-1 fs-6"
                  >
                    10063284
                  </a>
                </td>
                <td>
                  <a
                    href="#"
                    className="text-gray-900 fw-bold text-hover-primary d-block mb-1 fs-6"
                  >
                    1665669
                  </a>
                </td>
                <td>
                  <span className="badge badge-light-danger fs-7 fw-semibold">
                    48304404
                  </span>
                </td>
              </tr>

              <tr>
                <td>
                  <div className="d-flex align-items-center">
                    <div className="symbol symbol-50px me-5">
                      <span className="symbol-label bg-light-info">
                        <KTIcon
                          iconName="profile-user"
                          className="fs-2x text-info"
                        />
                      </span>
                    </div>
                    <div className="d-flex justify-content-start flex-column">
                      <a
                        href="#"
                        className="text-gray-900 fw-bold text-hover-primary mb-1 fs-6"
                      >
                        Total Employee :
                      </a>
                    </div>
                  </div>
                </td>
                <td></td>
                <td>
                  {/* <a
                    href="#"
                    className="text-gray-900 fw-bold text-hover-primary d-block mb-1 fs-6"
                  >
                    <span className="badge badge-primary fs-7 fw-semibold">
                      View 29 Employees
                    </span>
                  </a> */}
                  <div className="card-toolbar">
          {/* begin::Menu */}
          <button
            type="button"
            className="btn btn-sm btn-primary me-3"
            data-kt-menu-trigger="click"
            data-kt-menu-placement="bottom-end"
            data-kt-menu-flip="top-end"
          >
            View 29 Employees
          </button>
          <Dropdown1 />
          {/* end::Menu */}
        </div>

                  
                </td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>
        {/* end::Table container */}
      </div>
      {/* begin::Body */}
    </div>
  );
};

export { SalaryComparisonTable };
