import React, { useEffect, useState } from "react";
import { KTIcon } from "../../../helpers";
import { Dropdown1 } from "../../content/dropdown/Dropdown1";
import axios from "axios";

type Props = {
  className: string;
};

type AssetDetail = {
  name: string;
  proficiency: number;

  induction: string;
  remarks: string;
  datE_FORMALITY: string;
  statuS_DATE: string;
  toT_AMT: string;
  adJ_AMT: string;
  status: string;
};

const TablesWidget3: React.FC<Props> = ({ className }) => {
  const [assetDetails, setAssetDetails] = useState<AssetDetail[]>([]);

  useEffect(() => {
    // Fetch family details from API
    axios
      .get("https://localhost:44360/api/Employee/H008601/ASSETS")
      .then((response) => {
        setAssetDetails(response.data);
      })
      .catch((error) => {
        console.error("Error fetching family details:", error);
      });
  }, []);

  function formatDate(dateString: string) {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0"); // Adding 1 because months are zero-based
    const year = date.getFullYear();
    return `${day}-${month}-${year}`;
  }

  return (
    <div className={`card ${className}`}>
      {/* begin::Header */}
      <div className="card-header border-0 pt-5">
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bold fs-3 mb-1">Assets List </span>
          <span className="text-muted mt-1 fw-semibold fs-7">
            {assetDetails.length} Assets
          </span>
          <span className="text-muted mt-1 fw-semibold fs-7"></span>
        </h3>
        <div className="card-toolbar">
          {/* begin::Menu */}
          <button
            type="button"
            className="btn btn-sm btn-primary me-3"
            data-kt-menu-trigger="click"
            data-kt-menu-placement="bottom-end"
            data-kt-menu-flip="top-end"
          >
            <KTIcon iconName="plus" className="fs-2" />
            Add Asset
          </button>
          <Dropdown1 />
          {/* end::Menu */}
        </div>
      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className="card-body py-3">
        {/* begin::Table container */}
        <div className="table-responsive">
          {/* begin::Table */}
          <table className="table align-middle gs-0 gy-3">
            {/* begin::Table head */}
            <thead>
              <tr>
                <th className="p-0 w-50px"></th>
                <th className="p-0 min-w-150px fw-bold">Name </th>
                <th className="p-0 min-w-150px fw-bold">Remarks </th>
                <th className="p-0 min-w-140px text-left fw-bold">
                  Effective Date
                </th>
                <th className="p-0 min-w-120px text-left fw-bold">
                  Complete Date
                </th>
                <th className="p-0 min-w-40px fw-bold"> Amount</th>
                <th className="p-0 min-w-40px fw-bold text-left">Adjust </th>
              </tr>
            </thead>
            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
              {assetDetails.map((assetDetail, index) => (
                <tr key={index}>
                  <td>
                    <div className="symbol symbol-50px me-2">
                      <span className="symbol-label bg-light-info">
                        <KTIcon iconName="laptop" className="fs-2x text-info" />
                      </span>
                    </div>
                  </td>
                  <td>
                    <a
                      href="#"
                      className="text-gray-900 fw-bold text-hover-primary fs-6"
                    >
                      {assetDetail.induction}
                    </a>
                    <span className="fw-bolder d-flex align-items-right">
                      <div className="badge badge-light-danger fw-bolder d-flex align-items-right">
                        {assetDetail.status}
                      </div>
                      {/* <div className='badge badge-light-success fw-bolder d-flex align-items-right'>{assetDetail.induction}</div> */}
                      {/* <div className='badge badge-light-info fw-bolder d-flex align-items-right'>{assetDetail.inducT_CODE}</div> */}
                      {/* "inducT_CODE": "A1002",
    "induction": "LAPTOP",
    "datE_FORMALITY": "2010-08-30T00:00:00",
    "remarks": "Hp Compaq 510",
    "status": "PENDING",
    "statuS_DATE": "2050-12-31T00:00:00",
    "toT_AMT": 0,
    "adJ_AMT": 0 */}
                    </span>
                  </td>

                  <td>
                    <a
                      href="#"
                      className="text-gray-900 fw-bold text-hover-primary fs-6"
                    >
                      {assetDetail.remarks}
                    </a>
                    {/* <span className='fw-bolder d-flex align-items-right'>
                      <div className='badge badge-light-danger fw-bolder d-flex align-items-right'>{assetDetail.status}</div>
                      <div className='badge badge-light-success fw-bolder d-flex align-items-right'>{assetDetail.induction}</div>
               
                      
                      </span> */}
                  </td>

                  <td className="text-left text-muted fw-semibold">
                    <span className="fw-bolder d-flex align-items-right">
                      <div className="badge badge-light-danger fw-bolder d-flex align-items-right">
                        {formatDate(assetDetail.datE_FORMALITY)}
                      </div>
                    </span>
                  </td>
                  <td className="text-left text-muted fw-semibold">
                    <span className="fw-bolder d-flex align-items-right">
                      <div className="badge badge-light-danger fw-bolder d-flex align-items-right">
                        {formatDate(assetDetail.statuS_DATE)}
                      </div>
                    </span>
                  </td>
                  <td className="text-end text-muted fw-semibold">
                    <span className="fw-bolder d-flex align-items-left">
                      <div className="badge badge-light-success fw-bolder d-flex align-items-right">
                        {assetDetail.toT_AMT}
                      </div>
                    </span>
                  </td>
                  <td className="text-end text-muted fw-semibold">
                    <span className="fw-bolder d-flex align-items-right">
                      <div className="badge badge-light-danger fw-bolder d-flex align-items-right">
                        {assetDetail.adJ_AMT}
                      </div>
                    </span>
                  </td>
                </tr>
              ))}
            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>
        {/* end::Table container */}
      </div>
      {/* begin::Body */}
    </div>
  );
};

export { TablesWidget3 };
