import { useQuery } from "react-query";
import { isNotEmpty, QUERIES } from "../../../../../../_metronic/helpers";
import { useListView } from "../core/ListViewProvider";
import { JourneyDetailsEditModalForm } from "./JourneyDetailsEditModalForm";
import { getJourneyDetailsById } from "../core/_requests";

const JourneyDetailsEditModalFromWrapper = () => {
  const { itemIdForUpdate, setItemIdForUpdate } = useListView();
  const enabledQuery: boolean = isNotEmpty(itemIdForUpdate);
  const {
    isLoading,
    data: JourneyDetails,
    error,
  } = useQuery(
    `${QUERIES.JOURNEY_DETAILS_LIST}-JourneyDetails-${itemIdForUpdate}`,
    () => {
      return getJourneyDetailsById(itemIdForUpdate);
    },
    {
      cacheTime: 0,
      enabled: enabledQuery,
      onError: (err) => {
        setItemIdForUpdate(undefined);
        console.error(err);
      },
    }
  );

  if (!itemIdForUpdate) {
    return (
      <JourneyDetailsEditModalForm
        isBankDetailsLoading={isLoading}
        bankDetails={{ id: undefined }}
      />
    );
  }

  if (!isLoading && !error && JourneyDetails) {
    return (
      <JourneyDetailsEditModalForm
        isBankDetailsLoading={isLoading}
        bankDetails={JourneyDetails}
      />
    );
  }

  return null;
};

export { JourneyDetailsEditModalFromWrapper };
