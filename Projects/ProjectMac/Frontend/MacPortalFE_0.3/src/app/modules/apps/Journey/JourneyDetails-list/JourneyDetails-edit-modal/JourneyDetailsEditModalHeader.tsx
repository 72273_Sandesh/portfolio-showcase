import { useEffect, useState } from "react";
import { KTIcon } from "../../../../../../_metronic/helpers";
import { globalVariable, setGlobalVariable } from "../../GlobalVariable";
import { useListView } from "../core/ListViewProvider";

const JourneyDetailsEditModalHeader = () => {
  const [headerName, setHeaderName] = useState("");

  useEffect(() => {
    const getHeaderName = () => {
      if (globalVariable === "addJourneyDetails") return "Add New Journey Details";
      else if (globalVariable === "editJourneyDetails") return "Edit Journey Details";
      else return "View Journey Details";
    };
    
    setHeaderName(getHeaderName);
  }, []);

  

  const { setItemIdForUpdate } = useListView();
  const openAddModal = () => {
    setItemIdForUpdate(undefined);
    setGlobalVariable("addJourneyDetails");
  };
  return (
    <div className="modal-header">
      {/* begin::Modal title */}
        <h2 className="fw-bolder">{headerName}</h2>
      
      {/* end::Modal title */}

      {/* begin::Close */}
      <div
        className="btn btn-icon btn-sm btn-active-icon-primary"
        data-kt-users-modal-action="close"
        onClick={openAddModal}
        style={{ cursor: "pointer" }}
      >
        <KTIcon iconName="cross" className="fs-1" />
      </div>
      {/* end::Close */}
    </div>
  );
};

export { JourneyDetailsEditModalHeader };
