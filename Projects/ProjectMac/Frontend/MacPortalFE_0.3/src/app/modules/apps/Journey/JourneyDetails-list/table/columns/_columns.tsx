import { Column } from "react-table";
import { JourneyDetails } from "../../core/_models";
import { JourneyDetailsActionsCell } from "./JourneyDetailsActionsCell";
import { JourneyDetailsCustomHeader } from "./JourneyDetailsCustomHeader";
import { JourneyDetailsLastLoginCell } from "./JourneyDetailsLastLoginCell";
import { JourneyDetailsTwoStepsCell } from "./JourneyDetailsTwoStepsCell";
import { JourneyDetailsInfoCell } from "./JourneyDetailsInfoCell";
import { JourneyDetailsChangeUser } from "./JourneyDetailsChangeUser";
const JourneyDetailsColumns: ReadonlyArray<Column<JourneyDetails>> = [
  {
    Header: (props) => (
      <JourneyDetailsCustomHeader
        tableProps={props}
        title="EMPLOYEE NAME"
        className="min-w-125px text-left "
      />
    ),
    id: "name",
    Cell: ({ ...props }) => (
      <JourneyDetailsInfoCell
        bankDetails={props.data[props.row.index]}
        id={props.data[props.row.index].id}
      />
    ),
  },
  {
    Header: (props) => (
      <JourneyDetailsCustomHeader
        tableProps={props}
        title="TRANSACTION ID"
        className="min-w-125px text-left"
      />
    ),
    accessor: "changeUser",
    Cell: ({ ...props }) => (
      <JourneyDetailsChangeUser
        changeUser={props.data[props.row.index].changeUser}
      />
    ),
  },
  {
    Header: (props) => (
      <JourneyDetailsCustomHeader
        tableProps={props}
        title="TRANSACTION DATE"
        className="min-w-125px text-left"
      />
    ),
    id: "changeDate",
    Cell: ({ ...props }) => (
      <JourneyDetailsLastLoginCell
        changeDate={props.data[props.row.index].changeDate ?? new Date()}
      />
    ),
  },
  {
    Header: (props) => (
      <JourneyDetailsCustomHeader
        tableProps={props}
        title="STATUS"
        className="min-w-125px text-left"
      />
    ),
    accessor: "status",
    Cell: ({ value }) => (
      <span>
        <div className=" fw-bolder d-flex align-items-right">
          <div className="d-flex align-items-right">
            {
              <div className="d-flex align-items-right">
                {value === "A" ? (
                  <div className="badge badge-light-success fw-bolder d-flex align-items-right">
                    ACTIVE
                  </div>
                ) : value === "N" ? (
                  <div className="badge badge-light-danger fw-bolder d-flex align-items-right">
                    INACTIVE
                  </div>
                ) : (
                  <div className="badge d-flex align-items-right">&nbsp;</div>
                )}
              </div>
            }
          </div>
        </div>
      </span>
    ),
  },
  {
    Header: (props) => (
      <JourneyDetailsCustomHeader
        tableProps={props}
        title="WF_PROCESS_LEVEL"
        className="min-w-125px text-left"
      />
    ),
    id: "deptHead",
    Cell: ({ ...props }) => (
      <JourneyDetailsTwoStepsCell
        deptHead={props.data[props.row.index].deptHead}
      />
    ),
  },
  {
    Header: (props) => (
      <JourneyDetailsCustomHeader
        tableProps={props}
        title="VISIBLITY"
        className="min-w-125px text-left"
      />
    ),
    id: "changeDate1",
    Cell: ({ ...props }) => (
      <JourneyDetailsLastLoginCell
        changeDate={props.data[props.row.index].changeDate ?? new Date()}
      />
    ),
  },
  {
    Header: (props) => (
      <JourneyDetailsCustomHeader
        tableProps={props}
        title="STATE"
        className="min-w-125px text-left"
      />
    ),
    id: "changeDate2",
    Cell: ({ ...props }) => (
      <JourneyDetailsLastLoginCell
        changeDate={props.data[props.row.index].changeDate ?? new Date()}
      />
    ),
  },
  {
    Header: (props) => (
      <JourneyDetailsCustomHeader
        tableProps={props}
        title="Actions"
        className="min-w-100px text-left"
      />
    ),
    id: "actions",
    Cell: ({ ...props }) => (
      <JourneyDetailsActionsCell id={props.data[props.row.index].id} />
    ),
  },
];

export { JourneyDetailsColumns };
