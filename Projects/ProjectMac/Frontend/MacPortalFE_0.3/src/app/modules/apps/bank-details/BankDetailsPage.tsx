import {Route, Routes, Outlet, Navigate} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { BankDetailsListWrapper } from './bankDetails-list/BankDetailsList';

const BankDetailsBreadcrumbs: Array<PageLink> = [
  {
    title: 'Transactions - ',
    path: '/apps/bankDetails/bankDetails',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const BankDetailsPage = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='bankDetails'
          element={
            <>
              <PageTitle breadcrumbs={BankDetailsBreadcrumbs}>Employee</PageTitle>
              <BankDetailsListWrapper />
              <ToastContainer/>
            </>
          }
        />
      </Route>
      <Route index element={<Navigate to='/apps/bank-details/users' />} />
    </Routes>
  )
}

export default BankDetailsPage
