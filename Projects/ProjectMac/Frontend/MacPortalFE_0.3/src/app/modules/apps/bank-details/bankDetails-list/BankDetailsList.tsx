import { KTCard } from "../../../../../_metronic/helpers"
import { Content } from "../../../../../_metronic/layout/components/content"
import { ToolbarWrapper } from "../../../../../_metronic/layout/components/toolbar"
import { BankDetailsEditModal } from "./bankDetails-edit-modal/BankDetailsEditModal"
import { BankDetailsListHeader } from "./components/header/BankDetailsListHeader"
import { ListViewProvider, useListView } from "./core/ListViewProvider"
import { QueryRequestProvider } from "./core/QueryRequestProvider"
import { QueryResponseProvider } from "./core/QueryResponseProvider"
import { BankDetailsTable } from "./table/BankDetailsTable"

const BankDetailsList = () => {
  const {itemIdForUpdate} = useListView()
  return (
    <>
      <KTCard>
        <BankDetailsListHeader />
        <BankDetailsTable />
      </KTCard>
      {itemIdForUpdate !== undefined && <BankDetailsEditModal />}
    </>
  )
}

const BankDetailsListWrapper = () => (
  <QueryRequestProvider>
    <QueryResponseProvider>
      <ListViewProvider>
        <ToolbarWrapper />
        <Content>
          <BankDetailsList />
        </Content>
      </ListViewProvider>
    </QueryResponseProvider>
  </QueryRequestProvider>
)

export {BankDetailsListWrapper}
