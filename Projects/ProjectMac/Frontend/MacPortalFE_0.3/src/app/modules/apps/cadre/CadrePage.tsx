import {Route, Routes, Outlet, Navigate} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {CadreListWrapper} from './cadre-list/CadreList'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const CadreBreadcrumbs: Array<PageLink> = [
  {
    title: 'Master',
    path: '/apps/cadre/cadres',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const CadrePage = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='cadres'
          element={
            <>
              <PageTitle breadcrumbs={CadreBreadcrumbs}>Cadre List</PageTitle>
              <CadreListWrapper />
              <ToastContainer/>
            </>
          }
        />
      </Route>
      <Route index element={<Navigate to='/apps/user-management/users' />} />
    </Routes>
  )
}

export default CadrePage
