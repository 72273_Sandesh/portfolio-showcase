import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { isNotEmpty, Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { initialCadre, Cadre, createCadretx } from "../core/_models";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
import { CadreListLoading } from "../components/loading/CadreListLoading";
import { createCadre, insertTransaction, updateCadre } from "../core/_requests";
import { useQueryResponse } from "../../../transaction-master/master-list/core/QueryResponseProvider";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useQueryClient, useMutation } from "react-query";
const API_URL = import.meta.env.VITE_CADRE_DROPDOWNS_API_URL;

type Props = {
  isCadreLoading: boolean;
  cadre: Cadre;
};

const editCadreSchema = Yup.object().shape({
  sH_DESCR: Yup.string().required("Short Description is required"),
  descr: Yup.string().required("Description is required"),
  chG_REASON: Yup.string(),
  cadreCategory: Yup.string().required("Category is required"),
  status: Yup.string().required("Status is required"),
});

interface cadreCategory {
  banD_CODE: string;
  descr: string;
}

const CadreEditModalForm: FC<Props> = ({ cadre, isCadreLoading }) => {
  //import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
  const { setItemIdForUpdate, formMode } = useListView();
  const { setItemIdForUpdate: closeForm } = useTxListView();
  // closeForm(undefined);

  const { refetch } = useQueryResponse();

  const [cadreCategorys, setCadreCategorys] = useState<cadreCategory[]>([]);
  const [chG_REASON, setchG_REASON] = useState<string>("");

  const [statuses] = useState(["ACTIVE", "INACTIVE"]);
  // const [status, setStatus] = useState("");

  const { currentUser } = useAuth();
  const navigate = useNavigate();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();

  useEffect(() => {
    axios
      .get(API_URL)
      .then((response) => {
        setCadreCategorys(response.data.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const [cadreForEdit] = useState<Cadre>({
    ...cadre,
    descr:
      cadre.descr || initialCadre.descr || cadre.fieldNames?.descr?.neW_VALUE,
    sH_DESCR:
      cadre.sH_DESCR ||
      initialCadre.sH_DESCR ||
      cadre.fieldNames?.sH_DESCR?.neW_VALUE,
    chG_TERM:
      cadre.chG_TERM ||
      initialCadre.chG_TERM ||
      cadre.fieldNames?.chG_TERM?.neW_VALUE,
    chG_DATE:
      cadre.chG_DATE ||
      initialCadre.chG_DATE ||
      cadre.fieldNames?.chG_DATE?.neW_VALUE,
    chG_USER:
      cadre.chG_USER ||
      initialCadre.chG_USER ||
      cadre.fieldNames?.chG_USER?.neW_VALUE,
    cadreCategory:
      cadre.banD_CODE ||
      initialCadre.banD_CODE ||
      cadre.fieldNames?.banD_CODE?.neW_VALUE,
    status:
      cadre.status ||
      initialCadre.status ||
      cadre.fieldNames?.status?.neW_VALUE,
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
    closeForm(undefined);
  };

  const createTx = useMutation((payload: any) => insertTransaction(payload), {
    onSuccess: (response: any) => {
      queryClient.invalidateQueries([
        `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
      ]);

      setItemIdForUpdate(undefined);
      closeForm(undefined);
      toast.success(
        <div>
          <h4 className="alert-heading">Successfully Submitted Form...</h4>
          <p className="alert alert-success" role="alert">
            <h6>{response?.message}</h6>
          </p>
        </div>
      );
      cancel(true);
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Failed to submit Form...</h4>
          <p className="alert alert-danger" role="alert">
            <h6>{response?.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formik = useFormik({
    initialValues: cadreForEdit,
    validationSchema: editCadreSchema,

    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      if (isNotEmpty(values.id)) {
        const payload: createCadretx = {
          tableName: "CADRE",
          keyVar: "",
          wfLevel: 1,
          conf: "N",
          siteCode: "S0001",
          emp: currentUser?.id?.toString() || "",
          filePath: null,
          file: null,
          strTranId: values.fieldNames ? values.id : "",
          folderPath: null,
          chG_USER: currentUser?.id?.toString() || "",
          chG_TERM: "TERM",
          remarks: cadre.chG_REASON,
          chG_DATE: new Date(),

          fieldUpdates: [
            {
              fieldName: "cadrE_CODE",
              oldValue:
                cadre.cadrE_CODE ||
                cadre.fieldNames?.cadrE_CODE?.olD_VALUE ||
                "",
              newValue:
                cadre.cadrE_CODE ||
                cadre.fieldNames?.cadrE_CODE?.olD_VALUE ||
                "",
            },
            {
              fieldName: "descr",
              oldValue: cadre.descr || cadre.fieldNames?.descr?.olD_VALUE,
              newValue: values.descr,
            },
            {
              fieldName: "sH_DESCR",
              oldValue: cadre.sH_DESCR || cadre.fieldNames?.sH_DESCR?.olD_VALUE,
              newValue: values.sH_DESCR,
            },
            {
              fieldName: "banD_CODE",
              oldValue:
                cadre.cadreCategory || cadre.fieldNames?.banD_CODE?.olD_VALUE,
              newValue: values.cadreCategory, //|| cadre.banD_CODE,
            },
            {
              fieldName: "chG_USER",
              oldValue: cadre.chG_USER || cadre.fieldNames?.chG_USER?.olD_VALUE,
              newValue: currentUser?.id.toString() || "",
            },
            {
              fieldName: "chG_TERM",
              oldValue: cadre.chG_TERM || cadre.fieldNames?.chG_TERM?.olD_VALUE,
              newValue: "TERM",
            },
            {
              fieldName: "chG_DATE",
              oldValue: cadre.chG_DATE || cadre.fieldNames?.chG_DATE?.olD_VALUE,
              newValue: new Date(),
            },
            {
              fieldName: "status",
              oldValue: cadre.status || cadre.fieldNames?.status?.olD_VALUE,
              newValue: values.status || cadre.status,
            },
          ],
        };
        createTx.mutate(payload);

        setSubmitting(false);
      } else {
        const payload: createCadretx = {
          tableName: "CADRE",
          keyVar: "",
          wfLevel: 1,
          conf: "N",
          siteCode: "S0001",
          emp: currentUser?.id?.toString() || "",
          filePath: null,
          file: null,
          folderPath: null,
          chG_USER: currentUser?.id?.toString() || "",
          chG_TERM: "TERM",
          chG_DATE: new Date(),
          remarks: "New Cadre Added",

          fieldUpdates: [
            {
              fieldName: "cadrE_CODE",
              oldValue: "",
              newValue: "",
            },
            {
              fieldName: "descr",
              oldValue: "",
              newValue: values.descr,
            },
            {
              fieldName: "sH_DESCR",
              oldValue: "",
              newValue: values.sH_DESCR,
            },
            {
              fieldName: "banD_CODE",
              oldValue: "",
              newValue: values.cadreCategory,
            },
            {
              fieldName: "status",
              oldValue: "",
              newValue: values.status,
            },
            {
              fieldName: "chG_USER",
              oldValue: "",
              newValue: currentUser?.id.toString() || "",
            },
            {
              fieldName: "chG_TERM",
              oldValue: "",
              newValue: "TERM",
            },
            {
              fieldName: "chG_DATE",
              oldValue: "",
              newValue: new Date(),
            },
          ],
        };

        createTx.mutate(payload);
        setSubmitting(false);
      }
    },
  });

  useEffect(() => {
    if (formMode === Mode.ADD) {
      formik.setFieldValue("status", "A");
    }
  }, []);

  function formatDate(dateString: string) {
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata", // Specify the Indian time zone
    });
    return `${formattedDate} ${formattedTime}`;
  }
  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column" id="kt_modal_add_user_scroll">
          <div className="row mb-3">
            {formMode !== Mode.ADD && (
              <div className="">
                <div className="fv-row">
                  {cadre.fieldNames ? (
                    <label className=" fw-bold fs-6 mb-2">
                      Transaction ID :
                    </label>
                  ) : (
                    <label className=" fw-bold fs-6 mb-2">Cadre Code :</label>
                  )}
                  <input
                    className="form-control form-control-solid mb-3 mb-lg-0"
                    value={formik.values.id ? formik.values.id : ""}
                    disabled
                  />
                </div>
              </div>
            )}

            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Name of Cadre
                </label>
                <input
                  placeholder="Name"
                  value={formik.values.descr?.toUpperCase()}
                  // {...formik.getFieldProps('descr')}
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "is-invalid": formik.touched.descr && formik.errors.descr,
                    },
                    { "is-valid": formik.touched.descr && !formik.errors.descr }
                  )}
                  type="text"
                  name="descr"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isCadreLoading ||
                    formMode === Mode.VIEW
                  }
                  onChange={(e) => {
                    formik.setFieldValue("descr", e.target.value.toUpperCase());
                  }}
                />
                {formik.touched.descr && formik.errors.descr && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.descr}</span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  {" "}
                  Short Description
                </label>
                <input
                  placeholder="Short Description"
                  value={formik.values.sH_DESCR?.toUpperCase()}
                  // {...formik.getFieldProps('sH_DESCR')}
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.sH_DESCR && formik.errors.sH_DESCR,
                    },
                    {
                      "is-valid":
                        formik.touched.sH_DESCR && !formik.errors.sH_DESCR,
                    }
                  )}
                  type="text"
                  name="sH_DESCR"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isCadreLoading ||
                    formMode === Mode.VIEW
                  }
                  onChange={(e) => {
                    formik.setFieldValue(
                      "sH_DESCR",
                      e.target.value.toUpperCase()
                    );
                  }}
                />
                {formik.touched.sH_DESCR && formik.errors.sH_DESCR && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.sH_DESCR}</span>
                  </div>
                )}
              </div>
            </div>

            {/* {begin : Category} */}
            <div className="col-md-6">
              <div className="fv-row">
                <label className="fw-bold fs-6 mb-2">Cadre Category:</label>
                <select
                  className={clsx(
                    "form-select form-select-solid fw-bolder",
                    {
                      "is-invalid":
                        formik.touched.cadreCategory &&
                        formik.errors.cadreCategory,
                    },
                    {
                      "is-valid":
                        formik.touched.cadreCategory &&
                        !formik.errors.cadreCategory,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("cadreCategory", e.target.value);
                  }}
                  value={formik.values.cadreCategory}
                >
                  <option value=""></option>
                  {cadreCategorys.map((cadreCategory) => (
                    <option
                      key={cadreCategory.banD_CODE}
                      value={cadreCategory.banD_CODE}
                    >
                      {cadreCategory.descr}
                    </option>
                  ))}
                </select>

                {formik.touched.cadreCategory &&
                  formik.errors.cadreCategory && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.cadreCategory}</span>
                    </div>
                  )}
              </div>
            </div>
            {/* {end : Category} */}

            <div className="col-md-6">
              <div className="fv-row">
                <label className="fw-bold fs-6 mb-2">Status:</label>

                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  value={formik.values.status}
                  // onChange={formik.handleChange}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                  }}
                  disabled={formMode === Mode.ADD || formMode === Mode.VIEW}
                >
                  <option value=""></option>
                  {statuses.map((status) => (
                    <option value={status === "ACTIVE" ? "A" : "N"}>
                      {status}
                    </option>
                  ))}
                </select>

                {formik.touched.status && formik.errors.status && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.status}</span>
                  </div>
                )}
              </div>
            </div>

            {formMode === Mode.VIEW && (
              <div className="col-md-6">
                <div className="fv-row">
                  <label className="fw-bold fs-6 mb-2"> Change Term</label>
                  <input
                    placeholder=""
                    {...formik.getFieldProps("chG_TERM")}
                    className={clsx(
                      "form-control form-control-solid mb-3 mb-lg-0",
                      {
                        "is-invalid":
                          formik.touched.chG_TERM && formik.errors.chG_TERM,
                      },
                      {
                        "is-valid":
                          formik.touched.chG_TERM && !formik.errors.chG_TERM,
                      }
                    )}
                    type="text"
                    name="chG_TERM"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isCadreLoading ||
                      formMode === Mode.VIEW
                    }
                  />
                  {formik.touched.chG_TERM && formik.errors.chG_TERM && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.chG_TERM}</span>
                    </div>
                  )}
                </div>
              </div>
            )}

            {formMode === Mode.VIEW && (
              <div className="col-md-6">
                <div className="fv-row">
                  <label className="fw-bold fs-6 mb-2"> Change User</label>
                  <input
                    placeholder=""
                    {...formik.getFieldProps("chG_USER")}
                    className={clsx(
                      "form-control form-control-solid mb-3 mb-lg-0",
                      {
                        "is-invalid":
                          formik.touched.chG_USER && formik.errors.chG_USER,
                      },
                      {
                        "is-valid":
                          formik.touched.chG_USER && !formik.errors.chG_USER,
                      }
                    )}
                    type="text"
                    name="chG_USER"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isCadreLoading ||
                      formMode === Mode.VIEW
                    }
                  />
                  {formik.touched.chG_USER && formik.errors.chG_USER && (
                    <div className="fv-plugins-message-container">
                      <span role="alert">{formik.errors.chG_USER}</span>
                    </div>
                  )}
                </div>
              </div>
            )}

            {formMode === Mode.VIEW && (
              <div className="col-md-6">
                <div className="fv-row">
                  <label className="fw-bold fs-6 mb-2"> Change Date</label>
                  <input
                    placeholder=""
                    {...formik.getFieldProps("chG_DATE")}
                    className={clsx(
                      "form-control form-control-solid mb-3 mb-lg-0",
                      {
                        "is-invalid":
                          formik.touched.chG_DATE && formik.errors.chG_DATE,
                      },
                      {
                        "is-valid":
                          formik.touched.chG_DATE && !formik.errors.chG_DATE,
                      }
                    )}
                    type="text"
                    name="chG_DATE"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isCadreLoading ||
                      formMode === Mode.VIEW
                    }
                    value={
                      formik.values.chG_DATE
                        ? formatDate(formik.values?.chG_DATE.toString())
                        : ""
                    }
                    // value={
                    //   formik.values.chG_DATE
                    //     ? formik.values?.chG_DATE.toISOString()
                    //     : ""
                    // }
                  />
                  {formik.touched.chG_DATE && formik.errors.chG_DATE && (
                    <div className="fv-plugins-message-container">
                      <span role="alert">{formik.errors.chG_DATE}</span>
                    </div>
                  )}
                </div>
              </div>
            )}
          </div>
          {formMode !== Mode.ADD && (
            <div className="row-mb-3">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Reason For Edit
                </label>
                <textarea
                  className="required form-control"
                  // value={chG_REASON}
                  // {...(formMode===Mode.VIEW
                  //   ? { ...formik.getFieldProps("chG_REASON") }
                  //   : chG_REASON)}
                  {...(formMode === Mode.VIEW
                    ? { ...formik.getFieldProps("chG_REASON") }
                    : { chG_REASON })}
                  //onChange={(e) => setReason(e.target.value)}
                  onChange={(e) => {
                    formik.setFieldValue(
                      "chG_REASON",
                      e.target.value.toUpperCase()
                    );
                    setchG_REASON(e.target.value.toUpperCase());
                  }}
                  rows={3}
                  {...(formMode === Mode.VIEW
                    ? { placeholder: "" }
                    : { placeholder: "This is Mandatory Field" })}
                  //  placeholder="This is Mandatory Field"
                  disabled={formMode === Mode.VIEW}
                ></textarea>
                {formik.touched.chG_REASON && formik.errors.chG_REASON && (
                  <div className="fv-plugins-message-container">
                    <span role="alert">{formik.errors.chG_REASON}</span>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
        <div className="text-center pt-15">
          {formMode !== Mode.VIEW && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isCadreLoading ||
                formik.isSubmitting ||
                !formik.touched ||
                (formMode !== Mode.ADD && chG_REASON === "")
              }
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isCadreLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isCadreLoading) && <CadreListLoading />}
    </>
  );
};

export { CadreEditModalForm };
