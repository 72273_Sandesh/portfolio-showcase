import { ID, Response } from "../../../../../../_metronic/helpers";
export type Cadre = {
  //previous cadre fields
  id?: ID;
  cadrE_CODE?: string;
  descr?: string;
  sH_DESCR?: string;
  lvE_TBLNO?: string;
  chG_DATE?: Date | string;
  chG_USER?: string;
  chG_TERM?: string;
  udF_NUM1?: number | string | null;
  cadrE_ID?: string;
  cadrE_ORDER?: number;
  banD_CODE?: string;
  mkit?: number;
  mmed?: number;
  chG_REASON?: string;
  cadreCategory?: string;
  status?: string;
  responseStatus?: string;
  message?: string;

  //new fields used for tx
  wF_PROCESS_LEVEL?: number;
  emP_CODE?: string;
  remarks?: string;
  SessionempCode?: string;
  moduleType?: string;
  fieldNames?: {
    cadrE_CODE: { olD_VALUE: string; neW_VALUE: string };
    descr: { olD_VALUE: string; neW_VALUE: string };
    sH_DESCR: { olD_VALUE: string; neW_VALUE: string };
    banD_CODE: { olD_VALUE: string; neW_VALUE: string };
    chG_USER?: { olD_VALUE: string; neW_VALUE: string };
    chG_TERM?: { olD_VALUE: string; neW_VALUE: string };
    chG_DATE?: { olD_VALUE: string; neW_VALUE: string };
    status: { olD_VALUE: string; neW_VALUE: string };
  };
};

//model for create and update tx payload
export type createCadretx = {
  responseStatus?: string;
  message?: string;
  tableName: string;
  keyVar: string;
  wfLevel: number;
  conf: string;
  siteCode: string;
  emp: string;
  filePath: string | null;
  file: string | null;
  folderPath: string | null;
  chG_DATE?: Date | string;
  strTranId?: string | null | number;
  chG_USER?: string;
  chG_TERM?: string;
  remarks?: string;
  fieldUpdates: [
    {
      fieldName: string;
      oldValue: string | undefined;
      newValue: string | undefined;
    }
  ];
};

export type CadreQueryResponse = Response<Array<Cadre>>;

export const initialCadre: Cadre = {};
