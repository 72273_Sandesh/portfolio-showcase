import {FC} from 'react'
import { Cadre } from '../../core/_models'

type Props = {
  cadre : Cadre
  // sH_DESCR:string

}

const CadreTwoStepsCell: FC<Props> = ({cadre}) => (
  <>
  <div className=' fw-bolder d-flex align-items-right'> <div className=''>{cadre.cadreCategory ?(cadre.cadreCategory.toUpperCase()):(<div className='badge badge-danger fw-bolder d-flex align-items-right'>NOT ASSIGNED</div>)}</div></div>
  {/* //badge badge-light-success fw-bolder d-flex align-items-center = > this class is for showing Green Box to content 
//<div className=' fw-bolder d-flex align-items-right'>
//{chG_DATE ?(formattedDate): <div className='badge badge-danger fw-bolder d-flex align-items-right'>Not Assigned</div>}
//</div> */}
{/* <span >
<div className=' fw-bolder d-flex align-items-right'>
  <div className='d-flex align-items-right'>{cadre.status === 'A' ? (<div className='badge badge-light-success fw-bolder d-flex align-items-right'>INACTIVE</div>) :(<div className='badge badge-light-danger fw-bolder d-flex align-items-right'>INACTIVE</div>) }</div>
</div>
</span> */}
</>
  )

export {CadreTwoStepsCell}
