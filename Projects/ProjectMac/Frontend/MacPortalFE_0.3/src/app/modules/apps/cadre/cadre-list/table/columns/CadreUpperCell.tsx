// import {FC} from 'react'

// type Props = {
//   chG_DATE:Date
// }

// const DepartmentLastLoginCell: FC<Props> = ({chG_DATE}) => (
  
//   <div className='badge badge-light fw-bolder d-flex align-items-right '>{chG_DATE}</div>
// )

// export {DepartmentLastLoginCell}


import React from 'react';

type Props = {
    chG_USER?: string;
};

const CadreUpperCell: React.FC<Props> = ({ chG_USER }) => {
//   const formattedDate = new Date(chG_DATE).toLocaleString('en-GB', {
//     day: '2-digit',
//     month: '2-digit',
//     year: 'numeric',
    // hour: '2-digit',
    // minute: '2-digit',
    // second: '2-digit',

  return (
    <div className=' fw-bolder d-flex align-items-right'>
      {chG_USER ?(chG_USER.toUpperCase()): <div className='badge badge-danger fw-bolder d-flex align-items-right'>NOT ASSSIGNED</div>}
    </div>


  );
};

export { CadreUpperCell };
