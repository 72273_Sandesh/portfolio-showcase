import {Route, Routes, Outlet, Navigate} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {DepartmentListWrapper} from './department-list/DepartmentList'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const DepartmentBreadcrumbs: Array<PageLink> = [
  {
    title: 'Master',
    path: '/apps/department/departments',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const DepartmentPage = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='departments'
          element={
            <>
              <PageTitle breadcrumbs={DepartmentBreadcrumbs}>Department List</PageTitle>
              <DepartmentListWrapper />
              <ToastContainer/>
            </>
          }
        />
      </Route>
      <Route index element={<Navigate to='/apps/user-management/users' />} />
    </Routes>
  )
}

export default DepartmentPage
