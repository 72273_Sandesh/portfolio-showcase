import { ID, Response } from "../../../../../../_metronic/helpers";
export type Department = {
  id?: ID;
  deptCode?: string;
  dept_Code?:string;
  name?: string;
  changeUser?: string;
  deptHead?: string;
  changeDate?: Date | string;
  shortDescription?: string;
  changeTerm?: string;
  status?: string;
  acctCode?: string;
  payTable?: string;
  cctrCode?: string;
  acctCodeAp?: string;
  cctrCodeAp?: string;
  udfNumber1?: number;
  shDeptCode?: string;
  changeReason?: string | null;
  newReason?: string | null;
  deptHeadName?: string;
  responseStatus?: string;
  message?: string;

  wF_PROCESS_LEVEL?: number;
  emP_CODE?: string;
  remarks?: string;
  SessionempCode?: string;
  moduleType?: string;
  fieldNames?: {
    DEPT_CODE: { olD_VALUE: string; neW_VALUE: string };
    DESCR: { olD_VALUE: string; neW_VALUE: string };
    SH_DESCR: { olD_VALUE: string; neW_VALUE: string };
    DEPT_HEAD: { olD_VALUE: string; neW_VALUE: string };
    CHG_USER: { olD_VALUE: string; neW_VALUE: string };
    CHG_TERM: { olD_VALUE: string; neW_VALUE: string };
    CHG_DATE: { olD_VALUE: string; neW_VALUE: string };
    status: { olD_VALUE: string; neW_VALUE: string };
  };
};

export type DepartmentTx = {
  responseStatus?: string;
  message?: string;
  tableName: string;
  keyVar: string;
  wfLevel: number;
  conf: string;
  siteCode: string;
  emp: string;
  filePath: string | null;
  file: string | null;
  folderPath: string | null;
  chG_DATE?: Date | string;
  strTranId?: string | null | number;
  chG_USER?: string;
  chG_TERM?: string;
  remarks?: string | null;
  fieldUpdates: [
    {
      fieldName: string;
      oldValue: string | undefined;
      newValue: string | undefined;
    }
  ];
};

export type DepartmentQueryResponse = Response<Array<Department>>;

export const initialDepartment: Department = {
  //sH_DESCR?: string,
  //chG_DATE?: Date,
  //chG_USER?: string,
  //chG_TERM?: string,
  //depT_HEAD?: string,
  // accT_CODE?: string,
  // paY_TABLE?: string,
  // cctR_CODE?:string,
  // sH_DESCR?: string,
  // accT_CODE__AP?: string,
  // cctR_CODE__AP?: string,
  // udF_NUM1?:number,
  // sH_DEPT_CODE?: string
};
