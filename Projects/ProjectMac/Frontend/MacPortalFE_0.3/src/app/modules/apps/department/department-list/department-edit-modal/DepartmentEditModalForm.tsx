import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { isNotEmpty, Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { initialDepartment, Department, DepartmentTx } from "../core/_models";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
import { DepartmentListLoading } from "../components/loading/DepartmentListLoading";
import { createDepartment, insertTransaction, updateDepartment } from "../core/_requests";
import { useQueryResponse } from "../../../transaction-master/master-list/core/QueryResponseProvider";
import { toast } from "react-toastify";

import axios from "axios";
import { useQueryClient ,useMutation } from "react-query";

type Props = {
  isDepartmentLoading: boolean;
  department: Department;
};

interface deptHead {
  shortName: string;
  empCode: string;
}
const API_URL = import.meta.env.VITE_DEPT_DROPDOWNS_API_URL;

const editDepartmentSchema = Yup.object().shape({
  name: Yup.string().required("Description is required"),
  shortDescription: Yup.string().required("Short Description is required"),
  status: Yup.string().required("Status is required"),
  deptHead: Yup.string().required("Department Head is required"),
  changeReason: Yup.string().min(3, "Minimum 3 characters"),
});

const DepartmentEditModalForm: FC<Props> = ({
  department,
  isDepartmentLoading,
}) => {
    //import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
    const { setItemIdForUpdate, formMode } = useListView();
    const { setItemIdForUpdate: closeForm } = useTxListView();
    // closeForm(undefined);

  const { refetch } = useQueryResponse();

  const [statuses] = useState(["ACTIVE", "INACTIVE"]);

  const [deptHeads, setDeptHeads] = useState<deptHead[]>([]);
  // const [deptHead, setDeptHead] = useState("");

  const [newReason, setNewReason] = useState("");

  // const [ipAddress, setIpAddress] = useState<string>("");
  const { currentUser } = useAuth();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();

  useEffect(() => {
    axios
      .get(API_URL)
      .then((response) => {
        setDeptHeads(response.data.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const [departmentForEdit] = useState<Department>({
    ...department,
    name:
      department.name ||
      initialDepartment.name ||
      department.fieldNames?.DESCR?.neW_VALUE,
    shortDescription:
      department.shortDescription ||
      initialDepartment.shortDescription ||
      department.fieldNames?.SH_DESCR?.neW_VALUE,
    deptHead:
      department.deptHead ||
      initialDepartment.deptHead ||
      department.fieldNames?.DEPT_HEAD?.neW_VALUE,
    status:
      department.status ||
      initialDepartment.status ||
      department.fieldNames?.status?.neW_VALUE,
    changeDate:
      department.changeDate ||
      initialDepartment.changeDate ||
      department.fieldNames?.CHG_DATE?.neW_VALUE,
    changeUser:
      department.changeUser ||
      initialDepartment.changeUser ||
      department.fieldNames?.CHG_USER?.neW_VALUE,
    changeTerm:
      department.changeTerm ||
      initialDepartment.changeTerm ||
      department.fieldNames?.CHG_TERM?.neW_VALUE,
  });

  const fetchIpAddress = async () => {};
  fetchIpAddress();

  function formatDate(dateTime: string) {
    const date = new Date(dateTime);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata", // Specify the Indian time zone
    });
    return `${formattedDate} ${formattedTime}`;
  }

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
    closeForm(undefined);
  };

  const createTx = useMutation((payload: any) => insertTransaction(payload), {
    onSuccess: (response: any) => {
      queryClient.invalidateQueries([
        `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
      ]);

      setItemIdForUpdate(undefined);
      closeForm(undefined);
      toast.success(
        <div>
          <h4 className="alert-heading">Successfully Submitted Form...</h4>
          <p className="alert alert-success" role="alert">
            <h6>{response?.message}</h6>
          </p>
        </div>
      );
      cancel(true);
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Failed to submit Form...</h4>
          <p className="alert alert-danger" role="alert">
            <h6>{response?.message}</h6>
          </p>
        </div>
      );
    },
  });


  const formik = useFormik({
    initialValues: departmentForEdit,
    validationSchema: editDepartmentSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      if (isNotEmpty(values.id)) {
        const payload: DepartmentTx = {
          tableName: "DEPARTMENT",
          keyVar: "",
          wfLevel: 1,
          conf: "N",
          siteCode: "S0001",
          emp: currentUser?.id?.toString() || "",
          filePath: null,
          file: null,
          strTranId: values.fieldNames ? values.id : "",
          folderPath: null,
          chG_USER: currentUser?.id?.toString() || "",
          chG_TERM: "TERM",
          remarks: department.changeReason,
          chG_DATE: new Date(),

          fieldUpdates: [
            {
              fieldName: "DEPT_CODE",
              oldValue:department.dept_Code || department.fieldNames?.DEPT_CODE?.olD_VALUE || "",
              newValue: values.dept_Code || department.fieldNames?.DEPT_CODE?.olD_VALUE || "",
            },
           {
              fieldName: "DESCR",
              oldValue:
                department.name || department.fieldNames?.DESCR?.olD_VALUE,
              newValue: values.name,
            },
            {
              fieldName: "SH_DESCR",
              oldValue:
                department.shortDescription ||
                department.fieldNames?.SH_DESCR?.olD_VALUE,
              newValue: values.shortDescription,
            },
            {
              fieldName: "DEPT_HEAD",
              oldValue:
                department.deptHead ||
                department.fieldNames?.DEPT_HEAD?.olD_VALUE,
              newValue: values.deptHead,
            },
            {
              fieldName: "CHG_USER",
              oldValue:
                department.changeUser ||
                department.fieldNames?.CHG_USER?.olD_VALUE,
              newValue: currentUser?.id?.toString() || "",
            },
            {
              fieldName: "CHG_TERM",
              oldValue:
                department.changeTerm ||
                department.fieldNames?.CHG_TERM?.olD_VALUE,
              newValue: "TERM",
            },
            {
              fieldName: "CHG_DATE",
              oldValue:
                department.changeDate ||
                department.fieldNames?.CHG_DATE?.olD_VALUE,
              newValue: new Date(),
            },
            {
              fieldName: "status",
              oldValue:
                department.status || department.fieldNames?.status?.olD_VALUE,
              newValue: values.status || department.status,
            },
          ],
        };
        createTx.mutate(payload);

        
        setSubmitting(false);
      } else {
        const payload: DepartmentTx = {
          tableName: "DEPARTMENT",
          keyVar: "",
          wfLevel: 1,
          conf: "N",
          siteCode: "S0001",
          emp: currentUser?.id?.toString() || "",
          filePath: null,
          file: null,
          folderPath: null,
          chG_USER: currentUser?.id?.toString() || "",
          chG_TERM: "TERM",
          remarks: "New Department Added",
          chG_DATE: new Date(),

          fieldUpdates: [
            {
              fieldName: "DEPT_CODE",
              oldValue: "",
              newValue: "",
            },
            {
              fieldName: "DESCR",
              oldValue: "",
              newValue: values.name,
            },
            {
              fieldName: "SH_DESCR",
              oldValue: "",
              newValue: values.shortDescription,
            },
            {
              fieldName: "DEPT_HEAD",
              oldValue: "",

              newValue: values.deptHead,
            },
            {
              fieldName: "CHG_USER",
              oldValue: "",
              newValue: currentUser?.id?.toString() || "",
            },
            {
              fieldName: "CHG_TERM",
              oldValue: "",
              newValue: "TERM",
            },
            {
              fieldName: "CHG_DATE",
              oldValue: "",
              newValue: new Date(),
            },
            {
              fieldName: "status",
              oldValue: "",
              newValue: values.status || department.status,
            },
          ],
        };
        createTx.mutate(payload);

        
        setSubmitting(false);
      }
    },
  });

  useEffect(() => {
    if (formMode === Mode.ADD) {
      formik.setFieldValue("status", "A");
    }
  }, []);

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column " id="kt_modal_add_user_scroll">
          {formMode !== Mode.ADD && (
            <div className="row mb-3">
              <div className="fv-row">
                <label className=" fw-bold fs-6 mb-2">Department Code :</label>
                <input
                  className="form-control form-control-solid mb-3 mb-lg-0"
                  value={formik.values.id ? formik.values.id : ""}
                  disabled
                />
              </div>
            </div>
          )}
          <div className="row mb-3">
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Department Name
                </label>
                <input
                  placeholder="Description"
                  {...formik.getFieldProps("name")}
                  onChange={(e) =>
                    formik.setFieldValue("name", e.target.value.toUpperCase())
                  }
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    { "is-invalid": formik.touched.name && formik.errors.name },
                    { "is-valid": formik.touched.name && !formik.errors.name }
                  )}
                  type="name"
                  name="name"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDepartmentLoading ||
                    formMode === Mode.VIEW
                  }
                />
                {formik.touched.name && formik.errors.name && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.name}</span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Short Description
                </label>
                <input
                  placeholder="Short Description"
                  {...formik.getFieldProps("shortDescription")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "shortDescription",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.shortDescription &&
                        formik.errors.shortDescription,
                    },
                    {
                      "is-valid":
                        formik.touched.shortDescription &&
                        !formik.errors.shortDescription,
                    }
                  )}
                  type="text"
                  name="shortDescription"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDepartmentLoading ||
                    formMode === Mode.VIEW
                  }
                />
                {formik.touched.shortDescription &&
                  formik.errors.shortDescription && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.shortDescription}</span>
                    </div>
                  )}
              </div>
            </div>
            {/* data empCode shortName */}
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required form-label fs-6 fw-bold">
                  Department Head
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid fw-bolder",
                    {
                      "is-invalid":
                        formik.touched.deptHead && formik.errors.deptHead,
                    },
                    {
                      "is-valid":
                        formik.touched.deptHead && !formik.errors.deptHead,
                    }
                  )}
                  onChange={(e) => {
                    formik.setFieldValue("deptHead", e.target.value);
                  }}
                  disabled={formMode === Mode.VIEW}
                  value={formik.values.deptHead}
                >
                  <option value=""></option>
                  {deptHeads.map((deptHead) => (
                    <option key={deptHead.empCode} value={deptHead.empCode}>
                      {deptHead.shortName}
                    </option>
                  ))}
                </select>
                {formik.touched.deptHead && formik.errors.deptHead && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.deptHead}</span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required form-label fs-6 fw-bold">
                  Status
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  value={formik.values.status}
                  // onChange={formik.handleChange}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                  }}
                  disabled={formMode === Mode.ADD || formMode === Mode.VIEW}
                >
                  <option value=""></option>
                  {statuses.map((status) => (
                    <option value={status === "ACTIVE" ? "A" : "N"}>
                      {status}
                    </option>
                  ))}
                </select>
                {formik.touched.status && formik.errors.status && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.status}</span>
                  </div>
                )}
              </div>
            </div>

            {formMode === Mode.VIEW && (
              <>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className="fw-bold fs-6 mb-2">Change User</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      value={formik.values.changeUser?.toUpperCase()}
                      disabled={formMode === Mode.VIEW}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className="fw-bold fs-6 mb-2">Change Term</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      value={formik.values.changeTerm}
                      //value={ipAddress}
                      disabled={formMode === Mode.VIEW}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className=" fw-bold fs-6 mb-2">Change Date</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      // value={formik.values.changeDate}
                      // value={
                      //   formik.values.changeDate
                      //     ? formik.values.changeDate.toString()
                      //     : ""
                      // }
                      value={
                        formik.values.changeDate
                          ? formatDate(formik.values?.changeDate.toString())
                          : ""
                      }
                      disabled={formMode === Mode.VIEW}
                    />
                  </div>
                </div>
              </>
            )}
          </div>
          {formMode !== Mode.ADD && (
            <div className="row mb-3">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Change Reason
                </label>
                <textarea
                  placeholder="Change Reason"
                  //value={{...formik.getFieldProps('changeReason')}}
                  {...(formMode === Mode.VIEW
                    ? { ...formik.getFieldProps("changeReason") }
                    : { newReason })}
                  onChange={(e) => {
                    formik.setFieldValue(
                      "changeReason",
                      e.target.value.toUpperCase()
                    );
                    setNewReason(e.target.value.toUpperCase());
                  }}
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.changeReason &&
                        formik.errors.changeReason,
                    },
                    {
                      "is-valid":
                        formik.touched.changeReason &&
                        !formik.errors.changeReason,
                    }
                  )}
                  // type="text"
                  name="changeReason"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDepartmentLoading ||
                    formMode === Mode.VIEW
                  }
                />
                {formik.touched.changeReason && formik.errors.changeReason && (
                  <div className="fv-plugins-message-container">
                    <span role="alert">{formik.errors.changeReason}</span>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
        <div className="text-center pt-15">
          {/* <button
            type='reset'
            onClick={() => cancel()}
            className='btn btn-light me-3'
            disabled={formik.isSubmitting || isDepartmentLoading}
          >
            Cancel
          </button> */}
          {formMode !== Mode.VIEW && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isDepartmentLoading ||
                formik.isSubmitting ||
                !formik.touched ||
                (formMode !== Mode.ADD && newReason === "")
              }
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isDepartmentLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isDepartmentLoading) && (
        <DepartmentListLoading />
      )}
    </>
  );
};

export { DepartmentEditModalForm };
