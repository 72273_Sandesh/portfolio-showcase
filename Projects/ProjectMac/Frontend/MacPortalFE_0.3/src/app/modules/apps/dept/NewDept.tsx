// import React, { useEffect, useState } from 'react';

// interface NewDepartmentFormProps {
//   onSubmit: (formData: DepartmentFormData) => void;
//   onCancel: () => void;
// }

// interface DepartmentFormData {
//   description: string;
//   shortDescription: string;
//   departmentHead: string;
//   status: string;
//   salaryAccount: string;
//   costCentre: string;
//   accountCodeAP: string;
//   cctrCodeAP: string;
//   payTable: string;
// }

// const NewDepartment: React.FC<NewDepartmentFormProps> = ({ onSubmit, onCancel }) => {
//   const [statuses, setStatuses] = useState([]);
//   const [status, setStatus] = useState('');

//   const [deptHeads, setDeptHeads] = useState([]);
//   const [deptHead, setDeptHead] = useState('');

//   const [salaryAccts, setSalaryAccts] = useState([]);
//   const [salaryAcct, setSalaryAcct] = useState('');

//   const [acctCodeAPs, setAcctCodeAPs] = useState([]);
//   const [acctCodeAP, setAcctCodeAP] = useState('');

//   const [payTables, setPayTables] = useState([]);
//   const [payTable, setPayTable] = useState('');

//   const [costCentres, setCostCentres] = useState([]);
//   const [costCentre, setCostCentre] = useState('');

//   const [cctrCodeAPs, setCCTRCodeAPs] = useState([]);
//   const [cctrCodeAP, setCCTRCodeAP] = useState('');

//   useEffect(() => {
    
      

//     fetchFromBackend('combinedDataEndpoint')
//     .then(combinedData => {
//         setStatuses(combinedData.statusData);
//         setDeptHeads(combinedData.deptHeadData);
//         setSalaryAccts(combinedData.salaryAcctsData);
//         setAcctCodeAPs(combinedData.acctCodeAPData);
//         setPayTables(combinedData.payTableData);
//         setCostCentres(combinedData.costCentreData);
//         setCCTRCodeAPs(combinedData.cctrCodeAPData);
//     })
//     .catch(error => console.error('Error fetching combined data:', error));

//   }, [])

    
//   const [formData, setFormData] = useState<DepartmentFormData>({
//     description: '',
//     shortDescription: '',
//     departmentHead: '',
//     status: '',
//     salaryAccount: '',
//     costCentre: '',
//     accountCodeAP: '',
//     cctrCodeAP: '',
//     payTable: ''
//   });
//   const fetchFromBackend = async (combinedDataEndpoint) => {
//     try {
//       const response = await fetch(combinedDataEndpoint);
//       const deptHeadData = await response.json();
//       return deptHeadData;
//     } catch (error) {
//       throw new Error('Failed to fetch grade from backend');
//     }
//   };
//   const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
//     const { name, value } = e.target;
//     setFormData(prevData => ({
//       ...prevData,
//       [name]: value
//     }));
//   };

//   const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
//     e.preventDefault();
//     onSubmit(formData);
//   };

//   return (
//     <div className="container mt-5">
//       <h1 className="mb-4">New Department</h1>
//       <form onSubmit={handleSubmit}>
//         <div className="mb-3">
//           <label htmlFor="description" className="form-label">Description<span className="text-danger">*</span></label>
//           <input type="text" className="form-control" id="description" name="description" value={formData.description} onChange={handleChange} required />
//         </div>
//         <div className="mb-3">
//           <label htmlFor="shortDescription" className="form-label">Short Description<span className="text-danger">*</span></label>
//           <input type="text" className="form-control" id="shortDescription" name="shortDescription" value={formData.shortDescription} onChange={handleChange} required />
//         </div>
//         {/* Add other form fields here */}
//          {/* begin::Input group */}
//          <div className='mb-3'>
//             <label className='form-label fs-6 fw-bold'>Status:</label>
//             <select
//               className='form-select form-select-solid fw-bolder'
//               onChange={(e) => setStatus(e.target.value)}
//               value={status}
//             >
//               <option value=''></option>
//               {statuses.map(status => (
//                 <option key={status} value={status}>{status}</option>
//               ))}
//             </select>
//           </div>
//           {/* end::Input group */}


//           {/* begin::Input group */}
//           <div className='mb-3'>
//             <label className='form-label fs-6 fw-bold'>Department Head:</label>
//             <select
//               className='form-select form-select-solid fw-bolder'
//               onChange={(e) => setDeptHead(e.target.value)}
//               value={deptHead}
//             >
//               <option value=''></option>
//               {deptHeads.map(deptHead => (
//                 <option key={deptHead} value={deptHead}>{deptHead}</option>
//               ))}
//             </select>
//           </div>
//           {/* end::Input group */}

//           {/* begin::Input group */}
//           <div className='mb-3'>
//             <label className='form-label fs-6 fw-bold'>Salary Account:</label>
//             <select
//               className='form-select form-select-solid fw-bolder'
//               onChange={(e) => setSalaryAcct(e.target.value)}
//               value={salaryAcct}
//             >
//               <option value=''></option>
//               {salaryAccts.map(salaryAcct => (
//                 <option key={salaryAcct} value={salaryAcct}>{salaryAcct}</option>
//               ))}
//             </select>
//           </div>
//           {/* end::Input group */}

//           {/* begin::Input group */}
//           <div className='mb-3'>
//             <label className='form-label fs-6 fw-bold'>Account Code AP:</label>
//             <select
//               className='form-select form-select-solid fw-bolder'
//               onChange={(e) => setAcctCodeAP(e.target.value)}
//               value={acctCodeAP}
//             >
//               <option value=''></option>
//               {acctCodeAPs.map(acctCodeAP => (
//                 <option key={acctCodeAP} value={acctCodeAP}>{acctCodeAP}</option>
//               ))}
//             </select>
//           </div>
//           {/* end::Input group */}

//           {/* begin::Input group */}
//           <div className='mb-3'>
//             <label className='form-label fs-6 fw-bold'>Pay Table:</label>
//             <select
//               className='form-select form-select-solid fw-bolder'
//               onChange={(e) => setPayTable(e.target.value)}
//               value={payTable}
//             >
//               <option value=''></option>
//               {payTables.map(payTable => (
//                 <option key={payTable} value={payTable}>{payTable}</option>
//               ))}
//             </select>
//           </div>
//           {/* end::Input group */}

//           {/* begin::Input group */}
//           <div className='mb-3'>
//             <label className='form-label fs-6 fw-bold'>Cost Centre:</label>
//             <select
//               className='form-select form-select-solid fw-bolder'
//               onChange={(e) => setCostCentre(e.target.value)}
//               value={costCentre}
//             >
//               <option value=''></option>
//               {costCentres.map(costCentre => (
//                 <option key={costCentre} value={costCentre}>{costCentre}</option>
//               ))}
//             </select>
//           </div>
//           {/* end::Input group */}

//           {/* begin::Input group */}
//           <div className='mb-3'>
//             <label className='form-label fs-6 fw-bold'>CCTR Code AP:</label>
//             <select
//               className='form-select form-select-solid fw-bolder'
//               onChange={(e) => setCCTRCodeAP(e.target.value)}
//               value={cctrCodeAP}
//             >
//               <option value=''></option>
//               {cctrCodeAPs.map(cctrCodeAP => (
//                 <option key={cctrCodeAP} value={cctrCodeAP}>{cctrCodeAP}</option>
//               ))}
//             </select>
//           </div>
//           {/* end::Input group */}
//           <div className="text-center mt-4">
//           <button type="submit" className="btn btn-primary mx-2">Submit</button>
//           <button type="button" className="btn btn-secondary mx-2" onClick={onCancel}>Cancel</button>
//         </div>
//       </form>
//     </div>
//   );
// };

// export default NewDepartment;

// // import React from 'react';

// // const NewDept: React.FC = () => {
// //   return (
// //     <div className="container mt-5">
// //       <h1 className="mb-4">New Department</h1>

// //     </div>
// //   );
// // };

// // export default NewDept;


import { NewList } from "../../../../_metronic/partials/widgets/lists/NewList";

const NewDept = () => {
  return <NewList />;
};

export default NewDept;
