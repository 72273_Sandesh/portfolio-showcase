import {ID, Response} from '../../../../../../_metronic/helpers'
export type Designation = {
  id?: ID
  name?:string,
  design_code?:string,
  longDesignation?:string,
  status?:string,
  gradeCode?:string,
  gradeName?:string,
  cadreCode?:string,
  cadreName?:string,
  gradeId?:string,
  changeUser?:string,
  changeTerm?:string,
  changeReason?:string,
  changeDate?:Date | string ,
  udfNum1?:number,
  udfStr1?:string,
  sapDesignCode?:string,
  responseStatus?: string;
  message?: string;


  wF_PROCESS_LEVEL?: number;
  emP_CODE?: string;
  remarks?: string;
  SessionempCode?: string;
  moduleType?: string;
  fieldNames?: {
  
    DESIGN_CODE: { olD_VALUE: string; neW_VALUE: string };
    DESIGNATION: { olD_VALUE: string; neW_VALUE: string };
    LONG_DESIGNATION: { olD_VALUE: string; neW_VALUE: string };
    STATUS: { olD_VALUE: string; neW_VALUE: string };
    GRADE_CODE: { olD_VALUE: string; neW_VALUE: string };
    CHG_DATE: { olD_VALUE: string; neW_VALUE: string };
    CHG_TERM: { olD_VALUE: string; neW_VALUE: string };
    CHG_USER: { olD_VALUE: string; neW_VALUE: string };
  };
};
      


//model for create and update tx payload
export type Designationtx = {
  responseStatus?: string;
  message?: string;
  tableName: string;
  keyVar: string;
  wfLevel: number;
  conf: string;
  siteCode: string;
  emp: string;
  filePath: string | null;
  file: string | null;
  folderPath: string | null;
  chG_DATE?: Date | string;
  strTranId?: string | null | number;
  chG_USER?: string;
  chG_TERM?: string;
  remarks?: string;
  fieldUpdates: [
    {
      fieldName: string;
      oldValue: string | number | undefined;
      newValue: string | number | undefined;
    }
  ];
};

export type DesignationsQueryResponse = Response<Array<Designation>>

export const initialDesignation: Designation = {
 
}
