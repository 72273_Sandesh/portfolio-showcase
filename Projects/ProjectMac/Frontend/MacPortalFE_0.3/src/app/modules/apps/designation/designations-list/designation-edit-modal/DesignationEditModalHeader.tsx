import {KTIcon} from '../../../../../../_metronic/helpers'
import {useListView} from '../core/ListViewProvider'

const DesignationEditModalHeader = () => {
  const {setItemIdForUpdate, formMode } = useListView()
  const openAddModal = () => {
    setItemIdForUpdate(undefined)
    
  }
  return (
    <div className='modal-header'>
      <div>
      <h2 className="fw-bolder">{formMode} Designation</h2>
  
     
    </div>
     
      <div
        className='btn btn-icon btn-sm btn-active-icon-primary'
        data-kt-users-modal-action='close'
        onClick={openAddModal}
        style={{cursor: 'pointer'}}
      >
        <KTIcon iconName='cross' className='fs-1' />
      </div>
      
    </div>
  )
}

export {DesignationEditModalHeader}
