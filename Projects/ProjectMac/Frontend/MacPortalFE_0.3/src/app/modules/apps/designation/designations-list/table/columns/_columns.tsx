import {Column} from 'react-table'
import {DesignationInfoCell} from './DesignationInfoCell'
import {DesignationActionsCell} from './DesignationActionsCell'
import {DesignationCustomHeader} from './DesignationCustomHeader'
import {Designation} from '../../core/_models'
import { DesignationChangeDate } from './DesignationChangeDate'
import { DesignationChangeUser } from './DesignationChangeUser'

const designationsColumns: ReadonlyArray<Column<Designation>> = [
  
  {
    Header: (props) => <DesignationCustomHeader tableProps={props} title='Designation' className='min-w-125px' />,
    id: 'name',
    Cell: ({...props}) => <DesignationInfoCell designation={props.data[props.row.index]} id={props.data[props.row.index].id} />,
   },
  {
    Header: (props) => (
      <DesignationCustomHeader tableProps={props} title='Grade Name' className='min-w-125px text-left' />
    ),
    accessor: 'gradeName',
    Cell: ({ value }) => (value ?value : value || value === null  || value === 'N' ? 'NA' : value)
  },
  {
    Header: (props) => (
      <DesignationCustomHeader tableProps={props} title='Cadre Name' className='min-w-125px text-left' />
    ),
    accessor: 'cadreName',
    Cell: ({ value }) => (value ? value : value || value === null  || value === 'N' ? 'NA' : value)
  },
  {
    Header: (props) => (
      <DesignationCustomHeader tableProps={props} title='Change User' className='min-w-125px text-left' />
    ),
    id: 'changeUser',
    Cell: ({...props}) => <DesignationChangeUser changeUser={props.data[props.row.index].changeUser} />,
  },
  
  {
    Header: (props) => (
      <DesignationCustomHeader tableProps={props} title='Change Date' className='min-w-125px text-left' />
    ),
    id: 'changeDate',
    Cell: ({...props}) => <DesignationChangeDate changeDate={props.data[props.row.index].changeDate  ?? new Date()} />,
  },
  {
    Header: (props) => (
      <DesignationCustomHeader tableProps={props} title='Status' className='min-w-125px text-left' />
    ),
    accessor: 'status',
    Cell: ({ value }) => (<span >
      <div className=' fw-bolder d-flex align-items-right'>
        <div className='d-flex align-items-right'><div className='d-flex align-items-right'>
  {value === 'A' ? (
    <div className='badge badge-light-success fw-bolder d-flex align-items-right'>ACTIVE</div>
  ) : value === 'N' ? (
    <div className='badge badge-light-danger fw-bolder d-flex align-items-right'>INACTIVE</div>
  ) : (
    <div className='badge d-flex align-items-right'></div>
  )}
</div>
</div>
      </div>
      </span>)
  },
  {
    Header: (props) => (
      <DesignationCustomHeader tableProps={props} title='Actions' className='min-w-100px' />
    ),
    id: 'actions',
    Cell: ({...props}) => <DesignationActionsCell id={props.data[props.row.index].id} />,
  },
]

export {designationsColumns}
