import { useEffect, useState } from "react";
import {
  initialQueryState,
  KTIcon,
  toAbsoluteUrl,
  useDebounce,
} from "../../../../_metronic/helpers/index";
import { useQueryRequest } from "./employee-list/core/QueryRequestProvider";
import { MenuComponent } from "../../../../_metronic/assets/ts/components";
import { useQueryResponse } from "./employee-list/core/QueryResponseProvider";
import { toast } from "react-toastify";
import axios from "axios";
import {
  filterDebouncedSearchTerm,
  filterDepartment,
  filterLocation,
  filterStatus,
  setIsFiltered,
} from "../../GlobalQuery";
import { useAuth } from "../../auth";

type dropdownMenus = {
  sitE_CODE: string;
  descr: string;
  deptCode: string;
  description: string;
};

const Toolbar = () => {
  const [location, setLocation] = useState(filterLocation);
  const [locations, setLocations] = useState<dropdownMenus[]>([]);

  const [department, setDepartment] = useState(filterDepartment);
  const [departments, setDepartments] = useState<dropdownMenus[]>([]);

  const [status, setStatus] = useState(filterStatus);
  const [statuses, setStatuses] = useState([]);

  const { updateState } = useQueryRequest();
  const [searchTerm, setSearchTerm] = useState<string>(
    filterDebouncedSearchTerm as string
  );

  const debouncedSearchTerm = useDebounce(searchTerm, 150);
  const { isLoading } = useQueryResponse();
  const filterAPI_URL = import.meta.env.VITE_EMP_FILTER_DROPDOWNS_API_URL;
  const { currentUser } = useAuth();

  useEffect(() => {
    MenuComponent.reinitialization();
    filterData();
    if (debouncedSearchTerm !== undefined && searchTerm !== undefined) {
      updateState({ search: debouncedSearchTerm, ...initialQueryState });
    }

    axios
      .get(`${filterAPI_URL}?empCode=${currentUser?.id}`)
      .then((response) => {
        setLocations(response.data.location);
        setDepartments(response.data.department);
        setStatuses(response.data.status);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, [debouncedSearchTerm]);

  const resetData = () => {
    updateState({ filter: undefined, ...initialQueryState });
  };

  const filterData = () => {
    try {
      // if (
      //   debouncedSearchTerm !== "" ||
      //   location !== "" ||
      //   department !== "" ||
      //   status !== ""
      // ) {
        setIsFiltered(true);
        updateState({
          filter: { debouncedSearchTerm, location, department, status },
          ...initialQueryState,
        });
      // } else {
      //   setIsFiltered(false);
      // }
    } catch (ex) {
      console.error(ex);
      // toast.error(`Department Form Submission Failed Because : ${ex}`)
      toast.error(
        <div>
          <h4 className="alert-heading">oops...</h4>
          <p className="alert alert-danger" role="alert">
            <h6>Failed</h6>
          </p>
        </div>
      );
    }
  };
  return (
    <div
      className="card rounded-0 shadow-none border-0 bgi-no-repeat bgi-position-x-end bgi-size-cover"
      style={{
        backgroundColor: "#663259",
        backgroundSize: "auto 150%",
        // backgroundImage: `url('${toAbsoluteUrl('media/misc/taieri.svg')}')`,
        backgroundImage: `url('${toAbsoluteUrl("media/logos/meds02.png")}')`,
      }}
    >
      {/* begin::body */}
      <div className="card-body container-xxl pt-10 pb-8">
        {/*begin::Title */}
        <div className=" d-flex align-items-center">
          <h1 className="fw-bold me-3 text-white">Search Employees</h1>

          <span className="fw-bold text-white opacity-50">
            HR Management System
          </span>
        </div>
        {/* end::Title */}

        {/* begin::Wrapper */}
        <div className="d-flex flex-column">
          {/* begin::Block */}
          <div className="d-lg-flex align-lg-items-center">
            {/* begin::Simple form */}
            <form
              data-kt-user-table-filter="form"
              className="rounded d-flex flex-column flex-lg-row align-items-lg-center bg-body p-5 w-xxl-850px h-lg-60px me-lg-10 my-5"
            >
              {/* begin::Row */}
              <div className="row flex-grow-1 mb-5 mb-lg-0">
                {/* begin::Search */}
                <div className="col-lg-3 d-flex align-items-center mb-3 mb-lg-0">
                  <KTIcon
                    iconName="magnifier"
                    className="fs-1 text-gray-500 me-1"
                  />
                  {/* begin::Input */}
                  <input
                    type="text"
                    className="form-control form-control-flush flex-grow-1"
                    name="search"
                    // value={search}
                    // onChange={(e) => setSearch(e.target.value)}
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.target.value)}
                    placeholder="Your Search"
                  />
                  {/* end::Input */}
                </div>
                {/* end::Search */}

                {/* begin::Location */}
                <div className="col-lg-3 d-flex align-items-center">
                  {/* begin::Desktop separator */}
                  <div className="bullet bg-secondary d-none d-lg-block h-30px w-2px me-5"></div>
                  {/* end::Desktop separator */}
                  <KTIcon
                    iconName="geolocation"
                    className="fs-1 text-gray-500 me-3"
                  />
                  {/* begin::Select */}
                  <select
                    className="form-select border-0 flex-grow-1"
                    data-kt-select2="true"
                    data-placeholder="Location"
                    data-allow-clear="true"
                    data-kt-user-table-filter="location"
                    data-hide-search="true"
                    value={location || ""}
                    onChange={(e) => setLocation(e.target.value)}
                  >
                    <option value="">Location</option>
                    {locations.map((location) => (
                      <option
                        key={location.sitE_CODE}
                        value={location.sitE_CODE}
                      >
                        {location.descr}
                      </option>
                    ))}
                  </select>
                  {/* end::Select */}
                </div>
                {/* end::Location */}

                {/* begin::Department */}
                <div className="col-lg-3 d-flex align-items-center mb-3 mb-lg-0">
                  {/* begin::Desktop separator */}
                  <div className="bullet bg-secondary d-none d-lg-block h-30px w-2px me-5"></div>
                  {/* end::Desktop separator */}
                  <KTIcon
                    iconName="share"
                    className="fs-1 text-gray-500 me-3"
                  />
                  {/* begin::Select */}
                  <select
                    className="form-select border-0 flex-grow-1"
                    data-kt-select2="true"
                    data-placeholder="Department"
                    data-allow-clear="true"
                    data-kt-user-table-filter="department"
                    data-hide-search="true"
                    onChange={(e) => setDepartment(e.target.value)}
                    value={department || ""}
                  >
                    <option value="">Department</option>
                    {departments.map((dept) => (
                      <option key={dept.deptCode} value={dept.deptCode}>
                        {" "}
                        {dept.description}{" "}
                      </option>
                    ))}
                  </select>

                  {/* end::Select */}
                </div>
                {/* end::Department */}

                {/* begin::Status */}
                <div className="col-lg-3 d-flex align-items-center">
                  {/* begin::Desktop separator */}
                  <div className="bullet bg-secondary d-none d-lg-block h-30px w-2px me-5"></div>
                  {/* end::Desktop separator */}
                  <KTIcon
                    iconName="switch"
                    className="fs-1 text-gray-500 me-3"
                  />
                  {/* begin::Select */}
                  <select
                    className="form-select border-0 flex-grow-1"
                    data-kt-select2="true"
                    data-placeholder="Status"
                    data-allow-clear="true"
                    data-kt-user-table-filter="status"
                    data-hide-search="true"
                    onChange={(e) => setStatus(e.target.value)}
                    value={status || ""}
                  >
                    <option value="">Status</option>
                    {statuses.map((sts) => (
                      <option key={sts} value={sts}>
                        {" "}
                        {sts}
                      </option>
                    ))}
                  </select>
                  {/* end::Select */}
                </div>
                {/* end::Status */}
              </div>
              {/* end::Row */}

              {/* begin::Search btn */}
              {/* <div className='row flex-grow-1 mb-5 mb-lg-0'> */}
              <div className="d-flex justify-content-end">
                <button
                  disabled={isLoading}
                  type="button"
                  onClick={filterData}
                  className="btn btn-dark fw-bold px-6"
                  data-kt-menu-dismiss="true"
                  data-kt-user-table-filter="filter"
                >
                  Apply
                </button>
              </div>
              {/* end::Search btn */}
            </form>
            {/* end::Simple form */}
          </div>
          {/* end::Block */}
        </div>
        {/*end::Wrapper*/}
      </div>
      {/*end::body*/}
    </div>
  );
};
export { Toolbar };
