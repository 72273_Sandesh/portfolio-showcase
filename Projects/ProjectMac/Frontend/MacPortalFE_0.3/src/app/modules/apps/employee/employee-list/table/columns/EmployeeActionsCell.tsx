import { FC, useEffect } from "react";
import { MenuComponent } from "../../../../../../../_metronic/assets/ts/components";
import { ID, KTIcon } from "../../../../../../../_metronic/helpers";
import { newQuery } from "../../core/QueryResponseProvider";
import { Link } from "react-router-dom";

type Props = {
  id: ID;
};

const EmployeeActionsCell: FC<Props> = ({ id }) => {
  const currentUserId = { userid: id };
  const queryData = { query: newQuery };

  useEffect(() => {
    MenuComponent.reinitialization();
  }, []);
  return (
    <>
      <a
        href="#"
        className="btn btn-light btn-active-light-primary btn-sm"
        data-kt-menu-trigger="click"
        data-kt-menu-placement="bottom-end"
      >
        Actions
        <KTIcon iconName="down" className="fs-5 m-0" />
      </a>
      {/* begin::Menu */}
      <div
        className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
        data-kt-menu="true"
      >
        {/* begin::Menu item */}
        <div className="menu-item px-3">
          <Link
            to="/crafted/pages/profile/overview"
            state={{ userId: currentUserId, queryData: queryData }}
            search={queryData}
            className="menu-link px-3"
          >
            View Profile
          </Link>
        </div>
        {/* end::Menu item */}
      </div>
      {/* end::Menu */}
    </>
  );
};

export { EmployeeActionsCell };
