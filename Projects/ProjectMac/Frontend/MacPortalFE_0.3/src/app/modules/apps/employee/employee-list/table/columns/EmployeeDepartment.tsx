import React from 'react';

type Props = {
  department?: string;
};

const EmployeeDepartment: React.FC<Props> = ({ department }) => {


  return (
    <div className=' fw-bolder d-flex align-items-right'>
      {department ?(department.toUpperCase()): <div className='badge badge-danger fw-bolder d-flex align-items-right'>NOT ASSSIGNED</div>}
    </div>


  );
};

export { EmployeeDepartment };
