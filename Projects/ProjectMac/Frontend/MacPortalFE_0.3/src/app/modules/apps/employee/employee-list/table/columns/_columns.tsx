import {Column} from 'react-table'
import {EmployeeInfoCell} from './EmployeeInfoCell'
import {EmployeeChangeDate} from './EmployeeChangeDate'
import {EmployeeActionsCell} from './EmployeeActionsCell'
import {EmployeeCustomHeader} from './EmployeeCustomHeader'
import {Employee} from '../../core/_models'
import { EmployeeChangeUser } from './EmployeeChangeUser'
import { EmployeeLocation } from './EmployeeLocation'
import { EmployeeStatusCell } from './EmployeeStatusCell'
import { EmployeeDepartment } from './EmployeeDepartment'

const employeeColumns: ReadonlyArray<Column<Employee>> = [
  // {
  //   Header: (props) => <DepartmentSelectionHeader tableProps={props} />,
  //   id: 'selection',
  //   Cell: ({...props}) => <DepartmentSelectionCell id={props.data[props.row.index].id} />,
  // },
  {
    Header: (props) => <EmployeeCustomHeader tableProps={props} title='Employee Name' className='min-w-125px text-left ' />,
    id: 'name',
    Cell: ({...props}) => <EmployeeInfoCell employee={props.data[props.row.index]} id={props.data[props.row.index].id} />,
  },
  {
    Header: (props) => (
      <EmployeeCustomHeader tableProps={props} title='Location' className='min-w-125px' />
    ),
    id: 'location',
    Cell: ({...props}) => <EmployeeLocation location={props.data[props.row.index].location} />,
  },
  {
    Header: (props) => (
      <EmployeeCustomHeader tableProps={props} title='Department' className='min-w-125px' />
    ),
    id: 'department',
    Cell: ({...props}) => <EmployeeDepartment department={props.data[props.row.index].department} />,
  },
  // {
  //   Header: (props) => (
  //     <EmployeeCustomHeader tableProps={props} title='Birth Date' className='min-w-125px' />
  //   ),
  //   id: 'dob',
  //   Cell: ({...props}) => <EmployeeBirthDate dob={props.data[props.row.index].dob} />,
  // },
  {
    Header: (props) => (
      <EmployeeCustomHeader tableProps={props} title='Change User' className='min-w-125px' />
    ),
    id: 'chG_USER',
    Cell: ({...props}) => <EmployeeChangeUser chG_USER={props.data[props.row.index].chG_USER} />,
  },
  {
    Header: (props) => (
      <EmployeeCustomHeader tableProps={props} title='Change Date' className='min-w-125px text-left' />
    ),
    id: 'chG_DATE',
    Cell: ({...props}) => <EmployeeChangeDate chG_DATE={props.data[props.row.index].chG_DATE?? new Date()} />,
  },
  {
    Header: (props) => (
      <EmployeeCustomHeader tableProps={props} title='status' className='min-w-125px' />
    ),
    id: 'status',
    Cell: ({...props}) => <EmployeeStatusCell status={props.data[props.row.index].status} />,
  },
  {
    Header: (props) => (
      <EmployeeCustomHeader tableProps={props} title='Actions' className='min-w-100px text-left' />
    ),
    id: 'actions',
    Cell: ({...props}) => <EmployeeActionsCell id={props.data[props.row.index].id} />,
  },
]

export {employeeColumns}
