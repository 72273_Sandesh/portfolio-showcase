import React from 'react';
import { useQueryResponseData } from '../../core/QueryResponseProvider';

const GradeTableFormat = () => {
  const grades = useQueryResponseData(); 

    return (
  
        <table id="table-to-export">
        <thead>
          <tr>
            <th>Id</th>
            <th>Description</th>
            <th>Short Description</th>
            <th>Cadre Code</th>
            <th>Status</th>
            <th>Lve Table Number</th>
            <th>Induction Table Number</th>
            <th>Notice Period</th>
            <th>Change Date</th>
            <th>Change User</th>
            <th>Change Term</th>
            <th>Approval Table Number</th>
            <th>Account Code Exp</th>
            <th>Cost Center Code Exp</th>
            <th>Grade Type</th>
            <th>Udf Number1</th>
            <th>Grade Order</th>
            <th>Grade Id</th>
            <th>Change Reason</th>
          </tr>
        </thead>
        <tbody>
          {grades.map(grade => (
            <tr key={grade.id}>
                <td>{grade.id}</td>
                <td>{grade.description}</td>
                <td>{grade.shortDescription}</td>
                <td>{grade.cadreCode}</td>
                <td>{grade.status}</td>
                <td>{grade.lveTableNumber}</td>
                <td>{grade.inductionTableNumber}</td>
                <td>{grade.noticePeriod}</td>
                <td>{grade.changeDate?.toString()}</td>
                <td>{grade.changeUser}</td>
                <td>{grade.changeTerm}</td>
                <td>{grade.approvalTableNumber}</td>
                <td>{grade.accountCodeExp}</td>
                <td>{grade.costCenterCodeExp}</td>
                <td>{grade.gradeType}</td>
                <td>{grade.udfNumber1}</td>
                <td>{grade.gradeOrder}</td>
                <td>{grade.gradeId}</td>
                <td>{grade.changeReason}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  
};

export default GradeTableFormat;
