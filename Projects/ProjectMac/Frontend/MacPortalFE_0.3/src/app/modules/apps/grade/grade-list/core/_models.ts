import { ID, Response } from "../../../../../../_metronic/helpers";
export type Grade = {
  id?: ID;
  description?: string;
  shortDescription?: string;
  cadreCode?: string;
  status?: string;
  lveTableNumber?: string;
  inductionTableNumber?: string;
  noticePeriod?: number | string;
  changeDate?: Date | string;
  changeUser?: string;
  changeTerm?: string;
  approvalTableNumber?: string;
  accountCodeExp?: string;
  costCenterCodeExp?: string;
  gradeType?: string;
  udfNumber1?: number;
  gradeOrder?: number;
  gradeId?: string;
  changeReason?: string;
  cadreName?: string;
  responseStatus?: string;
  message?: string;
  fieldNames?: {
    GRADE_CODE: { olD_VALUE: string; neW_VALUE: string };
    DESCR: { olD_VALUE: string; neW_VALUE: string };
    SH_DESCR: { olD_VALUE: string; neW_VALUE: string };
    CADRE_CODE: { olD_VALUE: string; neW_VALUE: string };
    NOTICE_PRD?: { olD_VALUE: string; neW_VALUE: string };
    CHG_DATE?: { olD_VALUE: string; neW_VALUE: string };
    CHG_USER?: { olD_VALUE: string; neW_VALUE: string };
    CHG_TERM: { olD_VALUE: string; neW_VALUE: string };
    CHG_REASON: { olD_VALUE: string; neW_VALUE: string };
    STATUS: { olD_VALUE: string; neW_VALUE: string };
};

  
          
};
export type Gradetx = {
  responseStatus?: string;
  message?: string;
  tableName: string;
  keyVar: string;
  wfLevel: number;
  conf: string;
  siteCode: string;
  emp: string;
  filePath: string | null;
  file: string | null;
  folderPath: string | null;
  chG_DATE?: Date | string;
  strTranId?: string | null | number;
  chG_USER?: string;
  chG_TERM?: string;
  remarks?: string;
  fieldUpdates: [
    {
      fieldName: string;
      oldValue: string | undefined;
      newValue: string | undefined;
    }
  ];
};
export type GradeQueryResponse = Response<Array<Grade>>;

export const initialGrade: Grade = {};
