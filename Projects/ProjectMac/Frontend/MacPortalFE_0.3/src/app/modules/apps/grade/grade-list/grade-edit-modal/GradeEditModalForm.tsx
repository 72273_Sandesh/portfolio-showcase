import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { isNotEmpty, Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { initialGrade, Grade, Gradetx } from "../core/_models";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
import { GradeListLoading } from "../components/loading/GradeListLoading";
import { createGrade, insertTransaction } from "../core/_requests";
import { useQueryResponse } from "../../../transaction-master/master-list/core/QueryResponseProvider";
import { toast } from "react-toastify";
import axios from "axios";
import { useQueryClient, useMutation } from "react-query";

type Props = {
  isGradeLoading: boolean;
  grade: Grade;
};
const API_URL = import.meta.env.VITE_GRADE_DROPDOWNS_API_URL;

const editGradeSchema = Yup.object().shape({
  description: Yup.string().required("Description is required"),
  shortDescription: Yup.string()
    //.min(3, 'Minimum 3 characters')
    //.max(500, 'Maximum 500 characters')
    .required("Short Description is required"),
  cadreCode: Yup.string()
    //.min(3, 'Minimum 3 characters')
    //.max(500, 'Maximum 500 characters')
    .required("Cadre is required"),
  noticePeriod: Yup.number().required("Notice Period is required"),
  status: Yup.string().required("Status is required"),
  changeReason: Yup.string().min(3, "Minimum 3 characters"),
});

type states = {
  cadreCode: string;
  cadre: string;
};
const GradeEditModalForm: FC<Props> = ({ grade, isGradeLoading }) => {
  const { setItemIdForUpdate, itemIdForUpdate, formMode } = useListView();
  const { setItemIdForUpdate: closeForm } = useTxListView();

  const { refetch } = useQueryResponse();

  const [cadreCodes, setCadreCodes] = useState<states[]>([]);
  const [cadreCode, setCadreCode] = useState("");

  const [statuses] = useState(["ACTIVE", "INACTIVE"]);

  const [newReason, setNewReason] = useState("");

  const { currentUser } = useAuth();
  const { query } = useQueryResponse();
  const queryClient = useQueryClient();

  useEffect(() => {
    axios
      .get(API_URL)
      .then((response) => {
        setCadreCodes(response.data.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);
  useEffect(() => {
    if (formMode === Mode.ADD) {
      formik.setFieldValue("status", "A");
    }
  }, []);

  const [gradeForEdit] = useState<Grade>({
    ...grade,
    description:
      grade.description ||
      initialGrade.description ||
      grade.fieldNames?.DESCR.neW_VALUE,
    shortDescription:
      grade.shortDescription ||
      initialGrade.shortDescription ||
      grade.fieldNames?.SH_DESCR.neW_VALUE,
    status:
      grade.status || initialGrade.status || grade.fieldNames?.STATUS.neW_VALUE,
    cadreCode:
      grade.cadreCode ||
      initialGrade.cadreCode ||
      grade.fieldNames?.CADRE_CODE.neW_VALUE,
    noticePeriod:
      grade.noticePeriod ||
      initialGrade.noticePeriod ||
      grade.fieldNames?.NOTICE_PRD?.neW_VALUE,
    changeDate:
      grade.changeDate ||
      initialGrade.changeDate ||
      grade.fieldNames?.CHG_DATE?.neW_VALUE,
    changeUser:
      grade.changeUser ||
      initialGrade.changeUser ||
      grade.fieldNames?.CHG_USER?.neW_VALUE,
    changeTerm:
      grade.changeTerm ||
      initialGrade.changeTerm ||
      grade.fieldNames?.CHG_TERM?.neW_VALUE,
  });
  const fetchIpAddress = async () => {};

  fetchIpAddress();
  function formatDate(dateTime: string) {
    const date = new Date(dateTime);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata", // Specify the Indian time zone
    });
    return `${formattedDate} ${formattedTime}`;
  }

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };

  const createTx = useMutation((payload: any) => insertTransaction(payload), {
    onSuccess: (response: any) => {
      queryClient.invalidateQueries([
        `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
      ]);

      setItemIdForUpdate(undefined);
      closeForm(undefined);
      toast.success(
        <div>
          <h4 className="alert-heading">Successfully Submitted Form...</h4>
          <p className="alert alert-success" role="alert">
            <h6>{response?.message}</h6>
          </p>
        </div>
      );
      cancel(true);
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Failed to submit Form...</h4>
          <p className="alert alert-danger" role="alert">
            <h6>{response?.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formik = useFormik({
    initialValues: gradeForEdit,
    validationSchema: editGradeSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);
      console.log("id for grade", values.id);
      if (isNotEmpty(values.id)) {
        const payload: Gradetx = {
          tableName: "GRADE",
          keyVar: "",
          wfLevel: 1,
          conf: "N",
          siteCode: "S0001",
          emp: currentUser?.id?.toString() || "",
          filePath: null,
          file: null,
          strTranId: values.fieldNames ? values.id : "",
          folderPath: null,
          chG_USER: currentUser?.id?.toString() || "",
          chG_TERM: "TERM",
          remarks: grade.changeReason,
          chG_DATE: new Date(),

          fieldUpdates: [
            {
              fieldName: "GRADE_CODE",
              oldValue:
                grade.gradeId || grade.fieldNames?.GRADE_CODE?.olD_VALUE || "",
              newValue:
                grade.gradeId || grade.fieldNames?.GRADE_CODE?.olD_VALUE || "",
            },
            {
              fieldName: "DESCR",
              oldValue: grade.description || grade.fieldNames?.DESCR?.olD_VALUE,
              newValue: values.description,
            },
            {
              fieldName: "SH_DESCR",
              oldValue:
                grade.shortDescription || grade.fieldNames?.SH_DESCR?.olD_VALUE,
              newValue: values.shortDescription,
            },
            {
              fieldName: "CADRE_CODE",
              oldValue:
                grade.cadreCode || grade.fieldNames?.CADRE_CODE?.olD_VALUE,
              newValue:
                cadreCode ||
                values.cadreCode ||
                grade.fieldNames?.CADRE_CODE?.olD_VALUE, //|| cadre.banD_CODE,
            },

            {
              fieldName: "NOTICE_PRD",
              oldValue:
                grade.noticePeriod?.toString() ||
                grade.fieldNames?.NOTICE_PRD?.olD_VALUE,
              newValue: values.noticePeriod?.toString(),
            },
            {
              fieldName: "CHG_USER",
              oldValue:
                grade?.changeUser || grade.fieldNames?.CHG_USER?.olD_VALUE,
              newValue: values?.changeUser,
            },
            {
              fieldName: "CHG_TERM",
              oldValue:
                grade.changeTerm || grade.fieldNames?.CHG_TERM?.olD_VALUE,
              newValue: "TERM",
            },
            {
              fieldName: "CHG_DATE",
              oldValue:
                grade.changeDate || grade.fieldNames?.CHG_DATE?.olD_VALUE,
              newValue: new Date(),
            },
            {
              fieldName: "STATUS",
              oldValue: grade.status || grade.fieldNames?.STATUS?.olD_VALUE,
              newValue: values.status || grade.status,
            },
          ],
        };
        createTx.mutate(payload);
        setSubmitting(false);
      } else {
        const payload: Gradetx = {
          tableName: "GRADE",
          keyVar: "",
          wfLevel: 1,
          conf: "N",
          siteCode: "S0001",
          emp: currentUser?.id?.toString() || "",
          filePath: null,
          file: null,
          folderPath: null,
          chG_USER: currentUser?.id?.toString() || "",
          chG_TERM: "TERM",
          chG_DATE: new Date(),
          remarks: "New Grade added",

          fieldUpdates: [
            {
              fieldName: "GRADE_CODE",
              oldValue: "",
              newValue: "",
            },
            {
              fieldName: "DESCR",
              oldValue: "",
              newValue: values.description,
            },
            {
              fieldName: "SH_DESCR",
              oldValue: "",
              newValue: values.shortDescription,
            },
            {
              fieldName: "CADRE_CODE",
              oldValue: "",
              newValue: cadreCode,
            },
            {
              fieldName: "NOTICE_PRD",
              oldValue: "",
              newValue: values.noticePeriod?.toString(),
            },
            {
              fieldName: "STATUS",
              oldValue: "",
              newValue: values.status,
            },
            {
              fieldName: "CHG_USER",
              oldValue: "",
              newValue: currentUser?.id,
            },
            {
              fieldName: "CHG_TERM",
              oldValue: "",
              newValue: "TERM",
            },
            {
              fieldName: "CHG_DATE",
              oldValue: "",
              newValue: new Date(),
            },
          ],
        };

        createTx.mutate(payload);
        setSubmitting(false);
      }
    },
  });

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column " id="kt_modal_add_user_scroll">
          {formMode !== Mode.ADD && (
            <div className="row mb-3">
              <div className="fv-row">
                <label className=" fw-bold fs-6 mb-2">Grade Code :</label>
                <input
                  className="form-control form-control-solid mb-3 mb-lg-0"
                  value={formik.values.id ? formik.values.id : ""}
                  disabled={formMode === Mode.VIEW || formMode === Mode.EDIT}
                />
              </div>
            </div>
          )}
          <div className="row mb-3">
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">Grade Name</label>
                <input
                  placeholder="Description"
                  {...formik.getFieldProps("description")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "description",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.description && formik.errors.description,
                    },
                    {
                      "is-valid":
                        formik.touched.description &&
                        !formik.errors.description,
                    }
                  )}
                  type="description"
                  name="description"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    formMode === Mode.VIEW
                  }
                />
                {formik.touched.description && formik.errors.description && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.description}</span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Short Description
                </label>
                <input
                  placeholder="Short Description"
                  {...formik.getFieldProps("shortDescription")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "shortDescription",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.shortDescription &&
                        formik.errors.shortDescription,
                    },
                    {
                      "is-valid":
                        formik.touched.shortDescription &&
                        !formik.errors.shortDescription,
                    }
                  )}
                  type="shortDescription"
                  name="shortDescription"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    formMode === Mode.VIEW
                  }
                />
                {formik.touched.shortDescription &&
                  formik.errors.shortDescription && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.shortDescription}</span>
                    </div>
                  )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="required  fs-6 fw-bold">Cadre</label>
                <select
                  name="cadreCode"
                  // className='form-select form-select-solid fw-bolder'
                  className={clsx(
                    "form-select form-select-solid fw-bolder",
                    {
                      "is-invalid":
                        formik.touched.cadreCode && formik.errors.cadreCode,
                    },
                    {
                      "is-valid":
                        formik.touched.cadreCode && !formik.errors.cadreCode,
                    }
                  )}
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    formMode === Mode.VIEW
                  }
                  onChange={(e) => {
                    formik.setFieldValue("cadreCode", e.target.value);
                    setCadreCode(e.target.value);
                  }}
                  value={formik.values.cadreCode}
                >
                  <option value=""></option>
                  {cadreCodes.map((cadreCode) => (
                    <option
                      key={cadreCode.cadreCode}
                      value={cadreCode.cadreCode}
                    >
                      {cadreCode.cadre.toUpperCase()}
                    </option>
                  ))}
                </select>
                {formik.touched.cadreCode && formik.errors.cadreCode && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.cadreCode}</span>
                  </div>
                )}
              </div>
            </div>

            <div className="col-md-6">
              <div className="fv-row">
                <label className=" fw-bold fs-6 mb-2">
                  Notice Period(Days)
                </label>
                <input
                  placeholder="Notice Period"
                  {...formik.getFieldProps("noticePeriod")}
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.noticePeriod &&
                        formik.errors.noticePeriod,
                    },
                    {
                      "is-valid":
                        formik.touched.noticePeriod &&
                        !formik.errors.noticePeriod,
                    }
                  )}
                  type="number"
                  name="noticePeriod"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    formMode === Mode.VIEW
                  }
                />
                {formik.touched.noticePeriod && formik.errors.noticePeriod && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.noticePeriod}</span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-6">
              <div className="fv-row">
                <label className="fw-bold fs-6 mb-2">Status</label>

                <select
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  value={formik.values.status}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                  }}
                  disabled={formMode === Mode.VIEW || formMode === Mode.ADD}
                >
                  {statuses.map((status) => (
                    <option value={status === "ACTIVE" ? "A" : "N"}>
                      {status}
                    </option>
                  ))}
                </select>
                {formik.touched.status && formik.errors.status && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.status}</span>
                  </div>
                )}
              </div>
            </div>
            {formMode === Mode.VIEW && (
              <>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className="fw-bold fs-6 mb-2">Change User</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      value={formik.values.changeUser?.toUpperCase()}
                      disabled={formMode === Mode.VIEW}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className="fw-bold fs-6 mb-2">Change Term</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      value={formik.values.changeTerm}
                      disabled={formMode === Mode.VIEW}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="fv-row">
                    <label className=" fw-bold fs-6 mb-2">Change Date</label>
                    <input
                      className="form-control form-control-solid mb-3 mb-lg-0"
                      value={
                        formik.values.changeDate
                          ? formatDate(formik.values?.changeDate.toString())
                          : ""
                      }
                      disabled={formMode === Mode.VIEW}
                    />
                  </div>
                </div>
              </>
            )}
          </div>
          {formMode !== Mode.ADD && (
            <div className="row mb-3">
              <div className="fv-row">
                <label className="required fw-bold fs-6 mb-2">
                  Change Reason
                </label>
                <textarea
                  placeholder="Change Reason"
                  {...(formMode === Mode.VIEW
                    ? { ...formik.getFieldProps("changeReason") }
                    : { newReason })}
                  onChange={(e) => {
                    formik.setFieldValue(
                      "changeReason",
                      e.target.value.toUpperCase()
                    );
                    setNewReason(e.target.value.toUpperCase());
                  }}
                  className={clsx(
                    "form-control form-control-solid mb-3 mb-lg-0",
                    {
                      "is-invalid":
                        formik.touched.changeReason &&
                        formik.errors.changeReason,
                    },
                    {
                      "is-valid":
                        formik.touched.changeReason &&
                        !formik.errors.changeReason,
                    }
                  )}
                  name="changeReason"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    formMode === Mode.VIEW
                  }
                />
                {formik.touched.changeReason && formik.errors.changeReason && (
                  <div className="fv-plugins-message-container">
                    <span role="alert">{formik.errors.changeReason}</span>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
        <div className="text-center pt-15">
          {formMode !== Mode.VIEW && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isGradeLoading ||
                formik.isSubmitting ||
                !formik.touched ||
                (formMode !== Mode.ADD && newReason === "")
              }
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isGradeLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isGradeLoading) && <GradeListLoading />}
    </>
  );
};

export { GradeEditModalForm };
