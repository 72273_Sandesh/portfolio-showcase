
import React from 'react';

type Props = {
  cadreName:string | undefined
};

const GradeCadre: React.FC<Props> = ({ cadreName }) => {

  return (
    <div className=' fw-bolder d-flex align-items-right'>
      {cadreName ?(cadreName): <div className='badge badge-danger fw-bolder d-flex align-items-right'>Not Assigned</div>}
    </div>
  );
};

export { GradeCadre };
