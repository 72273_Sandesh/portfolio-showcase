import { Column } from "react-table";
import { GradeInfoCell } from "./GradeInfoCell";
import { GradeActionsCell } from "./GradeActionsCell";
import { GradeCustomHeader } from "./GradeCustomHeader";
import { Grade } from "../../core/_models";
import { GradeShortDescription } from "./GradeShortDescription";
import { GradeChangeDate } from "./GradeChangeDate";
import { GradeChangeUser } from "./GradeChangeUser";
import { GradeCadre } from "./GradeCadre";

const gradeColumns: ReadonlyArray<Column<Grade>> = [
  {
    Header: (props) => (
      <GradeCustomHeader
        tableProps={props}
        title="Grade Name"
        className="min-w-125px text-left "
      />
    ),
    id: "description",
    Cell: ({ ...props }) => (
      <GradeInfoCell grade={props.data[props.row.index]} id={props.data[props.row.index].id} />
    ),
  },
  {
    Header: (props) => (
      <GradeCustomHeader
        tableProps={props}
        title="Description"
        className="min-w-125px text-left"
      />
    ),
    id: "shortDescription",
    Cell: ({ ...props }) => (
      <GradeShortDescription
        shortDescription={props.data[props.row.index].shortDescription}
      />
    ),
  },
  {
    Header: (props) => (
      <GradeCustomHeader
        tableProps={props}
        title="Cadre"
        className="min-w-125px text-left"
      />
    ),
    id: "cadreName",
    Cell: ({ ...props }) => (
      <GradeCadre cadreName={props.data[props.row.index].cadreName} />
    ),
  },
  {
    Header: (props) => (
      <GradeCustomHeader
        tableProps={props}
        title="Change User"
        className="min-w-125px text-left"
      />
    ),
    id: "changeUser",
    Cell: ({ ...props }) => (
      <GradeChangeUser changeUser={props.data[props.row.index].changeUser} />
    ),
  },
  {
    Header: (props) => (
      <GradeCustomHeader
        tableProps={props}
        title="Change Date"
        className="min-w-125px text-left"
      />
    ),
    id: "changeDate",
    Cell: ({ ...props }) => (
      <GradeChangeDate
        changeDate={props.data[props.row.index].changeDate ?? new Date()}
      />
    ),
  },
  {
    Header: (props) => (
      <GradeCustomHeader
        tableProps={props}
        title="Status"
        className="min-w-125px text-left"
      />
    ),
    accessor: "status",
    Cell: ({ value }) => (
      <span>
        <div className=" fw-bolder d-flex align-items-right">
          <div className="d-flex align-items-right">
            {
              <div className="d-flex align-items-right">
                {value === "A" ? (
                  <div className="badge badge-light-success fw-bolder d-flex align-items-right">
                    ACTIVE
                  </div>
                ) : value === "N" ? (
                  <div className="badge badge-light-danger fw-bolder d-flex align-items-right">
                    INACTIVE
                  </div>
                ) : (
                  <div className="badge d-flex align-items-right"></div>
                )}
              </div>
            }
          </div>
        </div>
      </span>
    ),
  },
  {
    Header: (props) => (
      <GradeCustomHeader
        tableProps={props}
        title="Actions"
        className="min-w-100px text-left"
      />
    ),
    id: "actions",
    Cell: ({ ...props }) => (
      <GradeActionsCell id={props.data[props.row.index].id} />
    ),
  },
];

export { gradeColumns };
