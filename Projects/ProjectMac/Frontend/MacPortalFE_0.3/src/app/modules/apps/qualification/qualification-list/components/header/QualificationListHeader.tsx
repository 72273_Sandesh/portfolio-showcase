import {useListView} from '../../core/ListViewProvider'
import {QualificationListToolbar} from './QualificationListToolbar'
import {QualificationListGrouping} from './QualificationListGrouping'
import {QualificationListSearchComponent} from './QualificationListSearchComponent'


const QualificationListHeader = () => {
  const {selected} = useListView()
  return (
    <div className='card-header border-0 pt-6'>
      <QualificationListSearchComponent />
      {/* begin::Card toolbar */}
      <div className='card-toolbar'>
        {/* begin::Group actions */}
        {selected.length > 0 ? <QualificationListGrouping /> : <QualificationListToolbar />}
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export {QualificationListHeader}
