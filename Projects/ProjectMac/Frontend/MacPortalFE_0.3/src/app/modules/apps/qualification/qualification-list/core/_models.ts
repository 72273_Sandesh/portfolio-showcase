import { ID, Response } from "../../../../../../_metronic/helpers";
export type Qualification = {
  //Qualification previous fields
  id?: ID;
  qual_code?: string;
  name?: string;
  shortdescr?: string;
  eqival?: string;
  type?: string;
  changeDate?: Date | string;
  changeUser?: string;
  changeTerm?: string;
  status?: string;
  qualSalStrucDesc?: string;
  changeReason?: string;
  responseStatus?: string;
  message?: string;

  //new fields used for tx
  wF_PROCESS_LEVEL?: number;
  emP_CODE?: string;
  remarks?: string;
  SessionempCode?: string;
  moduleType?: string;
  fieldNames?: {
    QLF_CODE: { olD_VALUE: string; neW_VALUE: string };
    DESCR: { olD_VALUE: string; neW_VALUE: string };
    SH_DESCR: { olD_VALUE: string; neW_VALUE: string };
    EQIVAL: { olD_VALUE: string; neW_VALUE: string };
    TYPE?: { olD_VALUE: string; neW_VALUE: string };
    STATUS: { olD_VALUE: string; neW_VALUE: string };
    CHG_TERM?: { olD_VALUE: string; neW_VALUE: string };
    CHG_DATE?: { olD_VALUE: string; neW_VALUE: string };
    CHG_USER: { olD_VALUE: string; neW_VALUE: string };
  };
};

//model for create and update tx payload
export type Qualificationtx = {
  responseStatus?: string;
  message?: string;
  tableName: string;
  keyVar: string;
  wfLevel: number;
  conf: string;
  siteCode: string;
  emp: string;
  filePath: string | null;
  file: string | null;
  folderPath: string | null;
  chG_DATE?: Date | string;
  strTranId?: string | null | number;
  chG_USER?: string;
  chG_TERM?: string;
  remarks?: string;
  fieldUpdates: [
    {
      fieldName: string;
      oldValue: string | number | undefined;
      newValue: string | number | undefined;
    }
  ];
};
export type QualificationQueryResponse = Response<Array<Qualification>>;

export const initialQualification: Qualification = {};
