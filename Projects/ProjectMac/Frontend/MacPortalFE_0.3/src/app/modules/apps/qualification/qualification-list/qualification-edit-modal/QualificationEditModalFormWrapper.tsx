import {useQuery} from 'react-query'
import {QualificationEditModalForm} from './QualificationEditModalForm'
import {isNotEmpty, QUERIES} from '../../../../../../_metronic/helpers'
import {useListView} from '../core/ListViewProvider'
import {getQualificationById} from '../core/_requests'

const QualificationEditModalFormWrapper = () => {
  const {itemIdForUpdate, setItemIdForUpdate} = useListView()
  const enabledQuery: boolean = isNotEmpty(itemIdForUpdate)
  const {
    isLoading,
    data: qualification,
    error,
  } = useQuery(
    `${QUERIES.QUALIFICATION_LIST}-qualification-${itemIdForUpdate}`,
    () => {
      return getQualificationById(itemIdForUpdate)
    },
    {
      cacheTime: 0,
      enabled: enabledQuery,
      onError: (err) => {
        setItemIdForUpdate(undefined)
        console.error(err)
      },
    }
  )

  if (!itemIdForUpdate) {
    return <QualificationEditModalForm isQualificationLoading={isLoading} qualification={{id: undefined}} />
  }

  if (!isLoading && !error && qualification) {
    return <QualificationEditModalForm isQualificationLoading={isLoading} qualification={qualification} />
  }

  return null
}

export {QualificationEditModalFormWrapper}
