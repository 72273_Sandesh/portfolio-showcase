// import {FC} from 'react'

// type Props = {
//   chG_DATE:Date
// }

// const DepartmentLastLoginCell: FC<Props> = ({chG_DATE}) => (
  
//   <div className='badge badge-light fw-bolder d-flex align-items-right '>{chG_DATE}</div>
// )

// export {DepartmentLastLoginCell}


import React from 'react';

type Props = {
  changeDate: Date;
};

const QualificationLastLoginCell: React.FC<Props> = ({ changeDate }) => {
  const formattedDate = new Date(changeDate).toLocaleString('en-GB', {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric',
    // hour: '2-digit',
    // minute: '2-digit',
    // second: '2-digit',
  });

  return (
    <div className=' fw-bolder d-flex align-items-right'>
    {changeDate ?
      <div className='badge badge-light fw-bolder d-flex align-items-right'>{formattedDate}</div>
      : <div className='badge badge-danger fw-bolder d-flex align-items-right'>NOT CHANGED</div>
    }
  </div>

  );
};

export { QualificationLastLoginCell };
