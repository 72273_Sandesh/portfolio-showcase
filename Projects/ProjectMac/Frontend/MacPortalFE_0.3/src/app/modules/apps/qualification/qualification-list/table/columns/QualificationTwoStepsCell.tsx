import { FC } from "react";

type Props = {
  type: string | undefined; // Allow undefined here
};

const QualificationTwoStepsCell: FC<Props> = ({ type }) => (
  <div className="fw-bolder d-flex align-items-right">
    <div>
      {type ? (
        type.toUpperCase()
      ) : (
        <div className="badge badge-danger fw-bolder d-flex align-items-right">
          NOT ASSIGNED
        </div>
      )}
    </div>
  </div>
);

export { QualificationTwoStepsCell };
