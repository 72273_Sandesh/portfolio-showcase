import React from "react";
import { useQueryResponseData } from "../../core/QueryResponseProvider";

const MasterDetailsTableFormat = () => {
  const masterDetails = useQueryResponseData();

  return (
    <table id="table-to-export">
      <thead>
        <tr>
          <th>Transaction Id</th>
          <th>Master Name</th>
          <th>Level</th>
          <th>Status</th>
          <th>Transaction Date</th>
        </tr>
      </thead>
      <tbody>
        {masterDetails.map((masterDetail) => (
          <tr key={masterDetail.id}>
            <td>{masterDetail.id}</td>
            <td>{masterDetail.tablE_NAME}</td>
            <td>{masterDetail.wF_PROCESS_LEVEL}</td>
            <td>{masterDetail.confirmed}</td>
            <td>{masterDetail.traN_DATE}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default MasterDetailsTableFormat;
