import { ID, Response } from "../../../../../../_metronic/helpers";
export type MasterDetails = {
  id?: ID;
  traN_DATE?: string;
  tablE_NAME?: string;
  keY_VAR?: string;
  wF_PROCESS_LEVEL?: number | undefined;
  sqL_QRY?: string;
  confirmed?: string;
  chG_DATE?: string;
  chG_USER?: string;
  chG_TERM?: string;
  emP_CODE?: string;
  remarks?: string;
  neW_VALUE?: string;
  filE_PATH?: string;

  fieldNames: {
    //fields for cadre
    cadrE_CODE: { olD_VALUE: string; neW_VALUE: string };
    descr: { olD_VALUE: string; neW_VALUE: string };
    sH_DESCR: { olD_VALUE: string; neW_VALUE: string };
    banD_CODE: { olD_VALUE: string; neW_VALUE: string };
    chG_USER: { olD_VALUE: string; neW_VALUE: string };
    chG_TERM: { olD_VALUE: string; neW_VALUE: string };
    chG_DATE: { olD_VALUE: string; neW_VALUE: string };
    status: { olD_VALUE: string; neW_VALUE: string };

    //fields for grade
    GRADE_CODE: { olD_VALUE: string; neW_VALUE: string };
    CADRE_CODE: { olD_VALUE: string; neW_VALUE: string };
    NOTICE_PRD: { olD_VALUE: string; neW_VALUE: string };
    changeTerm: { olD_VALUE: string; neW_VALUE: string };
    changeDate: { olD_VALUE: string; neW_VALUE: string };
    STATUS: { olD_VALUE: string; neW_VALUE: string };

    //fields for department
    DEPT_CODE: { olD_VALUE: string; neW_VALUE: string };
    DESCR: { olD_VALUE: string; neW_VALUE: string };
    SH_DESCR: { olD_VALUE: string; neW_VALUE: string };
    DEPT_HEAD: { olD_VALUE: string; neW_VALUE: string };
    CHG_USER: { olD_VALUE: string; neW_VALUE: string };
    CHG_TERM: { olD_VALUE: string; neW_VALUE: string };
    CHG_DATE: { olD_VALUE: string; neW_VALUE: string };

    //fields for qualification
    QLF_CODE: { olD_VALUE: string; neW_VALUE: string };
    EQIVAL: { olD_VALUE: string; neW_VALUE: string };
    TYPE: { olD_VALUE: string; neW_VALUE: string };

    //fields for designation
    DESIGN_CODE: { olD_VALUE: string; neW_VALUE: string };
    DESIGNATION: { olD_VALUE: string; neW_VALUE: string };
    LONG_DESIGNATION: { olD_VALUE: string; neW_VALUE: string };
  };

  reviewers: [
    {
      traN_ID: string;
      leveL_NO: number;
      confirmed: string;
      emP_CODE__REVIEW: string | number | undefined;
      shorT_NAME: string;
      chG_DATE: string;
    }
  ];

  //CADRE
  cadrE_CODE?: string;
  descr?: string;
  sH_DESCR?: string;
  lvE_TBLNO?: string;
  udF_NUM1?: number | string | null;
  cadrE_ID?: string;
  cadrE_ORDER?: number;
  banD_CODE?: string;
  mkit?: number;
  mmed?: number;

  chG_REASON?: string;
  cadreCategory?: string;
  status?: string;
  responseStatus?: string;
  message?: string;
  SessionempCode?: string;
  moduleType?: string;

  //DEPARTMENT
  deptCode?: string;
  name?: string;
  shortDescription?: string;
  deptHead?: string;
  changeUser?: string;

  //GRADE
  GRADE_CODE?: string;
  CADRE_CODE?: string;
  DESCR?: string;
  SH_DESCR?: string;
  noticePeriod?: string;
  changeTerm?: string;
  changeDate?: string;

  //Qualification
  QLF_CODE: string;
  EQIVAL: string;
  TYPE?: string;
};

export type MasterDetailsQueryResponse = Response<Array<MasterDetails>>;

export const initialMasterDetails: MasterDetails = {};
