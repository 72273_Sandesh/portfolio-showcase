import axios, { AxiosResponse } from "axios";
import { ID, Response } from "../../../../../../_metronic/helpers";
import { MasterDetails, MasterDetailsQueryResponse } from "./_models";

const MASTERDETAILS_URL = import.meta.env.VITE_DEPT_API_URL;
const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;

const getMasterDetails = (
  query: string,
  loggedInUser: string | number | undefined
): Promise<MasterDetailsQueryResponse> => {
  return axios
    .get(
      `${MASTERTX_URL}/GetAllTransaction?${query}&SessionempCode=${loggedInUser}`
    )
    .then((d: AxiosResponse<MasterDetailsQueryResponse>) => d.data);
};

const getMasterDetailsById = (id: ID): Promise<MasterDetails | undefined> => {
  return axios
    .get(`${MASTERTX_URL}/GetTransactionByID?tranId=${id}`)
    .then((response: AxiosResponse<Response<MasterDetails>>) => response.data)
    .then((data: Response<MasterDetails>) => {
      return data.transactions[0];
    });
};

const approveMasterDetails = (
  id: ID,
  tablE_NAME: string | undefined,
  // chG_USER:string ,
  // SessionempCode:string,
  emP_CODE: string | undefined,
  remarks: string | undefined,
  wF_PROCESS_LEVEL: string | undefined,
  employeecode: string,
  // moduleType:string,
  ip: string

): Promise<void> => {
  return (
    axios
      .post(
        `${MASTERTX_URL}/approve?tranId=${id}&ipAddress=${ip}&empCode=${emP_CODE}&SessionempCode=${employeecode}&tableName=${tablE_NAME}&wfProcLevel=${wF_PROCESS_LEVEL}&remarks=${remarks}`
        //https://localhost:44360/approve?tranId=123&ipAddress=123&empCode=123&SessionempCode=123&tableName=123&wfProcLevel=123&moduleType=123&remarks=123
      )
      .then((response) => {
        return response.data;
      })
  );
};

const declineMasterDetails = (payload: object): Promise<void> => {
  return axios.post(`${MASTERTX_URL}/reject`, payload).then((response) => {
    return response.data;
  });
};

const createMasterDetails = (
  bankDetails: MasterDetails
): Promise<MasterDetails | undefined> => {
  return axios
    .post(`${MASTERDETAILS_URL}/InsertDepartment`, bankDetails)
    .then((response: AxiosResponse<Response<MasterDetails>>) => response.data)
    .then((response: Response<MasterDetails>) => response.data);
};

const updateMasterDetails = (
  bankDetails: MasterDetails
): Promise<MasterDetails | undefined> => {
  return axios
    .put(
      `${MASTERDETAILS_URL}/UpdateDepartment/${bankDetails.emP_CODE}`,
      bankDetails
    )
    .then((response: AxiosResponse<Response<MasterDetails>>) => response.data)
    .then((response: Response<MasterDetails>) => response.data);
};

const deleteMasterDetails = (bankDetailsId: ID): Promise<void> => {
  return axios.delete(`${MASTERDETAILS_URL}/${bankDetailsId}`).then(() => {});
};

const deleteSelectedMasterDetails = (
  bankDetailsIds: Array<ID>
): Promise<void> => {
  const requests = bankDetailsIds.map((id) =>
    axios.delete(`${MASTERDETAILS_URL}/${id}`)
  );
  return axios.all(requests).then(() => {});
};

export {
  getMasterDetails,
  deleteMasterDetails,
  deleteSelectedMasterDetails,
  getMasterDetailsById,
  createMasterDetails,
  updateMasterDetails,
  approveMasterDetails,
  declineMasterDetails,
};
