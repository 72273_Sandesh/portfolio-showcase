import { useQuery } from "react-query";
import { isNotEmpty, Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { useListView } from "../core/ListViewProvider";
import { getMasterDetailsById } from "../core/_requests";
import { MasterAddTxModalForm } from "./MasterAddTxModalForm";
import { CadreTxEditModalForm } from "./CadreTxEditModalForm";
import { DepartmentTxEditModalForm } from "./DepartmentTxEditModalForm";
import { DesignationTxEditModalForm } from "./DesignationTxEditModalForm";
import { GradeTxEditModalForm } from "./GradeTxEditModalForm";
import { CadreEditModalForm } from "../../../cadre/cadre-list/cadre-edit-modal/CadreEditModalForm";
import { useAuth } from "../../../../auth";
import { DepartmentEditModalForm } from "../../../department/department-list/department-edit-modal/DepartmentEditModalForm";
import { GradeEditModalForm } from "../../../grade/grade-list/grade-edit-modal/GradeEditModalForm";
import { QualificationEditModalForm } from "../../../qualification/qualification-list/qualification-edit-modal/QualificationEditModalForm";
import { QualificationTxEditModalForm } from "./QualificationTxEditModalForm";
import { DesignationEditModalForm } from "../../../designation/designations-list/designation-edit-modal/DesignationEditModalForm";

const MasterDetailsEditModalFromWrapper = () => {
  const { currentUser } = useAuth();
  const loggedInUser = currentUser?.id?.toString();

  const {
    itemIdForUpdate,
    setItemIdForUpdate,
    tableNameForUpdate,
    setTableNameForUpdate,
    formMode,
  } = useListView();
  const enabledQuery: boolean = isNotEmpty(itemIdForUpdate);
  const {
    isLoading,
    data: bankDetails,
    error,
  } = useQuery(
    `${QUERIES.MASTER_DETAILS_LIST}-masterDetails-${itemIdForUpdate}`,
    () => {
      return getMasterDetailsById(itemIdForUpdate);
    },
    {
      cacheTime: 0,
      enabled: enabledQuery,
      onError: (err) => {
        setItemIdForUpdate(undefined);
        setTableNameForUpdate(undefined);
        console.error(err);
      },
    }
  );

  if (!itemIdForUpdate) {
    return (
      <MasterAddTxModalForm
        isBankDetailsLoading={isLoading}
        bankDetails={{ id: undefined }}
      />
    );
  }

  if (!isLoading && !error && bankDetails) {
    if (tableNameForUpdate === "CADRE") {
      return bankDetails.emP_CODE === loggedInUser && formMode === Mode.EDIT ? (
        <CadreEditModalForm isCadreLoading={isLoading} cadre={bankDetails} />
      ) : (
        <CadreTxEditModalForm
          isBankDetailsLoading={isLoading}
          masterDetails={bankDetails}
        />
      );
    } else if (tableNameForUpdate === "DEPARTMENT") {
      return bankDetails.emP_CODE === loggedInUser && formMode === Mode.EDIT ? (
        <DepartmentEditModalForm
          isDepartmentLoading={isLoading}
          department={bankDetails}
        />
      ) : (
        <DepartmentTxEditModalForm
          isBankDetailsLoading={isLoading}
          masterDetails={bankDetails}
        />
      );
    } else if (tableNameForUpdate === "DESIGNATION") {
      return bankDetails.emP_CODE === loggedInUser && formMode === Mode.EDIT ? (
        <DesignationEditModalForm
          isDesignationLoading={isLoading}
          designation={bankDetails}
        />
      ) : (
        <DesignationTxEditModalForm
          isBankDetailsLoading={isLoading}
          masterDetails={bankDetails}
        />
      );
    } else if (tableNameForUpdate === "GRADE") {
      return bankDetails.emP_CODE === loggedInUser && formMode === Mode.EDIT ? (
        <GradeEditModalForm isGradeLoading={isLoading} grade={bankDetails} />
      ) : (
        <GradeTxEditModalForm
          isBankDetailsLoading={isLoading}
          masterDetails={bankDetails}
        />
      );
    } else if (tableNameForUpdate === "QUALIFICATION") {
      return bankDetails.emP_CODE === loggedInUser && formMode === Mode.EDIT ? (
        <QualificationEditModalForm
          isQualificationLoading={isLoading}
          qualification={bankDetails}
        />
      ) : (
        <QualificationTxEditModalForm
          isBankDetailsLoading={isLoading}
          masterDetails={bankDetails}
        />
      );
    }
  }

  return null;
};

export { MasterDetailsEditModalFromWrapper };
