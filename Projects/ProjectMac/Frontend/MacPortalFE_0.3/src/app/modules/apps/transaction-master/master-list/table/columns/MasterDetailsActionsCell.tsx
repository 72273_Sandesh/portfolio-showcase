// import {FC, useEffect} from 'react'
// import {useMutation, useQueryClient} from 'react-query'
// import {MenuComponent} from '../../../../../../../_metronic/assets/ts/components'
// import {ID, KTIcon, QUERIES} from '../../../../../../../_metronic/helpers'
// import {useListView} from '../../core/ListViewProvider'
// import {useQueryResponse} from '../../core/QueryResponseProvider'
// import {deleteBankDetails} from '../../core/_requests'
// import { setGlobalVariable } from '../../../GlobalVariable'

// type Props = {
//   id: ID
// }

// const BankDetailsActionsCell: FC<Props> = ({id}) => {
//   const {setItemIdForUpdate} = useListView()
//   const {query} = useQueryResponse()
//   const queryClient = useQueryClient()

//   useEffect(() => {
//     MenuComponent.reinitialization()
//   }, [])

//   const openEditModal = () => {
//     setItemIdForUpdate(id)
//     setGlobalVariable("Edit Bank Details")
//   }
//   const openInfoModal = () => {
//     setItemIdForUpdate(id)
//     setGlobalVariable("View Bank Details")
//   } //logi for Flag for Info

//   const deleteItem = useMutation(() => deleteBankDetails(id), {
//     // 💡 response of the mutation is passed to onSuccess
//     onSuccess: () => {
//       // ✅ update detail view directly
//       queryClient.invalidateQueries([`${QUERIES.BANK_DETAILS_LIST}-${query}`])
//     },
//   })

//   return (
//     <>
//       <a
//         href='#'
//         className='btn btn-light btn-active-light-primary btn-sm justify-content: flex-center'
//         data-kt-menu-trigger='click'
//         data-kt-menu-placement='bottom-end'
//       >
//         Actions
//         <KTIcon iconName='down' className='fs-5 m-0  justify-content: flex-center'  />
//       </a>
//       {/* begin::Menu */}
//       <div
//         className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-20 w-125px py-4'
//         data-kt-menu='true'
//       >
//         {/* begin::Menu item */}
//         <div className='menu-item px-3'>
//           <a className='menu-link px-3' onClick={openInfoModal}>
//             View
//           </a>
//         </div>
//         {/* end::Menu item */}

//         {/* begin::Menu item */}
//         <div className='menu-item px-3'>
//           <a className='menu-link px-3'>
//             Approve
//           </a>
//         </div>
//         {/* end::Menu item */}

//         {/* begin::Menu item */}
//         <div className='menu-item px-3'>
//           <a className='menu-link px-3'>
//             Reject
//           </a>
//         </div>
//         {/* end::Menu item */}

//         {/* begin::Menu item */}
//         {/* <div className='menu-item px-3'>
//           <a
//             className='menu-link px-3'
//             data-kt-users-table-filter='delete_row'
//             onClick={async () => await deleteItem.mutateAsync()}
//           >
//             Deactivate
//           </a>
//         </div> */}
//         {/* end::Menu item */}
//       </div>
//       {/* end::Menu */}
//     </>
//   )
// }

// export {BankDetailsActionsCell}

// import {FC, useEffect} from 'react'
// import {useMutation, useQueryClient} from 'react-query'
// import {MenuComponent} from '../../../../../../../_metronic/assets/ts/components'
// import {ID, KTIcon, Mode, QUERIES} from '../../../../../../../_metronic/helpers'
// import {useListView} from '../../core/ListViewProvider'
// import {useQueryResponse} from '../../core/QueryResponseProvider'
// import {deleteMasterDetails} from '../../core/_requests'
//
// type Props = {
//   id: ID
// }

// const MasterDetailsActionsCell: FC<Props> = ({id}) => {
//   const {setItemIdForUpdate} = useListView()
//   const {query} = useQueryResponse()
//   const queryClient = useQueryClient()

//   useEffect(() => {
//     MenuComponent.reinitialization()
//   }, [])

//   const openEditModal = () => {
//     setItemIdForUpdate(id)
//     setGlobalVariable("editBankDetails")
//   }
//   const openInfoModal = () => {
//     setItemIdForUpdate(id)
//     setGlobalVariable("infoBankDetails")
//   }

//   const deleteItem = useMutation(() => deleteMasterDetails(id), {
//     // 💡 response of the mutation is passed to onSuccess
//     onSuccess: () => {
//       // ✅ update detail view directly
//       queryClient.invalidateQueries([`${QUERIES.MASTER_DETAILS_LIST}-${query}`])
//     },
//   })

//   return (
//     <>
//       <a
//         href='#'
//         className='btn btn-light btn-active-light-primary btn-sm justify-content: flex-center'
//         data-kt-menu-trigger='click'
//         data-kt-menu-placement='bottom-end'
//       >
//         Actions
//         <KTIcon iconName='down' className='fs-5 m-0  justify-content: flex-center'  />
//       </a>
//       {/* begin::Menu */}
//       <div
//         className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-20 w-125px py-4'
//         data-kt-menu='true'
//       >
//         {/* begin::Menu item */}
//         <div className='menu-item px-3'>
//           <a className='menu-link px-3' onClick={openInfoModal}>
//             View
//           </a>
//         </div>
//         {/* end::Menu item */}

//         {/* begin::Menu item */}
//         <div className='menu-item px-3'>
//           <a className='menu-link px-3' onClick={openEditModal}>
//             Edit
//           </a>
//         </div>
//         {/* end::Menu item */}

//         {/* begin::Menu item */}
//         <div className='menu-item px-3'>
//           <a className='menu-link px-3'>
//             Approve
//           </a>
//         </div>
//         {/* end::Menu item */}

//          {/* begin::Menu item */}
//          <div className='menu-item px-3'>
//           <a className='menu-link px-3'>
//             Decline
//           </a>
//         </div>
//         {/* end::Menu item */}
//       </div>
//       {/* end::Menu */}
//     </>
//   )
// }

// export {MasterDetailsActionsCell}

// import { FC, useEffect, useState } from "react";
// import { useMutation, useQueryClient } from "react-query";
// import { MenuComponent } from "../../../../../../../_metronic/assets/ts/components";
// import { ID, KTIcon, QUERIES } from "../../../../../../../_metronic/helpers";
// import { useListView } from "../../core/ListViewProvider";
// import { useQueryResponse } from "../../core/QueryResponseProvider";
// import {
//   deleteMasterDetails,
//   approveMasterDetails,
//   declineMasterDetails,
// } from "../../core/_requests";
// import { setGlobalVariable } from "../../../GlobalVariable";
// import Swal from "sweetalert2";

// type Props = {
//   id: ID;
// };

// const MasterDetailsActionsCell: FC<Props> = ({ id }) => {
//   const { setItemIdForUpdate } = useListView();
//   const { query } = useQueryResponse();
//   const queryClient = useQueryClient();

//   useEffect(() => {
//     MenuComponent.reinitialization();
//   }, []);

//   const openEditModal = () => {
//     setItemIdForUpdate(id);
//     setGlobalVariable("editMasterDetails");
//   };

//   const openInfoModal = () => {
//     setItemIdForUpdate(id);
//     setGlobalVariable("infoMasterDetails");
//   };

//   const deleteItem = useMutation(() => deleteMasterDetails(id), {
//     onSuccess: () => {
//       queryClient.invalidateQueries([
//         `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
//       ]);
//     },
//   });

//   const approveItem = useMutation(() => approveMasterDetails(id), {
//     onSuccess: () => {
//       Swal.fire({
//         title: "Success!",
//         text: "Item approved successfully.",
//         icon: "success",
//         confirmButtonText: "OK",
//       });
//       queryClient.invalidateQueries([`${QUERIES.MASTER_DETAILS_LIST}-${query}`])
//     },
//     onError: (error: any) => {
//       Swal.fire({
//         title: "Error!",
//         text: `Error approving item: ${error.message || "Unknown error"}`,
//         icon: "error",
//         confirmButtonText: "OK",
//       });
//     },
//   });

//   const declineItem = useMutation(() => declineMasterDetails(id), {
//     onSuccess: () => {
//       Swal.fire({
//         title: "Success!",
//         text: "Item declined successfully.",
//         icon: "success",
//         confirmButtonText: "OK",
//       });
//       queryClient.invalidateQueries([`${QUERIES.MASTER_DETAILS_LIST}-${query}`])
//     },
//     onError: (error: any) => {
//       Swal.fire({
//         title: "Error!",
//         text: `Error declining item: ${error.message || "Unknown error"}`,
//         icon: "error",
//         confirmButtonText: "OK",
//       });
//     },
//   });

//   return (
//     <>
//       <a
//         href="#"
//         className="btn btn-light btn-active-light-primary btn-sm"
//         data-kt-menu-trigger="click"
//         data-kt-menu-placement="bottom-end"
//       >
//         Actions
//         <KTIcon iconName="down" className="fs-5 m-0" />
//       </a>
//       {/* begin::Menu */}
//       <div
//         className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-20 w-125px py-4"
//         data-kt-menu="true"
//       >
//         {/* begin::Menu item */}
//         <div className="menu-item px-3">
//           <a className="menu-link px-3" onClick={openInfoModal}>
//             View
//           </a>
//         </div>
//         {/* end::Menu item */}

//         {/* begin::Menu item */}
//         <div className="menu-item px-3">
//           <a className="menu-link px-3" onClick={openEditModal}>
//             Edit
//           </a>
//         </div>
//         {/* end::Menu item */}

//         {/* begin::Menu item */}
//         <div className="menu-item px-3">
//           <a className="menu-link px-3" onClick={() => approveItem.mutate()}>
//             Approve
//           </a>
//         </div>
//         {/* end::Menu item */}

//         {/* begin::Menu item */}
//         <div className="menu-item px-3">
//           <a className="menu-link px-3" onClick={() => declineItem.mutate()}>
//             Decline
//           </a>
//         </div>
//         {/* end::Menu item */}
//       </div>
//       {/* end::Menu */}
//     </>
//   );
// };

// export { MasterDetailsActionsCell };

import { FC, useEffect } from "react";
import { useMutation, useQueryClient } from "react-query";
import { MenuComponent } from "../../../../../../../_metronic/assets/ts/components";
import {
  ID,
  KTIcon,
  Mode,
  QUERIES,
} from "../../../../../../../_metronic/helpers";
import { useListView } from "../../core/ListViewProvider";
import { useQueryResponse } from "../../core/QueryResponseProvider";
import {
  deleteMasterDetails,
  approveMasterDetails,
  declineMasterDetails,
} from "../../core/_requests";
import { setGlobalVariable } from "../../../GlobalVariable";
import Swal from "sweetalert2";
import { useAuth } from "../../../../../auth";

type Props = {
  id: ID;
  tableName: string | undefined;
  emP_CODE?: string | undefined;
  remarks?: string;
  wF_PROCESS_LEVEL?: number;
};

const MasterDetailsActionsCell: FC<Props> = ({
  id,
  tableName,
  remarks,
  emP_CODE,
  wF_PROCESS_LEVEL,
}) => {
  const { setItemIdForUpdate, setTableNameForUpdate, setFormMode } =
    useListView();
  const { query } = useQueryResponse();
  const queryClient = useQueryClient();

  const { currentUser } = useAuth();
  const loggedInUser = currentUser?.id.toString() || "";
  const ip = "term";

  useEffect(() => {
    MenuComponent.reinitialization();
  }, []);

  const openEditModal = () => {
    setItemIdForUpdate(id);
    setTableNameForUpdate(tableName);
    setFormMode(Mode.EDIT);
  };
  const openInfoModal = () => {
    setItemIdForUpdate(id);
    setTableNameForUpdate(tableName);
    setFormMode(Mode.VIEW);
  };

  const deleteItem = useMutation(() => deleteMasterDetails(id), {
    onSuccess: () => {
      queryClient.invalidateQueries([
        `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
      ]);
    },
  });

  // const approveItem = useMutation(() => approveMasterDetails(id,tableName,loggedInUser,ip), {
  //   onSuccess: () => {
  //     Swal.fire({
  //       title: "Success!",
  //       text: "Transaction approved successfully.",
  //       icon: "success",
  //       confirmButtonText: "OK",
  //     });
  //     queryClient.invalidateQueries([`${QUERIES.MASTER_DETAILS_LIST}-${query}`])
  //   },
  //   onError: (error: Error) => {
  //     Swal.fire({
  //       title: "Error!",
  //       text: `Error approving item: ${error.message || "Unknown error"}`,
  //       icon: "error",
  //       confirmButtonText: "OK",
  //     });
  //   },
  // });
  const approveItem = useMutation(
    () =>
      approveMasterDetails(
        id,
        tableName,
        emP_CODE,
        remarks,
        wF_PROCESS_LEVEL,
        loggedInUser,
        ip
      ),
    {
      onSuccess: (response: any) => {
        const { responseStatus, message } = response.data;
        console.log(response.data);
        if (responseStatus === "S") {
          Swal.fire({
            title: "Success!",
            text: "Transaction approved successfully.",
            icon: "success",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        } else if (responseStatus === "E") {
          Swal.fire({
            title: "Error!",
            text: `Error approving item: ${message.join(", ")}`,
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        } else {
          Swal.fire({
            title: "Error!",
            text: "Unknown response status.",
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        }
      },
      onError: (error: Error) => {
        Swal.fire({
          title: "Error!",
          text: `Error approving item: ${error.message || "Unknown error"}`,
          icon: "error",
          confirmButtonText: "OK",
        });
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);
      },
    }
  );

  const transactionDetails = {
    transactionId: id,
    empCode: loggedInUser,
    remoteAddr: ip,
  };

  const declineItem = useMutation(
    () => declineMasterDetails(transactionDetails),
    {
      // onSuccess: () => {
      //   Swal.fire({
      //     title: "Success!",
      //     text: "Transaction Rejected successfully.",
      //     icon: "success",
      //     confirmButtonText: "OK",
      //   });
      //   queryClient.invalidateQueries([`${QUERIES.MASTER_DETAILS_LIST}-${query}`])
      // },
      // onError: (error: Error) => {
      //   Swal.fire({
      //     title: "Error!",
      //     text: `Error declining item: ${error.message || "Unknown error"}`,
      //     icon: "error",
      //     confirmButtonText: "OK",
      //   });
      // },

      onSuccess: (response: any) => {
        const { responseStatus, message } = response.data;
        console.log(response.data);
        if (responseStatus === "S") {
          Swal.fire({
            title: "Success!",
            text: "Transaction Rejected successfully.",
            icon: "success",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        } else if (responseStatus === "E") {
          Swal.fire({
            title: "Error!",
            text: `Error While Rejecting Transaction: ${message.join(", ")}`,
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        } else {
          Swal.fire({
            title: "Error!",
            text: "Unknown response status.",
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        }
      },
      onError: (error: Error) => {
        Swal.fire({
          title: "Error!",
          text: `Error While Rejecting Transaction: ${
            error.message || "Unknown error"
          }`,
          icon: "error",
          confirmButtonText: "OK",
        });
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);
      },
    }
  );

  const confirmApprove = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "Do you want to approve this Transaction?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, approve it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
    }).then((result) => {
      if (result.isConfirmed) {
        approveItem.mutate();
      }
    });
  };

  const confirmDecline = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "Do you want to Reject this Transaction?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, Reject it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
    }).then((result) => {
      if (result.isConfirmed) {
        declineItem.mutate();
      }
    });
  };

  return (
    <>
      <a
        href="#"
        className="btn btn-light btn-active-light-primary btn-sm"
        data-kt-menu-trigger="click"
        data-kt-menu-placement="bottom-end"
      >
        Actions
        <KTIcon iconName="down" className="fs-5 m-0" />
      </a>
      {/* begin::Menu */}
      <div
        className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-20 w-125px py-4"
        data-kt-menu="true"
      >
        {/* begin::Menu item */}
        <div className="menu-item px-3">
          <a className="menu-link px-3" onClick={openInfoModal}>
            View
          </a>
        </div>
        {/* end::Menu item */}

        {/* begin::Menu item */}
        {emP_CODE === loggedInUser &&
          wF_PROCESS_LEVEL === 1 &&
          !query.includes("txMode") && (
            <div className="menu-item px-3">
              <a className="menu-link px-3" onClick={openEditModal}>
                Edit
              </a>
            </div>
          )}
        {/* end::Menu item */}

        {/* begin::Menu item */}
        {/* <div className="menu-item px-3">
          <a className="menu-link px-3" onClick={openInfoModal}>
            Approve
          </a>
        </div> */}
        {/* end::Menu item */}

        {/* begin::Menu item */}
        {/* <div className="menu-item px-3">
          <a className="menu-link px-3" onClick={openInfoModal}>
            Reject
          </a>
        </div> */}
        {/* end::Menu item */}
      </div>
      {/* end::Menu */}
    </>
  );
};

export { MasterDetailsActionsCell };
