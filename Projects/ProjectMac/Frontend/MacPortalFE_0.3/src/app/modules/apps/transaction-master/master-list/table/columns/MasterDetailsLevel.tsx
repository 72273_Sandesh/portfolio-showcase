import {FC} from 'react'

type Props = {
  wF_PROCESS_LEVEL: string | number | undefined
}

const MasterDetailsLevel: FC<Props> = ({wF_PROCESS_LEVEL}) => (
  <>
   {/* <div className='badge badge-light-success fw-bolder d-flex align-items-center'>{wF_PROCESS_LEVEL ?(wF_PROCESS_LEVEL):('Not Assigned')}</div>*/}
   {/* <div className=' fw-bolder d-flex align-items-right'>
      {wF_PROCESS_LEVEL ?Level (wF_PROCESS_LEVEL): <div className='badge badge-danger fw-bolder d-flex align-items-right'>No data</div>}
    </div> */}
      <div className=' fw-bolder d-flex align-items-right'>
      {wF_PROCESS_LEVEL != null  ? (
        <span>Level {wF_PROCESS_LEVEL}</span>
      ) : (
        <div className='badge badge-danger fw-bolder d-flex align-items-right'>No data</div>
      )}
    </div>
    </> 
)

export {MasterDetailsLevel}
