import React from 'react';

type Props = {
  traN_DATE: string | Date ;
};

const MasterDetailsTxDate: React.FC<Props> = ({ traN_DATE }) => {
  const formattedDate = new Date(traN_DATE).toLocaleString('en-GB', {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric',

  });

  return (
    <div className=' fw-bolder d-flex align-items-right'>
          <div className='badge badge-light fw-bolder d-flex align-items-right'>{formattedDate}</div>
        </div>
    // <div className='badge badge-light fw-bolder d-flex align-items-right'>
    //   {formattedDate}
    // </div>
  );
};

export { MasterDetailsTxDate };
