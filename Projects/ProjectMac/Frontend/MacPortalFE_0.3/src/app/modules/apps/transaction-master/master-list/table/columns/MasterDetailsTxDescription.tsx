import { FC } from "react";

type Props = {
  neW_VALUE: string | undefined;
};

const MasterDetailsTxDescription: FC<Props> = ({ neW_VALUE }) => (
  <>
    <div className=" fw-bolder d-flex align-items-right">
      {neW_VALUE ? (
        neW_VALUE.toUpperCase()
      ) : (
        <div className="badge badge-danger fw-bolder d-flex align-items-right">
          NOT ASSIGNED
        </div>
      )}
    </div>
  </>
);

export { MasterDetailsTxDescription };
