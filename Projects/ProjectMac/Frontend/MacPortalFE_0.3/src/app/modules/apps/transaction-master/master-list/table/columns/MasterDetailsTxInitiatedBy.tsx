import React from 'react';


const MasterDetailsTxInitiatedBy: React.FC = () => {
  return (
    <div className=' fw-bolder d-flex align-items-right'>
          <div className='badge badge-light fw-bolder d-flex align-items-right'>InitiatedBy</div>
        </div>
  );
};

export { MasterDetailsTxInitiatedBy };
