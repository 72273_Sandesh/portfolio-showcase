import React from "react";

const MasterDetailsTxPendingWith: React.FC = () => {
  return (
    <div className=" fw-bolder d-flex align-items-right">
      <div className="badge badge-light fw-bolder d-flex align-items-right">
        AuthorizedPerson
      </div>
    </div>
  );
};

export { MasterDetailsTxPendingWith };
