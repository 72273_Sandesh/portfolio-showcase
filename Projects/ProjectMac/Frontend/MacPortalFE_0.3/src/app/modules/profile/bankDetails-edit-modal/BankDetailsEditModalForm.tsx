import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { toast } from "react-toastify";
import axios from "axios";
import clsx from "clsx";
import { BankDetails } from "../../apps/bank-details/bankDetails-list/core/_models";
import { useAuth } from "../../auth";
import { useListView } from "../../apps/employee/employee-list/core/ListViewProvider";
import { useQueryResponse } from "../../apps/employee/employee-list/core/QueryResponseProvider";
import {
  globalVariable,
  setGlobalVariable,
} from "../../apps/employee/GlobalVariable";
import { isNotEmpty } from "../../../../_metronic/helpers";
import {
  createBankDetails,
  updateBankDetails,
} from "../../apps/bank-details/bankDetails-list/core/_requests";
import { BankDetailsListLoading } from "../../apps/bank-details/bankDetails-list/components/loading/BankDetailsListLoading";
type Props = {
  isBankDetailsLoading: boolean;
  bankDetails: BankDetails;
  onClose: any;
};
interface DropdownEmp {
  id: string;
  emP_FNAME: string;
  emP_LNAME: string;
}
interface BankDetailsDropdown {
  banK_CODE: string;
  banK_NAME: string;
  banK_TYPE: string;
}
const editBankDetailsSchema = Yup.object().shape({
  paymenT_MODE: Yup.string().required("Mode of Payment is required"),
  banK_NAME: Yup.string().required("Bank Name is required"),
  banK_ACCT: Yup.string().required("Account Number is required"),
  banK_IFSC: Yup.string().required("IFSC Code is required"),
  emP_NAME: Yup.string().required("Name of Employee is required"),
  changeReason: Yup.string().required("Remark is required"),
});
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
const BankDetailsEditModalForm: FC<Props> = ({
  bankDetails,
  isBankDetailsLoading,
  onClose,
}) => {
  const { currentUser } = useAuth();
  const { setItemIdForUpdate } = useListView();
  const { refetch } = useQueryResponse();
  const [searchName, setSearchName] = useState("");
  const [dropDownEmpResp, setDropDownEmpResp] = useState<DropdownEmp[]>([]);
  const [bankNameDropdown, setBankNameDropdown] = useState<
    BankDetailsDropdown[]
  >([]);

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/ddl_GetBank`)
      .then((response) => {
        setBankNameDropdown(response.data.banks);
      })
      .catch((error) =>
        console.error("Error fetching Bank Names data:", error)
      );
  }, []);

  const handleSearchChange = async (searchString: string) => {
    setSearchName(searchString);

    if (searchString.length > 2) {
      axios
        .get(
          `${EMPLOYEE_URL}/GetAllEmployees?page=1&items_per_page=100&search=${searchString}`
        )
        .then((response) => {
          setDropDownEmpResp(response.data.data);
        })
        .catch((error) =>
          console.error("Error fetching combined data:", error)
        );
    } else {
      setDropDownEmpResp([]);
    }
  };

  const handleSelectEmployee = async (employee: any) => {
    setDropDownEmpResp([]);
    setSearchName(employee.emP_FNAME);

    axios
      .get(`${EMPLOYEE_URL}/${employee.id}/EMPPER`)
      .then((response) => {
        formik.setValues({
          emP_NAME: response.data[0].emP_NAME,
          emP_CODE: response.data[0].emP_CODE,
          sitE_NAME: response.data[0].sitE_NAME,
          depT_NAME: response.data[0].depT_NAME,
          designation: response.data[0].designation,
          datE_JOIN: response.data[0].datE_JOIN,
          paymenT_MODE: response.data[0].paymenT_MODE,
          banK_NAME: response.data[0].banK_NAME,
          banK_CODE: response.data[0].banK_CODE,
          banK_ACCT: response.data[0].banK_ACCT,
          banK_IFSC: response.data[0].banK_IFSC,
          emP_IMAGE: response.data[0].emP_IMAGE,
          changeReason: response.data[0].changeReason,
        });
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  };

  const [bankDetailsForEdit] = useState<BankDetails>({
    ...bankDetails,
    emP_NAME: bankDetails.emP_NAME,
    emP_CODE: bankDetails.emP_CODE,
    sitE_NAME: bankDetails.sitE_NAME,
    depT_NAME: bankDetails.depT_NAME,
    designation: bankDetails.designation,
    datE_JOIN: bankDetails.datE_JOIN,
    paymenT_MODE: bankDetails.paymenT_MODE,
    banK_NAME: bankDetails.banK_NAME,
    banK_CODE: bankDetails.banK_CODE,
    banK_ACCT: bankDetails.banK_ACCT,
    banK_IFSC: bankDetails.banK_IFSC,
    emP_IMAGE: bankDetails.emP_IMAGE,
    changeReason: bankDetails.changeReason,
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };

  const formik = useFormik({
    initialValues: bankDetailsForEdit,
    validationSchema: editBankDetailsSchema,

    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      if (isNotEmpty(values.emP_CODE) && globalVariable === "editBankDetails") {
        values.changeUser = currentUser?.first_name;
        values.changeTerm = "TERM";
        values.changeDate = new Date();
        const response = await updateBankDetails(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("editBankDetails");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      } else {
        values.id = "null";
        values.changeUser = currentUser?.first_name;
        values.changeTerm = "TERM";
        values.changeDate = new Date();

        const response = await createBankDetails(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("addBankDetails");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      }
    },
  });

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="card-body">
          <div className="bg-light-warning border border-dashed border-gray-300 rounded p-5">
            <div className="row mb-1">
              <div className="position-relative col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="emP_FNAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  EMPLOYEE NAME
                </label>
                {globalVariable !== "addBankDetails" ? (
                  <input
                    type="text"
                    id="emP_FNAME"
                    className="form-control form-control-lg form-control-solid "
                    name="emP_FNAME"
                    value={formik.values.emP_NAME}
                    readOnly
                  />
                ) : (
                  <input
                    type="text"
                    id="emP_FNAME"
                    className={
                      globalVariable !== "addBankDetails"
                        ? "form-control form-control-lg form-control-solid "
                        : "bg-white border border-dark form-control form-control-lg form-control-solid "
                    }
                    name="emP_FNAME"
                    placeholder={
                      globalVariable === "addBankDetails"
                        ? "Search employee..."
                        : "Enter Employee Name"
                    }
                    value={searchName}
                    onChange={(e) => handleSearchChange(e.target.value)}
                    readOnly={globalVariable !== "addBankDetails"}
                  />
                )}
                {dropDownEmpResp.length > 0 && (
                  <ul
                    className="position-absolute bg-white border border-secondary
                       rounded overflow-auto top-100 list-style-none p-0 mt-0 z-index-1 mh-300px min-w-300px"
                    style={{ left: 167 }}
                  >
                    {dropDownEmpResp.map((employee) => (
                      <li
                        className="ms-5 p-2 cursor-pointer text-hover-primary"
                        key={employee.id}
                        value={employee.id}
                        onClick={() => handleSelectEmployee(employee)}
                      >
                        {employee.emP_FNAME} {employee.emP_LNAME}
                      </li>
                    ))}
                  </ul>
                )}
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="emP_CODE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  EMPLOYEE CODE
                </label>
                <input
                  type="text"
                  id="emP_CODE"
                  className="form-control form-control-lg form-control-solid  "
                  name="emP_CODE"
                  placeholder="Enter Employee Code"
                  value={formik.values.emP_CODE}
                  onChange={formik.handleChange}
                  readOnly
                />
              </div>
            </div>

            {globalVariable === "infoBankDetails" && (
              <div className="row mb-1">
                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="trId"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    TRANSACTION ID
                  </label>
                  <input
                    type="text"
                    id="trId"
                    className="form-control form-control-lg form-control-solid "
                    name="trId"
                    placeholder="Enter Transaction Id"
                    value={""}
                    onChange={formik.handleChange}
                    readOnly
                  />
                </div>

                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="trDate"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    TRANSACTION DATE
                  </label>
                  <input
                    type="date"
                    id="trDate"
                    className="form-control form-control-lg form-control-solid"
                    name="trDate"
                    placeholder="Enter Date of Transaction"
                    value={""}
                    onChange={formik.handleChange}
                    readOnly
                  />
                </div>
              </div>
            )}

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sitE_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SITE NAME
                </label>
                <input
                  type="text"
                  id="sitE_NAME"
                  className="form-control form-control-lg form-control-solid "
                  name="sitE_NAME"
                  placeholder="Enter Site Name"
                  value={formik.values.sitE_NAME}
                  onChange={formik.handleChange}
                  readOnly
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="depT_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DEPARTMENT
                </label>
                <input
                  type="text"
                  id="depT_NAME"
                  className="form-control form-control-lg form-control-solid  "
                  name="depT_NAME"
                  placeholder="Enter Department Name"
                  value={formik.values.depT_NAME}
                  onChange={formik.handleChange}
                  readOnly
                />
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="designation"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DESIGNATION
                </label>
                <input
                  type="text"
                  id="designation"
                  className="form-control form-control-lg form-control-solid "
                  name="designation"
                  placeholder="Enter Designation"
                  value={formik.values.designation}
                  onChange={formik.handleChange}
                  readOnly
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="datE_JOIN"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  JOIN DATE
                </label>
                <input
                  type="date"
                  id="datE_JOIN"
                  className="form-control form-control-lg form-control-solid  "
                  name="datE_JOIN"
                  placeholder="Enter Designation"
                  value={formik.values.datE_JOIN?.split("T")[0]}
                  onChange={formik.handleChange}
                  readOnly
                />
              </div>
            </div>
          </div>

          <div className="p-5">
            {globalVariable === "addBankDetails" &&
            searchName === "" &&
            dropDownEmpResp.length === 0 ? (
              <div className="text-center">
                <img
                  src="/media/logos/searchEmpWarn.png"
                  style={{ height: "150px" }}
                />
                <h1>Please Search Employee Name First...</h1>
              </div>
            ) : (
              <>
                <div className="row mb-1">
                  <div className="col-lg-6">
                    <div className="d-flex align-items-center mb-1">
                      <label
                        htmlFor="paymenT_MODE"
                        className="col-form-label required fw-bold fs-6 me-2"
                        style={{ minWidth: "150px" }}
                      >
                        MODE OF PAYMENT
                      </label>
                      <select
                        id="paymenT_MODE"
                        className={clsx(
                          "form-select form-select-lg form-select-solid ",
                          {
                            "is-invalid":
                              formik.touched.paymenT_MODE &&
                              formik.errors.paymenT_MODE,
                          },
                          {
                            "is-valid":
                              formik.touched.paymenT_MODE &&
                              !formik.errors.paymenT_MODE,
                          }
                        )}
                        name="paymenT_MODE"
                        value={formik.values.paymenT_MODE}
                        onChange={formik.handleChange}
                        disabled={globalVariable === "infoBankDetails"}
                      >
                        <option value={formik.values.paymenT_MODE}>
                          {formik.values.paymenT_MODE}
                        </option>
                      </select>
                    </div>
                    {formik.touched.paymenT_MODE &&
                      formik.errors.paymenT_MODE && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert">{formik.errors.paymenT_MODE}</span>
                        </div>
                      )}
                  </div>
                  <div className="col-lg-6">
                    <div className="d-flex align-items-center mb-1">
                      <label
                        htmlFor="banK_NAME"
                        className="col-form-label required fw-bold fs-6 me-2"
                        style={{ minWidth: "150px" }}
                      >
                        BANK NAME
                      </label>
                      <select
                        id="banK_NAME"
                        // className="form-select form-select-lg form-select-solid "
                        className={clsx(
                          "form-select form-select-lg form-select-solid ",
                          {
                            "is-invalid":
                              formik.touched.banK_NAME &&
                              formik.errors.banK_NAME,
                          },
                          {
                            "is-valid":
                              formik.touched.banK_NAME &&
                              !formik.errors.banK_NAME,
                          }
                        )}
                        name="banK_NAME"
                        value={formik.values.banK_NAME}
                        onChange={formik.handleChange}
                        disabled={globalVariable === "infoBankDetails"}
                      >
                        <option value={formik.values.banK_CODE}>
                          {formik.values.banK_NAME}
                        </option>
                        {bankNameDropdown.map((bankName) => (
                          <option value={bankName.banK_CODE}>
                            {bankName.banK_NAME}
                          </option>
                        ))}
                      </select>
                    </div>
                    {formik.touched.banK_NAME && formik.errors.banK_NAME && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert">{formik.errors.banK_NAME}</span>
                      </div>
                    )}
                  </div>
                </div>

                <div className="row mb-1">
                  <div className="col-lg-6">
                    <div className="d-flex align-items-center mb-1">
                      <label
                        htmlFor="banK_ACCT"
                        className="col-form-label required fw-bold fs-6 me-2"
                        style={{ minWidth: "150px" }}
                      >
                        ACCOUNT NUMBER
                      </label>
                      <input
                        type="number"
                        id="banK_ACCT"
                        // className="form-control form-control-lg form-control-solid "
                        className={clsx(
                          "form-control form-control-lg form-control-solid",
                          {
                            "is-invalid":
                              formik.touched.banK_ACCT &&
                              formik.errors.banK_ACCT,
                          },
                          {
                            "is-valid":
                              formik.touched.banK_ACCT &&
                              !formik.errors.banK_ACCT,
                          }
                        )}
                        name="banK_ACCT"
                        placeholder="Enter Organization Name"
                        value={formik.values.banK_ACCT}
                        onChange={formik.handleChange}
                        readOnly={globalVariable === "infoBankDetails"}
                      />
                    </div>
                    {formik.touched.banK_ACCT && formik.errors.banK_ACCT && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert">{formik.errors.banK_ACCT}</span>
                      </div>
                    )}
                  </div>
                  <div className="col-lg-6">
                    <div className=" d-flex align-items-center mb-1">
                      <label
                        htmlFor="banK_IFSC"
                        className="col-form-label required fw-bold fs-6 me-2"
                        style={{ minWidth: "150px" }}
                      >
                        IFSC CODE
                      </label>
                      <input
                        type="text"
                        id="banK_IFSC"
                        // className="form-control form-control-lg form-control-solid  "
                        className={clsx(
                          "form-control form-control-lg form-control-solid",
                          {
                            "is-invalid":
                              formik.touched.banK_IFSC &&
                              formik.errors.banK_IFSC,
                          },
                          {
                            "is-valid":
                              formik.touched.banK_IFSC &&
                              !formik.errors.banK_IFSC,
                          }
                        )}
                        name="banK_IFSC"
                        placeholder="Enter IFSC code"
                        value={formik.values.banK_IFSC}
                        onChange={formik.handleChange}
                        readOnly={globalVariable === "infoBankDetails"}
                      />
                    </div>

                    {formik.touched.banK_IFSC && formik.errors.banK_IFSC && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert">{formik.errors.banK_IFSC}</span>
                      </div>
                    )}
                  </div>
                </div>

                <div className="row mb-1">
                  <div className="col-lg-6">
                    <div className="d-flex align-items-center mb-1">
                      <label
                        htmlFor="emP_NAME"
                        className="col-form-label required fw-bold fs-6 me-2"
                        style={{ minWidth: "150px" }}
                      >
                        NAME ON BANK PASSBOOK
                      </label>
                      <input
                        type="text"
                        id="emP_NAME"
                        // className="form-control form-control-lg form-control-solid "
                        className={clsx(
                          "form-control form-control-lg form-control-solid",
                          {
                            "is-invalid":
                              formik.touched.emP_NAME && formik.errors.emP_NAME,
                          },
                          {
                            "is-valid":
                              formik.touched.emP_NAME &&
                              !formik.errors.emP_NAME,
                          }
                        )}
                        name="emP_NAME"
                        placeholder="Enter Name as per Bank"
                        value={formik.values.emP_NAME}
                        onChange={formik.handleChange}
                        readOnly={globalVariable === "infoBankDetails"}
                      />
                    </div>
                    {formik.touched.emP_NAME && formik.errors.emP_NAME && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert">{formik.errors.emP_NAME}</span>
                      </div>
                    )}
                  </div>
                  <div className="col-lg-6">
                    <div className=" d-flex align-items-center mb-1">
                      <label
                        htmlFor="emP_IMAGE"
                        className="col-form-label required fw-bold fs-6 me-2"
                        style={{ minWidth: "150px" }}
                      >
                        ATTACH DOCUMENT
                      </label>
                      <input
                        name="emP_IMAGE"
                        // className="form-control form-control-lg flex-grow-1"
                        className={clsx(
                          "form-control form-control-lg flex-grow-1",
                          {
                            "is-invalid":
                              formik.touched.emP_IMAGE &&
                              formik.errors.emP_IMAGE,
                          },
                          {
                            "is-valid":
                              formik.touched.emP_IMAGE &&
                              !formik.errors.emP_IMAGE,
                          }
                        )}
                        type="file"
                        id="pdf"
                        // onChange={handlePdfChange}
                        accept="application/pdf"
                        required
                        disabled={globalVariable === "infoBankDetails"}
                      />
                    </div>
                    {formik.touched.emP_IMAGE && formik.errors.emP_IMAGE && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert">{formik.errors.emP_IMAGE}</span>
                      </div>
                    )}
                  </div>
                </div>

                <div className="row mb-1">
                  <div className="col-lg-6">
                    <div className="d-flex align-items-center mb-1">
                      <label
                        htmlFor="changeReason"
                        className="col-form-label required fw-bold fs-6 me-2"
                        style={{ minWidth: "150px" }}
                      >
                        REMARKS
                      </label>
                      <input
                        type="text"
                        id="changeReason"
                        // className="form-control form-control-lg form-control-solid "
                        className={clsx(
                          "form-control form-control-lg form-control-solid ",
                          {
                            "is-invalid":
                              formik.touched.changeReason &&
                              formik.errors.changeReason,
                          },
                          {
                            "is-valid":
                              formik.touched.changeReason &&
                              !formik.errors.changeReason,
                          }
                        )}
                        name="changeReason"
                        placeholder="Enter Organization Name"
                        value={formik.values.changeReason}
                        onChange={formik.handleChange}
                        readOnly={globalVariable === "infoBankDetails"}
                      />
                    </div>
                    {formik.touched.changeReason &&
                      formik.errors.changeReason && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert">{formik.errors.changeReason}</span>
                        </div>
                      )}
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
        <div className="text-center pt-15">
          {globalVariable === "infoBankDetails" ? (
            <button
              type="reset"
              onClick={onClose}
              className="btn btn-light me-3"
            >
              Back
            </button>
          ) : (
            <button
              type="reset"
              onClick={onClose}
              className="btn btn-light me-3"
            >
              Cancel
            </button>
          )}
          {globalVariable !== "infoBankDetails" && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isBankDetailsLoading ||
                formik.isSubmitting ||
                // !formik.isValid ||
                !formik.touched ||
                (globalVariable === "addBankDetails" && searchName === "")
              }
            >
              <span className="indicator-label">Submit</span>

              {(formik.isSubmitting || isBankDetailsLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isBankDetailsLoading) && (
        <BankDetailsListLoading />
      )}
    </>
  );
};

export { BankDetailsEditModalForm };
