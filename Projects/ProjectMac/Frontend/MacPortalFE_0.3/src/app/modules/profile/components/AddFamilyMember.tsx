import { Content } from "../../../../_metronic/layout/components/content";
import FamilyMemberAdd from "./FamilyMemberAdd";
import {  } from "./LanguageDetailAdd";

export function AddFamilyMember() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <FamilyMemberAdd />
      </div>
    </Content>
  );
}
