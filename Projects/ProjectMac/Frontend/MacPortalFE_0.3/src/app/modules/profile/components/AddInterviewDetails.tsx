import { Content } from "../../../../_metronic/layout/components/content";
import { InterviewDetailsAdd } from "./InterviewDetailAdd";

export function AddInterviewDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <InterviewDetailsAdd />
      </div>
    </Content>
  );
}
