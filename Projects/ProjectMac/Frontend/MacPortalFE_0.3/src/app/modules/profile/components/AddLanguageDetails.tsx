import { Content } from "../../../../_metronic/layout/components/content";
import { LanguageDetailsAdd } from "./LanguageDetailAdd";

export function AddLanguageDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <LanguageDetailsAdd />
      </div>
    </Content>
  );
}
