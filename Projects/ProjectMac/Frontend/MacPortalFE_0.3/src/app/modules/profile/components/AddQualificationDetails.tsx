import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { toast } from "react-toastify";
import { employeeIdForView } from "../../GlobalQuery";

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
interface qualifications {
  qualification: string;
  qlF_CODE: string;
}

const AddQualificationDetails: React.FC = () => {
  const [qualifications, setQualifications] = useState<qualifications[]>([]);
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/ddl_GetQualification`)
      .then((response) => {
        setQualifications(response.data.qualifications);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    degreE_NAME: Yup.string().required("Degree is required"),
    qlF_CODE: Yup.string().required("Qualification Name is required"),
    froM_DATE: Yup.date().required("Start Date is required"),
    tO_DATE: Yup.date().required("End Date is required"),
    university: Yup.string().required("Board/University is required"),
    institute: Yup.string().required("institute is required"),
    class: Yup.string().required("Grade is required"),
    perc: Yup.number().required("Percentage is required").min(0).max(100),
    subjecT1: Yup.string().required("Subject 1 is required"),
    subjecT2: Yup.string().required("Subject 2 is required"),
    subjecT3: Yup.string().required("Subject 3 is required"),
    subjecT4: Yup.string().required("Subject 4 is required"),
    statuS_TYPE: Yup.string().required("Qualification Status is required"),
  });

  const formik = useFormik({
    initialValues: {
      emP_CODE: "",
      seQ_NO: "",
      degreE_NAME: "",
      qlF_CODE: "",
      qualName: "",
      froM_DATE: "",
      tO_DATE: "",
      university: "",
      institute: "",
      class: "",
      perc: "",
      subjecT1: "",
      subjecT2: "",
      subjecT3: "",
      subjecT4: "",
      statuS_TYPE: "",
      pasS_YEAR: "",
      exP_COMP_DATE: "",
      remarks: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values, formik) => {
      axios
        .post(`${EMPLOYEE_URL}/AddQualifiaction`, {
          ...values,
          seQ_NO: "3",
          emP_CODE: employeeIdForView,
          remarks: "remark",
          exP_COMP_DATE: values.exP_COMP_DATE || null,
        })
        .then((response) => {
          console.log("Post successful!", response.data);
          const { responseStatus, message } = response.data.data;
          if (responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
            formik.resetForm();
            navigate(-1);
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );
        });
    },
  });

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Add New Qualification</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="degreE_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DEGREE
                </label>
                <select
                  className="form-select form-select-lg form-select-solid"
                  name="degreE_NAME"
                  value={formik.values.degreE_NAME}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value="">Select Degree</option>
                  <option value="SSC">SSC</option>
                  <option value="HSC">HSC</option>
                  <option value="Diploma">Diploma</option>
                  <option value="Graduation">Graduation</option>
                  <option value="Post Graduation">Post Graduation</option>
                  <option value="Other">Other</option>
                </select>
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="qlF_CODE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  QUALIFICATION
                </label>
                <select
                  id="qlF_CODE"
                  className="form-select form-select-lg form-select-solid"
                  name="qlF_CODE"
                  onChange={formik.handleChange}
                >
                  <option value=""> </option>
                  {qualifications.map((qual) => (
                    <option value={qual.qlF_CODE}>{qual.qualification}</option>
                  ))}
                </select>
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.degreE_NAME && formik.errors.degreE_NAME ? (
                  <div className="text-danger">{formik.errors.degreE_NAME}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.qlF_CODE && formik.errors.qlF_CODE ? (
                  <div className="text-danger">{formik.errors.qlF_CODE}</div>
                ) : null}
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="university"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  BOARD / UNIVERSITY
                </label>
                <input
                  type="text"
                  id="university"
                  className="form-control form-control-lg form-control-solid"
                  name="university"
                  placeholder="Enter Board / University"
                  value={formik.values.university}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="institute"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  INSTITUTE
                </label>
                <input
                  type="text"
                  id="institute"
                  className="form-control form-control-lg form-control-solid"
                  name="institute"
                  placeholder="Enter institute"
                  value={formik.values.institute}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.university && formik.errors.university ? (
                  <div className="text-danger">{formik.errors.university}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.institute && formik.errors.institute ? (
                  <div className="text-danger">{formik.errors.institute}</div>
                ) : null}
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="froM_DATE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  START DATE
                </label>
                <input
                  type="date"
                  id="froM_DATE"
                  className="form-control form-control-lg form-control-solid"
                  name="froM_DATE"
                  value={formik.values.froM_DATE}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="tO_DATE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  END DATE
                </label>
                <input
                  type="date"
                  id="tO_DATE"
                  className="form-control form-control-lg form-control-solid"
                  name="tO_DATE"
                  value={formik.values.tO_DATE}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.froM_DATE && formik.errors.froM_DATE ? (
                  <div className="text-danger">{formik.errors.froM_DATE}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.tO_DATE && formik.errors.tO_DATE ? (
                  <div className="text-danger">{formik.errors.tO_DATE}</div>
                ) : null}
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="class"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  GRADE
                </label>
                <input
                  type="text"
                  id="class"
                  className="form-control form-control-lg form-control-solid"
                  name="class"
                  placeholder="Enter class"
                  value={formik.values.class}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="perc"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  PERCENTAGE
                </label>
                <input
                  type="number"
                  id="perc"
                  className="form-control form-control-lg form-control-solid"
                  name="perc"
                  placeholder="Enter Perc"
                  value={formik.values.perc}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  min={0}
                  max={100}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.class && formik.errors.class ? (
                  <div className="text-danger">{formik.errors.class}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.perc && formik.errors.perc ? (
                  <div className="text-danger">{formik.errors.perc}</div>
                ) : null}
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="subjecT1"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SUBJECT 1
                </label>
                <input
                  type="text"
                  id="subjecT1"
                  className="form-control form-control-lg form-control-solid"
                  name="subjecT1"
                  placeholder="Enter subject 1"
                  value={formik.values.subjecT1}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="subjecT2"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SUBJECT 2
                </label>
                <input
                  type="text"
                  id="subjecT2"
                  className="form-control form-control-lg form-control-solid"
                  name="subjecT2"
                  placeholder="Enter subject 2"
                  value={formik.values.subjecT2}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.subjecT1 && formik.errors.subjecT1 ? (
                  <div className="text-danger">{formik.errors.subjecT1}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.subjecT2 && formik.errors.subjecT2 ? (
                  <div className="text-danger">{formik.errors.subjecT2}</div>
                ) : null}
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="subjecT3"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SUBJECT 3
                </label>
                <input
                  type="text"
                  id="subjecT3"
                  className="form-control form-control-lg form-control-solid"
                  name="subjecT3"
                  placeholder="Enter subject 3"
                  value={formik.values.subjecT3}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="subjecT4"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SUBJECT 4
                </label>
                <input
                  type="text"
                  id="subjecT4"
                  className="form-control form-control-lg form-control-solid"
                  name="subjecT4"
                  placeholder="Enter subject 4"
                  value={formik.values.subjecT4}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.subjecT3 && formik.errors.subjecT3 ? (
                  <div className="text-danger">{formik.errors.subjecT3}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.subjecT4 && formik.errors.subjecT4 ? (
                  <div className="text-danger">{formik.errors.subjecT4}</div>
                ) : null}
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="statuS_TYPE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  STATUS
                </label>
                <select
                  name="statuS_TYPE"
                  className="form-select form-select-lg form-select-solid flex-grow-1 me-2"
                  value={formik.values?.statuS_TYPE}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value=""></option>
                  <option value="C">COMPLETED</option>
                  <option value="P">PERSUING</option>
                </select>
              </div>
              {formik.values?.statuS_TYPE.toLowerCase() !== "p" ? (
                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="pasS_YEAR"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PASSING YEAR
                  </label>
                  <input
                    type="text"
                    id="pasS_YEAR"
                    className="form-control form-control-lg form-control-solid"
                    name="pasS_YEAR"
                    placeholder="Enter passing year"
                    value={formik.values.pasS_YEAR}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    required
                    min={1974}
                    max={3000}
                  />
                </div>
              ) : (
                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="exP_COMP_DATE"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    EXPECTED COMPLETION DATE
                  </label>
                  <input
                    type="date"
                    id="exP_COMP_DATE"
                    className="form-control form-control-lg form-control-solid "
                    name="exP_COMP_DATE"
                    placeholder="Enter Expected Completion Date"
                    value={formik.values?.exP_COMP_DATE} //.toString()} //.split("T")[0]}
                    onChange={formik.handleChange}
                    required
                  />
                </div>
              )}
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.statuS_TYPE && formik.errors.statuS_TYPE ? (
                  <div className="text-danger">{formik.errors.statuS_TYPE}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.pasS_YEAR && formik.errors.pasS_YEAR ? (
                  <div className="text-danger">{formik.errors.pasS_YEAR}</div>
                ) : null}
              </div>
            </div>
          </div>

          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                //formik.isSubmitting ||
                !formik.isValid
              }
            >
              <span className="indicator-label">Save Changes</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AddQualificationDetails;
