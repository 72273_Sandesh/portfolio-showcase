import { Content } from '../../../../_metronic/layout/components/content'
import BasicDetailsEdit from './BasicDetailsEdit'
import CandidateDetailsEdit from './CandidateDetailsEdit'


export function EditCandidateDetails() {
  return (
    <Content>
        <div className='mb-5 mb-xl-8'>
        <CandidateDetailsEdit />
        </div>
        
   </Content>
  )
}