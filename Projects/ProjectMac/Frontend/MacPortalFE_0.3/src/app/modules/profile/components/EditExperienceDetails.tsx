import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView, fMIdForUpdate } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
type states = {
  statE_CODE: string;
  descr: string;
  counT_CODE: string;
};

const EditExperienceDetails: React.FC = () => {
  const navigate = useNavigate();
  const [Country, setCountry] = useState<states[]>([]);
  const [State, setState] = useState<states[]>([]);
  const [Currency, setCurrency] = useState<states[]>([]);
  const {currentUser} = useAuth();

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPEXP`)
      .then((response) => {
        formik.setValues({
          emP_CODE: response.data[fMIdForUpdate].emP_CODE,
          organisation: response.data[fMIdForUpdate].organisation,
          lasT_DESIGNATION: response.data[fMIdForUpdate].lasT_DESIGNATION,
          froM_DATE: response.data[fMIdForUpdate].froM_DATE,
          tO_DATE: response.data[fMIdForUpdate].tO_DATE,
          grosS_AMT: response.data[fMIdForUpdate].grosS_AMT,
          curR_CODE: response.data[fMIdForUpdate].curR_CODE,
          chG_USER: response.data[fMIdForUpdate].chG_USER,
          chG_DATE: response.data[fMIdForUpdate].chG_DATE,
          chG_TERM: response.data[fMIdForUpdate].chG_TERM,
          department: response.data[fMIdForUpdate].department,
          country: response.data[fMIdForUpdate].country,
          state: response.data[fMIdForUpdate].state,
          seQ_NO: response.data[fMIdForUpdate].seQ_NO,
          worK_LOCATION: response.data[fMIdForUpdate].worK_LOCATION,
          seP_REASON: response.data[fMIdForUpdate].seP_REASON,
          curR_DESCR: response.data[fMIdForUpdate].curR_DESCR,
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });

    axios
      .get(`${EMPLOYEE_URL}/ddl_GetCountry`)
      .then((response) => {
        setCountry(response.data.country);
      })
      .catch((error) => {
        console.error("Error fetching Country details:", error);
      });

    axios
      .get(`${EMPLOYEE_URL}/ddl_GetStates`)
      .then((response) => {
        setState(response.data.states);
      })
      .catch((error) => {
        console.error("Error fetching States details:", error);
      });

    axios
      .get(`${EMPLOYEE_URL}/ddl_GetCurrency`)
      .then((response) => {
        setCurrency(response.data.currency);
      })
      .catch((error) => {
        console.error("Error fetching States details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    organisation: Yup.string().required("Organization Name is required"),
    lasT_DESIGNATION: Yup.string().required("Designation is required"),
    froM_DATE: Yup.string().required("Start Date is required"),
    tO_DATE: Yup.string().required("End Date is required"),
    grosS_AMT: Yup.string().required("Gross Salary is required"),
    department: Yup.string().required("Department is required"),
    worK_LOCATION: Yup.string().required("Work Location is required"),
    seP_REASON: Yup.string().required("Reason of Separation is required"),
    curR_CODE: Yup.string().required("Currency Code is required"),
    country: Yup.string().required("Country name is required"),
    state: Yup.string().required("State name is required"),
  });

  const formik = useFormik({
    initialValues: {
      emP_CODE: "",
      organisation: "",
      lasT_DESIGNATION: "",
      froM_DATE: "",
      tO_DATE: "",
      grosS_AMT: "",
      curR_CODE: "",
      curR_DESCR: "",
      chG_USER: "",
      chG_DATE: "",
      chG_TERM: "",
      department: "",
      country: "",
      state: "",
      seQ_NO: "",
      worK_LOCATION: "",
      seP_REASON: "",
    },
    validationSchema: validationSchema,

    onSubmit: (values, formik) => {
      console.log("Submitting form data:", values);

      axios
        .put(
          `${EMPLOYEE_URL}/UpdateEmpExperience/${employeeIdForView}/${values.froM_DATE}`,
          {
            ...values,
            designation: values.lasT_DESIGNATION,
            chG_USER: currentUser?.id,
            chG_DATE: new Date(),
            chG_TERM: "TERM",
          }
        )
        .then((response) => {
          console.log("Post successful!", response.data);
          const { responseStatus, message } = response.data.data;
          if (responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(message)
                      ? message.join("\n")
                      : message}
                  </h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(message)
                      ? message.join("\n")
                      : message}
                  </h6>
                </p>
              </div>
            );
          }

          formik.resetForm();
          navigate(-1);
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );
        });
    },
  });

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Experience Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="organisation"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  ORGANIZATION
                </label>
                <input
                  type="text"
                  id="organisation"
                  className="form-control form-control-lg form-control-solid "
                  name="organisation"
                  placeholder="Enter Organization Name"
                  value={formik.values?.organisation}
                  onChange={formik.handleChange}
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="lasT_DESIGNATION"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DESIGNATION
                </label>
                <input
                  type="text"
                  id="lasT_DESIGNATION"
                  className="form-control form-control-lg form-control-solid  "
                  name="lasT_DESIGNATION"
                  placeholder="Enter Designation"
                  value={formik.values?.lasT_DESIGNATION}
                  onChange={formik.handleChange}
                />
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.organisation && formik.errors.organisation ? (
                    <div className="text-danger">
                      {formik.errors.organisation}
                    </div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.lasT_DESIGNATION &&
                  formik.errors.lasT_DESIGNATION ? (
                    <div className="text-danger">
                      {formik.errors.lasT_DESIGNATION}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="department"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DEPARTMENT
                </label>
                <input
                  type="text"
                  id="department"
                  className="form-control form-control-lg form-control-solid "
                  name="department"
                  placeholder="Enter Department"
                  value={formik.values?.department}
                  onChange={formik.handleChange}
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="worK_LOCATION"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  WORK LOCATION
                </label>
                <input
                  type="text"
                  id="worK_LOCATION"
                  className="form-control form-control-lg form-control-solid "
                  name="worK_LOCATION"
                  placeholder="Enter Work Location"
                  value={formik.values?.worK_LOCATION}
                  onChange={formik.handleChange}
                />
              </div>

              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.department && formik.errors.department ? (
                    <div className="text-danger">
                      {formik.errors.department}
                    </div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.worK_LOCATION &&
                  formik.errors.worK_LOCATION ? (
                    <div className="text-danger">
                      {formik.errors.worK_LOCATION}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="froM_DATE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  START DATE
                </label>
                <input
                  type="date"
                  id="froM_DATE"
                  className="form-control form-control-lg form-control-solid"
                  name="froM_DATE"
                  placeholder="Enter Start Date"
                  value={
                    formik.values?.froM_DATE
                      ? formik.values.froM_DATE.split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="tO_DATE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  END DATE
                </label>
                <input
                  type="date"
                  id="tO_DATE"
                  className="form-control form-control-lg form-control-solid"
                  name="tO_DATE"
                  placeholder="Enter End Date"
                  value={
                    formik.values?.tO_DATE
                      ? formik.values.tO_DATE.split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                />
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.froM_DATE && formik.errors.froM_DATE ? (
                    <div className="text-danger">{formik.errors.froM_DATE}</div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.tO_DATE && formik.errors.tO_DATE ? (
                    <div className="text-danger">{formik.errors.tO_DATE}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="grosS_AMT"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SALARY
                </label>

                <select
                  id="curR_CODE"
                  className="form-select form-select-lg form-select-solid me-2"
                  name="curR_CODE"
                  onChange={formik.handleChange}
                >
                  <option value={formik.values?.curR_CODE}>
                    {formik.values?.curR_DESCR}
                  </option>

                  {Currency.map((state) => (
                    <option value={state.descr}>{state.descr}</option>
                  ))}
                </select>

                <input
                  name="grosS_AMT"
                  type="number"
                  className="form-control form-control-lg form-control-solid flex-grow-1"
                  placeholder="Enter Salary"
                  value={formik.values?.grosS_AMT}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="seP_REASON"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SEPARATION REASON
                </label>
                <input
                  type="text"
                  id="seP_REASON"
                  className="form-control form-control-lg form-control-solid "
                  name="seP_REASON"
                  placeholder="Enter Separation Reason"
                  value={formik.values?.seP_REASON}
                  onChange={formik.handleChange}
                />
              </div>

              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.grosS_AMT && formik.errors.grosS_AMT ? (
                    <div className="text-danger">{formik.errors.grosS_AMT}</div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.seP_REASON && formik.errors.seP_REASON ? (
                    <div className="text-danger">
                      {formik.errors.seP_REASON}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="country"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  COUNTRY
                </label>
                <select
                  id="country"
                  className="form-select form-select-lg form-select-solid"
                  name="country"
                  onChange={formik.handleChange}
                >
                  <option value={formik.values?.country}>
                    {formik.values?.country}
                  </option>

                  {Country.map((state) => (
                    <option value={state.counT_CODE}>{state.descr}</option>
                  ))}
                </select>
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="state"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  STATE
                </label>
                <select
                  id="state"
                  className="form-select form-select-lg form-select-solid"
                  name="state"
                  onChange={formik.handleChange}
                >
                  <option value={formik.values?.state}>
                    {formik.values?.state}
                  </option>

                  {State.map((state) => (
                    <option key={state.statE_CODE} value={state.statE_CODE}>
                      {state.descr}
                    </option>
                  ))}
                </select>
              </div>

              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.country && formik.errors.country ? (
                    <div className="text-danger">{formik.errors.country}</div>
                  ) : null}
                </div>
                <div className="col-lg-6">
                  {formik.touched.state && formik.errors.state ? (
                    <div className="text-danger">{formik.errors.state}</div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export { EditExperienceDetails };
