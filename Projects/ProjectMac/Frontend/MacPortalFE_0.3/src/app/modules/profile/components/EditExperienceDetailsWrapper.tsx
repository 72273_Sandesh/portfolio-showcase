import { Content } from "../../../../_metronic/layout/components/content";
import { EditExperienceDetails } from "./EditExperienceDetails";

export function EditExperienceDetailsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <EditExperienceDetails />
      </div>
    </Content>
  );
}
