import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const EditPFDeatils: React.FC = () => {
  const navigate = useNavigate();
  const [remainingPerc, setRemainingPerc] = useState(100);
  const {currentUser} = useAuth();

  const validationSchema = Yup.object().shape({
    membeR_NAME: Yup.string().required("Member name is required"),
    pF_NOMINEE: Yup.string().required("PF nominee is required"),
    pF_PERC: Yup.string().required("PF percentage is required"),
  });

  const formik = useFormik({
    initialValues: {
      familyMembers: [
        {
          seQ_NO: "",
          membeR_NAME: "",
          pF_NOMINEE: "",
          pF_PERC: "",
        },
      ],
    },
    validationSchema: Yup.object().shape({
      familyMembers: Yup.array().of(validationSchema),
    }),
    onSubmit: (values, formik) => {
      const payload = values.familyMembers.map((member) => ({
        chG_USER: currentUser?.id,
        chG_DATE: new Date(),
        chG_TERM: "TERM",
        emP_CODE:employeeIdForView,
        seQ_NO: member.seQ_NO,
        membeR_NAME: member.membeR_NAME,
        pF_NOMINEE: member.pF_NOMINEE,
        pF_PERC: member.pF_PERC,
      }));
      console.log("submitting pf data...");
      axios
        .post(`${EMPLOYEE_URL}/updateFamilyNominee`, payload)
        .then((response) => {
          const {responseStatus,message} = response.data.data;
          if (responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(message)
                      ? message.join("\n")
                      : message}
                  </h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(message)
                      ? message.join("\n")
                      : message}
                  </h6>
                </p>
              </div>
            );
            navigate(-1);
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );

          // Reset the form even if there's an error
          formik.resetForm();
        });
    },
  });

  const handleOnChange = (index: number, value: string) => {
    const updatedFamilyMembers = [...formik.values.familyMembers];
    updatedFamilyMembers[index].pF_PERC = value;

    const totalAssigned = updatedFamilyMembers.reduce((sum, member) => {
      return sum + (parseInt(member.pF_PERC) || 0);
    }, 0);

    setRemainingPerc(100 - totalAssigned);
    formik.setFieldValue("familyMembers", updatedFamilyMembers);
  };

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMP_FAMDET`)
      .then((response) => {
        formik.setValues({
          familyMembers: response.data,
        });
      })
      .catch((error) => {
        console.error("Error fetching familyMember details:", error);
      });
  }, []);

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit PF Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {formik.values.familyMembers.map((familyMember, index) => (
              <div key={index}>
                <div className="row mb-1">
                  <div className="col-lg-4 d-flex align-items-center mb-1">
                    <label
                      htmlFor={`familyMembers[${index}].membeR_NAME`}
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      FAMILY MEMBER :
                    </label>
                    <input
                      type="text"
                      id={`familyMembers[${index}].membeR_NAME`}
                      className="form-control form-control-lg form-control-solid bg-light-danger"
                      name={`familyMembers[${index}].membeR_NAME`}
                      placeholder="Enter Full Name"
                      value={familyMember.membeR_NAME?.toUpperCase()}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      readOnly
                      style={{ cursor: "not-allowed" }}
                    />
                  </div>
                  <div className="col-lg-4 d-flex align-items-center mb-1">
                    <label
                      htmlFor={`familyMembers[${index}].pF_NOMINEE`}
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      PF NOMINEE :
                    </label>
                    <select
                      className="form-select form-select-lg form-select-solid"
                      name={`familyMembers[${index}].pF_NOMINEE`}
                      value={familyMember.pF_NOMINEE}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    >
                      <option value="Y">YES</option>
                      <option value="N">NO</option>
                    </select>
                  </div>
                  <div className="col-lg-4 d-flex align-items-center mb-1">
                    <label
                      htmlFor={`familyMembers[${index}].pF_PERC`}
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      PF PERCENTAGE :
                    </label>
                    <input
                      type="number"
                      id={`familyMembers[${index}].pF_PERC`}
                      className="form-control form-control-lg form-control-solid"
                      name={`familyMembers[${index}].pF_PERC`}
                      placeholder="Enter PF Percentage"
                      value={
                        familyMember.pF_NOMINEE === "N"
                          ? 0
                          : familyMember.pF_PERC
                      }
                      // onChange={(e)=>handleOnChange(e.target.value)}
                      onChange={(e) => handleOnChange(index, e.target.value)}
                      onBlur={formik.handleBlur}
                      readOnly={familyMember.pF_NOMINEE === "N"}
                      style={
                        familyMember.pF_NOMINEE === "N"
                          ? { cursor: "not-allowed" }
                          : {}
                      }
                      min={0}
                      max={familyMember.pF_PERC + remainingPerc}
                    />
                  </div>
                </div>

                {index < formik.values.familyMembers.length - 1 && <hr />}
              </div>
            ))}
          </div>
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || remainingPerc !== 0}
            >
              {formik.isSubmitting ? "Please wait..." : "Save Changes"}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default EditPFDeatils;
