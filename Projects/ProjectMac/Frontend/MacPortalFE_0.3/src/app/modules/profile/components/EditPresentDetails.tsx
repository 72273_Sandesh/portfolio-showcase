import { Content } from "../../../../_metronic/layout/components/content";
import PresentDetailsEdit from "./PresentDetailsEdit";

export function EditPresentDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <PresentDetailsEdit />
      </div>
    </Content>
  );
}
