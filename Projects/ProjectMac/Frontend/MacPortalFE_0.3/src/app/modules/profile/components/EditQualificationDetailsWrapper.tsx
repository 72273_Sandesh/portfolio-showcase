import { Content } from "../../../../_metronic/layout/components/content";
import EditQualificationDetails from "./EditQualificationDetails";

export function EditQualificationDetailsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <EditQualificationDetails/>
      </div>
    </Content>
  );
}
