import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const KYCDetailsEdit: React.FC = () => {
  const [employee, setEmployee] = useState<any>({});
  const navigate = useNavigate();
  const [pdf, setPdf] = useState<File | null>(null);
  const { currentUser } = useAuth();

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setEmployee(response.data[0]);
        formik.setValues({
          aadhaR_EMP_NAME: response.data[0].aadhaR_EMP_NAME.toUpperCase() || "",
          aadhaaR_NO: response.data[0].aadhaaR_NO || "",
          paN_EMP_NAME: response.data[0].paN_EMP_NAME || "",
          electioN_CARD_NO: response.data[0].electioN_CARD_NO || "",
          driV_LIC_NO: response.data[0].driV_LIC_NO || "",
          passporT_NO: response.data[0].passporT_NO || "",
          passporT_ISS_PLACE: response.data[0].passporT_ISS_PLACE || "",
          passporT_ISS_DATE: response.data[0].passporT_ISS_DATE || "",
          passporT_EXP_DATE: response.data[0].passporT_EXP_DATE || "",
          paN_NO: response.data[0].paN_NO || "",
          pdf: null,
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    aadhaR_EMP_NAME: Yup.string().required("Name on aadhaar is required"),

    paN_EMP_NAME: Yup.string().required("Name on PAN is required"),

    passporT_ISS_DATE: Yup.string().required("Passport Issue Date is required"),
    passporT_EXP_DATE: Yup.string().required(
      "Passport Expiration Date is required"
    ),
  });

  const formik = useFormik({
    initialValues: {
      aadhaR_EMP_NAME: "",
      aadhaaR_NO: "",
      paN_EMP_NAME: "",
      electioN_CARD_NO: "",
      driV_LIC_NO: "",
      passporT_NO: "",
      passporT_ISS_PLACE: "",
      passporT_ISS_DATE: "",
      passporT_EXP_DATE: "",
      paN_NO: "",
      pdf: null,
    },
    validationSchema: validationSchema,
    onSubmit: (values, formik) => {
      console.log("Submitting form data:", values);
      const formData = new FormData();

      Object.keys(values).forEach((key) => {
        if (key === "pdf" && pdf) {
          formData.append(key, pdf);
        } else {
          formData.append(key, values[key as keyof typeof values] as string);
        }
      });

      axios
        .put(`${EMPLOYEE_URL}/UpdateEmployeeKYCDetails/${employeeIdForView}`, {
          ...values,
          chG_DATE: new Date(),
          chG_USER: currentUser?.id, //const { currentUser } = useAuth();
          chG_TERM: "TERM",
          // pdf: pdf,
        })
        .then((response) => {
          console.log("Basic Details Posted successfully!", response.data);
          const { responseStatus, message } = response.data.data;
          if (responseStatus === "E") {
            formik.setSubmitting(false);
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );

            formik.resetForm();
            navigate(-1);
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );

          formik.setSubmitting(false);
        });
    },
  });

  const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (file) {
      setPdf(file);
    }
  };

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit KYC Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              {/* <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="aadhaR_EMP_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Name on Aadhaar:
                </label>
                <input
                  type="text"
                  id="aadhaR_EMP_NAME"
                  className="form-control form-control-lg form-control-solid"
                  name="aadhaR_EMP_NAME"
                  placeholder="Enter name on AADHAAR"
                  value={formik.values?.aadhaR_EMP_NAME}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div> */}
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="aadhaR_EMP_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  NAME ON ADHAR :
                </label>
                <input
                  name="aadhaR_EMP_NAME"
                  type="text"
                  className="form-control form-control-lg form-control-solid me-2"
                  placeholder="Enter Name on Aadhaar"
                  value={formik.values?.aadhaR_EMP_NAME}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                <input
                  name="birtH_DATE"
                  className="form-control form-control-lg flex-grow-1"
                  type="file"
                  id="pdf"
                  onChange={handlePdfChange}
                  accept="application/pdf"
                  required
                />
                {formik.touched.pdf && formik.errors.pdf ? (
                  <div className="text-danger">{formik.errors.pdf}</div>
                ) : null}
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  ADHAR NO :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="aadhaaR_NO"
                  placeholder="Enter Aadhaar No"
                  value={formik.values.aadhaaR_NO?.toUpperCase()}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.aadhaR_EMP_NAME &&
                formik.errors.aadhaR_EMP_NAME ? (
                  <div className="text-danger">
                    {formik.errors.aadhaR_EMP_NAME}
                  </div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.aadhaaR_NO && formik.errors.aadhaaR_NO ? (
                  <div className="text-danger">{formik.errors.aadhaaR_NO}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              {/* <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="paN_EMP_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Name on PAN:
                </label>
                <input
                  type="text"
                  id="paN_EMP_NAME"
                  className="form-control form-control-lg form-control-solid"
                  name="paN_EMP_NAME"
                  placeholder="Enter name on PAN"
                  value={formik.values?.paN_EMP_NAME}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div> */}
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="paN_EMP_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  NAME ON PAN :
                </label>
                <input
                  name="paN_EMP_NAME"
                  type="text"
                  className="form-control form-control-lg form-control-solid me-2"
                  placeholder="Enter Name on PAN"
                  value={formik.values?.paN_EMP_NAME}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                <input
                  name="birtH_DATE"
                  className="form-control form-control-lg flex-grow-1"
                  type="file"
                  id="pdf"
                  onChange={handlePdfChange}
                  accept="application/pdf"
                  required
                />
                {formik.touched.pdf && formik.errors.pdf ? (
                  <div className="text-danger">{formik.errors.pdf}</div>
                ) : null}
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  PAN NO. :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="paN_NO"
                  placeholder="Enter PAN No."
                  value={formik.values?.paN_NO}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.paN_EMP_NAME && formik.errors.paN_EMP_NAME ? (
                  <div className="text-danger">
                    {formik.errors.paN_EMP_NAME}
                  </div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.paN_NO && formik.errors.paN_NO ? (
                  <div className="text-danger">{formik.errors.paN_NO}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="electioN_CARD_NO"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  VOTER CARD NO. :
                </label>
                <input
                  type="text"
                  id="electioN_CARD_NO"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="electioN_CARD_NO"
                  placeholder="Enter voter card No."
                  value={formik.values?.electioN_CARD_NO}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  DRIVING LICENCE NO. :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="driV_LIC_NO"
                  placeholder="Enter Driving License No."
                  value={formik.values?.driV_LIC_NO?.toUpperCase()}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.electioN_CARD_NO &&
                formik.errors.electioN_CARD_NO ? (
                  <div className="text-danger">
                    {formik.errors.electioN_CARD_NO}
                  </div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.driV_LIC_NO && formik.errors.driV_LIC_NO ? (
                  <div className="text-danger">{formik.errors.driV_LIC_NO}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="passporT_NO"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  PASSPORT NO. :
                </label>
                <input
                  type="text"
                  id="passporT_NO"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="passporT_NO"
                  placeholder="Enter passport No."
                  value={formik.values?.passporT_NO}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  PASSPORT ISSUE PLACE :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="passporT_ISS_PLACE"
                  placeholder="Enter passport issue place"
                  value={formik.values?.passporT_ISS_PLACE?.toUpperCase()}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.passporT_NO && formik.errors.passporT_NO ? (
                  <div className="text-danger">{formik.errors.passporT_NO}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.passporT_ISS_PLACE &&
                formik.errors.passporT_ISS_PLACE ? (
                  <div className="text-danger">
                    {formik.errors.passporT_ISS_PLACE}
                  </div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="passporT_ISS_DATE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  PASSPORT ISSUE DATE :
                </label>
                <input
                  type="date"
                  id="passporT_ISS_DATE"
                  className="form-control form-control-lg form-control-solid"
                  name="passporT_ISS_DATE"
                  placeholder="Enter passport issue date"
                  value={
                    formik.values?.passporT_ISS_DATE
                      ? new Date(formik.values?.passporT_ISS_DATE)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  PASSPORT EXPIRATION DATE :
                </label>
                <input
                  type="date"
                  className="form-control form-control-lg form-control-solid"
                  name="passporT_EXP_DATE"
                  placeholder="Enter passport expiration date"
                  value={
                    formik.values?.passporT_EXP_DATE
                      ? new Date(formik.values?.passporT_EXP_DATE)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.passporT_ISS_DATE &&
                formik.errors.passporT_ISS_DATE ? (
                  <div className="text-danger">
                    {formik.errors.passporT_ISS_DATE}
                  </div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.passporT_EXP_DATE &&
                formik.errors.passporT_EXP_DATE ? (
                  <div className="text-danger">
                    {formik.errors.passporT_EXP_DATE}
                  </div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit is HERE */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default KYCDetailsEdit;
