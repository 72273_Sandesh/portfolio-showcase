import { Content } from "../../../../_metronic/layout/components/content";
import { KYC } from "./KYC";
import { PersonalDetails } from "./PersonalDetails";


export function KYCWrapper() {
  return (
    <Content>
      <>
        <div className="row">

        <div>
            <PersonalDetails />
          </div>

          <div>
            <KYC />
          </div>
          </div>
      </>
    </Content>
  );
}
