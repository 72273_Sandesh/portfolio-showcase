import { Content } from "../../../../_metronic/layout/components/content";
import { BasicDetails } from "./BasicDetails";
import { PersonalDetails } from "./PersonalDetails";

export function Overview() {
  return (
    <Content>
      <>
        <div className="row">
          <div>
            <PersonalDetails />
          </div>

          <div>
            <BasicDetails />
          </div>
          {/* 
          <div>
            <ContactDetails />
          </div>

          <div>
            <WorkDetails />
          </div> */}

          {/* <div>
            <LanguageDetailsWidget />
          </div> */}

          {/* <div>
            <KYC />
          </div> */}
        </div>
      </>
    </Content>
  );
}
