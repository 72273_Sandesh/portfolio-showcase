import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

interface Response {
  responseStatus: string;
  message: string;
}

const PersonalDetailsEdit: React.FC = () => {
  const [employee, setEmployee] = useState<any>({});
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setEmployee(response.data[0]);
        // Update formik's initial values after fetching employee data
        formik.setValues({
          sitE_NAME: response.data[0].sitE_NAME.toUpperCase() || "",
          depT_NAME: response.data[0].depT_NAME.toUpperCase() || "",
          designation: response.data[0].designation.toUpperCase() || "",
          section: response.data[0].section.toUpperCase() || "",
          gradE_NAME: response.data[0].gradE_NAME.toUpperCase() || "",
          cadre: response.data[0].cadre.toUpperCase() || "",
          joininG_CTC: response.data[0].joininG_CTC || "",
          curR_CTC: response.data[0].curR_CTC || "",
          maC_EXP: response.data[0].maC_EXP || "",
          toT_EXP: response.data[0].toT_EXP || "",
          emaiL_ID_OFF: response.data[0].emaiL_ID_OFF.toUpperCase() || "",
          mobilE_NO_OFF: response.data[0].mobilE_NO_OFF || "",
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    sitE_NAME: Yup.string().required("Location is required"),
    depT_NAME: Yup.string().required("Department is required"),
    designation: Yup.string().required("Designation is required"),
    section: Yup.string().required("Section is required"),
    gradE_NAME: Yup.string().required("Grade is required"),
    // cadre: Yup.string().required("Cadre is required"),
    // joininG_CTC: Yup.number()
    //   .required("Joining CTC is required")
    //   .positive("Joining CTC must be positive") // Optional: Ensure positive value
    //   .min(0, "Joining CTC can't be negative"), // Optional: Ensure non-negative value
    curR_CTC: Yup.number()
      .required("Current CTC is required")
      .positive("Current CTC must be positive") // Optional: Ensure positive value
      .min(0, "Current CTC can't be negative"), // Optional: Ensure non-negative value
    maC_EXP: Yup.number()
      .required("Macleods Experience is required")
      .positive("Macleods Experience must be positive"), // Optional: Ensure positive value
    toT_EXP: Yup.number()
      .required("Total Experience is required")
      .positive("Total Experience must be positive"), // Optional: Ensure positive value
    emaiL_ID_OFF: Yup.string()
      .email("Invalid email format")
      .required("Official Email is required"),
    mobilE_NO_OFF: Yup.string(),
    //   .matches(/^[0-9]+$/, "Mobile number must contain only digits")
    //   .min(10, "Mobile number must be at least 10 digits")
    //   .max(15, "Mobile number can't exceed 15 digits")
    //   .required("Official Mobile is required"),
  });

  const formik = useFormik({
    initialValues: {
      sitE_NAME: "", // Initially empty
      depT_NAME: "",
      designation: "",
      section: "",
      gradE_NAME: "",
      cadre: "",
      joininG_CTC: "",
      curR_CTC: "",
      maC_EXP: "",
      toT_EXP: "",
      emaiL_ID_OFF: "",
      mobilE_NO_OFF: "",
    },
    validationSchema: validationSchema,
    // onSubmit: (values) => {
    //   console.log('Submitting form data:', values);
    //   // Implement your submit logic here, e.g., send updated data to the server

    //   setEmployee({ sitE_NAME: '', depT_NAME: '' ,designation:'',section:""});
    //   formik.resetForm();// Upon successful submission, clear the form
    // },

    onSubmit: (values, formik) => {
      console.log("Submitting form data:", values);

      axios
        .post<Response>("your_backend_endpoint", values)
        .then((response) => {
          console.log("Official Details Posted successfully!", response.data);
          if (response.data.responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(response.data.message)
                      ? response.data.message.join("\n")
                      : response.data.message}
                  </h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(response.data.message)
                      ? response.data.message.join("\n")
                      : response.data.message}
                  </h6>
                </p>
              </div>
            );

            // Clear employee data and reset form
            setEmployee({
              sitE_NAME: "",
              depT_NAME: "",
              designation: "",
              section: "",
              gradE_NAME: "",
              cadre: "",
              joininG_CTC: "",
              curR_CTC: "",
              maC_EXP: "",
              toT_EXP: "",
              emaiL_ID_OFF: "",
              mobilE_NO_OFF: "",
            });
            formik.resetForm();
            navigate(-1); // Redirect to previous page or desired location
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );
        });
    },
  });

  const handleClose = () => {
    navigate(-1);
    // navigate("/crafted/pages/profile/overview");
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-warning card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Official Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sitE_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Location :
                </label>
                <input
                  type="text"
                  id="sitE_NAME"
                  className="form-control form-control-lg form-control-solid"
                  name="sitE_NAME"
                  placeholder="Enter location name"
                  value={formik.values?.sitE_NAME}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  Department :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="depT_NAME"
                  placeholder="Enter department name"
                  value={formik.values.depT_NAME?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.sitE_NAME && formik.errors.sitE_NAME ? (
                  <div className="text-danger">{formik.errors.sitE_NAME}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.depT_NAME && formik.errors.depT_NAME ? (
                  <div className="text-danger">{formik.errors.depT_NAME}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="section"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Section :
                </label>
                <input
                  type="text"
                  id="section"
                  className="form-control form-control-lg form-control-solid"
                  name="section"
                  placeholder="Enter section name"
                  value={formik.values?.section}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  Designation :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="designation"
                  placeholder="Enter designation name"
                  value={formik.values.designation?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.section && formik.errors.section ? (
                  <div className="text-danger">{formik.errors.section}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.designation && formik.errors.designation ? (
                  <div className="text-danger">{formik.errors.designation}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="gradE_NAME"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Grade :
                </label>
                <input
                  type="text"
                  id="gradE_NAME"
                  className="form-control form-control-lg form-control-solid"
                  name="gradE_NAME"
                  placeholder="Enter grade name"
                  value={formik.values?.gradE_NAME}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  Cadre/Level :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="cadre"
                  placeholder="Enter cadre name"
                  value={formik.values.cadre?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.gradE_NAME && formik.errors.gradE_NAME ? (
                  <div className="text-danger">{formik.errors.gradE_NAME}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.cadre && formik.errors.cadre ? (
                  <div className="text-danger">{formik.errors.cadre}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="joininG_CTC"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Joining CTC :
                </label>
                <input
                  type="text"
                  id="joininG_CTC"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="joininG_CTC"
                  placeholder="Enter joining CTC"
                  value={formik.values?.joininG_CTC}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  Current CTC :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="curR_CTC"
                  placeholder="Enter current CTC"
                  value={formik.values.curR_CTC}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.joininG_CTC && formik.errors.joininG_CTC ? (
                  <div className="text-danger">{formik.errors.joininG_CTC}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.curR_CTC && formik.errors.curR_CTC ? (
                  <div className="text-danger">{formik.errors.curR_CTC}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="maC_EXP"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Total Macleods Experience :
                </label>
                <input
                  type="number"
                  id="maC_EXP"
                  className="form-control form-control-lg form-control-solid"
                  name="maC_EXP"
                  placeholder="Enter macleods experiance"
                  value={formik.values?.maC_EXP}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  Total Experience :
                </label>
                <input
                  type="number"
                  className="form-control form-control-lg form-control-solid"
                  name="toT_EXP"
                  placeholder="Enter total experiance"
                  value={formik.values.toT_EXP}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.maC_EXP && formik.errors.maC_EXP ? (
                  <div className="text-danger">{formik.errors.maC_EXP}</div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.toT_EXP && formik.errors.toT_EXP ? (
                  <div className="text-danger">{formik.errors.toT_EXP}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="emaiL_ID_OFF"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Official Email :
                </label>
                <input
                  type="text"
                  id="emaiL_ID_OFF"
                  className="form-control form-control-lg form-control-solid"
                  name="emaiL_ID_OFF"
                  placeholder="Enter official email"
                  value={formik.values?.emaiL_ID_OFF}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  Official Mobile :
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="mobilE_NO_OFF"
                  placeholder="Enter official mobile"
                  value={formik.values.mobilE_NO_OFF?.toUpperCase()}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.emaiL_ID_OFF && formik.errors.emaiL_ID_OFF ? (
                  <div className="text-danger">
                    {formik.errors.emaiL_ID_OFF}
                  </div>
                ) : null}
              </div>

              <div className="col-lg-6">
                {formik.touched.mobilE_NO_OFF && formik.errors.mobilE_NO_OFF ? (
                  <div className="text-danger">
                    {formik.errors.mobilE_NO_OFF}
                  </div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit is HERE */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default PersonalDetailsEdit;
