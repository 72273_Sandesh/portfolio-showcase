
import { Content } from "../../../../_metronic/layout/components/content";
import { PreJoiningDocs } from "../../../../_metronic/partials/widgets/tables/PreJoiningDocs";

export function PreJoiningDocsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
      <PreJoiningDocs className='mb-5 mb-xl-8' />
      </div>
    </Content>
  );
}
