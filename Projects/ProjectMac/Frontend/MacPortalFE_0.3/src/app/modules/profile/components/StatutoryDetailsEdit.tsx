import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

interface Response {
  responseStatus:string;
  message:string;
}

const StatutoryDetailsEdit: React.FC = () => {
  const [employee, setEmployee] = useState<any>({});
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setEmployee(response.data[0]);
        // Update formik's initial values after fetching employee data
        formik.setValues({
          //   induction: response.data[0].induction || "",
          pF_UN_NO: response.data[0].pF_UN_NO || "",
          pF_NO: response.data[0].pF_NO || "",
          fpF_NO: response.data[0].fpF_NO || "",
          esiC_NO: response.data[0].esiC_NO || "",
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    // pF_UN_NO: Yup.string().required("PF UN No required"),
    // pF_NO: Yup.string().required("PF No required"),
    // fpF_NO: Yup.string().required("FPF No required"),
    // esiC_NO: Yup.string().required("ESIC No required"),
  });

  const formik = useFormik({
    initialValues: {
      pF_UN_NO: "",
      pF_NO: "",
      fpF_NO: "",
      esiC_NO: "",
    },
    validationSchema: validationSchema,
    // onSubmit: (values) => {
    //   console.log('Submitting form data:', values);
    //   // Implement your submit logic here, e.g., send updated data to the server

    //   setEmployee({ sitE_NAME: '', depT_NAME: '' ,designation:'',section:""});
    //   formik.resetForm();// Upon successful submission, clear the form
    // },

    onSubmit: (values) => {
      console.log("Submitting form data:", values);
    
      axios
        .post<Response>("your_backend_endpoint", values)
        .then((response) => {
          console.log("Post successful!", response.data);
          if (response.data.responseStatus === "E") {
            toast.error(
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>{Array.isArray(response.data.message) ? response.data.message.join("\n") : response.data.message}</h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">Successfully Submitted Form...</h4>
                <p className="alert alert-success" role="alert">
                  <h6>{Array.isArray(response.data.message) ? response.data.message.join("\n") : response.data.message}</h6>
                </p>
              </div>
            );
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );
        });
    
      setEmployee({
        pF_UN_NO: "",
        pF_NO: "",
        fpF_NO: "",
        esiC_NO: "",
      });
      formik.resetForm();
      navigate(-1);
    },
    
    
  });

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Statutory Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="pF_UN_NO"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  PF UAN NO.:
                </label>
                <input
                  type="text"
                  id="pF_UN_NO"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="pF_UN_NO"
                  placeholder="Enter PF UAN NO"
                  value={formik.values?.pF_UN_NO}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="pF_NO"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  PF NO.:
                </label>
                <input
                  type="text"
                  id="pF_NO"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="pF_NO"
                  placeholder="Enter PF NO"
                  value={formik.values.pF_NO?.toUpperCase()}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.pF_UN_NO && formik.errors.pF_UN_NO ? (
                  <div className="text-danger">{formik.errors.pF_UN_NO}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.pF_NO && formik.errors.pF_NO ? (
                  <div className="text-danger">{formik.errors.pF_NO}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="esiC_NO"
                  className="col-form-label  fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  ESIC NO :
                </label>
                <input
                  type="text"
                  id="esiC_NO"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="esiC_NO"
                  placeholder="Enter ESIC NO Code"
                  value={formik.values?.esiC_NO}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="fpF_NO"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  FPF NO :
                </label>
                <input
                  type="text"
                  id="fpF_NO"
                  className="form-control form-control-lg form-control-solid bg-light-danger"
                  style={{ cursor: "not-allowed" }}
                  name="fpF_NO"
                  placeholder="Enter Full Name"
                  value={formik.values.fpF_NO?.toUpperCase()}
                  readOnly
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.esiC_NO && formik.errors.esiC_NO ? (
                  <div className="text-danger">{formik.errors.esiC_NO}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.fpF_NO && formik.errors.fpF_NO ? (
                  <div className="text-danger">{formik.errors.fpF_NO}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit is HERE */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting || !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};
export default StatutoryDetailsEdit;
