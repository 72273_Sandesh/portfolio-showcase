import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

interface states {
  deptCode: string;
  description: string;
  desigN_CODE: string;
  designation: string;
  siteCode: string;
  flD_VALUE: string;
  descr: string;
  sectionCode: string;
  menuDescr: string;
}
const TransferHistoryAdd: React.FC = () => {
  const [employee, setEmployee] = useState({});
  const navigate = useNavigate();
  const [department, setDepartment] = useState<states[]>([]);
  const [ddltransaction, setTransaction] = useState<states[]>([]);
  const [location, setLocation] = useState<states[]>([]);
  const [designation, setDesignation] = useState<states[]>([]);
  const [currentDate, setCurrentDate] = useState<string>("");
  const [section, setSection] = useState<states[]>([]);

  const formatDate = (date: Date): string => {
    const pad = (num: number): string => num.toString().padStart(2, "0");

    const day = pad(date.getDate());
    const month = pad(date.getMonth() + 1);
    const year = date.getFullYear();
    const hours = pad(date.getHours());
    const minutes = pad(date.getMinutes());
    const seconds = pad(date.getSeconds());

    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;
  };

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/EMP_WORK_PAYROLL_site/${employeeIdForView}`)
      .then((response) => {
        setLocation(response.data);
      })
      .catch((error) => {
        console.error("Error fetching Country details:", error);
      });

    axios
      .get(`https://localhost:44360/ddl_GetTransaction`)
      .then((response) => {
        setTransaction(response.data.transaction);
      })
      .catch((error) => {
        console.error("Error fetching Country details:", error);
      });

    axios
      .get(`${EMPLOYEE_URL}/ddl_GetDepartment`)
      .then((response) => {
        setDepartment(response.data.department);
      })
      .catch((error) => {
        console.error("Error fetching Country details:", error);
      });

    axios
      .get(`${EMPLOYEE_URL}/ddl_GetDesignation`)
      .then((response) => {
        setDesignation(response.data.designation);
      })
      .catch((error) => {
        console.error("Error fetching Country details:", error);
      });

    const now = new Date();
    setCurrentDate(formatDate(now));
  }, []);

  useEffect(() => {
    if (currentDate) {
      formik.setFieldValue("offerdate", currentDate);
    }
  }, [currentDate]);
  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        const employeeData = response.data[0];
        setEmployee(employeeData);

        formik.setValues({
          designation: employeeData.designation || "",
          cadre: employeeData.cadre || "",
          curR_CTC: employeeData.curR_CTC || "",
          basic: employeeData.basic || "",
          section: employeeData.section || "",
          gradE_NAME: employeeData.gradE_NAME || "",
          depT_NAME: employeeData.depT_NAME || "",
          offerdate: employeeData.offerdate,
          transaction: employeeData.transaction || "",
          effectivedate: employeeData.effectivedate || "",
        });
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    gradE_NAME: Yup.string(),
    transaction: Yup.string().required("Transaction type is required"),
    depT_NAME: Yup.string().when("transaction", {
      is: (val: string) => ["T", "L", "J", "O", "S"].includes(val),
      then: (schema) => schema.required("Department name is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    cadre: Yup.string().when("transaction", {
      is: (val: string) => ["T", "L", "J", "O", "S"].includes(val),
      then: (schema) => schema.required("Cadre name is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    section: Yup.string().when("transaction", {
      is: (val: string) => ["T", "L", "J", "O", "S"].includes(val),
      then: (schema) => schema.required("Section name is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    designation: Yup.string().when("transaction", {
      is: (val: string) =>
        ["Q", "Z", "R", "K", "E", "C", "B", "P", "L", "O", "S"].includes(val),
      then: (schema) => schema.required("Designation name is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    //------------------------------------------------------------------------------------------------------------
    curR_CTC: Yup.string().when("transaction", {
      is: (val: string) =>
        ["H", "M", "Q", "I", "K", "U", "V", "F", "C", "P", "L", "J"].includes(
          val
        ),
      then: (schema) => schema.required("CTC per anum is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    basic: Yup.string().when("transaction", {
      is: (val: string) =>
        ["H", "M", "Q", "I", "K", "U", "V", "F", "C", "P", "L", "J"].includes(
          val
        ),
      then: (schema) => schema.required("CTC per month is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    bonus: Yup.string().when("transaction", {
      is: (val: string) =>
        ["H", "M", "Q", "I", "K", "U", "V", "F", "C", "P", "L", "J"].includes(
          val
        ),
      then: (schema) => schema.required("Bonus is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    petrol: Yup.string().when("transaction", {
      is: (val: string) =>
        ["H", "M", "Q", "I", "K", "U", "V", "F", "C", "P", "L", "J"].includes(
          val
        ),
      then: (schema) => schema.required("Petrol Allowance is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    lta: Yup.string().when("transaction", {
      is: (val: string) =>
        ["H", "M", "Q", "I", "K", "U", "V", "F", "C", "P", "L", "J"].includes(
          val
        ),
      then: (schema) => schema.required("LTA Allowance is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    meal: Yup.string().when("transaction", {
      is: (val: string) =>
        ["H", "M", "Q", "I", "K", "U", "V", "F", "C", "P", "L", "J"].includes(
          val
        ),
      then: (schema) => schema.required("Meal Allowance is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    telephone: Yup.string().when("transaction", {
      is: (val: string) =>
        ["H", "M", "Q", "I", "K", "U", "V", "F", "C", "P", "L", "J"].includes(
          val
        ),
      then: (schema) => schema.required("Telephone Allowance is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    books: Yup.string().when("transaction", {
      is: (val: string) =>
        ["H", "M", "Q", "I", "K", "U", "V", "F", "C", "P", "L", "J"].includes(
          val
        ),
      then: (schema) => schema.required("Books Allowance is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    mediclaim: Yup.string().when("transaction", {
      is: (val: string) =>
        ["H", "M", "Q", "I", "K", "U", "V", "F", "C", "P", "L", "J"].includes(
          val
        ),
      then: (schema) => schema.required("Mediclaim Allowance is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    perbonus: Yup.string().when("transaction", {
      is: (val: string) =>
        ["H", "M", "Q", "I", "K", "U", "V", "F", "C", "P", "L", "J"].includes(
          val
        ),
      then: (schema) => schema.required("Performance bonus name is required"),
      otherwise: (schema) => schema.notRequired(),
    }),

    offerdate: Yup.string(),
    effectivedate: Yup.string().required("Effective date is required"),
  });

  const formik = useFormik({
    initialValues: {
      designation: "",
      cadre: "",
      curR_CTC: "",
      basic: "",
      section: "",
      gradE_NAME: "",
      depT_NAME: "",
      transaction: "",
      effectivedate: "",
      offerdate: currentDate || "",
    },
    validationSchema: validationSchema,

    onSubmit: (values) => {
      console.log("Submitting form data:", values);
      values.offerdate = currentDate;
      axios
        .post("your_backend_endpoint", values)
        .then((response) => {
          console.log("Post successful!", response.data);
          toast.success("Work and Payroll details edited successfully");
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">error</h6>
              <p className="alert alert-danger" role="alert">
                <h6>Error Message</h6>
              </p>
            </div>
          );
        });

      setEmployee({
        designation: "",
        cadre: "",
        curR_CTC: "",
        basic: "",
        section: "",
        gradE_NAME: "",
        depT_NAME: "",
        offerdate: "",
        transaction: "",
        effectivedate: "",
      });
      formik.resetForm();
      navigate(-1);
    },
  });

  const handleClose = () => {
    navigate(-1);
  };
  // Function to handle CTC per annum change
  const handleCurRCTCChange = (e) => {
    const annualCTC = e.target.value;
    formik.setFieldValue("curR_CTC", annualCTC);

    // Calculate the monthly CTC and update the corresponding field
    if (!isNaN(annualCTC) && annualCTC !== "") {
      const monthlyCTC = (parseFloat(annualCTC) / 12).toFixed(2);
      formik.setFieldValue("basic", monthlyCTC);

      const newGrossPM = (annualCTC / 12 / 2).toFixed(2);
      formik.setFieldValue("grosspm", newGrossPM);
    } else {
      formik.setFieldValue("basic", "");
      formik.setFieldValue("grosspm", "");
    }
  };

  const fetchSection = async (department: string) => {
    try {
      //`https://localhost:44360/ddl_GetSections?siteCode=S0001&deptCode=D0001`
      //`https://localhost:44360/ddl_GetSections?siteCode=${globelocation}&deptCode=${department}`

      const response = await axios.get(
        `https://localhost:44360/ddl_GetSections?siteCode=${formik.values.cadre}&deptCode=${department}`
      );
      setSection(response.data.sections);
    } catch (error) {
      console.error("Error fetching Sections Data:", error);
    }
  };

  useEffect(() => {
    if (formik.values.depT_NAME) {
      fetchSection(formik.values.depT_NAME);
    } else {
      setSection([]);
    }
  }, [formik.values.depT_NAME]);

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Add Journey Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            <div className="row mb-1">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="transaction"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Transaction:
                </label>

                <select
                  id="transaction"
                  className="form-select form-select-lg form-select-solid "
                  name="transaction"
                  onChange={(e) => {
                    formik.handleChange(e);
                  }}
                >
                  <option value="">{formik.values.transaction}</option>
                  {ddltransaction.map((state) => (
                    <option key={state.flD_VALUE} value={state.flD_VALUE}>
                      {state.descr}
                    </option>
                  ))}
                </select>
              </div>
              <div className="row mb-1">
                <div className="col-lg-6">
                  {formik.touched.transaction && formik.errors.transaction ? (
                    <div className="text-danger">
                      {formik.errors.transaction}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="offerdate"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Order Date:
                </label>
                <input
                  type="text"
                  id="offerdate"
                  className="form-control bg-light-danger form-control-lg form-control-solid"
                  name="offerdate"
                  placeholder="Enter offerdate Name"
                  style={{ cursor: "not-allowed" }}
                  value={formik.values.offerdate}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  readOnly
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="effectivedate"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  Effective Date:
                </label>

                <input
                  type="date"
                  id="effectivedate"
                  className="form-control form-control-lg form-control-solid"
                  name="effectivedate"
                  placeholder="Enter effectivedate Name"
                  value={formik.values.effectivedate}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.offerdate && formik.errors.offerdate ? (
                  <div className="text-danger">{formik.errors.offerdate}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.effectivedate && formik.errors.effectivedate ? (
                  <div className="text-danger">
                    {formik.errors.effectivedate}
                  </div>
                ) : null}
              </div>
            </div>

            {/* Cadre and Designation Fields */}
            <div className="row mb-1">
              <>
                {["T", "L", "J", "O", "S"].includes(
                  formik.values.transaction
                ) && (
                  <div className=" d-flex align-items-center mb-1">
                    <label
                      htmlFor="cadre"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Location:
                    </label>

                    <select
                      id="cadre"
                      className="form-select form-select-lg form-select-solid "
                      name="cadre"
                      onChange={formik.handleChange}
                    >
                      <option value="">{formik.values.cadre}</option>
                      {location.map((state) => (
                        <option value={state?.siteCode}>
                          {state?.description}
                        </option>
                      ))}
                    </select>
                  </div>
                )}
                <div className="row mb-1">
                  <div className="col-lg-6">
                    {formik.touched.cadre && formik.errors.cadre ? (
                      <div className="text-danger">{formik.errors.cadre}</div>
                    ) : null}
                  </div>
                </div>
              </>
            </div>

            {["T", "L", "J", "O", "S"].includes(formik.values.transaction) && (
              <>
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="depT_NAME"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    Department:
                  </label>
                  <select
                    id="depT_NAME"
                    className="form-select form-select-lg form-select-solid mb-1"
                    name="depT_NAME"
                    onChange={(e) => {
                      formik.handleChange(e);
                      formik.values.section = "";
                      fetchSection(e.target.value);
                      console.log(e.target.value);
                    }}
                  >
                    <option value="">{formik.values.depT_NAME}</option>
                    {department.map((state) => (
                      <option value={state?.deptCode}>
                        {state?.description}
                      </option>
                    ))}
                  </select>
                </div>
              </>
            )}

            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.depT_NAME && formik.errors.depT_NAME ? (
                  <div className="text-danger">{formik.errors.depT_NAME}</div>
                ) : null}
              </div>
            </div>

            {/* TLJOS */}
            {/* {["T", "L", "J", "O", "S"].includes(formik.values.transaction) && (
              <>
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="section"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    Section:
                  </label>
                  <select
                    id="section"
                    className="form-select form-select-lg form-select-solid mb-1"
                    name="section"
                    onChange={formik.handleChange}
                  >
                    <option value="">{formik.values.section}</option>
                    {section.map((state) => (
                      <option key={state.sectionCode} value={state.sectionCode}>
                        {state.menuDescr}
                      </option>
                    ))}
                  </select>
                </div>
                <div className="row mb-1">
                  <div className="col-lg-6">
                    {formik.touched.section && formik.errors.section ? (
                      <div className="text-danger">{formik.errors.section}</div>
                    ) : null}
                  </div>
                </div>
              </>
            )}

            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.section && formik.errors.section ? (
                  <div className="text-danger">{formik.errors.section}</div>
                ) : null}
              </div>
            </div> */}

            {["T", "L", "J", "O", "S"].includes(formik.values.transaction) && (
              <>
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="section"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    Section:
                  </label>
                  <select
                    id="section"
                    className="form-select form-select-lg form-select-solid mb-1"
                    name="section"
                    onChange={formik.handleChange}
                    value={formik.values.section} // This ensures the selected section is displayed
                  >
                    {formik.values.section && (
                      <option value={formik.values.section}>
                        {formik.values.section}
                      </option>
                    )}
                    {section.length === 0 ? (
                      <option value="">No sections available</option>
                    ) : (
                      section.map((state) => (
                        <option
                          key={state.sectionCode}
                          value={state.sectionCode}
                        >
                          {state.menuDescr}
                        </option>
                      ))
                    )}
                  </select>
                </div>
                <div className="row mb-1">
                  <div className="col-lg-6">
                    {formik.touched.section && formik.errors.section ? (
                      <div className="text-danger">{formik.errors.section}</div>
                    ) : null}
                  </div>
                </div>
              </>
            )}

            {["Q", "Z", "R", "K", "E", "C", "B", "P", "L", "O", "S"].includes(
              formik.values.transaction
            ) && (
              <>
                <div className=" d-flex align-items-center mb-1">
                  <label
                    htmlFor="designation"
                    className="col-form-label fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    Designation:
                  </label>

                  <select
                    id="designation"
                    className="form-select form-select-lg form-select-solid "
                    name="designation"
                    onChange={formik.handleChange}
                  >
                    <option value="">{formik.values.designation}</option>
                    {designation.map((state) => (
                      <option value={state?.desigN_CODE}>
                        {state?.designation}
                      </option>
                    ))}
                  </select>
                </div>
              </>
            )}
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.designation && formik.errors.designation ? (
                  <div className="text-danger">{formik.errors.designation}</div>
                ) : null}
              </div>
            </div>

            {/* Cadre and Designation Fields */}

            {/* Conditional CTC Fields */}
            {[
              "H",
              "M",
              "Q",
              "I",
              "K",
              "U",
              "V",
              "F",
              "C",
              "P",
              "L",
              "J",
            ].includes(formik.values.transaction) && (
              <div>
                <hr
                  style={{
                    color: "black",
                    height: 5,
                  }}
                />

                <div className="row mb-0">
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="curR_CTC"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CTC per annum:
                    </label>
                    <input
                      type="text"
                      id="curR_CTC"
                      className="form-control form-control-lg form-control-solid"
                      name="curR_CTC"
                      placeholder="Enter curR_CTC amount"
                      value={formik.values.curR_CTC}
                      onChange={handleCurRCTCChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                  <div className="col-md-6 d-flex align-items-center mb-3">
                    <label
                      htmlFor="basic"
                      className="col-form-label fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CTC per month:
                    </label>
                    <input
                      type="text"
                      id="basic"
                      className="form-control form-control-lg form-control-solid"
                      name="basic"
                      placeholder="Enter amount for basic"
                      value={formik.values.basic}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className="row mb-0">
                  <div className="col-md-6">
                    {formik.touched.curR_CTC && formik.errors.curR_CTC ? (
                      <div className="text-danger">
                        {formik.errors.curR_CTC}
                      </div>
                    ) : null}
                  </div>
                  <div className="col-md-6">
                    {formik.touched.basic && formik.errors.basic ? (
                      <div className="text-danger">{formik.errors.basic}</div>
                    ) : null}
                  </div>
                </div>

                <hr
                  style={{
                    color: "black",
                    height: 5,
                  }}
                />

                <div className="row mb-1">
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="bonus"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Bonus:
                    </label>
                    <input
                      type="number"
                      id="bonus"
                      className="form-control form-control-lg form-control-solid"
                      name="bonus"
                      placeholder="Enter bonus amount"
                      value={formik.values.bonus}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="petrol"
                      className="col-form-label fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Petrol:
                    </label>
                    <input
                      type="number"
                      id="petrol"
                      className="form-control form-control-lg form-control-solid"
                      name="petrol"
                      placeholder="Enter amount for petrol"
                      value={formik.values.petrol}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-md-6">
                    {formik.touched.bonus && formik.errors.bonus ? (
                      <div className="text-danger">{formik.errors.bonus}</div>
                    ) : null}
                  </div>
                  <div className="col-md-6">
                    {formik.touched.petrol && formik.errors.petrol ? (
                      <div className="text-danger">{formik.errors.petrol}</div>
                    ) : null}
                  </div>
                </div>

                {/* LTA and Meal */}
                <div className="row mb-1">
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="lta"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      LTA:
                    </label>
                    <input
                      type="number"
                      id="lta"
                      className="form-control form-control-lg form-control-solid"
                      name="lta"
                      placeholder="Enter lta amount"
                      value={formik.values.lta}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="meal"
                      className="col-form-label fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Meal:
                    </label>
                    <input
                      type="number"
                      id="meal"
                      className="form-control form-control-lg form-control-solid"
                      name="meal"
                      placeholder="Enter amount for meal"
                      value={formik.values.meal}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-md-6">
                    {formik.touched.lta && formik.errors.lta ? (
                      <div className="text-danger">{formik.errors.lta}</div>
                    ) : null}
                  </div>
                  <div className="col-md-6">
                    {formik.touched.meal && formik.errors.meal ? (
                      <div className="text-danger">{formik.errors.meal}</div>
                    ) : null}
                  </div>
                </div>

                {/* Telephone and Books */}
                <div className="row mb-1">
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="telephone"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Telephone:
                    </label>
                    <input
                      type="number"
                      id="telephone"
                      className="form-control form-control-lg form-control-solid"
                      name="telephone"
                      placeholder="Enter telephone amount"
                      value={formik.values.telephone}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="books"
                      className="col-form-label fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Books:
                    </label>
                    <input
                      type="number"
                      id="books"
                      className="form-control form-control-lg form-control-solid"
                      name="books"
                      placeholder="Enter amount for books"
                      value={formik.values.books}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-md-6">
                    {formik.touched.telephone && formik.errors.telephone ? (
                      <div className="text-danger">
                        {formik.errors.telephone}
                      </div>
                    ) : null}
                  </div>
                  <div className="col-md-6">
                    {formik.touched.books && formik.errors.books ? (
                      <div className="text-danger">{formik.errors.books}</div>
                    ) : null}
                  </div>
                </div>

                {/* Mediclaim and Performance Bonus */}
                <div className="row mb-1">
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="mediclaim"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Mediclaim:
                    </label>
                    <input
                      type="number"
                      id="mediclaim"
                      className="form-control form-control-lg form-control-solid"
                      name="mediclaim"
                      placeholder="Enter mediclaim amount"
                      value={formik.values.mediclaim}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="perbonus"
                      className="col-form-label fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Performance Bonus:
                    </label>
                    <input
                      type="number"
                      id="perbonus"
                      className="form-control form-control-lg form-control-solid"
                      name="perbonus"
                      placeholder="Enter amount for perbonus"
                      value={formik.values.perbonus}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-md-6">
                    {formik.touched.mediclaim && formik.errors.mediclaim ? (
                      <div className="text-danger">
                        {formik.errors.mediclaim}
                      </div>
                    ) : null}
                  </div>
                  <div className="col-md-6">
                    {formik.touched.perbonus && formik.errors.perbonus ? (
                      <div className="text-danger">
                        {formik.errors.perbonus}
                      </div>
                    ) : null}
                  </div>
                </div>

                <hr
                  style={{
                    color: "black",
                    height: 5,
                  }}
                />

                <div className="row mb-1">
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="grosspm"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Gross PM:
                    </label>
                    <input
                      type="number"
                      id="grosspm"
                      className="form-control form-control-lg form-control-solid"
                      name="grosspm"
                      placeholder="Enter grosspm amount"
                      value={formik.values.grosspm}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="minwages"
                      className="col-form-label fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Min Wages:
                    </label>
                    <input
                      type="number"
                      id="minwages"
                      className="form-control form-control-lg form-control-solid"
                      name="minwages"
                      placeholder="Enter amount for minwages"
                      value={formik.values.minwages}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-md-6">
                    {formik.touched.grosspm && formik.errors.grosspm ? (
                      <div className="text-danger">{formik.errors.grosspm}</div>
                    ) : null}
                  </div>
                  <div className="col-md-6">
                    {formik.touched.minwages && formik.errors.minwages ? (
                      <div className="text-danger">
                        {formik.errors.minwages}
                      </div>
                    ) : null}
                  </div>
                </div>

                <div className="row mb-1">
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="basicDA"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Basic/DA:
                    </label>
                    <input
                      type="number"
                      id="basicDA"
                      className="form-control form-control-lg form-control-solid"
                      name="basicDA"
                      placeholder="Enter basicDA amount"
                      value={formik.values.basicDA}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="hra"
                      className="col-form-label fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      HRA:
                    </label>
                    <input
                      type="number"
                      id="hra"
                      className="form-control form-control-lg form-control-solid"
                      name="hra"
                      placeholder="Enter amount for hra"
                      value={formik.values.hra}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-md-6">
                    {formik.touched.basicDA && formik.errors.basicDA ? (
                      <div className="text-danger">{formik.errors.basicDA}</div>
                    ) : null}
                  </div>
                  <div className="col-md-6">
                    {formik.touched.hra && formik.errors.hra ? (
                      <div className="text-danger">{formik.errors.hra}</div>
                    ) : null}
                  </div>
                </div>

                <div className="row mb-1">
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="metroalllowed"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Metro allowed:
                    </label>
                    <input
                      type="number"
                      id="metroalllowed"
                      className="form-control form-control-lg form-control-solid"
                      name="metroalllowed"
                      placeholder="Enter metroalllowed amount"
                      value={formik.values.metroalllowed}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="education"
                      className="col-form-label fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Education:
                    </label>
                    <input
                      type="number"
                      id="education"
                      className="form-control form-control-lg form-control-solid"
                      name="education"
                      placeholder="Enter amount for education"
                      value={formik.values.education}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-md-6">
                    {formik.touched.metroalllowed &&
                    formik.errors.metroalllowed ? (
                      <div className="text-danger">
                        {formik.errors.metroalllowed}
                      </div>
                    ) : null}
                  </div>
                  <div className="col-md-6">
                    {formik.touched.education && formik.errors.education ? (
                      <div className="text-danger">
                        {formik.errors.education}
                      </div>
                    ) : null}
                  </div>
                </div>

                <div className="row mb-1">
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="otherallowance"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Other Allowance:
                    </label>
                    <input
                      type="number"
                      id="otherallowance"
                      className="form-control form-control-lg form-control-solid"
                      name="otherallowance"
                      placeholder="Enter otherallowance amount"
                      value={formik.values.otherallowance}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-md-6">
                    {formik.touched.otherallowance &&
                    formik.errors.otherallowance ? (
                      <div className="text-danger">
                        {formik.errors.otherallowance}
                      </div>
                    ) : null}
                  </div>
                </div>

                <div className="row mb-1">
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="employerpf"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Employer PF:
                    </label>
                    <input
                      type="number"
                      id="employerpf"
                      className="form-control form-control-lg form-control-solid"
                      name="employerpf"
                      placeholder="Enter employerpf amount"
                      value={formik.values.employerpf}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="employeeesic"
                      className="col-form-label fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Employee ESIC:
                    </label>
                    <input
                      type="number"
                      id="employeeesic"
                      className="form-control form-control-lg form-control-solid"
                      name="employeeesic"
                      placeholder="Enter amount for employeeesic"
                      value={formik.values.employeeesic}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-md-6">
                    {formik.touched.employerpf && formik.errors.employerpf ? (
                      <div className="text-danger">
                        {formik.errors.employerpf}
                      </div>
                    ) : null}
                  </div>
                  <div className="col-md-6">
                    {formik.touched.employeeesic &&
                    formik.errors.employeeesic ? (
                      <div className="text-danger">
                        {formik.errors.employeeesic}
                      </div>
                    ) : null}
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-md-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="balanceamount"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Balance Amount:
                    </label>
                    <input
                      type="number"
                      id="balanceamount"
                      className="form-control form-control-lg form-control-solid"
                      name="balanceamount"
                      placeholder="Enter balanceamount amount"
                      value={formik.values.balanceamount}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-md-6">
                    {formik.touched.balanceamount &&
                    formik.errors.balanceamount ? (
                      <div className="text-danger">
                        {formik.errors.balanceamount}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
            )}
          </div>

          {/* Form Actions */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting}
              //|| !formik.isValid
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default TransferHistoryAdd;
