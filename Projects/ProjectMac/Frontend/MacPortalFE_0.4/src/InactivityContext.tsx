// import React, { createContext, useContext, useEffect, useState, ReactNode } from "react";
// import { useAuth } from "./app/modules/auth";

// interface InactivityContextProps {
//   updateExpireTime: () => void;
// }

// const InactivityContext = createContext<InactivityContextProps | undefined>(
//   undefined
// );

// export const InactivityProvider: React.FC<{ children: ReactNode }> = ({
//   children,
// }) => {
//   const { logout, refreshAuthToken } = useAuth();
//   const [timeSpan] = useState(2); // in minutes

//   const checkForInActivity = () => {
//     const expireTime = localStorage.getItem("expireTime");
//     if (expireTime && parseInt(expireTime, 10) < Date.now()) {
//       logout();
//     }
//   };

//   const updateExpireTime = () => {
//     refreshAuthToken();
//     const expireTime = Date.now() + 1000 * timeSpan * 60;
//     localStorage.setItem("expireTime", expireTime.toString());
//   };

//   useEffect(() => {
//     const interval = setInterval(() => {
//       checkForInActivity();
//     }, 1000);
//     return () => clearInterval(interval);
//   }, []);

//   return (
//     <InactivityContext.Provider value={{ updateExpireTime }}>
//       {children}
//     </InactivityContext.Provider>
//   );
// };

// export const useInactivity = () => {
//   const context = useContext(InactivityContext);
//   if (!context) {
//     throw new Error("useInactivity must be used within an InactivityProvider");
//   }
//   return context;
// };



import React, { createContext, useContext, useEffect, useState, ReactNode } from "react";
import { useAuth } from "./app/modules/auth";

interface InactivityContextProps {
  updateExpireTime: () => void;
  timeLeft: number;
}

const InactivityContext = createContext<InactivityContextProps | undefined>(
  undefined
);

export const InactivityProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const { logout, refreshAuthToken } = useAuth();
  const [timeLeft, setTimeLeft] = useState(0);
  const [timeSpan] = useState(30); // in minutes

  const checkForInActivity = () => {
    const expireTime = localStorage.getItem("expireTime");
    if (expireTime && parseInt(expireTime, 10) < Date.now()) {
      logout();
    }
  };

  const updateExpireTime = () => {
    // refreshAuthToken();
    const expireTime = Date.now() + 1000 * timeSpan * 60;
    console.log("updateExpireTime() called...");
    localStorage.setItem("expireTime", expireTime.toString());
    setTimeLeft(timeSpan * 60);
  };

  useEffect(() => {
    const interval = setInterval(() => {
      checkForInActivity();
      const expireTime = localStorage.getItem("expireTime");
      if (expireTime) {
        const timeLeft = Math.max(parseInt(expireTime, 10) - Date.now(), 0);
        setTimeLeft(Math.floor(timeLeft / 1000));
      }
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    updateExpireTime();

    const updateExpireTimeListener = () => updateExpireTime();

    const elements = document.querySelectorAll(
      "button, input, a, select, option, .menu, .btn, .menu-item"
    );

    elements.forEach((element) => {
      element.addEventListener("click", updateExpireTimeListener);
    });

    return () => {
      elements.forEach((element) => {
        element.removeEventListener("click", updateExpireTimeListener);
      });
    };
  }, []);

  return (
    <InactivityContext.Provider value={{ updateExpireTime, timeLeft }}>
      {children}
    </InactivityContext.Provider>
  );
};

export const useInactivity = () => {
  const context = useContext(InactivityContext);
  if (!context) {
    throw new Error("useInactivity must be used within an InactivityProvider");
  }
  return context;
};
