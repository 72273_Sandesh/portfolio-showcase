const QUERIES = {
  EmployeeSearch_LIST: 'employeeSearch-list',
  EMPLOYEE_LIST:'employee-list',
  DEPARTMENT_LIST:'department-list',
  QUALIFICATION_LIST: 'qualification-list',
  CADRE_LIST:'cadre-list',
  GRADE_LIST:'grade-list',
  USERS_LIST: 'users-list',
  BANK_DETAILS_LIST:'bankDetails-list',
  MASTER_DETAILS_LIST:'masterDetails-list',
  JOURNEY_DETAILS_LIST:'journeyDetails-list',
  Designations_LIST:'designations-list'
}

export {QUERIES}
