import clsx from "clsx";
import { KTIcon } from "../../../helpers";
import {
  HeaderNotificationsMenu,
  HeaderUserMenu,
  Search,
  ThemeModeSwitcher,
} from "../../../partials";
import { useLayout } from "../../core";
import { useTimeout } from "../../../../timeout";
import { useAuth } from "../../../../app/modules/auth";

const itemClass = "ms-1 ms-md-4";
const btnClass =
  "btn btn-icon btn-custom btn-icon-muted btn-active-light btn-active-color-primary w-35px h-35px";
const userAvatarClass = "symbol-35px";
const btnIconClass = "fs-2";

const Navbar = () => {
  const { config } = useLayout();
  const { currentUser } = useAuth();
  const timeLeft = useTimeout();
  const formatTime = (seconds: number) => {
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    return `${String(minutes).padStart(2, "0")}:${String(
      remainingSeconds
    ).padStart(2, "0")}`;
  };

  const blinker = (seconds: number) => {
    return seconds <= 30 && " animation-blink";
  };

  return (
    <div className="track-activity app-navbar flex-shrink-0">
      <div className={clsx("app-navbar-item", itemClass)}>
        <span
          className="fw-bold fs-6 p-2 px-4 border border-gray-300 rounded text-gray-600 "
        >
          <div className={``+blinker(timeLeft)}>{formatTime(timeLeft)}</div>
        </span>
      </div>

      <div className={clsx("app-navbar-item align-items-stretch", itemClass)}>
        <Search />
      </div>

      {/* <div className={clsx("app-navbar-item", itemClass)}>
        <div id="kt_activities_toggle" className={btnClass}>
          <KTIcon iconName="chart-simple" className={btnIconClass} />
        </div>
      </div> */}

      <div className={clsx("app-navbar-item", itemClass)}>
        <div
          data-kt-menu-trigger="{default: 'click'}"
          data-kt-menu-attach="parent"
          data-kt-menu-placement="bottom-end"
          className={clsx("position-relative", btnClass)}
          //id="kt_drawer_chat_toggle"
        >
          <KTIcon iconName="notification-on" className={btnIconClass} />
          <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-warning">
            99
            {/* <span className="visually-hidden">unread messages</span> */}
          </span>
          <span className="pulse-ring" />
          {/* <span className="bullet bullet-dot bg-success h-6px w-6px position-absolute translate-middle top-0 start-50 animation-blink" /> */}
        </div>
        <HeaderNotificationsMenu />
      </div>

      {/* <div className={clsx("app-navbar-item", itemClass)}>
        <div
          className={clsx("position-relative", btnClass)}
          id="kt_drawer_chat_toggle"
        >
          <KTIcon iconName="messages" className={btnIconClass} />
          <span className="bullet bullet-dot bg-success h-6px w-6px position-absolute translate-middle top-0 start-50 animation-blink" />
        </div>
      </div> */}

      <div className={clsx("app-navbar-item", itemClass)}>
        <ThemeModeSwitcher
          toggleBtnClass={clsx("btn-active-light-primary btn-custom")}
        />
      </div>

      <div className={clsx("app-navbar-item", itemClass)}>
        <div
          className={clsx("cursor-pointer symbol", userAvatarClass)}
          data-kt-menu-trigger="{default: 'click'}"
          data-kt-menu-attach="parent"
          data-kt-menu-placement="bottom-end"
        >
          {/* <img src={toAbsoluteUrl("media/avatars/300-3.jpg")} alt="" /> */}
          <>
            {currentUser?.imageUrl ? (
              <img src={currentUser?.imageUrl} alt="User" />
            ) : (
              <img src="/media/avatars/User.png" alt="Default User" />
            )}
          </>
        </div>
        <HeaderUserMenu />
      </div>

      {config.app?.header?.default?.menu?.display && (
        <div
          className="app-navbar-item d-lg-none ms-2 me-n3"
          title="Show header menu"
        >
          <div
            className="btn btn-icon btn-active-color-primary w-35px h-35px"
            id="kt_app_header_menu_toggle"
          >
            <KTIcon iconName="text-align-left" className={btnIconClass} />
          </div>
        </div>
      )}
    </div>
  );
};

export { Navbar };
