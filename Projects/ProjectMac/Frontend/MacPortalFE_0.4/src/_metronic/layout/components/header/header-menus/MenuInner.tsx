import { useIntl } from "react-intl";
import { MenuItem } from "./MenuItem";
import { MenuInnerWithSub } from "./MenuInnerWithSub";
import { useAuth } from "../../../../../app/modules/auth";
// import {MegaMenu} from './MegaMenu'

export function MenuInner() {
  const intl = useIntl();

  const { currentUser } = useAuth();
  const LoggedInUser = currentUser?.id;
  return (
    <>
      <MenuItem
        title={intl.formatMessage({ id: "MENU.DASHBOARD" })}
        to="/dashboard"
      />
      {/* <MenuItem title="Our Mission" to="/builder" /> */}

      <MenuInnerWithSub
        title="Core HR"
        to="/crafted"
        menuPlacement="bottom-start"
        menuTrigger="click"
      >
        {/* PAGES */}
        <MenuInnerWithSub
          title="Pages"
          to="/crafted/pages"
          fontIcon="bi-archive"
          hasArrow={true}
          menuPlacement="right-start"
          menuTrigger={`{default:'click', lg: 'hover'}`}
        >
          <MenuInnerWithSub
            title="Profile"
            to="/crafted/pages/profile"
            hasArrow={true}
            hasBullet={true}
            menuPlacement="right-start"
            menuTrigger={`{default:'click', lg: 'hover'}`}
          >
            <MenuItem
              to="/crafted/pages/profile/overview"
              title="Overview"
              hasBullet={true}
            />
            <MenuItem
              to="/crafted/pages/profile/projects"
              title="Projects"
              hasBullet={true}
            />
            <MenuItem
              to="/crafted/pages/profile/campaigns"
              title="Campaigns"
              hasBullet={true}
            />
            <MenuItem
              to="/crafted/pages/profile/documents"
              title="Documents"
              hasBullet={true}
            />
            <MenuItem
              to="/crafted/pages/profile/connections"
              title="Connections"
              hasBullet={true}
            />
          </MenuInnerWithSub>
          <MenuInnerWithSub
            title="Wizards"
            to="/crafted/pages/wizards"
            hasArrow={true}
            hasBullet={true}
            menuPlacement="right-start"
            menuTrigger={`{default:'click', lg: 'hover'}`}
          >
            <MenuItem
              to="/crafted/pages/wizards/horizontal"
              title="Horizontal"
              hasBullet={true}
            />
            <MenuItem
              to="/crafted/pages/wizards/vertical"
              title="Vertical"
              hasBullet={true}
            />
          </MenuInnerWithSub>
        </MenuInnerWithSub>

        {/* Masters */}
        <MenuInnerWithSub
          title="Masters"
          to="/crafted/pages"
          icon="category"
          hasArrow={true}
          menuPlacement="right-start"
          menuTrigger={`{default:'click', lg: 'hover'}`}
        >
          <MenuItem to="/apps/cadre/cadres" title="Cadre" hasBullet={true} />
          <MenuItem
            to="/apps/department/departments"
            title="Department"
            hasBullet={true}
          />
          <MenuItem
            to="/apps/designation/designations"
            title="Designation"
            hasBullet={true}
          />
          <MenuItem to="/apps/grade/grades" title="Grade" hasBullet={true} />
          <MenuItem
            to="/apps/qualification/qualifications"
            title="Qualification"
            hasBullet={true}
          />
          <MenuItem
            to="apps/employee/employees"
            title="Employee"
            hasBullet={true}
          />
        </MenuInnerWithSub>

        {/* Transactions */}
        {(LoggedInUser === "H032422   " ||
          LoggedInUser === "H004711   " ||
          LoggedInUser === "H008567   ") && (
          <MenuInnerWithSub
            title="Transactions"
            to="/crafted/accounts"
            icon="arrow-right-left"
            hasArrow={true}
            menuPlacement="right-start"
            menuTrigger={`{default:'click', lg: 'hover'}`}
          >
            <MenuInnerWithSub
              title="Employee"
              to="/crafted/accounts"
              icon="profile-user"
              hasArrow={true}
              menuPlacement="right-start"
              menuTrigger={`{default:'click', lg: 'hover'}`}
            >
              <MenuItem
                to="/apps/bankDetails/bankDetails"
                title="Bank-Deatils"
                hasBullet={true}
              />
              <MenuItem
                to="/apps/Journey/Journey"
                title="Journey-Deatils"
                hasBullet={true}
              />
            </MenuInnerWithSub>

            <MenuItem
              to="/apps/masterDetails/masterDetails"
              title="Masters"
              icon="category"
            />
          </MenuInnerWithSub>
        )}

        {/* Reports */}
        <MenuInnerWithSub
          title="Reports"
          to="/error"
          fontIcon="bi-layers"
          hasArrow={true}
          menuPlacement="right-start"
          menuTrigger={`{default:'click', lg: 'hover'}`}
        >
          <MenuItem
            to="/error/404"
            title="Personal Information"
            hasBullet={true}
          />
          <MenuItem
            to="/error/500"
            title="Contact Information"
            hasBullet={true}
          />
          <MenuItem
            to="/error/500"
            title="Family Information"
            hasBullet={true}
          />
          <MenuItem
            to="/error/500"
            title="Education Information"
            hasBullet={true}
          />
          <MenuItem
            to="/error/500"
            title="Experience Information"
            hasBullet={true}
          />
          <MenuItem
            to="/error/500"
            title="Official Information"
            hasBullet={true}
          />
          <MenuItem to="/error/500" title="Entry Report" hasBullet={true} />
          <MenuItem
            to="/error/500"
            title="Promotion/Increment Report"
            hasBullet={true}
          />
          <MenuItem
            to="/error/500"
            title="Transfer History Report"
            hasBullet={true}
          />

          <MenuItem
            to="/error/404"
            title="Pending/Regularized Reports"
            hasBullet={true}
          />
          <MenuItem
            to="/error/500"
            title="Comparision Report (Educ.Exp.)"
            hasBullet={true}
          />
          <MenuItem
            to="/error/500"
            title="Statutory Benefit Information"
            hasBullet={true}
          />
          <MenuItem
            to="/error/500"
            title="Salary Structure History"
            hasBullet={true}
          />
          <MenuItem
            to="/error/500"
            title="Emp CTC Structure History"
            hasBullet={true}
          />
          <MenuItem
            to="/error/500"
            title="Pay Structure History Deatails"
            hasBullet={true}
          />
          <MenuItem to="/error/500" title="Employee Profile" hasBullet={true} />
          <MenuItem
            to="/error/500"
            title="Employee Details for Insurance"
            hasBullet={true}
          />
          <MenuItem
            to="/error/500"
            title="candidate attachments"
            hasBullet={true}
          />

          <MenuItem
            to="/error/500"
            title="Medical Checkup MIS"
            hasBullet={true}
          />
        </MenuInnerWithSub>

        {/* Widgets */}
        <MenuInnerWithSub
          title="Widgets"
          to="/crafted/widgets"
          fontIcon="bi-layers"
          hasArrow={true}
          menuPlacement="right-start"
          menuTrigger={`{default:'click', lg: 'hover'}`}
        >
          <MenuItem
            to="/crafted/widgets/lists"
            title="Lists"
            hasBullet={true}
          />
          <MenuItem
            to="/crafted/widgets/statistics"
            title="Statistics"
            hasBullet={true}
          />
          <MenuItem
            to="/crafted/widgets/charts"
            title="Charts"
            hasBullet={true}
          />
          <MenuItem
            to="/crafted/widgets/mixed"
            title="Mixed"
            hasBullet={true}
          />
          <MenuItem
            to="/crafted/widgets/tables"
            title="Tables"
            hasBullet={true}
          />
          <MenuItem
            to="/crafted/widgets/feeds"
            title="Feeds"
            hasBullet={true}
          />
        </MenuInnerWithSub>
      </MenuInnerWithSub>

      {/* Begin:Recuitment */}
      {/* <MenuInnerWithSub
        title="Recruitment"
        to="/apps"
        menuPlacement="bottom-start"
        menuTrigger="click"
      > */}
      {/* Transactions
        <MenuInnerWithSub
          title='Transactions'
          to='/apps/chat'
          fontIcon='bi-archive'
          hasArrow={true}
          menuPlacement='right-start'
          menuTrigger={`{default:'click', lg: 'hover'}`}
        >
          <MenuItem to='/apps/chat/private-chat' title='Resume' hasBullet={true} />
          <MenuItem to='/apps/chat/group-chat' title='Schedule' hasBullet={true} />
          <MenuItem to='/apps/chat/drawer-chat' title='Offer Calculator' hasBullet={true} />
          <MenuItem to='/apps/chat/private-chat' title='Approve/Decline Offer' hasBullet={true} />
          <MenuItem to='/apps/chat/group-chat' title='Offer History' hasBullet={true} />
          <MenuItem to='/apps/chat/drawer-chat' title='Reimbursement' hasBullet={true} />
          <MenuItem to='/apps/chat/private-chat' title='Job Posting' hasBullet={true} />
          <MenuItem to='/apps/chat/group-chat' title='Approve/Decline Job Posting ' hasBullet={true} />
          <MenuItem to='/apps/chat/drawer-chat' title='create Event' hasBullet={true} />
          <MenuItem to='/apps/chat/drawer-chat' title='Bulk Interview Schedule' hasBullet={true} />
        </MenuInnerWithSub>

 {/* Reports */}
      {/* <MenuInnerWithSub
          title="Reports"
          to="/apps/chat"
          fontIcon="bi-layers"
          hasArrow={true}
          menuPlacement="right-start"
          menuTrigger={`{default:'click', lg: 'hover'}`}
        >
          <MenuItem
            to="/apps/chat/private-chat"
            title="Candidate Schedule"
            hasBullet={true}
          />
          <MenuItem
            to="/apps/chat/group-chat"
            title="Candidate Summary"
            hasBullet={true}
          />
          <MenuItem
            to="/apps/chat/drawer-chat"
            title="Reimbursement Summary"
            hasBullet={true}
          />

          <MenuItem
            to="/apps/chat/private-chat"
            title="Candidate Information"
            hasBullet={true}
          />
          <MenuItem
            to="/apps/chat/group-chat"
            title="Offer Given Status"
            hasBullet={true}
          />
          <MenuItem
            to="/apps/chat/drawer-chat"
            title="Hire Source Summary"
            hasBullet={true}
          />

          <MenuItem
            to="/apps/chat/private-chat"
            title="Hire Source Details"
            hasBullet={true}
          />
          <MenuItem
            to="/apps/chat/group-chat"
            title="Walk-In Event Status"
            hasBullet={true}
          />
          <MenuItem
            to="/apps/chat/drawer-chat"
            title="Internal Employee Transfer"
            hasBullet={true}
          />
        </MenuInnerWithSub>

        {/* JD Dashboard */}
      {/* <MenuInnerWithSub
          title="JD Dashboard"
          to="/apps/chat"
          icon="abstract-28"
          hasArrow={true}
          menuPlacement="right-start"
          menuTrigger={`{default:'click', lg: 'hover'}`}
        >
          <MenuItem
            to="/apps/chat/private-chat"
            title="Private Chat"
            hasBullet={true}
          />
          <MenuItem
            to="/apps/chat/group-chat"
            title="Group Chart"
            hasBullet={true}
          />
          <MenuItem
            to="/apps/chat/drawer-chat"
            title="Drawer Chart"
            hasBullet={true}
          />
        </MenuInnerWithSub>
        <MenuItem
          icon="abstract-28"
          to="/apps/user-management/users"
          title="Dashboard"
        />

        <MenuItem
          icon="abstract-28"
          to="/apps/user-management/users"
          title="User management"
        />
      </MenuInnerWithSub>  */}

      {/*Begin:Seperation */}
      {/* <MenuInnerWithSub title='Seperation' to='/apps' menuPlacement='bottom-start' menuTrigger='click'>
       
        <MenuInnerWithSub
          title='Reports'
          to='/apps/chat'
          fontIcon='bi-layers'
          hasArrow={true}
          menuPlacement='right-start'
          menuTrigger={`{default:'click', lg: 'hover'}`}
        >
          <MenuItem to='/apps/chat/private-chat' title='Pending status' hasBullet={true} />
          <MenuItem to='/apps/chat/group-chat' title='Provisional Statement' hasBullet={true} />
          <MenuItem to='/apps/chat/drawer-chat' title='Check Issue Letter' hasBullet={true} />

          <MenuItem to='/apps/chat/private-chat' title='Cover Letter' hasBullet={true} />
          <MenuItem to='/apps/chat/group-chat' title='FFS Link Letter' hasBullet={true} />
          <MenuItem to='/apps/chat/drawer-chat' title='FFS Login Trace' hasBullet={true} />

          <MenuItem to='/apps/chat/private-chat' title='Relieving Letter' hasBullet={true} />
          <MenuItem to='/apps/chat/group-chat' title='Experience Letter' hasBullet={true} />
          <MenuItem to='/apps/chat/drawer-chat' title='Gratuity Claim Form' hasBullet={true} />

          <MenuItem to='/apps/chat/private-chat' title='Exit Feedback Analysis' hasBullet={true} />
          <MenuItem to='/apps/chat/group-chat' title='Exit Feedback Analysis Summary' hasBullet={true} />
          <MenuItem to='/apps/chat/drawer-chat' title='Gratuity Statement' hasBullet={true} />

        </MenuInnerWithSub>
        <MenuItem icon='abstract-28' to='/apps/user-management/users' title='Dashboard' /> 
      </MenuInnerWithSub> */}

      {/* <MenuInnerWithSub
        isMega={true}
        title='Recruitment'
        to='/mega-menu'
        menuPlacement='bottom-start'
        menuTrigger='click'
      >
        <MegaMenu />
      </MenuInnerWithSub> */}

      {/*Begin:Data Analytics */}
      {/* <MenuInnerWithSub
        title="Data Analytics"
        to="/apps"
        menuPlacement="bottom-start"
        menuTrigger="click"
      >
        <MenuInnerWithSub
          title="Reports"
          to="/apps/chat"
          fontIcon="bi-layers"
          hasArrow={true}
          menuPlacement="right-start"
          menuTrigger={`{default:'click', lg: 'hover'}`}
        >
          <MenuItem
            to="/apps/chat/private-chat"
            title="Private Chat"
            hasBullet={true}
          />
          <MenuItem
            to="/apps/chat/group-chat"
            title="Group Chart"
            hasBullet={true}
          />
          <MenuItem
            to="/apps/chat/drawer-chat"
            title="Drawer Chart"
            hasBullet={true}
          />
        </MenuInnerWithSub>

        <MenuItem
          fontIcon="bi-person"
          to="/apps/user-management/users"
          title="Recuitement"
        />
        <MenuItem
          icon="abstract-28"
          to="/apps/user-management/users"
          title="Employee Joined"
        />
      </MenuInnerWithSub> */}
    </>
  );
}
