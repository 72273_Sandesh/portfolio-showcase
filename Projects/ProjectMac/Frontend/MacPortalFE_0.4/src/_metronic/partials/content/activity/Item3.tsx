
import {FC} from 'react'
import {KTIcon, toAbsoluteUrl} from '../../../helpers'

const Item3: FC = () => {
  return (
    <div className='timeline-item'>
      <div className='timeline-line w-40px'></div>

      <div className='timeline-icon symbol symbol-circle symbol-40px'>
        <div className='symbol-label bg-light'>
          <KTIcon iconName='disconnect' className='fs-2 text-gray-500' />
        </div>
      </div>

      <div className='timeline-content mb-10 mt-n1'>
        <div className='mb-5 pe-3'>
          <a href='#' className='fs-5 fw-bold text-gray-800 text-hover-primary mb-2'>
            3 New Incoming Project Files:
          </a>

          <div className='d-flex align-items-center mt-1 fs-6'>
            <div className='text-muted me-2 fs-7'>Sent at 10:30 PM by</div>

            <div
              className='symbol symbol-circle symbol-25px'
              data-bs-toggle='tooltip'
              data-bs-boundary='window'
              data-bs-placement='top'
              title='Jan Hummer'
            >
              <img src={toAbsoluteUrl('media/avatars/300-23.jpg')} alt='img' />
            </div>
          </div>
        </div>

        <div className='overflow-auto pb-5'>
          <div className='d-flex align-items-center border border-dashed border-gray-300 rounded min-w-700px p-5'>
            <div className='d-flex flex-aligns-center pe-10 pe-lg-20'>
              <img alt='' className='w-30px me-3' src={toAbsoluteUrl('media/svg/files/pdf.svg')} />

              <div className='ms-1 fw-bold'>
                <a href='#' className='fs-6 text-hover-primary fw-bolder'>
                  Finance KPI App Guidelines
                </a>

                <div className='text-gray-500'>1.9mb</div>
              </div>
            </div>

            <div className='d-flex flex-aligns-center pe-10 pe-lg-20'>
              <img alt='' className='w-30px me-3' src={toAbsoluteUrl('media/svg/files/doc.svg')} />

              <div className='ms-1 fw-bold'>
                <a href='#' className='fs-6 text-hover-primary fw-bolder'>
                  Client UAT Testing Results
                </a>

                <div className='text-gray-500'>18kb</div>
              </div>
            </div>

            <div className='d-flex flex-aligns-center'>
              <img alt='' className='w-30px me-3' src={toAbsoluteUrl('media/svg/files/css.svg')} />

              <div className='ms-1 fw-bold'>
                <a href='#' className='fs-6 text-hover-primary fw-bolder'>
                  Finance Reports
                </a>

                <div className='text-gray-500'>20mb</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export {Item3}




// import {FC} from 'react'
// import {KTIcon, toAbsoluteUrl} from '../../../helpers'

// const Item1: FC = () => {
//   return (
    
//     <div className='timeline-item'>
//       <div className='timeline-line w-40px'></div>

//       <div className='timeline-icon symbol symbol-circle symbol-40px me-4'>
//         <div className='symbol-label bg-light'>
//           <KTIcon iconName='message-text-2' className='fs-2 text-gray-500' />
//         </div>
//       </div>

//       <div className='timeline-content mb-10 mt-n1'>
//         <div className='pe-3 mb-5'>
//           <div className='pe-3 mb-5'>
//             <div className='fs-5 fw-bold mb-2'>SSC</div>
//             <div className=' fw-bolder d-flex align-items-right'>
//             <div className='badge badge-light-success fw-bolder d-flex align-items-right'>General Studies</div>
//           </div>
//           <div className='d-flex align-items-center mt-1 fs-6'>
//             <a href='#' className='text-primary fw-bolder me-1'>
//               Maharashtra State Board
//             </a>
//             <div
//               className='symbol symbol-circle symbol-25px'
//               data-bs-toggle='tooltip'
//               data-bs-boundary='window'
//               data-bs-placement='top'
//               title='Marcus Dotson'
//             >
//               <img src={toAbsoluteUrl('media/avatars/300-2.jpg')} alt='img' />
//             </div>
//           </div>
//           <div className='fs-8 fw-bold mb-2'>2014</div>
//           <div className='d-flex flex-column w-100 me-2'>
//             <div className='d-flex flex-stack mb-2'>
//               <span className='text-muted me-2 fs-7 fw-semibold'>80.80%</span>
//             </div>
//             <div className='progress h-6px w-100'>
//               <div className='progress-bar bg-primary' role='progressbar' style={{width: '80.80%'}} ></div>
//             </div>
//           </div>
//         </div>
//           {/* <div className='fs-5 fw-bold mb-2'>
//             There are 2 new tasks for you in “AirPlus Mobile APp” project:
//           </div>

//           <div className='d-flex align-items-center mt-1 fs-6'>
//             <div className='text-muted me-2 fs-7'>Added at 4:23 PM by</div>

//             <div
//               className='symbol symbol-circle symbol-25px'
//               data-bs-toggle='tooltip'
//               data-bs-boundary='window'
//               data-bs-placement='top'
//               title='Nina Nilson'
//             >
//               <img src={toAbsoluteUrl('media/avatars/300-14.jpg')} alt='img' />
//             </div>
//           </div> */}
//         </div>

//         {/* <div className='overflow-auto pb-5'> 
//           {/* <div className='d-flex align-items-center border border-dashed border-gray-300 rounded min-w-750px px-7 py-3 mb-5'>
//             <a href='#' className='fs-5 text-gray-900 text-hover-primary fw-bold w-375px min-w-200px'>
//               Meeting with customer
//             </a>

//             <div className='min-w-175px pe-2'>
//               <span className='badge badge-light text-muted'>Application Design</span>
//             </div>

//             <div className='symbol-group symbol-hover flex-nowrap flex-grow-1 min-w-100px pe-2'>
//               <div className='symbol symbol-circle symbol-25px'>
//                 <img src={toAbsoluteUrl('media/avatars/300-2.jpg')} alt='img' />
//               </div>

//               <div className='symbol symbol-circle symbol-25px'>
//                 <img src={toAbsoluteUrl('media/avatars/300-14.jpg')} alt='img' />
//               </div>

//               <div className='symbol symbol-circle symbol-25px'>
//                 <div className='symbol-label fs-8 fw-bold bg-primary text-inverse-primary'>A</div>
//               </div>
//             </div> */}

//             {/* <div className='min-w-125px pe-2'>
//               <span className='badge badge-light-primary'>In Progress</span>
//             </div>

//             <a href='#' className='btn btn-sm btn-light btn-active-light-primary'>
//               View
//             </a>
//           </div> */}

//           {/* <div className='d-flex align-items-center border border-dashed border-gray-300 rounded min-w-750px px-7 py-3 mb-0'>
//             <a href='#' className='fs-5 text-gray-900 text-hover-primary fw-bold w-375px min-w-200px'>
//               Project Delivery Preparation
//             </a>

//             <div className='min-w-175px'>
//               <span className='badge badge-light text-muted'>CRM System Development</span>
//             </div>

//             <div className='symbol-group symbol-hover flex-nowrap flex-grow-1 min-w-100px'>
//               <div className='symbol symbol-circle symbol-25px'>
//                 <img src={toAbsoluteUrl('media/avatars/300-20.jpg')} alt='img' />
//               </div>

//               <div className='symbol symbol-circle symbol-25px'>
//                 <div className='symbol-label fs-8 fw-bold bg-success text-inverse-primary'>B</div>
//               </div>
//             </div>

//             <div className='min-w-125px'>
//               <span className='badge badge-light-success'>Completed</span>
//             </div>

//             <a href='#' className='btn btn-sm btn-light btn-active-light-primary'>
//               View
//             </a>
//           </div> 
//         </div> */}
//       </div>
//     </div>
//   )
// }

// export {Item1}
