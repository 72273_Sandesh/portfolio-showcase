import React from "react";
// import Typewriter from "typewriter-effect";

type Props = {
  className: string;
  description: string | undefined;
  color: string;
  img: string;
  userId: string | undefined | number;
};

const CardsWidget22 = ({
  className,
  description,
  color,
  img,
  currentusername,
}: Props) => {
  const getGreeting = () => {
    const currentHour = new Date().getHours();
    if (currentHour < 12) {
      return "Good Morning,";
    } else if (currentHour < 17) {
      return "Good Afternoon,";
    } else {
      return "Good Evening,";
    }
  };

  return (
    <div
      className={`card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end ${className}`}
      style={{
        backgroundColor: color,
        backgroundImage: `url('${img}')`,
        backgroundSize: "20%",
        backgroundPosition: "right 10px center",
      }}
    >
      <div className="card-header pt-5">
        <div className="card-title d-flex flex-column">
          {/* <span className="text-white opacity-75 pt-3 fw-semibold fs-3">
            {getGreeting()}
          </span> */}
          {/* <span className='fs-2 fw-bold text-white me-2 lh-1 ls-n2'>{currentusername}     {description}</span> */}
          {/* <span className="fs-2 fw-bold text-white me-2 lh-1 ls-n2">
            <Typewriter
              options={{
                strings: [`${currentusername}  ${description}`],
                autoStart: true,
                loop: true,
                cursor: "|",
                delay: 100,
                deleteSpeed: 0,
                pauseFor: 2000,
              }}
            />
          </span> */}
        </div>
      </div>
    </div>
  );
};

export { CardsWidget22 };
