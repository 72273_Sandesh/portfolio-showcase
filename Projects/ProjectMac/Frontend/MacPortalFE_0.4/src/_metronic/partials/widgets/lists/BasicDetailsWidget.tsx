import { FC, useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";


type Props = {
  className: string;
};

const BasicDetailsWidget: FC<Props> = ({ className }) => {
    const [basicDetails, setBasicDetails] = useState([]);

    useEffect(() => {
      // Fetch basic details from the backend API
      axios
        .get("https://localhost:44360/api/Employee/H003333/EMPPER")
        .then((response) => {
          setBasicDetails(response.data[0]);
        })
        .catch((error) => {
          console.error("Error fetching basic details:", error);
        });
    }, []);

  return (
    <div className={`card ${className}`} id="Basic Details">
          <div className="card-header cursor-pointer">
          
          </div>
        
    </div>
  );
};

export { BasicDetailsWidget };
