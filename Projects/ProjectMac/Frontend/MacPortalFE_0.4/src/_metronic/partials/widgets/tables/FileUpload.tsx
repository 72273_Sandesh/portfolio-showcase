import axios from "axios";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { employeeIdForView } from "../../../../app/modules/GlobalQuery";
import { useAuth } from "../../../../app/modules/auth";

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

export function FileUpload() {
  const { currentUser } = useAuth();
  const [files, setFiles] = useState([]);
  const [fileNames, setFileNames] = useState([]);
  const [userId] = useState(currentUser?.emp_Code);
  const [userIpAddress, setUserIpAddress] = useState();
  const [empCode] = useState(employeeIdForView);
  const [storageCode_Par] = useState("TEL");
  const [hidDocsId] = useState(null);
  const [file, setFile] = useState<Blob | File | null>(null);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setUserIpAddress(response.data);
      })
      .catch((error) => console.error("Error fetching IP address:", error));
  }, []);

  const handleFileChange = (event) => {
    const selectedFile = event.target.files[0];
    setFile(selectedFile);

    const selectedFiles = event.target.files;
    const fileArray = Array.from(selectedFiles);
    setFiles(fileArray);

    const namesArray = fileArray.map((file) => file.name);
    setFileNames(namesArray);
  };

  const handleSubmit = async () => {
    if (files.length === 0 || fileNames.length !== files.length) {
      toast.error("Please select files and ensure each file has a name.");
      return;
    }

    const formData = new FormData();

    files.forEach((file, index) => {
      formData.append("supportDocs", file);
      formData.append("fileNames", fileNames[index] || file.name);
    });

    if (hidDocsId) {
      formData.append("hidDocsId", hidDocsId);
    }

    axios
      .post(
        `${EMPLOYEE_URL}/upload?userId=${userId}&userIpAddress=${userIpAddress}&empCode=${empCode}&storageCode_Par=${storageCode_Par}`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      )
      .then((response) => {
        const { responseStatus, message } = response.data.data;
        console.log("File uploaded successfully!", response.data);

        if (responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Error</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
              </p>
            </div>
          );
        } else {
          toast.success(
            <div>
              <h4 className="alert-heading">Success</h4>
              <p className="alert alert-success" role="alert">
                <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
              </p>
            </div>
          );
          setFile(null);
          // setIsFormOpen(false);
        }
      })
      .catch((error) => {
        console.error("Error posting data:", error);
        toast.error(
          <div>
            <h6 className="alert-heading">Error Posting Data</h6>
            <p className="alert alert-danger" role="alert">
              <h6>{error.message}</h6>
            </p>
          </div>
        );
      });
  };

// if (!isFormOpen) {
//     return null; 
//   }

  return (
    <div
      className="menu menu-sub menu-sub-dropdown w-250px w-md-300px"
      data-kt-menu="true"
    >
      <div className="px-7 py-5">
        <div className="fs-5 text-gray-900 fw-bolder">Upload New Document</div>
      </div>

      <div className="separator border-gray-200"></div>

      <div className="px-7 py-5">
        <div className="mb-10">
          <label className="form-label fw-bold">
            Choose a PDF file from Your Device
          </label>

          <div className="input-group">
            <input
              name="pdf"
              className="form-control form-control-lg flex-grow-1"
              type="file"
              id="pdf"
              placeholder="Attach proof document"
              onChange={handleFileChange}
              accept="application/pdf"
            />
          </div>
          {file && (
            <section>
              File details:
              <ul>
                <li>Name: {file.name}</li>
                <li>Type: {file.type}</li>
                <li>Size: {file.size} bytes</li>
              </ul>
            </section>
          )}
        </div>

        <div className="d-flex justify-content-end">
          <button
            type="button"
            className="btn btn-sm btn-light btn-active-light-primary me-2"
            onClick={() => setFile(null)}
            data-kt-menu-dismiss="true"
          >
            Cancel
          </button>

          <button
            onClick={handleSubmit}
            className="btn btn-sm btn-primary"
            disabled={!file}
          >
            Upload a file
          </button>
        </div>
      </div>
    </div>
  );
}
