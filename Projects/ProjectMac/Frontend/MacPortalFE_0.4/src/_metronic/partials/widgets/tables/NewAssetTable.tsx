import React, { useEffect, useState } from "react";
import { KTIcon } from "../../../helpers";
import axios from "axios";
import { employeeIdForView, setFMIdForUpdate } from "../../../../app/modules/GlobalQuery";
import { Link, useNavigate } from "react-router-dom";

type Props = {
  className: string;
};

type AssetDetail = {
  name: string;
  proficiency: number;
  induction: string; // Assuming this property is part of AssetDetail
  datE_FORMALITY: string;
  statuS_DATE: string;
  remarks: string;
  toT_AMT: string;
  adJ_AMT: string;
};



const NewAssetsTable: React.FC<Props> = ({ className }) => {
  const [assetDetails, setAssetDetails] = useState<AssetDetail[]>([]);
  const EMPLOYEE_URL =import.meta.env.VITE_EMPLOYEE_API_URL;
  const navigate = useNavigate();
  useEffect(() => {
    // Fetch asset details from API
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/ASSETS`)
      .then((response) => {
        setAssetDetails(response.data);
      })
      .catch((error) => {
        console.error("Error fetching asset details:", error);
      });
  }, []);

  const editAssetList=(index : number)=>
    {
      setFMIdForUpdate(index);
      navigate("/crafted/pages/profile/EditAssetList");
    }

  return (
    <div className={`card ${className}`}>
  {/* <ToastContainer/> */}
      {/* begin::Header */}
      <div className="card-header bg-light-primary border-0 pt-5">
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bold fs-3 mb-1">Assets List</span>
          <span className="text-muted mt-1 fw-semibold fs-7">
            {assetDetails.length} Assets
          </span>
        </h3>
        <div
          className="card-toolbar"
          data-bs-toggle="tooltip"
          data-bs-placement="top"
          data-bs-trigger="hover"
          title="Click to add a user"
        >
               <Link
           to="/crafted/pages/profile/AddAssetList"
           className="btn btn-sm btn-primary me-3 align-self-center"
          >
        
            Add
          </Link>

          {/* <Link
           to="/crafted/pages/profile/EditAssetList"
           className="btn btn-sm btn-primary me-3 align-self-center"
          >
        
            Edit
          </Link> */}


        </div>
      </div>

     
      {/* end::Header */}
      {/* begin::Body */}
      <div className="card-body py-3">
        {/* begin::Table container */}
        <div className="table-responsive">
          {/* begin::Table */}
          <table className="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
            {/* begin::Table head */}
            <thead>
              <tr className="fw-bold text-muted">
                <th className="min-w-140px text-start">Name</th>
                <th className="min-w-100px ">Effection</th>
                <th className="min-w-140px">Completion</th>
                <th className="min-w-120px text-center">Remark</th>
                <th className="min-w-120px text-center">Amount</th>
                <th className="min-w-100px text-center">Adjust</th>
                <th className="min-w-100px text-end">Actions</th>
              </tr>
            </thead>
            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
              {assetDetails.map((assetDetail, index) => (
                <tr key={index}>
                  <td>
                    <div className="d-flex align-items-center">
                    <div className='symbol symbol-50px me-2'>
                        <span className="symbol-label bg-light-info">
                        <KTIcon
                          iconName="parcel"
                          className="fs-2x text-info"
                        />
                      </span>
                  </div>
                      <div className="d-flex justify-content-start flex-column">
                        <a
                          href="#"
                          className="text-gray-900 fw-semibold text-hover-primary fs-6"
                        >
                          {assetDetail.induction}
                        </a>
                      </div>
                    </div>
                  </td>
                  <td>
                    <a
                      href="#"
                      className="text-gray-900 fw-semibold text-hover-primary d-block fs-6"
                    >
                    
                      {new Date(assetDetail.datE_FORMALITY).toLocaleDateString(
                      "en-GB"
                    )}
                    </a>
                  </td>
                  <td>
                    <a
                      href="#"
                      className="text-gray-900 fw-semibold text-hover-primary d-block fs-6"
                    >
                      
                      {new Date(assetDetail.statuS_DATE).toLocaleDateString(
                      "en-GB"
                    )}
                    </a>
                  </td>
                  <td className="text-center">
                    <a
                      href="#"
                      className="text-gray-900 fw-semibold text-hover-primary d-block fs-6"
                      style={{ maxWidth: '400px', wordWrap: 'break-word' }}
                    >
                      {assetDetail.remarks}
                    </a>
                  </td>
                  <td>
                    <a
                      href="#"
                      className="text-gray-900 fw-semibold text-hover-primary d-block fs-6"
                    >
                      {assetDetail.toT_AMT}
                    </a>
                  </td>
                  <td className="text-center">
                    <a
                      href="#"
                      className="text-gray-900 fw-semibold text-hover-primary d-block fs-6"
                    >
                      {assetDetail.adJ_AMT}
                    </a>
                  </td>

                  <td className="text-end">
                  <a className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-2' onClick={()=>editAssetList(index)}>
                  <KTIcon iconName="pencil" className="fs-3" />
                      </a>



                      <a
                        className="btn btn-icon btn-bg-light btn-active-color-primary btn-sm"
                        style={{ cursor: "not-allowed" }}
                      >
                        <KTIcon
                          iconName="trash"
                          className="fs-3"
                          // style={{ pointerEvents: "none" }}
                        />
                      </a>
                  </td>
                 
                </tr>
              ))}
            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>
        {/* end::Table container */}
      </div>
      {/* end::Body */}
    </div>
  );
};

export { NewAssetsTable };
