const Loader = () => {
  return (
    <>
      <div
        className="modal fade show d-block"
        id="kt_modal_add_user"
        role="dialog"
        tabIndex={-1}
        aria-modal="true"
      >
        <div className="modal-dialog modal-dialog-centered mw-100px">
          <img
            src="/media/logos/macLoader1.1_gif.gif"
            style={{ height: "100px" }}
          />
        </div>
      </div>

      <div className="modal-backdrop fade show bg-light"></div>
    </>
  );
};

export { Loader };
