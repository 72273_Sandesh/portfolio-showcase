// import { FC, useEffect, useState } from "react";
// import * as Yup from "yup";
// import { useFormik } from "formik";
// import { KTIcon, isNotEmpty } from "../../../../../../_metronic/helpers";
// import { initialBankDetails, BankDetails } from "../core/_models";
// import { useAuth } from "../../../../auth";
// import { useListView } from "../core/ListViewProvider";
// import { BankDetailsListLoading } from "../components/loading/BankDetailsListLoading";
// import { createBankDetails, updateBankDetails } from "../core/_requests";
// import { globalVariable, setGlobalVariable } from "../../GlobalVariable";
// import { toast } from "react-toastify";
// import axios from "axios";
// import { useQueryResponse } from "../core/QueryResponseProvider";
// import clsx from "clsx";
// type Props = {
//   isBankDetailsLoading: boolean;
//   bankDetails: BankDetails;
// };
// interface DropdownEmp {
//   id: string;
//   emP_FNAME: string;
//   emP_LNAME: string;
// }
// interface BankDetailsDropdown {
//   banK_CODE: string;
//   banK_NAME: string;
//   banK_TYPE: string;
// }
// const editJourneyDetailsSchema = Yup.object().shape({
//   paymenT_MODE: Yup.string().required("Mode of Payment is required"),
//   banK_NAME: Yup.string().required("Bank Name is required"),
//   banK_ACCT: Yup.string().required("Account Number is required"),
//   banK_IFSC: Yup.string().required("IFSC Code is required"),
//   emP_NAME: Yup.string().required("Name of Employee is required"),
//   // emP_IMAGE: Yup.string().required("Attachment is required"),
//   changeReason: Yup.string().required("Remark is required"),
// });
// const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
// const BankDetailsEditModalForm: FC<Props> = ({
//   bankDetails,
//   isBankDetailsLoading,
// }) => {
//   const { currentUser } = useAuth();
//   const { setItemIdForUpdate } = useListView();
//   const { refetch } = useQueryResponse();
//   const [headerName, setHeaderName] = useState(globalVariable);
//   const [isEdit, setIsEdit] = useState(false);
//   const [searchName, setSearchName] = useState("");
//   const [dropDownEmpResp, setDropDownEmpResp] = useState<DropdownEmp[]>([]);
//   const [bankNameDropdown, setBankNameDropdown] = useState<
//     BankDetailsDropdown[]
//   >([]);
//   useEffect(() => {
//     axios
//       .get(`${EMPLOYEE_URL}/ddl_GetBank`)
//       .then((response) => {
//         setBankNameDropdown(response.data.banks);
//       })
//       .catch((error) =>
//         console.error("Error fetching Bank Names data:", error)
//       );
//   }, []);

//   const handleSearchChange = async (searchString: string) => {
//     setSearchName(searchString);

//     if (searchString.length > 2) {
//       axios
//         .get(
//           `${EMPLOYEE_URL}/GetAllEmployees?page=1&items_per_page=100&search=${searchString}`
//         )
//         .then((response) => {
//           setDropDownEmpResp(response.data.data);
//         })
//         .catch((error) =>
//           console.error("Error fetching combined data:", error)
//         );
//     } else {
//       setDropDownEmpResp([]);
//     }
//   };

//   const handleSelectEmployee = async (employee: any) => {
//     setDropDownEmpResp([]);
//     setSearchName(employee.emP_FNAME);

//     axios
//       .get(`${EMPLOYEE_URL}/${employee.id}/EMPPER`)
//       .then((response) => {
//         formik.setValues({
//           emP_NAME: response.data[0].emP_NAME,
//           emP_CODE: response.data[0].emP_CODE,
//           sitE_NAME: response.data[0].sitE_NAME,
//           depT_NAME: response.data[0].depT_NAME,
//           designation: response.data[0].designation,
//           datE_JOIN: response.data[0].datE_JOIN,
//           paymenT_MODE: response.data[0].paymenT_MODE,
//           banK_NAME: response.data[0].banK_NAME,
//           banK_CODE: response.data[0].banK_CODE,
//           banK_ACCT: response.data[0].banK_ACCT,
//           banK_IFSC: response.data[0].banK_IFSC,
//           emP_IMAGE: response.data[0].emP_IMAGE,
//           changeReason: response.data[0].changeReason,
//         });
//       })
//       .catch((error) => console.error("Error fetching combined data:", error));
//   };

//   const closeForm = () => {
//     setItemIdForUpdate(undefined);
//     setGlobalVariable("Add Bank Details");
//   };

//   const handleEditClick = () => {
//     setIsEdit(true);
//     setHeaderName("Edit Bank Details");
//   };

//   const handleCancelClick = () => {
//     setIsEdit(false);
//     setHeaderName("View Bank Details");
//     formik.resetForm();
//   };

//   const [bankDetailsForEdit] = useState<BankDetails>({
//     ...bankDetails,
//     emP_NAME: bankDetails.emP_NAME || initialBankDetails.emP_NAME,
//     emP_CODE: bankDetails.emP_CODE || initialBankDetails.emP_CODE,
//     sitE_NAME: bankDetails.sitE_NAME || initialBankDetails.sitE_NAME,
//     depT_NAME: bankDetails.depT_NAME || initialBankDetails.depT_NAME,
//     designation: bankDetails.designation || initialBankDetails.designation,
//     datE_JOIN: bankDetails.datE_JOIN || initialBankDetails.datE_JOIN,
//     paymenT_MODE: bankDetails.paymenT_MODE || initialBankDetails.paymenT_MODE,
//     banK_NAME: bankDetails.banK_NAME || initialBankDetails.banK_NAME,
//     banK_CODE: bankDetails.banK_CODE || initialBankDetails.banK_CODE,
//     banK_ACCT: bankDetails.banK_ACCT || initialBankDetails.banK_ACCT,
//     banK_IFSC: bankDetails.banK_IFSC || initialBankDetails.banK_IFSC,
//     emP_IMAGE: bankDetails.emP_IMAGE || initialBankDetails.emP_IMAGE,
//     changeReason: bankDetails.changeReason || initialBankDetails.changeReason,
//   });

//   // function formatDate(dateTime: string) {
//   //   const date = new Date(dateTime);
//   //   const formattedDate = date.toLocaleDateString("en-GB", {
//   //     day: "numeric",
//   //     month: "numeric",
//   //     year: "numeric",
//   //   });
//   //   const formattedTime = date.toLocaleTimeString("en-IN", {
//   //     hour: "numeric",
//   //     minute: "numeric",
//   //     second: "numeric",
//   //     timeZone: "Asia/Kolkata",
//   //   });
//   //   return `${formattedDate} ${formattedTime}`;
//   // }

//   const cancel = (withRefresh?: boolean) => {
//     if (withRefresh) {
//       refetch();
//     }
//     setItemIdForUpdate(undefined);
//   };

//   const formik = useFormik({
//     initialValues: bankDetailsForEdit,
//     validationSchema: editJourneyDetailsSchema,

//     onSubmit: async (values, { setSubmitting }) => {
//       setSubmitting(true);

//       if (isNotEmpty(values.emP_CODE) && isEdit) {
//         values.changeUser = currentUser?.first_name;
//         values.changeTerm = "TERM";
//         values.changeDate = new Date();
//         const response = await updateBankDetails(values);
//         if (response?.responseStatus === "E") {
//           toast.error(
//             <div>
//               <h4 className="alert-heading">Failed to submit Form...</h4>
//               <p className="alert alert-danger" role="alert">
//                 <h6>{response?.message}</h6>
//               </p>
//             </div>
//           );
//         } else {
//           setItemIdForUpdate(undefined);
//           setGlobalVariable("Add Bank Details");
//           toast.success(
//             <div>
//               <h4 className="alert-heading">Successfully Submitted Form...</h4>
//               <p className="alert alert-success" role="alert">
//                 <h6>{response?.message}</h6>
//               </p>
//             </div>
//           );
//           cancel(true);
//         }
//         setSubmitting(false);
//       } else {
//         values.id = "null";
//         values.changeUser = currentUser?.first_name;
//         values.changeTerm = "TERM";
//         values.changeDate = new Date();

//         const response = await createBankDetails(values);
//         if (response?.responseStatus === "E") {
//           toast.error(
//             <div>
//               <h4 className="alert-heading">Failed to submit Form...</h4>
//               <p className="alert alert-danger" role="alert">
//                 <h6>{response?.message}</h6>
//               </p>
//             </div>
//           );
//         } else {
//           setItemIdForUpdate(undefined);
//           setGlobalVariable("Add Bank Details");
//           toast.success(
//             <div>
//               <h4 className="alert-heading">Successfully Submitted Form...</h4>
//               <p className="alert alert-success" role="alert">
//                 <h6>{response?.message}</h6>
//               </p>
//             </div>
//           );
//           cancel(true);
//         }
//         setSubmitting(false);
//       }
//     },
//   });

//   return (
//     <>
//       <div className="modal-header">
//         {/* begin::Modal title */}
//         <h2 className="fw-bolder">{headerName}</h2>
//         {/* end::Modal title */}

//         {/* begin::Close */}
//         <div
//           className="btn btn-icon btn-sm btn-active-icon-primary"
//           data-kt-users-modal-action="close"
//           onClick={closeForm}
//           style={{ cursor: "pointer" }}
//         >
//           <KTIcon iconName="cross" className="fs-1" />
//         </div>
//         {/* end::Close */}
//       </div>
//       <div className="modal-body mx-5 mx-xl-5 my-2">
//         <form
//           id="kt_modal_add_user_form"
//           className="form"
//           onSubmit={formik.handleSubmit}
//           noValidate
//         >
//           <div className="card-body">
//             <div className="bg-light-warning border border-dashed border-gray-300 rounded p-5">
//               <div className="row mb-1">
//                 <div className="position-relative col-lg-6 d-flex align-items-center mb-1">
//                   <label
//                     htmlFor="emP_FNAME"
//                     className="col-form-label required fw-bold fs-6 me-2"
//                     style={{ minWidth: "150px" }}
//                   >
//                     EMPLOYEE NAME
//                   </label>
//                   {globalVariable !== "Add Bank Details" ? (
//                     <input
//                       type="text"
//                       id="emP_FNAME"
//                       className="form-control form-control-lg form-control-solid "
//                       name="emP_FNAME"
//                       value={formik.values.emP_NAME}
//                       readOnly
//                     />
//                   ) : (
//                     <input
//                       type="text"
//                       id="emP_FNAME"
//                       className={
//                         globalVariable !== "Add Bank Details"
//                           ? "form-control form-control-lg form-control-solid "
//                           : "bg-white border border-dark form-control form-control-lg form-control-solid "
//                       }
//                       name="emP_FNAME"
//                       placeholder={
//                         globalVariable === "Add Bank Details"
//                           ? "Search employee..."
//                           : "Enter Employee Name"
//                       }
//                       value={searchName}
//                       onChange={(e) => handleSearchChange(e.target.value)}
//                       readOnly={globalVariable !== "Add Bank Details"}
//                     />
//                   )}
//                   {dropDownEmpResp.length > 0 && (
//                     <ul
//                       className="position-absolute bg-white border border-secondary
//                        rounded overflow-auto top-100 list-style-none p-0 mt-0 z-index-1 mh-300px min-w-300px"
//                       style={{ left: 167 }}
//                     >
//                       {dropDownEmpResp.map((employee) => (
//                         <li
//                           className="ms-5 p-2 cursor-pointer text-hover-primary"
//                           key={employee.id}
//                           value={employee.id}
//                           onClick={() => handleSelectEmployee(employee)}
//                         >
//                           {employee.emP_FNAME} {employee.emP_LNAME}
//                         </li>
//                       ))}
//                     </ul>
//                   )}
//                 </div>

//                 <div className="col-lg-6 d-flex align-items-center mb-1">
//                   <label
//                     htmlFor="emP_CODE"
//                     className="col-form-label required fw-bold fs-6 me-2"
//                     style={{ minWidth: "150px" }}
//                   >
//                     EMPLOYEE CODE
//                   </label>
//                   <input
//                     type="text"
//                     id="emP_CODE"
//                     className="form-control form-control-lg form-control-solid  "
//                     name="emP_CODE"
//                     placeholder="Enter Employee Code"
//                     value={formik.values.emP_CODE}
//                     onChange={formik.handleChange}
//                     readOnly
//                   />
//                 </div>
//               </div>

//               {globalVariable !== "Add Bank Details" && (
//                 <div className="row mb-1">
//                   <div className="col-lg-6 d-flex align-items-center mb-1">
//                     <label
//                       htmlFor="trId"
//                       className="col-form-label required fw-bold fs-6 me-2"
//                       style={{ minWidth: "150px" }}
//                     >
//                       TRANSACTION ID
//                     </label>
//                     <input
//                       type="text"
//                       id="trId"
//                       className="form-control form-control-lg form-control-solid "
//                       name="trId"
//                       placeholder="Enter Transaction Id"
//                       value={""}
//                       onChange={formik.handleChange}
//                       readOnly
//                     />
//                   </div>

//                   <div className="col-lg-6 d-flex align-items-center mb-1">
//                     <label
//                       htmlFor="trDate"
//                       className="col-form-label required fw-bold fs-6 me-2"
//                       style={{ minWidth: "150px" }}
//                     >
//                       TRANSACTION DATE
//                     </label>
//                     <input
//                       type="date"
//                       id="trDate"
//                       className="form-control form-control-lg form-control-solid"
//                       name="trDate"
//                       placeholder="Enter Date of Transaction"
//                       value={""}
//                       onChange={formik.handleChange}
//                       readOnly
//                     />
//                   </div>
//                 </div>
//               )}

//               <div className="row mb-1">
//                 <div className="col-lg-6 d-flex align-items-center mb-1">
//                   <label
//                     htmlFor="sitE_NAME"
//                     className="col-form-label required fw-bold fs-6 me-2"
//                     style={{ minWidth: "150px" }}
//                   >
//                     SITE NAME
//                   </label>
//                   <input
//                     type="text"
//                     id="sitE_NAME"
//                     className="form-control form-control-lg form-control-solid "
//                     name="sitE_NAME"
//                     placeholder="Enter Site Name"
//                     value={formik.values.sitE_NAME}
//                     onChange={formik.handleChange}
//                     readOnly
//                   />
//                 </div>

//                 <div className="col-lg-6 d-flex align-items-center mb-1">
//                   <label
//                     htmlFor="depT_NAME"
//                     className="col-form-label required fw-bold fs-6 me-2"
//                     style={{ minWidth: "150px" }}
//                   >
//                     DEPARTMENT
//                   </label>
//                   <input
//                     type="text"
//                     id="depT_NAME"
//                     className="form-control form-control-lg form-control-solid  "
//                     name="depT_NAME"
//                     placeholder="Enter Department Name"
//                     value={formik.values.depT_NAME}
//                     onChange={formik.handleChange}
//                     readOnly
//                   />
//                 </div>
//               </div>

//               <div className="row mb-1">
//                 <div className="col-lg-6 d-flex align-items-center mb-1">
//                   <label
//                     htmlFor="designation"
//                     className="col-form-label required fw-bold fs-6 me-2"
//                     style={{ minWidth: "150px" }}
//                   >
//                     DESIGNATION
//                   </label>
//                   <input
//                     type="text"
//                     id="designation"
//                     className="form-control form-control-lg form-control-solid "
//                     name="designation"
//                     placeholder="Enter Designation"
//                     value={formik.values.designation}
//                     onChange={formik.handleChange}
//                     readOnly
//                   />
//                 </div>

//                 <div className="col-lg-6 d-flex align-items-center mb-1">
//                   <label
//                     htmlFor="datE_JOIN"
//                     className="col-form-label required fw-bold fs-6 me-2"
//                     style={{ minWidth: "150px" }}
//                   >
//                     JOIN DATE
//                   </label>
//                   <input
//                     type="date"
//                     id="datE_JOIN"
//                     className="form-control form-control-lg form-control-solid  "
//                     name="datE_JOIN"
//                     placeholder="Enter Designation"
//                     value={formik.values.datE_JOIN?.split("T")[0]}
//                     onChange={formik.handleChange}
//                     readOnly
//                   />
//                 </div>
//               </div>
//             </div>

//             <div className="p-5">
//               {globalVariable === "Add Bank Details" &&
//               searchName === "" &&
//               dropDownEmpResp.length === 0 ? (
//                 <div className="text-center">
//                   <img
//                     src="/media/logos/searchEmpWarn.png"
//                     style={{ height: "150px" }}
//                   />
//                   <h1>Please Search Employee Name First...</h1>
//                 </div>
//               ) : (
//                 <>
//                   <div className="row mb-1">
//                     <div className="col-lg-6">
//                       <div className="d-flex align-items-center mb-1">
//                         <label
//                           htmlFor="paymenT_MODE"
//                           className="col-form-label required fw-bold fs-6 me-2"
//                           style={{ minWidth: "150px" }}
//                         >
//                           MODE OF PAYMENT
//                         </label>
//                         <select
//                           id="paymenT_MODE"
//                           className={clsx(
//                             "form-select form-select-lg form-select-solid ",
//                             {
//                               "is-invalid":
//                                 formik.touched.paymenT_MODE &&
//                                 formik.errors.paymenT_MODE,
//                             },
//                             {
//                               "is-valid":
//                                 formik.touched.paymenT_MODE &&
//                                 !formik.errors.paymenT_MODE,
//                             }
//                           )}
//                           // className="form-select form-select-lg form-select-solid "
//                           name="paymenT_MODE"
//                           value={formik.values.paymenT_MODE}
//                           onChange={formik.handleChange}
//                           disabled={
//                             isEdit === false &&
//                             globalVariable !== "Add Bank Details"
//                           }
//                         >
//                           <option value={formik.values.paymenT_MODE}>
//                             {formik.values.paymenT_MODE}
//                           </option>
//                         </select>
//                       </div>
//                       {formik.touched.paymenT_MODE &&
//                         formik.errors.paymenT_MODE && (
//                           <div className="fv-plugins-message-container text-danger">
//                             <span role="alert">
//                               {formik.errors.paymenT_MODE}
//                             </span>
//                           </div>
//                         )}
//                     </div>
//                     <div className="col-lg-6">
//                       <div className="d-flex align-items-center mb-1">
//                         <label
//                           htmlFor="banK_NAME"
//                           className="col-form-label required fw-bold fs-6 me-2"
//                           style={{ minWidth: "150px" }}
//                         >
//                           BANK NAME
//                         </label>
//                         <select
//                           id="banK_NAME"
//                           // className="form-select form-select-lg form-select-solid "
//                           className={clsx(
//                             "form-select form-select-lg form-select-solid ",
//                             {
//                               "is-invalid":
//                                 formik.touched.banK_NAME &&
//                                 formik.errors.banK_NAME,
//                             },
//                             {
//                               "is-valid":
//                                 formik.touched.banK_NAME &&
//                                 !formik.errors.banK_NAME,
//                             }
//                           )}
//                           name="banK_NAME"
//                           value={formik.values.banK_NAME}
//                           onChange={formik.handleChange}
//                           // disabled={globalVariable === "infoJourneyDetails"}
//                           disabled={
//                             isEdit === false &&
//                             globalVariable !== "Add Bank Details"
//                           }
//                         >
//                           <option value={formik.values.banK_CODE}>
//                             {formik.values.banK_NAME}
//                           </option>
//                           {bankNameDropdown.map((bankName) => (
//                             <option value={bankName.banK_CODE}>
//                               {bankName.banK_NAME}
//                             </option>
//                           ))}
//                         </select>
//                       </div>
//                       {formik.touched.banK_NAME && formik.errors.banK_NAME && (
//                         <div className="fv-plugins-message-container text-danger">
//                           <span role="alert">{formik.errors.banK_NAME}</span>
//                         </div>
//                       )}
//                     </div>
//                   </div>

//                   <div className="row mb-1">
//                     <div className="col-lg-6">
//                       <div className="d-flex align-items-center mb-1">
//                         <label
//                           htmlFor="banK_ACCT"
//                           className="col-form-label required fw-bold fs-6 me-2"
//                           style={{ minWidth: "150px" }}
//                         >
//                           ACCOUNT NUMBER
//                         </label>
//                         <input
//                           type="number"
//                           id="banK_ACCT"
//                           // className="form-control form-control-lg form-control-solid "
//                           className={clsx(
//                             "form-control form-control-lg form-control-solid",
//                             {
//                               "is-invalid":
//                                 formik.touched.banK_ACCT &&
//                                 formik.errors.banK_ACCT,
//                             },
//                             {
//                               "is-valid":
//                                 formik.touched.banK_ACCT &&
//                                 !formik.errors.banK_ACCT,
//                             }
//                           )}
//                           name="banK_ACCT"
//                           placeholder="Enter Organization Name"
//                           value={formik.values.banK_ACCT}
//                           onChange={formik.handleChange}
//                           // readOnly={globalVariable === "infoJourneyDetails"}
//                           readOnly={
//                             isEdit === false &&
//                             globalVariable !== "Add Bank Details"
//                           }
//                         />
//                       </div>
//                       {formik.touched.banK_ACCT && formik.errors.banK_ACCT && (
//                         <div className="fv-plugins-message-container text-danger">
//                           <span role="alert">{formik.errors.banK_ACCT}</span>
//                         </div>
//                       )}
//                     </div>
//                     <div className="col-lg-6">
//                       <div className=" d-flex align-items-center mb-1">
//                         <label
//                           htmlFor="banK_IFSC"
//                           className="col-form-label required fw-bold fs-6 me-2"
//                           style={{ minWidth: "150px" }}
//                         >
//                           IFSC CODE
//                         </label>
//                         <input
//                           type="text"
//                           id="banK_IFSC"
//                           // className="form-control form-control-lg form-control-solid  "
//                           className={clsx(
//                             "form-control form-control-lg form-control-solid",
//                             {
//                               "is-invalid":
//                                 formik.touched.banK_IFSC &&
//                                 formik.errors.banK_IFSC,
//                             },
//                             {
//                               "is-valid":
//                                 formik.touched.banK_IFSC &&
//                                 !formik.errors.banK_IFSC,
//                             }
//                           )}
//                           name="banK_IFSC"
//                           placeholder="Enter IFSC code"
//                           value={formik.values.banK_IFSC}
//                           onChange={formik.handleChange}
//                           // readOnly={globalVariable === "infoJourneyDetails"}
//                           readOnly={
//                             isEdit === false &&
//                             globalVariable !== "Add Bank Details"
//                           }
//                         />
//                       </div>

//                       {formik.touched.banK_IFSC && formik.errors.banK_IFSC && (
//                         <div className="fv-plugins-message-container text-danger">
//                           <span role="alert">{formik.errors.banK_IFSC}</span>
//                         </div>
//                       )}
//                     </div>
//                   </div>

//                   <div className="row mb-1">
//                     <div className="col-lg-6">
//                       <div className="d-flex align-items-center mb-1">
//                         <label
//                           htmlFor="emP_NAME"
//                           className="col-form-label required fw-bold fs-6 me-2"
//                           style={{ minWidth: "150px" }}
//                         >
//                           NAME AS PER BANK PASSBOOK
//                         </label>
//                         <input
//                           type="text"
//                           id="emP_NAME"
//                           // className="form-control form-control-lg form-control-solid "
//                           className={clsx(
//                             "form-control form-control-lg form-control-solid",
//                             {
//                               "is-invalid":
//                                 formik.touched.emP_NAME &&
//                                 formik.errors.emP_NAME,
//                             },
//                             {
//                               "is-valid":
//                                 formik.touched.emP_NAME &&
//                                 !formik.errors.emP_NAME,
//                             }
//                           )}
//                           name="emP_NAME"
//                           placeholder="Enter Name as per Bank"
//                           value={formik.values.emP_NAME}
//                           onChange={formik.handleChange}
//                           // readOnly={globalVariable === "infoJourneyDetails"}
//                           readOnly={
//                             isEdit === false &&
//                             globalVariable !== "Add Bank Details"
//                           }
//                         />
//                       </div>
//                       {formik.touched.emP_NAME && formik.errors.emP_NAME && (
//                         <div className="fv-plugins-message-container text-danger">
//                           <span role="alert">{formik.errors.emP_NAME}</span>
//                         </div>
//                       )}
//                     </div>
//                     <div className="col-lg-6">
//                       <div className=" d-flex align-items-center mb-1">
//                         <label
//                           htmlFor="emP_IMAGE"
//                           className="col-form-label required fw-bold fs-6 me-2"
//                           style={{ minWidth: "150px" }}
//                         >
//                           ATTACH DOCUMENT
//                         </label>
//                         <input
//                           name="emP_IMAGE"
//                           // className="form-control form-control-lg flex-grow-1"
//                           className={clsx(
//                             "form-control form-control-lg flex-grow-1",
//                             {
//                               "is-invalid":
//                                 formik.touched.emP_IMAGE &&
//                                 formik.errors.emP_IMAGE,
//                             },
//                             {
//                               "is-valid":
//                                 formik.touched.emP_IMAGE &&
//                                 !formik.errors.emP_IMAGE,
//                             }
//                           )}
//                           type="file"
//                           id="pdf"
//                           // onChange={handlePdfChange}
//                           accept="application/pdf"
//                           required
//                           // disabled={globalVariable === "infoJourneyDetails"}
//                           disabled={
//                             isEdit === false &&
//                             globalVariable !== "Add Bank Details"
//                           }
//                         />
//                       </div>
//                       {formik.touched.emP_IMAGE && formik.errors.emP_IMAGE && (
//                         <div className="fv-plugins-message-container text-danger">
//                           <span role="alert">{formik.errors.emP_IMAGE}</span>
//                         </div>
//                       )}
//                     </div>
//                   </div>

//                   <div className="row mb-1">
//                     <div className="col-lg-6">
//                       <div className="d-flex align-items-center mb-1">
//                         <label
//                           htmlFor="changeReason"
//                           className="col-form-label required fw-bold fs-6 me-2"
//                           style={{ minWidth: "150px" }}
//                         >
//                           REMARKS
//                         </label>
//                         <input
//                           type="text"
//                           id="changeReason"
//                           // className="form-control form-control-lg form-control-solid "
//                           className={clsx(
//                             "form-control form-control-lg form-control-solid ",
//                             {
//                               "is-invalid":
//                                 formik.touched.changeReason &&
//                                 formik.errors.changeReason,
//                             },
//                             {
//                               "is-valid":
//                                 formik.touched.changeReason &&
//                                 !formik.errors.changeReason,
//                             }
//                           )}
//                           name="changeReason"
//                           placeholder="Enter Organization Name"
//                           value={formik.values.changeReason}
//                           onChange={formik.handleChange}
//                           readOnly={
//                             isEdit === false &&
//                             globalVariable !== "Add Bank Details"
//                           }
//                         />
//                       </div>
//                       {formik.touched.changeReason &&
//                         formik.errors.changeReason && (
//                           <div className="fv-plugins-message-container text-danger">
//                             <span role="alert">
//                               {formik.errors.changeReason}
//                             </span>
//                           </div>
//                         )}
//                     </div>
//                   </div>
//                 </>
//               )}
//             </div>
//           </div>
//           <div className="text-center pt-15">
//             {!isEdit ? (
//               <button
//                 type="reset"
//                 onClick={closeForm}
//                 className="btn btn-light me-3"
//               >
//                 Back
//               </button>
//             ) : (
//               <button
//                 type="reset"
//                 onClick={handleCancelClick}
//                 className="btn btn-light me-3"
//               >
//                 Cancel
//               </button>
//             )}
//             {!isEdit && globalVariable !== "Add Bank Details" && (
//               <button
//                 type="button"
//                 onClick={handleEditClick}
//                 className="btn btn-primary me-3"
//               >
//                 Edit
//               </button>
//             )}
//             {(isEdit || globalVariable === "Add Bank Details") && (
//               <button
//                 type="submit"
//                 className="btn btn-primary"
//                 disabled={
//                   isBankDetailsLoading ||
//                   formik.isSubmitting ||
//                   // !formik.isValid ||
//                   !formik.touched
//                 }
//               >
//                 <span className="indicator-label">Submit</span>

//                 {(formik.isSubmitting || isBankDetailsLoading) && (
//                   <span className="indicator-progress">
//                     Please wait...{" "}
//                     <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
//                   </span>
//                 )}
//               </button>
//             )}
//           </div>
//         </form>
//       </div>
//       {(formik.isSubmitting || isBankDetailsLoading) && (
//         <BankDetailsListLoading />
//       )}
//     </>
//   );
// };

// export { BankDetailsEditModalForm };

import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { isNotEmpty } from "../../../../../../_metronic/helpers";
import { useAuth } from "../../../../auth";
import { useListView } from "../core/ListViewProvider";
import { globalVariable, setGlobalVariable } from "../../GlobalVariable";
import { toast } from "react-toastify";
import axios from "axios";
import { useQueryResponse } from "../core/QueryResponseProvider";
import { JourneyDetailsListLoading } from "../components/Loading/JourneyDetailsListLoading";
import { JourneyDetails, initialJourneyDetails } from "../core/_models";
import { createJourneyDetails, updateJourneyDetails } from "../core/_requests";

type Props = {
  isBankDetailsLoading: boolean;
  bankDetails: JourneyDetails;
};
interface DropdownEmp {
  id: string;
  emP_FNAME: string;
  emP_LNAME: string;
}
interface BankDetailsDropdown {
  banK_CODE: string;
  banK_NAME: string;
  banK_TYPE: string;
}
interface states {
  deptCode: string;
  description: string;
  desigN_CODE: string;
  designation: string;
  siteCode: string;
  flD_VALUE: string;
  descr: string;
  sectionCode: string;
  menuDescr: string;
}
const editJourneyDetailsSchema = Yup.object().shape({
  // paymenT_MODE: Yup.string().required("Mode of Payment is required"),
  // banK_NAME: Yup.string().required("Bank Name is required"),
  // banK_ACCT: Yup.string().required("Account Number is required"),
  // banK_IFSC: Yup.string().required("IFSC Code is required"),
  // emP_NAME: Yup.string().required("Name of Employee is required"),
  // // emP_IMAGE: Yup.string().required("Attachment is required"),
  // changeReason: Yup.string().required("Remark is required"),
});
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
const JourneyDetailsEditModalForm: FC<Props> = ({
  bankDetails,
  isBankDetailsLoading,
}) => {
  const { currentUser } = useAuth();
  const { setItemIdForUpdate } = useListView();
  const { refetch } = useQueryResponse();
  const [searchName, setSearchName] = useState("");
  const [location, setLocation] = useState<states[]>([]);
  const [currentDate, setCurrentDate] = useState<string>("");
  const [selectedEmpcode ,setSelectedEmpcode]  = useState<string>("");
  const [designation, setDesignation] = useState<states[]>([]);
  const [section, setSection] = useState<states[]>([]);

  const [dropDownEmpResp, setDropDownEmpResp] = useState<DropdownEmp[]>([]);
  const [bankNameDropdown, setBankNameDropdown] = useState<
    BankDetailsDropdown[]
  >([]);
  const [ddltransaction, setTransaction] = useState<states[]>([]);
  useEffect(() => {
    if (currentDate) {
      formik.setFieldValue("offerdate", currentDate);
    }
  }, [currentDate]);
  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/ddl_GetBank`)
      .then((response) => {
        setBankNameDropdown(response.data.banks);
      })
      .catch((error) =>
        console.error("Error fetching Bank Names data:", error)
      );

     
  }, []);

  const handleSearchChange = async (searchString: string) => {
    setSearchName(searchString);

    if (searchString.length > 2) {
      axios
        .get(
          `${EMPLOYEE_URL}/GetAllEmployees?page=1&items_per_page=100&search=${searchString}`
        )
        .then((response) => {
          setDropDownEmpResp(response.data.data);
        })
        .catch((error) =>
          console.error("Error fetching combined data:", error)
        );
    } else {
      setDropDownEmpResp([]);
    }
  };

  const handleSelectEmployee = async (employee: any) => {
    setDropDownEmpResp([]);
    setSearchName(employee.emP_FNAME);

    // setSelectedEmpcode(employee.id)

    axios
      .get(`${EMPLOYEE_URL}/EMP_WORK_PAYROLL_site/${employee.id}`)
      .then((response) => {
        setLocation(response.data);
      })
      .catch((error) => {
        console.error("Error fetching Country details:", error);
      });
    axios
      .get(`${EMPLOYEE_URL}/${employee.id}/EMPPER`)
      .then((response) => {
        formik.setValues({
          emP_NAME: response.data[0].emP_NAME,
          emP_CODE: response.data[0].emP_CODE,
          sitE_NAME: response.data[0].sitE_NAME,
          depT_NAME: response.data[0].depT_NAME,
          designation: response.data[0].designation,
          datE_JOIN: response.data[0].datE_JOIN,
          paymenT_MODE: response.data[0].paymenT_MODE,
          banK_NAME: response.data[0].banK_NAME,
          banK_CODE: response.data[0].banK_CODE,
          banK_ACCT: response.data[0].banK_ACCT,
          banK_IFSC: response.data[0].banK_IFSC,
          emP_IMAGE: response.data[0].emP_IMAGE,
          changeReason: response.data[0].changeReason,
        });
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  };

  const formatDate = (date: Date): string => {
    const pad = (num: number): string => num.toString().padStart(2, "0");

    const day = pad(date.getDate());
    const month = pad(date.getMonth() + 1);
    const year = date.getFullYear();
    const hours = pad(date.getHours());
    const minutes = pad(date.getMinutes());
    const seconds = pad(date.getSeconds());

    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;
  };

  useEffect(() => {
    axios
      .get(`https://localhost:44360/ddl_GetTransaction`)
      .then((response) => {
        setTransaction(response.data.transaction);
      })
      .catch((error) => {
        console.error("Error fetching Country details:", error);
      });

      axios
      .get(`${EMPLOYEE_URL}/ddl_GetDesignation`)
      .then((response) => {
        setDesignation(response.data.designation);
      })
      .catch((error) => {
        console.error("Error fetching Country details:", error);
      });
      
      const now = new Date();
      setCurrentDate(formatDate(now));
    }, []);

  const closeForm = () => {
    setItemIdForUpdate(undefined);
    setGlobalVariable("addJourneyDetails");
  };

  const [bankDetailsForEdit] = useState<JourneyDetails>({
    ...bankDetails,
    emP_NAME: bankDetails.emP_NAME || initialJourneyDetails.emP_NAME,
    emP_CODE: bankDetails.emP_CODE || initialJourneyDetails.emP_CODE,
    sitE_NAME: bankDetails.sitE_NAME || initialJourneyDetails.sitE_NAME,
    depT_NAME: bankDetails.depT_NAME || initialJourneyDetails.depT_NAME,
    designation: bankDetails.designation || initialJourneyDetails.designation,
    datE_JOIN: bankDetails.datE_JOIN || initialJourneyDetails.datE_JOIN,
    paymenT_MODE:
      bankDetails.paymenT_MODE || initialJourneyDetails.paymenT_MODE,
    banK_NAME: bankDetails.banK_NAME || initialJourneyDetails.banK_NAME,
    banK_CODE: bankDetails.banK_CODE || initialJourneyDetails.banK_CODE,
    banK_ACCT: bankDetails.banK_ACCT || initialJourneyDetails.banK_ACCT,
    banK_IFSC: bankDetails.banK_IFSC || initialJourneyDetails.banK_IFSC,
    emP_IMAGE: bankDetails.emP_IMAGE || initialJourneyDetails.emP_IMAGE,
    offerdate:currentDate || "",
    changeReason:
      bankDetails.changeReason || initialJourneyDetails.changeReason,
  });

  // function formatDate(dateTime: string) {
  //   const date = new Date(dateTime);
  //   const formattedDate = date.toLocaleDateString("en-GB", {
  //     day: "numeric",
  //     month: "numeric",
  //     year: "numeric",
  //   });
  //   const formattedTime = date.toLocaleTimeString("en-IN", {
  //     hour: "numeric",
  //     minute: "numeric",
  //     second: "numeric",
  //     timeZone: "Asia/Kolkata",
  //   });
  //   return `${formattedDate} ${formattedTime}`;
  // }

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };

  const formik = useFormik({
    initialValues: bankDetailsForEdit,
    validationSchema: editJourneyDetailsSchema,

    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      if (
        isNotEmpty(values.emP_CODE) &&
        globalVariable === "editJourneyDetails"
      ) {
        values.changeUser = currentUser?.first_name;
        values.changeTerm = "TERM";
        values.changeDate = new Date();
        const response = await updateJourneyDetails(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("addJourneyDetails");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      } else {
        values.id = "null";
        values.changeUser = currentUser?.first_name;
        values.changeTerm = "TERM";
        values.changeDate = new Date();

        const response = await createJourneyDetails(values);
        if (response?.responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
        } else {
          setItemIdForUpdate(undefined);
          setGlobalVariable("addJourneyDetails");
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{response?.message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
        setSubmitting(false);
      }
    },
  });

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="card-body">
          <div className="bg-light-warning border border-dashed border-gray-300 rounded p-5">
            <div className="row mb-1">
              <div className="position-relative col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="emP_FNAME"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  EMPLOYEE NAME
                </label>
                {globalVariable !== "addJourneyDetails" ? (
                  <input
                    type="text"
                    id="emP_FNAME"
                    className="form-control form-control-lg form-control-solid "
                    name="emP_FNAME"
                    value={formik.values.emP_NAME}
                    readOnly
                  />
                ) : (
                  <input
                    type="text"
                    id="emP_FNAME"
                    className={
                      globalVariable !== "addJourneyDetails"
                        ? "form-control form-control-lg form-control-solid "
                        : "bg-white border border-dark form-control form-control-lg form-control-solid "
                    }
                    name="emP_FNAME"
                    placeholder={
                      globalVariable === "addJourneyDetails"
                        ? "Search employee..."
                        : "Enter Employee Name"
                    }
                    value={searchName}
                    onChange={(e) => handleSearchChange(e.target.value)}
                    readOnly={globalVariable !== "addJourneyDetails"}
                  />
                )}
                {dropDownEmpResp.length > 0 && (
                  <ul
                    className="position-absolute bg-white border border-secondary
                       rounded overflow-auto top-100 list-style-none p-0 mt-0 z-index-1 mh-300px min-w-300px"
                    style={{ left: 167 }}
                  >
                    {dropDownEmpResp.map((employee) => (
                      <li
                        className="ms-5 p-2 cursor-pointer text-hover-primary"
                        key={employee.id}
                        value={employee.id}
                        onClick={() => handleSelectEmployee(employee)}
                      >
                        {employee.emP_FNAME} {employee.emP_LNAME}
                      </li>
                    ))}
                  </ul>
                )}
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="emP_CODE"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  EMPLOYEE CODE
                </label>
                <input
                  type="text"
                  id="emP_CODE"
                  className="form-control form-control-lg form-control-solid  "
                  name="emP_CODE"
                  placeholder="Enter Employee Code"
                  value={formik.values.emP_CODE}
                  onChange={formik.handleChange}
                  readOnly
                />
              </div>
            </div>

            {/* {globalVariable !== "addJourneyDetails" && (
              <div className="row mb-1">
                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="trId"
                    className="col-form-label fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    TRANSACTION ID
                  </label>
                  <input
                    type="text"
                    id="trId"
                    className="form-control form-control-lg form-control-solid "
                    name="trId"
                    placeholder="Enter Transaction Id"
                    value={""}
                    onChange={formik.handleChange}
                    readOnly
                  />
                </div>

                <div className="col-lg-6 d-flex align-items-center mb-1">
                  <label
                    htmlFor="trDate"
                    className="col-form-label fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    TRANSACTION DATE
                  </label>
                  <input
                    type="date"
                    id="trDate"
                    className="form-control form-control-lg form-control-solid"
                    name="trDate"
                    placeholder="Enter Date of Transaction"
                    value={""}
                    onChange={formik.handleChange}
                    readOnly
                  />
                </div>
              </div>
            )} */}

            {/* <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sitE_NAME"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SITE NAME
                </label>
                <input
                  type="text"
                  id="sitE_NAME"
                  className="form-control form-control-lg form-control-solid "
                  name="sitE_NAME"
                  placeholder="Enter Site Name"
                  value={formik.values.sitE_NAME}
                  onChange={formik.handleChange}
                  readOnly
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="depT_NAME"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DEPARTMENT
                </label>
                <input
                  type="text"
                  id="depT_NAME"
                  className="form-control form-control-lg form-control-solid  "
                  name="depT_NAME"
                  placeholder="Enter Department Name"
                  value={formik.values.depT_NAME}
                  onChange={formik.handleChange}
                  readOnly
                />
              </div>
            </div> */}

            {/* <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="designation"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DESIGNATION
                </label>
                <input
                  type="text"
                  id="designation"
                  className="form-control form-control-lg form-control-solid "
                  name="designation"
                  placeholder="Enter Designation"
                  value={formik.values.designation}
                  onChange={formik.handleChange}
                  readOnly
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="datE_JOIN"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  JOIN DATE
                </label>
                <input
                  type="date"
                  id="datE_JOIN"
                  className="form-control form-control-lg form-control-solid  "
                  name="datE_JOIN"
                  placeholder="Enter Designation"
                  value={formik.values.datE_JOIN?.split("T")[0]}
                  onChange={formik.handleChange}
                  readOnly
                />
              </div>
            </div> */}
          </div>

          <div className="p-5">
            {globalVariable === "addJourneyDetails" &&
            searchName === "" &&
            dropDownEmpResp.length === 0 ? (
              <div className="text-center">
                <img
                  src="/media/logos/searchEmpWarn.png"
                  style={{ height: "150px" }}
                />
                <h1>Please Search Employee Name First...</h1>
              </div>
            ) : (
              <>
                <div className="row mb-1">
                  <div className="d-flex align-items-center mb-1">
                    <label
                      htmlFor="transaction"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Transaction:
                    </label>

                    <select
                      id="transaction"
                      className="form-select form-select-lg form-select-solid "
                      name="transaction"
                      onChange={(e) => {
                        formik.handleChange(e);
                      }}
                    >
                      <option value="">{formik.values.transaction}</option>
                      {ddltransaction.map((state) => (
                        <option key={state.flD_VALUE} value={state.flD_VALUE}>
                          {state.descr}
                        </option>
                      ))}
                    </select>
                  </div>
                  <div className="row mb-1">
                    <div className="col-lg-6">
                      {formik.touched.transaction &&
                      formik.errors.transaction ? (
                        <div className="text-danger">
                          {formik.errors.transaction}
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-lg-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="offerdate"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Order Date:
                    </label>
                    <input
                      type="text"
                      id="offerdate"
                      className="form-control bg-light-danger form-control-lg form-control-solid"
                      name="offerdate"
                      placeholder="Enter offerdate Name"
                      style={{ cursor: "not-allowed" }}
                      value={formik.values.offerdate}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      readOnly
                    />
                  </div>
                  <div className="col-lg-6 d-flex align-items-center mb-1">
                    <label
                      htmlFor="effectivedate"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      Effective Date:
                    </label>

                    <input
                      type="date"
                      id="effectivedate"
                      className="form-control form-control-lg form-control-solid"
                      name="effectivedate"
                      placeholder="Enter effectivedate Name"
                      value={formik.values.effectivedate}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className="row mb-1">
                  <div className="col-lg-6">
                    {formik.touched.offerdate && formik.errors.offerdate ? (
                      <div className="text-danger">
                        {formik.errors.offerdate}
                      </div>
                    ) : null}
                  </div>
                  <div className="col-lg-6">
                    {formik.touched.effectivedate &&
                    formik.errors.effectivedate ? (
                      <div className="text-danger">
                        {formik.errors.effectivedate}
                      </div>
                    ) : null}
                  </div>
                </div>

                 {/* Cadre and Designation Fields */}
  <div className="row mb-1">
    {["T", "L", "J", "O", "S"].includes(formik.values.transaction) && (
      <>
        <div className="d-flex align-items-center mb-1">
          <label
            htmlFor="cadre"
            className="col-form-label required fw-bold fs-6 me-2"
            style={{ minWidth: "150px" }}
          >
            Location:
          </label>

          <select
            id="cadre"
            className="form-select form-select-lg form-select-solid "
            name="cadre"
            onChange={formik.handleChange}
          >
            <option value="">{formik.values.cadre}</option>
            {location.map((state) => (
              <option value={state?.siteCode}>
                {state?.description}
              </option>
            ))}
          </select>
        </div>
        <div className="row mb-1">
          <div className="col-lg-6">
            {formik.touched.cadre && formik.errors.cadre ? (
              <div className="text-danger">{formik.errors.cadre}</div>
            ) : null}
          </div>
        </div>
      </>
    )}
  </div>
  {["T", "L", "J", "O", "S"].includes(formik.values.transaction) && (
              <>
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="section"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    Section:
                  </label>
                  <select
                    id="section"
                    className="form-select form-select-lg form-select-solid mb-1"
                    name="section"
                    onChange={formik.handleChange}
                    value={formik.values.section} // This ensures the selected section is displayed
                  >
                    {formik.values.section && (
                      <option value={formik.values.section}>
                        {formik.values.section}
                      </option>
                    )}
                    {section.length === 0 ? (
                      <option value="">No sections available</option>
                    ) : (
                      section.map((state) => (
                        <option
                          key={state.sectionCode}
                          value={state.sectionCode}
                        >
                          {state.menuDescr}
                        </option>
                      ))
                    )}
                  </select>
                </div>
                <div className="row mb-1">
                  <div className="col-lg-6">
                    {formik.touched.section && formik.errors.section ? (
                      <div className="text-danger">{formik.errors.section}</div>
                    ) : null}
                  </div>
                </div>
              </>
            )}

            {["Q", "Z", "R", "K", "E", "C", "B", "P", "L", "O", "S"].includes(
              formik.values.transaction
            ) && (
              <>
                <div className=" d-flex align-items-center mb-1">
                  <label
                    htmlFor="designation"
                    className="col-form-label fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    Designation:
                  </label>

                  <select
                    id="designation"
                    className="form-select form-select-lg form-select-solid "
                    name="designation"
                    onChange={formik.handleChange}
                  >
                    <option value="">{formik.values.designation}</option>
                    {designation.map((state) => (
                      <option value={state?.desigN_CODE}>
                        {state?.designation}
                      </option>
                    ))}
                  </select>
                </div>
              </>
            )}
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.designation && formik.errors.designation ? (
                  <div className="text-danger">{formik.errors.designation}</div>
                ) : null}
              </div>
            </div>

              </>
            )}
          </div>
        </div>
        <div className="text-center pt-15">
          {globalVariable === "infoJourneyDetails" ? (
            <button
              type="reset"
              onClick={closeForm}
              className="btn btn-light me-3"
            >
              Back
            </button>
          ) : (
            <button
              type="reset"
              onClick={closeForm}
              className="btn btn-light me-3"
            >
              Cancel
            </button>
          )}
          {globalVariable !== "infoJourneyDetails" && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isBankDetailsLoading ||
                formik.isSubmitting ||
                // !formik.isValid ||
                !formik.touched ||
                (globalVariable === "addJourneyDetails" && searchName === "")
              }
            >
              <span className="indicator-label">Submit</span>

              {(formik.isSubmitting || isBankDetailsLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isBankDetailsLoading) && (
        <JourneyDetailsListLoading />
      )}
    </>
  );
};

export { JourneyDetailsEditModalForm };
