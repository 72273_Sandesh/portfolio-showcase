import { KTCard } from "../../../../../_metronic/helpers";
import { Content } from "../../../../../_metronic/layout/components/content";
import { ToolbarWrapper } from "../../../../../_metronic/layout/components/toolbar";
import { Loader } from "../../../../../_metronic/utilities/Loader";
import { JourneyDetailsEditModal } from "./JourneyDetails-edit-modal/JourneyDetailsEditModal";
import { JourneyDetailsListHeader } from "./components/Header/JourneyDetailsListHeader";
import { ListViewProvider, useListView } from "./core/ListViewProvider";
import { QueryRequestProvider } from "./core/QueryRequestProvider";
import { QueryResponseProvider, useQueryResponseLoading } from "./core/QueryResponseProvider";
import { JourneyDetailsTable } from "./table/JourneyDetailsTable";

const JourneyDetailsList = () => {
  const { itemIdForUpdate } = useListView();
  const isLoading = useQueryResponseLoading()

  return (
    <>
      <KTCard>
        <JourneyDetailsListHeader />
        <JourneyDetailsTable />
      </KTCard>
      {itemIdForUpdate !== undefined && <JourneyDetailsEditModal />}
      
      {isLoading && <Loader />}
      
    </>
  );
};

const JourneyDetailsListWrapper = () => (
  <QueryRequestProvider>
    <QueryResponseProvider>
      <ListViewProvider>
        <ToolbarWrapper />
        <Content>
          <JourneyDetailsList />
        </Content>
      </ListViewProvider>
    </QueryResponseProvider>
  </QueryRequestProvider>
);

export { JourneyDetailsListWrapper };
