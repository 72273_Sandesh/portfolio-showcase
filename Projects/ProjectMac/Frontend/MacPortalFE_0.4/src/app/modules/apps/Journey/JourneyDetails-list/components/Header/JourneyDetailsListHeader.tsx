import { useListView } from "../../core/ListViewProvider";
import { JourneyDetailsListGrouping } from "./JourneyDetailsListGrouping";
import { JourneyDetailsListSearchComponent } from "./JourneyDetailsListSearchComponents";
import { JourneyDetailsListToolbar } from "./JourneyDetailsListToolbar";

const JourneyDetailsListHeader = () => {
  const { selected } = useListView();
  return (
    <div className="card-header border-0 pt-6">
      <JourneyDetailsListSearchComponent />
      {/* begin::Card toolbar */}
      <div className="card-toolbar">
        {/* begin::Group actions */}
        {selected.length > 0 ? (
          <JourneyDetailsListGrouping />
        ) : (
          <JourneyDetailsListToolbar />
        )}
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  );
};

export { JourneyDetailsListHeader };
