import axios, { AxiosResponse } from "axios";
import { ID, Response } from "../../../../../../_metronic/helpers";
import { JourneyDetails, JourneyDetailsQueryResponse } from "./_models";

const BANKDETAILS_URL = import.meta.env.VITE_DEPT_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const getJourneyDetails = (
  query: string
): Promise<JourneyDetailsQueryResponse> => {
  return axios
    .get(`${EMPLOYEE_URL}/GetAllEmployees?${query}`)
    .then((d: AxiosResponse<JourneyDetailsQueryResponse>) => d.data);
};

const getJourneyDetailsById = (id: ID): Promise<JourneyDetails | undefined> => {
  return axios
    .get(`${EMPLOYEE_URL}/${id}/EMPPER`)
    .then((response: AxiosResponse<Response<JourneyDetails>>) => response.data)
    .then((data: Response<JourneyDetails>) => {
      return data[0];
    });
};

const createJourneyDetails = (
  bankDetails: JourneyDetails
): Promise<JourneyDetails | undefined> => {
  return axios
    .post(`${BANKDETAILS_URL}/InsertDepartment`, bankDetails)
    .then((response: AxiosResponse<Response<JourneyDetails>>) => response.data)
    .then((response: Response<JourneyDetails>) => response.data);
};

const updateJourneyDetails = (
  bankDetails: JourneyDetails
): Promise<JourneyDetails | undefined> => {
  return axios
    .put(
      `${BANKDETAILS_URL}/UpdateDepartment/${bankDetails.emP_CODE}`,
      bankDetails
    )
    .then((response: AxiosResponse<Response<JourneyDetails>>) => response.data)
    .then((response: Response<JourneyDetails>) => response.data);
};

const deleteJourneyDetails = (bankDetailsId: ID): Promise<void> => {
  return axios.delete(`${BANKDETAILS_URL}/${bankDetailsId}`).then(() => {});
};

const deleteSelectedJourneyDetails = (
  bankDetailsIds: Array<ID>
): Promise<void> => {
  const requests = bankDetailsIds.map((id) =>
    axios.delete(`${BANKDETAILS_URL}/${id}`)
  );
  return axios.all(requests).then(() => {});
};

export {
  getJourneyDetails,
  deleteJourneyDetails,
  deleteSelectedJourneyDetails,
  getJourneyDetailsById,
  createJourneyDetails,
  updateJourneyDetails,
};
