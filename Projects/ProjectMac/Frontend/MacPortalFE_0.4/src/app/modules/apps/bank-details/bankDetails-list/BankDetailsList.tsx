import { KTCard, Mode } from "../../../../../_metronic/helpers";
import { Content } from "../../../../../_metronic/layout/components/content";
import { ToolbarWrapper } from "../../../../../_metronic/layout/components/toolbar";
import { Loader } from "../../../../../_metronic/utilities/Loader";
import { BankDetailsEditModal } from "./bankDetails-edit-modal/BankDetailsEditModal";
import { BankDetailsListHeader } from "./components/header/BankDetailsListHeader";
import { ListViewProvider, useListView } from "./core/ListViewProvider";
import { QueryRequestProvider } from "./core/QueryRequestProvider";
import { QueryResponseProvider, useQueryResponseLoading } from "./core/QueryResponseProvider";
import { BankDetailsTable } from "./table/BankDetailsTable";

const BankDetailsList = () => {
  const { itemIdForUpdate, formMode } = useListView();
  const isLoading = useQueryResponseLoading()

  return (
    <>
      <KTCard>
        <div style={{ display: itemIdForUpdate ? "none" : "" }}>
          <BankDetailsListHeader />
        </div>
        <BankDetailsTable />
      </KTCard>
      {itemIdForUpdate !== undefined && formMode === Mode.VIEW && (
        <BankDetailsEditModal />
      )}
      
      {isLoading && <Loader />}
      
    </>
  );
};

const BankDetailsListWrapper = () => (
  <QueryRequestProvider>
    <QueryResponseProvider>
      <ListViewProvider>
        <ToolbarWrapper />
        <Content>
          <BankDetailsList />
        </Content>
      </ListViewProvider>
    </QueryResponseProvider>
  </QueryRequestProvider>
);

export { BankDetailsListWrapper };
