import {useListView} from '../../core/ListViewProvider'
import { BankDetailsListGrouping } from './BankDetailsListGrouping'
import { BankDetailsListSearchComponent } from './BankDetailsListSearchComponent'
import { BankDetailsListToolbar } from './BankDetailsListToolbar'

const BankDetailsListHeader = () => {
  const {selected} = useListView()
  return (
    <div className='card-header border-0 pt-6'>
      <BankDetailsListSearchComponent />
      {/* begin::Card toolbar */}
      <div className='card-toolbar'>
        {/* begin::Group actions */}
        {selected.length > 0 ? <BankDetailsListGrouping /> : <BankDetailsListToolbar />}
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export {BankDetailsListHeader}
