import axios, { AxiosResponse } from "axios";
import { ID, Response } from "../../../../../../_metronic/helpers";
import { BankDetails, BankDetailsQueryResponse } from "./_models";

const BANKDETAILS_URL = import.meta.env.VITE_DEPT_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;

const getBankDetails = (query: string , loggedInUser: string | number | undefined): Promise<BankDetailsQueryResponse> => {
  return axios
    .get(
    `${MASTERTX_URL}/GetAllTransaction?${query}&SessionempCode=${loggedInUser}`)
    .then((d: AxiosResponse<BankDetailsQueryResponse>) => d.data);
};

const getBankDetailsById = (id:ID): Promise<BankDetails | undefined> => {
  return axios
    .get(`${EMPLOYEE_URL}/${id}/EMPPER`)
    .then((response: AxiosResponse<Response<BankDetails>>) => response.data)
    .then((data: Response<BankDetails>) => {
      return data[0];
    });
};

const createBankDetails = (bankDetails: BankDetails): Promise<BankDetails | undefined> => {
  return axios
    .post(`${BANKDETAILS_URL}/InsertDepartment`, bankDetails)
    .then((response: AxiosResponse<Response<BankDetails>>) => response.data)
    .then((response: Response<BankDetails>) => response.data);
};

const updateBankDetails = (bankDetails: BankDetails): Promise<BankDetails | undefined> => {
  return axios
    .put(`${BANKDETAILS_URL}/UpdateDepartment/${bankDetails.emP_CODE}`, bankDetails)
    .then((response: AxiosResponse<Response<BankDetails>>) => response.data)
    .then((response: Response<BankDetails>) => response.data);
};

const deleteBankDetails = (bankDetailsId: ID): Promise<void> => {
  return axios.delete(`${BANKDETAILS_URL}/${bankDetailsId}`).then(() => {});
};

const deleteSelectedBankDetails = (bankDetailsIds: Array<ID>): Promise<void> => {
  const requests = bankDetailsIds.map((id) => axios.delete(`${BANKDETAILS_URL}/${id}`));
  return axios.all(requests).then(() => {});
};

export {
  getBankDetails,
  deleteBankDetails,
  deleteSelectedBankDetails,
  getBankDetailsById,
  createBankDetails,
  updateBankDetails,
};
