import React from 'react';

type Props = {
  wF_PROCESS_LEVEL: string | number | undefined;
};

const BankDetailsLastLoginCell: React.FC<Props> = ({ wF_PROCESS_LEVEL }) => {


  return (
    <div className=' fw-bolder d-flex align-items-right'>
         <div className=' fw-bolder d-flex align-items-right'>
      {wF_PROCESS_LEVEL != null  ? (
        <span>Level {wF_PROCESS_LEVEL}</span>
      ) : (
        <div className='badge badge-danger fw-bolder d-flex align-items-right'>No data</div>
      )}
    </div>
        </div>
    // <div className='badge badge-light fw-bolder d-flex align-items-right'>
    //   {formattedDate}
    // </div>
  );
};

export { BankDetailsLastLoginCell };
