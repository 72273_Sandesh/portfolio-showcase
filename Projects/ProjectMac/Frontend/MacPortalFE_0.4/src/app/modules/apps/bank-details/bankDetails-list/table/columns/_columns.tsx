import { Column } from "react-table";
import { BankDetailsInfoCell } from "./BankDetailsInfoCell";
import { BankDetailsCustomHeader } from "./BankDetailsCustomHeader";
import { BankDetails } from "../../core/_models";
import { BankDetailsActionsCell } from "./BankDetailsActionsCell";
import { BankDetailsLastLoginCell } from "./BankDetailsLastLoginCell";
import { BankDetailsChangeUser } from "./BankDetailsChangeUser";
import { BankDetailsTwoStepsCell } from "./BankDetailsTwoStepsCell";
const BankDetailsColumns: ReadonlyArray<Column<BankDetails>> = [
  {
    Header: (props) => (
      <BankDetailsCustomHeader
        tableProps={props}
        title="MASTER NAME"
        className="min-w-125px text-left "
      />
    ),
    id: "name",
    Cell: ({ ...props }) => (
      <BankDetailsInfoCell bankDetails={props.data[props.row.index]} id={props.data[props.row.index].id} />
    ),
  },
    {
    Header: (props) => (
      <BankDetailsCustomHeader
        tableProps={props}
        title="DESCRIPTION"
        className="min-w-125px text-left"
      />
    ),
    accessor: "neW_VALUE",
    Cell: ({ ...props }) => (
      <BankDetailsChangeUser
      neW_VALUE={props.data[props.row.index].neW_VALUE}
      />
    ),
  },
  {
    Header: (props) => (
      <BankDetailsCustomHeader
        tableProps={props}
        title="LEVEL"
        className="min-w-125px text-left"
      />
    ),
    id: "wF_PROCESS_LEVEL",
    Cell: ({ ...props }) => (
      <BankDetailsLastLoginCell
        wF_PROCESS_LEVEL={props.data[props.row.index].wF_PROCESS_LEVEL }
      />
    ),
  },
  {
    Header: (props) => (
      <BankDetailsCustomHeader
        tableProps={props}
        title="STATUS"
        className="min-w-125px text-left"
      />
    ),
    accessor: "status",
    Cell: ({ value }) => (
      <span>
        <div className=" fw-bolder d-flex align-items-right">
          <div className="d-flex align-items-right">
            {
              <div className="d-flex align-items-right">
                {value === "A" ? (
                  <div className="badge badge-light-success fw-bolder d-flex align-items-right">
                    ACTIVE
                  </div>
                ) : value === "N" ? (
                  <div className="badge badge-light-danger fw-bolder d-flex align-items-right">
                    INACTIVE
                  </div>
                ) : (
                  <div className="badge d-flex align-items-right">&nbsp;</div>
                )}
              </div>
            }
          </div>
        </div>
      </span>
    ),
  },
  {
    Header: (props) => (
      <BankDetailsCustomHeader
        tableProps={props}
        title="WF_PROCESS_LEVEL"
        className="min-w-125px text-left"
      />
    ),
    id: "deptHead",
    Cell: ({ ...props }) => (
      <BankDetailsTwoStepsCell
        deptHead={props.data[props.row.index].deptHead}
      />
    ),
  },
  {
    Header: (props) => (
      <BankDetailsCustomHeader
        tableProps={props}
        title="VISIBLITY"
        className="min-w-125px text-left"
      />
    ),
    id: "changeDate1",
    Cell: ({ ...props }) => (
      <BankDetailsLastLoginCell
        changeDate={props.data[props.row.index].changeDate ?? new Date()}
      />
    ),
  },
  {
    Header: (props) => (
      <BankDetailsCustomHeader
        tableProps={props}
        title="STATE"
        className="min-w-125px text-left"
      />
    ),
    id: "changeDate2",
    Cell: ({ ...props }) => (
      <BankDetailsLastLoginCell
        changeDate={props.data[props.row.index].changeDate ?? new Date()}
      />
    ),
  },
  {
    Header: (props) => (
      <BankDetailsCustomHeader
        tableProps={props}
        title="Actions"
        className="min-w-100px text-left"
      />
    ),
    id: "actions",
    Cell: ({ ...props }) => (
      <BankDetailsActionsCell id={props.data[props.row.index].id} />
    ),
  },
];

export { BankDetailsColumns };