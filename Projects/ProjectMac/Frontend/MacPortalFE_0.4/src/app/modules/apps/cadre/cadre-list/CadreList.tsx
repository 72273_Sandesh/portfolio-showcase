import {ListViewProvider, useListView} from './core/ListViewProvider'
import {QueryRequestProvider} from './core/QueryRequestProvider'
import {QueryResponseProvider, useQueryResponseLoading} from './core/QueryResponseProvider'
import {CadreListHeader} from './components/header/CadreListHeader'
import {CadreTable} from './table/CadreTable'
import {CadreEditModal} from './cadre-edit-modal/CadreEditModal'
import {KTCard} from '../../../../../_metronic/helpers'
import { ToolbarWrapper } from '../../../../../_metronic/layout/components/toolbar'
import { Content } from '../../../../../_metronic/layout/components/content'
import { Loader } from '../../../../../_metronic/utilities/Loader'

const CadreList = () => {
  const {itemIdForUpdate} = useListView()
  const isLoading = useQueryResponseLoading()
  return (
    <>
      <KTCard>
        <CadreListHeader />
        <CadreTable />
      </KTCard>
      {itemIdForUpdate !== undefined && <CadreEditModal />}
      
      {isLoading && <Loader />}
      
    </>
  )
}

const CadreListWrapper = () => (
  <QueryRequestProvider>
    <QueryResponseProvider>
      <ListViewProvider>
        <ToolbarWrapper />
        <Content>
          <CadreList />
        </Content>
      </ListViewProvider>
    </QueryResponseProvider>
  </QueryRequestProvider>
)

export {CadreListWrapper}
