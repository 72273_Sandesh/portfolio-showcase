import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { Cadre } from "../core/_models";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
import { CadreListLoading } from "../components/loading/CadreListLoading";
import { useQueryResponse } from "../../../transaction-master/master-list/core/QueryResponseProvider";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import { useQueryClient, useMutation } from "react-query";
import Tooltip, { TooltipProps, tooltipClasses } from "@mui/material/Tooltip";
import { styled } from "@mui/material/styles";
import { createTransaction } from "../../../transaction-master/master-list/core/_models";
import { insertTransaction } from "../../../transaction-master/master-list/core/_requests";

const API_URL = import.meta.env.VITE_CADRE_DROPDOWNS_API_URL;
const CADRE_URL = import.meta.env.VITE_CADRE_API_URL;

type Props = {
  isCadreLoading: boolean;
  cadre: Cadre;
};

interface cadCatDrpdwn {
  banD_CODE: string;
  descr: string;
}

const CadreEditModalForm: FC<Props> = ({ cadre, isCadreLoading }) => {
  const { setItemIdForUpdate, formMode } = useListView();
  const { itemIdForUpdate, setItemIdForUpdate: closeForm } = useTxListView();
  const [cadreValues, setCadreValues] = useState<Cadre>();
  const { refetch } = useQueryResponse();

  const [cadCatDrpdwns, setCadCatDrpdwns] = useState<cadCatDrpdwn[]>([]);
  const [privateIP, setPrivateIP] = useState();
  const [statuses] = useState([
    { code: "A", descr: "ACTIVE" },
    { code: "N", descr: "INACTIVE" },
  ]);

  const { currentUser } = useAuth();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();

  const [isDataFetched, setIsDataFetched] = useState(false);

  const editCadreSchema = Yup.object().shape({
    sH_DESCR: Yup.string()
      .trim()
      .required("Cadre Short Description is required"),
    descr: Yup.string().trim().required("Cadre Description is required"),
    banD_CODE: Yup.string().trim().required("Cadre Category is required"),
    status: Yup.string().trim().required("Cadre Status is required"),
    chG_REASON: Yup.string().when([], {
      is: () => formMode !== Mode.ADD,
      then: (schema) =>
        schema
          .trim()
          .required("Change Reason is required")
          .min(5, "Minimum 5 characters"),
      otherwise: (schema) => schema.notRequired(),
    }),
  });

  useEffect(() => {
    axios
      .get(API_URL)
      .then((response) => {
        setCadCatDrpdwns(response.data.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const [cadreForEdit] = useState<Cadre>({
    ...cadre,
    cadrE_CODE: cadre.cadrE_CODE || cadre.fieldNames?.cadrE_CODE?.neW_KEY,
    descr: cadre.descr || cadre.fieldNames?.DESCR?.neW_KEY,
    sH_DESCR: cadre.sH_DESCR || cadre.fieldNames?.sH_DESCR?.neW_KEY,
    chG_TERM:
      cadre.tX_CHG_TERM ||
      cadre.chG_TERM ||
      cadre.fieldNames?.chG_TERM?.neW_KEY,
    chG_DATE:
      cadre.tX_CHG_DATE ||
      cadre.chG_DATE ||
      cadre.fieldNames?.chG_DATE?.neW_KEY,
    chG_USER:
      cadre.tX_CHG_USER ||
      cadre.chG_USER ||
      cadre.fieldNames?.chG_USER?.neW_KEY,
    banD_CODE: cadre.banD_CODE || cadre.fieldNames?.banD_CODE?.neW_KEY,
    cadreCategory: cadre.cadreCategory || cadre.fieldNames?.banD_CODE?.neW_KEY,
    status: cadre.status || cadre.fieldNames?.status?.neW_KEY,
    chG_REASON: formMode === Mode.VIEW ? cadre.chG_REASON : "",
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
    closeForm(undefined);
  };

  useEffect(() => {
    if (itemIdForUpdate && !isDataFetched) {
      axios
        .get(
          `${CADRE_URL}/GetCadreByCadreCode/${cadre.fieldNames?.cadrE_CODE?.neW_KEY}`
        )
        .then((response) => {
          const data = response.data.data;

          setCadreValues(data);
          formik.setValues({
            cadrE_CODE:
              cadre.fieldNames?.cadrE_CODE?.neW_KEY || data.cadrE_CODE,
            descr: cadre.fieldNames?.DESCR?.neW_KEY || data.descr,
            sH_DESCR: cadre.fieldNames?.sH_DESCR?.neW_KEY || data.sH_DESCR,
            chG_TERM: cadre.fieldNames?.chG_TERM?.neW_KEY || data.chG_TERM,
            chG_DATE: cadre.fieldNames?.chG_DATE?.neW_KEY || data.chG_DATE,
            chG_USER: cadre.fieldNames?.chG_USER?.neW_KEY || data.chG_USER,
            banD_CODE: cadre.fieldNames?.banD_CODE?.neW_KEY || data.banD_CODE,
            cadreCategory:
              cadre.fieldNames?.banD_CODE?.neW_VALUE || data.cadreCategory,
            status: cadre.fieldNames?.status?.neW_KEY || data.status,
            // remarks:
            //   formMode === Mode.VIEW ? cadre.chG_REASON || cadre.remarks : "",
          });
          setIsDataFetched(true);
        })
        .catch((error) =>
          console.error("Error fetching combined data:", error)
        );
    }
  }, [itemIdForUpdate, isDataFetched, cadre]);

  const createTx = useMutation((payload: any) => insertTransaction(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        closeForm(undefined);
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
        cancel(true);
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formattedDate = (date: Date) => {
    return new Date(date).toLocaleString("en-GB", {
      day: "2-digit",
      month: "2-digit",
      year: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    });
  };

  const formik = useFormik({
    initialValues: cadreForEdit,
    validationSchema: editCadreSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      const payload: createTransaction = {
        tableName: "CADRE",
        keyVar: "",
        wfLevel: 1,
        conf: "N",
        siteCode: "S0001",
        emp: currentUser?.id?.toString() || "",
        filePath: null,
        file: null,
        strTranId: cadre.fieldNames ? itemIdForUpdate : null,
        folderPath: null,
        tX_CHG_USER: currentUser?.id?.toString() || "",
        tX_CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        remarks: values.chG_REASON || "NEW CADRE ADDED",

        fieldUpdates: [
          {
            SEQ_NO: "1",
            FIELD_NAME: "cadrE_CODE",
            FIELD_DESCRIPTION: "CADRE CODE",
            OLD_KEY:
              cadre.cadrE_CODE || cadre.fieldNames?.cadrE_CODE?.olD_KEY || null,
            OLD_VALUE:
              cadre.cadrE_CODE ||
              cadre.fieldNames?.cadrE_CODE?.olD_VALUE ||
              null,
            NEW_KEY:
              cadre.cadrE_CODE ||
              cadre.fieldNames?.cadrE_CODE?.olD_VALUE ||
              null,
            NEW_VALUE:
              cadre.cadrE_CODE ||
              cadre.fieldNames?.cadrE_CODE?.olD_VALUE ||
              null,
            FILE_PATH: "",
          },
          ...((itemIdForUpdate !== undefined &&
            cadre.fieldNames?.DESCR?.olD_KEY === values.descr) ||
          (cadre.descr !== values.descr && cadreValues?.descr !== values.descr)
            ? [
                {
                  SEQ_NO: "2",
                  FIELD_NAME: "DESCR",
                  FIELD_DESCRIPTION: "DESCRIPTION",
                  OLD_KEY:
                    cadre.descr ||
                    cadre.fieldNames?.DESCR?.olD_KEY ||
                    cadreValues?.descr ||
                    "",
                  OLD_VALUE:
                    cadre.descr ||
                    cadre.fieldNames?.DESCR?.olD_VALUE ||
                    cadreValues?.descr ||
                    "",
                  NEW_KEY: values.descr,
                  NEW_VALUE: values.descr,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            cadre.fieldNames?.sH_DESCR?.olD_KEY === values.sH_DESCR) ||
          (cadre.sH_DESCR !== values.sH_DESCR &&
            cadreValues?.sH_DESCR !== values.sH_DESCR)
            ? [
                {
                  SEQ_NO: "3",
                  FIELD_NAME: "sH_DESCR",
                  FIELD_DESCRIPTION: "SHORT DESCRIPTION",
                  OLD_KEY:
                    cadre.sH_DESCR ||
                    cadre.fieldNames?.sH_DESCR?.olD_KEY ||
                    cadreValues?.sH_DESCR ||
                    "",
                  OLD_VALUE:
                    cadre.sH_DESCR ||
                    cadre.fieldNames?.sH_DESCR?.olD_VALUE ||
                    cadreValues?.sH_DESCR ||
                    "",
                  NEW_KEY: values.sH_DESCR,
                  NEW_VALUE: values.sH_DESCR,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            cadre.fieldNames?.banD_CODE?.olD_KEY === values.banD_CODE) ||
          (cadre.banD_CODE !== values.banD_CODE &&
            cadreValues?.banD_CODE !== values.banD_CODE)
            ? [
                {
                  SEQ_NO: "4",
                  FIELD_NAME: "banD_CODE",
                  FIELD_DESCRIPTION: "CADRE CATEGORY",
                  OLD_KEY:
                    cadre.banD_CODE ||
                    cadre.fieldNames?.banD_CODE?.olD_KEY ||
                    cadreValues?.banD_CODE ||
                    "",
                  OLD_VALUE:
                    cadre.cadreCategory ||
                    cadre.fieldNames?.banD_CODE?.olD_VALUE ||
                    cadreValues?.cadreCategory ||
                    "",
                  NEW_KEY: values.banD_CODE,
                  NEW_VALUE: values.cadreCategory,
                  FILE_PATH: "",
                },
              ]
            : []),

          ...((itemIdForUpdate !== undefined &&
            cadre.fieldNames?.status?.olD_KEY === values.status) ||
          (cadre.status !== values.status &&
            cadreValues?.status !== values.status)
            ? [
                {
                  SEQ_NO: "5",
                  FIELD_NAME: "status",
                  FIELD_DESCRIPTION: "STATUS",
                  OLD_KEY:
                    cadre.status ||
                    cadre.fieldNames?.status?.olD_KEY ||
                    cadreValues?.status ||
                    "",
                  OLD_VALUE:
                    cadre.status === "A"
                      ? "ACTIVE"
                      : cadre.status === "N"
                      ? "INACTIVE"
                      : "" || cadre.fieldNames?.status?.olD_KEY === "A"
                      ? "ACTIVE"
                      : cadre.fieldNames?.status?.olD_KEY === "N"
                      ? "INACTIVE"
                      : "" || cadreValues?.status === "A"
                      ? "ACTIVE"
                      : cadreValues?.status === "N"
                      ? "INACTIVE"
                      : "",
                  NEW_KEY: values.status,
                  NEW_VALUE:
                    values.status === "A"
                      ? "ACTIVE"
                      : values.status === "N"
                      ? "INACTIVE"
                      : "",
                  FILE_PATH: "",
                },
              ]
            : []),

          {
            SEQ_NO: "6",
            FIELD_NAME: "chG_USER",
            FIELD_DESCRIPTION: "CHANGE USER",
            OLD_KEY:
              cadre.chG_USER ||
              cadre.fieldNames?.chG_USER?.olD_KEY ||
              cadreValues?.chG_USER ||
              "",
            OLD_VALUE:
              cadre.chG_USER ||
              cadre.fieldNames?.chG_USER?.olD_VALUE ||
              cadreValues?.chG_USER ||
              "",
            NEW_KEY: currentUser?.id?.toString() || "",
            NEW_VALUE: currentUser?.emp_Name || "",
            FILE_PATH: "",
          },
          {
            SEQ_NO: "7",
            FIELD_NAME: "chG_TERM",
            FIELD_DESCRIPTION: "CHANGE TERM",
            OLD_KEY:
              cadre.chG_TERM ||
              cadre.fieldNames?.chG_TERM?.olD_KEY ||
              cadreValues?.chG_TERM ||
              "",
            OLD_VALUE:
              cadre.chG_TERM ||
              cadre.fieldNames?.chG_TERM?.olD_VALUE ||
              cadreValues?.chG_TERM ||
              "",
            NEW_KEY: privateIP,
            NEW_VALUE: privateIP,
            FILE_PATH: "",
          },
          {
            SEQ_NO: "8",
            FIELD_NAME: "chG_DATE",
            FIELD_DESCRIPTION: "CHANGE DATE",
            OLD_KEY:
              cadre.chG_DATE ||
              cadre.fieldNames?.chG_DATE?.olD_KEY ||
              cadreValues?.chG_DATE ||
              "",
            OLD_VALUE:
              (cadre.chG_DATE && formattedDate(cadre.chG_DATE)) ||
              (cadreValues?.chG_DATE && formattedDate(cadreValues?.chG_DATE)) ||
              (cadre.fieldNames?.chG_DATE?.olD_VALUE &&
                formattedDate(
                  cadre.chG_DATE || cadre.fieldNames?.chG_DATE?.olD_VALUE
                )) ||
              "",
            NEW_KEY: new Date(),
            NEW_VALUE: formattedDate(new Date()),
            FILE_PATH: "",
          },
          {
            SEQ_NO: "9",
            FIELD_NAME: "CHG_REASON",
            FIELD_DESCRIPTION: "CHANGE REASON",
            OLD_KEY:
              cadre.chG_REASON ||
              cadre.fieldNames?.CHG_REASON?.olD_KEY ||
              cadreValues?.chG_REASON ||
              "",
            OLD_VALUE:
              cadre.chG_REASON ||
              cadre.fieldNames?.CHG_REASON?.olD_KEY ||
              cadreValues?.chG_REASON ||
              "",
            NEW_KEY: values.chG_REASON || "NEW CADRE ADDED",
            NEW_VALUE: values.chG_REASON || "NEW CADRE ADDED",
            FILE_PATH: "",
          },
        ],
      };
      createTx.mutate(payload);

      setSubmitting(false);
    },
  });

  useEffect(() => {
    if (formMode === Mode.ADD) {
      formik.setFieldValue("status", "A");
    }
  }, []);

  function formatDate(dateString: string) {
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata",
    });
    return `${formattedDate} ${formattedTime}`;
  }

  const LightTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: theme.palette.common.white,
      color: "rgba(0, 0, 0, 0.87)",
      boxShadow: theme.shadows[1],
      fontSize: 15,
    },
  }));

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column" id="kt_modal_add_user_scroll">
          {/* Begin: Edit Row */}
          <div className="row mb-1">
            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="department"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DESCRIPTION
                </label>
                <input
                  placeholder="Description"
                  {...formik.getFieldProps("descr")}
                  onChange={(e) =>
                    formik.setFieldValue("descr", e.target.value.toUpperCase())
                  }
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "bg-light-warning": cadre.fieldNames?.DESCR?.neW_VALUE,
                    },
                    {
                      "is-invalid": formik.touched.descr && formik.errors.descr,
                    },
                    {
                      "is-valid": formik.touched.descr && !formik.errors.descr,
                    }
                  )}
                  type="text"
                  name="descr"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isCadreLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(cadre.fieldNames?.DESCR && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      cadre.fieldNames?.DESCR?.olD_VALUE === null
                        ? "-"
                        : cadre.fieldNames?.DESCR?.olD_VALUE
                    }`,
                  })}
                />
              </div>
              {formik.touched.descr && formik.errors.descr && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.descr}</span>
                </div>
              )}
            </div>

            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="worK_LOCATION"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SHORT DESCR.
                </label>
                <input
                  placeholder="Short Description"
                  {...formik.getFieldProps("sH_DESCR")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "sH_DESCR",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "bg-light-warning": cadre.fieldNames?.sH_DESCR?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.sH_DESCR && formik.errors.sH_DESCR,
                    },
                    {
                      "is-valid":
                        formik.touched.sH_DESCR && !formik.errors.sH_DESCR,
                    }
                  )}
                  type="text"
                  name="sH_DESCR"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isCadreLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(cadre.fieldNames?.sH_DESCR && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      cadre.fieldNames?.sH_DESCR?.olD_VALUE === null
                        ? "-"
                        : cadre.fieldNames?.sH_DESCR?.olD_VALUE
                    }`,
                  })}
                />
              </div>
              {formik.touched.sH_DESCR && formik.errors.sH_DESCR && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.sH_DESCR}</span>
                </div>
              )}
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="banD_CODE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CADRE CATEGORY
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        cadre.fieldNames?.banD_CODE?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.banD_CODE && formik.errors.banD_CODE,
                    },
                    {
                      "is-valid":
                        formik.touched.banD_CODE && !formik.errors.banD_CODE,
                    }
                  )}
                  {...formik.getFieldProps("banD_CODE")}
                  onChange={(e) => {
                    const selectedValue = e.target.value;
                    const selectedOption = cadCatDrpdwns.find(
                      (cadCatDrpdwn) => cadCatDrpdwn.banD_CODE === selectedValue
                    );

                    formik.setFieldValue("banD_CODE", selectedValue);
                    formik.setFieldValue(
                      "cadreCategory",
                      selectedOption?.descr || ""
                    );
                  }}
                  disabled={
                    formik.isSubmitting ||
                    isCadreLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(cadre.fieldNames?.banD_CODE && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      cadre.fieldNames?.banD_CODE?.olD_VALUE === null
                        ? "-"
                        : cadre.fieldNames?.banD_CODE?.olD_VALUE
                    }`,
                  })}
                >
                  <option value=""></option>
                  {cadCatDrpdwns.map((cadCatDrpdwn) => (
                    <option
                      key={cadCatDrpdwn.banD_CODE}
                      value={cadCatDrpdwn.banD_CODE}
                    >
                      {cadCatDrpdwn.descr}
                    </option>
                  ))}
                </select>
              </div>
              {formik.touched.banD_CODE && formik.errors.banD_CODE && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.banD_CODE}</span>
                </div>
              )}
            </div>

            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="status"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  STATUS
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    { "bg-light-warning": cadre.fieldNames?.status?.neW_VALUE },
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  {...formik.getFieldProps("status")}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                  }}
                  disabled={formMode === Mode.ADD || formMode === Mode.VIEW}
                  {...(cadre.fieldNames?.status && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      cadre.fieldNames?.status?.olD_VALUE === null
                        ? "-"
                        : cadre.fieldNames?.status?.olD_VALUE
                    }`,
                  })}
                >
                  <option value=""></option>
                  {statuses.map((status) => (
                    <option value={status.code}>{status.descr}</option>
                  ))}
                </select>
              </div>
              {formik.touched.status && formik.errors.status && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.status}</span>
                </div>
              )}
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            {formMode === Mode.VIEW && (
              <>
                <div className="col-lg-6">
                  <div className="d-flex align-items-center mb-1">
                    <label
                      htmlFor="department"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CHANGE TERM
                    </label>
                    <input
                      {...formik.getFieldProps("chG_TERM")}
                      className="form-control form-control-solid pl-3 mb-3"
                      type="text"
                      name="chG_TERM"
                      autoComplete="off"
                      disabled={
                        formik.isSubmitting ||
                        isCadreLoading ||
                        formMode === Mode.VIEW
                      }
                    />
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="d-flex align-items-center mb-1">
                    <label
                      htmlFor="worK_LOCATION"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CHANGE USER
                    </label>
                    <input
                      placeholder=""
                      {...formik.getFieldProps("chG_USER")}
                      className="form-control form-control-solid pl-3 mb-3"
                      type="text"
                      name="chG_USER"
                      autoComplete="off"
                      disabled={
                        formik.isSubmitting ||
                        isCadreLoading ||
                        formMode === Mode.VIEW
                      }
                    />
                  </div>
                </div>
              </>
            )}
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            {formMode === Mode.VIEW && (
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="department"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE DATE
                </label>
                <input
                  {...formik.getFieldProps("chG_DATE")}
                  className="form-control form-control-solid pl-3 mb-3"
                  type="text"
                  name="chG_DATE"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isCadreLoading ||
                    formMode === Mode.VIEW
                  }
                  value={
                    formik.values.chG_DATE
                      ? formatDate(formik.values?.chG_DATE.toString())
                      : ""
                  }
                />
              </div>
            )}

            {formMode !== Mode.ADD && (
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="chG_REASON"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CHANGE REASON
                  </label>
                  <input
                    placeholder="Change Reason"
                    {...formik.getFieldProps("chG_REASON")}
                    onChange={(e) =>
                      formik.setFieldValue(
                        "chG_REASON",
                        e.target.value.toUpperCase()
                      )
                    }
                    className={clsx(
                      "form-control form-control-solid pl-3 mb-3",
                      {
                        "is-invalid":
                          formik.touched.chG_REASON && formik.errors.chG_REASON,
                      },
                      {
                        "is-valid":
                          formik.touched.chG_REASON &&
                          !formik.errors.chG_REASON,
                      }
                    )}
                    type="text"
                    name="chG_REASON"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isCadreLoading ||
                      formMode === Mode.VIEW
                    }
                  />
                </div>
                {formik.touched.chG_REASON && formik.errors.chG_REASON && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.chG_REASON}</span>
                  </div>
                )}
              </div>
            )}
          </div>
          {/* End: Edit Row */}
        </div>
        <div className="text-center pt-15">
          {formMode !== Mode.VIEW && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isCadreLoading ||
                formik.isSubmitting ||
                !formik.touched ||
                !formik.isValid
              }
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isCadreLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isCadreLoading) && <CadreListLoading />}
    </>
  );
};

export { CadreEditModalForm };
