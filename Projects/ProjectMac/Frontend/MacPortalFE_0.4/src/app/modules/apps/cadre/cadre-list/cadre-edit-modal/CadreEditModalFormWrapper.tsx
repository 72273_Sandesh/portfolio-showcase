import {useQuery} from 'react-query'
import {CadreEditModalForm} from './CadreEditModalForm'
import {isNotEmpty, QUERIES} from '../../../../../../_metronic/helpers'
import {useListView} from '../core/ListViewProvider'
import {getCadreById} from '../core/_requests'

const CadreEditModalFormWrapper = () => {
  const {itemIdForUpdate, setItemIdForUpdate} = useListView()
  const enabledQuery: boolean = isNotEmpty(itemIdForUpdate)
  const {
    isLoading,
    data: cadre,
    error,
  } = useQuery(
    `${QUERIES.CADRE_LIST}-cadre-${itemIdForUpdate}`,
    () => {
      return getCadreById(itemIdForUpdate)
    },
    {
      cacheTime: 0,
      enabled: enabledQuery,
      onError: (err) => {
        setItemIdForUpdate(undefined)
        console.error(err)
      },
    }
  )

  if (!itemIdForUpdate) {
    return <CadreEditModalForm isCadreLoading={isLoading} cadre={{id: undefined}} />
  }

  if (!isLoading && !error && cadre) {
    return <CadreEditModalForm isCadreLoading={isLoading} cadre={cadre} />
  }

  return null
}

export {CadreEditModalFormWrapper}
