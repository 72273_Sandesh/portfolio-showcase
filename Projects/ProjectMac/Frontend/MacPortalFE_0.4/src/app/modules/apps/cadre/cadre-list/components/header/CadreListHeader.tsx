import {useListView} from '../../core/ListViewProvider'
import {CadreListToolbar} from './CadreListToolbar'
import {CadreListGrouping} from './CadreListGrouping'
import {CadreListSearchComponent} from './CadreListSearchComponent'


const CadreListHeader = () => {
  const {selected} = useListView()
  return (
    <div className='card-header border-0 pt-6'>
      <CadreListSearchComponent />
      {/* begin::Card toolbar */}
      <div className='card-toolbar'>
        {/* begin::Group actions */}
        {selected.length > 0 ? <CadreListGrouping /> : <CadreListToolbar />}
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export {CadreListHeader}
