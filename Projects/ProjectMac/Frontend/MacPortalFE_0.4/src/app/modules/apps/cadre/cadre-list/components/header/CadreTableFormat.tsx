import { useQueryResponseData } from "../../core/QueryResponseProvider";

const CadreTableFormat = () => {
  const cadres = useQueryResponseData(); //<Array<Department>>

  return (
    <table id="table-to-export">
      <thead>
        <tr>
          <th>Cadre Id</th>
          <th>Cadre Code</th>
          <th>Cadre Name</th>
          <th>Description</th>
          <th>lvE_TBLNO</th>
          <th>Change Date</th>
          <th>Change User</th>
          <th>Change Term</th>
          <th>UDF Number</th>
          <th>Cadre Id</th>
          <th>Cadre Order</th>
          <th>Band Code</th>
          <th>mkit</th>
          <th>mmed</th>
          <th>Change Reason</th>
          <th>Cadre Category</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        {cadres.map((cadre) => (
          <tr key={cadre.id}>
            <td>{cadre.id}</td>
            <td>{cadre.cadrE_CODE}</td>
            <td>{cadre.descr}</td>
            <td>{cadre.sH_DESCR}</td>
            <td>{cadre.lvE_TBLNO}</td>
            <td>{cadre.chG_DATE?.toString()}</td>
            <td>{cadre.chG_USER}</td>
            <td>{cadre.chG_TERM}</td>
            <td>{cadre.udF_NUM1}</td>
            <td>{cadre.cadrE_ID}</td>
            <td>{cadre.cadrE_ORDER}</td>
            <td>{cadre.banD_CODE}</td>
            <td>{cadre.mkit}</td>
            <td>{cadre.mmed}</td>
            <td>{cadre.chG_REASON}</td>
            <td>{cadre.cadreCategory}</td>
            <td>{cadre.status}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default CadreTableFormat;
