import { ID, Response } from "../../../../../../_metronic/helpers";
export type Cadre = {
  //previous cadre fields
  id?: ID;
  cadrE_CODE?: string;
  descr?: string;
  sH_DESCR?: string;
  lvE_TBLNO?: string;
  chG_DATE?: Date | string;
  chG_USER?: string;
  chG_TERM?: string;
  udF_NUM1?: number | string | null;
  cadrE_ID?: string;
  cadrE_ORDER?: number;
  banD_CODE?: string;
  mkit?: number;
  mmed?: number;
  chG_REASON?: string;
  cadreCategory?: string;
  status?: string;
  // statusDescr?: string;
  responseStatus?: string;
  message?: string;
  fieldName?: string;
  //new fields used for tx
  wF_PROCESS_LEVEL?: number;
  emP_CODE?: string;
  remarks?: string;
  SessionempCode?: string;
  moduleType?: string;
  tX_CHG_DATE?: string|Date;
  tX_CHG_USER?: string;
  tX_CHG_TERM?: string;

  fieldNames?: {
    cadrE_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    DESCR: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    sH_DESCR: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    banD_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    chG_USER?: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    chG_TERM?: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    chG_DATE?: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    status: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_REASON: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
  };
};

export type CadreQueryResponse = Response<Array<Cadre>>;

export const initialCadre: Cadre = {};
