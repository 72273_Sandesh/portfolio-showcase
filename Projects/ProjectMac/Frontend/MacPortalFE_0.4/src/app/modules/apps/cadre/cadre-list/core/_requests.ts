import axios, { AxiosResponse } from "axios";
import { ID, Response } from "../../../../../../_metronic/helpers";
import { Cadre, CadreQueryResponse} from "./_models";
//for sake of refrence we put here each api from backend

const API_URL = import.meta.env.VITE_CADRE_API_URL;
const CADRE_URL = `${API_URL}`;

//https://localhost:44360/api/Cadre/GetAllCadres/query?page=1&items_per_page=10
const getCadre = (query: string): Promise<CadreQueryResponse> => {
  return axios
    .get(`${CADRE_URL}/GetAllCadres/query?${query}`)
    .then((d: AxiosResponse<CadreQueryResponse>) => d.data);
};

//https://localhost:44360/api/Cadre/GetCadreByCadreCode/L0007
const getCadreById = (id:ID): Promise<Cadre | undefined> => {
  return axios
    .get(`${CADRE_URL}/GetCadreByCadreCode/${id}`)
    .then((response: AxiosResponse<Response<Cadre>>) => response.data)
    .then((response: Response<Cadre>) => response.data);
};

//https://localhost:44360/api/Cadre/createCadre
const createCadre = (cadre: Cadre): Promise<Cadre | undefined> => {
  return axios
    .post(`${CADRE_URL}/createCadre`, cadre)
    .then((response: AxiosResponse<Response<Cadre>>) => response.data)
    .then((response: Response<Cadre>) => response.data);
};

//https://localhost:44360/api/Cadre/EditCadre
const updateCadre = (cadre: Cadre): Promise<Cadre | undefined> => {
  return axios
    .put(`${CADRE_URL}/EditCadre/${cadre.id}`, cadre)
    .then((response: AxiosResponse<Response<Cadre>>) => response.data)
    .then((response: Response<Cadre>) => response.data);
    
};

//we are not supposed to use delete action in our code 
const deleteCadre = (departmentId: ID): Promise<void> => {
  return axios.delete(`${CADRE_URL}/${departmentId}`).then(() => {});
};

const deleteSelectedCadre = (departmentIds: Array<ID>): Promise<void> => {
  const requests = departmentIds.map((id) => axios.delete(`${CADRE_URL}/${id}`));
  return axios.all(requests).then(() => {});
};

export {
  getCadre,
  deleteCadre,
  deleteSelectedCadre,
  getCadreById,
  createCadre,
  updateCadre,
};
