import { Column } from "react-table";
import { CadreInfoCell } from "./CadreInfoCell";
import { CadreLastLoginCell } from "./CadreLastLoginCell";
import { CadreTwoStepsCell } from "./CadreTwoStepsCell";
import { CadreActionsCell } from "./CadreActionsCell";
import { CadreCustomHeader } from "./CadreCustomHeader";
import { Cadre } from "../../core/_models";
import { CadreUpperCell } from "./CadreUpperCell";
import { CadreShortDescription } from "./CadreShortDescription";
import { CadreStatusCell } from "./CadreStatusCell";

const cadreColumns: ReadonlyArray<Column<Cadre>> = [
  {
    Header: (props) => (
      <CadreCustomHeader
        tableProps={props}
        title="Cadre Name"
        className="min-w-125px text-left "
      />
    ),
    id: "descr",
    Cell: ({ ...props }) => (
      <CadreInfoCell cadre={props.data[props.row.index]} id={props.data[props.row.index].id}/>
    ),
  },
  {
    Header: (props) => (
      <CadreCustomHeader
        tableProps={props}
        title="Short Description"
        className="min-w-125px"
      />
    ),
    id: "sH_DESCR",
    Cell: ({ ...props }) => (
      <CadreShortDescription sH_DESCR={props.data[props.row.index].sH_DESCR} />
    ),
  },
  {
    Header: (props) => (
      <CadreCustomHeader
        tableProps={props}
        title="Category"
        className="min-w-125px text-left "
      />
    ),
    id: "cadreCategory",
    Cell: ({ ...props }) => (
      <CadreTwoStepsCell cadre={props.data[props.row.index]} />
    ),
  },
  {
    Header: (props) => (
      <CadreCustomHeader
        tableProps={props}
        title="Change User"
        className="min-w-125px"
      />
    ),
    id: "chG_USER",
    Cell: ({ ...props }) => (
      <CadreUpperCell chG_USER={props.data[props.row.index].chG_USER} />
    ),
  },
  {
    Header: (props) => (
      <CadreCustomHeader
        tableProps={props}
        title="Change Date"
        className="min-w-125px text-left"
      />
    ),
    id: "chG_DATE",
    Cell: ({ ...props }) => {
      const chG_DATE = props.data[props.row.index].chG_DATE ?? new Date();
      return <CadreLastLoginCell chG_DATE={chG_DATE} />;
    },
  },

  {
    Header: (props) => (
      <CadreCustomHeader
        tableProps={props}
        title="status"
        className="min-w-125px"
      />
    ),
    id: "status",
    Cell: ({ ...props }) => (
      <CadreStatusCell status={props.data[props.row.index].status} />
    ),
  },
  {
    Header: (props) => (
      <CadreCustomHeader
        tableProps={props}
        title="Actions"
        className="min-w-100px text-left"
      />
    ),
    id: "actions",
    Cell: ({ ...props }) => (
      <CadreActionsCell id={props.data[props.row.index].id} />
    ),
  },
];

export { cadreColumns };
