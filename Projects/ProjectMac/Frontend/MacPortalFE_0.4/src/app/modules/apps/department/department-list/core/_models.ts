import { ID, Response } from "../../../../../../_metronic/helpers";
export type Department = {
  id?: ID;
  deptCode?: string;
  dept_Code?: string;
  name?: string;
  changeUser?: string;
  deptHead?: string;
  changeDate?: Date | string;
  shortDescription?: string;
  changeTerm?: string;
  status?: string;
  acctCode?: string;
  payTable?: string;
  cctrCode?: string;
  acctCodeAp?: string;
  cctrCodeAp?: string;
  udfNumber1?: number;
  shDeptCode?: string;
  changeReason?: string | null;
  newReason?: string | null;
  deptHeadName?: string;
  responseStatus?: string;
  message?: string;

  wF_PROCESS_LEVEL?: number;
  emP_CODE?: string;
  remarks?: string;
  SessionempCode?: string;
  moduleType?: string;
  tX_CHG_DATE?: string|Date;
  tX_CHG_USER?: string;
  tX_CHG_TERM?: string;
  
  fieldNames?: {
    DEPT_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    DESCR: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    SH_DESCR: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    DEPT_HEAD: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_USER: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_TERM: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_DATE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    STATUS: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_REASON: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
  };
};

export type DepartmentQueryResponse = Response<Array<Department>>;

export const initialDepartment: Department = {
  //sH_DESCR?: string,
  //chG_DATE?: Date,
  //chG_USER?: string,
  //chG_TERM?: string,
  //depT_HEAD?: string,
  // accT_CODE?: string,
  // paY_TABLE?: string,
  // cctR_CODE?:string,
  // sH_DESCR?: string,
  // accT_CODE__AP?: string,
  // cctR_CODE__AP?: string,
  // udF_NUM1?:number,
  // sH_DEPT_CODE?: string
};
