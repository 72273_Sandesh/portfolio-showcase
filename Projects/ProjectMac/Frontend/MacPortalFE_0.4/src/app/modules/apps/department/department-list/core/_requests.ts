import axios, { AxiosResponse } from "axios";
import { ID, Response } from "../../../../../../_metronic/helpers";
import { Department, DepartmentQueryResponse } from "./_models";

const DEPARTMENT_URL = import.meta.env.VITE_DEPT_API_URL;
//https://localhost:44360/api/Department/GetDepartmentByDeptCode/0012


const getDepartment = (query: string): Promise<DepartmentQueryResponse> => {
  return axios
    .get(`${DEPARTMENT_URL}/GetAllDepartments/query?${query}`)
    .then((d: AxiosResponse<DepartmentQueryResponse>) => d.data);
};

const getDepartmentById = (id:ID): Promise<Department | undefined> => {
  return axios
    .get(`${DEPARTMENT_URL}/GetDepartmentByDeptCode/${id}`)
    .then((response: AxiosResponse<Response<Department>>) => response.data)
    .then((response: Response<Department>) => response.data);
};

const createDepartment = (department: Department): Promise<Department | undefined> => {
  return axios
    .post(`${DEPARTMENT_URL}/InsertDepartment`, department)
    .then((response: AxiosResponse<Response<Department>>) => response.data)
    .then((response: Response<Department>) => response.data);
};

const updateDepartment = (department: Department): Promise<Department | undefined> => {
  return axios
    .put(`${DEPARTMENT_URL}/UpdateDepartment/${department.id}`, department)
    .then((response: AxiosResponse<Response<Department>>) => response.data)
    .then((response: Response<Department>) => response.data);
};

const deleteDepartment = (departmentId: ID): Promise<void> => {
  return axios.delete(`${DEPARTMENT_URL}/${departmentId}`).then(() => {});
};

const deleteSelectedDepartment = (departmentIds: Array<ID>): Promise<void> => {
  const requests = departmentIds.map((id) => axios.delete(`${DEPARTMENT_URL}/${id}`));
  return axios.all(requests).then(() => {});
};

export {
  getDepartment,
  deleteDepartment,
  deleteSelectedDepartment,
  getDepartmentById,
  createDepartment,
  updateDepartment,
};
