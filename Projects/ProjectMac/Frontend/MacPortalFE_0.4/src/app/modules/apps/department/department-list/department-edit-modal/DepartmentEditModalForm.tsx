// import { FC, useEffect, useState } from "react";
// import * as Yup from "yup";
// import { useFormik } from "formik";
// import { isNotEmpty, Mode, QUERIES } from "../../../../../../_metronic/helpers";
// import { initialDepartment, Department, DepartmentTx } from "../core/_models";
// import { useAuth } from "../../../../auth";
// import clsx from "clsx";
// import { useListView } from "../core/ListViewProvider";
// import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
// import { DepartmentListLoading } from "../components/loading/DepartmentListLoading";
// import { createDepartment, insertTransaction, updateDepartment } from "../core/_requests";
// import { useQueryResponse } from "../../../transaction-master/master-list/core/QueryResponseProvider";
// import { toast } from "react-toastify";

// import axios from "axios";
// import { useQueryClient ,useMutation } from "react-query";

// type Props = {
//   isDepartmentLoading: boolean;
//   department: Department;
// };

// interface deptHead {
//   shortName: string;
//   empCode: string;
// }
// const API_URL = import.meta.env.VITE_DEPT_DROPDOWNS_API_URL;

// const editDepartmentSchema = Yup.object().shape({
//   name: Yup.string().required("Description is required"),
//   shortDescription: Yup.string().required("Short Description is required"),
//   status: Yup.string().required("Status is required"),
//   deptHead: Yup.string().required("Department Head is required"),
//   changeReason: Yup.string().min(3, "Minimum 3 characters"),
// });

// const DepartmentEditModalForm: FC<Props> = ({
//   department,
//   isDepartmentLoading,
// }) => {
//     //import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
//     const { setItemIdForUpdate, formMode } = useListView();
//     const { setItemIdForUpdate: closeForm } = useTxListView();
//     // closeForm(undefined);

//   const { refetch } = useQueryResponse();

//   const [statuses] = useState(["ACTIVE", "INACTIVE"]);

//   const [deptHeads, setDeptHeads] = useState<deptHead[]>([]);
//   // const [deptHead, setDeptHead] = useState("");

//   const [newReason, setNewReason] = useState("");

//   // const [ipAddress, setIpAddress] = useState<string>("");
//   const { currentUser } = useAuth();

//   const { query } = useQueryResponse();
//   const queryClient = useQueryClient();

//   useEffect(() => {
//     axios
//       .get(API_URL)
//       .then((response) => {
//         setDeptHeads(response.data.data);
//       })
//       .catch((error) => console.error("Error fetching combined data:", error));
//   }, []);

//   const [departmentForEdit] = useState<Department>({
//     ...department,
//     name:
//       department.name ||
//       initialDepartment.name ||
//       department.fieldNames?.DESCR?.neW_VALUE,
//     shortDescription:
//       department.shortDescription ||
//       initialDepartment.shortDescription ||
//       department.fieldNames?.SH_DESCR?.neW_VALUE,
//     deptHead:
//       department.deptHead ||
//       initialDepartment.deptHead ||
//       department.fieldNames?.DEPT_HEAD?.neW_VALUE,
//     status:
//       department.status ||
//       initialDepartment.status ||
//       department.fieldNames?.status?.neW_VALUE,
//     changeDate:
//       department.changeDate ||
//       initialDepartment.changeDate ||
//       department.fieldNames?.CHG_DATE?.neW_VALUE,
//     changeUser:
//       department.changeUser ||
//       initialDepartment.changeUser ||
//       department.fieldNames?.CHG_USER?.neW_VALUE,
//     changeTerm:
//       department.changeTerm ||
//       initialDepartment.changeTerm ||
//       department.fieldNames?.CHG_TERM?.neW_VALUE,
//   });

//   const fetchIpAddress = async () => {};
//   fetchIpAddress();

//   function formatDate(dateTime: string) {
//     const date = new Date(dateTime);
//     const formattedDate = date.toLocaleDateString("en-GB", {
//       day: "numeric",
//       month: "numeric",
//       year: "numeric",
//     });
//     const formattedTime = date.toLocaleTimeString("en-IN", {
//       hour: "numeric",
//       minute: "numeric",
//       second: "numeric",
//       timeZone: "Asia/Kolkata", // Specify the Indian time zone
//     });
//     return `${formattedDate} ${formattedTime}`;
//   }

//   const cancel = (withRefresh?: boolean) => {
//     if (withRefresh) {
//       refetch();
//     }
//     setItemIdForUpdate(undefined);
//     closeForm(undefined);
//   };

//   const createTx = useMutation((payload: any) => insertTransaction(payload), {
//     onSuccess: (response: any) => {
//       queryClient.invalidateQueries([
//         `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
//       ]);

//       setItemIdForUpdate(undefined);
//       closeForm(undefined);
//       toast.success(
//         <div>
//           <h4 className="alert-heading">Successfully Submitted Form...</h4>
//           <p className="alert alert-success" role="alert">
//             <h6>{response?.message}</h6>
//           </p>
//         </div>
//       );
//       cancel(true);
//     },
//     onError: (response: any) => {
//       toast.error(
//         <div>
//           <h4 className="alert-heading">Failed to submit Form...</h4>
//           <p className="alert alert-danger" role="alert">
//             <h6>{response?.message}</h6>
//           </p>
//         </div>
//       );
//     },
//   });

//   const formik = useFormik({
//     initialValues: departmentForEdit,
//     validationSchema: editDepartmentSchema,
//     onSubmit: async (values, { setSubmitting }) => {
//       setSubmitting(true);

//       if (isNotEmpty(values.id)) {
//         const payload: DepartmentTx = {
//           tableName: "DEPARTMENT",
//           keyVar: "",
//           wfLevel: 1,
//           conf: "N",
//           siteCode: "S0001",
//           emp: currentUser?.id?.toString() || "",
//           filePath: null,
//           file: null,
//           strTranId: values.fieldNames ? values.id : "",
//           folderPath: null,
//           CHG_USER: currentUser?.id?.toString() || "",
//           CHG_TERM: "TERM",
//           remarks: department.changeReason,
//           CHG_DATE: new Date(),

//           fieldUpdates: [
//             {
//               fieldName: "DEPT_CODE",
//               oldValue:department.dept_Code || department.fieldNames?.DEPT_CODE?.olD_VALUE || "",
//               newValue: values.dept_Code || department.fieldNames?.DEPT_CODE?.olD_VALUE || "",
//             },
//            {
//               fieldName: "DESCR",
//               oldValue:
//                 department.name || department.fieldNames?.DESCR?.olD_VALUE,
//               newValue: values.name,
//             },
//             {
//               fieldName: "SH_DESCR",
//               oldValue:
//                 department.shortDescription ||
//                 department.fieldNames?.SH_DESCR?.olD_VALUE,
//               newValue: values.shortDescription,
//             },
//             {
//               fieldName: "DEPT_HEAD",
//               oldValue:
//                 department.deptHead ||
//                 department.fieldNames?.DEPT_HEAD?.olD_VALUE,
//               newValue: values.deptHead,
//             },
//             {
//               fieldName: "CHG_USER",
//               oldValue:
//                 department.changeUser ||
//                 department.fieldNames?.CHG_USER?.olD_VALUE,
//               newValue: currentUser?.id?.toString() || "",
//             },
//             {
//               fieldName: "CHG_TERM",
//               oldValue:
//                 department.changeTerm ||
//                 department.fieldNames?.CHG_TERM?.olD_VALUE,
//               newValue: "TERM",
//             },
//             {
//               fieldName: "CHG_DATE",
//               oldValue:
//                 department.changeDate ||
//                 department.fieldNames?.CHG_DATE?.olD_VALUE,
//               newValue: new Date(),
//             },
//             {
//               fieldName: "status",
//               oldValue:
//                 department.status || department.fieldNames?.status?.olD_VALUE,
//               newValue: values.status || department.status,
//             },
//           ],
//         };
//         createTx.mutate(payload);

//         setSubmitting(false);
//       } else {
//         const payload: DepartmentTx = {
//           tableName: "DEPARTMENT",
//           keyVar: "",
//           wfLevel: 1,
//           conf: "N",
//           siteCode: "S0001",
//           emp: currentUser?.id?.toString() || "",
//           filePath: null,
//           file: null,
//           folderPath: null,
//           CHG_USER: currentUser?.id?.toString() || "",
//           CHG_TERM: "TERM",
//           remarks: "New Department Added",
//           CHG_DATE: new Date(),

//           fieldUpdates: [
//             {
//               fieldName: "DEPT_CODE",
//               oldValue: "",
//               newValue: "",
//             },
//             {
//               fieldName: "DESCR",
//               oldValue: "",
//               newValue: values.name,
//             },
//             {
//               fieldName: "SH_DESCR",
//               oldValue: "",
//               newValue: values.shortDescription,
//             },
//             {
//               fieldName: "DEPT_HEAD",
//               oldValue: "",

//               newValue: values.deptHead,
//             },
//             {
//               fieldName: "CHG_USER",
//               oldValue: "",
//               newValue: currentUser?.id?.toString() || "",
//             },
//             {
//               fieldName: "CHG_TERM",
//               oldValue: "",
//               newValue: "TERM",
//             },
//             {
//               fieldName: "CHG_DATE",
//               oldValue: "",
//               newValue: new Date(),
//             },
//             {
//               fieldName: "status",
//               oldValue: "",
//               newValue: values.status || department.status,
//             },
//           ],
//         };
//         createTx.mutate(payload);

//         setSubmitting(false);
//       }
//     },
//   });

//   useEffect(() => {
//     if (formMode === Mode.ADD) {
//       formik.setFieldValue("status", "A");
//     }
//   }, []);

//   return (
//     <>
//       <form
//         id="kt_modal_add_user_form"
//         className="form"
//         onSubmit={formik.handleSubmit}
//         noValidate
//       >
//         <div className="d-flex flex-column " id="kt_modal_add_user_scroll">
//           {formMode !== Mode.ADD && (
//             <div className="row mb-3">
//               <div className="fv-row">
//                 <label className=" fw-bold fs-6 mb-2">Department Code :</label>
//                 <input
//                   className="form-control form-control-solid mb-3 mb-lg-0"
//                   value={formik.values.id ? formik.values.id : ""}
//                   disabled
//                 />
//               </div>
//             </div>
//           )}
//           <div className="row mb-3">
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required fw-bold fs-6 mb-2">
//                   Department Name
//                 </label>
//                 <input
//                   placeholder="Description"
//                   {...formik.getFieldProps("name")}
//                   onChange={(e) =>
//                     formik.setFieldValue("name", e.target.value.toUpperCase())
//                   }
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     { "is-invalid": formik.touched.name && formik.errors.name },
//                     { "is-valid": formik.touched.name && !formik.errors.name }
//                   )}
//                   type="name"
//                   name="name"
//                   autoComplete="off"
//                   disabled={
//                     formik.isSubmitting ||
//                     isDepartmentLoading ||
//                     formMode === Mode.VIEW
//                   }
//                 />
//                 {formik.touched.name && formik.errors.name && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.name}</span>
//                   </div>
//                 )}
//               </div>
//             </div>
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required fw-bold fs-6 mb-2">
//                   Short Description
//                 </label>
//                 <input
//                   placeholder="Short Description"
//                   {...formik.getFieldProps("shortDescription")}
//                   onChange={(e) =>
//                     formik.setFieldValue(
//                       "shortDescription",
//                       e.target.value.toUpperCase()
//                     )
//                   }
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid":
//                         formik.touched.shortDescription &&
//                         formik.errors.shortDescription,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.shortDescription &&
//                         !formik.errors.shortDescription,
//                     }
//                   )}
//                   type="text"
//                   name="shortDescription"
//                   autoComplete="off"
//                   disabled={
//                     formik.isSubmitting ||
//                     isDepartmentLoading ||
//                     formMode === Mode.VIEW
//                   }
//                 />
//                 {formik.touched.shortDescription &&
//                   formik.errors.shortDescription && (
//                     <div className="fv-plugins-message-container text-danger">
//                       <span role="alert">{formik.errors.shortDescription}</span>
//                     </div>
//                   )}
//               </div>
//             </div>
//             {/* data empCode shortName */}
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required form-label fs-6 fw-bold">
//                   Department Head
//                 </label>
//                 <select
//                   className={clsx(
//                     "form-select form-select-solid fw-bolder",
//                     {
//                       "is-invalid":
//                         formik.touched.deptHead && formik.errors.deptHead,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.deptHead && !formik.errors.deptHead,
//                     }
//                   )}
//                   onChange={(e) => {
//                     formik.setFieldValue("deptHead", e.target.value);
//                   }}
//                   disabled={formMode === Mode.VIEW}
//                   value={formik.values.deptHead}
//                 >
//                   <option value=""></option>
//                   {deptHeads.map((deptHead) => (
//                     <option key={deptHead.empCode} value={deptHead.empCode}>
//                       {deptHead.shortName}
//                     </option>
//                   ))}
//                 </select>
//                 {formik.touched.deptHead && formik.errors.deptHead && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.deptHead}</span>
//                   </div>
//                 )}
//               </div>
//             </div>
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required form-label fs-6 fw-bold">
//                   Status
//                 </label>
//                 <select
//                   className={clsx(
//                     "form-select form-select-solid pl-3 mb-3",
//                     {
//                       "is-invalid":
//                         formik.touched.status && formik.errors.status,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.status && !formik.errors.status,
//                     }
//                   )}
//                   value={formik.values.status}
//                   // onChange={formik.handleChange}
//                   onChange={(e) => {
//                     formik.setFieldValue("status", e.target.value);
//                   }}
//                   disabled={formMode === Mode.ADD || formMode === Mode.VIEW}
//                 >
//                   <option value=""></option>
//                   {statuses.map((status) => (
//                     <option value={status === "ACTIVE" ? "A" : "N"}>
//                       {status}
//                     </option>
//                   ))}
//                 </select>
//                 {formik.touched.status && formik.errors.status && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.status}</span>
//                   </div>
//                 )}
//               </div>
//             </div>

//             {formMode === Mode.VIEW && (
//               <>
//                 <div className="col-md-6">
//                   <div className="fv-row">
//                     <label className="fw-bold fs-6 mb-2">Change User</label>
//                     <input
//                       className="form-control form-control-solid mb-3 mb-lg-0"
//                       value={formik.values.changeUser?.toUpperCase()}
//                       disabled={formMode === Mode.VIEW}
//                     />
//                   </div>
//                 </div>
//                 <div className="col-md-6">
//                   <div className="fv-row">
//                     <label className="fw-bold fs-6 mb-2">Change Term</label>
//                     <input
//                       className="form-control form-control-solid mb-3 mb-lg-0"
//                       value={formik.values.changeTerm}
//                       //value={ipAddress}
//                       disabled={formMode === Mode.VIEW}
//                     />
//                   </div>
//                 </div>
//                 <div className="col-md-6">
//                   <div className="fv-row">
//                     <label className=" fw-bold fs-6 mb-2">Change Date</label>
//                     <input
//                       className="form-control form-control-solid mb-3 mb-lg-0"
//                       // value={formik.values.changeDate}
//                       // value={
//                       //   formik.values.changeDate
//                       //     ? formik.values.changeDate.toString()
//                       //     : ""
//                       // }
//                       value={
//                         formik.values.changeDate
//                           ? formatDate(formik.values?.changeDate.toString())
//                           : ""
//                       }
//                       disabled={formMode === Mode.VIEW}
//                     />
//                   </div>
//                 </div>
//               </>
//             )}
//           </div>
//           {formMode !== Mode.ADD && (
//             <div className="row mb-3">
//               <div className="fv-row">
//                 <label className="required fw-bold fs-6 mb-2">
//                   Change Reason
//                 </label>
//                 <textarea
//                   placeholder="Change Reason"
//                   //value={{...formik.getFieldProps('changeReason')}}
//                   {...(formMode === Mode.VIEW
//                     ? { ...formik.getFieldProps("changeReason") }
//                     : { newReason })}
//                   onChange={(e) => {
//                     formik.setFieldValue(
//                       "changeReason",
//                       e.target.value.toUpperCase()
//                     );
//                     setNewReason(e.target.value.toUpperCase());
//                   }}
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid":
//                         formik.touched.changeReason &&
//                         formik.errors.changeReason,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.changeReason &&
//                         !formik.errors.changeReason,
//                     }
//                   )}
//                   // type="text"
//                   name="changeReason"
//                   autoComplete="off"
//                   disabled={
//                     formik.isSubmitting ||
//                     isDepartmentLoading ||
//                     formMode === Mode.VIEW
//                   }
//                 />
//                 {formik.touched.changeReason && formik.errors.changeReason && (
//                   <div className="fv-plugins-message-container">
//                     <span role="alert">{formik.errors.changeReason}</span>
//                   </div>
//                 )}
//               </div>
//             </div>
//           )}
//         </div>
//         <div className="text-center pt-15">
//           {/* <button
//             type='reset'
//             onClick={() => cancel()}
//             className='btn btn-light me-3'
//             disabled={formik.isSubmitting || isDepartmentLoading}
//           >
//             Cancel
//           </button> */}
//           {formMode !== Mode.VIEW && (
//             <button
//               type="submit"
//               className="btn btn-primary"
//               disabled={
//                 isDepartmentLoading ||
//                 formik.isSubmitting ||
//                 !formik.touched ||
//                 (formMode !== Mode.ADD && newReason === "")
//               }
//             >
//               <span className="indicator-label">Submit</span>
//               {(formik.isSubmitting || isDepartmentLoading) && (
//                 <span className="indicator-progress">
//                   Please wait...{" "}
//                   <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
//                 </span>
//               )}
//             </button>
//           )}
//         </div>
//       </form>
//       {(formik.isSubmitting || isDepartmentLoading) && (
//         <DepartmentListLoading />
//       )}
//     </>
//   );
// };

// export { DepartmentEditModalForm };

import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
import { useQueryResponse } from "../../../transaction-master/master-list/core/QueryResponseProvider";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import { useQueryClient, useMutation } from "react-query";
import Tooltip, { TooltipProps, tooltipClasses } from "@mui/material/Tooltip";
import { styled } from "@mui/material/styles";
import { Department } from "../core/_models";
import { DepartmentListLoading } from "../components/loading/DepartmentListLoading";
import { createTransaction } from "../../../transaction-master/master-list/core/_models";
import { insertTransaction } from "../../../transaction-master/master-list/core/_requests";

const API_URL = import.meta.env.VITE_DEPT_DROPDOWNS_API_URL;
const DEPARTMENT_URL = import.meta.env.VITE_DEPT_API_URL;

type Props = {
  isDepartmentLoading: boolean;
  department: Department;
};

interface deptHeadDropdown {
  shortName: string;
  empCode: string;
}

const DepartmentEditModalForm: FC<Props> = ({
  department,
  isDepartmentLoading,
}) => {
  const { setItemIdForUpdate, formMode } = useListView();
  const { itemIdForUpdate, setItemIdForUpdate: closeForm } = useTxListView();
  const [departmentValues, setDepartmentValues] = useState<Department>();
  const { refetch } = useQueryResponse();

  const [deptHeads, setDeptHeads] = useState<deptHeadDropdown[]>([]);
  const [statuses] = useState([
    { code: "A", descr: "ACTIVE" },
    { code: "N", descr: "INACTIVE" },
  ]);

  const [privateIP, setPrivateIP] = useState();
  const { currentUser } = useAuth();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();

  const [isDataFetched, setIsDataFetched] = useState(false);

  const editDepartmentSchema = Yup.object().shape({
    name: Yup.string().trim().required("Description is required"),
    shortDescription: Yup.string()
      .trim()
      .required("Short Description is required"),
    deptHead: Yup.string().trim().required("Department Head is required"),
    status: Yup.string().trim().required("Department Status is required"),
    changeReason: Yup.string().when([], {
      is: () => formMode !== Mode.ADD,
      then: (schema) =>
        schema
          .trim()
          .required("Change Reason is required")
          .min(5, "Minimum 5 characters"),
      otherwise: (schema) => schema.notRequired(),
    }),
  });

  useEffect(() => {
    axios
      .get(API_URL)
      .then((response) => {
        setDeptHeads(response.data.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const [departmentForEdit] = useState<Department>({
    ...department,
    dept_Code:
      department.dept_Code || department.fieldNames?.DEPT_CODE?.neW_KEY,
    name: department.name || department.fieldNames?.DESCR?.neW_KEY,
    shortDescription:
      department.shortDescription || department.fieldNames?.SH_DESCR?.neW_KEY,
    changeTerm:
      department.tX_CHG_TERM ||
      department.changeTerm ||
      department.fieldNames?.CHG_TERM?.neW_KEY,
    changeDate:
      department.tX_CHG_DATE ||
      department.changeDate ||
      department.fieldNames?.CHG_DATE?.neW_KEY,
    changeUser:
      department.tX_CHG_USER ||
      department.changeUser ||
      department.fieldNames?.CHG_USER?.neW_KEY,
    deptHead: department.deptHead || department.fieldNames?.DEPT_HEAD?.neW_KEY,
    deptHeadName:
      department.deptHeadName || department.fieldNames?.DEPT_HEAD?.neW_VALUE,
    status: department.status || department.fieldNames?.STATUS?.neW_KEY,
    changeReason: formMode === Mode.VIEW ? department.changeReason : "",
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
    closeForm(undefined);
  };

  useEffect(() => {
    if (itemIdForUpdate && !isDataFetched) {
      axios
        .get(
          `${DEPARTMENT_URL}/GetDepartmentByDeptCode/${department.fieldNames?.DEPT_CODE?.neW_KEY}`
        )
        .then((response) => {
          const data = response.data.data;

          setDepartmentValues(data);
          formik.setValues({
            dept_Code:
              department.fieldNames?.DEPT_CODE?.neW_KEY || data.dept_Code,
            name: department.fieldNames?.DESCR?.neW_KEY || data.name,
            shortDescription:
              department.fieldNames?.SH_DESCR?.neW_KEY || data.shortDescription,
            changeTerm:
              department.fieldNames?.CHG_TERM?.neW_KEY || data.CHG_TERM,
            changeDate:
              department.fieldNames?.CHG_DATE?.neW_KEY || data.CHG_DATE,
            tX_CHG_USER:
              department.fieldNames?.CHG_USER?.neW_KEY || data.CHG_USER,
            deptHead:
              department.fieldNames?.DEPT_HEAD?.neW_KEY || data.deptHead,
            deptHeadName:
              department.fieldNames?.DEPT_HEAD?.neW_VALUE || data.deptHeadName,
            status: department.fieldNames?.STATUS?.neW_KEY || data.status,
            // remarks:
            //   formMode === Mode.VIEW
            //     ? department.changeReason || department.remarks
            //     : "",
          });
          setIsDataFetched(true);
        })
        .catch((error) =>
          console.error("Error fetching combined data:", error)
        );
    }
  }, [itemIdForUpdate, isDataFetched, department]);

  const createTx = useMutation((payload: any) => insertTransaction(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        closeForm(undefined);
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
        cancel(true);
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formattedDate = (date: Date) => {
    return new Date(date).toLocaleString("en-GB", {
      day: "2-digit",
      month: "2-digit",
      year: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    });
  };

  const formik = useFormik({
    initialValues: departmentForEdit,
    validationSchema: editDepartmentSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      const payload: createTransaction = {
        tableName: "DEPARTMENT",
        keyVar: "",
        wfLevel: 1,
        conf: "N",
        siteCode: "S0001",
        emp: currentUser?.id?.toString() || "",
        filePath: null,
        file: null,
        strTranId: department.fieldNames ? itemIdForUpdate : null,
        folderPath: null,
        tX_CHG_USER: currentUser?.id?.toString() || "",
        tX_CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        remarks: values.changeReason || "NEW DEPARTMENT ADDED",

        fieldUpdates: [
          {
            SEQ_NO: "1",
            FIELD_NAME: "DEPT_CODE",
            FIELD_DESCRIPTION: "DEPARTMENT CODE",
            OLD_KEY:
              department.dept_Code ||
              department.fieldNames?.DEPT_CODE?.olD_KEY ||
              null,
            OLD_VALUE:
              department.dept_Code ||
              department.fieldNames?.DEPT_CODE?.olD_VALUE ||
              null,
            NEW_KEY:
              department.dept_Code ||
              department.fieldNames?.DEPT_CODE?.olD_VALUE ||
              null,
            NEW_VALUE:
              department.dept_Code ||
              department.fieldNames?.DEPT_CODE?.olD_VALUE ||
              null,
            FILE_PATH: "",
          },
          ...((itemIdForUpdate !== undefined &&
            department.fieldNames?.DESCR?.olD_KEY === values.name) ||
          (department.name !== values.name &&
            departmentValues?.name !== values.name)
            ? [
                {
                  SEQ_NO: "2",
                  FIELD_NAME: "DESCR",
                  FIELD_DESCRIPTION: "DESCRIPTION",
                  OLD_KEY:
                    department.name ||
                    department.fieldNames?.DESCR?.olD_KEY ||
                    departmentValues?.name ||
                    "",
                  OLD_VALUE:
                    department.name ||
                    department.fieldNames?.DESCR?.olD_VALUE ||
                    departmentValues?.name ||
                    "",
                  NEW_KEY: values.name,
                  NEW_VALUE: values.name,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            department.fieldNames?.SH_DESCR?.olD_KEY ===
              values.shortDescription) ||
          (department.shortDescription !== values.shortDescription &&
            departmentValues?.shortDescription !== values.shortDescription)
            ? [
                {
                  SEQ_NO: "3",
                  FIELD_NAME: "SH_DESCR",
                  FIELD_DESCRIPTION: "SHORT DESCRIPTION",
                  OLD_KEY:
                    department.shortDescription ||
                    department.fieldNames?.SH_DESCR?.olD_KEY ||
                    departmentValues?.shortDescription ||
                    "",
                  OLD_VALUE:
                    department.shortDescription ||
                    department.fieldNames?.SH_DESCR?.olD_VALUE ||
                    departmentValues?.shortDescription ||
                    "",
                  NEW_KEY: values.shortDescription,
                  NEW_VALUE: values.shortDescription,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            department.fieldNames?.DEPT_HEAD?.olD_KEY === values.deptHead) ||
          (department.deptHead !== values.deptHead &&
            departmentValues?.deptHead !== values.deptHead)
            ? [
                {
                  SEQ_NO: "4",
                  FIELD_NAME: "DEPT_HEAD",
                  FIELD_DESCRIPTION: "DEPARTMENT HEAD",
                  OLD_KEY:
                    department.deptHead ||
                    department.fieldNames?.DEPT_HEAD?.olD_KEY ||
                    departmentValues?.deptHead ||
                    "",
                  OLD_VALUE:
                    department.deptHeadName ||
                    department.fieldNames?.DEPT_HEAD?.olD_VALUE ||
                    departmentValues?.deptHeadName ||
                    "",
                  NEW_KEY: values.deptHead,
                  NEW_VALUE: values.deptHeadName,
                  FILE_PATH: "",
                },
              ]
            : []),

          ...((itemIdForUpdate !== undefined &&
            department.fieldNames?.STATUS?.olD_KEY === values.status) ||
          (department.status !== values.status &&
            departmentValues?.status !== values.status)
            ? [
                {
                  SEQ_NO: "5",
                  FIELD_NAME: "STATUS",
                  FIELD_DESCRIPTION: "STATUS",
                  OLD_KEY:
                    department.status ||
                    department.fieldNames?.STATUS?.olD_KEY ||
                    departmentValues?.status ||
                    "",
                  OLD_VALUE:
                    department.status === "A"
                      ? "ACTIVE"
                      : department.status === "N"
                      ? "INACTIVE"
                      : "" || department.fieldNames?.STATUS?.olD_KEY === "A"
                      ? "ACTIVE"
                      : department.fieldNames?.STATUS?.olD_KEY === "N"
                      ? "INACTIVE"
                      : "" || departmentValues?.status === "A"
                      ? "ACTIVE"
                      : departmentValues?.status === "N"
                      ? "INACTIVE"
                      : "",
                  NEW_KEY: values.status,
                  NEW_VALUE:
                    values.status === "A"
                      ? "ACTIVE"
                      : values.status === "N"
                      ? "INACTIVE"
                      : "",
                  FILE_PATH: "",
                },
              ]
            : []),

          {
            SEQ_NO: "6",
            FIELD_NAME: "CHG_USER",
            FIELD_DESCRIPTION: "CHANGE USER",
            OLD_KEY:
              department.changeUser ||
              department.fieldNames?.CHG_USER?.olD_KEY ||
              departmentValues?.changeUser ||
              "",
            OLD_VALUE:
              department.changeUser ||
              department.fieldNames?.CHG_USER?.olD_VALUE ||
              departmentValues?.changeUser ||
              "",
            NEW_KEY: currentUser?.id?.toString() || "",
            NEW_VALUE: currentUser?.emp_Name || "",
            FILE_PATH: "",
          },
          {
            SEQ_NO: "7",
            FIELD_NAME: "CHG_TERM",
            FIELD_DESCRIPTION: "CHANGE TERM",
            OLD_KEY:
              department.changeTerm ||
              department.fieldNames?.CHG_TERM?.olD_KEY ||
              departmentValues?.changeTerm ||
              "",
            OLD_VALUE:
              department.changeTerm ||
              department.fieldNames?.CHG_TERM?.olD_VALUE ||
              departmentValues?.changeTerm ||
              "",
            NEW_KEY: privateIP,
            NEW_VALUE: privateIP,
            FILE_PATH: "",
          },
          {
            SEQ_NO: "8",
            FIELD_NAME: "CHG_DATE",
            FIELD_DESCRIPTION: "CHANGE DATE",
            OLD_KEY:
              department.changeDate ||
              department.fieldNames?.CHG_DATE?.olD_KEY ||
              departmentValues?.changeDate ||
              "",
            OLD_VALUE:
              (department.changeDate && formattedDate(department.changeDate)) ||
              (departmentValues?.changeDate &&
                formattedDate(departmentValues?.changeDate)) ||
              (department.fieldNames?.CHG_DATE?.olD_VALUE &&
                formattedDate(
                  department.changeDate ||
                    department.fieldNames?.CHG_DATE?.olD_VALUE
                )) ||
              "",
            NEW_KEY: new Date(),
            NEW_VALUE: formattedDate(new Date()),
            FILE_PATH: "",
          },
          {
            SEQ_NO: "9",
            FIELD_NAME: "CHG_REASON",
            FIELD_DESCRIPTION: "CHANGE REASON",
            OLD_KEY:
              department.changeReason ||
              department.fieldNames?.CHG_REASON?.olD_KEY ||
              departmentValues?.changeReason ||
              "",
            OLD_VALUE:
              department.changeReason ||
              department.fieldNames?.CHG_REASON?.olD_KEY ||
              departmentValues?.changeReason ||
              "",
            NEW_KEY: values.changeReason || "NEW DEPARTMENT ADDED",
            NEW_VALUE: values.changeReason || "NEW DEPARTMENT ADDED",
            FILE_PATH: "",
          },
        ],
      };
      createTx.mutate(payload);

      setSubmitting(false);
    },
  });

  useEffect(() => {
    if (formMode === Mode.ADD) {
      formik.setFieldValue("status", "A");
    }
  }, []);

  function formatDate(dateString: string) {
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata",
    });
    return `${formattedDate} ${formattedTime}`;
  }

  const LightTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: theme.palette.common.white,
      color: "rgba(0, 0, 0, 0.87)",
      boxShadow: theme.shadows[1],
      fontSize: 15,
    },
  }));

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column" id="kt_modal_add_user_scroll">
          {/* Begin: Edit Row */}
          <div className="row mb-1">
            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="descr"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DESCRIPTION
                </label>
                <input
                  placeholder="Enter Description"
                  {...formik.getFieldProps("name")}
                  onChange={(e) =>
                    formik.setFieldValue("name", e.target.value.toUpperCase())
                  }
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        department.fieldNames?.DESCR?.neW_VALUE,
                    },
                    {
                      "is-invalid": formik.touched.name && formik.errors.name,
                    },
                    {
                      "is-valid": formik.touched.name && !formik.errors.name,
                    }
                  )}
                  type="text"
                  name="name"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDepartmentLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(department.fieldNames?.DESCR && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      department.fieldNames?.DESCR?.olD_VALUE === null
                        ? "-"
                        : department.fieldNames?.DESCR?.olD_VALUE
                    }`,
                  })}
                />
              </div>
              {formik.touched.name && formik.errors.name && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.name}</span>
                </div>
              )}
            </div>

            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="shortDescription"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SHORT DESCR.
                </label>
                <input
                  placeholder="Short Description"
                  {...formik.getFieldProps("shortDescription")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "shortDescription",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        department.fieldNames?.SH_DESCR?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.shortDescription &&
                        formik.errors.shortDescription,
                    },
                    {
                      "is-valid":
                        formik.touched.shortDescription &&
                        !formik.errors.shortDescription,
                    }
                  )}
                  type="text"
                  name="shortDescription"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDepartmentLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(department.fieldNames?.SH_DESCR && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      department.fieldNames?.SH_DESCR?.olD_VALUE === null
                        ? "-"
                        : department.fieldNames?.SH_DESCR?.olD_VALUE
                    }`,
                  })}
                />
              </div>
              {formik.touched.shortDescription &&
                formik.errors.shortDescription && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.shortDescription}</span>
                  </div>
                )}
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="deptHead"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DEPARTMENT HEAD
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        department.fieldNames?.DEPT_HEAD?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.deptHead && formik.errors.deptHead,
                    },
                    {
                      "is-valid":
                        formik.touched.deptHead && !formik.errors.deptHead,
                    }
                  )}
                  {...formik.getFieldProps("deptHead")}
                  onChange={(e) => {
                    const selectedValue = e.target.value;
                    const selectedOption = deptHeads.find(
                      (_deptHead) => _deptHead.empCode === selectedValue
                    );

                    formik.setFieldValue("deptHead", selectedValue);
                    formik.setFieldValue(
                      "deptHeadName",
                      selectedOption?.shortName || ""
                    );
                  }}
                  disabled={
                    formik.isSubmitting ||
                    isDepartmentLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(department.fieldNames?.DEPT_HEAD && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      department.fieldNames?.DEPT_HEAD?.olD_VALUE === null
                        ? "-"
                        : department.fieldNames?.DEPT_HEAD?.olD_VALUE
                    }`,
                  })}
                >
                  <option value=""></option>
                  {deptHeads.map((_deptHead) => (
                    <option key={_deptHead.empCode} value={_deptHead.empCode}>
                      {_deptHead.shortName}
                    </option>
                  ))}
                </select>
              </div>
              {formik.touched.deptHead && formik.errors.deptHead && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.deptHead}</span>
                </div>
              )}
            </div>

            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="status"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  STATUS
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        department.fieldNames?.STATUS?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  {...formik.getFieldProps("status")}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                  }}
                  disabled={formMode === Mode.ADD || formMode === Mode.VIEW}
                  {...(department.fieldNames?.STATUS && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      department.fieldNames?.STATUS?.olD_VALUE === null
                        ? "-"
                        : department.fieldNames?.STATUS?.olD_VALUE
                    }`,
                  })}
                >
                  <option value=""></option>
                  {statuses.map((status) => (
                    <option value={status.code}>{status.descr}</option>
                  ))}
                </select>
              </div>
              {formik.touched.status && formik.errors.status && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.status}</span>
                </div>
              )}
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            {formMode === Mode.VIEW && (
              <>
                <div className="col-lg-6">
                  <div className="d-flex align-items-center mb-1">
                    <label
                      htmlFor="changeTerm"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CHANGE TERM
                    </label>
                    <input
                      {...formik.getFieldProps("changeTerm")}
                      className="form-control form-control-solid pl-3 mb-3"
                      type="text"
                      name="changeTerm"
                      autoComplete="off"
                      disabled={
                        formik.isSubmitting ||
                        isDepartmentLoading ||
                        formMode === Mode.VIEW
                      }
                    />
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="d-flex align-items-center mb-1">
                    <label
                      htmlFor="changeUser"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CHANGE USER
                    </label>
                    <input
                      placeholder=""
                      {...formik.getFieldProps("changeUser")}
                      className="form-control form-control-solid pl-3 mb-3"
                      type="text"
                      name="changeUser"
                      autoComplete="off"
                      disabled={
                        formik.isSubmitting ||
                        isDepartmentLoading ||
                        formMode === Mode.VIEW
                      }
                    />
                  </div>
                </div>
              </>
            )}
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            {formMode === Mode.VIEW && (
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="changeDate"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE DATE
                </label>
                <input
                  {...formik.getFieldProps("changeDate")}
                  className="form-control form-control-solid pl-3 mb-3"
                  type="text"
                  name="changeDate"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDepartmentLoading ||
                    formMode === Mode.VIEW
                  }
                  value={
                    formik.values.changeDate
                      ? formatDate(formik.values?.changeDate.toString())
                      : ""
                  }
                />
              </div>
            )}

            {formMode !== Mode.ADD && (
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="changeReason"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CHANGE REASON
                  </label>
                  <input
                    placeholder="Change Reason"
                    {...formik.getFieldProps("changeReason")}
                    onChange={(e) =>
                      formik.setFieldValue(
                        "changeReason",
                        e.target.value.toUpperCase()
                      )
                    }
                    className={clsx(
                      "form-control form-control-solid pl-3 mb-3",
                      {
                        "is-invalid":
                          formik.touched.changeReason &&
                          formik.errors.changeReason,
                      },
                      {
                        "is-valid":
                          formik.touched.changeReason &&
                          !formik.errors.changeReason,
                      }
                    )}
                    type="text"
                    name="changeReason"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isDepartmentLoading ||
                      formMode === Mode.VIEW
                    }
                  />
                </div>
                {formik.touched.changeReason && formik.errors.changeReason && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.changeReason}</span>
                  </div>
                )}
              </div>
            )}
          </div>
          {/* End: Edit Row */}
        </div>
        {/* </div> */}
        <div className="text-center pt-15">
          {formMode !== Mode.VIEW && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isDepartmentLoading ||
                formik.isSubmitting ||
                !formik.touched ||
                !formik.isValid
              }
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isDepartmentLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isDepartmentLoading) && (
        <DepartmentListLoading />
      )}
    </>
  );
};

export { DepartmentEditModalForm };
