import {useListView} from '../../core/ListViewProvider'
import {DesignationsListToolbar} from './DesignationsListToolbar'
import {DesignationsListGrouping} from './DesignationsListGrouping'
import {DesignationsListSearchComponent} from './DesignationsListSearchComponent'

const DesignationsListHeader = () => {
  const {selected} = useListView()
  return (
    <div className='card-header border-0 pt-6'>
      <DesignationsListSearchComponent />
      {/* begin::Card toolbar */}
      <div className='card-toolbar'>
        {/* begin::Group actions */}
        {selected.length > 0 ? <DesignationsListGrouping /> : <DesignationsListToolbar />}
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export {DesignationsListHeader}
