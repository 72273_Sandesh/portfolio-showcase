import { ID, Response } from "../../../../../../_metronic/helpers";
export type Designation = {
  id?: ID;
  name?: string;
  design_code?: string;
  longDesignation?: string;
  status?: string;
  gradeCode?: string;
  gradeName?: string;
  cadreCode?: string;
  cadreName?: string;
  gradeId?: string;
  changeUser?: string;
  changeTerm?: string;
  changeReason?: string;
  changeDate?: Date | string;
  udfNum1?: number;
  udfStr1?: string;
  sapDesignCode?: string;
  responseStatus?: string;
  message?: string;

  wF_PROCESS_LEVEL?: number;
  emP_CODE?: string;
  remarks?: string;
  SessionempCode?: string;
  moduleType?: string;
  tX_CHG_DATE?: string | Date;
  tX_CHG_USER?: string;
  tX_CHG_TERM?: string;

  fieldNames?: {
    DESIGN_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    DESIGNATION: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    LONG_DESIGNATION: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    STATUS: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    GRADE_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_DATE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_TERM: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_USER: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_REASON: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
  };
};

export type DesignationsQueryResponse = Response<Array<Designation>>;

export const initialDesignation: Designation = {};
