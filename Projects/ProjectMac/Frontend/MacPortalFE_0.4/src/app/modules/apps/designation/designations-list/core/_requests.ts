import axios, { AxiosResponse } from "axios";
import { ID, Response } from "../../../../../../_metronic/helpers";
import { Designation, DesignationsQueryResponse } from "./_models";

const Designation_URL = import.meta.env.VITE_DESIGN_API_URL;

const getDesignations = (query: string): Promise<DesignationsQueryResponse> => {
  return axios
  .get(`${Designation_URL}/GetAllDesignations?${query}`)
    .then((d: AxiosResponse<DesignationsQueryResponse>) => d.data);
};
console.log(getDesignations);
const getDesignationById = (id: ID): Promise<Designation | undefined> => {
  return axios
    .get(`${Designation_URL}/GetDesignationByCode/${id}`)
    .then((response: AxiosResponse<Response<Designation>>) => response.data)
    .then((response: Response<Designation>) => response.data);
};

const createDesignation = (designation: Designation): Promise<Designation | undefined> => {
  return axios
    .post(`${Designation_URL}/CreateDesignation`, designation)
    .then((response: AxiosResponse<Response<Designation>>) => response.data)
    .then((response: Response<Designation>) => response.data);
};

const updateDesignation = (designation: Designation): Promise<Designation | undefined> => {
  return axios
    .put(`${Designation_URL}/UpdateDesignation/${designation.id}`, designation)
    .then((response: AxiosResponse<Response<Designation>>) => response.data)
    .then((response: Response<Designation>) => response.data);
};

const deleteDesignation = (designationId: ID): Promise<void> => {
  return axios.delete(`${Designation_URL}/${designationId}`).then(() => {});
};

const deleteSelectedDesignations = (designationIds: Array<ID>): Promise<void> => {
  const requests = designationIds.map((id) => axios.delete(`${Designation_URL}/${id}`));
  return axios.all(requests).then(() => {});
};

export {
  getDesignations,
  deleteDesignation,
  deleteSelectedDesignations,
  getDesignationById,
  createDesignation,
  updateDesignation,
};
