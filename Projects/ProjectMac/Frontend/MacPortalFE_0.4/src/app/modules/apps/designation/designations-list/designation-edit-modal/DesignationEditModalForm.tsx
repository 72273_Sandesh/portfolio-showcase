// import { FC, useEffect, useState } from "react";
// import * as Yup from "yup";
// import { useFormik } from "formik";
// import { isNotEmpty, Mode, QUERIES } from "../../../../../../_metronic/helpers";
// import {
//   initialDesignation,
//   Designation,
//   Designationtx,
// } from "../core/_models";
// import { useAuth } from "../../../../auth";
// import clsx from "clsx";
// import { useListView } from "../core/ListViewProvider";
// import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
// import { DesignationsListLoading } from "../components/loading/DesignationsListLoading";
// import { createDesignation, insertTransaction } from "../core/_requests";
// import { useQueryResponse } from "../../../transaction-master/master-list/core/QueryResponseProvider";
// import { toast } from "react-toastify";
// import { useQueryClient, useMutation } from "react-query";

// import axios from "axios";

// type Props = {
//   isDesignationLoading: boolean;
//   designation: Designation;
// };

// interface grade {
//   id: number;
//   description: string;
// }

// const API_URL = import.meta.env.VITE_DESIGN_DROPDOWNS_API_URL;

// const editDesignationSchema = Yup.object().shape({
//   name: Yup.string().required("Description is required"),
//   longDesignation: Yup.string().required("Short Description is required"),
//   // changeReason: Yup.string().min(3, "Minimum 3 characters"),
//   gradeCode: Yup.string().required("Grade is required"),
//   status: Yup.string().required("Status is required"),
// });

// const DesignationEditModalForm: FC<Props> = ({
//   designation,
//   isDesignationLoading,
// }) => {
//   const { setItemIdForUpdate, formMode } = useListView();
//   const { setItemIdForUpdate: closeForm } = useTxListView();

//   const { refetch } = useQueryResponse();
//   const { query } = useQueryResponse();
//   const queryClient = useQueryClient();

//   const [statuses] = useState(["ACTIVE", "INACTIVE"]);

//   const [grades, setGrades] = useState<grade[]>([]);

//   const [newReason, setNewReason] = useState("");

//   // const [ipAddress, setIpAddress] = useState<string>('');
//   const { currentUser } = useAuth();

//   useEffect(() => {
//     axios
//       .get(API_URL)
//       .then((response) => {
//         setGrades(response.data.data);
//       })
//       .catch((error) => console.error("Error fetching combined data:", error));
//   }, []);

//   const [designationForEdit] = useState<Designation>({
//     ...designation,
//     name:
//       designation.name ||
//       initialDesignation.name ||
//       designation?.fieldNames?.DESIGNATION?.neW_VALUE,
//     longDesignation:
//       designation.longDesignation ||
//       initialDesignation.longDesignation ||
//       designation?.fieldNames?.LONG_DESIGNATION?.neW_VALUE,
//     gradeCode:
//       designation.gradeCode ||
//       initialDesignation.gradeCode ||
//       designation?.fieldNames?.GRADE_CODE?.neW_VALUE,
//     status:
//       designation.status ||
//       initialDesignation.status ||
//       designation?.fieldNames?.STATUS?.neW_VALUE,
//     // changeDate: designation.changeDate || initialDesignation.changeDate || designation?.fieldNames?.CHG_DATE?.neW_VALUE,
//     // changeUser: designation.changeUser || initialDesignation.changeUser || designation?.fieldNames?.CHG_USER?.neW_VALUE,
//     // changeTerm: designation.changeTerm || initialDesignation.changeTerm || designation?.fieldNames?.CHG_TERM?.neW_VALUE,
//   });
//   const fetchIpAddress = async () => {};

//   fetchIpAddress();
//   function formatDate(dateTime: string) {
//     const date = new Date(dateTime);
//     const formattedDate = date.toLocaleDateString("en-GB", {
//       day: "numeric",
//       month: "numeric",
//       year: "numeric",
//     });
//     const formattedTime = date.toLocaleTimeString("en-IN", {
//       hour: "numeric",
//       minute: "numeric",
//       second: "numeric",
//       timeZone: "Asia/Kolkata", // Specify the Indian time zone
//     });
//     return `${formattedDate} ${formattedTime}`;
//   }

//   const cancel = (withRefresh?: boolean) => {
//     if (withRefresh) {
//       refetch();
//     }
//     setItemIdForUpdate(undefined);
//   };

//   const createTx = useMutation((payload: any) => insertTransaction(payload), {
//     onSuccess: (response: any) => {
//       queryClient.invalidateQueries([
//         `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
//       ]);

//       setItemIdForUpdate(undefined);
//       closeForm(undefined);
//       toast.success(
//         <div>
//           <h4 className="alert-heading">Successfully Submitted Form...</h4>
//           <p className="alert alert-success" role="alert">
//             <h6>{response?.message}</h6>
//           </p>
//         </div>
//       );
//       cancel(true);
//     },
//     onError: (response: any) => {
//       toast.error(
//         <div>
//           <h4 className="alert-heading">Failed to submit Form...</h4>
//           <p className="alert alert-danger" role="alert">
//             <h6>{response?.message}</h6>
//           </p>
//         </div>
//       );
//     },
//   });

//   const formik = useFormik({
//     initialValues: designationForEdit,
//     validationSchema: editDesignationSchema,
//     onSubmit: async (values, { setSubmitting }) => {
//       setSubmitting(true);

//       if (isNotEmpty(values.id)) {
//         const payload: Designationtx = {
//           tableName: "DESIGNATION",
//           keyVar: "",
//           wfLevel: 1,
//           conf: "N",
//           siteCode: "S0001",
//           emp: currentUser?.id?.toString() || "",
//           filePath: null,
//           file: null,
//           strTranId: "",
//           // values.fieldNames ? values.id : "",
//           folderPath: null,
//           chG_USER: currentUser?.id?.toString() || "",
//           chG_TERM: "TERM",
//           remarks: designation?.changeReason,
//           chG_DATE: new Date(),

//           fieldUpdates: [
//             {
//               fieldName: "DESIGN_CODE",
//               oldValue:
//                 designation.design_code ||
//                 designation.fieldNames?.DESIGN_CODE.olD_VALUE ||
//                 "",
//               newValue:
//                 values.design_code ||
//                 designation.fieldNames?.DESIGN_CODE?.olD_VALUE ||
//                 "",
//             },
//             {
//               fieldName: "DESIGNATION",
//               oldValue:
//                 designation.name ||
//                 designation.fieldNames?.DESIGNATION?.olD_VALUE,
//               newValue: values.name,
//             },
//             {
//               fieldName: "LONG_DESIGNATION",
//               oldValue:
//                 designation.longDesignation ||
//                 designation.fieldNames?.LONG_DESIGNATION?.olD_VALUE,
//               newValue: values.longDesignation,
//             },
//             {
//               fieldName: "STATUS",
//               oldValue:
//                 designation.status || designation.fieldNames?.STATUS?.olD_VALUE,
//               newValue: values.status,
//             },
//             {
//               fieldName: "GRADE_CODE",
//               oldValue:
//                 designation.gradeCode ||
//                 designation.fieldNames?.GRADE_CODE?.olD_VALUE,
//               newValue:
//                 values.gradeCode ||
//                 designation.fieldNames?.GRADE_CODE?.olD_VALUE,
//             },

//             {
//               fieldName: "CHG_DATE",
//               oldValue:
//                 designation.changeDate ||
//                 designation.fieldNames?.CHG_DATE?.olD_VALUE,
//               newValue: new Date(),
//             },
//             {
//               fieldName: "CHG_TERM",
//               oldValue:
//                 designation.changeTerm ||
//                 designation.fieldNames?.CHG_TERM?.olD_VALUE,
//               newValue: "TERM",
//             },
//             {
//               fieldName: "CHG_USER",
//               oldValue:
//                 designation.changeUser ||
//                 designation.fieldNames?.CHG_USER?.olD_VALUE,
//               newValue: currentUser?.id?.toString() || "",
//             },
//           ],
//         };
//         createTx.mutate(payload);
//         setSubmitting(false);
//       } else {
//         const payload: Designationtx = {
//           tableName: "DESIGNATION",
//           keyVar: "",
//           wfLevel: 1,
//           conf: "N",
//           siteCode: "S0001",
//           emp: currentUser?.id?.toString() || "",
//           filePath: null,
//           file: null,
//           folderPath: null,
//           chG_USER: currentUser?.id?.toString() || "",
//           chG_TERM: "TERM",
//           chG_DATE: new Date(),
//           remarks: "New Designation Added",

//           fieldUpdates: [
//             {
//               fieldName: "DESIGN_CODE",
//               oldValue: "",
//               newValue: "",
//             },
//             {
//               fieldName: "DESIGNATION",
//               oldValue: "",
//               newValue: values.name,
//             },
//             {
//               fieldName: "LONG_DESIGNATION",
//               oldValue: "",
//               newValue: values.longDesignation,
//             },
//             {
//               fieldName: "GRADE_CODE",
//               oldValue: "",
//               newValue: values.gradeCode,
//             },
//             {
//               fieldName: "STATUS",
//               oldValue: "",
//               newValue: values.status,
//             },
//             {
//               fieldName: "CHG_TERM",
//               oldValue: "",
//               newValue: "TERM",
//             },
//             {
//               fieldName: "CHG_DATE",
//               oldValue: "",
//               newValue: new Date(),
//             },
//             {
//               fieldName: "CHG_USER",
//               oldValue: "",
//               newValue: currentUser?.id?.toString() || "",
//             },
//           ],
//         };

//         createTx.mutate(payload);
//         setSubmitting(false);
//       }
//     },
//   });
//   useEffect(() => {
//     if (formMode === Mode.ADD) {
//       formik.setFieldValue("status", "A");
//     }
//   }, []);

//   return (
//     <>
//       <form
//         id="kt_modal_add_user_form"
//         className="form"
//         onSubmit={formik.handleSubmit}
//         noValidate
//       >
//         <div className="d-flex flex-column" id="kt_modal_add_user_scroll">
//           {formMode !== Mode.ADD && (
//             <div className="row mb-3">
//               <div className="fv-row">
//                 <label className=" fw-bold fs-6 mb-2">Designation Code :</label>
//                 <input
//                   className="form-control form-control-solid mb-3 mb-lg-0"
//                   value={formik.values.id ? formik.values.id : ""}
//                   disabled={formMode === Mode.VIEW || formMode === Mode.EDIT}
//                 />
//               </div>
//             </div>
//           )}
//           <div className="row mb-3">
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required fw-bold fs-6 mb-2">
//                   Designation Name
//                 </label>
//                 <input
//                   placeholder="Description"
//                   {...formik.getFieldProps("name")}
//                   onChange={(e) =>
//                     formik.setFieldValue("name", e.target.value.toUpperCase())
//                   }
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     { "is-invalid": formik.touched.name && formik.errors.name },
//                     { "is-valid": formik.touched.name && !formik.errors.name }
//                   )}
//                   type="name"
//                   name="name"
//                   autoComplete="off"
//                   disabled={
//                     formik.isSubmitting ||
//                     isDesignationLoading ||
//                     formMode === Mode.VIEW
//                   }
//                 />
//                 {formik.touched.name && formik.errors.name && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.name}</span>
//                   </div>
//                 )}
//               </div>
//             </div>
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required fw-bold fs-6 mb-2">
//                   Short Description
//                 </label>
//                 <input
//                   placeholder="Short Description"
//                   {...formik.getFieldProps("longDesignation")}
//                   onChange={(e) =>
//                     formik.setFieldValue(
//                       "longDesignation",
//                       e.target.value.toUpperCase()
//                     )
//                   }
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid":
//                         formik.touched.longDesignation &&
//                         formik.errors.longDesignation,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.longDesignation &&
//                         !formik.errors.longDesignation,
//                     }
//                   )}
//                   type="text"
//                   name="longDesignation"
//                   autoComplete="off"
//                   disabled={
//                     formik.isSubmitting ||
//                     isDesignationLoading ||
//                     formMode === Mode.VIEW
//                   }
//                 />
//                 {formik.touched.longDesignation &&
//                   formik.errors.longDesignation && (
//                     <div className="fv-plugins-message-container text-danger">
//                       <span role="alert">{formik.errors.longDesignation}</span>
//                     </div>
//                   )}
//               </div>
//             </div>
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required fs-6 fw-bold">Grade</label>

//                 <select
//                   className={clsx(
//                     "form-select form-select-solid fw-bolder",
//                     {
//                       "is-invalid":
//                         formik.touched.gradeCode && formik.errors.gradeCode,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.gradeCode && !formik.errors.gradeCode,
//                     }
//                   )}
//                   onChange={(e) => {
//                     formik.setFieldValue("gradeCode", e.target.value);
//                   }}
//                   disabled={formMode === Mode.VIEW}
//                   value={formik.values.gradeCode}
//                 >
//                   <option value=""></option>
//                   {grades.map((grade) => (
//                     <option key={grade.id} value={grade.id}>
//                       {grade.description}
//                     </option>
//                   ))}
//                 </select>

//                 {formik.touched.gradeCode && formik.errors.gradeCode && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.gradeCode}</span>
//                   </div>
//                 )}
//               </div>
//             </div>
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required fs-6 fw-bold">Status</label>
//                 {/* <select
//                   // className='form-select form-select-solid fw-bolder'
//                   className={clsx(
//                     "form-select form-select-solid fw-bolder",
//                     {
//                       "is-invalid":
//                         formik.touched.status && formik.errors.status,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.status && !formik.errors.status,
//                     }
//                   )}
//                   onChange={(e) => {
//                     formik.setFieldValue("status", e.target.value);
//                     setStatus(e.target.value);
//                   }}
//                   value={status}
//                   disabled={globalVariable === "infoDesign"}
//                 >
//                   {globalVariable === "addDesign" ? (
//                     <option value="">{formik.values.status}</option>
//                   ) : (
//                     <option value="">
//                       {formik.values.status === "A"
//                         ? "ACTIVE"
//                         : formik.values.status === "N"
//                         ? "INACTIVE"
//                         : ""}
//                     </option>
//                   )}
//                   {statuses.map((status) => (
//                     <option key={status} value={status}>
//                       {status}
//                     </option>
//                   ))}
//                 </select> */}

//                 <select
//                   className={clsx(
//                     "form-control form-control-solid pl-3 mb-3",
//                     {
//                       "is-invalid":
//                         formik.touched.status && formik.errors.status,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.status && !formik.errors.status,
//                     }
//                   )}
//                   value={formik.values.status}
//                   // onChange={formik.handleChange}
//                   onChange={(e) => {
//                     formik.setFieldValue("status", e.target.value);
//                   }}
//                   disabled={formMode === Mode.ADD || formMode === Mode.VIEW}
//                   //disabled={formMode !== Mode.EDIT}
//                 >
//                   {statuses.map((status) => (
//                     <option value={status === "ACTIVE" ? "A" : "N"}>
//                       {status}
//                     </option>
//                   ))}
//                 </select>
//                 {formik.touched.status && formik.errors.status && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.status}</span>
//                   </div>
//                 )}
//               </div>
//             </div>

//             {formMode === Mode.VIEW && (
//               <>
//                 <div className="col-md-6">
//                   <div className="fv-row">
//                     <label className="fw-bold fs-6 mb-2">Change User</label>
//                     <input
//                       className="form-control form-control-solid mb-3 mb-lg-0"
//                       value={formik.values.changeUser?.toUpperCase()}
//                       disabled={formMode === Mode.VIEW}
//                     />
//                   </div>
//                 </div>
//                 <div className="col-md-6">
//                   <div className="fv-row">
//                     <label className="fw-bold fs-6 mb-2">Change Term</label>
//                     <input
//                       className="form-control form-control-solid mb-3 mb-lg-0"
//                       value={formik.values.changeTerm}
//                       //value={ipAddress}
//                       disabled={formMode === Mode.VIEW}
//                     />
//                   </div>
//                 </div>
//                 <div className="col-md-6">
//                   <div className="fv-row">
//                     <label className=" fw-bold fs-6 mb-2">Change Date</label>
//                     <input
//                       className="form-control form-control-solid mb-3 mb-lg-0"
//                       // value={formik.values.changeDate}
//                       // value={formik.values.changeDate ? formatDate(formik.values.changeDate) : ''}
//                       // value={
//                       //   formik.values.changeDate
//                       //     ? formik.values.changeDate.toString()
//                       //     : ""
//                       // }
//                       value={
//                         formik.values.changeDate
//                           ? formatDate(formik.values?.changeDate.toString())
//                           : ""
//                       }
//                       disabled={formMode === Mode.VIEW}
//                     />
//                   </div>
//                 </div>
//               </>
//             )}

//             {/* <div className='col-md-6'>
//               <div className='fv-row'>
//                 <label className='form-label fs-6 fw-bold'>Salary Account:</label>
//                 <select
//                   className='form-select form-select-solid fw-bolder'
//                   onChange={(e) => { formik.setFieldValue('acctCode', e.target.value); setSalaryAcct(e.target.value) }}
//                   value={salaryAcct}
//                   disabled={globalVariable === 'infoDesign'}
//                 >
//                   <option value=''>{formik.values.acctCode}</option>
//                   {salaryAccts.map(salaryAcct => (
//                     <option key={salaryAcct} value={salaryAcct}>{salaryAcct}</option>
//                   ))}
//                 </select>
//               </div>
//             </div>
//             <div className='col-md-6'>
//               <div className='fv-row'>
//                 <label className='form-label fs-6 fw-bold'>Account Code AP:</label>
//                 <select
//                   className='form-select form-select-solid fw-bolder'
//                   onChange={(e) => { formik.setFieldValue('acctCodeAp', e.target.value); setAcctCodeAP(e.target.value) }}
//                   value={acctCodeAP}
//                   disabled={globalVariable === 'infoDesign'}
//                 >
//                   <option value=''>{formik.values.acctCodeAp}</option>
//                   {acctCodeAPs.map(acctCodeAP => (
//                     <option key={acctCodeAP} value={acctCodeAP}>{acctCodeAP}</option>
//                   ))}
//                 </select>
//               </div>
//             </div>
//             <div className='col-md-6'>
//               <div className='fv-row'>
//                 <label className='form-label fs-6 fw-bold'>Pay Table:</label>
//                 <select
//                   className='form-select form-select-solid fw-bolder'
//                   onChange={(e) => { formik.setFieldValue('payTable', e.target.value); setPayTable(e.target.value) }}
//                   value={payTable}
//                   disabled={globalVariable === 'infoDesign'}
//                 >
//                   <option value=''>{formik.values.payTable}</option>
//                   {payTables.map(payTable => (
//                     <option key={payTable} value={payTable}>{payTable}</option>
//                   ))}
//                 </select>
//               </div>
//             </div>
//             <div className='col-md-6'>
//               <div className='fv-row'>
//                 <label className='form-label fs-6 fw-bold'>Cost Centre:</label>
//                 <select
//                   className='form-select form-select-solid fw-bolder'
//                   onChange={(e) => { formik.setFieldValue('cctrCode', e.target.value); setCostCentre(e.target.value) }}
//                   value={costCentre}
//                   disabled={globalVariable === 'infoDesign'}
//                 >
//                   <option value=''>{formik.values.cctrCode}</option>
//                   {costCentres.map(costCentre => (
//                     <option key={costCentre} value={costCentre}>{costCentre}</option>
//                   ))}
//                 </select>
//               </div>
//             </div>
//             <div className='col-md-6'>
//               <div className='fv-row'>
//                 <label className='form-label fs-6 fw-bold'>CCTR CODE AP:</label>
//                 <select
//                   className='form-select form-select-solid fw-bolder'
//                   onChange={(e) => { formik.setFieldValue('cctrCodeAp', e.target.value); setCCTRCodeAP(e.target.value) }}
//                   value={cctrCodeAP}
//                   disabled={globalVariable === 'infoDesign'}
//                 >
//                   <option value=''>{formik.values.cctrCodeAp}</option>
//                   {cctrCodeAPs.map(cctrCodeAP => (
//                     <option key={cctrCodeAP} value={cctrCodeAP}>{cctrCodeAP}</option>
//                   ))}
//                 </select>
//               </div>
//             </div>*/}
//           </div>
//           {formMode !== Mode.ADD && (
//             <div className="row mb-3">
//               <div className="fv-row">
//                 <label className="required fw-bold fs-6 mb-2">
//                   Change Reason
//                 </label>
//                 <textarea
//                   placeholder="Change Reason"
//                   //value={{...formik.getFieldProps('changeReason')}}
//                   {...(formMode === Mode.VIEW
//                     ? { ...formik.getFieldProps("changeReason") }
//                     : { newReason })}
//                   onChange={(e) => {
//                     formik.setFieldValue(
//                       "changeReason",
//                       e.target.value.toUpperCase()
//                     );
//                     setNewReason(e.target.value.toUpperCase());
//                   }}
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid":
//                         formik.touched.changeReason &&
//                         formik.errors.changeReason,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.changeReason &&
//                         !formik.errors.changeReason,
//                     }
//                   )}
//                   // type='text'
//                   name="changeReason"
//                   autoComplete="off"
//                   disabled={
//                     formik.isSubmitting ||
//                     isDesignationLoading ||
//                     formMode === Mode.VIEW
//                   }
//                 />
//                 {formik.touched.changeReason && formik.errors.changeReason && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.changeReason}</span>
//                   </div>
//                 )}
//               </div>
//             </div>
//           )}
//         </div>
//         <div className="text-center pt-15">
//           {/* <button
//             type='reset'
//             onClick={() => cancel()}
//             className='btn btn-light me-3'
//             disabled={formik.isSubmitting || isDesignationLoading}
//           >
//             Cancel
//           </button> */}
//           {formMode !== Mode.VIEW && (
//             <button
//               type="submit"
//               className="btn btn-primary"
//               disabled={
//                 isDesignationLoading ||
//                 formik.isSubmitting ||
//                 !formik.touched ||
//                 (formMode !== Mode.ADD && newReason === "")
//               }
//             >
//               <span className="indicator-label">Submit</span>
//               {(formik.isSubmitting || isDesignationLoading) && (
//                 <span className="indicator-progress">
//                   Please wait...{" "}
//                   <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
//                 </span>
//               )}
//             </button>
//           )}
//         </div>
//       </form>
//       {(formik.isSubmitting || isDesignationLoading) && (
//         <DesignationsListLoading />
//       )}
//     </>
//   );
// };

// export { DesignationEditModalForm };

import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
import { useQueryResponse } from "../../../transaction-master/master-list/core/QueryResponseProvider";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import { useQueryClient, useMutation } from "react-query";
import Tooltip, { TooltipProps, tooltipClasses } from "@mui/material/Tooltip";
import { styled } from "@mui/material/styles";
import { Designation } from "../core/_models";
import { DesignationsListLoading } from "../components/loading/DesignationsListLoading";
import { createTransaction } from "../../../transaction-master/master-list/core/_models";
import { insertTransaction } from "../../../transaction-master/master-list/core/_requests";

const API_URL = import.meta.env.VITE_DESIGN_DROPDOWNS_API_URL;
const DESIGNATION_URL = import.meta.env.VITE_DESIGN_API_URL;

type Props = {
  isDesignationLoading: boolean;
  designation: Designation;
};

interface gradesDropdown {
  id: string;
  description: string;
}

const DesignationEditModalForm: FC<Props> = ({
  designation,
  isDesignationLoading,
}) => {
  const { setItemIdForUpdate, formMode } = useListView();
  const { itemIdForUpdate, setItemIdForUpdate: closeForm } = useTxListView();
  const [designationValues, setDesignationValues] = useState<Designation>();
  const { refetch } = useQueryResponse();

  const [grades, setGrades] = useState<gradesDropdown[]>([]);
  const [statuses] = useState([
    { code: "A", descr: "ACTIVE" },
    { code: "N", descr: "INACTIVE" },
  ]);

  const [privateIP, setPrivateIP] = useState();
  const { currentUser } = useAuth();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();

  const [isDataFetched, setIsDataFetched] = useState(false);

  const editDesignationSchema = Yup.object().shape({
    name: Yup.string().trim().required("Description is required"),
    longDesignation: Yup.string()
      .trim()
      .required("Short Description is required"),
    gradeCode: Yup.string().trim().required("Department Head is required"),
    status: Yup.string().trim().required("Department Status is required"),
    changeReason: Yup.string().when([], {
      is: () => formMode !== Mode.ADD,
      then: (schema) =>
        schema
          .trim()
          .required("Change Reason is required")
          .min(5, "Minimum 5 characters"),
      otherwise: (schema) => schema.notRequired(),
    }),
  });

  useEffect(() => {
    axios
      .get(API_URL)
      .then((response) => {
        setGrades(response.data.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const [designationForEdit] = useState<Designation>({
    ...designation,
    design_code:
      designation.design_code || designation.fieldNames?.DESIGN_CODE?.neW_KEY,
    name: designation.name || designation.fieldNames?.DESIGNATION?.neW_KEY,
    longDesignation:
      designation.longDesignation ||
      designation.fieldNames?.LONG_DESIGNATION?.neW_KEY,
    changeTerm:
      designation.tX_CHG_TERM ||
      designation.changeTerm ||
      designation.fieldNames?.CHG_TERM?.neW_KEY,
    changeDate:
      designation.tX_CHG_DATE ||
      designation.changeDate ||
      designation.fieldNames?.CHG_DATE?.neW_KEY,
    changeUser:
      designation.tX_CHG_USER ||
      designation.changeUser ||
      designation.fieldNames?.CHG_USER?.neW_KEY,
    gradeCode:
      designation.gradeCode || designation.fieldNames?.GRADE_CODE?.neW_KEY,
    gradeName:
      designation.gradeName || designation.fieldNames?.GRADE_CODE?.neW_VALUE,
    status: designation.status || designation.fieldNames?.STATUS?.neW_KEY,
    changeReason: formMode === Mode.VIEW ? designation.changeReason : "",
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
    closeForm(undefined);
  };

  useEffect(() => {
    if (itemIdForUpdate && !isDataFetched) {
      axios
        .get(
          `${DESIGNATION_URL}/GetDesignationByCode/${designation.fieldNames?.DESIGN_CODE?.neW_KEY}`
        )
        .then((response) => {
          const data = response.data.data;

          setDesignationValues(data);
          formik.setValues({
            design_code:
              designation.fieldNames?.DESIGN_CODE?.neW_KEY || data.dept_Code,
            name: designation.fieldNames?.DESIGNATION?.neW_KEY || data.name,
            longDesignation:
              designation.fieldNames?.LONG_DESIGNATION?.neW_KEY ||
              data.longDesignation,
            changeTerm:
              designation.fieldNames?.CHG_TERM?.neW_KEY || data.changeTerm,
            changeDate:
              designation.fieldNames?.CHG_DATE?.neW_KEY || data.changeDate,
            changeUser:
              designation.fieldNames?.CHG_USER?.neW_KEY || data.changeUser,
            gradeCode:
              designation.fieldNames?.GRADE_CODE?.neW_KEY || data.gradeCode,
            gradeName:
              designation.fieldNames?.GRADE_CODE?.neW_VALUE || data.gradeName,
            status: designation.fieldNames?.STATUS?.neW_KEY || data.status,
            // remarks:
            //   formMode === Mode.VIEW
            //     ? designation.changeReason || designation.remarks
            //     : "",
          });
          setIsDataFetched(true);
        })
        .catch((error) =>
          console.error("Error fetching combined data:", error)
        );
    }
  }, [itemIdForUpdate, isDataFetched, designation]);

  const createTx = useMutation((payload: any) => insertTransaction(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        closeForm(undefined);
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
        cancel(true);
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formattedDate = (date: Date) => {
    return new Date(date).toLocaleString("en-GB", {
      day: "2-digit",
      month: "2-digit",
      year: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    });
  };

  const formik = useFormik({
    initialValues: designationForEdit,
    validationSchema: editDesignationSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      const payload: createTransaction = {
        tableName: "DESIGNATION",
        keyVar: "",
        wfLevel: 1,
        conf: "N",
        siteCode: "S0001",
        emp: currentUser?.id?.toString() || "",
        filePath: null,
        file: null,
        strTranId: designation.fieldNames ? itemIdForUpdate : null,
        folderPath: null,
        tX_CHG_USER: currentUser?.id?.toString() || "",
        tX_CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        remarks: values.changeReason || "NEW DESIGNATION ADDED",

        fieldUpdates: [
          {
            SEQ_NO: "1",
            FIELD_NAME: "DESIGN_CODE",
            FIELD_DESCRIPTION: "DESIGNATION CODE",
            OLD_KEY:
              designation.design_code ||
              designation.fieldNames?.DESIGN_CODE?.olD_KEY ||
              null,
            OLD_VALUE:
              designation.design_code ||
              designation.fieldNames?.DESIGN_CODE?.olD_VALUE ||
              null,
            NEW_KEY:
              designation.design_code ||
              designation.fieldNames?.DESIGN_CODE?.olD_VALUE ||
              null,
            NEW_VALUE:
              designation.design_code ||
              designation.fieldNames?.DESIGN_CODE?.olD_VALUE ||
              null,
            FILE_PATH: "",
          },
          ...((itemIdForUpdate !== undefined &&
            designation.fieldNames?.DESIGNATION?.olD_KEY === values.name) ||
          (designation.name !== values.name &&
            designationValues?.name !== values.name)
            ? [
                {
                  SEQ_NO: "2",
                  FIELD_NAME: "DESIGNATION",
                  FIELD_DESCRIPTION: "DESCRIPTION",
                  OLD_KEY:
                    designation.name ||
                    designation.fieldNames?.DESIGNATION?.olD_KEY ||
                    designationValues?.name ||
                    "",
                  OLD_VALUE:
                    designation.name ||
                    designation.fieldNames?.DESIGNATION?.olD_VALUE ||
                    designationValues?.name ||
                    "",
                  NEW_KEY: values.name,
                  NEW_VALUE: values.name,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            designation.fieldNames?.LONG_DESIGNATION?.olD_KEY ===
              values.longDesignation) ||
          (designation.longDesignation !== values.longDesignation &&
            designationValues?.longDesignation !== values.longDesignation)
            ? [
                {
                  SEQ_NO: "3",
                  FIELD_NAME: "LONG_DESIGNATION",
                  FIELD_DESCRIPTION: "SHORT DESCRIPTION",
                  OLD_KEY:
                    designation.longDesignation ||
                    designation.fieldNames?.LONG_DESIGNATION?.olD_KEY ||
                    designationValues?.longDesignation ||
                    "",
                  OLD_VALUE:
                    designation.longDesignation ||
                    designation.fieldNames?.LONG_DESIGNATION?.olD_VALUE ||
                    designationValues?.longDesignation ||
                    "",
                  NEW_KEY: values.longDesignation,
                  NEW_VALUE: values.longDesignation,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            designation.fieldNames?.GRADE_CODE?.olD_KEY === values.gradeCode) ||
          (designation.gradeCode !== values.gradeCode &&
            designationValues?.gradeCode !== values.gradeCode)
            ? [
                {
                  SEQ_NO: "4",
                  FIELD_NAME: "GRADE_CODE",
                  FIELD_DESCRIPTION: "GRADE",
                  OLD_KEY:
                    designation.gradeCode ||
                    designation.fieldNames?.GRADE_CODE?.olD_KEY ||
                    designationValues?.gradeCode ||
                    "",
                  OLD_VALUE:
                    designation.gradeName ||
                    designation.fieldNames?.GRADE_CODE?.olD_VALUE ||
                    designationValues?.gradeName ||
                    "",
                  NEW_KEY: values.gradeCode,
                  NEW_VALUE: values.gradeName,
                  FILE_PATH: "",
                },
              ]
            : []),

          ...((itemIdForUpdate !== undefined &&
            designation.fieldNames?.STATUS?.olD_KEY === values.status) ||
          (designation.status !== values.status &&
            designationValues?.status !== values.status)
            ? [
                {
                  SEQ_NO: "5",
                  FIELD_NAME: "STATUS",
                  FIELD_DESCRIPTION: "STATUS",
                  OLD_KEY:
                    designation.status ||
                    designation.fieldNames?.STATUS?.olD_KEY ||
                    designationValues?.status ||
                    "",
                  OLD_VALUE:
                    designation.status === "A"
                      ? "ACTIVE"
                      : designation.status === "N"
                      ? "INACTIVE"
                      : "" || designation.fieldNames?.STATUS?.olD_KEY === "A"
                      ? "ACTIVE"
                      : designation.fieldNames?.STATUS?.olD_KEY === "N"
                      ? "INACTIVE"
                      : "" || designationValues?.status === "A"
                      ? "ACTIVE"
                      : designationValues?.status === "N"
                      ? "INACTIVE"
                      : "",
                  NEW_KEY: values.status,
                  NEW_VALUE:
                    values.status === "A"
                      ? "ACTIVE"
                      : values.status === "N"
                      ? "INACTIVE"
                      : "",
                  FILE_PATH: "",
                },
              ]
            : []),

          {
            SEQ_NO: "6",
            FIELD_NAME: "CHG_USER",
            FIELD_DESCRIPTION: "CHANGE USER",
            OLD_KEY:
              designation.changeUser ||
              designation.fieldNames?.CHG_USER?.olD_KEY ||
              designationValues?.changeUser ||
              "",
            OLD_VALUE:
              designation.changeUser ||
              designation.fieldNames?.CHG_USER?.olD_VALUE ||
              designationValues?.changeUser ||
              "",
            NEW_KEY: currentUser?.id?.toString() || "",
            NEW_VALUE: currentUser?.emp_Name || "",
            FILE_PATH: "",
          },
          {
            SEQ_NO: "7",
            FIELD_NAME: "CHG_TERM",
            FIELD_DESCRIPTION: "CHANGE TERM",
            OLD_KEY:
              designation.changeTerm ||
              designation.fieldNames?.CHG_TERM?.olD_KEY ||
              designationValues?.changeTerm ||
              "",
            OLD_VALUE:
              designation.changeTerm ||
              designation.fieldNames?.CHG_TERM?.olD_VALUE ||
              designationValues?.changeTerm ||
              "",
            NEW_KEY: privateIP,
            NEW_VALUE: privateIP,
            FILE_PATH: "",
          },
          {
            SEQ_NO: "8",
            FIELD_NAME: "CHG_DATE",
            FIELD_DESCRIPTION: "CHANGE DATE",
            OLD_KEY:
              designation.changeDate ||
              designation.fieldNames?.CHG_DATE?.olD_KEY ||
              designationValues?.changeDate ||
              "",
            OLD_VALUE:
              (designation.changeDate &&
                formattedDate(designation.changeDate)) ||
              (designationValues?.changeDate &&
                formattedDate(designationValues?.changeDate)) ||
              (designation.fieldNames?.CHG_DATE?.olD_VALUE &&
                formattedDate(
                  designation.changeDate ||
                    designation.fieldNames?.CHG_DATE?.olD_VALUE
                )) ||
              "",
            NEW_KEY: new Date(),
            NEW_VALUE: formattedDate(new Date()),
            FILE_PATH: "",
          },
          {
            SEQ_NO: "9",
            FIELD_NAME: "CHG_REASON",
            FIELD_DESCRIPTION: "CHANGE REASON",
            OLD_KEY:
              designation.changeReason ||
              designation.fieldNames?.CHG_REASON?.olD_KEY ||
              designationValues?.changeReason ||
              "",
            OLD_VALUE:
              designation.changeReason ||
              designation.fieldNames?.CHG_REASON?.olD_KEY ||
              designationValues?.changeReason ||
              "",
            NEW_KEY: values.changeReason || "NEW DESIGNATION ADDED",
            NEW_VALUE: values.changeReason || "NEW DESIGNATION ADDED",
            FILE_PATH: "",
          },
        ],
      };
      createTx.mutate(payload);

      setSubmitting(false);
    },
  });

  useEffect(() => {
    if (formMode === Mode.ADD) {
      formik.setFieldValue("status", "A");
    }
  }, []);

  function formatDate(dateString: string) {
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata",
    });
    return `${formattedDate} ${formattedTime}`;
  }

  const LightTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: theme.palette.common.white,
      color: "rgba(0, 0, 0, 0.87)",
      boxShadow: theme.shadows[1],
      fontSize: 15,
    },
  }));

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column" id="kt_modal_add_user_scroll">
          {/* Begin: Edit Row */}
          <div className="row mb-1">
            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="name"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DESCRIPTION
                </label>
                <input
                  placeholder="Enter Description"
                  {...formik.getFieldProps("name")}
                  onChange={(e) =>
                    formik.setFieldValue("name", e.target.value.toUpperCase())
                  }
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        designation.fieldNames?.DESIGNATION?.neW_VALUE,
                    },
                    {
                      "is-invalid": formik.touched.name && formik.errors.name,
                    },
                    {
                      "is-valid": formik.touched.name && !formik.errors.name,
                    }
                  )}
                  type="text"
                  name="name"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDesignationLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(designation.fieldNames?.DESIGNATION && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      designation.fieldNames?.DESIGNATION?.olD_VALUE === null
                        ? "-"
                        : designation.fieldNames?.DESIGNATION?.olD_VALUE
                    }`,
                  })}
                />
              </div>
              {formik.touched.name && formik.errors.name && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.name}</span>
                </div>
              )}
            </div>

            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="longDesignation"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SHORT DESCR.
                </label>
                <input
                  placeholder="Short Description"
                  {...formik.getFieldProps("longDesignation")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "longDesignation",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        designation.fieldNames?.LONG_DESIGNATION?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.longDesignation &&
                        formik.errors.longDesignation,
                    },
                    {
                      "is-valid":
                        formik.touched.longDesignation &&
                        !formik.errors.longDesignation,
                    }
                  )}
                  type="text"
                  name="longDesignation"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDesignationLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(designation.fieldNames?.LONG_DESIGNATION && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      designation.fieldNames?.LONG_DESIGNATION?.olD_VALUE ===
                      null
                        ? "-"
                        : designation.fieldNames?.LONG_DESIGNATION?.olD_VALUE
                    }`,
                  })}
                />
              </div>
              {formik.touched.longDesignation &&
                formik.errors.longDesignation && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.longDesignation}</span>
                  </div>
                )}
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="gradeCode"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  GRADE
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        designation.fieldNames?.GRADE_CODE?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.gradeCode && formik.errors.gradeCode,
                    },
                    {
                      "is-valid":
                        formik.touched.gradeCode && !formik.errors.gradeCode,
                    }
                  )}
                  {...formik.getFieldProps("gradeCode")}
                  onChange={(e) => {
                    const selectedValue = e.target.value;
                    const selectedOption = grades.find(
                      (_gradeCode) => _gradeCode.id === selectedValue
                    );

                    formik.setFieldValue("gradeCode", selectedValue);
                    formik.setFieldValue(
                      "gradeName",
                      selectedOption?.description || ""
                    );
                  }}
                  disabled={
                    formik.isSubmitting ||
                    isDesignationLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(designation.fieldNames?.GRADE_CODE && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      designation.fieldNames?.GRADE_CODE?.olD_VALUE === null
                        ? "-"
                        : designation.fieldNames?.GRADE_CODE?.olD_VALUE
                    }`,
                  })}
                >
                  <option value=""></option>
                  {grades.map((_gradeCode) => (
                    <option key={_gradeCode.id} value={_gradeCode.id}>
                      {_gradeCode.description}
                    </option>
                  ))}
                </select>
              </div>
              {formik.touched.gradeCode && formik.errors.gradeCode && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.gradeCode}</span>
                </div>
              )}
            </div>

            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="status"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  STATUS
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        designation.fieldNames?.STATUS?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  {...formik.getFieldProps("status")}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                  }}
                  disabled={formMode === Mode.ADD || formMode === Mode.VIEW}
                  {...(designation.fieldNames?.STATUS && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      designation.fieldNames?.STATUS?.olD_VALUE === null
                        ? "-"
                        : designation.fieldNames?.STATUS?.olD_VALUE
                    }`,
                  })}
                >
                  <option value=""></option>
                  {statuses.map((status) => (
                    <option value={status.code}>{status.descr}</option>
                  ))}
                </select>
              </div>
              {formik.touched.status && formik.errors.status && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.status}</span>
                </div>
              )}
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            {formMode === Mode.VIEW && (
              <>
                <div className="col-lg-6">
                  <div className="d-flex align-items-center mb-1">
                    <label
                      htmlFor="changeTerm"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CHANGE TERM
                    </label>
                    <input
                      {...formik.getFieldProps("changeTerm")}
                      className="form-control form-control-solid pl-3 mb-3"
                      type="text"
                      name="changeTerm"
                      autoComplete="off"
                      disabled={
                        formik.isSubmitting ||
                        isDesignationLoading ||
                        formMode === Mode.VIEW
                      }
                    />
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="d-flex align-items-center mb-1">
                    <label
                      htmlFor="changeUser"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CHANGE USER
                    </label>
                    <input
                      placeholder=""
                      {...formik.getFieldProps("changeUser")}
                      className="form-control form-control-solid pl-3 mb-3"
                      type="text"
                      name="changeUser"
                      autoComplete="off"
                      disabled={
                        formik.isSubmitting ||
                        isDesignationLoading ||
                        formMode === Mode.VIEW
                      }
                    />
                  </div>
                </div>
              </>
            )}
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            {formMode === Mode.VIEW && (
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="changeDate"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE DATE
                </label>
                <input
                  {...formik.getFieldProps("changeDate")}
                  className="form-control form-control-solid pl-3 mb-3"
                  type="text"
                  name="changeDate"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isDesignationLoading ||
                    formMode === Mode.VIEW
                  }
                  value={
                    formik.values.changeDate
                      ? formatDate(formik.values?.changeDate.toString())
                      : ""
                  }
                />
              </div>
            )}

            {formMode !== Mode.ADD && (
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="changeReason"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CHANGE REASON
                  </label>
                  <input
                    placeholder="Change Reason"
                    {...formik.getFieldProps("changeReason")}
                    onChange={(e) =>
                      formik.setFieldValue(
                        "changeReason",
                        e.target.value.toUpperCase()
                      )
                    }
                    className={clsx(
                      "form-control form-control-solid pl-3 mb-3",
                      {
                        "is-invalid":
                          formik.touched.changeReason &&
                          formik.errors.changeReason,
                      },
                      {
                        "is-valid":
                          formik.touched.changeReason &&
                          !formik.errors.changeReason,
                      }
                    )}
                    type="text"
                    name="changeReason"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isDesignationLoading ||
                      formMode === Mode.VIEW
                    }
                  />
                </div>
                {formik.touched.changeReason && formik.errors.changeReason && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.changeReason}</span>
                  </div>
                )}
              </div>
            )}
          </div>
          {/* End: Edit Row */}
        </div>
        {/* </div> */}
        <div className="text-center pt-15">
          {formMode !== Mode.VIEW && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isDesignationLoading ||
                formik.isSubmitting ||
                !formik.touched ||
                !formik.isValid
              }
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isDesignationLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isDesignationLoading) && (
        <DesignationsListLoading />
      )}
    </>
  );
};

export { DesignationEditModalForm };
