import {KTIcon} from '../../../../../../_metronic/helpers'
import {useListView} from '../core/ListViewProvider'

const DesignationEditModalHeader = () => {
  const {itemIdForUpdate,setItemIdForUpdate, formMode } = useListView()
  const openAddModal = () => {
    setItemIdForUpdate(undefined)
    
  }
  return (
    <div className='modal-header'>
      {/* begin::Modal title */}
      <div className="fw-bolder">
        <h2>
          {formMode} DESIGNATION{" "}
          {itemIdForUpdate && (
            <label className="fw-bold ms-5 border border-gray-300 rounded text-gray-600 p-2 fs-6">
              ID : {itemIdForUpdate}
            </label>
          )}
        </h2>
      </div>
      {/* end::Modal title */}
     
      <div
        className='btn btn-icon btn-sm btn-active-icon-primary'
        data-kt-users-modal-action='close'
        onClick={openAddModal}
        style={{cursor: 'pointer'}}
      >
        <KTIcon iconName='cross' className='fs-1' />
      </div>
      
    </div>
  )
}

export {DesignationEditModalHeader}
