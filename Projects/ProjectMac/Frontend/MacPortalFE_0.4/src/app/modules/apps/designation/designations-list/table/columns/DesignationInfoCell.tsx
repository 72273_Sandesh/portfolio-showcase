import {FC} from 'react'
import {Designation} from '../../core/_models'
import { ID, Mode } from '../../../../../../../_metronic/helpers'
import { useListView } from '../../core/ListViewProvider'

type Props = {
  designation: Designation
  id:ID
}

const DesignationInfoCell: FC<Props> = ({designation,id}) => { 
  const {setItemIdForUpdate,setFormMode} = useListView()
  const openInfoModal = () => {
    setItemIdForUpdate(id)
    setFormMode(Mode.VIEW)
  }
  return(
  <div className='d-flex align-items-center'>
    
    <div className='d-flex flex-column'>
      <a  className='text-gray-800 text-hover-primary mb-1' onClick={openInfoModal}>
        {designation.name?.toUpperCase()}
      </a>
      <span >
        <div className=' fw-bolder d-flex align-items-right'>
          <div className='badge badge-light-success fw-bolder d-flex align-items-right'>{designation.id}</div>
        </div>
    </span>
    </div>
  </div>
)
}
export {DesignationInfoCell}
