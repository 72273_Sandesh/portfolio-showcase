import {ListViewProvider, useListView} from './core/ListViewProvider'
import {QueryRequestProvider} from './core/QueryRequestProvider'
import {QueryResponseProvider} from './core/QueryResponseProvider'
import {EmployeeSearchListHeader} from './components/header/EmployeeSearchListHeader'
import {EmployeeSearchTable} from './table/EmployeeSearchTable'
import {EmployeeSearchEditModal} from './employeeSearch-edit-modal/EmployeeSearchEditMaodal'
import {KTCard} from '../../../../../_metronic/helpers'
import { ToolbarWrapper } from '../../../../../_metronic/layout/components/toolbar'
import { Content } from '../../../../../_metronic/layout/components/content'

const EmployeeSearchList = () => {
  const {itemIdForUpdate} = useListView()
  return (
    <>
      <KTCard>
        <EmployeeSearchListHeader />
        <EmployeeSearchTable />
      </KTCard>
      {itemIdForUpdate !== undefined && <EmployeeSearchEditModal />}
    </>
  )
}

const EmployeeSearchListWrapper = () => (
  <QueryRequestProvider>
    <QueryResponseProvider>
      <ListViewProvider>
        <ToolbarWrapper />
        <Content>
          <EmployeeSearchList />
        </Content>
      </ListViewProvider>
    </QueryResponseProvider>
  </QueryRequestProvider>
)

export {EmployeeSearchListWrapper}
