// import {useEffect, useState} from 'react'
// import {MenuComponent} from '../../../../../../../_metronic/assets/ts/components'
// import {initialQueryState, KTIcon} from '../../../../../../../_metronic/helpers'
// import {useQueryRequest} from '../../core/QueryRequestProvider'
// import {useQueryResponse} from '../../core/QueryResponseProvider'
// import axios from 'axios';

// const EmployeeSearchListFilter = () => {
//   const {updateState} = useQueryRequest()
//   const {isLoading} = useQueryResponse()
  

//   useEffect(() => {
//     MenuComponent.reinitialization()
//   }, [])
  
//   const [locationOptions, setLocationOptions] = useState([]);
//   const [selectedLocation, setSelectedLocation] = useState('');

//   const [deaprtmentOptions, setDepartmentOptions] = useState([]);
//   const [selectedDepartment, setSelectedDepartment] = useState('');

//   const [ctcOptions, setCTCOptions] = useState([]);
//   const [selectedCTC, setSelectedCTC] = useState('');

//   const [statusOptions, setStatusOptions] = useState([]);
//   const [selectedStatus, setSelectedStatus] = useState('');
 
//   const resetData = () => {
//     updateState({filter: undefined, ...initialQueryState})
//   }

//   const filterData = () => {
//     updateState({
//       filter: {department,location ,status,ctc},
      
//       ...initialQueryState,
//     })
//   }

//   useEffect(() => {
//     fetchData();
//   }, []);


//   useEffect(() => {
//     fetchDataDept();
//   }, []);

//   useEffect(() => {
//     fetchDataCTC();
//   }, []);

  
//   useEffect(() => {
//     fetchDataStatus();
//   }, []);
  
//   const fetchData = async () => {
//     try {
//       const response = await axios.get('https://dev-5qvzh6ebkuk68u8.api.raw-labs.com/macleods/users/locations');
//       setLocationOptions(response.data);
//     } catch (error) {
//       console.error('Error fetching data:', error);
//     }
//   };

//   const handleLocationChange = (e) => {
//     setSelectedLocation(e.target.value);
//   };



//   const fetchDataDept = async () => {
//     try {
//       const response = await axios.get('https://localhost:44360/api/Department/GetAllDepartments');
//       setDepartmentOptions(response.data);
//     } catch (error) {
//       console.error('Error fetching data:', error);
//     }
//   };

//   const handleDepartmentChange = (e) => {
//     setSelectedDepartment(e.target.value);
//   };


//   const fetchDataCTC = async () => {
//     try {
//       const response = await axios.get('http://your-backend-api-url/ctc');
//       setCTCOptions(response.data);
//     } catch (error) {
//       console.error('Error fetching data:', error);
//     }
//   };

//   const handleCTCChange = (e) => {
//     setSelectedCTC(e.target.value);
//   };

//   const fetchDataStatus = async () => {
//     try {
//       const response = await axios.get('http://your-backend-api-url/status');
//       setStatusOptions(response.data);
//     } catch (error) {
//       console.error('Error fetching data:', error);
//     }
//   };

//   const handleStatusChange = (e) => {
//     setSelectedStatus(e.target.value);
//   };


//   return (
//     <>
//       {/* begin::Filter Button */}
//       <button
//         disabled={isLoading}
//         type='button'
//         className='btn btn-light-primary me-3'
//         data-kt-menu-trigger='click'
//         data-kt-menu-placement='bottom-end'
//       >
//         <KTIcon iconName='filter' className='fs-2' />
//         Filter
//       </button>
//       {/* end::Filter Button */}
//       {/* begin::SubMenu */}
//       <div className='menu menu-sub menu-sub-dropdown w-300px w-md-325px' data-kt-menu='true'>
//         {/* begin::Header */}
//         <div className='px-7 py-5'>
//           {/* <div className='fs-5 text-gray-900 fw-bolder'>Filter Options</div> */}
//         </div>
//         {/* end::Header */}

//         {/* begin::Separator */}
//         {/* <div className='separator border-gray-200'></div> */}
//         {/* end::Separator */}



       
//           {/* end::Input group */}

//         {/* begin::Content */}
//         <div className='px-7 py-5' data-kt-user-table-filter='form'>




//            {/* begin::Input group */}
//            {/* <div className='mb-3'>
//             <label className='form-label fs-6 fw-bold'>Location:</label>
//             <select
//               className='form-select form-select-solid fw-bolder'
//               data-kt-select2='true'
//               data-placeholder='Select option'
//               data-allow-clear='true'
//               data-kt-user-table-filter='role'
//               data-hide-search='true'
//               onChange={(e) => setLocation(e.target.value)}
//               value={location}
//             >
//               <option value=''></option>
//               <option value='agarwalholdindspvtltd'>Agarwal Holdings Pvt. Ltd.</option>
//               <option value='headoffice'>Head Office</option>
//               <option value='rnddivision'>R N D Division</option>
//               <option value='palgharunitI'>Palghar Unit I</option>
//               <option value='damanunitIIphaseI'>Daman Unit II Phase I</option>

            
//               <option value='damanunitIIphaseII'>Daman Unit II Phase II</option>
//               <option value='damanunitIIIkabra'>Daman Unit III Kabra</option>
//               <option value='dhanlaxmiunitIV'>Dhanlaxmi Unit IV</option>
//               <option value='sarigamunitV'>Sarigam Unit V</option>
//               <option value='baddiunitVI'>Baddi Unit VI</option>

             
//               <option value='indoresez'>Indore SEZ</option>
//               <option value='randddivisionIIIplot18'>R & D Division (III) Plot 18</option>
//               <option value='macleodscentreofexcellancesarigam'>Macleods Centre of Excellance - Sarigam</option>
//               <option value='randddivisionIV'>R & D Division (IV)</option>
//               <option value='siliguriwarehouse'>Siliguri Warehouse</option>

              
//               <option value='sarigamHblock'>Sarigam H Block</option>
//               <option value='sarigamrnd'>Sarigam R N D</option>
//               <option value='bhiwandigodown'>Bhiwandi Godown</option>
//               <option value='marolgodown'>Marol Godown</option>
//               <option value='baddiunitIIIcartondivision'>Baddi Unit III (Carton Division)</option>

//               <option value='damanunitVII'>Daman Unit VII</option>
//               <option value='randddivisionII'>R & D Division (II)</option>
//               <option value='randddivisionIII'>R & D Division (III)</option>
//               <option value='jaipur'>Jaipur</option>
//               <option value='sikkimunitIX'>Sikkim unit IX </option>
//               <option value='DahejUnitX'>Dahej Unit X</option>

//               <option value='ambalawarehouse'> Ambala WareHouse</option>
//               <option value='macleodscentreofexcellance'>Macleods Centre of Excellance</option>
//               <option value='rnddivisionVplot19'>R N D Division V Plot 19 </option>
//               <option value='randddivisionVIplot124'>R & D Division (VI) Plot 124</option>
              
//             </select>
//           </div> */}

// <div className='mb-3'>
//       <label className='form-label fs-6 fw-bold'>Location:</label>
//       <select
//         className='form-select form-select-solid fw-bolder'
//         data-kt-select2='true'
//         data-placeholder='Select option'
//         data-allow-clear='true'
//         data-kt-user-table-filter='role'
//         data-hide-search='true'
//         onChange={handleLocationChange}
//         value={selectedLocation}
//       >
//         <option value=''></option>
//         {locationOptions.map((option) => (
//           <option key={option} value={option}>{option}</option>
//         ))}
//       </select>
//     </div>
//           {/* end::Input group */}


//             {/* begin::Input group */}
//         {/* <div className='mb-3'>
//             <label className='form-label fs-6 fw-bold'>Department:</label>
//             <select
//               className='form-select form-select-solid fw-bolder'
//               data-kt-select2='true'
//               data-placeholder='Select option'
//               data-allow-clear='true'
//               data-kt-user-table-filter='two-step'
//               data-hide-search='true'
//               onChange={(e) => setDepartment(e.target.value)}
//               value={department}
//             >
//               <option value='accountsandfinance'>Accounts & finance </option>
//               <option value='administration'>Administration </option>
//               <option value='amd'>AMD</option>
//               <option value='analyticalvalidation'>Analytical Validation</option>

//               <option value='api'>API</option>
//               <option value='bioanalytical'>Bio Analytical</option>
//               <option value='bioequivalance'>Bio-Equivalance</option>
//               <option value='biotechrandd'>BioTech R & D</option> 

              
//               <option value='bulkdrugs'>Bulk Drugs</option>
//               <option value='centralwarehouse'>Central Warehouse</option>
//               <option value='civil'>Civil</option>
//               <option value='clinicaltrials'>Clinical Trials</option>

//               <option value='corporateengineering'>Corporate Engineering</option>
//               <option value='corporateqa'>Copporate Q. A.</option>
//               <option value='crd'>CRD</option>
//               <option value='crm'>CRM</option>
//               <option value='directorsoffice'>Directors Office</option>

//               <option value='dispatch'>Dispatch</option>
//               <option value='distribution'>Distribution</option>
//               <option value='domesticbussiness'>Domestic Bussiness</option>
//               <option value='engineeringandutility'>Engineering and Utility</option>
//               <option value='engineeringservices'>Engineering Services</option>

//               <option value='etp'>ETP</option>
//               <option value='fandd'>F and D </option>
//               <option value='fermentationrandd'>Fermentation R & D</option>
//               <option value='foildepartment'>Foil Department</option>
//               <option value='humanresources'>Human Resource</option>

//               <option value='informationsystem'>Information System</option>
//               <option value='institutionalsales'>Institutional Sales</option>
//               <option value='internalaudit'>Internal Audit</option>
//               <option value='internatinalbussiness'>International Bussiness</option>
//               <option value='internationallogistics'>International Logistics</option>

//               <option value='ipm'>IPM </option>
//               <option value='jaipurdepot'>Jaipur Depot</option>
//               <option value='legal'>Legal</option>
//               <option value='liasioning'>Liasioning</option>
//               <option value='machineoperator'>Machine Operator</option>

//               <option value='maintenance'>Maintenance</option>
//               <option value='marolgodown'>Marol Godown</option>
//               <option value='medicalservices'>Medical Services</option>
//               <option value='microbiologylab'>Microbiology Lab </option>
//               <option value='officestaff'>Office Staff</option>

//               <option value='offsetprinting'>Offset Printing</option>
//               <option value='operationalexcellance'>Operational Excellance</option>
//               <option value='operations'>Operations</option>
//               <option value='packging'>Packaging</option>
//               <option value='pathologylab'>Pathology Lab</option>

//               <option value='pdr'>PDR</option>
//               <option value='pdt'>PDT</option>
//               <option value='pharmacovigilance'>Pharmacovigilance</option>
//               <option value='portfoliomanagement'>Portfolio management</option>
//               <option value='ppic'>PPIC</option>

//               <option value='processengineering'>Process Engineering</option>
//               <option value='production'>Production</option>
//               <option value='projectmanagement'>Project Management</option>
//               <option value='projects'>Projects</option>
//               <option value='purchase'>Purchase</option>

//               <option value='qa'>Q. A.</option>
//               <option value='qc'>Q. C.</option>
//               <option value='regulatoryaffairs'>Regulatory Affairs</option>
//               <option value='safetyandenvironmentcontrol'>Safety and Environment Control</option>
//               <option value='salesandadmin'>Sales and Admin</option>

//               <option value='sample'>Sample</option>
//               <option value='stores'>Stores</option>
//               <option value='tblab'>TB Lab</option>
//               <option value='technicalservices'>Technical Services</option>
//               <option value='technologytransfer'>Technology Transfer</option>

//               <option value='traineeintern'>Trainee Intern</option>
//               <option value='unknown'>Unkonwn</option>
              
//             </select>
//           </div> */}

// <div className='mb-3'>
//       <label className='form-label fs-6 fw-bold'>Department:</label>
//       <select
//         className='form-select form-select-solid fw-bolder'
//         data-kt-select2='true'
//         data-placeholder='Select option'
//         data-allow-clear='true'
//         data-kt-user-table-filter='role'
//         data-hide-search='true'
//         onChange={handleDepartmentChange}
//         value={selectedDepartment}
//       >
//         <option value=''></option>
//         {deaprtmentOptions.map((option) => (
//           <option key={option} value={option}>{option}</option>
//         ))}
//       </select>
//     </div>
//           {/* end::Input group */}

//           {/* begin::Input group */}
//           {/* <div className='mb-3'>
//             <label className='form-label fs-6 fw-bold'>Status:</label>
//             <select
//               className='form-select form-select-solid fw-bolder'
//               data-kt-select2='true'
//               data-placeholder='Select option'
//               data-allow-clear='true'
//               data-kt-user-table-filter='two-step'
//               data-hide-search='true'
//               onChange={(e) => setStatus(e.target.value)}
//               value={status}
//             >
//               <option value=''></option>
//               <option value='active'>Active</option>
//               <option value='seperated'>Separated</option>
//               <option value='all'>All</option> */}
//               {/* <option value='2 days ago'>2 days ago</option> */}
//             {/* </select>
//           </div> */}
//           <div className='mb-3'>
//       <label className='form-label fs-6 fw-bold'>status:</label>
//       <select
//         className='form-select form-select-solid fw-bolder'
//         data-kt-select2='true'
//         data-placeholder='Select option'
//         data-allow-clear='true'
//         data-kt-user-table-filter='role'
//         data-hide-search='true'
//         onChange={handleStatusChange}
//         value={selectedStatus}
//       >
//         <option value=''></option>
//         {statusOptions.map((option) => (
//           <option key={option} value={option}>{option}</option>
//         ))}
//       </select>
//     </div>
//           {/* end::Input group */}






//            {/* begin::Input group */}
//            {/* <div className='mb-10'>
//             <label className='form-label fs-6 fw-bold'>CTC Range:</label>
//             <select
//               className='form-select form-select-solid fw-bolder'
//               data-kt-select2='true'
//               data-placeholder='Select option'
//               data-allow-clear='true'
//               data-kt-user-table-filter='two-step'
//               data-hide-search='true'
//               onChange={(e) => setCTC(e.target.value)}
//               value={ctc}
//             >
//               <option value=''></option>
//               <option value='0to2'>0 - 2 LPA </option>
//               <option value='2to5'>2 - 5 LPA</option>
//               <option value='5to10'>5 - 10 LPA</option>
//               <option value='10to20'>10 - 20 LPA</option>
//               <option value='20to50'>20 - 50 LPA</option>
//               <option value='50plus'> Above 50 LPA</option> */}

//               {/* <option value='2 days ago'>2 days ago</option> */}
//             {/* </select>
//           </div> */}
//           <div className='mb-3'>
//       <label className='form-label fs-6 fw-bold'>CTC Range:</label>
//       <select
//         className='form-select form-select-solid fw-bolder'
//         data-kt-select2='true'
//         data-placeholder='Select option'
//         data-allow-clear='true'
//         data-kt-user-table-filter='role'
//         data-hide-search='true'
//         onChange={handleCTCChange}
//         value={selectedCTC}
//       >
//         <option value=''></option>
//         {ctcOptions.map((option) => (
//           <option key={option} value={option}>{option}</option>
//         ))}
//       </select>
//     </div>
//           {/* end::Input group */}


          

//           {/* begin::Actions */}
//           <div className='d-flex justify-content-end'>
//             <button
//               type='button'
//               disabled={isLoading}
//               onClick={filterData}
//               className='btn btn-light btn-active-light-primary fw-bold me-2 px-6'
//               data-kt-menu-dismiss='true'
//               data-kt-user-table-filter='reset'
//             >
//               Reset
//             </button>
//             <button
//               disabled={isLoading}
//               type='button'
//               onClick={resetData}
//               className='btn btn-primary fw-bold px-6'
//               data-kt-menu-dismiss='true'
//               data-kt-user-table-filter='filter'
//             >
//               Apply
//             </button>
//           </div>
//           {/* end::Actions */}
//         </div>
//         {/* end::Content */}
//       </div>
//       {/* end::SubMenu */}
//     </>
//   )
// }

// export {EmployeeSearchListFilter}


import {FC} from 'react'

const EmployeeSearchListFilter: FC = () => {
  return (<div >
          <h1>This Page is under Development...</h1>
          <h3>please contact to admin</h3>
          </div>)
}

export {EmployeeSearchListFilter}