import {KTIcon} from '../../../../../../../_metronic/helpers'
import {useListView} from '../../core/ListViewProvider'
import {EmployeeSearchListFilter} from './EmployeeSearchListFilter'

const EmployeeSearchListToolbar = () => {
  const {setItemIdForUpdate} = useListView()
  const openAddEmployeeSearchModal = () => {
    setItemIdForUpdate(null)
  }

  return (
    <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
      <EmployeeSearchListFilter />

      {/* begin::Export */}
      {/* <button type='button' className='btn btn-light-primary me-3'>
        <KTIcon iconName='exit-up' className='fs-2' />
        Export
      </button> */}
      {/* end::Export */}

      {/* begin::Add user */}
      {/* <button type='button' className='btn btn-primary' onClick={openAddEmployeeSearchModal}>
        <KTIcon iconName='plus' className='fs-2' />
        Add User
      </button> */}
      {/* end::Add user */}
    </div>
  )
}

export {EmployeeSearchListToolbar}
