import {useQuery} from 'react-query'
import {EmployeeSearchEditModalForm} from './EmployeeSearchEditModalForm'
import {isNotEmpty, QUERIES} from '../../../../../../_metronic/helpers'
import {useListView} from '../core/ListViewProvider'
import {getEmployeeSearchById} from '../core/_requests'

const EmployeeSearchEditModalFormWrapper = () => {
  const {itemIdForUpdate, setItemIdForUpdate} = useListView()
  const enabledQuery: boolean = isNotEmpty(itemIdForUpdate)
  const {
    isLoading,
    data: employeeSearch,
    error,
  } = useQuery(
    `${QUERIES.EmployeeSearch_LIST}-employeeSearch-${itemIdForUpdate}`,
    () => {
      return getEmployeeSearchById(itemIdForUpdate)
    },
    {
      cacheTime: 0,
      enabled: enabledQuery,
      onError: (err) => {
        setItemIdForUpdate(undefined)
        console.error(err)
      },
    }
  )

  if (!itemIdForUpdate) {
    return <EmployeeSearchEditModalForm isEmployeeSearchLoading={isLoading} employeeSearch={{id: undefined}} />
  }

  if (!isLoading && !error && employeeSearch) {
    return <EmployeeSearchEditModalForm isEmployeeSearchLoading={isLoading} employeeSearch={employeeSearch} />
  }

  return null
}

export {EmployeeSearchEditModalFormWrapper}
