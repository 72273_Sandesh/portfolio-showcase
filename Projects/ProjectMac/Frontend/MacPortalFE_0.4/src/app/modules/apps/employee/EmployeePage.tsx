import {Route, Routes, Outlet, Navigate} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {EmployeeListWrapper} from './employee-list/EmployeeList'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const EmployeeBreadcrumbs: Array<PageLink> = [
  {
    title: 'Master',
    path: '/apps/employee/employees',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const EmployeePage = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='employees'
          element={
            <>
              <PageTitle breadcrumbs={EmployeeBreadcrumbs}>Employee List</PageTitle>
              <EmployeeListWrapper />
              <ToastContainer/>
            </>
          }
        />
      </Route>
      <Route index element={<Navigate to='/apps/user-management/users' />} />
    </Routes>
  )
}

export default EmployeePage
