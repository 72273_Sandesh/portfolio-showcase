import {ListViewProvider, useListView} from './core/ListViewProvider'
import {QueryRequestProvider} from './core/QueryRequestProvider'
import {QueryResponseProvider, useQueryResponseLoading} from './core/QueryResponseProvider'
import {EmployeeListHeader} from './components/header/EmployeeListHeader'
import {EmployeeTable} from './table/EmployeeTable'
import {EmployeeEditModal} from './employee-edit-modal/EmployeeEditModal'
import {KTCard} from '../../../../../_metronic/helpers'
import { ToolbarWrapper } from '../../../../../_metronic/layout/components/toolbar'
import { Content } from '../../../../../_metronic/layout/components/content'
import { Toolbar } from '../Toolbar'
import { Loader } from '../../../../../_metronic/utilities/Loader'

const EmployeeList = () => {
  const {itemIdForUpdate} = useListView()
  const isLoading = useQueryResponseLoading()

  return (
    <>
      <KTCard>
        <EmployeeListHeader />
        <EmployeeTable />
      </KTCard>
      {itemIdForUpdate !== undefined && <EmployeeEditModal />}

      {isLoading && <Loader />}
      
    </>
  )
}

const EmployeeListWrapper = () => (
  <QueryRequestProvider>
    <QueryResponseProvider>
      <ListViewProvider>
        <ToolbarWrapper />
        <Content>
        <Toolbar/>
          <EmployeeList />
        </Content>
      </ListViewProvider>
    </QueryResponseProvider>
  </QueryRequestProvider>
)

export {EmployeeListWrapper}
