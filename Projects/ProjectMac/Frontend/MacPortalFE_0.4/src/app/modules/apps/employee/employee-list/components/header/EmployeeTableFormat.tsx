import { useQueryResponseData } from "../../core/QueryResponseProvider";

const EmployeeTableFormat = () => {
  const employees = useQueryResponseData(); 

  return (
    <table id="table-to-export">
      <thead>
        <tr>
          <th>Employee Id</th>
          <th>First Name</th>
          <th>Middle Name</th>
          <th>Last Name</th>
          <th>Designation</th>
          <th>Birth Date</th>
          <th>Joining Date</th>
          <th>Basic salary</th>
          <th>Gross salary</th>
          <th>Designation Code</th>
          <th>Employee Site</th>
          <th>Departmentb Code</th>
          <th>Grade</th>
          <th>Cadre</th>
          <th>Change Date</th>
          <th>Change User</th>
          <th>Change Term</th>
          <th>Location</th>
          <th>Department</th>
          <th>Status</th>
          <th>Change Reason</th>
        </tr>
      </thead>
      <tbody>
        {employees.map((employee) => (
          <tr key={employee.id}>
            <td>{employee.id}</td>
            <td>{employee.emP_FNAME}</td>
            <td>{employee.emP_MNAME}</td>
            <td>{employee.emP_LNAME}</td>
            <td>{employee.designation}</td>
            <td>{employee.dob?.toString()}</td>
            <td>{employee.datE_JOIN?.toString()}</td>
            <td>{employee.basic}</td>
            <td>{employee.gross}</td>
            <td>{employee.desigN_CODE}</td>
            <td>{employee.emP_SITE}</td>
            <td>{employee.depT_CODE}</td>
            <td>{employee.grade}</td>
            <td>{employee.cadre}</td>
            <td>{employee.chG_DATE?.toString()}</td>
            <td>{employee.chG_USER}</td>
            <td>{employee.chG_TERM}</td>
            <td>{employee.location}</td>
            <td>{employee.department}</td>
            <td>{employee.status}</td>
            <td>{employee.changeReason}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default EmployeeTableFormat;
