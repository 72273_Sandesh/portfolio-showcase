/* eslint-disable react-refresh/only-export-components */
/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useContext, useState, useEffect, useMemo } from "react";
import { useQuery } from "react-query";
import {
  createResponseContext,
  initialQueryResponse,
  initialQueryState,
  PaginationState,
  QUERIES,
  stringifyRequestQuery,
  WithChildren,
} from "../../../../../../_metronic/helpers";
import { getEmployee } from "./_requests";
import { Employee } from "./_models";
import { useQueryRequest } from "./QueryRequestProvider";
import {
  globalQuery,
  isFiltered,
  setGlobalQuery,
} from "../../../../GlobalQuery";

const QueryResponseContext =
  createResponseContext<Employee>(initialQueryResponse);
const QueryResponseProvider: FC<WithChildren> = ({ children }) => {
  const { state } = useQueryRequest();
  const [query, setQuery] = useState<string>(stringifyRequestQuery(state));
  const updatedQuery = useMemo(() => stringifyRequestQuery(state), [state]);

  useEffect(() => {
    if (globalQuery !== "") {
      setQuery(globalQuery);
    } else if (query !== updatedQuery) {
      setQuery(updatedQuery);
    }
  }, [updatedQuery]);

  // const {
  //   isFetching,
  //   refetch,
  //   data: response,
  // } = useQuery(
  //   `${QUERIES.EMPLOYEE_LIST}-${query}`,
  //   () => {
  //     return getEmployee(query)
  //   },
  //   {cacheTime: 0, keepPreviousData: true, refetchOnWindowFocus: false}
  // )

  // const {
  //   isFetching,
  //   refetch,
  //   data: response,
  // } = useQuery(
  //   `${QUERIES.EMPLOYEE_LIST}-${query}`,
  //   () => {
  //     if (query.includes("debouncedSearchTerm=") || query.includes("location=") || query.includes("department=") || query.includes("status=")) {
  //       return getEmployee(query);
  //     } else {
  //       return null;
  //     }
  //   },
  //   { cacheTime: 0, keepPreviousData: true, refetchOnWindowFocus: false }
  // );

  const {
    isFetching,
    refetch,
    data: response,
  } = useQuery(
    `${QUERIES.EMPLOYEE_LIST}-${query}`,
    () => {
      if (isFiltered) {
        setGlobalQuery("");
        return getEmployee(query);
      } else if (
        (!isFiltered && query.includes("debouncedSearchTerm=")) ||
        query.includes("location=") ||
        query.includes("department=") ||
        query.includes("status=")
      ) {
        setGlobalQuery("");
        return getEmployee(query);
      }
    },
    { cacheTime: 0, keepPreviousData: true, refetchOnWindowFocus: false }
  );

  return (
    <QueryResponseContext.Provider
      value={{ isLoading: isFetching, refetch, response, query }}
    >
      {children}
    </QueryResponseContext.Provider>
  );
};

const useQueryResponse = () => useContext(QueryResponseContext);

const useQueryResponseData = () => {
  const { response } = useQueryResponse();
  if (!response) {
    return [];
  }

  return response?.data || [];
};

const useQueryResponsePagination = () => {
  const defaultPaginationState: PaginationState = {
    links: [],
    ...initialQueryState,
  };

  const { response } = useQueryResponse();
  if (!response || !response.payload || !response.payload.pagination) {
    return defaultPaginationState;
  }

  return response.payload.pagination;
};

const useQueryResponseLoading = (): boolean => {
  const { isLoading } = useQueryResponse();
  return isLoading;
};

export {
  QueryResponseProvider,
  useQueryResponse,
  useQueryResponseData,
  useQueryResponsePagination,
  useQueryResponseLoading,
};
