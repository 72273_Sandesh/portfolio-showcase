import axios, { AxiosResponse } from "axios";
import { ID, Response } from "../../../../../../_metronic/helpers";
import { Employee, EmployeeQueryResponse } from "./_models";

const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
const getEmployee = (query: string): Promise<EmployeeQueryResponse> => {
  return axios
    .get(`${EMPLOYEE_URL}/GetAllEmployees?${query}`)
    .then((d: AxiosResponse<EmployeeQueryResponse>) => d.data);
};
const getEmployeeById = (id: ID): Promise<Employee | undefined> => {
  return axios
    .get(`${EMPLOYEE_URL}/${id}/EMPPER`)
    .then((response: AxiosResponse<Response<Employee>>) => response.data)
    .then((data: Response<Employee>) => {
      console.log("The data is : ", data); 
      return data.data;
    });
};

const createEmployee = (cadre: Employee): Promise<Employee | undefined> => {
  return axios
    .post(`${EMPLOYEE_URL}/CreateQualification`, cadre)
    .then((response: AxiosResponse<Response<Employee>>) => response.data)
    .then((response: Response<Employee>) => response.data);
};
const updateEmployee = (cadre: Employee): Promise<Employee | undefined> => {
  return axios
    .put(`${EMPLOYEE_URL}/UpdateQualification/${cadre.id}`, cadre)
    .then((response: AxiosResponse<Response<Employee>>) => response.data)
    .then((response: Response<Employee>) => response.data);
};

const deleteEmployee = (departmentId: ID): Promise<void> => {
  return axios.delete(`${EMPLOYEE_URL}/${departmentId}`).then(() => {});
};

const deleteSelectedEmployee = (departmentIds: Array<ID>): Promise<void> => {
  const requests = departmentIds.map((id) =>
    axios.delete(`${EMPLOYEE_URL}/${id}`)
  );
  return axios.all(requests).then(() => {});
};

export {
  getEmployee,
  deleteEmployee,
  deleteSelectedEmployee,
  getEmployeeById,
  createEmployee,
  updateEmployee,
};
