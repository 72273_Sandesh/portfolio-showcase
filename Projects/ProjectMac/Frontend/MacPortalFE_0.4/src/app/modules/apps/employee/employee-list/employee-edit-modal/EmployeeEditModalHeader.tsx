import {KTIcon} from '../../../../../../_metronic/helpers'
import { globalVariable, setGlobalVariable } from '../../GlobalVariable'
import {useListView} from '../core/ListViewProvider'

const EmployeeEditModalHeader = () => {
  const {setItemIdForUpdate} = useListView()
  const openAddModal = () => {
    setItemIdForUpdate(undefined)
    setGlobalVariable("addEmployee")
  }
  return (
    <div className='modal-header'>
      {/* begin::Modal title */}
      {globalVariable == 'addEmployee' && (<h2 className='fw-bolder'>Add New Employee</h2>)
     || globalVariable == 'editEmployee' && (<h2 className='fw-bolder'>Edit Employee</h2>)
      || globalVariable == 'infoEmployee' && (<h2 className='fw-bolder'>Employee Details</h2> )}
      {/* end::Modal title */}

      {/* begin::Close */}
      <div
        className='btn btn-icon btn-sm btn-active-icon-primary'
        data-kt-users-modal-action='close'
        onClick={openAddModal}
        style={{cursor: 'pointer'}}
      >
        <KTIcon iconName='cross' className='fs-1' />
      </div>
      {/* end::Close */}
    </div>
  )
}

export {EmployeeEditModalHeader}



