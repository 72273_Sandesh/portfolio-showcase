// import {useMemo, useState} from 'react'
// import {useTable, ColumnInstance, Row} from 'react-table'
// import {CustomHeaderColumn} from './columns/CustomHeaderColumn'
// import {CustomRow} from './columns/CustomRow'
// import {useQueryResponseData, useQueryResponseLoading} from '../core/QueryResponseProvider'
// import {employeeColumns} from './columns/_columns'
// import {Employee} from '../core/_models'
// import {EmployeeListLoading} from '../components/loading/EmployeeListLoading'
// import {EmployeeListPagination} from '../components/pagination/EmployeeListPagination'
// import {KTCardBody} from '../../../../../../_metronic/helpers'

// const EmployeeTable = () => {
//   const employee = useQueryResponseData()
//   const isLoading = useQueryResponseLoading()
//   const data = useMemo(() => employee, [employee])
//   const columns = useMemo(() => employeeColumns, [])
//   const {getTableProps, getTableBodyProps, headers, rows, prepareRow} = useTable({
//     columns,
//     data,
//   })
//   const [showMessage, setShowMessage] = useState(false);

//   return (
//     <KTCardBody className='py-2'>
//       <div className='table-responsive'>
//         <table
//           id='kt_table_users'
//           className='table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer'
//           {...getTableProps()}
//         >
//           <thead>
//             <tr className='text-start text-muted fw-bolder fs-7 text-uppercase gs-0'>
//               {headers.map((column: ColumnInstance<Employee>) => (
//                 <CustomHeaderColumn key={column.id} column={column} />
//               ))}
//             </tr>
//           </thead>
//           <tbody className='text-gray-600 fw-bold' {...getTableBodyProps()}>
//             {rows.length > 0 ? (
//               rows.map((row: Row<Employee>, i) => {
//                 prepareRow(row)
//                 return <CustomRow row={row} key={`row-${i}-${row.id}`} />
//               })
//             ) : (
//               <tr>
//                 <td colSpan={7}>
//                   {/* <div className='d-flex text-center w-100 align-content-center justify-content-center'>
//                     No matching records found
//                   </div> */}

//  <div>
//       <div
//         className='d-flex text-center w-100 align-content-center justify-content-center'
//       >
//         <span
//           onMouseEnter={() => setShowMessage(true)}
//           onMouseLeave={() => setShowMessage(false)}
//         >
//           No matching records found
//         </span>
//       </div>
//       {showMessage && (
//        <div style={{ color: 'red', textAlign: 'center', marginTop: '20px' }}>
//        Please apply ATLEAST ONE filter...!
//      </div>

//       )}
//     </div>

//                 </td>
//               </tr>
//             )}
//           </tbody>
//         </table>
//       </div>
//       <EmployeeListPagination />
//       {isLoading && <EmployeeListLoading />}
//     </KTCardBody>
//   )
// }

// export {EmployeeTable}

import { useMemo } from "react";
import { useTable, ColumnInstance, Row } from "react-table";
import { CustomHeaderColumn } from "./columns/CustomHeaderColumn";
import { CustomRow } from "./columns/CustomRow";
import {
  useQueryResponseData,
  useQueryResponseLoading,
} from "../core/QueryResponseProvider";
import { employeeColumns } from "./columns/_columns";
import { Employee } from "../core/_models";
import { EmployeeListLoading } from "../components/loading/EmployeeListLoading";
import { EmployeeListPagination } from "../components/pagination/EmployeeListPagination";
import { KTCardBody } from "../../../../../../_metronic/helpers";
import { isFiltered } from "../../../../GlobalQuery";

const EmployeeTable = () => {
  const employee = useQueryResponseData();
  const isLoading = useQueryResponseLoading();
  const data = useMemo(() => employee, [employee]);
  const columns = useMemo(() => employeeColumns, []);
  const { getTableProps, getTableBodyProps, headers, rows, prepareRow } =
    useTable({
      columns,
      data,
    });

  return (
    <KTCardBody className="py-4">
      <div className="table-responsive">
        <table
          id="kt_table_users"
          className="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
          {...getTableProps()}
        >
          <thead>
            <tr className="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
              {headers.map((column: ColumnInstance<Employee>) => (
                <CustomHeaderColumn key={column.id} column={column} />
              ))}
            </tr>
          </thead>
          <tbody className="text-gray-600 fw-bold" {...getTableBodyProps()}>
            {rows.length > 0 ? (
              rows.map((row: Row<Employee>, i) => {
                prepareRow(row);
                return <CustomRow row={row} key={`row-${i}-${row.id}`} />;
              })
            ) : (
              <tr>
                <td colSpan={7}>
                  <div className="text-danger d-flex text-center w-100 align-content-center justify-content-center">
                    {isFiltered
                      ? " No matching records found"
                      : "Please apply at least one filter to proceed!"}
                  </div>
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
      <EmployeeListPagination />
      {/* {isLoading && <EmployeeListLoading />} */}
    </KTCardBody>
  );
};

export { EmployeeTable };
