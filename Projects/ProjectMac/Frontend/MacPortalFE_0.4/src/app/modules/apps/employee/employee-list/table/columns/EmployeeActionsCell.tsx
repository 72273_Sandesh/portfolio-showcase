import { FC, useEffect } from "react";
import { MenuComponent } from "../../../../../../../_metronic/assets/ts/components";
import { ID, KTIcon } from "../../../../../../../_metronic/helpers";
import { useQueryResponse } from "../../core/QueryResponseProvider";
import { Link } from "react-router-dom";
import { useListView } from "../../core/ListViewProvider";

type Props = {
  id: ID;
};

const EmployeeActionsCell: FC<Props> = ({ id }) => {
  const {setItemIdForUpdate} = useListView()
  const {query} = useQueryResponse()
  const currentUserId = { userid: id };
  const queryData = { query: query };

  useEffect(() => {
    MenuComponent.reinitialization();
  }, []);

  // const openInfoModal = () => {
  //   setItemIdForUpdate(id)
  // }

  return (
    <>
      <a
        href="#"
        className="btn btn-light btn-active-light-primary btn-sm"
        data-kt-menu-trigger="click"
        data-kt-menu-placement="bottom-end"
      >
        Actions
        <KTIcon iconName="down" className="fs-5 m-0" />
      </a>
      {/* begin::Menu */}
      <div
        className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
        data-kt-menu="true"
      >
        {/* begin::Menu item */}
        <div className="menu-item px-3">
          <Link
            to="/crafted/pages/profile/overview"
            state={{ userId: currentUserId, queryData: queryData }}
            search={queryData}
            className="menu-link px-3"
          >
            View Profile
          </Link>
        </div>
        {/* end::Menu item */}
        {/* begin::Menu item */}
        {/* <div className='menu-item px-3'>
          <a className='menu-link px-3' onClick={openInfoModal}>
            Edit
          </a>
        </div> */}
        {/* end::Menu item */}
      </div>
      {/* end::Menu */}
    </>
  );
};

export { EmployeeActionsCell };
