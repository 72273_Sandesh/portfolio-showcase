import { ID, Response } from "../../../../../../_metronic/helpers";
export type Grade = {
  id?: ID;
  description?: string;
  grade_code?:string;
  shortDescription?: string;
  cadreCode?: string;
  status?: string;
  lveTableNumber?: string;
  inductionTableNumber?: string;
  noticePeriod?: number | string;
  changeDate?: Date | string;
  changeUser?: string;
  changeTerm?: string;
  approvalTableNumber?: string;
  accountCodeExp?: string;
  costCenterCodeExp?: string;
  gradeType?: string;
  udfNumber1?: number;
  gradeOrder?: number;
  gradeId?: string;
  changeReason?: string;
  remarks?: string;
  cadreName?: string;
  responseStatus?: string;
  message?: string;
  tX_CHG_DATE?: string | Date;
  tX_CHG_USER?: string;
  tX_CHG_TERM?: string;
  cadreCategory?:string;

  fieldNames?: {
    GRADE_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    DESCR: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    SH_DESCR: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CADRE_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    NOTICE_PRD?: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_DATE?: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_USER?: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_TERM: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_REASON: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    STATUS: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
  };
};

export type GradeQueryResponse = Response<Array<Grade>>;

export const initialGrade: Grade = {};
