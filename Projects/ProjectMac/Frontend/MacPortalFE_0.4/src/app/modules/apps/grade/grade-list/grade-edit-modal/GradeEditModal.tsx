import {useEffect} from 'react'
import {GradeEditModalHeader} from './GradeEditModalHeader'
import {GradeEditModalFormWrapper} from './GradeEditModalFormWrapper'

const GradeEditModal = () => {
  useEffect(() => {
    document.body.classList.add('modal-open')
    return () => {
      document.body.classList.remove('modal-open')
    }
  }, [])

  return (
    <>
      <div
        className='modal fade show d-block'
        id='kt_modal_add_user'
        role='dialog'
        tabIndex={-1}
        aria-modal='true'
      >
        {/* begin::Modal dialog */}
        <div className='modal-dialog modal-dialog-centered mw-1000px'>
          {/* begin::Modal content */}
          <div className='modal-content'>
            <GradeEditModalHeader />
            {/* begin::Modal body */}
            <div className='modal-body mx-5 mx-xl-5 my-2'>
              <GradeEditModalFormWrapper />
            </div>
            {/* end::Modal body */}
          </div>
          {/* end::Modal content */}
        </div>
        {/* end::Modal dialog */}
      </div>
      {/* begin::Modal Backdrop */}
      <div className='modal-backdrop fade show'></div>
      {/* end::Modal Backdrop */}
    </>
  )
}

export {GradeEditModal}
