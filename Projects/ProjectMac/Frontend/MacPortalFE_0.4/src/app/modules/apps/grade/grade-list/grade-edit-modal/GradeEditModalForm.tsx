// import { FC, useEffect, useState } from "react";
// import * as Yup from "yup";
// import { useFormik } from "formik";
// import { isNotEmpty, Mode, QUERIES } from "../../../../../../_metronic/helpers";
// import { initialGrade, Grade, Gradetx } from "../core/_models";
// import { useAuth } from "../../../../auth";
// import clsx from "clsx";
// import { useListView } from "../core/ListViewProvider";
// import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
// import { GradeListLoading } from "../components/loading/GradeListLoading";
// import { createGrade, insertTransaction } from "../core/_requests";
// import { useQueryResponse } from "../../../transaction-master/master-list/core/QueryResponseProvider";
// import { toast } from "react-toastify";
// import axios from "axios";
// import { useQueryClient, useMutation } from "react-query";

// type Props = {
//   isGradeLoading: boolean;
//   grade: Grade;
// };
// const API_URL = import.meta.env.VITE_GRADE_DROPDOWNS_API_URL;

// const editGradeSchema = Yup.object().shape({
//   description: Yup.string().required("Description is required"),
//   shortDescription: Yup.string()
//     //.min(3, 'Minimum 3 characters')
//     //.max(500, 'Maximum 500 characters')
//     .required("Short Description is required"),
//   cadreCode: Yup.string()
//     //.min(3, 'Minimum 3 characters')
//     //.max(500, 'Maximum 500 characters')
//     .required("Cadre is required"),
//   noticePeriod: Yup.number().required("Notice Period is required"),
//   status: Yup.string().required("Status is required"),
//   changeReason: Yup.string().min(3, "Minimum 3 characters"),
// });

// type states = {
//   cadreCode: string;
//   cadre: string;
// };
// const GradeEditModalForm: FC<Props> = ({ grade, isGradeLoading }) => {
//   const { setItemIdForUpdate, itemIdForUpdate, formMode } = useListView();
//   const { setItemIdForUpdate: closeForm } = useTxListView();

//   const { refetch } = useQueryResponse();

//   const [cadreCodes, setCadreCodes] = useState<states[]>([]);
//   const [cadreCode, setCadreCode] = useState("");

//   const [statuses] = useState(["ACTIVE", "INACTIVE"]);

//   const [newReason, setNewReason] = useState("");

//   const { currentUser } = useAuth();
//   const { query } = useQueryResponse();
//   const queryClient = useQueryClient();

//   useEffect(() => {
//     axios
//       .get(API_URL)
//       .then((response) => {
//         setCadreCodes(response.data.data);
//       })
//       .catch((error) => console.error("Error fetching combined data:", error));
//   }, []);
//   useEffect(() => {
//     if (formMode === Mode.ADD) {
//       formik.setFieldValue("status", "A");
//     }
//   }, []);

//   const [gradeForEdit] = useState<Grade>({
//     ...grade,
//     description:
//       grade.description ||
//       initialGrade.description ||
//       grade.fieldNames?.DESCR.neW_VALUE,
//     shortDescription:
//       grade.shortDescription ||
//       initialGrade.shortDescription ||
//       grade.fieldNames?.SH_DESCR.neW_VALUE,
//     status:
//       grade.status || initialGrade.status || grade.fieldNames?.STATUS.neW_VALUE,
//     cadreCode:
//       grade.cadreCode ||
//       initialGrade.cadreCode ||
//       grade.fieldNames?.CADRE_CODE.neW_VALUE,
//     noticePeriod:
//       grade.noticePeriod ||
//       initialGrade.noticePeriod ||
//       grade.fieldNames?.NOTICE_PRD?.neW_VALUE,
//     changeDate:
//       grade.changeDate ||
//       initialGrade.changeDate ||
//       grade.fieldNames?.CHG_DATE?.neW_VALUE,
//     changeUser:
//       grade.changeUser ||
//       initialGrade.changeUser ||
//       grade.fieldNames?.CHG_USER?.neW_VALUE,
//     changeTerm:
//       grade.changeTerm ||
//       initialGrade.changeTerm ||
//       grade.fieldNames?.CHG_TERM?.neW_VALUE,
//   });
//   const fetchIpAddress = async () => {};

//   fetchIpAddress();
//   function formatDate(dateTime: string) {
//     const date = new Date(dateTime);
//     const formattedDate = date.toLocaleDateString("en-GB", {
//       day: "numeric",
//       month: "numeric",
//       year: "numeric",
//     });
//     const formattedTime = date.toLocaleTimeString("en-IN", {
//       hour: "numeric",
//       minute: "numeric",
//       second: "numeric",
//       timeZone: "Asia/Kolkata", // Specify the Indian time zone
//     });
//     return `${formattedDate} ${formattedTime}`;
//   }

//   const cancel = (withRefresh?: boolean) => {
//     if (withRefresh) {
//       refetch();
//     }
//     setItemIdForUpdate(undefined);
//   };

//   const createTx = useMutation((payload: any) => insertTransaction(payload), {
//     onSuccess: (response: any) => {
//       const { responseStatus, message } = response;

//       if (responseStatus === "E") {
//         toast.error(
//           <div>
//             <h4 className="alert-heading">Error</h4>

//             <p className="alert alert-danger" role="alert">
//               <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
//             </p>
//           </div>
//         );
//       } else {
//         queryClient.invalidateQueries([
//           `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
//         ]);

//         setItemIdForUpdate(undefined);
//         closeForm(undefined);
//         toast.success(
//           <div>
//             <h4 className="alert-heading">Success</h4>
//             <p className="alert alert-success" role="alert">
//               <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
//             </p>
//           </div>
//         );
//         cancel(true);
//       }
//     },
//     onError:(response: any)=>{
//       toast.error(
//         <div>
//           <h4 className="alert-heading">Error</h4>

//           <p className="alert alert-danger" role="alert">
//             <h6>{response}</h6>
//           </p>
//         </div>
//       );
//     }
//   });

//   const formik = useFormik({
//     initialValues: gradeForEdit,
//     validationSchema: editGradeSchema,
//     onSubmit: async (values, { setSubmitting }) => {
//       setSubmitting(true);
//       console.log("id for grade", values.id);
//       if (isNotEmpty(values.id)) {
//         const payload: Gradetx = {
//           tableName: "GRADE",
//           keyVar: "",
//           wfLevel: 1,
//           conf: "N",
//           siteCode: "S0001",
//           emp: currentUser?.id?.toString() || "",
//           filePath: null,
//           file: null,
//           strTranId: values.fieldNames ? values.id : "",
//           folderPath: null,
//           chG_USER: currentUser?.id?.toString() || "",
//           chG_TERM: "TERM",
//           remarks: grade.changeReason,
//           chG_DATE: new Date(),

//           fieldUpdates: [
//             {
//               fieldName: "GRADE_CODE",
//               oldValue:
//                 grade.gradeId || grade.fieldNames?.GRADE_CODE?.olD_VALUE || "",
//               newValue:
//                 grade.gradeId || grade.fieldNames?.GRADE_CODE?.olD_VALUE || "",
//             },
//             {
//               fieldName: "DESCR",
//               oldValue: grade.description || grade.fieldNames?.DESCR?.olD_VALUE,
//               newValue: values.description,
//             },
//             {
//               fieldName: "SH_DESCR",
//               oldValue:
//                 grade.shortDescription || grade.fieldNames?.SH_DESCR?.olD_VALUE,
//               newValue: values.shortDescription,
//             },
//             {
//               fieldName: "CADRE_CODE",
//               oldValue:
//                 grade.cadreCode || grade.fieldNames?.CADRE_CODE?.olD_VALUE,
//               newValue:
//                 cadreCode ||
//                 values.cadreCode ||
//                 grade.fieldNames?.CADRE_CODE?.olD_VALUE, //|| cadre.banD_CODE,
//             },

//             {
//               fieldName: "NOTICE_PRD",
//               oldValue:
//                 grade.noticePeriod?.toString() ||
//                 grade.fieldNames?.NOTICE_PRD?.olD_VALUE,
//               newValue: values.noticePeriod?.toString(),
//             },
//             {
//               fieldName: "CHG_USER",
//               oldValue:
//                 grade?.changeUser || grade.fieldNames?.CHG_USER?.olD_VALUE,
//               newValue: values?.changeUser,
//             },
//             {
//               fieldName: "CHG_TERM",
//               oldValue:
//                 grade.changeTerm || grade.fieldNames?.CHG_TERM?.olD_VALUE,
//               newValue: "TERM",
//             },
//             {
//               fieldName: "CHG_DATE",
//               oldValue:
//                 grade.changeDate || grade.fieldNames?.CHG_DATE?.olD_VALUE,
//               newValue: new Date(),
//             },
//             {
//               fieldName: "STATUS",
//               oldValue: grade.status || grade.fieldNames?.STATUS?.olD_VALUE,
//               newValue: values.status || grade.status,
//             },
//           ],
//         };
//         createTx.mutate(payload);
//         setSubmitting(false);
//       } else {
//         const payload: Gradetx = {
//           tableName: "GRADE",
//           keyVar: "",
//           wfLevel: 1,
//           conf: "N",
//           siteCode: "S0001",
//           emp: currentUser?.id?.toString() || "",
//           filePath: null,
//           file: null,
//           folderPath: null,
//           chG_USER: currentUser?.id?.toString() || "",
//           chG_TERM: "TERM",
//           chG_DATE: new Date(),
//           remarks: "New Grade added",

//           fieldUpdates: [
//             {
//               fieldName: "GRADE_CODE",
//               oldValue: "",
//               newValue: "",
//             },
//             {
//               fieldName: "DESCR",
//               oldValue: "",
//               newValue: values.description,
//             },
//             {
//               fieldName: "SH_DESCR",
//               oldValue: "",
//               newValue: values.shortDescription,
//             },
//             {
//               fieldName: "CADRE_CODE",
//               oldValue: "",
//               newValue: cadreCode,
//             },
//             {
//               fieldName: "NOTICE_PRD",
//               oldValue: "",
//               newValue: values.noticePeriod?.toString(),
//             },
//             {
//               fieldName: "STATUS",
//               oldValue: "",
//               newValue: values.status,
//             },
//             {
//               fieldName: "CHG_USER",
//               oldValue: "",
//               newValue: currentUser?.id,
//             },
//             {
//               fieldName: "CHG_TERM",
//               oldValue: "",
//               newValue: "TERM",
//             },
//             {
//               fieldName: "CHG_DATE",
//               oldValue: "",
//               newValue: new Date(),
//             },
//           ],
//         };

//         createTx.mutate(payload);
//         setSubmitting(false);
//       }
//     },
//   });

//   return (
//     <>
//       <form
//         id="kt_modal_add_user_form"
//         className="form"
//         onSubmit={formik.handleSubmit}
//         noValidate
//       >
//         <div className="d-flex flex-column " id="kt_modal_add_user_scroll">
//           {formMode !== Mode.ADD && (
//             <div className="row mb-3">
//               <div className="fv-row">
//                 <label className=" fw-bold fs-6 mb-2">Grade Code :</label>
//                 <input
//                   className="form-control form-control-solid mb-3 mb-lg-0"
//                   value={formik.values.id ? formik.values.id : ""}
//                   disabled={formMode === Mode.VIEW || formMode === Mode.EDIT}
//                 />
//               </div>
//             </div>
//           )}
//           <div className="row mb-3">
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required fw-bold fs-6 mb-2">Grade Name</label>
//                 <input
//                   placeholder="Description"
//                   {...formik.getFieldProps("description")}
//                   onChange={(e) =>
//                     formik.setFieldValue(
//                       "description",
//                       e.target.value.toUpperCase()
//                     )
//                   }
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid":
//                         formik.touched.description && formik.errors.description,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.description &&
//                         !formik.errors.description,
//                     }
//                   )}
//                   type="description"
//                   name="description"
//                   autoComplete="off"
//                   disabled={
//                     formik.isSubmitting ||
//                     isGradeLoading ||
//                     formMode === Mode.VIEW
//                   }
//                 />
//                 {formik.touched.description && formik.errors.description && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.description}</span>
//                   </div>
//                 )}
//               </div>
//             </div>
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required fw-bold fs-6 mb-2">
//                   Short Description
//                 </label>
//                 <input
//                   placeholder="Short Description"
//                   {...formik.getFieldProps("shortDescription")}
//                   onChange={(e) =>
//                     formik.setFieldValue(
//                       "shortDescription",
//                       e.target.value.toUpperCase()
//                     )
//                   }
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid":
//                         formik.touched.shortDescription &&
//                         formik.errors.shortDescription,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.shortDescription &&
//                         !formik.errors.shortDescription,
//                     }
//                   )}
//                   type="shortDescription"
//                   name="shortDescription"
//                   autoComplete="off"
//                   disabled={
//                     formik.isSubmitting ||
//                     isGradeLoading ||
//                     formMode === Mode.VIEW
//                   }
//                 />
//                 {formik.touched.shortDescription &&
//                   formik.errors.shortDescription && (
//                     <div className="fv-plugins-message-container text-danger">
//                       <span role="alert">{formik.errors.shortDescription}</span>
//                     </div>
//                   )}
//               </div>
//             </div>
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required  fs-6 fw-bold">Cadre</label>
//                 <select
//                   name="cadreCode"
//                   // className='form-select form-select-solid fw-bolder'
//                   className={clsx(
//                     "form-select form-select-solid fw-bolder",
//                     {
//                       "is-invalid":
//                         formik.touched.cadreCode && formik.errors.cadreCode,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.cadreCode && !formik.errors.cadreCode,
//                     }
//                   )}
//                   disabled={
//                     formik.isSubmitting ||
//                     isGradeLoading ||
//                     formMode === Mode.VIEW
//                   }
//                   onChange={(e) => {
//                     formik.setFieldValue("cadreCode", e.target.value);
//                     setCadreCode(e.target.value);
//                   }}
//                   value={formik.values.cadreCode}
//                 >
//                   <option value=""></option>
//                   {cadreCodes.map((cadreCode) => (
//                     <option
//                       key={cadreCode.cadreCode}
//                       value={cadreCode.cadreCode}
//                     >
//                       {cadreCode.cadre.toUpperCase()}
//                     </option>
//                   ))}
//                 </select>
//                 {formik.touched.cadreCode && formik.errors.cadreCode && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.cadreCode}</span>
//                   </div>
//                 )}
//               </div>
//             </div>

//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className=" fw-bold fs-6 mb-2">
//                   Notice Period(Days)
//                 </label>
//                 <input
//                   placeholder="Notice Period"
//                   {...formik.getFieldProps("noticePeriod")}
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid":
//                         formik.touched.noticePeriod &&
//                         formik.errors.noticePeriod,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.noticePeriod &&
//                         !formik.errors.noticePeriod,
//                     }
//                   )}
//                   type="number"
//                   name="noticePeriod"
//                   autoComplete="off"
//                   disabled={
//                     formik.isSubmitting ||
//                     isGradeLoading ||
//                     formMode === Mode.VIEW
//                   }
//                 />
//                 {formik.touched.noticePeriod && formik.errors.noticePeriod && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.noticePeriod}</span>
//                   </div>
//                 )}
//               </div>
//             </div>
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="fw-bold fs-6 mb-2">Status</label>

//                 <select
//                   className={clsx(
//                     "form-control form-control-solid pl-3 mb-3",
//                     {
//                       "is-invalid":
//                         formik.touched.status && formik.errors.status,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.status && !formik.errors.status,
//                     }
//                   )}
//                   value={formik.values.status}
//                   onChange={(e) => {
//                     formik.setFieldValue("status", e.target.value);
//                   }}
//                   disabled={formMode === Mode.VIEW || formMode === Mode.ADD}
//                 >
//                   {statuses.map((status) => (
//                     <option value={status === "ACTIVE" ? "A" : "N"}>
//                       {status}
//                     </option>
//                   ))}
//                 </select>
//                 {formik.touched.status && formik.errors.status && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.status}</span>
//                   </div>
//                 )}
//               </div>
//             </div>
//             {formMode === Mode.VIEW && (
//               <>
//                 <div className="col-md-6">
//                   <div className="fv-row">
//                     <label className="fw-bold fs-6 mb-2">Change User</label>
//                     <input
//                       className="form-control form-control-solid mb-3 mb-lg-0"
//                       value={formik.values.changeUser?.toUpperCase()}
//                       disabled={formMode === Mode.VIEW}
//                     />
//                   </div>
//                 </div>
//                 <div className="col-md-6">
//                   <div className="fv-row">
//                     <label className="fw-bold fs-6 mb-2">Change Term</label>
//                     <input
//                       className="form-control form-control-solid mb-3 mb-lg-0"
//                       value={formik.values.changeTerm}
//                       disabled={formMode === Mode.VIEW}
//                     />
//                   </div>
//                 </div>
//                 <div className="col-md-6">
//                   <div className="fv-row">
//                     <label className=" fw-bold fs-6 mb-2">Change Date</label>
//                     <input
//                       className="form-control form-control-solid mb-3 mb-lg-0"
//                       value={
//                         formik.values.changeDate
//                           ? formatDate(formik.values?.changeDate.toString())
//                           : ""
//                       }
//                       disabled={formMode === Mode.VIEW}
//                     />
//                   </div>
//                 </div>
//               </>
//             )}
//           </div>
//           {formMode !== Mode.ADD && (
//             <div className="row mb-3">
//               <div className="fv-row">
//                 <label className="required fw-bold fs-6 mb-2">
//                   Change Reason
//                 </label>
//                 <textarea
//                   placeholder="Change Reason"
//                   {...(formMode === Mode.VIEW
//                     ? { ...formik.getFieldProps("changeReason") }
//                     : { newReason })}
//                   onChange={(e) => {
//                     formik.setFieldValue(
//                       "changeReason",
//                       e.target.value.toUpperCase()
//                     );
//                     setNewReason(e.target.value.toUpperCase());
//                   }}
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid":
//                         formik.touched.changeReason &&
//                         formik.errors.changeReason,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.changeReason &&
//                         !formik.errors.changeReason,
//                     }
//                   )}
//                   name="changeReason"
//                   autoComplete="off"
//                   disabled={
//                     formik.isSubmitting ||
//                     isGradeLoading ||
//                     formMode === Mode.VIEW
//                   }
//                 />
//                 {formik.touched.changeReason && formik.errors.changeReason && (
//                   <div className="fv-plugins-message-container">
//                     <span role="alert">{formik.errors.changeReason}</span>
//                   </div>
//                 )}
//               </div>
//             </div>
//           )}
//         </div>
//         <div className="text-center pt-15">
//           {formMode !== Mode.VIEW && (
//             <button
//               type="submit"
//               className="btn btn-primary"
//               disabled={
//                 isGradeLoading ||
//                 formik.isSubmitting ||
//                 !formik.touched ||
//                 (formMode !== Mode.ADD && newReason === "")
//               }
//             >
//               <span className="indicator-label">Submit</span>
//               {(formik.isSubmitting || isGradeLoading) && (
//                 <span className="indicator-progress">
//                   Please wait...{" "}
//                   <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
//                 </span>
//               )}
//             </button>
//           )}
//         </div>
//       </form>
//       {(formik.isSubmitting || isGradeLoading) && <GradeListLoading />}
//     </>
//   );
// };

// export { GradeEditModalForm };

import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { Grade } from "../core/_models";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
import { useQueryResponse } from "../../../transaction-master/master-list/core/QueryResponseProvider";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import { useQueryClient, useMutation } from "react-query";
import Tooltip, { TooltipProps, tooltipClasses } from "@mui/material/Tooltip";
import { styled } from "@mui/material/styles";
import { GradeListLoading } from "../components/loading/GradeListLoading";
import { createTransaction } from "../../../transaction-master/master-list/core/_models";
import { insertTransaction } from "../../../transaction-master/master-list/core/_requests";

const API_URL = import.meta.env.VITE_GRADE_DROPDOWNS_API_URL;
const GRADE_URL = import.meta.env.VITE_GRADE_API_URL;

type Props = {
  isGradeLoading: boolean;
  grade: Grade;
};

interface cadCatDrpdwn {
  cadreCode: string;
  cadre: string;
}

const GradeEditModalForm: FC<Props> = ({ grade, isGradeLoading }) => {
  const { setItemIdForUpdate, formMode } = useListView();
  const { itemIdForUpdate, setItemIdForUpdate: closeForm } = useTxListView();
  const [cadreValues, setCadreValues] = useState<Grade>();
  const { refetch } = useQueryResponse();

  const [cadCatDrpdwns, setCadCatDrpdwns] = useState<cadCatDrpdwn[]>([]);
  const [statuses] = useState([
    { code: "A", descr: "ACTIVE" },
    { code: "N", descr: "INACTIVE" },
  ]);

  const [privateIP, setPrivateIP] = useState();
  const { currentUser } = useAuth();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();

  const [isDataFetched, setIsDataFetched] = useState(false);

  const editGradeSchema = Yup.object().shape({
    description: Yup.string().trim().required("Grade Description is required"),
    shortDescription: Yup.string()
      .trim()
      .required("Grade Short Description is required"),
    cadreCode: Yup.string().trim().required("Grade Category is required"),
    noticePeriod: Yup.string().trim().required("Notice Period is required"),
    status: Yup.string().trim().required("Grade Status is required"),
    changeReason: Yup.string().when([], {
      is: () => formMode !== Mode.ADD,
      then: (schema) =>
        schema
          .trim()
          .required("Change Reason is required")
          .min(5, "Minimum 5 characters"),
      otherwise: (schema) => schema.notRequired(),
    }),
  });

  useEffect(() => {
    axios
      .get(API_URL)
      .then((response) => {
        setCadCatDrpdwns(response.data.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const [gradeForEdit] = useState<Grade>({
    ...grade,
    grade_code: grade.grade_code || grade.fieldNames?.GRADE_CODE?.neW_KEY,
    description: grade.description || grade.fieldNames?.DESCR?.neW_KEY,
    shortDescription:
      grade.shortDescription || grade.fieldNames?.SH_DESCR?.neW_KEY,
    cadreCode: grade.cadreCode || grade.fieldNames?.CADRE_CODE?.neW_KEY,
    cadreName: grade.cadreName || grade.fieldNames?.CADRE_CODE?.neW_KEY,
    status: grade.status || grade.fieldNames?.STATUS?.neW_KEY,
    noticePeriod: grade.noticePeriod || grade.fieldNames?.NOTICE_PRD?.neW_KEY,
    // remarks: formMode === Mode.VIEW ? grade.changeReason || grade.remarks : "",
    changeReason: formMode === Mode.VIEW ? grade.changeReason : "",
    changeTerm:
      grade.changeTerm ||
      grade.tX_CHG_TERM ||
      grade.fieldNames?.CHG_TERM?.neW_KEY,
    changeDate:
      grade.changeDate ||
      grade.tX_CHG_DATE ||
      grade.fieldNames?.CHG_DATE?.neW_KEY,
    changeUser:
      grade.changeUser ||
      grade.tX_CHG_USER ||
      grade.fieldNames?.CHG_USER?.neW_KEY,
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
    closeForm(undefined);
  };

  useEffect(() => {
    if (itemIdForUpdate && !isDataFetched) {
      axios
        .get(
          `${GRADE_URL}/GetGradeByGradeId/${grade.fieldNames?.GRADE_CODE?.neW_KEY}`
        )
        .then((response) => {
          const data = response.data.data;

          setCadreValues(data);
          formik.setValues({
            grade_code:
              grade.fieldNames?.GRADE_CODE?.neW_KEY || data.grade_code,
            description: grade.fieldNames?.DESCR?.neW_KEY || data.description,
            shortDescription:
              grade.fieldNames?.SH_DESCR?.neW_KEY || data.shortDescription,
            cadreCode: grade.fieldNames?.CADRE_CODE?.neW_KEY || data.cadreCode,
            cadreName:
              grade.fieldNames?.CADRE_CODE?.neW_VALUE || data.cadreName,
            status: grade.fieldNames?.STATUS?.neW_KEY || data.status,
            noticePeriod:
              grade.fieldNames?.NOTICE_PRD?.neW_KEY || data.noticePeriod,
            // remarks: formMode === Mode.VIEW ? grade.changeReason || grade.remarks : "",
            changeTerm: grade.fieldNames?.CHG_TERM?.neW_KEY || data.changeTerm,
            changeDate: grade.fieldNames?.CHG_DATE?.neW_KEY || data.changeDate,
            changeUser: grade.fieldNames?.CHG_USER?.neW_KEY || data.changeUser,
          });
          setIsDataFetched(true);
        })
        .catch((error) =>
          console.error("Error fetching combined data:", error)
        );
    }
  }, [itemIdForUpdate, isDataFetched, grade]);

  const createTx = useMutation((payload: any) => insertTransaction(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        closeForm(undefined);
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
        cancel(true);
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formattedDate = (date: Date) => {
    return new Date(date).toLocaleString("en-GB", {
      day: "2-digit",
      month: "2-digit",
      year: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    });
  };

  const formik = useFormik({
    initialValues: gradeForEdit,
    validationSchema: editGradeSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      const payload: createTransaction = {
        tableName: "GRADE",
        keyVar: "",
        wfLevel: 1,
        conf: "N",
        siteCode: "S0001",
        emp: currentUser?.id?.toString() || "",
        filePath: null,
        file: null,
        strTranId: grade.fieldNames ? itemIdForUpdate : null,
        folderPath: null,
        tX_CHG_USER: currentUser?.id?.toString() || "",
        tX_CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        remarks: values.changeReason || "NEW GRADE ADDED",

        fieldUpdates: [
          {
            SEQ_NO: "1",
            FIELD_NAME: "GRADE_CODE",
            FIELD_DESCRIPTION: "GRADE CODE",
            OLD_KEY:
              grade.grade_code || grade.fieldNames?.GRADE_CODE?.olD_KEY || null,
            OLD_VALUE:
              grade.grade_code ||
              grade.fieldNames?.GRADE_CODE?.olD_VALUE ||
              null,
            NEW_KEY:
              grade.grade_code ||
              grade.fieldNames?.GRADE_CODE?.olD_VALUE ||
              null,
            NEW_VALUE:
              grade.grade_code ||
              grade.fieldNames?.GRADE_CODE?.olD_VALUE ||
              null,
            FILE_PATH: "",
          },
          ...((itemIdForUpdate !== undefined &&
            grade.fieldNames?.DESCR?.olD_KEY === values.description) ||
          (grade.description !== values.description &&
            cadreValues?.description !== values.description)
            ? [
                {
                  SEQ_NO: "2",
                  FIELD_NAME: "DESCR",
                  FIELD_DESCRIPTION: "DESCRIPTION",
                  OLD_KEY:
                    grade.description ||
                    grade.fieldNames?.DESCR?.olD_KEY ||
                    cadreValues?.description ||
                    "",
                  OLD_VALUE:
                    grade.description ||
                    grade.fieldNames?.DESCR?.olD_VALUE ||
                    cadreValues?.description ||
                    "",
                  NEW_KEY: values.description,
                  NEW_VALUE: values.description,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            grade.fieldNames?.SH_DESCR?.olD_KEY === values.shortDescription) ||
          (grade.shortDescription !== values.shortDescription &&
            cadreValues?.shortDescription !== values.shortDescription)
            ? [
                {
                  SEQ_NO: "3",
                  FIELD_NAME: "SH_DESCR",
                  FIELD_DESCRIPTION: "SHORT DESCRIPTION",
                  OLD_KEY:
                    grade.shortDescription ||
                    grade.fieldNames?.SH_DESCR?.olD_KEY ||
                    cadreValues?.shortDescription ||
                    "",
                  OLD_VALUE:
                    grade.shortDescription ||
                    grade.fieldNames?.SH_DESCR?.olD_VALUE ||
                    cadreValues?.shortDescription ||
                    "",
                  NEW_KEY: values.shortDescription,
                  NEW_VALUE: values.shortDescription,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            grade.fieldNames?.CADRE_CODE?.olD_KEY === values.cadreCode) ||
          (grade.cadreCode !== values.cadreCode &&
            cadreValues?.cadreCode !== values.cadreCode)
            ? [
                {
                  SEQ_NO: "4",
                  FIELD_NAME: "CADRE_CODE",
                  FIELD_DESCRIPTION: "CADRE",
                  OLD_KEY:
                    grade.cadreCode ||
                    grade.fieldNames?.CADRE_CODE?.olD_KEY ||
                    cadreValues?.cadreCode ||
                    "",
                  OLD_VALUE:
                    grade.cadreName ||
                    grade.fieldNames?.CADRE_CODE?.olD_VALUE ||
                    cadreValues?.cadreName ||
                    "",
                  NEW_KEY: values.cadreCode,
                  NEW_VALUE: values.cadreName,
                  FILE_PATH: "",
                },
              ]
            : []),

          ...((itemIdForUpdate !== undefined &&
            grade.fieldNames?.STATUS?.olD_KEY === values.status) ||
          (grade.status !== values.status &&
            cadreValues?.status !== values.status)
            ? [
                {
                  SEQ_NO: "5",
                  FIELD_NAME: "STATUS",
                  FIELD_DESCRIPTION: "STATUS",
                  OLD_KEY:
                    grade.status ||
                    grade.fieldNames?.STATUS?.olD_KEY ||
                    cadreValues?.status ||
                    "",
                  OLD_VALUE:
                    grade.status === "A"
                      ? "ACTIVE"
                      : grade.status === "N"
                      ? "INACTIVE"
                      : "" || grade.fieldNames?.STATUS?.olD_KEY === "A"
                      ? "ACTIVE"
                      : grade.fieldNames?.STATUS?.olD_KEY === "N"
                      ? "INACTIVE"
                      : "" || cadreValues?.status === "A"
                      ? "ACTIVE"
                      : cadreValues?.status === "N"
                      ? "INACTIVE"
                      : "",
                  NEW_KEY: values.status,
                  NEW_VALUE:
                    values.status === "A"
                      ? "ACTIVE"
                      : values.status === "N"
                      ? "INACTIVE"
                      : "",
                  FILE_PATH: "",
                },
              ]
            : []),

          ...((itemIdForUpdate !== undefined &&
            grade.fieldNames?.NOTICE_PRD?.olD_KEY === values.noticePeriod) ||
          (grade.noticePeriod !== values.noticePeriod &&
            cadreValues?.noticePeriod !== values.noticePeriod)
            ? [
                {
                  SEQ_NO: "6",
                  FIELD_NAME: "NOTICE_PRD",
                  FIELD_DESCRIPTION: "NOTICE PERIOD",
                  OLD_KEY:
                    grade.noticePeriod?.toString() ||
                    grade.fieldNames?.NOTICE_PRD?.olD_KEY?.toString() ||
                    cadreValues?.noticePeriod?.toString() ||
                    "",
                  OLD_VALUE:
                    grade.noticePeriod?.toString() ||
                    grade.fieldNames?.NOTICE_PRD?.olD_VALUE?.toString() ||
                    cadreValues?.noticePeriod?.toString() ||
                    "",
                  NEW_KEY: values.noticePeriod,
                  NEW_VALUE: values.noticePeriod,
                  FILE_PATH: "",
                },
              ]
            : []),

          {
            SEQ_NO: "7",
            FIELD_NAME: "CHG_USER",
            FIELD_DESCRIPTION: "CHANGE USER",
            OLD_KEY:
              grade.changeUser ||
              grade.fieldNames?.CHG_USER?.olD_KEY ||
              cadreValues?.changeUser ||
              "",
            OLD_VALUE:
              grade.changeUser ||
              grade.fieldNames?.CHG_USER?.olD_VALUE ||
              cadreValues?.changeUser ||
              "",
            NEW_KEY: currentUser?.id?.toString() || "",
            NEW_VALUE: currentUser?.emp_Name || "",
            FILE_PATH: "",
          },
          {
            SEQ_NO: "8",
            FIELD_NAME: "CHG_TERM",
            FIELD_DESCRIPTION: "CHANGE TERM",
            OLD_KEY:
              grade.changeTerm ||
              grade.fieldNames?.CHG_TERM?.olD_KEY ||
              cadreValues?.changeTerm ||
              "",
            OLD_VALUE:
              grade.changeTerm ||
              grade.fieldNames?.CHG_TERM?.olD_VALUE ||
              cadreValues?.changeTerm ||
              "",
            NEW_KEY: privateIP,
            NEW_VALUE: privateIP,
            FILE_PATH: "",
          },

          {
            SEQ_NO: "9",
            FIELD_NAME: "CHG_DATE",
            FIELD_DESCRIPTION: "CHANGE DATE",
            OLD_KEY:
              grade.changeDate ||
              grade.fieldNames?.CHG_DATE?.olD_KEY ||
              cadreValues?.changeDate ||
              "",
            OLD_VALUE:
              (grade.changeDate && formattedDate(grade.changeDate)) ||
              (cadreValues?.changeDate &&
                formattedDate(cadreValues?.changeDate)) ||
              (grade.fieldNames?.CHG_DATE?.olD_VALUE &&
                formattedDate(
                  grade.changeDate || grade.fieldNames?.CHG_DATE?.olD_VALUE
                )) ||
              "",
            NEW_KEY: new Date(),
            NEW_VALUE: formattedDate(new Date()),
            FILE_PATH: "",
          },
          {
            SEQ_NO: "10",
            FIELD_NAME: "CHG_REASON",
            FIELD_DESCRIPTION: "CHANGE REASON",
            OLD_KEY:
              grade.changeReason || grade.fieldNames?.CHG_REASON?.olD_KEY || "",
            OLD_VALUE:
              grade.changeReason || grade.fieldNames?.CHG_TERM?.olD_VALUE || "",
            NEW_KEY: values.changeReason || "NEW GRADE ADDED",
            NEW_VALUE: values.changeReason || "NEW GRADE ADDED",
            FILE_PATH: "",
          },
        ],
      };
      createTx.mutate(payload);

      setSubmitting(false);
    },
  });

  useEffect(() => {
    if (formMode === Mode.ADD) {
      formik.setFieldValue("status", "A");
    }
  }, []);

  function formatDate(dateString: string) {
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata",
    });
    return `${formattedDate} ${formattedTime}`;
  }

  const LightTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: theme.palette.common.white,
      color: "rgba(0, 0, 0, 0.87)",
      boxShadow: theme.shadows[1],
      fontSize: 15,
    },
  }));

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column" id="kt_modal_add_user_scroll">
          {/* Begin: Edit Row */}
          <div className="row mb-1">
            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="description"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DESCRIPTION
                </label>
                <input
                  placeholder="Name"
                  {...formik.getFieldProps("description")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "description",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "bg-light-warning": grade.fieldNames?.DESCR?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.description && formik.errors.description,
                    },
                    {
                      "is-valid":
                        formik.touched.description &&
                        !formik.errors.description,
                    }
                  )}
                  type="text"
                  name="description"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(grade.fieldNames?.DESCR && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      grade.fieldNames?.DESCR?.olD_VALUE === null
                        ? "-"
                        : grade.fieldNames?.DESCR?.olD_VALUE
                    }`,
                  })}
                />
              </div>
              {formik.touched.description && formik.errors.description && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.description}</span>
                </div>
              )}
            </div>

            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="shortDescription"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SHORT DESCR.
                </label>
                <input
                  placeholder="Short Description"
                  {...formik.getFieldProps("shortDescription")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "shortDescription",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "bg-light-warning": grade.fieldNames?.SH_DESCR?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.shortDescription &&
                        formik.errors.shortDescription,
                    },
                    {
                      "is-valid":
                        formik.touched.shortDescription &&
                        !formik.errors.shortDescription,
                    }
                  )}
                  type="text"
                  name="shortDescription"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(grade.fieldNames?.SH_DESCR && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      grade.fieldNames?.SH_DESCR?.olD_VALUE === null
                        ? "-"
                        : grade.fieldNames?.SH_DESCR?.olD_VALUE
                    }`,
                  })}
                />
              </div>
              {formik.touched.shortDescription &&
                formik.errors.shortDescription && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.shortDescription}</span>
                  </div>
                )}
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}

            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="cadreCode"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CADRE
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        grade.fieldNames?.CADRE_CODE?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.cadreCode && formik.errors.cadreCode,
                    },
                    {
                      "is-valid":
                        formik.touched.cadreCode && !formik.errors.cadreCode,
                    }
                  )}
                  {...formik.getFieldProps("cadreCode")}
                  onChange={(e) => {
                    const selectedValue = e.target.value;
                    const selectedOption = cadCatDrpdwns.find(
                      (cadCatDrpdwn) => cadCatDrpdwn.cadreCode === selectedValue
                    );

                    formik.setFieldValue("cadreCode", selectedValue);
                    formik.setFieldValue(
                      "cadreName",
                      selectedOption?.cadre || ""
                    );
                  }}
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(grade.fieldNames?.CADRE_CODE && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      grade.fieldNames?.CADRE_CODE?.olD_VALUE === null
                        ? "-"
                        : grade.fieldNames?.CADRE_CODE?.olD_VALUE
                    }`,
                  })}
                >
                  <option value=""></option>
                  {cadCatDrpdwns.map((cadCatDrpdwn) => (
                    <option
                      key={cadCatDrpdwn.cadreCode}
                      value={cadCatDrpdwn.cadreCode}
                    >
                      {cadCatDrpdwn.cadre}
                    </option>
                  ))}
                </select>
              </div>
              {formik.touched.cadreCode && formik.errors.cadreCode && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.cadreCode}</span>
                </div>
              )}
            </div>

            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="status"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  STATUS
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    { "bg-light-warning": grade.fieldNames?.STATUS?.neW_VALUE },
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  {...formik.getFieldProps("status")}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                  }}
                  disabled={formMode === Mode.ADD || formMode === Mode.VIEW}
                  {...(grade.fieldNames?.STATUS && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      grade.fieldNames?.STATUS?.olD_VALUE === null
                        ? "-"
                        : grade.fieldNames?.STATUS?.olD_VALUE
                    }`,
                  })}
                >
                  <option value=""></option>
                  {statuses.map((status) => (
                    <option value={status.code}>{status.descr}</option>
                  ))}
                </select>
              </div>
              {formik.touched.status && formik.errors.status && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.status}</span>
                </div>
              )}
            </div>

            {/* End: Edit Row */}

            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="noticePeriod"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  NOTICE PERIOD
                </label>
                <input
                  placeholder="Notice period"
                  {...formik.getFieldProps("noticePeriod")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "noticePeriod",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        grade.fieldNames?.NOTICE_PRD?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.noticePeriod &&
                        formik.errors.noticePeriod,
                    },
                    {
                      "is-valid":
                        formik.touched.noticePeriod &&
                        !formik.errors.noticePeriod,
                    }
                  )}
                  type="number"
                  name="noticePeriod"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(grade.fieldNames?.NOTICE_PRD && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      grade.fieldNames?.NOTICE_PRD?.olD_VALUE === null
                        ? "-"
                        : grade.fieldNames?.NOTICE_PRD?.olD_VALUE
                    }`,
                  })}
                />
              </div>
              {formik.touched.noticePeriod && formik.errors.noticePeriod && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.noticePeriod}</span>
                </div>
              )}
            </div>

            {formMode !== Mode.ADD && (
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="changeReason"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CHANGE REASON
                  </label>
                  <input
                    placeholder="Change Reason"
                    {...formik.getFieldProps("changeReason")}
                    onChange={(e) =>
                      formik.setFieldValue(
                        "changeReason",
                        e.target.value.toUpperCase()
                      )
                    }
                    className={clsx(
                      "form-control form-control-solid pl-3 mb-3",
                      {
                        "is-invalid":
                          formik.touched.changeReason &&
                          formik.errors.changeReason,
                      },
                      {
                        "is-valid":
                          formik.touched.changeReason &&
                          !formik.errors.changeReason,
                      }
                    )}
                    type="text"
                    name="changeReason"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isGradeLoading ||
                      formMode === Mode.VIEW
                    }
                  />
                </div>

                {formik.touched.changeReason && formik.errors.changeReason ? (
                  <div className="text-danger">
                    {formik.errors.changeReason}
                  </div>
                ) : null}
              </div>
            )}

            {/* Begin: Edit Row */}
            {formMode === Mode.VIEW && (
              <>
                <div className="col-lg-6">
                  <div className="d-flex align-items-center mb-1">
                    <label
                      htmlFor="changeTerm"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CHANGE TERM
                    </label>
                    <input
                      {...formik.getFieldProps("changeTerm")}
                      className="form-control form-control-solid pl-3 mb-3"
                      type="text"
                      name="changeTerm"
                      autoComplete="off"
                      disabled={
                        formik.isSubmitting ||
                        isGradeLoading ||
                        formMode === Mode.VIEW
                      }
                    />
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="d-flex align-items-center mb-1">
                    <label
                      htmlFor="changeUser"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CHANGE USER
                    </label>
                    <input
                      placeholder=""
                      {...formik.getFieldProps("changeUser")}
                      className="form-control form-control-solid pl-3 mb-3"
                      type="text"
                      name="changeUser"
                      autoComplete="off"
                      disabled={
                        formik.isSubmitting ||
                        isGradeLoading ||
                        formMode === Mode.VIEW
                      }
                    />
                  </div>
                </div>
              </>
            )}
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}

            {formMode === Mode.VIEW && (
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="changeDate"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE DATE
                </label>
                <input
                  {...formik.getFieldProps("changeDate")}
                  className="form-control form-control-solid pl-3 mb-3"
                  type="text"
                  name="changeDate"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isGradeLoading ||
                    formMode === Mode.VIEW
                  }
                  value={
                    formik.values.changeDate
                      ? formatDate(formik.values?.changeDate.toString())
                      : ""
                  }
                />
              </div>
            )}
          </div>
          {/* End: Edit Row */}
        </div>
        {/* </div> */}
        <div className="text-center pt-15">
          {formMode !== Mode.VIEW && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isGradeLoading ||
                formik.isSubmitting ||
                !formik.touched ||
                !formik.isValid
              }
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isGradeLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isGradeLoading) && <GradeListLoading />}
    </>
  );
};

export { GradeEditModalForm };
