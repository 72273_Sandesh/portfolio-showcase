import { ID, Response } from "../../../../../../_metronic/helpers";
export type Qualification = {
  //Qualification previous fields
  id?: ID;
  qual_code?: string;
  name?: string;
  shortdescr?: string;
  eqival?: string;
  type?: string;
  typeDescr?: string;
  changeDate?: Date | string;
  changeUser?: string;
  changeTerm?: string;
  status?: string;
  qualSalStrucDesc?: string;
  changeReason?: string;
  responseStatus?: string;
  message?: string;

  //new fields used for tx
  wF_PROCESS_LEVEL?: number;
  emP_CODE?: string;
  remarks?: string;
  SessionempCode?: string;
  moduleType?: string;
  tX_CHG_DATE?: string | Date;
  tX_CHG_USER?: string;
  tX_CHG_TERM?: string;
  
  fieldNames?: {
    QLF_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    DESCR: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    SH_DESCR: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    EQIVAL: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    TYPE?: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    STATUS: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_TERM?: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_DATE?: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_USER: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_REASON: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
  };
};

export type QualificationQueryResponse = Response<Array<Qualification>>;

export const initialQualification: Qualification = {};
