// import { FC, useEffect, useState } from "react";
// import * as Yup from "yup";
// import { useFormik } from "formik";
// import { isNotEmpty, QUERIES, Mode } from "../../../../../../_metronic/helpers";
// import {
//   initialQualification,
//   Qualification,
//   Qualificationtx,
// } from "../core/_models";
// import { useAuth } from "../../../../auth";
// import clsx from "clsx";
// import { useListView } from "../core/ListViewProvider";
// import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
// import { QualificationListLoading } from "../components/loading/QualificationListLoading";
// import {
//   createQualification,
//   insertTransaction,
//   updateQualification,
// } from "../core/_requests";
// import { useQueryResponse } from "../../../transaction-master/master-list/core/QueryResponseProvider";
// import { toast } from "react-toastify";
// import "react-toastify/dist/ReactToastify.css";
// import axios from "axios";
// import { useQueryClient, useMutation } from "react-query";
// const API_URL = import.meta.env.VITE_QUAL_DROPDOWNS_API_URL;

// type Props = {
//   isQualificationLoading: boolean;
//   qualification: Qualification;
// };

// const editQualificationSchema = Yup.object().shape({
//   shortdescr: Yup.string().required("Short Description is required"),
//   name: Yup.string().required("Description is required"),
//   changeReason: Yup.string(),
//   eqival: Yup.string().required("Salary description is required"),
//   status: Yup.string().required("Status is required"),
//   type: Yup.string().required("Qualification type is required"),
// });
// type states = {
//   traN_ID: string;
//   descr: string;
// };

// const QualificationEditModalForm: FC<Props> = ({
//   qualification,
//   isQualificationLoading,
// }) => {
//   const { setItemIdForUpdate, formMode } = useListView();
//   const { setItemIdForUpdate: closeForm } = useTxListView();
//   const { refetch } = useQueryResponse();

//   const [qualSalStrucDescs, setQualSalStrucDescs] = useState<states[]>([]);
//   const [QualSalStrucDesc, setQualSalStrucDesc] = useState("");

//   const [statuses] = useState(["ACTIVE", "INACTIVE"]);

//   const { query } = useQueryResponse();
//   const queryClient = useQueryClient();

//   const [types] = useState([
//     "DIPLOMA",
//     "UNDER GRADUATE",
//     "GRADUATE",
//     "POST GRADUATE",
//   ]);
//   const [type, setType] = useState("");

//   const [changeReason, setChangeReason] = useState<string>("");

//   const { currentUser } = useAuth();
//   useEffect(() => {
//     axios
//       .get(API_URL)
//       .then((response) => {
//         setQualSalStrucDescs(response.data.data);
//       })
//       .catch((error) => console.error("Error fetching combined data:", error));
//   }, []);

//   const [qualificationForEdit] = useState<Qualification>({
//     ...qualification,
//     name:
//       qualification?.name ||
//       initialQualification?.name ||
//       qualification?.fieldNames?.DESCR.neW_VALUE,
//     shortdescr:
//       qualification?.shortdescr ||
//       initialQualification?.shortdescr ||
//       qualification?.fieldNames?.SH_DESCR.neW_VALUE,
//     status:
//       qualification?.status ||
//       initialQualification?.status ||
//       qualification?.fieldNames?.STATUS.neW_VALUE,
//     type:
//       qualification?.type ||
//       initialQualification?.type ||
//       qualification?.fieldNames?.TYPE?.neW_VALUE,
//     eqival:
//       qualification?.eqival ||
//       initialQualification?.eqival ||
//       qualification?.fieldNames?.EQIVAL.neW_VALUE,
//   });

//   const closeModal = () => {
//     closeForm(undefined);
//   };

//   const cancel = (withRefresh?: boolean) => {
//     if (withRefresh) {
//       refetch();
//     }
//     setItemIdForUpdate(undefined);
//   };

//   const createTx = useMutation((payload: any) => insertTransaction(payload), {
//     onSuccess: (response: any) => {
//       const { responseStatus, message } = response;

//       if (responseStatus === "E") {
//         toast.error(
//           <div>
//             <h4 className="alert-heading">Error</h4>

//             <p className="alert alert-danger" role="alert">
//               <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
//             </p>
//           </div>
//         );
//       } else {
//         queryClient.invalidateQueries([
//           `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
//         ]);

//         setItemIdForUpdate(undefined);
//         closeForm(undefined);
//         toast.success(
//           <div>
//             <h4 className="alert-heading">Success</h4>
//             <p className="alert alert-success" role="alert">
//               <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
//             </p>
//           </div>
//         );
//         cancel(true);
//       }
//     },
//     onError:(response: any)=>{
//       toast.error(
//         <div>
//           <h4 className="alert-heading">Error</h4>

//           <p className="alert alert-danger" role="alert">
//             <h6>{response.message}</h6>
//           </p>
//         </div>
//       );
//     }
//   });

//   const formik = useFormik({
//     initialValues: qualificationForEdit,
//     validationSchema: editQualificationSchema,

//     onSubmit: async (values, { setSubmitting }) => {
//       setSubmitting(true);

//       if (isNotEmpty(values.id)) {
//         const payload: Qualificationtx = {
//           tableName: "QUALIFICATION",
//           keyVar: "",
//           wfLevel: 1,
//           conf: "N",
//           siteCode: "S0001",
//           emp: currentUser?.id?.toString() || "",
//           filePath: null,
//           file: null,
//           strTranId: values.fieldNames ? values.id : "",
//           folderPath: null,
//           chG_USER: currentUser?.id?.toString() || "",
//           chG_TERM: "TERM",
//           remarks: qualification?.changeReason,
//           chG_DATE: new Date(),

//           fieldUpdates: [
//             {
//               fieldName: "QLF_CODE",
//               oldValue:
//                 qualification.qual_code ||
//                 qualification.fieldNames?.QLF_CODE.olD_VALUE ||
//                 "",
//               newValue:
//                 values.qual_code ||
//                 qualification.fieldNames?.QLF_CODE?.olD_VALUE ||
//                 "",
//             },
//             {
//               fieldName: "DESCR",
//               oldValue:
//                 qualification.name ||
//                 qualification.fieldNames?.DESCR?.olD_VALUE,
//               newValue: values.name,
//             },
//             {
//               fieldName: "SH_DESCR",
//               oldValue:
//                 qualification.shortdescr ||
//                 qualification.fieldNames?.SH_DESCR?.olD_VALUE,
//               newValue: values.shortdescr,
//             },
//             {
//               fieldName: "STATUS",
//               oldValue:
//                 qualification.status ||
//                 qualification.fieldNames?.STATUS?.olD_VALUE,
//               newValue: values.status,
//             },
//             {
//               fieldName: "EQIVAL",
//               oldValue:
//                 qualification.eqival ||
//                 qualification.fieldNames?.EQIVAL?.olD_VALUE,
//               newValue:
//                 values.eqival || qualification.fieldNames?.EQIVAL?.olD_VALUE,
//             },
//             {
//               fieldName: "TYPE",
//               oldValue:
//                 qualification.type || qualification.fieldNames?.TYPE?.olD_VALUE,
//               newValue: values.type,
//             },
//             {
//               fieldName: "CHG_DATE",
//               oldValue:
//                 qualification.changeDate ||
//                 qualification.fieldNames?.CHG_DATE?.olD_VALUE,
//               newValue: new Date(),
//             },
//             {
//               fieldName: "CHG_TERM",
//               oldValue:
//                 qualification.changeTerm ||
//                 qualification.fieldNames?.CHG_TERM?.olD_VALUE,
//               newValue: "TERM",
//             },
//             {
//               fieldName: "CHG_USER",
//               oldValue:
//                 qualification.changeUser ||
//                 qualification.fieldNames?.CHG_USER?.olD_VALUE,
//               newValue: currentUser?.id?.toString() || "",
//             },
//           ],
//         };
//         createTx.mutate(payload);
//         setSubmitting(false);
//       } else {
//         const payload: Qualificationtx = {
//           tableName: "QUALIFICATION",
//           keyVar: "",
//           wfLevel: 1,
//           conf: "N",
//           siteCode: "S0001",
//           emp: currentUser?.id?.toString() || "",
//           filePath: null,
//           file: null,
//           folderPath: null,
//           chG_USER: currentUser?.id?.toString() || "",
//           chG_TERM: "TERM",
//           chG_DATE: new Date(),
//           remarks: "New Qualification Added",

//           fieldUpdates: [
//             {
//               fieldName: "QLF_CODE",
//               oldValue: "",
//               newValue: "",
//             },
//             {
//               fieldName: "DESCR",
//               oldValue: "",
//               newValue: values.name,
//             },
//             {
//               fieldName: "SH_DESCR",
//               oldValue: "",
//               newValue: values.shortdescr,
//             },
//             {
//               fieldName: "TYPE",
//               oldValue: "",
//               newValue: values.type,
//             },
//             {
//               fieldName: "EQIVAL",
//               oldValue: "",
//               newValue: values.eqival,
//             },
//             {
//               fieldName: "STATUS",
//               oldValue: "",
//               newValue: values.status,
//             },
//             {
//               fieldName: "CHG_TERM",
//               oldValue: "",
//               newValue: "TERM",
//             },
//             {
//               fieldName: "CHG_DATE",
//               oldValue: "",
//               newValue: new Date(),
//             },
//             {
//               fieldName: "CHG_USER",
//               oldValue: "",
//               newValue: currentUser?.id?.toString() || "",
//             },
//           ],
//         };

//         createTx.mutate(payload);
//         setSubmitting(false);
//       }
//     },
//   });

//   function formatDate(dateString: string) {
//     const date = new Date(dateString);
//     const formattedDate = date.toLocaleDateString("en-GB", {
//       day: "numeric",
//       month: "numeric",
//       year: "numeric",
//     });
//     const formattedTime = date.toLocaleTimeString("en-IN", {
//       hour: "numeric",
//       minute: "numeric",
//       second: "numeric",
//       timeZone: "Asia/Kolkata", // Specify the Indian time zone
//     });
//     return `${formattedDate} ${formattedTime}`;
//   }
//   useEffect(() => {
//     if (formMode === Mode.ADD) {
//       formik.setFieldValue("status", "A");
//     }
//   }, []);

//   return (
//     <>
//       <form
//         id="kt_modal_add_user_form"
//         className="form"
//         onSubmit={formik.handleSubmit}
//         noValidate
//       >
//         <div className="d-flex flex-column" id="kt_modal_add_user_scroll">
//           <div className="row mb-3">
//             {formMode !== Mode.ADD && (
//               <div className="">
//                 <div className="fv-row">
//                   <label className=" fw-bold fs-6 mb-2">
//                     Qualification Code :
//                   </label>
//                   <input
//                     className="form-control form-control-solid mb-3 mb-lg-0"
//                     value={formik.values.id ? formik.values.id : ""}
//                   />
//                 </div>
//               </div>
//             )}

//             {/* //here "col-md-6" is for locating two input options aside to each others  */}
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required fw-bold fs-6 mb-2">
//                   Name of Qualification
//                 </label>
//                 <input
//                   placeholder="name"
//                   value={formik.values.name?.toUpperCase()}
//                   // {...formik.getFieldProps('name')}
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     { "is-invalid": formik.touched.name && formik.errors.name },
//                     { "is-valid": formik.touched.name && !formik.errors.name }
//                   )}
//                   type="text"
//                   name="name"
//                   autoComplete="off"
//                   disabled={
//                     formik.isSubmitting ||
//                     isQualificationLoading ||
//                     formMode === Mode.VIEW
//                   }
//                   onChange={(e) => {
//                     formik.setFieldValue("name", e.target.value.toUpperCase());
//                   }}
//                 />
//                 {formik.touched.name && formik.errors.name && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.name}</span>
//                   </div>
//                 )}
//               </div>
//             </div>

//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="required fw-bold fs-6 mb-2">
//                   {" "}
//                   Short Description
//                 </label>
//                 <input
//                   placeholder="Short Description"
//                   value={formik.values.shortdescr?.toUpperCase()}
//                   // {...formik.getFieldProps('shortdescr')}
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid":
//                         formik.touched.shortdescr && formik.errors.shortdescr,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.shortdescr && !formik.errors.shortdescr,
//                     }
//                   )}
//                   type="text"
//                   name="shortdescr"
//                   autoComplete="off"
//                   disabled={
//                     formik.isSubmitting ||
//                     isQualificationLoading ||
//                     formMode === Mode.VIEW
//                   }
//                   onChange={(e) => {
//                     formik.setFieldValue(
//                       "shortdescr",
//                       e.target.value.toUpperCase()
//                     );
//                   }}
//                 />
//                 {formik.touched.shortdescr && formik.errors.shortdescr && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.shortdescr}</span>
//                   </div>
//                 )}
//               </div>
//             </div>

//             {/* {begin : Category} */}
//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="fs-6 fw-bold">Salary Structure:</label>
//                 {/* <select
//                   // className="form-select form-select-solid fw-bolder"
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid":
//                         formik.touched.qualSalStrucDesc &&
//                         formik.errors.qualSalStrucDesc,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.qualSalStrucDesc &&
//                         !formik.errors.qualSalStrucDesc,
//                     }
//                   )}
//                   onChange={(e) => {
//                     formik.setFieldValue("qualSalStrucDesc", e.target.value);
//                     setQualSalStrucDesc(e.target.value);
//                   }}
//                   value={qualSalStrucDesc}
//                   disabled={formMode === Mode.VIEW}
//                 >
//                   <option value="cadreCategory">
//                     {formik.values.qualSalStrucDesc}
//                   </option>
//                   {qualSalStrucDescs.map((qualSalStrucDesc) => (
//                     <option
//                       key={qualSalStrucDesc.traN_ID}
//                       value={qualSalStrucDesc.traN_ID}
//                     >
//                       {qualSalStrucDesc.name}
//                     </option>
//                   ))}
//                 </select> */}

//                 <select
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid":
//                         formik.touched.qualSalStrucDesc &&
//                         formik.errors.qualSalStrucDesc,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.qualSalStrucDesc &&
//                         !formik.errors.qualSalStrucDesc,
//                     }
//                   )}
//                   //formik.values?.fieldNames?.EQIVAL?.neW_VALUE
//                   onChange={(e) => {
//                     formik.setFieldValue("eqival", e.target.value);
//                     setQualSalStrucDesc(e.target.value);
//                   }}
//                   value={formik.values.eqival}
//                   disabled={formMode === Mode.VIEW}
//                 >
//                   <option value=""></option>
//                   {qualSalStrucDescs.map((e) => (
//                     <option key={e.traN_ID} value={e.traN_ID}>
//                       {e?.name?.toUpperCase()}
//                     </option>
//                   ))}
//                 </select>

//                 {/* <select
//                      className={clsx(
//                       "form-control form-control-solid mb-3 mb-lg-0",
//                       {
//                         "is-invalid":
//                           formik.touched.qualSalStrucDesc &&
//                           formik.errors.qualSalStrucDesc,
//                       },
//                       {
//                         "is-valid":
//                           formik.touched.qualSalStrucDesc &&
//                           !formik.errors.qualSalStrucDesc,
//                       }
//                     )}
//                     onChange={(e) => {
//                       formik.setFieldValue("qualSalStrucDesc", e.target.value);
//                       setQualSalStrucDesc(e.target.value);
//                     }}
//                       value={qualSalStrucDesc
//                         // qualSalStrucDescs.find(
//                         //   (qualSalStrucDesc) =>
//                         //     qualSalStrucDesc.traN_ID ===
//                         //     formik.values?.fieldNames?.EQIVAL?.neW_VALUE
//                         // )?.name || ""
//                       }
//                       disabled={formMode === Mode.VIEW}
//                     >
//                       <option value="cadreCategory">
//                     {formik.values.qualSalStrucDesc}
//                   </option>
//                       {qualSalStrucDescs.map((qualSalStrucDesc) => (
//                         <option key={qualSalStrucDesc.traN_ID} value={qualSalStrucDesc.name}>
//                           {qualSalStrucDesc?.name?.toUpperCase()}
//                         </option>
//                       ))}
//                     </select> */}
//                 {formik.touched.qualSalStrucDesc &&
//                   formik.errors.qualSalStrucDesc && (
//                     <div className="fv-plugins-message-container text-danger">
//                       <span role="alert">{formik.errors.qualSalStrucDesc}</span>
//                     </div>
//                   )}
//               </div>
//             </div>

//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="fs-6 fw-bold">Status:</label>
//                 {/* <select
//                   // className="form-select form-select-solid fw-bolder"
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid":
//                         formik.touched.status && formik.errors.status,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.status && !formik.errors.status,
//                     }
//                   )}
//                   onChange={(e) => {
//                     formik.setFieldValue("status", e.target.value);

//                   }}
//                   value={status}
//                   disabled={globalVariable === "infoQualification"}
//                 >
//                   {globalVariable === "addQualification" ? (
//                     <option value="">{formik.values.status}</option>
//                   ) : (
//                     <option value={status === "ACTIVE" ? "A" : "N"}>
//                     {status}
//                     </option>
//                   )}
//                   {statuses.map((status) => (
//                     <option key={status} value={status}>
//                       {status}
//                     </option>
//                   ))}
//                 </select> */}

//                 <select
//                   className={clsx(
//                     "form-control form-control-solid pl-3 mb-3",
//                     {
//                       "is-invalid":
//                         formik.touched.status && formik.errors.status,
//                     },
//                     {
//                       "is-valid":
//                         formik.touched.status && !formik.errors.status,
//                     }
//                   )}
//                   value={formik.values.status}
//                   // onChange={formik.handleChange}
//                   onChange={(e) => {
//                     formik.setFieldValue("status", e.target.value);
//                   }}
//                   disabled={formMode === Mode.ADD || formMode === Mode.VIEW}
//                 >
//                   {statuses.map((status) => (
//                     <option value={status === "ACTIVE" ? "A" : "N"}>
//                       {status}
//                     </option>
//                   ))}
//                 </select>
//                 {formik.touched.status && formik.errors.status && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.status}</span>
//                   </div>
//                 )}
//               </div>
//             </div>

//             <div className="col-md-6">
//               <div className="fv-row">
//                 <label className="fs-6 fw-bold">Type:</label>
//                 <select
//                   // className="form-select form-select-solid fw-bolder"
//                   className={clsx(
//                     "form-control form-control-solid mb-3 mb-lg-0",
//                     {
//                       "is-invalid": formik.touched.type && formik.errors.type,
//                     },
//                     {
//                       "is-valid": formik.touched.type && !formik.errors.type,
//                     }
//                   )}
//                   onChange={(e) => {
//                     formik.setFieldValue("type", e.target.value);
//                     setType(e.target.value);
//                   }}
//                   value={type}
//                   disabled={formMode === Mode.VIEW}
//                 >
//                   {formMode === Mode.ADD ? (
//                     <option value="">{formik.values.type}</option>
//                   ) : (
//                     <option value="">{formik.values.type}</option>
//                   )}
//                   {types.map((type) => (
//                     <option key={type} value={type}>
//                       {type}
//                     </option>
//                   ))}
//                 </select>
//                 {formik.touched.type && formik.errors.type && (
//                   <div className="fv-plugins-message-container text-danger">
//                     <span role="alert">{formik.errors.type}</span>
//                   </div>
//                 )}
//               </div>
//             </div>
//             {/* {end : Category} */}

//             {formMode === Mode.VIEW && (
//               <div className="col-md-6">
//                 <div className="fv-row">
//                   <label className="fw-bold fs-6 mb-2"> Change Term</label>
//                   <input
//                     placeholder=""
//                     {...formik.getFieldProps("changeTerm")}
//                     className={clsx(
//                       "form-control form-control-solid mb-3 mb-lg-0",
//                       {
//                         "is-invalid":
//                           formik.touched.changeTerm && formik.errors.changeTerm,
//                       },
//                       {
//                         "is-valid":
//                           formik.touched.changeTerm &&
//                           !formik.errors.changeTerm,
//                       }
//                     )}
//                     type="text"
//                     name="chG_TERM"
//                     autoComplete="off"
//                     disabled={
//                       formik.isSubmitting ||
//                       isQualificationLoading ||
//                       formMode === Mode.VIEW
//                     }
//                     //|| globalVariable === 'infoCadre'
//                   />
//                   {formik.touched.changeTerm && formik.errors.changeTerm && (
//                     <div className="fv-plugins-message-container">
//                       <span role="alert">{formik.errors.changeTerm}</span>
//                     </div>
//                   )}
//                 </div>
//               </div>
//             )}

//             {formMode === Mode.VIEW && (
//               <div className="col-md-6">
//                 <div className="fv-row">
//                   <label className="fw-bold fs-6 mb-2"> Change User</label>
//                   <input
//                     placeholder=""
//                     value={formik.values.changeUser?.toUpperCase()}
//                     // {...formik.getFieldProps('changeUser')}
//                     className={clsx(
//                       "form-control form-control-solid mb-3 mb-lg-0",
//                       {
//                         "is-invalid":
//                           formik.touched.changeUser && formik.errors.changeUser,
//                       },
//                       {
//                         "is-valid":
//                           formik.touched.changeUser &&
//                           !formik.errors.changeUser,
//                       }
//                     )}
//                     type="text"
//                     name="chG_USER"
//                     autoComplete="off"
//                     disabled={
//                       formik.isSubmitting ||
//                       isQualificationLoading ||
//                       formMode === Mode.VIEW
//                     }
//                     //|| globalVariable === 'infoCadre'
//                   />
//                   {formik.touched.changeUser && formik.errors.changeUser && (
//                     <div className="fv-plugins-message-container">
//                       <span role="alert">{formik.errors.changeUser}</span>
//                     </div>
//                   )}
//                 </div>
//               </div>
//             )}

//             {formMode === Mode.VIEW && (
//               <div className="col-md-6">
//                 <div className="fv-row">
//                   <label className="fw-bold fs-6 mb-2"> Change Date</label>
//                   <input
//                     placeholder=""
//                     {...formik.getFieldProps("changeDate")}
//                     className={clsx(
//                       "form-control form-control-solid mb-3 mb-lg-0",
//                       {
//                         "is-invalid":
//                           formik.touched.changeDate && formik.errors.changeDate,
//                       },
//                       {
//                         "is-valid":
//                           formik.touched.changeDate &&
//                           !formik.errors.changeDate,
//                       }
//                     )}
//                     type="text"
//                     name="changeDate"
//                     autoComplete="off"
//                     disabled={
//                       formik.isSubmitting ||
//                       isQualificationLoading ||
//                       formMode === Mode.VIEW
//                     }
//                     // value={formik.values.changeDate? formatDate(formik.values.changeDate : "" }
//                     value={
//                       formik.values.changeDate
//                         ? formatDate(formik.values?.changeDate.toString())
//                         : ""
//                     }

//                     //|| globalVariable === 'infoCadre'
//                   />
//                   {formik.touched.changeDate && formik.errors.changeDate && (
//                     <div className="fv-plugins-message-container">
//                       <span role="alert">{formik.errors.changeDate}</span>
//                     </div>
//                   )}
//                 </div>
//               </div>
//             )}

//             {formMode !== Mode.ADD && (
//               <div className="row-mb-3">
//                 <div className="fv-row">
//                   <label className="required fw-bold fs-6 mb-2">
//                     Reason For Edit
//                   </label>
//                   <textarea
//                     className="required form-control"
//                     // value={changeReason}
//                     {...(formMode === Mode.VIEW
//                       ? { ...formik.getFieldProps("changeReason") }
//                       : { changeReason })}
//                     //onChange={(e) => setReason(e.target.value)}
//                     onChange={(e) => {
//                       formik.setFieldValue(
//                         "changeReason",
//                         e.target.value.toUpperCase()
//                       );
//                       setChangeReason(e.target.value.toUpperCase());
//                     }}
//                     rows={3}
//                     {...(formMode === Mode.VIEW
//                       ? { placeholder: "" }
//                       : { placeholder: "This is Mandatory Field" })}
//                     //  placeholder="This is Mandatory Field"
//                     disabled={formMode === Mode.VIEW}
//                   ></textarea>
//                   {formik.touched.changeReason &&
//                     formik.errors.changeReason && (
//                       <div className="fv-plugins-message-container">
//                         <span role="alert">{formik.errors.changeReason}</span>
//                       </div>
//                     )}
//                 </div>
//               </div>
//             )}
//           </div>
//         </div>
//         <div className="text-center pt-15">
//           {/* <button
//             type='reset'
//             onClick={() => cancel()}
//             className='btn btn-light me-3'
//             disabled={formik.isSubmitting || isQualificationLoading}
//           >
//             Cancel
//           </button> */}
//           {formMode !== Mode.VIEW && (
//             <button
//               type="submit"
//               className="btn btn-primary"
//               disabled={
//                 isQualificationLoading ||
//                 formik.isSubmitting ||
//                 !formik.touched ||
//                 (formMode !== Mode.ADD && changeReason === "")
//               }
//               // onClick={handleFormSubmit} // Call handleFormSubmit on button click)
//             >
//               <span className="indicator-label">Submit</span>
//               {(formik.isSubmitting || isQualificationLoading) && (
//                 <span className="indicator-progress">
//                   Please wait...{" "}
//                   <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
//                 </span>
//               )}
//             </button>
//           )}
//         </div>
//       </form>
//       {(formik.isSubmitting || isQualificationLoading) && (
//         <QualificationListLoading />
//       )}
//     </>
//   );
// };

// export { QualificationEditModalForm };

import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { useAuth } from "../../../../auth";
import clsx from "clsx";
import { useListView } from "../core/ListViewProvider";
import { useListView as useTxListView } from "../../../transaction-master/master-list/core/ListViewProvider";
import { useQueryResponse } from "../../../transaction-master/master-list/core/QueryResponseProvider";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import { useQueryClient, useMutation } from "react-query";
import Tooltip, { TooltipProps, tooltipClasses } from "@mui/material/Tooltip";
import { styled } from "@mui/material/styles";
import { Qualification } from "../core/_models";
import { QualificationListLoading } from "../components/loading/QualificationListLoading";
import { createTransaction } from "../../../transaction-master/master-list/core/_models";
import { insertTransaction } from "../../../transaction-master/master-list/core/_requests";

const API_URL = import.meta.env.VITE_QUAL_DROPDOWNS_API_URL;
const QUAL_URL = import.meta.env.VITE_QUAL_API_URL;

type Props = {
  isQualificationLoading: boolean;
  qualification: Qualification;
};

interface salStructDrpdwn {
  traN_ID: string;
  descr: string;
}

const QualificationEditModalForm: FC<Props> = ({
  qualification,
  isQualificationLoading,
}) => {
  const { setItemIdForUpdate, formMode } = useListView();
  const { itemIdForUpdate, setItemIdForUpdate: closeForm } = useTxListView();
  const [qualificationValues, setQualificationValues] =
    useState<Qualification>();
  const { refetch } = useQueryResponse();

  const [salStructs, setSalStructs] = useState<salStructDrpdwn[]>([]);
  const [types] = useState([
    { code: "DIPLOMA", descr: "DIPLOMA" },
    { code: "UNDER GRADUATE", descr: "UNDER GRADUATE" },
    { code: "GRADUATE", descr: "GRADUATE" },
    { code: "POST GRADUATE", descr: "POST GRADUATE" },
  ]);
  const [statuses] = useState([
    { code: "A", descr: "ACTIVE" },
    { code: "N", descr: "INACTIVE" },
  ]);

  const [privateIP, setPrivateIP] = useState();
  const { currentUser } = useAuth();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();

  const [isDataFetched, setIsDataFetched] = useState(false);

  const editQualificationSchema = Yup.object().shape({
    shortdescr: Yup.string()
      .trim()
      .required("Qualification Short Description is required"),
    name: Yup.string().trim().required("Qualification Description is required"),
    type: Yup.string().trim().required("Qualification Category is required"),
    eqival: Yup.string()
      .trim()
      .required("Qualification Salary Structure is required"),
    status: Yup.string().trim().required("Qualification Status is required"),
    changeReason: Yup.string().when([], {
      is: () => formMode !== Mode.ADD,
      then: (schema) =>
        schema
          .trim()
          .required("Change Reason is required")
          .min(5, "Minimum 5 characters"),
      otherwise: (schema) => schema.notRequired(),
    }),
  });

  useEffect(() => {
    axios
      .get(API_URL)
      .then((response) => {
        setSalStructs(response.data.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const [qualificationForEdit] = useState<Qualification>({
    ...qualification,
    qual_code:
      qualification.qual_code || qualification.fieldNames?.QLF_CODE?.neW_KEY,
    name: qualification.name || qualification.fieldNames?.DESCR?.neW_KEY,
    shortdescr:
      qualification.shortdescr || qualification.fieldNames?.SH_DESCR?.neW_KEY,
    changeTerm:
      qualification.tX_CHG_TERM ||
      qualification.changeTerm ||
      qualification.fieldNames?.CHG_TERM?.neW_KEY,
    changeDate:
      qualification.tX_CHG_DATE ||
      qualification.changeDate ||
      qualification.fieldNames?.CHG_DATE?.neW_KEY,
    changeUser:
      qualification.tX_CHG_USER ||
      qualification.changeUser ||
      qualification.fieldNames?.CHG_USER?.neW_KEY,
    type: qualification.type || qualification.fieldNames?.TYPE?.neW_KEY,
    typeDescr: qualification.type || qualification.fieldNames?.TYPE?.neW_VALUE,
    eqival: qualification.eqival || qualification.fieldNames?.EQIVAL?.neW_KEY,
    qualSalStrucDesc:
      qualification.qualSalStrucDesc ||
      qualification.fieldNames?.EQIVAL?.neW_VALUE,
    status: qualification.status || qualification.fieldNames?.STATUS?.neW_KEY,
    changeReason: formMode === Mode.VIEW ? qualification.changeReason : "",
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
    closeForm(undefined);
  };

  useEffect(() => {
    if (itemIdForUpdate && !isDataFetched) {
      axios
        .get(
          `${QUAL_URL}/GetQualificationByCode/${qualification.fieldNames?.QLF_CODE?.neW_KEY}`
        )
        .then((response) => {
          const data = response.data.data;

          setQualificationValues(data);
          formik.setValues({
            qual_code:
              qualification.fieldNames?.QLF_CODE?.neW_KEY || data.qual_code,
            name: qualification.fieldNames?.DESCR?.neW_KEY || data.name,
            shortdescr:
              qualification.fieldNames?.SH_DESCR?.neW_KEY || data.shortdescr,
            changeTerm:
              qualification.fieldNames?.CHG_TERM?.neW_KEY || data.changeTerm,
            changeDate:
              qualification.fieldNames?.CHG_DATE?.neW_KEY || data.changeDate,
            changeUser:
              qualification.fieldNames?.CHG_USER?.neW_KEY || data.changeUser,
            type: qualification.fieldNames?.TYPE?.neW_KEY || data.type,
            typeDescr: qualification.fieldNames?.TYPE?.neW_VALUE || data.type,
            eqival: qualification.fieldNames?.EQIVAL?.neW_KEY || data.eqival,
            qualSalStrucDesc:
              qualification.fieldNames?.EQIVAL?.neW_VALUE ||
              data.qualSalStrucDesc,
            status: qualification.fieldNames?.STATUS?.neW_KEY || data.status,
          });
          setIsDataFetched(true);
        })
        .catch((error) =>
          console.error("Error fetching combined data:", error)
        );
    }
  }, [itemIdForUpdate, isDataFetched, qualification]);

  const createTx = useMutation((payload: any) => insertTransaction(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        closeForm(undefined);
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
        cancel(true);
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formattedDate = (date: Date) => {
    return new Date(date).toLocaleString("en-GB", {
      day: "2-digit",
      month: "2-digit",
      year: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    });
  };

  const formik = useFormik({
    initialValues: qualificationForEdit,
    validationSchema: editQualificationSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      const payload: createTransaction = {
        tableName: "QUALIFICATION",
        keyVar: "",
        wfLevel: 1,
        conf: "N",
        siteCode: "S0001",
        emp: currentUser?.id?.toString() || "",
        filePath: null,
        file: null,
        strTranId: qualification.fieldNames ? itemIdForUpdate : null,
        folderPath: null,
        tX_CHG_USER: currentUser?.id?.toString() || "",
        tX_CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        remarks: values.changeReason || "NEW QUALIFICATION ADDED",

        fieldUpdates: [
          {
            SEQ_NO: "1",
            FIELD_NAME: "QLF_CODE",
            FIELD_DESCRIPTION: "QUALIFICATION CODE",
            OLD_KEY:
              qualification.qual_code ||
              qualification.fieldNames?.QLF_CODE?.olD_KEY ||
              null,
            OLD_VALUE:
              qualification.qual_code ||
              qualification.fieldNames?.QLF_CODE?.olD_VALUE ||
              null,
            NEW_KEY:
              qualification.qual_code ||
              qualification.fieldNames?.QLF_CODE?.olD_VALUE ||
              null,
            NEW_VALUE:
              qualification.qual_code ||
              qualification.fieldNames?.QLF_CODE?.olD_VALUE ||
              null,
            FILE_PATH: "",
          },
          ...((itemIdForUpdate !== undefined &&
            qualification.fieldNames?.DESCR?.olD_KEY === values.name) ||
          (qualification.name !== values.name &&
            qualificationValues?.name !== values.name)
            ? [
                {
                  SEQ_NO: "2",
                  FIELD_NAME: "DESCR",
                  FIELD_DESCRIPTION: "DESCRIPTION",
                  OLD_KEY:
                    qualification.name ||
                    qualification.fieldNames?.DESCR?.olD_KEY ||
                    qualificationValues?.name ||
                    "",
                  OLD_VALUE:
                    qualification.name ||
                    qualification.fieldNames?.DESCR?.olD_VALUE ||
                    qualificationValues?.name ||
                    "",
                  NEW_KEY: values.name,
                  NEW_VALUE: values.name,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            qualification.fieldNames?.SH_DESCR?.olD_KEY ===
              values.shortdescr) ||
          (qualification.shortdescr !== values.shortdescr &&
            qualificationValues?.shortdescr !== values.shortdescr)
            ? [
                {
                  SEQ_NO: "3",
                  FIELD_NAME: "SH_DESCR",
                  FIELD_DESCRIPTION: "SHORT DESCRIPTION",
                  OLD_KEY:
                    qualification.shortdescr ||
                    qualification.fieldNames?.SH_DESCR?.olD_KEY ||
                    qualificationValues?.shortdescr ||
                    "",
                  OLD_VALUE:
                    qualification.shortdescr ||
                    qualification.fieldNames?.SH_DESCR?.olD_VALUE ||
                    qualificationValues?.shortdescr ||
                    "",
                  NEW_KEY: values.shortdescr,
                  NEW_VALUE: values.shortdescr,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            qualification.fieldNames?.TYPE?.olD_KEY === values.type) ||
          (qualification.type !== values.type &&
            qualificationValues?.type !== values.type)
            ? [
                {
                  SEQ_NO: "4",
                  FIELD_NAME: "TYPE",
                  FIELD_DESCRIPTION: "QUALIFICATION TYPE",
                  OLD_KEY:
                    qualification.type ||
                    qualification.fieldNames?.TYPE?.olD_KEY ||
                    qualificationValues?.type ||
                    "",
                  OLD_VALUE:
                    qualification.type ||
                    qualification.fieldNames?.TYPE?.olD_VALUE ||
                    qualificationValues?.type ||
                    "",
                  NEW_KEY: values.type,
                  NEW_VALUE: values.typeDescr,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            qualification.fieldNames?.EQIVAL?.olD_KEY === values.eqival) ||
          (qualification.eqival !== values.eqival &&
            qualificationValues?.eqival !== values.eqival)
            ? [
                {
                  SEQ_NO: "4",
                  FIELD_NAME: "EQIVAL",
                  FIELD_DESCRIPTION: "SALARY STRUCTURE",
                  OLD_KEY:
                    qualification.eqival ||
                    qualification.fieldNames?.EQIVAL?.olD_KEY ||
                    qualificationValues?.eqival ||
                    "",
                  OLD_VALUE:
                    qualification.qualSalStrucDesc ||
                    qualification.fieldNames?.EQIVAL?.olD_VALUE ||
                    qualificationValues?.qualSalStrucDesc ||
                    "",
                  NEW_KEY: values.eqival,
                  NEW_VALUE: values.qualSalStrucDesc,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            qualification.fieldNames?.STATUS?.olD_KEY === values.status) ||
          (qualification.status !== values.status &&
            qualificationValues?.status !== values.status)
            ? [
                {
                  SEQ_NO: "5",
                  FIELD_NAME: "STATUS",
                  FIELD_DESCRIPTION: "STATUS",
                  OLD_KEY:
                    qualification.status ||
                    qualification.fieldNames?.STATUS?.olD_KEY ||
                    qualificationValues?.status ||
                    "",
                  OLD_VALUE:
                    qualification.status === "A"
                      ? "ACTIVE"
                      : qualification.status === "N"
                      ? "INACTIVE"
                      : "" || qualification.fieldNames?.STATUS?.olD_KEY === "A"
                      ? "ACTIVE"
                      : qualification.fieldNames?.STATUS?.olD_KEY === "N"
                      ? "INACTIVE"
                      : "" || qualificationValues?.status === "A"
                      ? "ACTIVE"
                      : qualificationValues?.status === "N"
                      ? "INACTIVE"
                      : "",
                  NEW_KEY: values.status,
                  NEW_VALUE:
                    values.status === "A"
                      ? "ACTIVE"
                      : values.status === "N"
                      ? "INACTIVE"
                      : "",
                  FILE_PATH: "",
                },
              ]
            : []),

          {
            SEQ_NO: "6",
            FIELD_NAME: "CHG_USER",
            FIELD_DESCRIPTION: "CHANGE USER",
            OLD_KEY:
              qualification.changeUser ||
              qualification.fieldNames?.CHG_USER?.olD_KEY ||
              qualificationValues?.changeUser ||
              "",
            OLD_VALUE:
              qualification.changeUser ||
              qualification.fieldNames?.CHG_USER?.olD_VALUE ||
              qualificationValues?.changeUser ||
              "",
            NEW_KEY: currentUser?.id?.toString() || "",
            NEW_VALUE: currentUser?.emp_Name || "",
            FILE_PATH: "",
          },
          {
            SEQ_NO: "7",
            FIELD_NAME: "CHG_TERM",
            FIELD_DESCRIPTION: "CHANGE TERM",
            OLD_KEY:
              qualification.changeTerm ||
              qualification.fieldNames?.CHG_TERM?.olD_KEY ||
              qualificationValues?.changeTerm ||
              "",
            OLD_VALUE:
              qualification.changeTerm ||
              qualification.fieldNames?.CHG_TERM?.olD_VALUE ||
              qualificationValues?.changeTerm ||
              "",
            NEW_KEY: privateIP,
            NEW_VALUE: privateIP,
            FILE_PATH: "",
          },
          {
            SEQ_NO: "8",
            FIELD_NAME: "CHG_DATE",
            FIELD_DESCRIPTION: "CHANGE DATE",
            OLD_KEY:
              qualification.changeDate ||
              qualification.fieldNames?.CHG_DATE?.olD_KEY ||
              qualificationValues?.changeDate ||
              "",
            OLD_VALUE:
              (qualification.changeDate &&
                formattedDate(qualification.changeDate)) ||
              (qualificationValues?.changeDate &&
                formattedDate(qualificationValues?.changeDate)) ||
              (qualification.fieldNames?.CHG_DATE?.olD_VALUE &&
                formattedDate(
                  qualification.changeDate ||
                    qualification.fieldNames?.CHG_DATE?.olD_VALUE
                )) ||
              "",
            NEW_KEY: new Date(),
            NEW_VALUE: formattedDate(new Date()),
            FILE_PATH: "",
          },
          {
            SEQ_NO: "9",
            FIELD_NAME: "CHG_REASON",
            FIELD_DESCRIPTION: "CHANGE REASON",
            OLD_KEY:
              qualification.changeReason ||
              qualification.fieldNames?.CHG_REASON?.olD_KEY ||
              qualificationValues?.changeReason ||
              "",
            OLD_VALUE:
              qualification.changeReason ||
              qualification.fieldNames?.CHG_REASON?.olD_KEY ||
              qualificationValues?.changeReason ||
              "",
            NEW_KEY: values.changeReason || "NEW QUALIFICATION ADDED",
            NEW_VALUE: values.changeReason || "NEW QUALIFICATION ADDED",
            FILE_PATH: "",
          },
        ],
      };
      createTx.mutate(payload);

      setSubmitting(false);
    },
  });

  useEffect(() => {
    if (formMode === Mode.ADD) {
      formik.setFieldValue("status", "A");
    }
  }, []);

  function formatDate(dateString: string) {
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    });
    const formattedTime = date.toLocaleTimeString("en-IN", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: "Asia/Kolkata",
    });
    return `${formattedDate} ${formattedTime}`;
  }

  const LightTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: theme.palette.common.white,
      color: "rgba(0, 0, 0, 0.87)",
      boxShadow: theme.shadows[1],
      fontSize: 15,
    },
  }));

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="d-flex flex-column" id="kt_modal_add_user_scroll">
          {/* Begin: Edit Row */}
          <div className="row mb-1">
            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="name"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  DESCRIPTION
                </label>
                <input
                  placeholder="Description"
                  {...formik.getFieldProps("name")}
                  onChange={(e) =>
                    formik.setFieldValue("name", e.target.value.toUpperCase())
                  }
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        qualification.fieldNames?.DESCR?.neW_VALUE,
                    },
                    {
                      "is-invalid": formik.touched.name && formik.errors.name,
                    },
                    {
                      "is-valid": formik.touched.name && !formik.errors.name,
                    }
                  )}
                  type="text"
                  name="name"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isQualificationLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(qualification.fieldNames?.DESCR && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      qualification.fieldNames?.DESCR?.olD_VALUE === null
                        ? "-"
                        : qualification.fieldNames?.DESCR?.olD_VALUE
                    }`,
                  })}
                />
              </div>
              {formik.touched.name && formik.errors.name && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.name}</span>
                </div>
              )}
            </div>
            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="shortdescr"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SHORT DESCR.
                </label>
                <input
                  placeholder="Short Description"
                  {...formik.getFieldProps("shortdescr")}
                  onChange={(e) =>
                    formik.setFieldValue(
                      "shortdescr",
                      e.target.value.toUpperCase()
                    )
                  }
                  className={clsx(
                    "form-control form-control-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        qualification.fieldNames?.SH_DESCR?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.shortdescr && formik.errors.shortdescr,
                    },
                    {
                      "is-valid":
                        formik.touched.shortdescr && !formik.errors.shortdescr,
                    }
                  )}
                  type="text"
                  name="shortdescr"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isQualificationLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(qualification.fieldNames?.SH_DESCR && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      qualification.fieldNames?.SH_DESCR?.olD_VALUE === null
                        ? "-"
                        : qualification.fieldNames?.SH_DESCR?.olD_VALUE
                    }`,
                  })}
                />
              </div>
              {formik.touched.shortdescr && formik.errors.shortdescr && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.shortdescr}</span>
                </div>
              )}
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="eqival"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  SALARY STRUCTURE
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        qualification.fieldNames?.EQIVAL?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.eqival && formik.errors.eqival,
                    },
                    {
                      "is-valid":
                        formik.touched.eqival && !formik.errors.eqival,
                    }
                  )}
                  {...formik.getFieldProps("eqival")}
                  onChange={(e) => {
                    const selectedValue = e.target.value;
                    const selectedOption = salStructs.find(
                      (salStruct) => salStruct.traN_ID === selectedValue
                    );

                    formik.setFieldValue("eqival", selectedValue);
                    formik.setFieldValue(
                      "qualSalStrucDesc",
                      selectedOption?.descr || ""
                    );
                  }}
                  disabled={
                    formik.isSubmitting ||
                    isQualificationLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(qualification.fieldNames?.EQIVAL && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      qualification.fieldNames?.EQIVAL?.olD_VALUE === null
                        ? "-"
                        : qualification.fieldNames?.EQIVAL?.olD_VALUE
                    }`,
                  })}
                >
                  <option value=""></option>
                  {salStructs.map((salStruct) => (
                    <option key={salStruct.traN_ID} value={salStruct.traN_ID}>
                      {salStruct.descr}
                    </option>
                  ))}
                </select>
              </div>
              {formik.touched.eqival && formik.errors.eqival && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.eqival}</span>
                </div>
              )}
            </div>
            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="type"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  QUALIFICATION TYPE
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        qualification.fieldNames?.TYPE?.neW_VALUE,
                    },
                    {
                      "is-invalid": formik.touched.type && formik.errors.type,
                    },
                    {
                      "is-valid": formik.touched.type && !formik.errors.type,
                    }
                  )}
                  {...formik.getFieldProps("type")}
                  onChange={(e) => {
                    const selectedValue = e.target.value;
                    const selectedOption = types.find(
                      (type) => type.code === selectedValue
                    );

                    formik.setFieldValue("type", selectedValue);
                    formik.setFieldValue(
                      "typeDescr",
                      selectedOption?.descr || ""
                    );
                  }}
                  disabled={
                    formik.isSubmitting ||
                    isQualificationLoading ||
                    formMode === Mode.VIEW
                  }
                  {...(qualification.fieldNames?.TYPE && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      qualification.fieldNames?.TYPE?.olD_VALUE === null
                        ? "-"
                        : qualification.fieldNames?.TYPE?.olD_VALUE
                    }`,
                  })}
                >
                  <option value=""></option>
                  {types.map((type) => (
                    <option key={type.code} value={type.code}>
                      {type.descr}
                    </option>
                  ))}
                </select>
              </div>
              {formik.touched.type && formik.errors.type && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.type}</span>
                </div>
              )}
            </div>
            <div className="col-lg-6">
              <div className="d-flex align-items-center mb-1">
                <label
                  htmlFor="status"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  STATUS
                </label>
                <select
                  className={clsx(
                    "form-select form-select-solid pl-3 mb-3",
                    {
                      "bg-light-warning":
                        qualification.fieldNames?.STATUS?.neW_VALUE,
                    },
                    {
                      "is-invalid":
                        formik.touched.status && formik.errors.status,
                    },
                    {
                      "is-valid":
                        formik.touched.status && !formik.errors.status,
                    }
                  )}
                  {...formik.getFieldProps("status")}
                  onChange={(e) => {
                    formik.setFieldValue("status", e.target.value);
                  }}
                  disabled={formMode === Mode.ADD || formMode === Mode.VIEW}
                  {...(qualification.fieldNames?.STATUS && {
                    "data-bs-toggle": "tooltip",
                    "data-bs-placement": "top",
                    title: `OLD VALUE : ${
                      qualification.fieldNames?.STATUS?.olD_VALUE === null
                        ? "-"
                        : qualification.fieldNames?.STATUS?.olD_VALUE
                    }`,
                  })}
                >
                  <option value=""></option>
                  {statuses.map((status) => (
                    <option value={status.code}>{status.descr}</option>
                  ))}
                </select>
              </div>
              {formik.touched.status && formik.errors.status && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{formik.errors.status}</span>
                </div>
              )}
            </div>
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            {formMode === Mode.VIEW && (
              <>
                <div className="col-lg-6">
                  <div className="d-flex align-items-center mb-1">
                    <label
                      htmlFor="changeTerm"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CHANGE TERM
                    </label>
                    <input
                      placeholder="Change Term"
                      {...formik.getFieldProps("changeTerm")}
                      className="form-control form-control-solid pl-3 mb-3"
                      type="text"
                      name="changeTerm"
                      autoComplete="off"
                      disabled={
                        formik.isSubmitting ||
                        isQualificationLoading ||
                        formMode === Mode.VIEW
                      }
                    />
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="d-flex align-items-center mb-1">
                    <label
                      htmlFor="changeUser"
                      className="col-form-label required fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CHANGE USER
                    </label>
                    <input
                      placeholder="Change User"
                      {...formik.getFieldProps("changeUser")}
                      className="form-control form-control-solid pl-3 mb-3"
                      type="text"
                      name="changeUser"
                      autoComplete="off"
                      disabled={
                        formik.isSubmitting ||
                        isQualificationLoading ||
                        formMode === Mode.VIEW
                      }
                    />
                  </div>
                </div>
              </>
            )}
            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            {formMode === Mode.VIEW && (
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="changeDate"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE DATE
                </label>
                <input
                  {...formik.getFieldProps("changeDate")}
                  className="form-control form-control-solid pl-3 mb-3"
                  type="text"
                  name="changeDate"
                  autoComplete="off"
                  disabled={
                    formik.isSubmitting ||
                    isQualificationLoading ||
                    formMode === Mode.VIEW
                  }
                  value={
                    formik.values.changeDate
                      ? formatDate(formik.values?.changeDate.toString())
                      : ""
                  }
                />
              </div>
            )}

            {formMode !== Mode.ADD && (
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="changeReason"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CHANGE REASON
                  </label>
                  <input
                    placeholder="Change Reason"
                    {...formik.getFieldProps("changeReason")}
                    onChange={(e) =>
                      formik.setFieldValue(
                        "changeReason",
                        e.target.value.toUpperCase()
                      )
                    }
                    className={clsx(
                      "form-control form-control-solid pl-3 mb-3",
                      {
                        "is-invalid":
                          formik.touched.changeReason &&
                          formik.errors.changeReason,
                      },
                      {
                        "is-valid":
                          formik.touched.changeReason &&
                          !formik.errors.changeReason,
                      }
                    )}
                    type="text"
                    name="changeReason"
                    autoComplete="off"
                    disabled={
                      formik.isSubmitting ||
                      isQualificationLoading ||
                      formMode === Mode.VIEW
                    }
                  />
                </div>
                {formik.touched.changeReason && formik.errors.changeReason && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.changeReason}</span>
                  </div>
                )}
              </div>
            )}
          </div>
          {/* End: Edit Row */}
        </div>
        <div className="text-center pt-15">
          {formMode !== Mode.VIEW && (
            <button
              type="submit"
              className="btn btn-primary"
              disabled={
                isQualificationLoading ||
                formik.isSubmitting ||
                !formik.touched ||
                !formik.isValid
              }
            >
              <span className="indicator-label">Submit</span>
              {(formik.isSubmitting || isQualificationLoading) && (
                <span className="indicator-progress">
                  Please wait...{" "}
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
          )}
        </div>
      </form>
      {(formik.isSubmitting || isQualificationLoading) && (
        <QualificationListLoading />
      )}
    </>
  );
};

export { QualificationEditModalForm };
