
import {FC} from 'react'
import {Qualification} from '../../core/_models'
import { ID, Mode } from '../../../../../../../_metronic/helpers'
import { useListView } from '../../core/ListViewProvider'

type Props = {
    qualification: Qualification,
    id:ID
}

const QualificationInfoCell: FC<Props> = ({qualification,id}) => { 
  const {setItemIdForUpdate,setFormMode} = useListView()
  const openInfoModal = () => {
    setItemIdForUpdate(id)
    setFormMode(Mode.VIEW)
  }
  return(
  <div className='d-flex align-items-center '>
    {/* begin:: Avatar */}
    {/* <div className='symbol symbol-circle symbol-50px overflow-hidden me-3'>
      <a href='#'>
        {department.avatar ? (
          <div className='symbol-label'>
            <img src={toAbsoluteUrl(`media/${department.avatar}`)} alt={department.name} className='w-100' />
          </div>
        ) : (
          <div
            className={clsx(
              'symbol-label fs-3',
              `bg-light-${department.initials?.state}`,
              `text-${department.initials?.state}`
            )}
          >
            {department.initials?.label}
          </div>
        )}
      </a>
    </div> */}
    <div className='d-flex flex-column'>
      <a  className='text-gray-800 text-hover-primary mb-1' onClick={openInfoModal}>
        {qualification.name?.toUpperCase()}
      </a>
      {/* <span className='badge badge-light-success fw-bolder d-flex align-items-right' >{cadre.id}</span> */}
      <span >
        <div className=' fw-bolder d-flex align-items-right'>
          <div className='badge badge-light-success fw-bolder d-flex align-items-right'>{qualification.id}</div>
        </div>
    </span>

    </div>
  </div>
)
}
export {QualificationInfoCell}


