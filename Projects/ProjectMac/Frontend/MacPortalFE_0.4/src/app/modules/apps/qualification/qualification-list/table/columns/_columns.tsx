import { Column } from "react-table";
import { QualificationInfoCell } from "./QualificationInfoCell";
import { QualificationLastLoginCell } from "./QualificationLastLoginCell";
import { QualificationTwoStepsCell } from "./QualificationTwoStepsCell";
import { QualificationActionsCell } from "./QualificationActionsCell";
import { QualificationCustomHeader } from "./QualificationCustomHeader";
import { Qualification } from "../../core/_models";
import { QualificationUpperCell } from "./QualificationUpperCell";
import { QualificationShortDescription } from "./QualificationShortDescription";
import { QualoificationStatusCell } from "./QualificationStatusCell";

const qualificationColumns: ReadonlyArray<Column<Qualification>> = [
  {
    Header: (props) => (
      <QualificationCustomHeader
        tableProps={props}
        title="Qualification Name"
        className="min-w-125px text-left "
      />
    ),
    id: "name",
    Cell: ({ ...props }) => (
      <QualificationInfoCell qualification={props.data[props.row.index]} id={props.data[props.row.index].id} />
    ),
  },
  {
    Header: (props) => (
      <QualificationCustomHeader
        tableProps={props}
        title="Short Description"
        className="min-w-125px"
      />
    ),
    id: "shortdescr",
    Cell: ({ ...props }) => (
      <QualificationShortDescription
        shortdescr={props.data[props.row.index].shortdescr}
      />
    ),
  },
  {
    Header: (props) => (
      <QualificationCustomHeader
        tableProps={props}
        title="Type"
        className="min-w-125px"
      />
    ),
    id: "type",
    Cell: ({ ...props }) => (
      <QualificationTwoStepsCell type={props.data[props.row.index].type} />
    ),
  },
  {
    Header: (props) => (
      <QualificationCustomHeader
        tableProps={props}
        title="Change User"
        className="min-w-125px"
      />
    ),
    id: "changeUser",
    Cell: ({ ...props }) => (
      <QualificationUpperCell
        changeUser={props.data[props.row.index].changeUser}
      />
    ),
  },
  {
    Header: (props) => (
      <QualificationCustomHeader
        tableProps={props}
        title="Change Date"
        className="min-w-125px text-left"
      />
    ),
    id: "changeDate",
    Cell: ({ ...props }) => (
      <QualificationLastLoginCell
        changeDate={props.data[props.row.index].changeDate ?? new Date()}
      />
    ),
  },
  {
    Header: (props) => (
      <QualificationCustomHeader
        tableProps={props}
        title="status"
        className="min-w-125px"
      />
    ),
    id: "status",
    Cell: ({ ...props }) => (
      <QualoificationStatusCell status={props.data[props.row.index].status} />
    ),
  },
  {
    Header: (props) => (
      <QualificationCustomHeader
        tableProps={props}
        title="Actions"
        className="min-w-100px text-left"
      />
    ),
    id: "actions",
    Cell: ({ ...props }) => (
      <QualificationActionsCell id={props.data[props.row.index].id} />
    ),
  },
];

export { qualificationColumns };
