import { ID } from "../../../../_metronic/helpers";
import { useAuth } from "../../auth";

let globalVariable: string = 'addMasterDetails';
let _currentUser: ID = '';
export const setGlobalVariable = (newValue :string)=>{
    globalVariable = newValue;
};
export const useSessionUser = () => {
    const { currentUser } = useAuth();
    _currentUser = currentUser?.id;
  };
export {globalVariable,_currentUser};