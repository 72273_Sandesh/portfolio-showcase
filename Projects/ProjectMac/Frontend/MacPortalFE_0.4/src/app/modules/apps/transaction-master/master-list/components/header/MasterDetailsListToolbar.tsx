import React, { useState, useEffect, useRef } from "react";
import {
  KTIcon,
  Mode,
  initialQueryState,
} from "../../../../../../../_metronic/helpers";
import { useListView } from "../../core/ListViewProvider";
import * as XLSX from "xlsx";
import { toast } from "react-toastify";
import MasterDetailsTableFormat from "./MasterDetailsTableFormat";
import { useQueryRequest } from "../../core/QueryRequestProvider";
import "./abc.css";

const MasterDetailsListToolbar: React.FC = () => {
  const { setItemIdForUpdate, setFormMode } = useListView();
  const [exporting, setExporting] = useState<boolean>(false);
  const tableRef = useRef<HTMLDivElement>(null);
  const [_txMode, setTxMode] = useState("");
  const { updateState } = useQueryRequest();

  useEffect(() => {
    return () => {
      setExporting(false);
    };
  }, []);

  const changeTxMode = (txMode: string) => {
    setTxMode(txMode);
    try {
      updateState({
        ...initialQueryState,
        _switch: { txMode },
      });
    } catch (ex) {
      console.error(ex);
      toast.error(
        <div>
          <h4 className="alert-heading">oops...</h4>
          <p className="alert alert-danger" role="alert">
            <h6>Failed</h6>
          </p>
        </div>
      );
    }
  };
  const openAddMasterDetailsModal = () => {
    setItemIdForUpdate(null);
    setFormMode(Mode.ADD);
  };
  const handleExport = () => {
    setExporting(true);
    const input = tableRef.current as HTMLTableElement;
    if (!input) {
      // toast.error(`Exporting BankDetails Failed...`);
      toast.error(
        <div>
          <h6 className="alert-heading">Error </h6>
          <p className="alert alert-danger" role="alert">
            <h6>Exporting MasterDetails Failed...</h6>
          </p>
        </div>
      );
      console.error("Table container element not found.");
      setExporting(false);
      return;
    }
    const tableData = parseTableData(input);
    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.aoa_to_sheet(tableData);
    XLSX.utils.book_append_sheet(wb, ws, "MasterDetails");
    const excelFileName = "MasterDetails.xlsx";
    XLSX.writeFile(wb, excelFileName);
    setExporting(false);
  };

  const parseTableData = (table: HTMLTableElement): string[][] => {
    const data: string[][] = [];

    // Include the header row
    const headerRowData: string[] = [];
    table.querySelectorAll("thead th").forEach((headerCell) => {
      headerRowData.push((headerCell as HTMLTableCellElement).innerText);
    });
    data.push(headerRowData);

    // Include the body rows
    const bodyRows = table.querySelectorAll("tbody tr");
    bodyRows.forEach((row) => {
      const rowData: string[] = [];
      row.querySelectorAll("td").forEach((cell) => {
        rowData.push((cell as HTMLTableCellElement).innerText);
      });
      data.push(rowData);
    });

    return data;
  };

  return (
    <>
      {/* <ToastContainer/> */}
      <div style={{ display: "none" }}>
        <div ref={tableRef}>
          {/*style={{...styles}}>  */}
          <MasterDetailsTableFormat />
        </div>
      </div>
      <div
        className="d-flex justify-content-end"
        data-kt-user-table-toolbar="base"
      >
        {/* <div className="me-3">
          <div
            className="nav-group nav-group-outline mx-auto"
            data-kt-buttons="true"
          >
            <label>
              <input
                type="radio"
                className="btn-check"
                name="type"
                value="has"
                onClick={() => {
                  changeTxMode('');
                }}
                defaultChecked
              />
              <span className="btn btn-color-muted btn-active btn-light-success me-1">
              Sent Box
              <KTIcon iconName="arrow-up-right" className="fs-1" />
              </span>
            </label>

            <label>
              <input
                type="radio"
                className="btn-check"
                name="type"
                value="users"
                onClick={() => {
                  changeTxMode('A');
                }}
              />
              <span className="btn btn-color-muted btn-active btn-light-success">
              Inbox
              <KTIcon iconName="arrow-down-right" className="fs-1" />
              </span>
            </label>
          </div>
        </div> */}

        <div className="me-10">
          <div className="switch-container">
            <label className="switch">
              <input
                type="checkbox"
                onChange={(e) => changeTxMode(e.target.checked ? "A" : "")}
              />
              <span className="slider">
                <span className="slider-label outbox ms-3">
                  {/* <KTIcon iconName="arrow-up-right" className="icon" /> */}
                  My Requests
                </span>
                <span className="slider-label inbox">
                 My Approvals {/*<KTIcon iconName="arrow-down-right" className="icon" /> */}
                </span>
              </span>
            </label>
          </div>
        </div>

        <button
          type="button"
          className="btn btn-light-primary me-3"
          onClick={handleExport}
          disabled={exporting}
        >
          <KTIcon iconName="exit-up" className="fs-1" />
          {/* {exporting ? "Exporting..." : "Export"} */}
        </button>
        {/* <button
          type="button"
          className="btn btn-primary"
          onClick={openAddMasterDetailsModal}
        >
          <KTIcon iconName="plus" className="fs-2" />
          Add 
        </button> */}
      </div>
    </>
  );
};

export { MasterDetailsListToolbar };
