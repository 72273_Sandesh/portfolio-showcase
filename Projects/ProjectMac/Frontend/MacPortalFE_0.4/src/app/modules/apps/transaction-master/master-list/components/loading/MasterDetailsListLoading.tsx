const MasterDetailsListLoading = () => {
    const styles = {
      position: "fixed",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      backgroundColor: "rgba(255, 255, 255, 0.8)",
      backdropFilter: "blur(0.1px)",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      zIndex: 1000,
    };
  
    return (
      <div style={{ ...styles, position: "absolute", textAlign: "center" }}>
        <img
          src="/media/logos/macLoader1.1_gif.gif"
          style={{ height: "100px" }}
        />
      </div>
    //   <div className="page-loader flex-column bg-dark bg-opacity-25">
    // <span className="spinner-border text-primary" role="status"></span>
    // <span className="text-gray-800 fs-6 fw-semibold mt-5">Loading...</span>
// </div>
    );
  };
  
  export { MasterDetailsListLoading };
  

// import React, { useState } from 'react';

// const MasterDetailsListLoading: React.FC = () => {
//   const [isLoading, setIsLoading] = useState(false);

//   const handleToggleClick = () => {
//     setIsLoading(true);

//     // Simulate loading and hide after 3 seconds
//     setTimeout(() => {
//       setIsLoading(false);
//     }, 3000);
//   };

//   return (
//     <>
//       <button id="kt_page_loading_overlay" onClick={handleToggleClick}>
//         Toggle Loading
//       </button>

//       {isLoading && (
//         <div className="page-loader flex-column bg-dark bg-opacity-25">
//           <span className="spinner-border text-primary" role="status"></span>
//           <span className="text-gray-800 fs-6 fw-semibold mt-5">Loading...</span>
//         </div>
//       )}
//     </>
//   );
// };

// export {MasterDetailsListLoading};
