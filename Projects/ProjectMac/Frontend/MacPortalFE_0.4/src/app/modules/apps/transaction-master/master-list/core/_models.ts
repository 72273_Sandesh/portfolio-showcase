import { ID, Response } from "../../../../../../_metronic/helpers";
export type MasterDetails = {
  id?: ID;
  traN_DATE?: string;
  tablE_NAME?: string;
  keY_VAR?: string;
  wF_PROCESS_LEVEL?: number | undefined;
  sqL_QRY?: string;
  confirmed?: string;
  tX_CHG_DATE?: string;
  tX_CHG_USER?: string;
  tX_CHG_TERM?: string;
  emP_CODE?: string;
  remarks?: string;
  neW_VALUE?: string;
  filE_PATH?: string;
  initiateD_AS?: string;
  approveR_NAME?: string;

  fieldNames: {
    //fields for cadre
    cadrE_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    descr: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    sH_DESCR: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    banD_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    chG_USER: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    chG_TERM: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    chG_DATE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    status: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };

    //fields for grade
    GRADE_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CADRE_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    NOTICE_PRD: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    changeTerm: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    changeDate: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    STATUS: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };

    //fields for department
    DEPT_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    DESCR: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    SH_DESCR: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    DEPT_HEAD: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_USER: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_TERM: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CHG_DATE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };

    //fields for qualification
    QLF_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    EQIVAL: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    TYPE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };

    //fields for designation
    DESIGN_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    DESIGNATION: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    LONG_DESIGNATION: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };

    //fields for EMPLOYEE BANK DETAILS
    EMP_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    WORK_SITE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    GRATUITY_DATE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    CURR_CTC: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    BASIC: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    PAY_MODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    PAY_TYPE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    BANK_CODE: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    BANK_IFSC: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    BANK_ACCT: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
    DD_PAYABLE_BANK: {
      olD_KEY: string;
      olD_VALUE: string;
      neW_KEY: string;
      neW_VALUE: string;
      FILE_PATH: string;
    };
  };

  reviewers: [
    {
      traN_ID: string;
      leveL_NO: number;
      confirmed: string;
      emP_CODE__REVIEW: string | number | undefined;
      shorT_NAME: string;
      chG_DATE: string;
      reason: string;
    }
  ];

  //CADRE
  cadrE_CODE?: string;
  descr?: string;
  sH_DESCR?: string;
  lvE_TBLNO?: string;
  udF_NUM1?: number | string | null;
  cadrE_ID?: string;
  cadrE_ORDER?: number;
  banD_CODE?: string;
  mkit?: number;
  mmed?: number;

  chG_REASON?: string;
  cadreCategory?: string;
  status?: string;
  responseStatus?: string;
  message?: string;
  SessionempCode?: string;
  moduleType?: string;

  //DEPARTMENT
  deptCode?: string;
  name?: string;
  shortDescription?: string;
  deptHead?: string;
  changeUser?: string;

  //GRADE
  GRADE_CODE?: string;
  CADRE_CODE?: string;
  DESCR?: string;
  SH_DESCR?: string;
  noticePeriod?: string;
  changeTerm?: string;
  changeDate?: string;

  //Qualification
  QLF_CODE: string;
  EQIVAL: string;
  TYPE?: string;

  //Employee Basic Details
  emP_NAME: string;
  emP_FNAME: string;
  birtH_DATE: string;
  emP_LNAME: string;
  emP_MNAME: string;
  shorT_NAME: string;
  religion: string;
  blooD_GRP: string;
  fatheR_NAME: string;
  motheR_NAME: string;
  emaiL_ID_PER: string;
  mobilE_NO: string;
  pdf: string;
  gender: string;
  emP_IMAGE: string;
  gendeR_DESCR: string;
};

export type createTransaction = {
  TRAN_ID?: string;
  responseStatus?: string;
  message?: string;
  tableName: string;
  keyVar: string;
  wfLevel: number;
  conf: string;
  siteCode: string;
  emp: string;
  filePath: string | null;
  file: string | null;
  folderPath: string | null;
  strTranId?: string | null | number;
  tX_CHG_DATE?: string | Date;
  tX_CHG_USER?: string;
  tX_CHG_TERM?: string;
  remarks?: string;
  fieldUpdates: {
    SEQ_NO: string;
    FIELD_NAME: string;
    FIELD_DESCRIPTION: string | undefined;
    OLD_KEY: string | undefined | Date | null | number;
    OLD_VALUE: string | undefined | Date | null | number;
    NEW_KEY: string | undefined | Date | null | number;
    NEW_VALUE: string | undefined | Date | null | number;
    FILE_PATH: string | undefined;
  }[];
};

export type MasterDetailsQueryResponse = Response<Array<MasterDetails>>;

export const initialMasterDetails: MasterDetails = {};
