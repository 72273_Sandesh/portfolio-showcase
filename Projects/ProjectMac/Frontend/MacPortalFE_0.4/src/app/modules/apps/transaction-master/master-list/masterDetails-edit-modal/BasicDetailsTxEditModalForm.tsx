import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { useListView } from "../core/ListViewProvider";
import { useQueryResponse } from "../core/QueryResponseProvider";
import "./TrackingComponent.css";
import { initialMasterDetails, MasterDetails } from "../core/_models";
import axios from "axios";
import { useAuth } from "../../../../auth";
import { toast } from "react-toastify";
import {
  ID,
  Mode,
  QUERIES,
  toAbsoluteUrl,
} from "../../../../../../_metronic/helpers";
import { ProgressMeter } from "./ProgressMeter";
import Swal from "sweetalert2";
import { useMutation, useQueryClient } from "react-query";
import { approveMasterDetails, declineMasterDetails } from "../core/_requests";

type Props = {
  isBankDetailsLoading: boolean;
  masterDetails: MasterDetails;
};
interface cadreCategory {
  banD_CODE: string;
  descr: string;
}
const editBankDetailsSchema = Yup.object().shape({
  descr: Yup.string().required("Description of Payment is required"),
  sH_DESCR: Yup.string().required("Short Description is required"),
  banD_CODE: Yup.string().required("Cadre Category is required"),
  status: Yup.string().required("Status is required"),
});

const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;
const API_URL = import.meta.env.VITE_CADRE_DROPDOWNS_API_URL;

const BasicDetailsTxEditModalForm: FC<Props> = ({
  masterDetails,
  isBankDetailsLoading,
}) => {
  const { itemIdForUpdate, setItemIdForUpdate, formMode, tableNameForUpdate } =
    useListView();
  const { refetch } = useQueryResponse();
  const { currentUser } = useAuth();
  const [cadreCategories, setCadreCategories] = useState<cadreCategory[]>([]);
  const [statuses] = useState(["ACTIVE", "INACTIVE"]);
  const [isEditable, setIsEditable] = useState<boolean>();
  const { query } = useQueryResponse();
  const queryClient = useQueryClient();
  const loggedInUser = currentUser?.id?.toString() || "";
  const ip = "term";
  const [flag, setFlag] = useState<boolean>(false);

  useEffect(() => {
    axios
      .get(API_URL)
      .then((response) => {
        setCadreCategories(response.data.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));

    setIsEditable(formMode === Mode.VIEW);
  }, []);

  const closeForm = () => {
    setItemIdForUpdate(undefined);
  };

  const approveItem = useMutation(
    (remarks: string) =>
      approveMasterDetails(
        itemIdForUpdate,
        tableNameForUpdate,
        formik.values.chG_USER,
        remarks,
        formik.values.wF_PROCESS_LEVEL?.toString(),
        loggedInUser,
        ip
      ),
    {
      onSuccess: (response: any) => {
        const { responseStatus, message } = response.data;
        console.log(response.data);
        if (responseStatus === "S") {
          Swal.fire({
            title: "Success!",
            text: "Transaction approved successfully.",
            icon: "success",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
          setItemIdForUpdate(undefined);
        } else if (responseStatus === "E") {
          Swal.fire({
            title: "Error!",
            text: `Error approving item: ${message.join(", ")}`,
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        } else {
          Swal.fire({
            title: "Error!",
            text: "Unknown response status.",
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        }
      },
      onError: (error: Error) => {
        Swal.fire({
          title: "Error!",
          text: `Error approving item: ${error.message || "Unknown error"}`,
          icon: "error",
          confirmButtonText: "OK",
        });
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);
      },
    }
  );

  const declineItem = useMutation(
    (transactionDetails: object) => declineMasterDetails(transactionDetails),
    {
      onSuccess: (response: any) => {
        const { responseStatus, message } = response.data;
        console.log(response.data);
        if (responseStatus === "S") {
          Swal.fire({
            title: "Success!",
            text: "Transaction Rejected successfully.",
            icon: "success",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
          setItemIdForUpdate(undefined);
        } else if (responseStatus === "E") {
          Swal.fire({
            title: "Error!",
            text: `Error While Rejecting Transaction: ${message.join(", ")}`,
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        } else {
          Swal.fire({
            title: "Error!",
            text: "Unknown response status.",
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        }
      },
      onError: (error: Error) => {
        Swal.fire({
          title: "Error!",
          text: `Error While Rejecting Transaction: ${
            error.message || "Unknown error"
          }`,
          icon: "error",
          confirmButtonText: "OK",
        });
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);
      },
    }
  );

  const confirmApprove = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "Do you want to approve this Transaction?",
      icon: "warning",
      input: "text", // Adding input field for remarks
      inputPlaceholder: "Enter remarks...",
      inputAttributes: {
        "aria-label": "Type your remarks here",
      },
      showCancelButton: true,
      confirmButtonText: "Yes, approve it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
      preConfirm: (remarksApprove) => {
        if (!remarksApprove) {
          Swal.showValidationMessage("Remarks are required.");
        } else {
          return remarksApprove;
        }
      },
    }).then((result) => {
      if (result.isConfirmed) {
        approveItem.mutate(result.value); // Pass the remarks to the mutation
      }
    });
  };

  const confirmDecline = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "Do you want to reject this transaction?",
      icon: "warning",
      input: "text",
      inputPlaceholder: "Enter remarks...",
      inputAttributes: {
        "aria-label": "Type your remarks here",
      },
      showCancelButton: true,
      confirmButtonText: "Yes, reject it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
      preConfirm: (remarksDecline) => {
        if (!remarksDecline) {
          Swal.showValidationMessage("Remarks are required.");
        } else {
          return remarksDecline;
        }
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const transactionDetails = {
          transactionId: masterDetails.id,
          empCode: loggedInUser,
          remoteAddr: ip,
          remarks: result.value,
        };
        declineItem.mutate(transactionDetails);
      }
    });
  };

  const [bankDetailsForEdit] = useState<MasterDetails>({
    ...masterDetails,
    id: masterDetails.id || initialMasterDetails.id,
    emP_CODE: masterDetails.emP_CODE || initialMasterDetails.emP_CODE,
    emP_NAME: masterDetails.emP_NAME || initialMasterDetails.emP_NAME,
    emP_FNAME: masterDetails.emP_FNAME || initialMasterDetails.emP_FNAME,
    birtH_DATE: masterDetails.birtH_DATE || initialMasterDetails.birtH_DATE,
    emP_LNAME: masterDetails.emP_LNAME || initialMasterDetails.emP_LNAME,
    emP_MNAME: masterDetails.emP_MNAME || initialMasterDetails.emP_MNAME,
    shorT_NAME: masterDetails.shorT_NAME || initialMasterDetails.shorT_NAME,
    religion: masterDetails.religion || initialMasterDetails.religion,
    blooD_GRP: masterDetails.blooD_GRP || initialMasterDetails.blooD_GRP,
    fatheR_NAME: masterDetails.fatheR_NAME || initialMasterDetails.fatheR_NAME,
    motheR_NAME: masterDetails.motheR_NAME || initialMasterDetails.motheR_NAME,
    emaiL_ID_PER:
      masterDetails.emaiL_ID_PER || initialMasterDetails.emaiL_ID_PER,
    mobilE_NO: masterDetails.mobilE_NO || initialMasterDetails.mobilE_NO,
    pdf: masterDetails.pdf || initialMasterDetails.pdf,
    gender: masterDetails.gender || initialMasterDetails.gender,
    chG_DATE: masterDetails.chG_DATE || initialMasterDetails.chG_DATE,
    chG_USER: masterDetails.chG_USER || initialMasterDetails.chG_USER,
    chG_TERM: masterDetails.chG_TERM || initialMasterDetails.chG_TERM,
    emP_IMAGE: masterDetails.emP_IMAGE || initialMasterDetails.emP_IMAGE,
    gendeR_DESCR:
      masterDetails.gendeR_DESCR || initialMasterDetails.gendeR_DESCR,
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };

  const handleSubmit = () => {
    const values = formik.values;

    console.log("Submitting..................");
    const payload = {
      chG_DATE: new Date(),
      chG_USER: currentUser?.id,
      tableName: "CADRE",
      keyVar: "",
      wfLevel: 1,
      conf: "N",
      siteCode: "S0001",
      emp: "H032422   ",
      filePath: values.filE_PATH || null,
      file: null,
      folderPath: null,
      strTranId: values.id || "",
      remarks: values.remarks || "",
      chg_Term: values.chG_TERM || "",
      fieldUpdates: [
        {
          fieldName: "cadrE_CODE",
          oldValue: values.fieldNames?.cadrE_CODE?.olD_VALUE || "",
          newValue: values.fieldNames?.cadrE_CODE?.neW_VALUE || "",
        },
        {
          fieldName: "descr",
          oldValue: values.fieldNames?.descr?.olD_VALUE || "",
          newValue: values.fieldNames?.descr?.neW_VALUE || "",
        },
        {
          fieldName: "sH_DESCR",
          oldValue: values.fieldNames?.sH_DESCR?.olD_VALUE || "",
          newValue: values.fieldNames?.sH_DESCR?.neW_VALUE || "",
        },
        {
          fieldName: "banD_CODE",
          oldValue: values.fieldNames?.banD_CODE?.olD_VALUE || "",
          newValue: values.fieldNames?.banD_CODE?.neW_VALUE || "",
        },
        {
          fieldName: "status",
          oldValue: values.fieldNames?.status?.olD_VALUE || "",
          newValue: values.fieldNames?.status?.neW_VALUE || "",
        },
      ],
    };

    axios
      .post(` ${MASTERTX_URL}/InsertMasterTransaction`, payload)
      .then((response) => {
        const { responseStatus, message } = response.data.data;
        if (responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{message}</h6>
              </p>
            </div>
          );
        } else {
          // setItemIdForUpdate(undefined);
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
      });
  };

  const formik = useFormik({
    initialValues: bankDetailsForEdit,
    validationSchema: editBankDetailsSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);
    },
  });

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="card-body">
          <div className="bg-light-warning border border-dashed border-gray-300 rounded p-5">
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="trId"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  TRANSACTION ID
                </label>
                <input
                  type="text"
                  id="id"
                  className="form-control form-control-lg form-control-solid "
                  name="id"
                  placeholder="Transaction Id"
                  value={formik.values?.id}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="trDate"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  TRANSACTION DATE
                </label>
                <input
                  type="date"
                  id="trDate"
                  className="form-control form-control-lg form-control-solid"
                  name="trDate"
                  placeholder="Date of Transaction"
                  value={formik.values.traN_DATE?.split("T")[0]}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sitE_NAME"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE USER
                </label>
                <input
                  type="text"
                  id="sitE_NAME"
                  className="form-control form-control-lg form-control-solid "
                  name="sitE_NAME"
                  placeholder="Change User"
                  value={formik.values.chG_USER}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="depT_NAME"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE DATE
                </label>
                <input
                  type="date"
                  id="datE_JOIN"
                  className="form-control form-control-lg form-control-solid  "
                  name="datE_JOIN"
                  placeholder="Change Date"
                  value={formik.values.chG_DATE?.split("T")[0]}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="designation"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE TERM
                </label>
                <input
                  type="text"
                  id="designation"
                  className="form-control form-control-lg form-control-solid "
                  name="designation"
                  placeholder="Change Term"
                  value={formik.values.chG_TERM}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="datE_JOIN"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE REASON
                </label>
                <input
                  type="text"
                  id="depT_NAME"
                  className="form-control form-control-lg form-control-solid  "
                  name="depT_NAME"
                  placeholder="No Change Reason"
                  value={formik.values.remarks}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>
            </div>
          </div>

          <div className="p-5">
            <>
              <div className="text-center">
                <ProgressMeter
                  transactionData={formik.values.reviewers}
                  flag={setFlag}
                />
              </div>
            </>
          </div>

          <div className="p-5 border">
            <>
              <div className="row mb-1">
                <div className="col-lg-4">
                  {/* <label className="form-control form-control-lg text-center bg-light-primary">
                    Field Name
                  </label> */}
                </div>
                <div className="col-lg-4">
                  <label className="form-control border-0 text-center fs-6">
                    PREVIOUS VALUES
                  </label>
                </div>
                <div className="col-lg-4">
                  <label className="form-control border-0 text-center fs-6">
                    CHANGED VALUES
                  </label>
                </div>
              </div>

              {/* <div className="row mb-1 delete_row">
                  <div className="col-lg-4">
                    <div className="d-flex align-items-center mb-1">
                      <label className="form-control border-0 text-start fs-6">
                        EMPLOYEE IMAGE :
                      </label>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="d-flex align-items-center mb-1">
                      <label className="form-control form-control-lg form-control-solid bg-light-danger">
                        {formik.values.fieldNames?.cadrE_CODE?.olD_VALUE}
                      </label>
                    </div>
                  </div>

                  <div className="col-lg-4">
                    <div className="d-flex align-items-center mb-1">
                      <label className="form-control form-control-lg form-control-solid bg-light-danger">
                        {formik.values.fieldNames?.cadrE_CODE?.neW_VALUE}
                      </label>
                    </div>
                  </div>
                </div> */}
              <div className="row mb-1 delete_row">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center justify-content-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      EMPLOYEE IMAGE :
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center justify-content-center mb-1">
                    <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative d-flex">
                      <img
                        src={
                          formik.values.fieldNames?.emP_IMAGE?.olD_VALUE ||
                          "/media/avatars/User.png"
                        }
                        alt="Old Employee"
                        className="faded-image p-2 mw-100px mh-100px border border-gray-300 borderRadius-5"
                        //   style={{
                        //     maxWidth: "100px",
                        //     maxHeight: "100px",
                        //     borderRadius: "5%",
                        //     border: "1px solid #CCCCCC",  // Add this line for a fine border
                        //   }}
                      />
                    </div>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center justify-content-center mb-1">
                    <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative d-flex">
                      <img
                        src={
                          formik.values.fieldNames?.emP_IMAGE?.neW_VALUE ||
                          "/media/avatars/User.png"
                        }
                        alt="New Employee"
                        className="faded-image p-2 mw-100px mh-100px border border-gray-300 borderRadius-5"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      EMPLOYEE CODE :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.descr?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.descr.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.descr.neW_VALUE"
                      value={formik.values?.fieldNames?.descr?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.descr?.neW_VALUE &&
                    formik.errors.fieldNames?.descr?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.descr.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      SHORT NAME :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.sH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.sH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.sH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.sH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.sH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.sH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.sH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>

              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      FIRST NAME :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.sH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.sH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.sH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.sH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.sH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.sH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.sH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>

              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      LAST NAME :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.sH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.sH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.sH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.sH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.sH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.sH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.sH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      MIDDLE NAME :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.sH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.sH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.sH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.sH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.sH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.sH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.sH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>

              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      GENDER :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.sH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.sH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.sH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.sH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.sH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.sH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.sH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>

              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      BIRTH DATE :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.sH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.sH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.sH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.sH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.sH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.sH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.sH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>

              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      RELIGION :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.sH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.sH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.sH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.sH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.sH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.sH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.sH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>

              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      BLOOD GROUP :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.sH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.sH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.sH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.sH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.sH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.sH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.sH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>

              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      FATHER NAME :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.sH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.sH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.sH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.sH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.sH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.sH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.sH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>

              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      MOTHER NAME :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.sH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.sH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.sH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.sH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.sH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.sH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.sH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>

              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      PERSONAL EMAIL :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.sH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.sH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.sH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.sH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.sH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.sH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.sH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>

              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      PERSONAL MOBILE :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.sH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.sH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.sH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.sH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.sH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.sH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.sH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>

             <div className="row mb-1 delete_row">
  <div className="col-lg-4">
    <div className="d-flex align-items-center justify-content-center mb-1">
      <label className="form-control border-0 text-start fs-6">
        UPLOADED DOCUMENTS:
      </label>
    </div>
  </div>

  <div className="col-lg-4">
    <div className="d-flex align-items-center justify-content-center mb-1">
      <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative d-flex">
        <img
          src={toAbsoluteUrl("media/svg/files/pdf.svg")}
          alt=""
          className="faded-image p-2 mw-50px mh-50px border border-gray-300 borderRadius-5"
          onClick={() => window.open('data:application/pdf;base64,JVBERi0xLjQAKMDAwMDA2NjE2==', '_blank')}
          style={{ cursor: 'pointer' }}
        />
      </div>
    </div>
  </div>

  <div className="col-lg-4">
    <div className="d-flex align-items-center justify-content-center mb-1">
      <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative d-flex">
        <img
          src={toAbsoluteUrl("media/svg/files/pdf.svg")}
          alt=""
          className="faded-image p-2 mw-50px mh-50px border border-gray-300 borderRadius-5"
          onClick={() => window.open('data:application/pdf;base64,JVBERi0xLjQAKMDAwMDA2NjE2==', '_blank')}
          style={{ cursor: 'pointer' }}
        />
      </div>
    </div>
  </div>
</div>


              {/* <div className="symbol symbol-45px me-5">
                        <img
                          src={toAbsoluteUrl("media/svg/files/doc.svg")}
                          alt=""
                        />
                      </div> */}
            </>
          </div>
        </div>

        <div className="card-footer d-flex justify-content-center py-6 px-9">
          {formik.values.confirmed === "N" &&
            formMode === Mode.VIEW &&
            loggedInUser !== formik.values.emP_CODE &&
            !flag && (
              <>
                <button
                  type="reset"
                  onClick={confirmApprove}
                  className="btn btn-light-success me-3"
                  data-kt-users-modal-action="cancel"
                  disabled={formik.isSubmitting || isBankDetailsLoading}
                >
                  Approve
                </button>
                <button
                  type="reset"
                  onClick={confirmDecline}
                  className="btn btn-light-danger me-3"
                  data-kt-users-modal-action="cancel"
                  disabled={formik.isSubmitting || isBankDetailsLoading}
                >
                  Reject
                </button>
              </>
            )}
          <button
            type="reset"
            onClick={closeForm}
            className="btn btn-light me-3"
            data-kt-users-modal-action="cancel"
            disabled={formik.isSubmitting || isBankDetailsLoading}
          >
            Cancel
          </button>

          {formMode !== Mode.VIEW &&
            loggedInUser === formik.values.emP_CODE && (
              <button
                type="submit"
                className="btn btn-primary"
                data-kt-users-modal-action="submit"
                disabled={formik.isSubmitting || isBankDetailsLoading}
                onClick={() => handleSubmit()}
              >
                <span className="indicator-label">Save</span>
                {(formik.isSubmitting || isBankDetailsLoading) && (
                  <span className="indicator-progress">
                    Please wait...
                    <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                  </span>
                )}
              </button>
            )}
        </div>
      </form>
    </>
  );
};

export { BasicDetailsTxEditModalForm };
