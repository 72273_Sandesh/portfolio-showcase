import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { useListView } from "../core/ListViewProvider";
import { useQueryResponse } from "../core/QueryResponseProvider";
import "./TrackingComponent.css";
import { initialMasterDetails, MasterDetails } from "../core/_models";
import axios from "axios";
import { useAuth } from "../../../../auth";
import { toast } from "react-toastify";
import { ID, Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { ProgressMeter } from "./ProgressMeter";
import Swal from "sweetalert2";
import { useMutation, useQueryClient } from "react-query";
import { approveMasterDetails, declineMasterDetails } from "../core/_requests";

type Props = {
  isBankDetailsLoading: boolean;
  masterDetails: MasterDetails;

  // id: ID;
  // tableName: string | undefined;
  // emP_CODE?: string | undefined;
  // remarks?: string;
  // wF_PROCESS_LEVEL?: number;
};
interface deptHead {
  shortName: string;
  empCode: string;
}
const editBankDetailsSchema = Yup.object().shape({
  descr: Yup.string().required("Description of Payment is required"),
  SH_DESCR: Yup.string().required("Short Description is required"),
  deptHead: Yup.string().required("Cadre Category is required"),
  status: Yup.string().required("Status is required"),
});

const API_URL = import.meta.env.VITE_DEPT_DROPDOWNS_API_URL;
const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;

const DepartmentTxEditModalForm: FC<Props> = ({
  masterDetails,
  isBankDetailsLoading,
}) => {
  const { itemIdForUpdate, setItemIdForUpdate, formMode, tableNameForUpdate } =
    useListView();
  const { refetch } = useQueryResponse();
  const { currentUser } = useAuth();
  const [statuses] = useState(["ACTIVE", "INACTIVE"]);
  const [isEditable, setIsEditable] = useState<boolean>();
  const { query } = useQueryResponse();
  const queryClient = useQueryClient();
  const loggedInUser = currentUser?.id?.toString() || "";
  const ip = "term";
  const [flag, setFlag] = useState<boolean>(false);
  const [deptHeads, setDeptHeads] = useState<deptHead[]>([]);

  useEffect(() => {
    axios
      .get(API_URL)
      .then((response) => {
        setDeptHeads(response.data.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));

    setIsEditable(formMode === Mode.VIEW);
  }, []);

  const closeForm = () => {
    setItemIdForUpdate(undefined);
  };

  const approveItem = useMutation(
    (remarks: string) =>
      approveMasterDetails(
        itemIdForUpdate,
        tableNameForUpdate,
        formik.values.chG_USER,
        remarks,
        formik.values.wF_PROCESS_LEVEL?.toString(),
        loggedInUser,
        ip
      ),
    {
      onSuccess: (response: any) => {
        const { responseStatus, message } = response.data;
        console.log(response.data);
        if (responseStatus === "S") {
          Swal.fire({
            title: "Success!",
            text: "Transaction approved successfully.",
            icon: "success",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
          setItemIdForUpdate(undefined);
        } else if (responseStatus === "E") {
          Swal.fire({
            title: "Error!",
            text: `Error approving item: ${message.join(", ")}`,
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        } else {
          Swal.fire({
            title: "Error!",
            text: "Unknown response status.",
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        }
      },
      onError: (error: Error) => {
        Swal.fire({
          title: "Error!",
          text: `Error approving item: ${error.message || "Unknown error"}`,
          icon: "error",
          confirmButtonText: "OK",
        });
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);
      },
    }
  );

  const declineItem = useMutation(
    (transactionDetails: object) => declineMasterDetails(transactionDetails),
    {
      onSuccess: (response: any) => {
        const { responseStatus, message } = response.data;
        console.log(response.data);
        if (responseStatus === "S") {
          Swal.fire({
            title: "Success!",
            text: "Transaction Rejected successfully.",
            icon: "success",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
          setItemIdForUpdate(undefined);
        } else if (responseStatus === "E") {
          Swal.fire({
            title: "Error!",
            text: `Error While Rejecting Transaction: ${message.join(", ")}`,
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        } else {
          Swal.fire({
            title: "Error!",
            text: "Unknown response status.",
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        }
      },
      onError: (error: Error) => {
        Swal.fire({
          title: "Error!",
          text: `Error While Rejecting Transaction: ${
            error.message || "Unknown error"
          }`,
          icon: "error",
          confirmButtonText: "OK",
        });
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);
      },
    }
  );

  const confirmApprove = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "Do you want to approve this Transaction?",
      icon: "warning",
      input: "text", // Adding input field for remarks
      inputPlaceholder: "Enter remarks...",
      inputAttributes: {
        "aria-label": "Type your remarks here",
      },
      showCancelButton: true,
      confirmButtonText: "Yes, approve it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
      preConfirm: (remarksApprove) => {
        if (!remarksApprove) {
          Swal.showValidationMessage("Remarks are required.");
        } else {
          return remarksApprove;
        }
      },
    }).then((result) => {
      if (result.isConfirmed) {
        approveItem.mutate(result.value);
      }
    });
  };

  const confirmDecline = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "Do you want to reject this transaction?",
      icon: "warning",
      input: "text",
      inputPlaceholder: "Enter remarks...",
      inputAttributes: {
        "aria-label": "Type your remarks here",
      },
      showCancelButton: true,
      confirmButtonText: "Yes, reject it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
      preConfirm: (remarksDecline) => {
        if (!remarksDecline) {
          Swal.showValidationMessage("Remarks are required.");
        } else {
          return remarksDecline;
        }
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const transactionDetails = {
          transactionId: masterDetails.id,
          empCode: loggedInUser,
          remoteAddr: ip,
          remarks: result.value,
        };
        declineItem.mutate(transactionDetails);
      }
    });
  };

  const [bankDetailsForEdit] = useState<MasterDetails>({
    ...masterDetails,
    id: masterDetails.id || initialMasterDetails.id,
    fieldNames: masterDetails.fieldNames || initialMasterDetails.fieldNames,
    reviewers: masterDetails.reviewers || initialMasterDetails.reviewers,
    filE_PATH: masterDetails.filE_PATH || initialMasterDetails.filE_PATH,
    chG_DATE: masterDetails.chG_DATE || initialMasterDetails.chG_DATE,
    chG_USER: masterDetails.chG_USER || initialMasterDetails.chG_USER,
    chG_TERM: masterDetails.chG_TERM || initialMasterDetails.chG_TERM,
    traN_DATE: masterDetails.traN_DATE || initialMasterDetails.traN_DATE,
    remarks: masterDetails.remarks || initialMasterDetails.remarks,
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };

  const handleSubmit = () => {
    const values = formik.values;

    console.log("Submitting..................");
    const payload = {
      chG_DATE: new Date(),
      chG_USER: currentUser?.id,
      tableName: "CADRE",
      keyVar: "",
      wfLevel: 1,
      conf: "N",
      siteCode: "S0001",
      emp: "H032422   ",
      filePath: values.filE_PATH || null,
      file: null,
      folderPath: null,
      strTranId: values.id || "",
      remarks: values.remarks || "",
      chg_Term: values.chG_TERM || "",
      fieldUpdates: [
        {
          fieldName: "DEPT_CODE",
          oldValue: values.fieldNames?.DEPT_CODE?.olD_VALUE || "",
          newValue: values.fieldNames?.DEPT_CODE?.neW_VALUE || "",
        },
        {
          fieldName: "DESCR",
          oldValue: values.fieldNames?.DESCR?.olD_VALUE || "",
          newValue: values.fieldNames?.DESCR?.neW_VALUE || "",
        },
        {
          fieldName: "SH_DESCR",
          oldValue: values.fieldNames?.SH_DESCR?.olD_VALUE || "",
          newValue: values.fieldNames?.SH_DESCR?.neW_VALUE || "",
        },
        {
          fieldName: "deptHead",
          oldValue: values.fieldNames?.DEPT_HEAD?.olD_VALUE || "",
          newValue: values.fieldNames?.DEPT_HEAD?.neW_VALUE || "",
        },
        {
          fieldName: "status",
          oldValue: values.fieldNames?.status?.olD_VALUE || "",
          newValue: values.fieldNames?.status?.neW_VALUE || "",
        },
      ],
    };

    axios
      .post(`${MASTERTX_URL}/InsertMasterTransaction`, payload)
      .then((response) => {
        const { responseStatus, message } = response.data.data;
        if (responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{message}</h6>
              </p>
            </div>
          );
        } else {
          // setItemIdForUpdate(undefined);
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
      });
  };

  const formik = useFormik({
    initialValues: bankDetailsForEdit,
    validationSchema: editBankDetailsSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);
    },
  });

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="card-body">
          <div className="bg-light-warning border border-dashed border-gray-300 rounded p-5">
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="trId"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  TRANSACTION ID
                </label>
                <input
                  type="text"
                  id="id"
                  className="form-control form-control-lg form-control-solid "
                  name="id"
                  placeholder="Transaction Id"
                  value={formik.values?.id}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="trDate"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  TRANSACTION DATE
                </label>
                <input
                  type="date"
                  id="trDate"
                  className="form-control form-control-lg form-control-solid"
                  name="trDate"
                  placeholder="Date of Transaction"
                  value={formik.values.traN_DATE?.split("T")[0]}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sitE_NAME"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE USER
                </label>
                <input
                  type="text"
                  id="sitE_NAME"
                  className="form-control form-control-lg form-control-solid "
                  name="sitE_NAME"
                  placeholder="Change User"
                  value={formik.values.chG_USER}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="depT_NAME"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE DATE
                </label>
                <input
                  type="date"
                  id="datE_JOIN"
                  className="form-control form-control-lg form-control-solid  "
                  name="datE_JOIN"
                  placeholder="Change Date"
                  value={formik.values.chG_DATE?.split("T")[0]}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="designation"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE TERM
                </label>
                <input
                  type="text"
                  id="designation"
                  className="form-control form-control-lg form-control-solid "
                  name="designation"
                  placeholder="Change Term"
                  value={formik.values.chG_TERM}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="datE_JOIN"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE REASON
                </label>
                <input
                  type="text"
                  id="depT_NAME"
                  className="form-control form-control-lg form-control-solid  "
                  name="depT_NAME"
                  placeholder="No Change Reason"
                  value={formik.values.remarks}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>
            </div>
          </div>

          <div className="p-5">
            <>
              {/* <div className="text-center">
                <img
                  src="/media/logos/track02.png"
                  style={{ height: "120px" }}
                />
              </div> */}
              {/* <ProgressMeter appointment={appointment}/> */}

              <div className="text-center">
                <ProgressMeter
                  transactionData={formik.values.reviewers}
                  flag={setFlag}
                />
              </div>
            </>
          </div>

          <div className="p-5 border">
            <>
              <div className="row mb-1">
                <div className="col-lg-4">
                  {/* <label className="form-control form-control-lg text-center bg-light-primary">
                    Field Name
                  </label> */}
                </div>
                <div className="col-lg-4">
                  <label className="form-control border-0 text-center fs-6">
                    PREVIOUS VALUES
                  </label>
                </div>
                <div className="col-lg-4">
                  <label className="form-control border-0 text-center fs-6">
                    CHANGED VALUES
                  </label>
                </div>
              </div>

              {formik.values.fieldNames?.DEPT_CODE?.olD_VALUE !== null && (
                <div className="row mb-1 delete_row">
                  <div className="col-lg-4">
                    <div className="d-flex align-items-center mb-1">
                      <label className="form-control border-0 text-start fs-6">
                        DEPARTMENT CODE :
                      </label>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="d-flex align-items-center mb-1">
                      <label className="form-control form-control-lg form-control-solid bg-light-danger">
                        {formik.values.fieldNames?.DEPT_CODE?.olD_VALUE}
                      </label>
                    </div>
                  </div>

                  <div className="col-lg-4">
                    <div className="d-flex align-items-center mb-1">
                      <label className="form-control form-control-lg form-control-solid bg-light-danger">
                        {formik.values.fieldNames?.DEPT_CODE?.neW_VALUE}
                      </label>
                    </div>
                  </div>
                </div>
              )}

              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      DESCRIPTION :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.name.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.name.neW_VALUE"
                      value={formik.values?.fieldNames?.DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      SHORT DESCRIPTION :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.SH_DESCR?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.SH_DESCR.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.SH_DESCR.neW_VALUE"
                      value={formik.values?.fieldNames?.SH_DESCR?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.SH_DESCR?.neW_VALUE &&
                    formik.errors.fieldNames?.SH_DESCR?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.SH_DESCR.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      DEPARTMENT HEAD :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <select
                      className="form-control form-control-lg form-control-solid bg-light-danger"
                      value={formik.values?.fieldNames?.DEPT_HEAD?.olD_VALUE}
                      disabled
                    >
                      <option value=""></option>
                      {deptHeads.map((deptHead) => (
                        <option key={deptHead.empCode} value={deptHead.empCode}>
                          {deptHead.shortName}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <select
                      className="form-select form-select-lg form-select-solid"
                      value={formik.values?.fieldNames?.DEPT_HEAD?.neW_VALUE}
                      // onChange={formik.handleChange}
                      onChange={(e) => {
                        formik.setFieldValue(
                          "fieldNames.DEPT_HEAD.neW_VALUE",
                          e.target.value
                        );
                      }}
                      disabled={isEditable}
                    >
                      {deptHeads.map((deptHead) => (
                        <option key={deptHead.empCode} value={deptHead.empCode}>
                          {deptHead.shortName}
                        </option>
                      ))}
                    </select>
                    {formik.touched.fieldNames?.DEPT_HEAD?.neW_VALUE &&
                    formik.errors.fieldNames?.DEPT_HEAD?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.DEPT_HEAD.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      STATUS :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values?.fieldNames?.status?.olD_VALUE === "A"
                        ? "ACTIVE"
                        : formik.values?.fieldNames?.status?.olD_VALUE === "N"
                        ? "INACTIVE"
                        : ""}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    {/* <input
                      type="text"
                      id="fieldNames.status.neW_VALUE"
                      className="form-select form-select-lg form-select-solid"
                      name="fieldNames.status.neW_VALUE"
                      value={formik.values?.fieldNames?.status?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    /> */}
                    <select
                      className="form-select form-select-lg form-select-solid"
                      value={formik.values?.fieldNames?.status?.neW_VALUE}
                      // onChange={formik.handleChange}
                      onChange={(e) => {
                        formik.setFieldValue(
                          "fieldNames.status.neW_VALUE",
                          e.target.value
                        );
                      }}
                      disabled={isEditable}
                    >
                      {/* <option value="">
                        {formik.values?.fieldNames?.status?.neW_VALUE === "A"
                          ? "ACTIVE"
                          : formik.values?.fieldNames?.status?.neW_VALUE === "N"
                          ? "INACTIVE"
                          : ""}
                      </option> */}
                      {statuses.map((status) => (
                        <option value={status === "ACTIVE" ? "A" : "N"}>
                          {status}
                        </option>
                      ))}
                    </select>
                    {formik.touched.fieldNames?.status?.neW_VALUE &&
                    formik.errors.fieldNames?.status?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.status.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
            </>
          </div>
        </div>

        <div className="card-footer d-flex justify-content-center py-6 px-9">
          {formik.values.confirmed === "N" &&
            formMode === Mode.VIEW &&
            loggedInUser !== formik.values.emP_CODE &&
            !flag && (
              <>
                <button
                  type="reset"
                  onClick={confirmApprove}
                  className="btn btn-light-success me-3"
                  data-kt-users-modal-action="cancel"
                  disabled={formik.isSubmitting || isBankDetailsLoading}
                >
                  Approve
                </button>
                <button
                  type="reset"
                  onClick={confirmDecline}
                  className="btn btn-light-danger me-3"
                  data-kt-users-modal-action="cancel"
                  disabled={formik.isSubmitting || isBankDetailsLoading}
                >
                  Reject
                </button>
                {/* <button
                  type="reset"
                  onClick={confirmDecline}
                  className="btn btn-light-danger me-3"
                  data-kt-users-modal-action="cancel"
                  disabled={formik.isSubmitting || isBankDetailsLoading}
                >
                  Push Back
                </button> */}
              </>
            )}
          <button
            type="reset"
            onClick={closeForm}
            className="btn btn-light me-3"
            data-kt-users-modal-action="cancel"
            disabled={formik.isSubmitting || isBankDetailsLoading}
          >
            Cancel
          </button>

          {formMode !== Mode.VIEW &&
            loggedInUser === formik.values.emP_CODE && (
              <button
                type="submit"
                className="btn btn-primary"
                data-kt-users-modal-action="submit"
                disabled={formik.isSubmitting || isBankDetailsLoading}
                onClick={() => handleSubmit()}
              >
                <span className="indicator-label">Save</span>
                {(formik.isSubmitting || isBankDetailsLoading) && (
                  <span className="indicator-progress">
                    Please wait...
                    <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                  </span>
                )}
              </button>
            )}
        </div>
      </form>
    </>
  );
};

export { DepartmentTxEditModalForm };
