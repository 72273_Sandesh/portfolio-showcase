import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { useListView } from "../core/ListViewProvider";
import { useQueryResponse } from "../core/QueryResponseProvider";
import "./TrackingComponent.css";
import { initialMasterDetails, MasterDetails } from "../core/_models";
import axios from "axios";
import { useAuth } from "../../../../auth";
import { toast } from "react-toastify";
import { ID, Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { ProgressMeter } from "./ProgressMeter";
import Swal from "sweetalert2";
import { useMutation, useQueryClient } from "react-query";
import { approveMasterDetails, declineMasterDetails } from "../core/_requests";
import { employeeIdForView } from "../../../../GlobalQuery";

type Props = {
  isBankDetailsLoading: boolean;
  masterDetails: MasterDetails;
};
interface EmpSiteDropdownItem {
  siteCode: string;
  description: string;
  areaCode: string;
  address: string;
  financialEntity: string;
}
interface BankDetailsDropdown {
  banK_CODE: string;
  banK_NAME: string;
  banK_TYPE: string;
}
const editBankDetailsSchema = Yup.object().shape({
  descr: Yup.string().required("Description of Payment is required"),
  sH_DESCR: Yup.string().required("Short Description is required"),
  banD_CODE: Yup.string().required("Cadre Category is required"),
  status: Yup.string().required("Status is required"),
});

const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const EmpBankDetailsTxEditModalForm: FC<Props> = ({
  masterDetails,
  isBankDetailsLoading,
}) => {
  const { itemIdForUpdate, setItemIdForUpdate, formMode, tableNameForUpdate } =
    useListView();
  const { refetch } = useQueryResponse();
  const { currentUser } = useAuth();
  const [statuses] = useState(["ACTIVE", "INACTIVE"]);
  const [isEditable, setIsEditable] = useState<boolean>();
  const { query } = useQueryResponse();
  const queryClient = useQueryClient();
  const loggedInUser = currentUser?.id?.toString() || "";
  const ip = "term";
  const [flag, setFlag] = useState<boolean>(false);

  const [drpdwnEmpSites, setDrpdwnEmpSites] = useState<EmpSiteDropdownItem[]>(
    []
  );
  const [bankNameDropdown, setBankNameDropdown] = useState<
    BankDetailsDropdown[]
  >([]);
  const [payModes] = useState([
    { code: "C", descr: "CASH" },
    { code: "Q", descr: "CHEQUE" },
    { code: "D", descr: "DEMAND DRAFT" },
    { code: "T", descr: "TRANSFER" },
  ]);
  const [payTypes] = useState([
    { code: "S", descr: "SALARY" },
    { code: "W", descr: "WAGES" },
  ]);
  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/ddl_GetBank`)
      .then((response) => {
        setBankNameDropdown(response.data.banks);
      })
      .catch((error) =>
        console.error("Error fetching Bank Names data:", error)
      );

    axios
      .get(
        `${EMPLOYEE_URL}/EMP_WORK_PAYROLL_site/${
          employeeIdForView ? employeeIdForView : "H032422   "
        }`
      )
      .then((response) => {
        setDrpdwnEmpSites(response.data);
      })
      .catch((error) => {
        console.error("Error fetching EmpSite details:", error);
      });

    setIsEditable(formMode === Mode.VIEW);
  }, []);

  const closeForm = () => {
    setItemIdForUpdate(undefined);
  };

  const approveItem = useMutation(
    (remarks: string) =>
      approveMasterDetails(
        itemIdForUpdate,
        tableNameForUpdate,
        formik.values.chG_USER,
        remarks,
        formik.values.wF_PROCESS_LEVEL?.toString(),
        loggedInUser,
        ip
      ),
    {
      onSuccess: (response: any) => {
        const { responseStatus, message } = response.data;
        console.log(response.data);
        if (responseStatus === "S") {
          Swal.fire({
            title: "Success!",
            text: "Transaction approved successfully.",
            icon: "success",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
          setItemIdForUpdate(undefined);
        } else if (responseStatus === "E") {
          Swal.fire({
            title: "Error!",
            text: `Error approving item: ${message.join(", ")}`,
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        } else {
          Swal.fire({
            title: "Error!",
            text: "Unknown response status.",
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        }
      },
      onError: (error: Error) => {
        Swal.fire({
          title: "Error!",
          text: `Error approving item: ${error.message || "Unknown error"}`,
          icon: "error",
          confirmButtonText: "OK",
        });
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);
      },
    }
  );

  const declineItem = useMutation(
    (transactionDetails: object) => declineMasterDetails(transactionDetails),
    {
      onSuccess: (response: any) => {
        const { responseStatus, message } = response.data;
        console.log(response.data);
        if (responseStatus === "S") {
          Swal.fire({
            title: "Success!",
            text: "Transaction Rejected successfully.",
            icon: "success",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
          setItemIdForUpdate(undefined);
        } else if (responseStatus === "E") {
          Swal.fire({
            title: "Error!",
            text: `Error While Rejecting Transaction: ${message.join(", ")}`,
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        } else {
          Swal.fire({
            title: "Error!",
            text: "Unknown response status.",
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        }
      },
      onError: (error: Error) => {
        Swal.fire({
          title: "Error!",
          text: `Error While Rejecting Transaction: ${
            error.message || "Unknown error"
          }`,
          icon: "error",
          confirmButtonText: "OK",
        });
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);
      },
    }
  );

  const confirmApprove = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "Do you want to approve this Transaction?",
      icon: "warning",
      input: "text", // Adding input field for remarks
      inputPlaceholder: "Enter remarks...",
      inputAttributes: {
        "aria-label": "Type your remarks here",
      },
      showCancelButton: true,
      confirmButtonText: "Yes, approve it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
      preConfirm: (remarksApprove) => {
        if (!remarksApprove) {
          Swal.showValidationMessage("Remarks are required.");
        } else {
          return remarksApprove;
        }
      },
    }).then((result) => {
      if (result.isConfirmed) {
        approveItem.mutate(result.value); // Pass the remarks to the mutation
      }
    });
  };

  const confirmDecline = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "Do you want to reject this transaction?",
      icon: "warning",
      input: "text",
      inputPlaceholder: "Enter remarks...",
      inputAttributes: {
        "aria-label": "Type your remarks here",
      },
      showCancelButton: true,
      confirmButtonText: "Yes, reject it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
      preConfirm: (remarksDecline) => {
        if (!remarksDecline) {
          Swal.showValidationMessage("Remarks are required.");
        } else {
          return remarksDecline;
        }
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const transactionDetails = {
          transactionId: masterDetails.id,
          empCode: loggedInUser,
          remoteAddr: ip,
          remarks: result.value,
        };
        declineItem.mutate(transactionDetails);
      }
    });
  };

  const [bankDetailsForEdit] = useState<MasterDetails>({
    ...masterDetails,
    id: masterDetails.id || initialMasterDetails.id,
    fieldNames: masterDetails.fieldNames || initialMasterDetails.fieldNames,
    reviewers: masterDetails.reviewers || initialMasterDetails.reviewers,
    filE_PATH: masterDetails.filE_PATH || initialMasterDetails.filE_PATH,
    chG_DATE: masterDetails.chG_DATE || initialMasterDetails.chG_DATE,
    chG_USER: masterDetails.chG_USER || initialMasterDetails.chG_USER,
    chG_TERM: masterDetails.chG_TERM || initialMasterDetails.chG_TERM,
    traN_DATE: masterDetails.traN_DATE || initialMasterDetails.traN_DATE,
    remarks: masterDetails.remarks || initialMasterDetails.remarks,
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };

  const handleSubmit = () => {
    const values = formik.values;

    console.log("Submitting..................");
    const payload = {
      chG_DATE: new Date(),
      chG_USER: currentUser?.id,
      tableName: "CADRE",
      keyVar: "",
      wfLevel: 1,
      conf: "N",
      siteCode: "S0001",
      emp: "H032422   ",
      filePath: values.filE_PATH || null,
      file: null,
      folderPath: null,
      strTranId: values.id || "",
      remarks: values.remarks || "",
      chg_Term: values.chG_TERM || "",
      fieldUpdates: [
        {
          fieldName: "cadrE_CODE",
          oldValue: values.fieldNames?.cadrE_CODE?.olD_VALUE || "",
          newValue: values.fieldNames?.cadrE_CODE?.neW_VALUE || "",
        },
        {
          fieldName: "descr",
          oldValue: values.fieldNames?.descr?.olD_VALUE || "",
          newValue: values.fieldNames?.descr?.neW_VALUE || "",
        },
        {
          fieldName: "sH_DESCR",
          oldValue: values.fieldNames?.sH_DESCR?.olD_VALUE || "",
          newValue: values.fieldNames?.sH_DESCR?.neW_VALUE || "",
        },
        {
          fieldName: "banD_CODE",
          oldValue: values.fieldNames?.banD_CODE?.olD_VALUE || "",
          newValue: values.fieldNames?.banD_CODE?.neW_VALUE || "",
        },
        {
          fieldName: "status",
          oldValue: values.fieldNames?.status?.olD_VALUE || "",
          newValue: values.fieldNames?.status?.neW_VALUE || "",
        },
      ],
    };

    axios
      .post(` ${MASTERTX_URL}/InsertMasterTransaction`, payload)
      .then((response) => {
        const { responseStatus, message } = response.data.data;
        if (responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <p className="alert alert-danger" role="alert">
                <h6>{message}</h6>
              </p>
            </div>
          );
        } else {
          // setItemIdForUpdate(undefined);
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <p className="alert alert-success" role="alert">
                <h6>{message}</h6>
              </p>
            </div>
          );
          cancel(true);
        }
      });
  };

  const formik = useFormik({
    initialValues: bankDetailsForEdit,
    validationSchema: editBankDetailsSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);
    },
  });

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="card-body">
          <div className="bg-light-warning border border-dashed border-gray-300 rounded p-5">
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="trId"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  TRANSACTION ID
                </label>
                <input
                  type="text"
                  id="id"
                  className="form-control form-control-lg form-control-solid "
                  name="id"
                  placeholder="Transaction Id"
                  value={formik.values?.id}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="trDate"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  TRANSACTION DATE
                </label>
                <input
                  type="date"
                  id="trDate"
                  className="form-control form-control-lg form-control-solid"
                  name="trDate"
                  placeholder="Date of Transaction"
                  value={formik.values.traN_DATE?.split("T")[0]}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sitE_NAME"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE USER
                </label>
                <input
                  type="text"
                  id="sitE_NAME"
                  className="form-control form-control-lg form-control-solid "
                  name="sitE_NAME"
                  placeholder="Change User"
                  value={formik.values.chG_USER}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="depT_NAME"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE DATE
                </label>
                <input
                  type="date"
                  id="datE_JOIN"
                  className="form-control form-control-lg form-control-solid  "
                  name="datE_JOIN"
                  placeholder="Change Date"
                  value={formik.values.chG_DATE?.split("T")[0]}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="designation"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE TERM
                </label>
                <input
                  type="text"
                  id="designation"
                  className="form-control form-control-lg form-control-solid "
                  name="designation"
                  placeholder="Change Term"
                  value={formik.values.chG_TERM}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="datE_JOIN"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE REASON
                </label>
                <input
                  type="text"
                  id="depT_NAME"
                  className="form-control form-control-lg form-control-solid  "
                  name="depT_NAME"
                  placeholder="No Change Reason"
                  value={formik.values.remarks}
                  onChange={formik.handleChange}
                  disabled
                />
              </div>
            </div>
          </div>

          <div className="p-5">
            <>
              <div className="text-center">
                <ProgressMeter
                  transactionData={formik.values.reviewers}
                  flag={setFlag}
                />
              </div>
            </>
          </div>

          <div className="p-5 border">
            <>
              <div className="row mb-1">
                <div className="col-lg-4">
                  {/* <label className="form-control form-control-lg text-center bg-light-primary">
                    Field Name
                  </label> */}
                </div>
                <div className="col-lg-4">
                  <label className="form-control border-0 text-center fs-6">
                    PREVIOUS VALUES
                  </label>
                </div>
                <div className="col-lg-4">
                  <label className="form-control border-0 text-center fs-6">
                    CHANGED VALUES
                  </label>
                </div>
              </div>
              {formik.values.fieldNames?.EMP_CODE?.olD_VALUE !== null && (
                <div className="row mb-1 delete_row">
                  <div className="col-lg-4">
                    <div className="d-flex align-items-center mb-1">
                      <label className="form-control border-0 text-start fs-6">
                        EMPLOYEE CODE
                      </label>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="d-flex align-items-center mb-1">
                      <label className="form-control form-control-lg form-control-solid bg-light-danger">
                        {formik.values.fieldNames?.EMP_CODE?.olD_VALUE}
                      </label>
                    </div>
                  </div>

                  <div className="col-lg-4">
                    <div className="d-flex align-items-center mb-1">
                      <label className="form-control form-control-lg form-control-solid bg-light-danger">
                        {formik.values.fieldNames?.EMP_CODE?.neW_VALUE}
                      </label>
                    </div>
                  </div>
                </div>
              )}
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      WORK SITE :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <select
                      className="form-select form-select-lg form-select-solid bg-light-danger"
                      value={formik.values?.fieldNames?.WORK_SITE?.olD_VALUE}
                      disabled={isEditable}
                    >
                      <option value=""></option>
                      {drpdwnEmpSites.map((workSite) => (
                        <option
                          key={workSite.siteCode}
                          value={workSite.siteCode}
                        >
                          {workSite.description}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <select
                      className="form-select form-select-lg form-select-solid"
                      value={formik.values?.fieldNames?.WORK_SITE?.neW_VALUE}
                      disabled={isEditable}
                    >
                      <option value=""></option>
                      {drpdwnEmpSites.map((workSite) => (
                        <option
                          key={workSite.siteCode}
                          value={workSite.siteCode}
                        >
                          {workSite.description}
                        </option>
                      ))}
                    </select>
                    {formik.touched.fieldNames?.WORK_SITE?.neW_VALUE &&
                    formik.errors.fieldNames?.WORK_SITE?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.WORK_SITE.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      GRATUITY DATE :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {
                        formik.values.fieldNames?.GRATUITY_DATE?.olD_VALUE?.split(
                          "T"
                        )[0]
                      }
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.GRATUITY_DATE.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.GRATUITY_DATE.neW_VALUE"
                      value={
                        formik.values?.fieldNames?.GRATUITY_DATE?.neW_VALUE?.split(
                          "T"
                        )[0]
                      }
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.GRATUITY_DATE?.neW_VALUE &&
                    formik.errors.fieldNames?.GRATUITY_DATE?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.GRATUITY_DATE.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      BASIC PER MONTH :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.BASIC?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.BASIC.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.BASIC.neW_VALUE"
                      value={formik.values?.fieldNames?.BASIC?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.BASIC?.neW_VALUE &&
                    formik.errors.fieldNames?.BASIC?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.BASIC.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      CTC PER MONTH :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.CURR_CTC?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.CURR_CTC.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.CURR_CTC.neW_VALUE"
                      value={formik.values?.fieldNames?.CURR_CTC?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.CURR_CTC?.neW_VALUE &&
                    formik.errors.fieldNames?.CURR_CTC?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.CURR_CTC.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      NOTICE PERIOD :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values.fieldNames?.NOTICE_PRD?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.n?.NOTICE_PRD.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.n?.NOTICE_PRD.neW_VALUE"
                      value={formik.values?.fieldNames?.NOTICE_PRD?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />
                    {formik.touched.fieldNames?.NOTICE_PRD?.neW_VALUE &&
                    formik.errors.fieldNames?.NOTICE_PRD?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.NOTICE_PRD.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      DD PAYABLE BANK :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values?.fieldNames?.DD_PAYABLE_BANK?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.n?.DD_PAYABLE_BANK.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.n?.DD_PAYABLE_BANK.neW_VALUE"
                      value={
                        formik.values?.fieldNames?.DD_PAYABLE_BANK?.neW_VALUE
                      }
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />

                    {formik.touched.fieldNames?.status?.neW_VALUE &&
                    formik.errors.fieldNames?.status?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.status.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      MODE OF PAYMENT :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <select
                      className="form-select form-select-lg form-select-solid  bg-light-danger"
                      value={formik.values?.fieldNames?.PAY_MODE?.olD_VALUE}
                      disabled={isEditable}
                    >
                      <option value=""></option>
                      {payModes.map((workSite) => (
                        <option key={workSite.code} value={workSite.code}>
                          {workSite.descr}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <select
                      className="form-select form-select-lg form-select-solid"
                      value={formik.values?.fieldNames?.PAY_MODE?.neW_VALUE}
                      disabled={isEditable}
                    >
                      <option value=""></option>
                      {payModes.map((workSite) => (
                        <option key={workSite.code} value={workSite.code}>
                          {workSite.descr}
                        </option>
                      ))}
                    </select>
                    {formik.touched.fieldNames?.status?.neW_VALUE &&
                    formik.errors.fieldNames?.status?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.status.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      TYPE OF PAYMENT :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <select
                      className="form-select form-select-lg form-select-solid bg-light-danger"
                      value={formik.values?.fieldNames?.PAY_TYPE?.olD_VALUE}
                      disabled={isEditable}
                    >
                      <option value=""></option>
                      {payTypes.map((workSite) => (
                        <option key={workSite.code} value={workSite.code}>
                          {workSite.descr}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <select
                      className="form-select form-select-lg form-select-solid"
                      value={formik.values?.fieldNames?.PAY_TYPE?.neW_VALUE}
                      disabled={isEditable}
                    >
                      <option value=""></option>
                      {payTypes.map((workSite) => (
                        <option key={workSite.code} value={workSite.code}>
                          {workSite.descr}
                        </option>
                      ))}
                    </select>

                    {formik.touched.fieldNames?.status?.neW_VALUE &&
                    formik.errors.fieldNames?.status?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.status.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      BANK NAME :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <select
                      id="fieldNames.BANK_CODE.olD_VALUE"
                      name="fieldNames.BANK_CODE.olD_VALUE"
                      className="form-select form-select-lg form-select-solid bg-light-danger"
                      value={formik.values?.fieldNames?.BANK_CODE?.olD_VALUE}
                      disabled={isEditable}
                    >
                      <option value=""></option>
                      {bankNameDropdown.map((workSite) => (
                        <option
                          key={workSite.banK_CODE}
                          value={workSite.banK_CODE.trim()}
                        >
                          {workSite.banK_NAME}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <select
                      id="fieldNames.n?.BANK_CODE.neW_VALUE"
                      name="fieldNames.n?.BANK_CODE.neW_VALUE"
                      className="form-select form-select-lg form-select-solid"
                      value={formik.values?.fieldNames?.BANK_CODE?.neW_VALUE}
                      disabled={isEditable}
                    >
                      <option value=""></option>
                      {bankNameDropdown.map((bankName) => (
                        <option
                          key={bankName.banK_CODE}
                          value={bankName.banK_CODE.trim()}
                        >
                          {bankName.banK_NAME.toUpperCase()}
                        </option>
                      ))}
                    </select>
                    {formik.touched.fieldNames?.BANK_CODE?.neW_VALUE &&
                    formik.errors.fieldNames?.BANK_CODE?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.BANK_CODE.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      ACCOUNT NO :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values?.fieldNames?.BANK_ACCT?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.n?.BANK_ACCT.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.n?.BANK_ACCT.neW_VALUE"
                      value={formik.values?.fieldNames?.BANK_ACCT?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />

                    {formik.touched.fieldNames?.BANK_ACCT?.neW_VALUE &&
                    formik.errors.fieldNames?.BANK_ACCT?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.BANK_ACCT.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      IFSC CODE :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values?.fieldNames?.BANK_IFSC?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.n?.BANK_IFSC.neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.n?.BANK_IFSC.neW_VALUE"
                      value={formik.values?.fieldNames?.BANK_IFSC?.neW_VALUE}
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />

                    {formik.touched.fieldNames?.BANK_IFSC?.neW_VALUE &&
                    formik.errors.fieldNames?.BANK_IFSC?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.BANK_IFSC.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              {/* <div className="row mb-1">
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control border-0 text-start fs-6">
                      ATTACHED FILE: :
                    </label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <label className="form-control form-control-lg form-control-solid bg-light-danger">
                      {formik.values?.fieldNames?.?.olD_VALUE}
                    </label>
                  </div>
                </div>

                <div className="col-lg-4">
                  <div className="d-flex align-items-center mb-1">
                    <input
                      type="text"
                      id="fieldNames.n?..neW_VALUE"
                      className="form-control form-control-lg form-control-solid"
                      name="fieldNames.n?..neW_VALUE"
                      value={
                        formik.values?.fieldNames?.?.neW_VALUE
                      }
                      onChange={formik.handleChange}
                      disabled={isEditable}
                    />

                    {formik.touched.fieldNames?.status?.neW_VALUE &&
                    formik.errors.fieldNames?.status?.neW_VALUE ? (
                      <div className="text-danger">
                        {formik.errors.fieldNames.status.neW_VALUE}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div> */}
            </>
          </div>
        </div>

        <div className="card-footer d-flex justify-content-center py-6 px-9">
          {formik.values.confirmed === "N" &&
            formMode === Mode.VIEW &&
            loggedInUser !== formik.values.emP_CODE &&
            !flag && (
              <>
                <button
                  type="reset"
                  onClick={confirmApprove}
                  className="btn btn-light-success me-3"
                  data-kt-users-modal-action="cancel"
                  disabled={formik.isSubmitting || isBankDetailsLoading}
                >
                  Approve
                </button>
                <button
                  type="reset"
                  onClick={confirmDecline}
                  className="btn btn-light-danger me-3"
                  data-kt-users-modal-action="cancel"
                  disabled={formik.isSubmitting || isBankDetailsLoading}
                >
                  Reject
                </button>
              </>
            )}
          <button
            type="reset"
            onClick={closeForm}
            className="btn btn-light me-3"
            data-kt-users-modal-action="cancel"
            disabled={formik.isSubmitting || isBankDetailsLoading}
          >
            Cancel
          </button>

          {formMode !== Mode.VIEW &&
            loggedInUser === formik.values.emP_CODE && (
              <button
                type="submit"
                className="btn btn-primary"
                data-kt-users-modal-action="submit"
                disabled={formik.isSubmitting || isBankDetailsLoading}
                onClick={() => handleSubmit()}
              >
                <span className="indicator-label">Save</span>
                {(formik.isSubmitting || isBankDetailsLoading) && (
                  <span className="indicator-progress">
                    Please wait...
                    <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                  </span>
                )}
              </button>
            )}
        </div>
      </form>
    </>
  );
};

export { EmpBankDetailsTxEditModalForm };
