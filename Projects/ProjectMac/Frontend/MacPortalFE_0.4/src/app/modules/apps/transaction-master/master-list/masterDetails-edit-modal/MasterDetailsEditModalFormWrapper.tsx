import { useQuery } from "react-query";
import { isNotEmpty, Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { useListView } from "../core/ListViewProvider";
import { getMasterDetailsById } from "../core/_requests";
import { CadreEditModalForm } from "../../../cadre/cadre-list/cadre-edit-modal/CadreEditModalForm";
import { useAuth } from "../../../../auth";
import { DepartmentEditModalForm } from "../../../department/department-list/department-edit-modal/DepartmentEditModalForm";
import { GradeEditModalForm } from "../../../grade/grade-list/grade-edit-modal/GradeEditModalForm";
import { QualificationEditModalForm } from "../../../qualification/qualification-list/qualification-edit-modal/QualificationEditModalForm";
import { DesignationEditModalForm } from "../../../designation/designations-list/designation-edit-modal/DesignationEditModalForm";
import { EmpBankDetailsTxEditModalForm } from "./EmpBankDetailsTxEditModalForm";
import { ViewTransactionForm } from "./ViewTransactionForm";

const MasterDetailsEditModalFromWrapper = () => {
  
  const { currentUser } = useAuth();
  const loggedInUser = currentUser?.id?.toString();

  const {
    itemIdForUpdate,
    setItemIdForUpdate,
    tableNameForUpdate,
    setTableNameForUpdate,
    formMode,
  } = useListView();
  const enabledQuery: boolean = isNotEmpty(itemIdForUpdate);
  const {
    isLoading,
    data: bankDetails,
    error,
  } = useQuery(
    `${QUERIES.MASTER_DETAILS_LIST}-masterDetails-${itemIdForUpdate}`,
    () => {
      return getMasterDetailsById(itemIdForUpdate);
    },
    {
      cacheTime: 0,
      enabled: enabledQuery,
      onError: (err) => {
        setItemIdForUpdate(undefined);
        setTableNameForUpdate(undefined);
        console.error(err);
      },
    }
  );

  if (!isLoading && !error && bankDetails && formMode === Mode.VIEW) {
    return (
      <ViewTransactionForm
        isBankDetailsLoading={isLoading}
        masterDetails={bankDetails}
      />
    );
  }

  if (
    !isLoading &&
    !error &&
    bankDetails &&
    formMode === Mode.EDIT &&
    bankDetails.emP_CODE === loggedInUser
  ) {
    if (tableNameForUpdate === "CADRE") {
      return (
        <CadreEditModalForm isCadreLoading={isLoading} cadre={bankDetails} />
      );
    } else if (tableNameForUpdate === "DEPARTMENT") {
      return (
        <DepartmentEditModalForm
          isDepartmentLoading={isLoading}
          department={bankDetails}
        />
      );
    } else if (tableNameForUpdate === "DESIGNATION") {
      return (
        <DesignationEditModalForm
          isDesignationLoading={isLoading}
          designation={bankDetails}
        />
      );
    } else if (tableNameForUpdate === "GRADE") {
      return (
        <GradeEditModalForm isGradeLoading={isLoading} grade={bankDetails} />
      );
    } else if (tableNameForUpdate === "QUALIFICATION") {
      return (
        <QualificationEditModalForm
          isQualificationLoading={isLoading}
          qualification={bankDetails}
        />
      );
    } 
    // else if (tableNameForUpdate === "EMPLOYEE") {
    //   return (
    //     <EmpBankDetailsTxEditModalForm
    //       isBankDetailsLoading={isLoading}
    //       masterDetails={bankDetails}
    //     />
    //   );
    // }
  }

  return null;
};

export { MasterDetailsEditModalFromWrapper };
