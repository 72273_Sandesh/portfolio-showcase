import { KTIcon, Mode } from "../../../../../../_metronic/helpers";
import { setGlobalVariable } from "../../GlobalVariable";
import { useListView } from "../core/ListViewProvider";

const MasterDetailsEditModalHeader = () => {
  const {itemIdForUpdate, setItemIdForUpdate,tableNameForUpdate,formMode } = useListView();

  const openAddModal = () => {
    setItemIdForUpdate(undefined);
    setGlobalVariable("addMasterDetails");
  };
  return (
    <div className="modal-header">
      {/* begin::Modal title */}
      
      <div className="fw-bolder">
        <h2>
          {formMode}{" "}{tableNameForUpdate}{" "} TRANSACTION
          {/* {itemIdForUpdate && formMode===Mode.EDIT && ( */}
            <label className="fw-bold ms-5 border border-gray-300 rounded text-gray-600 p-2 fs-6">
              ID : {itemIdForUpdate}
            </label>
          {/* )} */}
        </h2>
        {/* <div className="fw-bolder"></div> */}
      </div>
      
      {/* end::Modal title */}

      {/* <div className="fw-bolder me-0">
        {itemIdForUpdate && formMode===Mode.EDIT && (<label className="fw-bold border border-gray-300 rounded text-gray-600 p-2 fs-6">  ID : {itemIdForUpdate}</label>)}
      </div> */}
      
      {/* begin::Close */}
      <div
        className="btn btn-icon btn-sm btn-active-icon-primary"
        data-kt-users-modal-action="close"
        onClick={openAddModal}
        style={{ cursor: "pointer" }}
      >
        <KTIcon iconName="cross" className="fs-1" />
      </div>
      {/* end::Close */}
    </div>
  );
};

export { MasterDetailsEditModalHeader };
