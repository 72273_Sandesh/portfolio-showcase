import * as React from "react";
import { styled } from "@mui/material/styles";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import Check from "@mui/icons-material/Check";
import Close from "@mui/icons-material/Close";
import { Typography, Box } from "@mui/material";
import StepConnector, {
  stepConnectorClasses,
} from "@mui/material/StepConnector";
import { StepIconProps } from "@mui/material/StepIcon";
import { useAuth } from "../../../../auth";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

// Custom styled connectors and step icons
const ColorlibConnector = styled(StepConnector)(({ theme }) => ({
  [`&.${stepConnectorClasses.alternativeLabel}`]: {
    top: 40,
  },
  [`&.${stepConnectorClasses.active}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      backgroundImage:
        "linear-gradient(75.7deg, rgb(34, 126, 34) 3.8%, rgb(99, 162, 17) 87.1%)",
    },
  },
  [`&.${stepConnectorClasses.completed}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      backgroundImage:
        "linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)",
    },
  },
  [`& .${stepConnectorClasses.line}`]: {
    height: 3,
    border: 0,
    backgroundColor:
      theme.palette.mode === "dark" ? theme.palette.grey[800] : "#eaeaf0",
    borderRadius: 1,
  },
}));

const getColorGradient = (status: string) => {
  switch (status) {
    case "Approved":
      return "linear-gradient(75.7deg, rgb(34, 126, 34) 3.8%, rgb(99, 162, 17) 87.1%)"; //"#227E22" // Green gradient
    case "Rejected":
      return "radial-gradient(circle at 10% 20%, rgb(221, 49, 49) 0%, rgb(119, 0, 0) 90%)"; // Red gradient
    default:
      return "linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)"; // Default gradient
  }
};

const ColorlibStepIconRoot = styled("div")<{
  ownerState: { completed?: boolean; active?: boolean; status?: string };
}>(({ theme, ownerState }) => ({
  backgroundColor:
    theme.palette.mode === "dark" ? theme.palette.grey[700] : "#ccc",
  zIndex: 1,
  color: "#fff",
  width: 30,
  height: 30,
  display: "flex",
  borderRadius: "50%",
  justifyContent: "center",
  alignItems: "center",
  ...(ownerState.active && {
    backgroundImage: getColorGradient(ownerState.status || "default"),
    boxShadow: "0 4px 10px 0 rgba(0,0,0,.25)",
  }),
  ...(ownerState.completed && {
    backgroundImage: getColorGradient(ownerState.status || "default"),
  }),
}));

const ColorlibStepIcon = (props: StepIconProps & { status?: string }) => {
  const { active, completed, icon, status } = props;
  return (
    <ColorlibStepIconRoot ownerState={{ completed, active, status }}>
      {completed ? <Check /> : icon}
    </ColorlibStepIconRoot>
  );
};

// Mapping for status classes
const getStatusClass = (confirmed: string) => {
  switch (confirmed) {
    case "Y":
      return "Approved";
    case "O":
      return "Pending";
    case "R":
      return "Rejected";
    default:
      return "todo";
  }
};

// Date formatting function
const formatDate = (dateString: string | null) => {
  if (!dateString) return;
  const date = new Date(dateString);
  return (
    <>
      <div>
        {date.toLocaleDateString("en-GB", {
          day: "numeric",
          month: "numeric",
          year: "numeric",
        })}
      </div>
      <div>
        {date.toLocaleTimeString("en-IN", {
          hour: "numeric",
          minute: "numeric",
          second: "numeric",
          timeZone: "Asia/Kolkata",
        })}
      </div>
    </>
  );
};

// Main component
interface LevelData {
  traN_ID: string;
  leveL_NO: number;
  confirmed: string;
  emP_CODE__REVIEW: string | number | undefined;
  shorT_NAME: string;
  chG_DATE: string | null;
  reason: string;
}

interface ProgressMeterProps {
  transactionData: LevelData[];
  flag: any;
}

const ProgressMeter: React.FC<ProgressMeterProps> = ({
  transactionData,
  flag,
}) => {
  const { currentUser } = useAuth();

  return (
    <Box sx={{ width: "100%", paddingTop: "10px" }}>
      <Stepper alternativeLabel connector={<ColorlibConnector />}>
        {transactionData
          .slice()
          .reverse()
          .map((data) => (
            <Step
              key={data.traN_ID}
              active={
                getStatusClass(data.confirmed) === "Approved" ||
                getStatusClass(data.confirmed) === "Rejected"
              }
            >
              {data.reason ? (
                <OverlayTrigger
                  placement="top"
                  delay={{ show: 250, hide: 400 }}
                  overlay={(props) => (
                    <Tooltip id="button-tooltip" {...props}>
                      {data.reason
                        ? data.reason.toUpperCase()
                        : "No reason provided"}
                    </Tooltip>
                  )}
                >
                  <label className="mb-2">View Reason</label>
                </OverlayTrigger>
              ) : (
                <label className="mb-2 text-light">-</label>
              )}
              <StepLabel
                StepIconComponent={(props) => (
                  <ColorlibStepIcon
                    {...props}
                    status={getStatusClass(data.confirmed)}
                  />
                )}
                error={getStatusClass(data.confirmed) === "Rejected"}
                optional={
                  <Typography variant="caption" sx={{ fontSize: "14px" }}>
                    {formatDate(data.chG_DATE)}
                  </Typography>
                }
                icon={
                  getStatusClass(data.confirmed) === "Approved" ? (
                    <Check sx={{ color: "white" }} />
                  ) : getStatusClass(data.confirmed) === "Rejected" ? (
                    <Close sx={{ color: "white" }} />
                  ) : (
                    <Box
                      sx={{
                        width: 10,
                        height: 10,
                        borderRadius: "50%",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        textIndent: 0,
                      }}
                    >
                      <Typography variant="caption" sx={{ fontSize: "14px" }}>
                        {data.leveL_NO}
                      </Typography>
                    </Box>
                  )
                }
              >
                <Typography variant="body2" sx={{ fontSize: "14px" }}>
                  {currentUser?.id === data.emP_CODE__REVIEW
                    ? "You"
                    : data.shorT_NAME}
                  {currentUser?.id === data.emP_CODE__REVIEW &&
                    data.confirmed !== "O" &&
                    flag(true)}
                </Typography>
                <Typography variant="body2" sx={{ fontSize: "14px" }}>
                  {data.leveL_NO === 0
                    ? "Initiated"
                    : getStatusClass(data.confirmed)}
                </Typography>
              </StepLabel>
            </Step>
          ))}
      </Stepper>
    </Box>
  );
};

export { ProgressMeter };
