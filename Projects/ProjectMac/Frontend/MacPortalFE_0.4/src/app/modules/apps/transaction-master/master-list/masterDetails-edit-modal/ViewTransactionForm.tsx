import { FC, useEffect, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { useListView } from "../core/ListViewProvider";
import { useQueryResponse } from "../core/QueryResponseProvider";
import "./TrackingComponent.css";
import { initialMasterDetails, MasterDetails } from "../core/_models";
import { useAuth } from "../../../../auth";
import { KTIcon, Mode, QUERIES } from "../../../../../../_metronic/helpers";
import { ProgressMeter } from "./ProgressMeter";
import Swal from "sweetalert2";
import { useMutation, useQueryClient } from "react-query";
import { approveMasterDetails, declineMasterDetails } from "../core/_requests";
import axios from "axios";

type Props = {
  isBankDetailsLoading: boolean;
  masterDetails: MasterDetails;
};
const editBankDetailsSchema = Yup.object().shape({
  descr: Yup.string().required("Description of Payment is required"),
  sH_DESCR: Yup.string().required("Short Description is required"),
  banD_CODE: Yup.string().required("Cadre Category is required"),
  status: Yup.string().required("Status is required"),
});

const ViewTransactionForm: FC<Props> = ({
  masterDetails,
  isBankDetailsLoading,
}) => {
  const { itemIdForUpdate, setItemIdForUpdate, formMode, tableNameForUpdate } =
    useListView();
  const { refetch } = useQueryResponse();
  const { currentUser } = useAuth();
  const { query } = useQueryResponse();
  const queryClient = useQueryClient();
  const loggedInUser = currentUser?.id?.toString() || "";
  const [privateIP, setPrivateIP] = useState("TERM");
  const [flag, setFlag] = useState<boolean>(false);

  const closeForm = () => {
    setItemIdForUpdate(undefined);
  };

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const approveItem = useMutation(
    (remarks: string) =>
      approveMasterDetails(
        itemIdForUpdate,
        tableNameForUpdate,
        formik.values.tX_CHG_USER,
        remarks,
        formik.values.wF_PROCESS_LEVEL?.toString(),
        loggedInUser,
        privateIP
      ),
    {
      onSuccess: (response: any) => {
        const { responseStatus, message } = response.data;
        console.log(response.data);
        if (responseStatus === "S") {
          Swal.fire({
            title: "Success!",
            text: "Transaction approved successfully.",
            icon: "success",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
          setItemIdForUpdate(undefined);
        } else if (responseStatus === "E") {
          Swal.fire({
            title: "Error!",
            text: `Error approving item: ${message.join(", ")}`,
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        } else {
          Swal.fire({
            title: "Error!",
            text: "Unknown response status.",
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        }
      },
      onError: (error: Error) => {
        Swal.fire({
          title: "Error!",
          text: `Error approving item: ${error.message || "Unknown error"}`,
          icon: "error",
          confirmButtonText: "OK",
        });
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);
      },
    }
  );

  const declineItem = useMutation(
    (transactionDetails: object) => declineMasterDetails(transactionDetails),
    {
      onSuccess: (response: any) => {
        const { responseStatus, message } = response.data;
        console.log(response.data);
        if (responseStatus === "S") {
          Swal.fire({
            title: "Success!",
            text: "Transaction Rejected successfully.",
            icon: "success",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
          setItemIdForUpdate(undefined);
        } else if (responseStatus === "E") {
          Swal.fire({
            title: "Error!",
            text: `Error While Rejecting Transaction: ${message.join(", ")}`,
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        } else {
          Swal.fire({
            title: "Error!",
            text: "Unknown response status.",
            icon: "error",
            confirmButtonText: "OK",
          });
          queryClient.invalidateQueries([
            `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
          ]);
        }
      },
      onError: (error: Error) => {
        Swal.fire({
          title: "Error!",
          text: `Error While Rejecting Transaction: ${
            error.message || "Unknown error"
          }`,
          icon: "error",
          confirmButtonText: "OK",
        });
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);
      },
    }
  );

  const confirmApprove = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "Do you want to approve this Transaction?",
      icon: "warning",
      input: "text", // Adding input field for remarks
      inputPlaceholder: "Enter remarks...",
      inputAttributes: {
        "aria-label": "Type your remarks here",
      },
      showCancelButton: true,
      confirmButtonText: "Yes, approve it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
      preConfirm: (remarksApprove) => {
        if (!remarksApprove) {
          Swal.showValidationMessage("Remarks are required.");
        } else {
          return remarksApprove;
        }
      },
    }).then((result) => {
      if (result.isConfirmed) {
        approveItem.mutate(result.value); // Pass the remarks to the mutation
      }
    });
  };

  const confirmDecline = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "Do you want to reject this transaction?",
      icon: "warning",
      input: "text",
      inputPlaceholder: "Enter remarks...",
      inputAttributes: {
        "aria-label": "Type your remarks here",
      },
      showCancelButton: true,
      confirmButtonText: "Yes, reject it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
      preConfirm: (remarksDecline) => {
        if (!remarksDecline) {
          Swal.showValidationMessage("Remarks are required.");
        } else {
          return remarksDecline;
        }
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const transactionDetails = {
          transactionId: masterDetails.id,
          empCode: loggedInUser,
          remoteAddr: privateIP,
          remarks: result.value,
        };
        declineItem.mutate(transactionDetails);
      }
    });
  };

  const [bankDetailsForEdit] = useState<MasterDetails>({
    ...masterDetails,
    id: masterDetails.id || initialMasterDetails.id,
    fieldNames: masterDetails.fieldNames || initialMasterDetails.fieldNames,
    reviewers: masterDetails.reviewers || initialMasterDetails.reviewers,
    filE_PATH: masterDetails.filE_PATH || initialMasterDetails.filE_PATH,
    tX_CHG_DATE: masterDetails.tX_CHG_DATE || initialMasterDetails.tX_CHG_DATE,
    tX_CHG_USER: masterDetails.tX_CHG_USER || initialMasterDetails.tX_CHG_USER,
    tX_CHG_TERM: masterDetails.tX_CHG_TERM || initialMasterDetails.tX_CHG_TERM,
    traN_DATE: masterDetails.traN_DATE || initialMasterDetails.traN_DATE,
    remarks: masterDetails.remarks || initialMasterDetails.remarks,
  });

  const cancel = (withRefresh?: boolean) => {
    if (withRefresh) {
      refetch();
    }
    setItemIdForUpdate(undefined);
  };

  const formik = useFormik({
    initialValues: bankDetailsForEdit,
    validationSchema: editBankDetailsSchema,
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);
    },
  });
  const formattedDate = (date: Date | string | undefined) => {
    return new Date(date).toLocaleString("en-GB", {
      day: "2-digit",
      month: "2-digit",
      year: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    });
  };

  return (
    <>
      <form
        id="kt_modal_add_user_form"
        className="form"
        onSubmit={formik.handleSubmit}
        noValidate
      >
        <div className="card-body">
          {/* <div className="bg-light-warning border border-dashed border-gray-300 rounded p-5">
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="trId"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  TRANSACTION ID
                </label>
                <label className="form-control form-control-lg form-control-solid  ">
                  {formik.values.id}
                </label>
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="trDate"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  TRANSACTION DATE
                </label>
                <label className="form-control form-control-lg form-control-solid  ">
                  {formattedDate(formik.values.traN_DATE)}
                </label>
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="sitE_NAME"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE USER
                </label>
                <label className="form-control form-control-lg form-control-solid  ">
                  {formik.values.tX_CHG_USER}
                </label>
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="depT_NAME"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE DATE
                </label>
                <label className="form-control form-control-lg form-control-solid  ">
                  {formattedDate(formik.values.tX_CHG_DATE)}
                </label>
              </div>
            </div>

            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="designation"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  CHANGE TERM
                </label>
                <label className="form-control form-control-lg form-control-solid  ">
                  {formik.values.tX_CHG_TERM}
                </label>
              </div>

              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="datE_JOIN"
                  className="col-form-label fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  REMARK
                </label>
                <label className="form-control form-control-lg form-control-solid  ">
                  {formik.values.remarks}
                </label>
              </div>
            </div>
          </div> */}

          <div className="border border-dashed border-gray-300 rounded p-5">
            <>
              <div className="text-center">
                <ProgressMeter
                  transactionData={formik.values.reviewers}
                  flag={setFlag}
                />
              </div>
            </>
          </div>

          <div className="p-5">
            <>
              <div className="row mb-1">
                <div className="col-lg-4">
                  <label className="form-control border-0 text-center fs-6">
                    FIELD NAME
                  </label>
                </div>
                <div className="col-lg-4">
                  <label className="form-control border-0 text-center fs-6">
                    PREVIOUS VALUE
                  </label>
                </div>
                <div className="col-lg-4">
                  <label className="form-control border-0 text-center fs-6">
                    CHANGED VALUE
                  </label>
                </div>
              </div>

              <div className="row mb-1">
                {Object.keys(formik.values.fieldNames).map((key) => {
                  const field = formik.values.fieldNames[key];
                  return (
                    <>
                      {field.neW_VALUE && (
                        <div className="row mb-1" key={key}>
                          <div className="col-lg-4">
                            <div className="d-flex align-items-center mb-1">
                              <label className="form-control border-0 text-start fs-6">
                                {field.fielD_DESCRIPTION}:
                              </label>
                            </div>
                          </div>

                          <div className="col-lg-4">
                            <div className="d-flex align-items-center mb-1">
                              <input
                                type="text"
                                className="form-control form-control-lg form-control-solid bg-light-danger"
                                value={field.olD_VALUE || ""}
                                readOnly
                              />
                            </div>
                          </div>

                          {/* <div className="col-lg-4">
                            <div className="d-flex align-items-center mb-1">
                              <label className="form-control form-control-lg form-control-solid bg-light-danger">
                                {field.olD_VALUE}
                              </label>
                            </div>
                          </div> */}

                          {/* <div className="col-lg-4">
                            <div className="d-flex align-items-center mb-1">
                              <label className="form-control form-control-lg form-control-solid bg-light-danger d-flex justify-content-between align-items-center">
                                <span>{field.neW_VALUE}</span>
                                
                                {field.filE_PATH && field.filE_PATH.trim() !== "" &&(
                                <div
                                  onClick={() => window.open(field.field_PATH, "_blank")}
                                  //onClick={() => window.open(field.filE_PATH, "_blank")}
                                  
                                  style={{ cursor: "pointer" }}
                                >
                                  <KTIcon
                                    iconName={"paper-clip"}
                                    className="fs-1 text-gray-500"
                                  />
                                </div>
                                )}
                              </label>
                            </div>
                          </div> */}

                          <div className="col-lg-4">
                            <div className="d-flex align-items-center mb-1">
                              <div className="input-group input-group-solid">
                                <input
                                  type="text"
                                  className="form-control form-control-lg form-control-solid bg-light-danger"
                                  value={field.neW_VALUE || ""}
                                  readOnly
                                />
                                {field.filE_PATH &&
                                  field.filE_PATH.trim() !== "" && (
                                    <div
                                      className="input-group-text"
                                      // onClick={() =>
                                      //   window.open(field.filE_PATH, "_blank")
                                      // }
                                      onClick={() =>
                                        window.open(field.field_PATH, "_blank")
                                      }
                                      style={{
                                        cursor: "pointer",
                                      }}
                                    >
                                      <KTIcon
                                        iconName={"paper-clip"}
                                        className="fs-1 text-gray-500"
                                      />
                                    </div>
                                  )}
                              </div>
                            </div>
                          </div>
                        </div>
                      )}
                    </>
                  );
                })}
              </div>
            </>
          </div>
        </div>

        <div className="card-footer d-flex justify-content-center py-6 px-9">
          {formik.values.confirmed === "N" &&
            formMode === Mode.VIEW &&
            loggedInUser !== formik.values.emP_CODE &&
            !flag && (
              <>
                <button
                  type="reset"
                  onClick={confirmApprove}
                  className="btn btn-light-success me-3"
                  data-kt-users-modal-action="cancel"
                  disabled={formik.isSubmitting || isBankDetailsLoading}
                >
                  Approve
                </button>
                <button
                  type="reset"
                  onClick={confirmDecline}
                  className="btn btn-light-danger me-3"
                  data-kt-users-modal-action="cancel"
                  disabled={formik.isSubmitting || isBankDetailsLoading}
                >
                  Reject
                </button>
              </>
            )}
          <button
            type="reset"
            onClick={closeForm}
            className="btn btn-light me-3"
            data-kt-users-modal-action="cancel"
            disabled={formik.isSubmitting || isBankDetailsLoading}
          >
            Cancel
          </button>
        </div>
      </form>
    </>
  );
};

export { ViewTransactionForm };

//code tp be put inside the  {field.neW_VALUE && (  for pin clip and attachments here
{
  /* <div className="row mb-1" key={key}>
<div className="col-lg-4">
  <div className="d-flex align-items-center mb-1">
    <label className="form-control border-0 text-start fs-6">
      {field.fielD_DESCRIPTION}:
    </label>
  </div>
</div>

<div className="col-lg-4">
  <div className="d-flex align-items-center mb-1 position-relative">
    <label className="form-control form-control-lg form-control-solid bg-light-danger d-flex justify-content-between align-items-center">
      <span>{field.olD_VALUE}</span>
      <div
        onClick={() =>
          window.open(field.field_PATH, "_blank")
        }
        style={{ cursor: "pointer" }}
      >
        <KTIcon
          iconName={"paper-clip"}
          className="fs-1 text-gray-500"
        />
      </div>
    </label>
  </div>
</div>

<div className="col-lg-4">
  <div className="d-flex align-items-center mb-1">
    <label className="form-control form-control-lg form-control-solid bg-light d-flex justify-content-between align-items-center">
      {field.neW_VALUE}
      <KTIcon
        iconName={"paper-clip"}
        className="fs-1 text-gray-500"
      />
    </label>
  </div>
</div>
</div> */
}
