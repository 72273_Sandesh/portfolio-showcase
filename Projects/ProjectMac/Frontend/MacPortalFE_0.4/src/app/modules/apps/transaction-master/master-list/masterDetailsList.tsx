import { KTCard, Mode } from "../../../../../_metronic/helpers";
import { Content } from "../../../../../_metronic/layout/components/content";
import { ToolbarWrapper } from "../../../../../_metronic/layout/components/toolbar";
import { Loader } from "../../../../../_metronic/utilities/Loader";
import { MasterDetailsListHeader } from "./components/header/MasterDetailsListHeader";
import { ListViewProvider, useListView } from "./core/ListViewProvider";
import { QueryRequestProvider } from "./core/QueryRequestProvider";
import { QueryResponseProvider, useQueryResponseLoading } from "./core/QueryResponseProvider";
import { MasterDetailsEditModal } from "./masterDetails-edit-modal/MasterDetailsEditModal";
import { MasterDetailsTable } from "./table/MasterDetailsTable";

const MasterDetailsList = () => {
  const { itemIdForUpdate, tableNameForUpdate, formMode } = useListView();
  const isLoading = useQueryResponseLoading()

  return (
    <>
      <KTCard>
        <div
          style={{
            display:
              itemIdForUpdate !== undefined &&
              formMode === Mode.EDIT &&
              (tableNameForUpdate === "EMPLOYEE_PAYROLL"
              ||
              tableNameForUpdate === "EMPLOYEE_BASIC"
              ||
               tableNameForUpdate === "EMPLOYEE_PRESENT"
              ||
               tableNameForUpdate === "EMPLOYEE_PERMANENT"
              ||
              tableNameForUpdate === "EMPLOYEE_EMERGENCY")
                ? "none"
                : "",
          }}
        >
          <MasterDetailsListHeader />
        </div>
        <MasterDetailsTable />
      </KTCard>
      {itemIdForUpdate !== undefined && formMode === Mode.VIEW && (
        <MasterDetailsEditModal />
      )}
      {itemIdForUpdate !== undefined &&
        formMode === Mode.EDIT &&
        tableNameForUpdate !== "EMPLOYEE_PAYROLL" && tableNameForUpdate !== "EMPLOYEE_BASIC" && tableNameForUpdate !== "EMPLOYEE_PRESENT" &&
        tableNameForUpdate !== "EMPLOYEE_PERMANENT" &&  tableNameForUpdate !== "EMPLOYEE_EMERGENCY"       && <MasterDetailsEditModal />}
      
        {isLoading && <Loader />}
        
        
    </>
  );
};

const MasterDetailsListWrapper = () => (
  <QueryRequestProvider>
    <QueryResponseProvider>
      <ListViewProvider>
        <ToolbarWrapper />
        <Content>
          <MasterDetailsList />
        </Content>
      </ListViewProvider>
    </QueryResponseProvider>
  </QueryRequestProvider>
);

export { MasterDetailsListWrapper };
