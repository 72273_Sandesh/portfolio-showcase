import { useMemo } from "react";
import { useTable, ColumnInstance, Row } from "react-table";
import { CustomHeaderColumn } from "./columns/CustomHeaderColumn";
import {
  useQueryResponseData,
  useQueryResponseLoading,
} from "../core/QueryResponseProvider";
import { MasterDetails } from "../core/_models";
import { KTCardBody, Mode } from "../../../../../../_metronic/helpers";
import { MasterDetailsListPagination } from "../components/pagination/MasterDetailsListPagination";
import { MasterDetailsListLoading } from "../components/loading/MasterDetailsListLoading";
import { MasterDetailsColumns } from "./columns/_columns";
import { CustomRow } from "./columns/CustomRow";
import { useListView } from "../core/ListViewProvider";
import BasicDetailsEdit from "../../../../profile/components/BasicDetailsEdit";
import WorkPayrollDetailsEdit from "../../../../profile/components/WorkPayrollDetailsEdit";
import PresentDetailsEdit from "../../../../profile/components/PresentDetailsEdit";
import PermanentDetailsEdit from "../../../../profile/components/PermanentDetailsEdit";
import EmergencyDetailsEdit from "../../../../profile/components/EmergencyDetailsEdit";

const MasterDetailsTable = () => {
  const bankDetails = useQueryResponseData();
  const isLoading = useQueryResponseLoading();
  const data = useMemo(() => bankDetails, [bankDetails]);
  const columns = useMemo(() => MasterDetailsColumns, []);
  const { getTableProps, getTableBodyProps, headers, rows, prepareRow } =
    useTable({
      columns,
      data,
    });

  const { itemIdForUpdate, formMode, tableNameForUpdate } = useListView();

  return (
    <>
      {itemIdForUpdate !== undefined && formMode === Mode.EDIT  && tableNameForUpdate==="EMPLOYEE_PAYROLL" ? (
        <WorkPayrollDetailsEdit />
      ) : 
      
       itemIdForUpdate !== undefined && formMode === Mode.EDIT  && tableNameForUpdate==="EMPLOYEE_BASIC" ? (
        <BasicDetailsEdit />
      ) :
      
      itemIdForUpdate !== undefined && formMode === Mode.EDIT  && tableNameForUpdate==="EMPLOYEE_PRESENT" ? (
        <PresentDetailsEdit />
      ) :

      itemIdForUpdate !== undefined && formMode === Mode.EDIT  && tableNameForUpdate==="EMPLOYEE_PERMANENT" ? (
        <PermanentDetailsEdit />
      ) :

      itemIdForUpdate !== undefined && formMode === Mode.EDIT  && tableNameForUpdate==="EMPLOYEE_EMERGENCY" ? (
        <EmergencyDetailsEdit />
      ) :
      
      
      (
        <KTCardBody className="py-4">
          <div className="table-responsive">
            <table
              id="kt_table_users"
              className="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
              {...getTableProps()}
            >
              <thead>
                <tr className="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
                  {headers.map((column: ColumnInstance<MasterDetails>) => (
                    <CustomHeaderColumn key={column.id} column={column} />
                  ))}
                </tr>
              </thead>
              <tbody className="text-gray-600 fw-bold" {...getTableBodyProps()}>
                {rows.length > 0 ? (
                  rows.map((row: Row<MasterDetails>, i) => {
                    prepareRow(row);
                    return <CustomRow row={row} key={`row-${i}-${row.id}`} />;
                  })
                ) : (
                  <tr>
                    <td colSpan={7}>
                      <div className="d-flex text-center w-100 align-content-center justify-content-center">
                        No matching records found
                      </div>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
          <MasterDetailsListPagination />
          {/* {isLoading && <MasterDetailsListLoading />} */}
        </KTCardBody>
      )}
    </>
  );
};

export { MasterDetailsTable };
