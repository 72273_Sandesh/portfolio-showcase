import { FC, useEffect } from "react";
import { MenuComponent } from "../../../../../../../_metronic/assets/ts/components";
import { ID, KTIcon, Mode } from "../../../../../../../_metronic/helpers";
import { useListView } from "../../core/ListViewProvider";
import { useQueryResponse } from "../../core/QueryResponseProvider";
import { useAuth } from "../../../../../auth";

type Props = {
  id: ID;
  tableName: string | undefined;
  emP_CODE?: string | undefined;
  remarks?: string;
  wF_PROCESS_LEVEL?: number;
  confirmed?: string | undefined;
};

const MasterDetailsActionsCell: FC<Props> = ({
  id,
  tableName,
  emP_CODE,
  wF_PROCESS_LEVEL,
  confirmed,
}) => {
  const { setItemIdForUpdate, setTableNameForUpdate, setFormMode } =
    useListView();
  const { query } = useQueryResponse();

  const { currentUser } = useAuth();
  const loggedInUser = currentUser?.id?.toString() || "";

  useEffect(() => {
    MenuComponent.reinitialization();
  }, []);

  const openEditModal = () => {
    setItemIdForUpdate(id);
    setTableNameForUpdate(tableName);
    setFormMode(Mode.EDIT);
  };

  const openInfoModal = () => {
    setItemIdForUpdate(id);
    setTableNameForUpdate(tableName);
    setFormMode(Mode.VIEW);
  };

  return (
    <>
      <a
        href="#"
        className="btn btn-light btn-active-light-primary btn-sm"
        data-kt-menu-trigger="click"
        data-kt-menu-placement="bottom-end"
      >
        Actions
        <KTIcon iconName="down" className="fs-5 m-0" />
      </a>
      {/* begin::Menu */}
      <div
        className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-20 w-125px py-4"
        data-kt-menu="true"
      >
        {/* begin::Menu item */}
        <div className="menu-item px-3">
          <a className="menu-link px-3" onClick={openInfoModal}>
            View
          </a>
        </div>
        {/* end::Menu item */}

        {/* begin::Menu item */}
        {emP_CODE === loggedInUser &&
          wF_PROCESS_LEVEL === 1 &&
          confirmed === "N" &&
          !query.includes("txMode") && (
            <div className="menu-item px-3">
              <a className="menu-link px-3" onClick={openEditModal}>
                Edit
              </a>
            </div>
          )}
        {/* end::Menu item */}
      </div>
      {/* end::Menu */}
    </>
  );
};

export { MasterDetailsActionsCell };
