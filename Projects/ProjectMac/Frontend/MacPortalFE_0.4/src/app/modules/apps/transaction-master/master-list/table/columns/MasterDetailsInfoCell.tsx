import { FC, useEffect } from "react";
import { MasterDetails } from "../../core/_models";
import { useListView } from "../../core/ListViewProvider";
import { MenuComponent } from "../../../../../../../_metronic/assets/ts/components";
import { setGlobalVariable } from "../../../GlobalVariable";
import { ID, Mode } from "../../../../../../../_metronic/helpers";

type Props = {
  bankDetails: MasterDetails;
  id: ID;
};

const MasterDetailsInfoCell: FC<Props> = ({ bankDetails, id }) => {
  const { setItemIdForUpdate, setTableNameForUpdate, setFormMode } = useListView();

  useEffect(() => {
    MenuComponent.reinitialization();
  }, []);

  const openInfoModal = () => {
    setItemIdForUpdate(id);
    setTableNameForUpdate(bankDetails.tablE_NAME);
    setFormMode(Mode.VIEW);

  };
  return (
    <div className="d-flex align-items-center ">
      <div className="d-flex flex-column">
        <a
          className="text-gray-800 text-hover-primary mb-1"
          onClick={openInfoModal}
        >
          {bankDetails.tablE_NAME?.toUpperCase()}{" "}
        </a>
        <span>
          <div className=" fw-bolder d-flex align-items-right">
            <div className="badge badge-light fw-bolder d-flex align-items-right">
            {bankDetails.id}
            </div>
          </div>
        </span>
      </div>
    </div>
  );
};
export { MasterDetailsInfoCell };
