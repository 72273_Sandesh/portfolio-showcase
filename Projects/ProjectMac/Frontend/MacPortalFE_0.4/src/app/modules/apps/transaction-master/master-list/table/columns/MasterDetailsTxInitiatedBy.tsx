import React from "react";
type Props = {
  initiateD_AS: string | undefined;
};

const MasterDetailsTxInitiatedBy: React.FC<Props> = ({ initiateD_AS }) => {
  return (
    <div className=" fw-bolder d-flex align-items-right">
      {initiateD_AS?.toUpperCase()}
    </div>
  );
};

export { MasterDetailsTxInitiatedBy };
