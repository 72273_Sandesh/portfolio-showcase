import React from "react";
type Props = {
  approveR_NAME: string | undefined;
};

const MasterDetailsTxPendingWith: React.FC<Props> = ({ approveR_NAME }) => {
  return (
    <div className=" fw-bolder d-flex align-items-right">
      {approveR_NAME?.toUpperCase()}
    </div>
  );
};

export { MasterDetailsTxPendingWith };
