import { Column } from "react-table";
import { MasterDetails } from "../../core/_models";
import { MasterDetailsCustomHeader } from "./MasterDetailsCustomHeader";
import { MasterDetailsInfoCell } from "./MasterDetailsInfoCell";
import { MasterDetailsActionsCell } from "./MasterDetailsActionsCell";
import { MasterDetailsTxInitiatedBy } from "./MasterDetailsTxInitiatedBy";
import { MasterDetailsTxDescription } from "./MasterDetailsTxDescription";
import { MasterDetailsLevel } from "./MasterDetailsLevel";
import { MasterDetailsTxDate } from "./MasterDetailsTxDate";
import { MasterDetailsTxPendingWith } from "./MasterDetailsTxPendingWith";
const MasterDetailsColumns: ReadonlyArray<Column<MasterDetails>> = [
  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="MASTER NAME"
        className="min-w-125px text-left "
      />
    ),
    id: "name",
    Cell: ({ ...props }) => (
      <MasterDetailsInfoCell
        bankDetails={props.data[props.row.index]}
        id={props.data[props.row.index].id}
      />
    ),
  },
  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="Master description"
        className="min-w-125px text-left"
      />
    ),
    accessor: "neW_VALUE",
    Cell: ({ ...props }) => (
      <MasterDetailsTxDescription
        neW_VALUE={props.data[props.row.index].neW_VALUE}
      />
    ),
  },

  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="Initiated By"
        className="min-w-125px text-left"
      />
    ),
    id: "initiateD_AS",
    Cell: ({ ...props }) => (
      <MasterDetailsTxInitiatedBy initiateD_AS={props.data[props.row.index].initiateD_AS}
      />
    ),
  },

  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="LEVEL"
        className="min-w-125px text-left"
      />
    ),
    id: "wF_PROCESS_LEVEL",
    Cell: ({ ...props }) => (
      <MasterDetailsLevel
        wF_PROCESS_LEVEL={props.data[props.row.index].wF_PROCESS_LEVEL}
      />
    ),
  },

  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="Status"
        className="min-w-125px text-left"
      />
    ),
    accessor: "confirmed",
    Cell: ({ value }) => (
      <span>
        <div className=" fw-bolder d-flex align-items-right">
          <div className="d-flex align-items-right">
            {
              <div className="d-flex align-items-right">
                {value === "Y" ? (
                  <div className="badge badge-light-success fw-bolder d-flex align-items-right">
                    Approved
                  </div>
                ) : value === "N" ? (
                  <div className="badge badge-light-warning fw-bolder d-flex align-items-right">
                    Pending
                  </div>
                ) : (
                  <div className="badge badge-light-danger fw-bolder d-flex align-items-right">
                    Rejected
                  </div>
                )}
              </div>
            }
          </div>
        </div>
      </span>
    ),
  },

  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="Level Person"
        className="min-w-125px text-left"
      />
    ),
    id: "approveR_NAME",
    Cell: ({ ...props }) => (
      <MasterDetailsTxPendingWith approveR_NAME={props.data[props.row.index].approveR_NAME}
      />
    ),
  },

  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="TRANSACTION DATE"
        className="min-w-125px text-left"
      />
    ),
    id: "traN_DATE",
    Cell: ({ ...props }) => (
      <MasterDetailsTxDate
        traN_DATE={props.data[props.row.index].traN_DATE ?? new Date()}
      />
    ),
  },
  {
    Header: (props) => (
      <MasterDetailsCustomHeader
        tableProps={props}
        title="Actions"
        className="min-w-100px text-left"
      />
    ),
    id: "actions",
    Cell: ({ ...props }) => (
      <MasterDetailsActionsCell
        id={props.data[props.row.index].id}
        tableName={props.data[props.row.index].tablE_NAME}
        emP_CODE={props.data[props.row.index].emP_CODE}
        remarks={props.data[props.row.index].remarks}
        wF_PROCESS_LEVEL={props.data[props.row.index].wF_PROCESS_LEVEL}
        confirmed={props.data[props.row.index].confirmed}
      />
    ),
  },
];

export { MasterDetailsColumns };
