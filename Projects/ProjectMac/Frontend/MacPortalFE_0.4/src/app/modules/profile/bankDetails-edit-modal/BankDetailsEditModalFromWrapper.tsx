import {useQuery} from 'react-query'
import {BankDetailsEditModalForm} from './BankDetailsEditModalForm'
import { QUERIES, isNotEmpty } from '../../../../_metronic/helpers'
import { getBankDetailsById } from '../../apps/bank-details/bankDetails-list/core/_requests'
import { employeeIdForView } from '../../GlobalQuery'

interface props{
  close:any;
}

const BankDetailsEditModalFromWrapper : React.FC<props> = ({close}) => {
  const enabledQuery: boolean = isNotEmpty(employeeIdForView)
  const {
    isLoading,
    data: bankDetails,
    error,
  } = useQuery(
    `${QUERIES.BANK_DETAILS_LIST}-bankDetails-${employeeIdForView}`,
    () => {
      return getBankDetailsById(employeeIdForView)
    },
    {
      cacheTime: 0,
      enabled: enabledQuery,
      onError: (err) => {
        // setItemIdForUpdate(undefined)
        console.error(err)
      },
    }
  )

  if (!employeeIdForView) {
    return <BankDetailsEditModalForm isBankDetailsLoading={isLoading} bankDetails={{id: undefined}} onClose={close}/>
  }

  if (!isLoading && !error && bankDetails) {
    return <BankDetailsEditModalForm isBankDetailsLoading={isLoading} bankDetails={bankDetails} onClose={close}/>
  }

  return null
}

export {BankDetailsEditModalFromWrapper}
