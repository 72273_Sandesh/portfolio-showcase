import { KTIcon } from "../../../../_metronic/helpers";

interface props {
  close: any;
}

const BankDetailsEditModalHeader: React.FC<props> = ({ close }) => {
  return (
    <div className="modal-header">
      {/* begin::Modal title */}
      <h2 className="fw-bolder">Change Bank Details</h2>
      {/* end::Modal title */}

      {/* begin::Close */}
      <div
        className="btn btn-icon btn-sm btn-active-icon-primary"
        data-kt-users-modal-action="close"
        onClick={close}
        style={{ cursor: "pointer" }}
      >
        <KTIcon iconName="cross" className="fs-1" />
      </div>
      {/* end::Close */}
    </div>
  );
};

export { BankDetailsEditModalHeader };
