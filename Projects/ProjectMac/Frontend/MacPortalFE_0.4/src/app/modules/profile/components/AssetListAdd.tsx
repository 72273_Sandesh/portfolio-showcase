import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
interface AssetDropdownItem {
  inducT_CODE: string;
  descr: string;
}
const AssetListAdd: React.FC = () => {
  const [employee, setEmployee] = useState({});
  const navigate = useNavigate();
  const { currentUser } = useAuth();
  const [drpdwnAssets, setDrpdwnAssets] = useState<AssetDropdownItem[]>([]);

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/ddl_GetAsstes`)
      .then((response) => {
        setDrpdwnAssets(response.data.assets);
      })
      .catch((error) => {
        console.error("Error fetching asset details:", error);
      });
  }, []);

  const validationSchema = Yup.object().shape({
    inducT_CODE: Yup.string().required("Induction required"),
    datE_FORMALITY: Yup.string().required("Date of Formality is required"),
    remarks: Yup.string().required("Remarks is required"),
    status: Yup.string().required("Status is required"),
    statuS_DATE: Yup.string().required("Date of Status is required"),
    toT_AMT: Yup.string().required("Total Amount is required"),
    adJ_AMT: Yup.string().required("Adjust Amount is required"),
  });

  const formik = useFormik({
    initialValues: {
      induction: "",
      datE_FORMALITY: "",
      remarks: "",
      status: "",
      statuS_DATE: "",
      toT_AMT: "",
      adJ_AMT: "",
      emP_CODE: "",
      inducT_CODE: "",
      chG_DATE: "",
      chG_USER: "",
      chG_TERM: "",
      traN_ID: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values, formik) => {
      console.log("Submitting form data:", values);
      axios
        .post( `${EMPLOYEE_URL}/AddAssets`, {
          ...values,
          emP_CODE: employeeIdForView?.toString() || "",
          chG_DATE: new Date(),
          chG_USER: currentUser?.id,
          chG_TERM: "TERM",
          traN_ID: "0000000002",
        })
        .then((response) => {
          const { responseStatus, message } = response.data.data;
          console.log("Post successful!", response.data);
          if (responseStatus === "E") {
            toast.error(                  
              <div>
                <h4 className="alert-heading">Failed to submit Form...</h4>
                <p className="alert alert-danger" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );
          } else {
            toast.success(
              <div>
                <h4 className="alert-heading">
                  Successfully Submitted Form...
                </h4>
                <p className="alert alert-success" role="alert">
                  <h6>
                    {Array.isArray(message) ? message.join("\n") : message}
                  </h6>
                </p>
              </div>
            );

            // Clear employee data and reset form
            setEmployee({
              induction: "",
              datE_FORMALITY: "",
              remarks: "",
              status: "",
              statuS_DATE: "",
              toT_AMT: "",
              adJ_AMT: "",
            });
            formik.resetForm();
            navigate(-1); 
          }
        })
        .catch((error) => {
          console.error("Error posting data:", error);
          toast.error(
            <div>
              <h6 className="alert-heading">Error Posting Data</h6>
              <p className="alert alert-danger" role="alert">
                <h6>{error.message}</h6>
              </p>
            </div>
          );
        });
    },
  });

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Add New Asset</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="inducT_CODE"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  ASSET NAME :
                </label>
                <select
                  name="inducT_CODE"
                  className="form-select form-select-lg form-select-solid"
                  value={formik.values?.inducT_CODE}
                  onChange={formik.handleChange}
                >
                  <option value=""> SELECT ASSET</option>
                  {drpdwnAssets.map((asset) => (
                    <option key={asset.inducT_CODE} value={asset.inducT_CODE}>
                    {asset.descr}
                    </option>
                  ))}
                </select>
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  STATUS :
                </label>
                <select
                  className="form-select form-select-lg form-select-solid"
                  name="status"
                  value={formik.values.status}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value="">Select form below</option>
                  <option value="C">COMPLETED</option>
                  <option value="P">PENDING</option>
                </select>
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.inducT_CODE && formik.errors.inducT_CODE ? (
                  <div className="text-danger">{formik.errors.inducT_CODE}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.status && formik.errors.status ? (
                  <div className="text-danger">{formik.errors.status}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="datE_FORMALITY"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  EFFECTION DATE :
                </label>
                <input
                  type="date"
                  className="form-control form-control-lg form-control-solid"
                  name="datE_FORMALITY"
                  placeholder="Select date of Effection"
                  value={
                    formik.values?.datE_FORMALITY
                      ? new Date(formik.values?.datE_FORMALITY)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  CONFIRMATION DATE:
                </label>
                <input
                  type="date"
                  className="form-control form-control-lg form-control-solid"
                  name="statuS_DATE"
                  placeholder="Enter date of confirmartion"
                  value={
                    formik.values?.statuS_DATE
                      ? new Date(formik.values?.statuS_DATE)
                          .toISOString()
                          .split("T")[0]
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.datE_FORMALITY &&
                formik.errors.datE_FORMALITY ? (
                  <div className="text-danger">
                    {formik.errors.datE_FORMALITY}
                  </div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.statuS_DATE && formik.errors.statuS_DATE ? (
                  <div className="text-danger">{formik.errors.statuS_DATE}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  htmlFor="adJ_AMT"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  ADJUST AMOUNT:
                </label>
                <input
                  type="number"
                  className="form-control form-control-lg form-control-solid"
                  name="adJ_AMT"
                  placeholder="Enter adjust amount"
                  value={formik.values?.adJ_AMT}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  min={0}
                />
              </div>
              <div className="col-lg-6 d-flex align-items-center mb-1">
                <label
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  {" "}
                  TOTAL AMOUNT:
                </label>
                <input
                  type="number"
                  className="form-control form-control-lg form-control-solid"
                  name="toT_AMT"
                  placeholder="Enter total amount"
                  value={formik.values?.toT_AMT}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  min={0}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.adJ_AMT && formik.errors.adJ_AMT ? (
                  <div className="text-danger">{formik.errors.adJ_AMT}</div>
                ) : null}
              </div>
              <div className="col-lg-6">
                {formik.touched.toT_AMT && formik.errors.toT_AMT ? (
                  <div className="text-danger">{formik.errors.toT_AMT}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}

            {/* Begin: Edit Row */}
            <div className="row mb-1">
              <div className=" d-flex align-items-center mb-1">
                <label
                  htmlFor="remarks"
                  className="col-form-label required fw-bold fs-6 me-2"
                  style={{ minWidth: "150px" }}
                >
                  REMARK:
                </label>
                <textarea
                  className="form-control form-control-lg form-control-solid"
                  rows={3}
                  name="remarks"
                  placeholder="Enter here remark about asset"
                  value={formik.values?.remarks}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>
            <div className="row mb-1">
              <div className="col-lg-6">
                {formik.touched.remarks && formik.errors.remarks ? (
                  <div className="text-danger">{formik.errors.remarks}</div>
                ) : null}
              </div>
            </div>

            {/* End: Edit Row */}
          </div>

          {/* Logic for Submit is HERE */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={//formik.isSubmitting || 
                !formik.isValid}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AssetListAdd;
