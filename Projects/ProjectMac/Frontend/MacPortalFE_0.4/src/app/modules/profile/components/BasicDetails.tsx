import React, { useCallback, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { ID, KTIcon } from "../../../../_metronic/helpers";
import { employeeIdForView } from "../../GlobalQuery";
import {
  InternalMode,
  ThemeModeComponent,
} from "../../../../_metronic/assets/ts/layout";
import { toast } from "react-toastify";

interface Language {
  emP_CODE: string | ID;
  id: string;
  name: string;
  proficiency: number;
  work_PROFICIENCY: string;
  speak: string;
  write: string;
  read: string;
  language: string;
  isEditing: boolean;
}

interface LangDropdownItem {
  descr: string;
  flD_VALUE: string;
}

interface BasicDetails {
  emP_CODE: string;
  shorT_NAME: string;
  emP_FNAME: string;
  emP_LNAME: string;
  emP_MNAME: string;
  gendeR_DESCR: string;
  birtH_DATE: string;
  religion: string;
  m_STATUS_DESCR: string;
  m_STATUS_DATE: string;
  blooD_GRP: string;
  fatheR_NAME: string;
  motheR_NAME: string;
  emaiL_ID_PER: string;
  mobilE_NO: string;
  sitE_NAME: string;
  depT_NAME: string;
  section: string;
  designation: string;
  gradE_NAME: string;
  cadre: string;
  joininG_CTC: string;
  curR_CTC: string;
  maC_EXP: string;
  toT_EXP: string;
  emaiL_ID_OFF: string;
  mobilE_NO_OFF: string;
  datE_JOIN: string;
  joineD_AS: string;
  reporT_TO: string;
  employmenT_TYPE: string;
  noticE_PRD: string;
  probatioN_DATE: string;
  traininG_PRD: string;
  proB_EXT: string;
  datE_CONF: string;
  probatioN_PRD: string;
  conF_EXT: string;
}

export function BasicDetails() {
  const [basicDetails, setBasicDetails] = useState<BasicDetails>();
  const [isMinimized, setIsCollapsed] = useState(false);
  const currentMode: InternalMode = ThemeModeComponent.getCurrentMode();

  const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
  const [languages, setLanguages] = useState<Language[]>([]);
  const [drpdwnLanguages, setDrpdwnLanguages] = useState<LangDropdownItem[]>(
    []
  );
  const [newLanguage, setNewLanguage] = useState<Language | null>(null);
  const [isAvailable, setIsAvailable] = useState(false);
  const [originalLanguages, setOriginalLanguages] = useState<Language[]>([]);

  const fetchLanguages = useCallback(() => {
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPLANG`)
      .then((response) => {
        setLanguages(response.data);
        setOriginalLanguages(response.data);
      })
      .catch((error) => {
        console.error("Error fetching language details:", error);
      });
  }, [EMPLOYEE_URL]);

  const fetchDropdownLanguages = useCallback(() => {
    axios
      .get(`${EMPLOYEE_URL}/ddl_GetLanguages`)
      .then((response) => {
        setDrpdwnLanguages(response.data.states);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, [EMPLOYEE_URL]);

  useEffect(() => {
    // Fetch language details from API
    fetchLanguages();
    fetchDropdownLanguages();
  }, [fetchLanguages, fetchDropdownLanguages]);

  const handleAdd = () => {
    setIsAvailable(true);
    setNewLanguage({
      emP_CODE: "",
      id: "",
      name: "",
      language: "",
      read: "N",
      write: "N",
      speak: "N",
      work_PROFICIENCY: "",
      proficiency: 0,
      isEditing: true,
    });
  };

  const handleSaveNewLanguage = () => {
    if (newLanguage) {
      newLanguage.emP_CODE = employeeIdForView;
      newLanguage.work_PROFICIENCY = "Y";
    }

    axios
      .post(`${EMPLOYEE_URL}/AddLanguage`, newLanguage)
      .then((response) => {
        const { responseStatus, message } = response.data.data;

        if (responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <div className="alert alert-danger" role="alert">
                <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
              </div>
            </div>
          );
        } else {
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <div className="alert alert-success" role="alert">
                <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
              </div>
            </div>
          );

          setLanguages([...languages, { ...response.data, isEditing: false }]);
          setOriginalLanguages([
            ...languages,
            { ...response.data, isEditing: false },
          ]);
          setNewLanguage(null);
          setIsAvailable(false);
          fetchLanguages();
        }
      })
      .catch((error) => {
        console.error("Error submitting form:", error);
      });
  };

  const handleCancelNewLanguage = () => {
    setIsAvailable(false);
    setNewLanguage(null);
  };

  const handleEdit = (index: number) => {
    setIsAvailable(true);
    const updatedLanguages = [...languages];
    updatedLanguages[index] = { ...originalLanguages[index], isEditing: true };
    setLanguages(updatedLanguages);
  };

  const handleSaveEdit = (index: number) => {
    const updatedLanguage = languages[index];
    // updatedLanguage.work_PROFICIENCY='Y';
    axios
      .put(
        `${EMPLOYEE_URL}/UpdateLanguage/${employeeIdForView}/${updatedLanguage.language}`,
        updatedLanguage
      )
      .then((response) => {
        const { responseStatus, message } = response.data.data;

        if (responseStatus === "E") {
          toast.error(
            <div>
              <h4 className="alert-heading">Failed to submit Form...</h4>
              <div className="alert alert-danger" role="alert">
                <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
              </div>
            </div>
          );
        } else {
          toast.success(
            <div>
              <h4 className="alert-heading">Successfully Submitted Form...</h4>
              <div className="alert alert-success" role="alert">
                <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
              </div>
            </div>
          );

          const updatedLanguages = [...languages];
          updatedLanguages[index] = { ...response.data, isEditing: false };
          setLanguages(updatedLanguages);
          setOriginalLanguages(updatedLanguages); // Update originalLanguages
          setIsAvailable(false);
          fetchLanguages();
        }
      })
      .catch((error) => {
        console.error("Error submitting form:", error);
      });
  };

  const handleCancelEdit = (index: number) => {
    setIsAvailable(false);
    const updatedLanguages = [...languages];
    updatedLanguages[index] = { ...originalLanguages[index], isEditing: false }; // Revert changes
    setLanguages(updatedLanguages);
  };

  const handleChange = (
    index: number,
    field: keyof Language,
    value: string | number
  ) => {
    if (newLanguage !== null) {
      setNewLanguage({
        ...newLanguage,
        [field]: value as Language[keyof Language],
      });
    } else {
      const updatedLanguages = [...languages];
      updatedLanguages[index] = {
        ...updatedLanguages[index],
        [field]: value as Language[keyof Language],
      };
      setLanguages(updatedLanguages);
    }
  };

  useEffect(() => {
    // Fetch basic details from the backend API
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setBasicDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, [EMPLOYEE_URL]);

  return (
    <div
      className={`card`}
      id="Contact Details"
      // style={{ background: "#F9F9FA" }}
      style={
        currentMode === "dark"
          ? { background: "#0F1014" }
          : { background: "#F9F9FA" }
      }
    >
      
        <div className="cardrow  mb-2 g-xxl-2">
          {/* <div className="rounded  bg-white"> */}
          <div
            className={
              "rounded " + (currentMode === "dark" ? "bg-light" : "bg-white")
            }
          >
            <div className="card-header bg-light-primary d-flex justify-content-between align-items-left cursor-pointer">
              <div className="card-title m-0">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                  Basic Details
                </h3>
              </div>
              <Link
                to="/crafted/pages/profile/EditBasicDetails"
                className="btn btn-sm btn-primary me-3 align-self-center"
              >
                Edit
              </Link>
            </div>
            <div className="card-body p-9">
              {/* Begin:first Row */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    EMPLOYEE CODE:
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.emP_CODE.toUpperCase()} */}
                    {basicDetails?.emP_CODE
                      ? basicDetails.emP_CODE.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    SHORT NAME:
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.shorT_NAME.toUpperCase()} */}
                    {basicDetails?.shorT_NAME
                      ? basicDetails.shorT_NAME.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>
              {/* End:first Row */}
              {/* Begin: Second Row */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    FIRST NAME:
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.emP_FNAME.toUpperCase()} */}
                    {basicDetails?.emP_FNAME
                      ? basicDetails.emP_FNAME.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    LAST NAME:
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.emP_LNAME.toUpperCase()} */}
                    {basicDetails?.emP_LNAME
                      ? basicDetails.emP_LNAME.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>
              {/* End: Second Row */}

              {/* Begin: Second Row */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    MIDDLE NAME:
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.emP_MNAME.toUpperCase()} */}
                    {basicDetails?.emP_MNAME
                      ? basicDetails.emP_MNAME.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    GENDER :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.gendeR_DESCR.toUpperCase()} */}
                    {basicDetails?.gendeR_DESCR
                      ? basicDetails.gendeR_DESCR.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>
              {/* End: Second Row */}

              {/* Begin: Second Row */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    BIRTH DATE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {new Date(basicDetails?.birtH_DATE).toLocaleDateString(
                      "en-GB"
                    )} */}
                    {basicDetails?.birtH_DATE
                      ? new Date(basicDetails?.birtH_DATE).toLocaleDateString(
                          "en-GB"
                        )
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    RELIGION:
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.religion.toUpperCase()} */}
                    {basicDetails?.religion
                      ? basicDetails.religion.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>
              {/* End: Second Row */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    {" "}
                    MARITAL STATUS :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.m_STATUS_DESCR.toUpperCase()} */}
                    {basicDetails?.m_STATUS_DESCR
                      ? basicDetails.m_STATUS_DESCR.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    MARRIAGE DATE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {new Date(basicDetails?.m_STATUS_DATE).toLocaleDateString(
                      "en-GB"
                    )} */}

                    {basicDetails?.m_STATUS_DATE
                      ? new Date(
                          basicDetails?.m_STATUS_DATE
                        ).toLocaleDateString("en-GB")
                      : ""}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    BLOOD GROUP :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.blooD_GRP.toUpperCase()} */}
                    {basicDetails?.blooD_GRP
                      ? basicDetails.blooD_GRP.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6"></div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    FATHER NAME :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.fatheR_NAME
                      ? basicDetails.fatheR_NAME.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    MOTHER NAME :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.motheR_NAME.toUpperCase()} */}
                    {basicDetails?.motheR_NAME
                      ? basicDetails.motheR_NAME.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin: Second Row */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    PERSONAL EMAIL:
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.emaiL_ID_PER.toUpperCase()} */}
                    {basicDetails?.emaiL_ID_PER
                      ? basicDetails.emaiL_ID_PER.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    PERSONAL MOBILE:
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.mobilE_NO.toUpperCase()} */}
                    {basicDetails?.mobilE_NO
                      ? basicDetails.mobilE_NO.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>
              {/* End: Second Row */}
            </div>
          </div>
        </div>
      
        <div className="cardrow g-2 mb-2 g-xxl-2">
          <div
            className={
              "rounded " + (currentMode === "dark" ? "bg-light" : "bg-white")
            }
          >
            <div className="card-header bg-light-primary  cursor-pointer">
              <div className="card-title m-0">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                  Official Details
                </h3>
              </div>
              {/* <Link
                to="/crafted/pages/profile/EditPersonalDetails"
                //src/app/modules/profile/components/PersonalDetailsEdit.tsx
                className="btn btn-sm btn-primary me-3 align-self-center"
              >
                Edit
              </Link> */}
            </div>
            <div className="card-body p-9">
              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    LOCATION :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.sitE_NAME.toUpperCase()} */}
                    {basicDetails?.sitE_NAME
                      ? basicDetails.sitE_NAME.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    DEPARTMENT :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.depT_NAME.toUpperCase()} */}
                    {basicDetails?.depT_NAME
                      ? basicDetails.depT_NAME.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}

              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    SECTION :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.section} */}
                    {basicDetails?.section
                      ? basicDetails.section.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    DESIGNATION :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.designation} */}
                    {basicDetails?.designation
                      ? basicDetails.designation.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    GRADE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.gradE_NAME} */}
                    {basicDetails?.gradE_NAME
                      ? basicDetails.gradE_NAME.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    CADRE/LEVEL :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.cadre.toUpperCase()} */}
                    {basicDetails?.cadre
                      ? basicDetails.cadre.toUpperCase()
                      : ""}
                  </span>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    JOINING CTC :
                  </label>
                  <span className="badge badge-light-success fs-6  text-hover-primary">
                    {basicDetails?.joininG_CTC}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    CURRENT CTC :{" "}
                  </label>
                  <span className="badge badge-light-success fs-6  text-hover-primary">
                    {basicDetails?.curR_CTC}
                  </span>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    MACLEODS EXPERIANCE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.maC_EXP}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    {" "}
                    TOTAL EXPERIANCE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.toT_EXP}
                  </span>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    OFFICIAL EMAIL :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.emaiL_ID_OFF.toUpperCase()} */}
                    {basicDetails?.emaiL_ID_OFF
                      ? basicDetails.emaiL_ID_OFF.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    OFFICIAL MOBILE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.mobilE_NO_OFF}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      
        <div className="cardrow g-2 mb-2 g-xxl-2">
          <div
            className={
              "rounded " + (currentMode === "dark" ? "bg-light" : "bg-white")
            }
          >
            <div className="card-header bg-light-primary d-flex justify-content-between align-items-left cursor-pointer">
              <div className="card-title m-0 ">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                  Joining Details
                </h3>
              </div>
              <Link
                to="/crafted/pages/profile/EditJoiningDetails"
                className="btn btn-sm btn-primary me-3 align-self-center"
              >
                Edit
              </Link>
            </div>
            <div className="card-body p-9">
              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    JOINING DATE :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails.datE_JOIN} */}
                    {/* {new Date(basicDetails?.datE_JOIN).toLocaleDateString(
                      "en-GB"
                    )} */}

                    {basicDetails?.datE_JOIN
                      ? new Date(basicDetails?.datE_JOIN).toLocaleDateString(
                          "en-GB"
                        )
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    JOINED AS :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.joineD_AS}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    JOINING CTC :
                  </label>

                  <span className="badge badge-light-success fs-6  text-hover-primary">
                    {basicDetails?.joininG_CTC}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    REPORT TO :{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.reporT_TO}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    EMPLOYMENT TYPE:{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.employmenT_TYPE.toUpperCase()} */}
                    {basicDetails?.employmenT_TYPE
                      ? basicDetails.employmenT_TYPE.toUpperCase()
                      : ""}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    NOTICE PERIOD:{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.noticE_PRD
                      ? `${basicDetails.noticE_PRD} days`
                      : ""}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    PROBATION DATE:{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails?.conF_EXT} */}

                    {basicDetails?.probatioN_DATE
                      ? new Date(
                          basicDetails?.probatioN_DATE
                        ).toLocaleDateString("en-GB")
                      : ""}
                  </span>
                </div>

                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    PROBATION MONTHS:{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.traininG_PRD}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    PROBATION EXTENSION:{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.proB_EXT}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}

              {/* Begin:first Row     */}
              <div className="row">
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    CONFIRMATION DATE:{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {/* {basicDetails.datE_CONF} */}
                    {/* {new Date(basicDetails?.datE_CONF).toLocaleDateString(
                      "en-GB"
                    )} */}

                    {basicDetails?.datE_CONF
                      ? new Date(basicDetails?.datE_CONF).toLocaleDateString(
                          "en-GB"
                        )
                      : ""}
                  </span>
                </div>

                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    CONFIRMATION MONTHS:{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.probatioN_PRD}
                  </span>
                </div>
                <div className="col-lg-6 mb-6">
                  <label style={{ minWidth: "170px" }} className="col-lg-4 fw-bold text-muted">
                    CONFIRMATION EXTENSION:{" "}
                  </label>
                  <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                    {basicDetails?.conF_EXT}
                  </span>
                </div>
              </div>
              {/* End:first Row     */}
            </div>
          </div>
        </div>
      
        <div className="cardrow g-2 mb-2 g-xxl-2">
          <div
            className={
              "rounded " + (currentMode === "dark" ? "bg-light" : "bg-white")
            }
          >
            <div className="card-header bg-light-primary d-flex justify-content-between align-items-left cursor-pointer">
              <div className="card-title m-0">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                  Language Details
                </h3>
              </div>

              <div className="d-flex justify-content-end">
                <button
                  onClick={handleAdd}
                  className="btn btn-sm btn-primary me-3 align-self-center"
                  disabled={isAvailable}
                >
                  ADD
                </button>
              </div>
            </div>
            <div className="card-body p-4">
              <div id="kt_language_details" className="card-body pt-0">
                {languages.map((language, index) => (
                  <div
                    key={index}
                    className={`d-flex align-items-center bg-light-${
                      index % 4 === 0
                        ? "warning"
                        : index % 4 === 1
                        ? "success"
                        : index % 4 === 2
                        ? "danger"
                        : "info"
                    } rounded p-5 mb-2`}
                  >
                    <div className="d-flex flex-wrap flex-stack">
                      <div className="d-flex flex-column flex-grow-1 pe-8">
                        <div className="d-flex flex-wrap">
                          <div className="min-w-30px py-0 px-4 me-6 mb-3 d-flex justify-content-center align-items-center">
                            <span
                              className={`text-${
                                index % 4 === 0
                                  ? "warning"
                                  : index % 4 === 1
                                  ? "success"
                                  : index % 4 === 2
                                  ? "danger"
                                  : "info"
                              } me-5`}
                            >
                              <KTIcon
                                iconName="abstract-26"
                                className={`text-${
                                  index % 4 === 0
                                    ? "warning"
                                    : index % 4 === 1
                                    ? "success"
                                    : index % 4 === 2
                                    ? "danger"
                                    : "info"
                                } fs-1 me-5`}
                              />
                            </span>
                          </div>

                          {language.isEditing ? (
                            <>
                              <div className="min-w-100px py-0 px-4 me-6 mb-3 d-flex justify-content-left align-items-center">
                                {/* <select
                                  name="newlanguage"
                                  className="form-select form-select-lg form-select-solid bg-light-danger flex-grow-1 me-2"
                                  value={language.language}
                                  onChange={(e) =>
                                    handleChange(
                                      index,
                                      "language",
                                      e.target.value
                                    )
                                  }
                                  aria-placeholder="Language Name"
                                  required
                                  style={{ width: "200px" }}
                                >
                                  <option value=""> SELECT LANGUGE</option>
                                  {drpdwnLanguages.map((lang) => (
                                    <option
                                      key={lang.flD_VALUE}
                                      value={lang.flD_VALUE}
                                    >
                                      {lang.descr}
                                    </option>
                                  ))}
                                </select> */}
                                <input
                                  name="newlanguage"
                                  className="form-control form-control-lg form-control-solid bg-light-danger flex-grow-1 me-2"
                                  value={language.language}
                                  style={{ width: "200px" }}
                                  readOnly
                                />
                              </div>
                              <div className="min-w-125px py-0 px-4 me-6 mb-3 d-flex align-items-center">
                                <div className="form-check form-check-custom form-check-solid me-5">
                                  <input
                                    className="form-check-input border border-gray-500"
                                    type="checkbox"
                                    checked={language.read === "Y"}
                                    onChange={(e) =>
                                      handleChange(
                                        index,
                                        "read",
                                        e.target.checked ? "Y" : "N"
                                      )
                                    }
                                    style={{ width: "16px", height: "16px" }}
                                  />
                                  <label className="form-check-label fw-bold">
                                    Read
                                  </label>
                                </div>
                                <div className="form-check form-check-custom form-check-solid me-5">
                                  <input
                                    className="form-check-input border border-gray-500"
                                    type="checkbox"
                                    checked={language.write === "Y"}
                                    onChange={(e) =>
                                      handleChange(
                                        index,
                                        "write",
                                        e.target.checked ? "Y" : "N"
                                      )
                                    }
                                    style={{ width: "16px", height: "16px" }}
                                  />
                                  <label className="form-check-label fw-bold">
                                    Write
                                  </label>
                                </div>
                                <div className="form-check form-check-custom form-check-solid">
                                  <input
                                    className="form-check-input border border-gray-500"
                                    type="checkbox"
                                    checked={language.speak === "Y"}
                                    onChange={(e) =>
                                      handleChange(
                                        index,
                                        "speak",
                                        e.target.checked ? "Y" : "N"
                                      )
                                    }
                                    style={{ width: "16px", height: "16px" }}
                                  />
                                  <label className="form-check-label fw-bold">
                                    Speak
                                  </label>
                                </div>
                              </div>
                              <div className="min-w-170px py-0 px-4 me-6 mb-3">
                                <div className="d-flex align-items-center">
                                  <span
                                    className={`fw-bold text-${
                                      index % 4 === 0
                                        ? "warning"
                                        : index % 4 === 1
                                        ? "success"
                                        : index % 4 === 2
                                        ? "danger"
                                        : "info"
                                    } py-1`}
                                  >
                                    Proficiency:
                                  </span>
                                  <input
                                    type="number"
                                    value={language.work_PROFICIENCY}
                                    onChange={(e) =>
                                      handleChange(
                                        index,
                                        "work_PROFICIENCY",
                                        e.target.value
                                      )
                                    }
                                    className="form-control"
                                    min={0}
                                    max={100}
                                    style={{
                                      width: "100px",
                                      marginLeft: "10px",
                                    }}
                                  />
                                </div>
                              </div>
                              <div className="min-w-125px py-0 px-4 me-6 mb-3 d-flex align-items-center">
                                <button
                                  onClick={() => handleSaveEdit(index)}
                                  className="btn btn-sm btn-primary me-2"
                                >
                                  Save
                                </button>
                                <button
                                  onClick={() => handleCancelEdit(index)}
                                  className="btn btn-sm btn-secondary"
                                >
                                  Cancel
                                </button>
                              </div>
                            </>
                          ) : (
                            <>
                              <div className="min-w-125px py-0 px-4 me-6 mb-3 d-flex justify-content-left align-items-center">
                                <a className="fw-bold text-gray-800 fs-6">
                                  {language.language
                                    ? language.language.toUpperCase()
                                    : ""}
                                </a>
                              </div>
                              <div className="min-w-125px py-0 px-4 me-6 mb-3 d-flex align-items-center">
                                <div className="form-check form-check-custom form-check-solid me-5">
                                  <input
                                    className="form-check-input"
                                    style={{ width: "16px", height: "16px" }}
                                    type="checkbox"
                                    checked={language.read === "Y"}
                                    readOnly
                                  />
                                  <label className="form-check-label fw-bold">
                                    Read
                                  </label>
                                </div>
                                <div className="form-check form-check-custom form-check-solid me-5">
                                  <input
                                    className="form-check-input"
                                    style={{ width: "16px", height: "16px" }}
                                    type="checkbox"
                                    checked={language.write === "Y"}
                                    readOnly
                                  />
                                  <label className="form-check-label fw-bold">
                                    Write
                                  </label>
                                </div>
                                <div className="form-check form-check-custom form-check-solid">
                                  <input
                                    className="form-check-input"
                                    style={{ width: "16px", height: "16px" }}
                                    type="checkbox"
                                    checked={language.speak === "Y"}
                                    readOnly
                                  />
                                  <label className="form-check-label fw-bold">
                                    Speak
                                  </label>
                                </div>
                              </div>
                              <div className="w-250px py-0 px-4 me-6 mb-3">
                                <div className="d-flex align-items-center">
                                  <span
                                    className={`fw-bold text-${
                                      index % 4 === 0
                                        ? "warning"
                                        : index % 4 === 1
                                        ? "success"
                                        : index % 4 === 2
                                        ? "danger"
                                        : "info"
                                    } py-1`}
                                  >
                                    Proficiency: {language.work_PROFICIENCY}%
                                  </span>
                                </div>
                                <div className="progress h-6px w-100">
                                  <div
                                    className={`progress-bar bg-${
                                      index % 4 === 0
                                        ? "warning"
                                        : index % 4 === 1
                                        ? "success"
                                        : index % 4 === 2
                                        ? "danger"
                                        : "info"
                                    }`}
                                    role="progressbar"
                                    style={{
                                      width: `${language.work_PROFICIENCY}%`,
                                    }}
                                  ></div>
                                </div>
                              </div>
                              <div className="min-w-100px py-0 px-4 me-0 mb-3 d-flex align-items-center text-end">
                                <button
                                  onClick={() => handleEdit(index)}
                                  className="btn btn-sm btn-primary"
                                  disabled={isAvailable}
                                >
                                  Edit
                                </button>
                              </div>
                            </>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
                {newLanguage && (
                  <div className="d-flex align-items-center bg-light-info rounded p-5 mb-2">
                    <div className="d-flex flex-wrap flex-stack">
                      <div className="d-flex flex-column flex-grow-1 pe-8">
                        <div className="d-flex flex-wrap">
                          <div className="min-w-30px py-0 px-4 me-6 mb-3 d-flex justify-content-center align-items-center">
                            <span className="text-info me-5">
                              <KTIcon
                                iconName="abstract-26"
                                className="text-info fs-1 me-5"
                              />
                            </span>
                          </div>
                          <div className="min-w-125px py-0 px-4 me-6 mb-3 d-flex justify-content-left align-items-center">
                            <select
                              name="language"
                              className="form-select form-select-lg form-select-solid bg-light-danger flex-grow-1 me-2"
                              value={newLanguage.language}
                              onChange={(e) =>
                                handleChange(-1, "language", e.target.value)
                              }
                              aria-placeholder="Language Name"
                              required
                              style={{ width: "200px" }}
                            >
                              <option value=""> SELECT LANGUGE</option>
                              {drpdwnLanguages.map((lang) => (
                                <option
                                  key={lang.flD_VALUE}
                                  value={lang.flD_VALUE}
                                >
                                  {lang.descr}
                                </option>
                              ))}
                            </select>
                          </div>
                          <div className="min-w-125px py-0 px-4 me-6 mb-3 d-flex align-items-center">
                            <div className="form-check form-check-custom form-check-solid me-5">
                              <input
                                className="form-check-input border border-gray-500"
                                style={{ width: "16px", height: "16px" }}
                                type="checkbox"
                                checked={newLanguage.read === "Y"}
                                onChange={(e) =>
                                  handleChange(
                                    -1,
                                    "read",
                                    e.target.checked ? "Y" : "N"
                                  )
                                }
                              />
                              <label className="form-check-label fw-bold">
                                Read
                              </label>
                            </div>
                            <div className="form-check form-check-custom form-check-solid me-5">
                              <input
                                className="form-check-input border border-gray-500"
                                style={{ width: "16px", height: "16px" }}
                                type="checkbox"
                                checked={newLanguage.write === "Y"}
                                onChange={(e) =>
                                  handleChange(
                                    -1,
                                    "write",
                                    e.target.checked ? "Y" : "N"
                                  )
                                }
                              />
                              <label className="form-check-label fw-bold">
                                Write
                              </label>
                            </div>
                            <div className="form-check form-check-custom form-check-solid">
                              <input
                                className="form-check-input border border-gray-500"
                                style={{ width: "16px", height: "16px" }}
                                type="checkbox"
                                checked={newLanguage.speak === "Y"}
                                onChange={(e) =>
                                  handleChange(
                                    -1,
                                    "speak",
                                    e.target.checked ? "Y" : "N"
                                  )
                                }
                              />
                              <label className="form-check-label fw-bold">
                                Speak
                              </label>
                            </div>
                          </div>
                          <div className="min-w-170px py-0 px-4 me-6 mb-3">
                            <div className="d-flex align-items-center">
                              <span className="fw-bold text-info py-1">
                                Proficiency:
                              </span>
                              <input
                                type="number"
                                value={newLanguage.work_PROFICIENCY}
                                onChange={(e) =>
                                  handleChange(
                                    -1,
                                    "work_PROFICIENCY",
                                    e.target.value
                                  )
                                }
                                min={0}
                                max={100}
                                className="form-control"
                                style={{ width: "100px", marginLeft: "10px" }}
                              />
                            </div>
                          </div>
                          <div className="min-w-125px py-0 px-4 me-6 mb-3 d-flex align-items-center">
                            <button
                              onClick={handleSaveNewLanguage}
                              className="btn btn-sm btn-primary me-2"
                            >
                              Save
                            </button>
                            <button
                              onClick={handleCancelNewLanguage}
                              className="btn btn-sm btn-secondary"
                            >
                              Cancel
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
    </div>
  );
}
