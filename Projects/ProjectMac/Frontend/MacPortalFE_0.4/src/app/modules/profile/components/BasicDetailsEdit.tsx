import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { KTIcon, Mode, QUERIES } from "../../../../_metronic/helpers";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
import { useListView } from "../../apps/transaction-master/master-list/core/ListViewProvider";
import clsx from "clsx";
import { useMutation, useQueryClient } from "react-query";
import {insertTransactionV2 } from "../../apps/transaction-master/master-list/core/_requests";
import { useQueryResponse } from "../../apps/employee/employee-list/core/QueryResponseProvider";

type FieldDetail = {
  olD_KEY: string;
  olD_VALUE: string;
  neW_KEY: string;
  neW_VALUE: string;
  FILE_PATH: string;
};

type BasicDetails = {
  emP_IMAGE: string;
  emP_CODE: string;
  emP_NAME: string;
  emP_FNAME: string;
  birtH_DATE: string;
  emP_LNAME: string;
  emP_MNAME: string;
  shorT_NAME: string;
  religion: string;
  blooD_GRP: string;
  fatheR_NAME: string;
  motheR_NAME: string;
  emaiL_ID_PER: string;
  mobilE_NO: string;
  birtH_DATE_FILE: string;
  gender: string;
  gender_val: string;
  CHG_REASON: string;

  fieldNames?: {
    EMP_CODE: FieldDetail;
    EMP_NAME: FieldDetail;
    EMP_FNAME: FieldDetail;
    BIRTH_DATE: FieldDetail;
    EMP_LNAME: FieldDetail;
    EMP_MNAME: FieldDetail;
    SHORT_NAME: FieldDetail;
    RELIGION: FieldDetail;
    BLOOD_GRP: FieldDetail;
    FATHER_NAME: FieldDetail;
    MOTHER_NAME: FieldDetail;
    EMAIL_ID_PER: FieldDetail;
    MOBILE_NO: FieldDetail;
    GENDER: FieldDetail;
    CHG_REASON: FieldDetail;
    EMP_IMAGE: FieldDetail;
  };
};

const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const BasicDetailsEdit: React.FC = () => {
  const { currentUser } = useAuth();
  const [privateIP, setPrivateIP] = useState();

  const { itemIdForUpdate, setItemIdForUpdate, formMode } = useListView();
  const [empCodeTxForView, setEmpCodeTxForView] = useState();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();
  const navigate = useNavigate();

  const [image] = useState<string>("media/avatars/User.png");
 

  const [basicDetails, setBasicDetails] = useState<BasicDetails>();
  const [basicDetailsTx, setBasicDetailsTx] = useState<BasicDetails>();

  const [genders] = useState([
    { code: "F", descr: "FEMALE" },
    { code: "M", descr: "MALE" },
  ]);

  const [religions] = useState([
    { code: "HINDU", descr: "HINDU" },
    { code: "MUSLIM", descr: "MUSLIM" },
    { code: "SIKH", descr: "SIKH" },
    { code: "CHRISTIAN", descr: "CHRISTIAN" },
    { code: "BAUDDHA", descr: "BAUDDHA" },
    { code: "JAIN", descr: "JAIN" },
    { code: "PARASI", descr: "PARASI" },
  ]);

  const [bloodGrps] = useState([
    { code: "A+", descr: "A+" },
    { code: "A-", descr: "A-" },
    { code: "B+", descr: "B+" },
    { code: "B-", descr: "B-" },
    { code: "AB+", descr: "AB+" },
    { code: "AB-", descr: "AB-" },
    { code: "O+", descr: "O+" },
    { code: "O-", descr: "O-" },
  ]);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching Ip Address:", error));

    itemIdForUpdate && fetchBasicDetailsTx();
    fetchBasicDetails();
  }, [empCodeTxForView]);



  const fetchBasicDetails = () => {
    axios
      .get(
        `${EMPLOYEE_URL}/${
          employeeIdForView ? employeeIdForView : empCodeTxForView
        }/EMPPER`
      )
      .then((response) => {
        setBasicDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching BASIC details:", error);
      });
  };

  const fetchBasicDetailsTx = () => {
    axios
      .get(`${MASTERTX_URL}/GetTransactionByID?tranId=${itemIdForUpdate}`)
      .then((response) => {
        setEmpCodeTxForView(
          response.data.transactions[0].fieldNames?.EMP_CODE?.neW_VALUE
        );

        setBasicDetailsTx(response.data.transactions[0]);
      });
  };

  useEffect(() => {
    formik.setValues({
      emP_IMAGE:
        basicDetailsTx?.fieldNames?.EMP_IMAGE?.neW_KEY ||
        basicDetails?.emP_IMAGE ||
        "",

      emP_IMAGE_O:
        basicDetailsTx?.fieldNames?.EMP_IMAGE?.neW_KEY ||
        basicDetails?.emP_IMAGE ||
        "",

      emP_CODE:
        basicDetailsTx?.fieldNames?.EMP_CODE?.neW_KEY ||
        basicDetails?.emP_CODE ||
        "",

      emP_CODE_O:
        basicDetailsTx?.fieldNames?.EMP_CODE?.neW_KEY ||
        basicDetails?.emP_CODE ||
        "",

      emP_NAME:
        basicDetailsTx?.fieldNames?.EMP_NAME?.neW_KEY ||
        basicDetails?.emP_NAME ||
        "",
      emP_NAME_O:
        basicDetailsTx?.fieldNames?.EMP_NAME?.neW_KEY ||
        basicDetails?.emP_NAME ||
        "",

      emP_FNAME:
        basicDetailsTx?.fieldNames?.EMP_FNAME?.neW_KEY ||
        basicDetails?.emP_FNAME ||
        "",
      emP_FNAME_O:
        basicDetailsTx?.fieldNames?.EMP_FNAME?.neW_KEY ||
        basicDetails?.emP_FNAME ||
        "",

      birtH_DATE:
        basicDetailsTx?.fieldNames?.BIRTH_DATE?.neW_KEY ||
        basicDetails?.birtH_DATE ||
        "",
      birtH_DATE_O:
        basicDetailsTx?.fieldNames?.BIRTH_DATE?.neW_KEY ||
        basicDetails?.birtH_DATE ||
        "",

      birtH_DATE_FILE:
        basicDetailsTx?.fieldNames?.BIRTH_DATE?.FILE_PATH ||
        basicDetails?.birtH_DATE_FILE ||
        "",
      birtH_DATE_FILE_O:
        basicDetailsTx?.fieldNames?.BIRTH_DATE?.FILE_PATH ||
        basicDetails?.birtH_DATE_FILE ||
        "",

      emP_LNAME:
        basicDetailsTx?.fieldNames?.EMP_LNAME?.neW_KEY ||
        basicDetails?.emP_LNAME ||
        "",
      emP_LNAME_O:
        basicDetailsTx?.fieldNames?.EMP_LNAME?.neW_KEY ||
        basicDetails?.emP_LNAME ||
        "",

      emP_MNAME:
        basicDetailsTx?.fieldNames?.EMP_MNAME?.neW_KEY ||
        basicDetails?.emP_MNAME ||
        "",
      emP_MNAME_O:
        basicDetailsTx?.fieldNames?.EMP_MNAME?.neW_KEY ||
        basicDetails?.emP_MNAME ||
        "",

      shorT_NAME:
        basicDetailsTx?.fieldNames?.SHORT_NAME?.neW_KEY ||
        basicDetails?.shorT_NAME ||
        "",
      shorT_NAME_O:
        basicDetailsTx?.fieldNames?.SHORT_NAME?.neW_KEY ||
        basicDetails?.shorT_NAME ||
        "",

      religion:
        basicDetailsTx?.fieldNames?.RELIGION?.neW_KEY ||
        basicDetails?.religion ||
        "",
      religion_O:
        basicDetailsTx?.fieldNames?.RELIGION?.neW_KEY ||
        basicDetails?.religion ||
        "",

      blooD_GRP:
        basicDetailsTx?.fieldNames?.BLOOD_GRP?.neW_KEY ||
        basicDetails?.blooD_GRP ||
        "",
      blooD_GRP_O:
        basicDetailsTx?.fieldNames?.BLOOD_GRP?.neW_KEY ||
        basicDetails?.blooD_GRP ||
        "",

      fatheR_NAME:
        basicDetailsTx?.fieldNames?.FATHER_NAME?.neW_KEY ||
        basicDetails?.fatheR_NAME ||
        "",
      fatheR_NAME_O:
        basicDetailsTx?.fieldNames?.FATHER_NAME?.neW_KEY ||
        basicDetails?.fatheR_NAME ||
        "",

      motheR_NAME:
        basicDetailsTx?.fieldNames?.MOTHER_NAME?.neW_KEY ||
        basicDetails?.motheR_NAME ||
        "",
      motheR_NAME_O:
        basicDetailsTx?.fieldNames?.MOTHER_NAME?.neW_KEY ||
        basicDetails?.motheR_NAME ||
        "",

      emaiL_ID_PER:
        basicDetailsTx?.fieldNames?.EMAIL_ID_PER?.neW_KEY ||
        basicDetails?.emaiL_ID_PER ||
        "",
      emaiL_ID_PER_O:
        basicDetailsTx?.fieldNames?.EMAIL_ID_PER?.neW_KEY ||
        basicDetails?.emaiL_ID_PER ||
        "",

      mobilE_NO:
        basicDetailsTx?.fieldNames?.MOBILE_NO?.neW_KEY ||
        basicDetails?.mobilE_NO ||
        "",
      mobilE_NO_O:
        basicDetailsTx?.fieldNames?.MOBILE_NO?.neW_KEY ||
        basicDetails?.mobilE_NO ||
        "",

      gender:
        basicDetailsTx?.fieldNames?.GENDER?.neW_KEY ||
        basicDetails?.gender ||
        "",
      gender_O:
        basicDetailsTx?.fieldNames?.GENDER?.neW_KEY ||
        basicDetails?.gender ||
        "",

      gender_val:
        basicDetailsTx?.fieldNames?.GENDER?.neW_VALUE ||
        basicDetails?.gender === "M"
          ? "MALE"
          : basicDetails?.gender === "F"
          ? "FEMALE"
          : "",
      gender_val_O:
        basicDetailsTx?.fieldNames?.GENDER?.olD_VALUE ||
        basicDetails?.gender === "M"
          ? "MALE"
          : basicDetails?.gender === "F"
          ? "FEMALE"
          : "",

      CHG_REASON: formMode === Mode.EDIT ? "" : basicDetails?.CHG_REASON,
    });
  }, [basicDetails]);

  const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    formik.setFieldValue("emP_IMAGE", e.target.files?.[0]);
    const file = e.target.files?.[0];
    if (file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        const base64String = reader.result as string;
        formik.setFieldValue("emP_IMAGE", base64String);
      };
      reader.readAsDataURL(file);
    }
  };

  const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (file) {
      const reader = new FileReader();

      reader.onloadend = () => {
        const base64String = reader.result;
        console.log(base64String);
        formik.setFieldValue("birtH_DATE_FILE", base64String);
      };
      reader.readAsDataURL(file);
    }
  };

  const validationSchema = Yup.object().shape({
    emP_NAME: Yup.string().required("Employee Full Name is required"),
    emP_FNAME: Yup.string().required("Employee First Name is required"),
    emP_LNAME: Yup.string().required("Employee Last Name is required"),
    emP_MNAME: Yup.string().required("Employee Middle Name is required"),
    gender: Yup.string().required("Gender is required"),
    birtH_DATE: Yup.date().required("Date of Birth is required"),
    religion: Yup.string().required("Religion is required"),
    emaiL_ID_PER: Yup.string().required("Personal Email is required"),
    mobilE_NO: Yup.string().required("Personal Mobile No is required"),
    blooD_GRP: Yup.string().required("Blood group is required"),
    fatheR_NAME: Yup.string().required("Father Name is required"),
    CHG_REASON: Yup.string().required("Change Reason is required"),
    birtH_DATE_FILE: Yup.string().when([], {
      is: () => basicDetails?.birtH_DATE !== formik.values.birtH_DATE,
      then: (schema) => schema.required("Proof attachment is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
  });

  const createTx = useMutation((payload: any) => insertTransactionV2(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        handleClose();
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formik = useFormik({
    initialValues: {
      emP_IMAGE: "",
      emP_IMAGE_O: "",

      emP_CODE: "",
      emP_CODE_O: "",

      emP_NAME: "",
      emP_NAME_O: "",

      emP_FNAME: "",
      emP_FNAME_O: "",

      emP_LNAME: "",
      emP_LNAME_O: "",

      emP_MNAME: "",
      emP_MNAME_O: "",

      shorT_NAME: "",
      shorT_NAME_O: "",

      birtH_DATE: "",
      birtH_DATE_O: "",

      birtH_DATE_FILE: "",
      birtH_DATE_FILE_O: "",

      religion: "",
      religion_O: "",

      blooD_GRP: "",
      blooD_GRP_O: "",

      fatheR_NAME: "",
      fatheR_NAME_O: "",

      motheR_NAME: "",
      motheR_NAME_O: "",

      emaiL_ID_PER: "",
      emaiL_ID_PER_O: "",

      mobilE_NO: "",
      mobilE_NO_O: "",

      gender: "",
      gender_O: "",

      gender_val: "",
      gender_val_O: "",

      CHG_REASON: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values, formik) => {
      const payload = {
        values,

        TABLENAME: "EMPLOYEE_BASIC",
        KEYVAR: "",
        WFLEVEL: 1,
        CONF: "N",
        SITECODE: "S0001",
        EMP: currentUser?.id?.toString() || "",
        FILEPATH: null,
        FILE: null,
        FOLDERPATH: null,
        FORMNAME: "Basic Details",
        tX_CHG_USER: currentUser?.id?.toString() || "",
        tX_CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        STRTRANID: itemIdForUpdate ? itemIdForUpdate : null,
      };

      createTx.mutate(payload);
      formik.setSubmitting(false);
    },
  });

  const handleClose = () => {
    itemIdForUpdate ? setItemIdForUpdate(undefined) : navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Basic Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative d-flex flex-column align-items-center">
              <div className="d-flex justify-content-center align-items-center mb-3 position-relative">
                <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative d-flex position-relative">
                  {/* {formik.values.emP_IMAGE && ( */}
                  <img
                    src={formik.values.emP_IMAGE}
                    alt={formik.errors.emP_IMAGE || "NO Image Chosen"}
                    className="max-width-100 mh-100 faded-image border border-dashed border-gray-300 rounded p-5"
                    onClick={() =>
                      // document.getElementById("upload-button").click()
                      {
                        const uploadButton =
                          document.getElementById("upload-button");
                        if (uploadButton) {
                          uploadButton.click();
                        } else {
                          console.error("Upload button not found.");
                        }
                      }
                    }
                  />
                  {/* )} */}
                  <label
                    htmlFor="upload-button"
                    className="btn btn-sm position-absolute top-0 end-0"
                    style={{ transform: "translate(50%, -50%)" }} // Adjust the position slightly
                  >
                    <KTIcon iconName="pencil" className="fs-2x text-dark" />
                  </label>
                  <input
                    id="upload-button"
                    type="file"
                    accept="image/*"
                    onChange={handleImageChange}
                    style={{ display: "none" }}
                    alt="/media/avatars/User.png"
                  />
                </div>
              </div>
              <style>
                {`
                  .faded-image {
                    opacity: 0.5;
                    transition: opacity 0.3s;
                  }
                  .faded-image:hover {
                    opacity: 1;
                  }
                `}
              </style>
            </div>

            {/* Begin: final outer Edit Row */}
            <div className="row mb-1">
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="WORK_SITE"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    EMPLOYEE CODE:
                  </label>
                  <input
                    // className="form-select form-select-lg form-select-solid"
                    className="form-control form-control-lg form-control-solid bg-light-danger"
                    {...(basicDetailsTx?.fieldNames?.EMP_CODE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        basicDetailsTx?.fieldNames?.EMP_CODE?.olD_VALUE === null
                          ? "-"
                          : basicDetailsTx?.fieldNames?.EMP_CODE?.olD_VALUE
                      }`,
                    })}
                    name="emP_CODE"
                    type="text"
                    value={formik.values?.emP_CODE}
                    // {...formik.getFieldProps("emP_CODE")}
                    onChange={formik.handleChange}
                    disabled
                  />
                </div>
                {formik.touched.emP_CODE && formik.errors.emP_CODE && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.emP_CODE}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="shorT_NAME"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    SHORT NAME:
                  </label>
                  <input
                    type="text"
                    id="shorT_NAME"
                    // className="form-control form-control-lg form-control-solid"
                    className="form-control form-control-lg form-control-solid bg-light-danger"
                    {...(basicDetailsTx?.fieldNames?.SHORT_NAME && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        basicDetailsTx?.fieldNames?.SHORT_NAME?.olD_VALUE ===
                        null
                          ? "-"
                          : basicDetailsTx?.fieldNames?.SHORT_NAME?.olD_VALUE
                      }`,
                    })}
                    name="shorT_NAME"
                    placeholder="Enter short name"
                    value={formik.values.shorT_NAME?.toUpperCase()}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    disabled
                  />
                </div>
                {formik.touched.shorT_NAME && formik.errors.shorT_NAME && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.shorT_NAME}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="emP_FNAME"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    FIRST NAME:
                  </label>
                  <input
                    type="text"
                    id="emP_FNAME"
                    name="emP_FNAME"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          basicDetailsTx?.fieldNames?.EMP_FNAME?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.emP_FNAME && formik.errors.emP_FNAME,
                      },
                      {
                        "is-valid":
                          formik.touched.emP_FNAME && !formik.errors.emP_FNAME,
                      }
                    )}
                    {...(basicDetailsTx?.fieldNames?.EMP_FNAME && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        basicDetailsTx?.fieldNames?.EMP_FNAME?.olD_VALUE ===
                        null
                          ? "-"
                          : basicDetailsTx?.fieldNames?.EMP_FNAME?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter first name"
                    value={formik.values.emP_FNAME}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.emP_FNAME && formik.errors.emP_FNAME && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.emP_FNAME}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="emP_LNAME"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    LAST NAME:
                  </label>
                  <input
                    type="text"
                    id="emP_LNAME"
                    name="emP_LNAME"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          basicDetailsTx?.fieldNames?.EMP_LNAME?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.emP_LNAME && formik.errors.emP_LNAME,
                      },
                      {
                        "is-valid":
                          formik.touched.emP_LNAME && !formik.errors.emP_LNAME,
                      }
                    )}
                    {...(basicDetailsTx?.fieldNames?.EMP_LNAME && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        basicDetailsTx?.fieldNames?.EMP_LNAME?.olD_VALUE ===
                        null
                          ? "-"
                          : basicDetailsTx?.fieldNames?.EMP_LNAME?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter last name"
                    value={formik.values.emP_LNAME}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.emP_LNAME && formik.errors.emP_LNAME && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.emP_LNAME}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="emP_MNAME"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    MIDDLE NAME:
                  </label>

                  <input
                    type="text"
                    id="emP_MNAME"
                    name="emP_MNAME"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          basicDetailsTx?.fieldNames?.EMP_MNAME?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.emP_MNAME && formik.errors.emP_MNAME,
                      },
                      {
                        "is-valid":
                          formik.touched.emP_MNAME && !formik.errors.emP_MNAME,
                      }
                    )}
                    {...(basicDetailsTx?.fieldNames?.EMP_MNAME && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        basicDetailsTx?.fieldNames?.EMP_MNAME?.olD_VALUE ===
                        null
                          ? "-"
                          : basicDetailsTx?.fieldNames?.EMP_MNAME?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter middle name"
                    value={formik.values.emP_MNAME}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.emP_MNAME && formik.errors.emP_MNAME && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.emP_MNAME}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="gender"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    GENDER:
                  </label>
                  <select
                    id="gender"
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          basicDetailsTx?.fieldNames?.GENDER?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.gender && formik.errors.gender,
                      },
                      {
                        "is-valid":
                          formik.touched.gender && !formik.errors.gender,
                      }
                    )}
                    {...(basicDetailsTx?.fieldNames?.GENDER && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        basicDetailsTx?.fieldNames?.GENDER?.olD_VALUE === null
                          ? "-"
                          : basicDetailsTx?.fieldNames?.GENDER?.olD_VALUE
                      }`,
                    })}
                    area-placeholder="Enter DD Payable Bank"
                    // value={formik.values.gender?.toUpperCase()}
                    // onChange={formik.handleChange}
                    {...formik.getFieldProps("gender")}
                    name="gender"
                    onChange={(e) => {
                      const selectedValue = e.target.value;
                      const selectedOption = genders.find(
                        (gender) => gender.code === selectedValue
                      );

                      formik.setFieldValue("gender", selectedValue);
                      formik.setFieldValue(
                        "gender_val",
                        selectedOption?.descr.toUpperCase() || ""
                      );
                    }}
                    onBlur={formik.handleBlur}
                  >
                    <option value=""></option>
                    {genders.map((gender) => (
                      <option key={gender.code} value={gender.code}>
                        {gender.descr}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.gender && formik.errors.gender && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.gender}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="birtH_DATE"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    BIRTH DATE:
                  </label>
                  <input
                    type="date"
                    id="birtH_DATE"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid me-2",
                      {
                        "bg-light-warning":
                          basicDetailsTx?.fieldNames?.BIRTH_DATE?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.birtH_DATE && formik.errors.birtH_DATE,
                      },
                      {
                        "is-valid":
                          formik.touched.birtH_DATE &&
                          !formik.errors.birtH_DATE,
                      }
                    )}
                    {...(basicDetailsTx?.fieldNames?.BIRTH_DATE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        basicDetailsTx?.fieldNames?.BIRTH_DATE?.olD_VALUE ===
                        null
                          ? "-"
                          : basicDetailsTx?.fieldNames?.BIRTH_DATE?.olD_VALUE
                      }`,
                    })}
                    name="birtH_DATE"
                    placeholder="Enter Gratuity Date"
                    // value={formik.values.birtH_DATE?.toUpperCase()}
                    value={
                      formik.values?.birtH_DATE
                        ? new Date(formik.values?.birtH_DATE)
                            .toISOString()
                            .split("T")[0]
                        : ""
                    }
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  <input
                    className={clsx(
                      "ps-2 form-control form-control-lg",
                      {
                        "bg-light-warning":
                          basicDetailsTx?.fieldNames?.BIRTH_DATE?.FILE_PATH,
                      },
                      {
                        "is-invalid":
                          formik.touched.birtH_DATE_FILE &&
                          formik.errors.birtH_DATE_FILE,
                      },
                      {
                        "is-valid":
                          formik.touched.birtH_DATE_FILE &&
                          !formik.errors.birtH_DATE_FILE,
                      }
                    )}
                    name="birtH_DATE_FILE"
                    type="file"
                    id="birtH_DATE_FILE"
                    onChange={handlePdfChange}
                    accept="application/pdf"
                  />
                </div>
                <>
                  {formik.touched.birtH_DATE_FILE &&
                    formik.errors.birtH_DATE_FILE && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert">
                          {formik.errors.birtH_DATE_FILE}
                        </span>
                      </div>
                    )}
                  {formik.touched.birtH_DATE && formik.errors.birtH_DATE && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.birtH_DATE}</span>
                    </div>
                  )}
                </>
              </div>
              {/* End: Edit Row */}
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="religion"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    RELIGION:
                  </label>
                  <select
                    id="religion"
                    // className="form-select form-select-lg form-select-solid "
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          basicDetailsTx?.fieldNames?.RELIGION?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.religion && formik.errors.religion,
                      },
                      {
                        "is-valid":
                          formik.touched.religion && !formik.errors.religion,
                      }
                    )}
                    {...(basicDetailsTx?.fieldNames?.RELIGION && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        basicDetailsTx?.fieldNames?.RELIGION?.olD_VALUE === null
                          ? "-"
                          : basicDetailsTx?.fieldNames?.RELIGION?.olD_VALUE
                      }`,
                    })}
                    aria-placeholder="Enter Religion"
                    // value={formik.values.religion}
                    {...formik.getFieldProps("religion")}
                    name="religion"
                    // onChange={(e) => {
                    //   const selectedValue = e.target.value;
                    //   const selectedOption = religions.find(
                    //     (religion) => religion.code === selectedValue
                    //   );

                    //   formik.setFieldValue("religion", selectedValue);
                    //   formik.setFieldValue(
                    //     "religion_val",
                    //     selectedOption?.descr || ""
                    //   );
                    // }}
                  >
                    <option value=""></option>
                    {religions.map((religion) => (
                      <option key={religion.code} value={religion.code}>
                        {religion.descr}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.religion && formik.errors.religion && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.religion}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="blooD_GRP"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    BLOOD GROUP:
                  </label>

                  <select
                    id="blooD_GRP"
                    // className="form-select form-select-lg form-select-solid"
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          basicDetailsTx?.fieldNames?.BLOOD_GRP?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.blooD_GRP && formik.errors.blooD_GRP,
                      },
                      {
                        "is-valid":
                          formik.touched.blooD_GRP && !formik.errors.blooD_GRP,
                      }
                    )}
                    {...(basicDetailsTx?.fieldNames?.BLOOD_GRP && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        basicDetailsTx?.fieldNames?.BLOOD_GRP?.olD_VALUE ===
                        null
                          ? "-"
                          : basicDetailsTx?.fieldNames?.BLOOD_GRP?.olD_VALUE
                      }`,
                    })}
                    aria-placeholder="Enter Payment Type"
                    // value={formik.values.blooD_GRP}
                    // onChange={formik.handleChange}
                    {...formik.getFieldProps("blooD_GRP")}
                    name="blooD_GRP"
                  >
                    <option value=""></option>
                    {bloodGrps.map((bldGrp) => (
                      <option key={bldGrp.code} value={bldGrp.code}>
                        {bldGrp.descr}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.blooD_GRP && formik.errors.blooD_GRP && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.blooD_GRP}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="fatheR_NAME"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    FATHER NAME:
                  </label>
                  {/* {basicDetailsTx?.fieldNames?.FATHER_NAME && (<OverlayTrigger
                    placement="top"
                    delay={{ show: 250, hide: 400 }}
                    overlay={(props) => (
                      <Tooltip id="button-tooltip" {...props}>
                       OLD VALUE : {basicDetailsTx?.fieldNames?.FATHER_NAME?.olD_VALUE}
                      </Tooltip>
                    )}
                  > */}
                  <input
                    id="fatheR_NAME"
                    // className="form-select form-select-lg form-select-solid "
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          basicDetailsTx?.fieldNames?.FATHER_NAME?.neW_KEY,
                      },
                      {
                        "is-invalid":
                          formik.touched.fatheR_NAME &&
                          formik.errors.fatheR_NAME,
                      },
                      {
                        "is-valid":
                          formik.touched.fatheR_NAME &&
                          !formik.errors.fatheR_NAME,
                      }
                    )}
                    {...(basicDetailsTx?.fieldNames?.FATHER_NAME && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        basicDetailsTx?.fieldNames?.FATHER_NAME?.olD_VALUE ===
                        null
                          ? "-"
                          : basicDetailsTx?.fieldNames?.FATHER_NAME?.olD_VALUE
                      }`,
                    })}
                    type="text"
                    name="fatheR_NAME"
                    placeholder="Enter Father Name"
                    value={formik.values.fatheR_NAME}
                    onChange={formik.handleChange}
                  />
                  {/* </OverlayTrigger>)} */}
                </div>
                {formik.touched.fatheR_NAME && formik.errors.fatheR_NAME && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.fatheR_NAME}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="emaiL_ID_PER"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PERSONAL EMAIL:
                  </label>
                  <input
                    type="text"
                    id="emaiL_ID_PER"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          basicDetailsTx?.fieldNames?.EMAIL_ID_PER?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.emaiL_ID_PER &&
                          formik.errors.emaiL_ID_PER,
                      },
                      {
                        "is-valid":
                          formik.touched.emaiL_ID_PER &&
                          !formik.errors.emaiL_ID_PER,
                      }
                    )}
                    {...(basicDetailsTx?.fieldNames?.EMAIL_ID_PER && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        basicDetailsTx?.fieldNames?.EMAIL_ID_PER?.olD_VALUE ===
                        null
                          ? "-"
                          : basicDetailsTx?.fieldNames?.EMAIL_ID_PER?.olD_VALUE
                      }`,
                    })}
                    // style={{ cursor: "not-allowed" }}
                    name="emaiL_ID_PER"
                    placeholder="Enter Personal Email"
                    value={formik.values?.emaiL_ID_PER}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.emaiL_ID_PER && formik.errors.emaiL_ID_PER && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.emaiL_ID_PER}</span>
                  </div>
                )}
              </div>
              {/* end: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="BANK_IFSC"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PERSONAL MOB:
                  </label>
                  <input
                    type="number"
                    id="mobilE_NO"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          basicDetailsTx?.fieldNames?.MOBILE_NO?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.mobilE_NO && formik.errors.mobilE_NO,
                      },
                      {
                        "is-valid":
                          formik.touched.mobilE_NO && !formik.errors.mobilE_NO,
                      }
                    )}
                    {...(basicDetailsTx?.fieldNames?.MOBILE_NO && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        basicDetailsTx?.fieldNames?.MOBILE_NO?.olD_VALUE ===
                        null
                          ? "-"
                          : basicDetailsTx?.fieldNames?.MOBILE_NO?.olD_VALUE
                      }`,
                    })}
                    // style={{ cursor: "not-allowed" }}
                    name="mobilE_NO"
                    placeholder="Enter Personal Mobile Number"
                    value={formik.values?.mobilE_NO}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.mobilE_NO && formik.errors.mobilE_NO && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.mobilE_NO}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}

              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="CHG_REASON"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CHANGE REASON:
                  </label>
                  <input
                    type="text"
                    id="CHG_REASON"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "is-invalid":
                          formik.touched.CHG_REASON && formik.errors.CHG_REASON,
                      },
                      {
                        "is-valid":
                          formik.touched.CHG_REASON &&
                          !formik.errors.CHG_REASON,
                      }
                    )}
                    name="CHG_REASON"
                    placeholder="Enter Change Reason"
                    value={formik.values.CHG_REASON?.toUpperCase()}
                    //
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.CHG_REASON && formik.errors.CHG_REASON && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.CHG_REASON}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}
            </div>
            {/* end: final outer Edit Row */}
          </div>

          {/* Logic for Submit */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default BasicDetailsEdit;
