import { Content } from "../../../../_metronic/layout/components/content";
import AssetListEdit from "./AssetListEdit";

export function EditAssetList() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <AssetListEdit />
      </div>
    </Content>
  );
}
