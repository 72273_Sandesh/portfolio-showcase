// import React, { useEffect, useState } from "react";
// import axios from "axios";
// import { useNavigate } from "react-router-dom";
// import { useFormik } from "formik";
// import * as Yup from "yup";
// import { employeeIdForView, fMIdForUpdate } from "../../GlobalQuery";
// import { toast } from "react-toastify";
// import { useAuth } from "../../auth";
// interface qualifications {
//   qualification: string;
//   qlF_CODE: string;
// }

// const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// const EditFamilyDetails: React.FC = () => {
//   const [qualifications, setQualifications] = useState<qualifications[]>([]);
//   const navigate = useNavigate();
//   const {currentUser} = useAuth()

//   useEffect(() => {
//     axios
//       .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMP_FAMDET`)
//       .then((response) => {
//         formik.setValues({
//           seQ_NO: response.data[fMIdForUpdate].seQ_NO,
//           membeR_NAME: response.data[fMIdForUpdate].membeR_NAME,
//           datE_BIRTH: response.data[fMIdForUpdate].datE_BIRTH,
//           sex: response.data[fMIdForUpdate].sex,
//           relation: response.data[fMIdForUpdate].relation,
//           occupation: response.data[fMIdForUpdate].occupation,
//           dependent: response.data[fMIdForUpdate].dependent,
//           physicaL_STATUS:response.data[fMIdForUpdate].physicaL_STATUS,
//           qlF_CODE:response.data[fMIdForUpdate].qlF_CODE,
//           adopted:response.data[fMIdForUpdate].adopted,
//         });
//       })
//       .catch((error) => {
//         console.error("Error fetching basic details:", error);
//       });

//       axios
//       .get(`${EMPLOYEE_URL}/ddl_GetQualification`)
//       .then((response) => {
//         setQualifications(response.data.qualifications);
//       })
//       .catch((error) => {
//         console.error("Error fetching basic details:", error);
//       });
//   }, []);

//   const validationSchema = Yup.object().shape({
//     membeR_NAME: Yup.string().required("Member name is required"),
//     datE_BIRTH: Yup.string().required("Birth date is required"),
//     sex: Yup.string().required("Gender is required"),
//     relation: Yup.string().required("Realation with employee is required"),
//     occupation: Yup.string().required("Occupation is required"),
//     dependent: Yup.string().required("Dependent status is required"),
//     physicaL_STATUS:Yup.string().required("Physical status is required"),
//     qlF_CODE:Yup.string().required("Qualification is required"),
//     adopted:Yup.string().required("Adoption status is required"),
//   });

//   const formik = useFormik({
//     initialValues: {
//       seQ_NO: "",
//       membeR_NAME: "",
//       datE_BIRTH: "",
//       sex: "",
//       relation: "",
//       occupation: "",
//       dependent: "",
//       physicaL_STATUS:"",
//       qlF_CODE:"",
//       adopted:"",
//     },
//     validationSchema: validationSchema,

//     onSubmit: (values, formik) => {
//       console.log("Submitting form data:", values);

//       axios
//         .put(`${EMPLOYEE_URL}/UpdateFamilyMembers${employeeIdForView}/${values.seQ_NO}`, {
//           ...values,
//           chG_USER: currentUser?.first_name,
//           chG_DATE: new Date(),
//           chG_TERM: "TERM",
//         })
//         .then((response) => {
//           console.log("Post successful!", response.data);
//           const { responseStatus, message } = response.data.data;
//           if (responseStatus === "E") {
//             toast.error(
//               <div>
//                 <h4 className="alert-heading">Failed to submit Form...</h4>
//                 <p className="alert alert-danger" role="alert">
//                   <h6>
//                     {Array.isArray(message) ? message.join("\n") : message}
//                   </h6>
//                 </p>
//               </div>
//             );
//           } else {

//             toast.success(
//               <div>
//                 <h4 className="alert-heading">
//                   Successfully Submitted Form...
//                 </h4>
//                 <p className="alert alert-success" role="alert">
//                   <h6>
//                     {Array.isArray(message) ? message.join("\n") : message}
//                   </h6>
//                 </p>
//               </div>
//             );
//             formik.resetForm();
//             navigate(-1);

//           }
//         })
//         .catch((error) => {
//           console.error("Error posting data:", error);
//           toast.error(
//             <div>
//               <h6 className="alert-heading">Error Posting Data</h6>
//               <p className="alert alert-danger" role="alert">
//                 <h6>{error.message}</h6>
//               </p>
//             </div>
//           );
//         });
//     },
//   });

//   const handleClose = () => {
//     navigate(-1);
//   };

//   return (
//     <div className="card mb-1 mb-xl-2">
//       <div
//         className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
//         role="button"
//         data-bs-target="#kt_account_profile_details"
//         aria-expanded="true"
//         aria-controls="kt_account_profile_details"
//       >
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Edit Family Member Details</h3>
//         </div>
//         <button
//           type="button"
//           className="btn-close"
//           aria-label="Close"
//           onClick={handleClose}
//           style={{ marginRight: "10px", cursor: "pointer" }}
//         ></button>
//       </div>

//       <div id="kt_account_profile_details" className="collapse show">
//         <form onSubmit={formik.handleSubmit} className="form">
//           <div className="card-body border-top p-9">
//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="membeR_NAME"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   MEMBER NAME
//                 </label>
//                 <input
//                   type="text"
//                   id="membeR_NAME"
//                   className="form-control form-control-lg form-control-solid "
//                   name="membeR_NAME"
//                   placeholder="Enter Member Name"
//                   value={formik.values?.membeR_NAME}
//                   onChange={formik.handleChange}
//                 />
//               </div>

//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="relation"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   RELATION
//                 </label>
//                 <input
//                   type="text"
//                   id="relation"
//                   className="form-control form-control-lg form-control-solid "
//                   name="relation"
//                   placeholder="Enter Relation"
//                   value={formik.values?.relation}
//                   onChange={formik.handleChange}
//                 />
//               </div>
//               <div className="row mb-1">
//                 <div className="col-lg-6">
//                   {formik.touched.membeR_NAME && formik.errors.membeR_NAME ? (
//                     <div className="text-danger">
//                       {formik.errors.membeR_NAME}
//                     </div>
//                   ) : null}
//                 </div>
//                 <div className="col-lg-6">
//                   {formik.touched.relation && formik.errors.relation ? (
//                     <div className="text-danger">{formik.errors.relation}</div>
//                   ) : null}
//                 </div>
//               </div>
//             </div>
//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="dependent"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   DEPENDENCY
//                 </label>
//                 <select
//                   id="dependent"
//                   className="form-control form-control-lg form-control-solid "
//                   name="dependent"
//                   // value={formik.values?.dependent==='Y' ? "YES" : 'NO'}
//                   onChange={formik.handleChange}
//                 >
//                   <option value={formik.values?.dependent}>
//                     {formik.values?.dependent === "Y" ? "YES" : "NO"}
//                   </option>
//                   <option value="Y">YES</option>
//                   <option value="N">NO</option>
//                 </select>
//               </div>

//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="occupation"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   OCCUPATION
//                 </label>
//                 <input
//                   type="text"
//                   id="occupation"
//                   className="form-control form-control-lg form-control-solid "
//                   name="occupation"
//                   placeholder="Enter Occupation"
//                   value={formik.values?.occupation}
//                   onChange={formik.handleChange}
//                 />
//               </div>

//               <div className="row mb-1">
//                 <div className="col-lg-6">
//                   {formik.touched.dependent && formik.errors.dependent ? (
//                     <div className="text-danger">{formik.errors.dependent}</div>
//                   ) : null}
//                 </div>
//                 <div className="col-lg-6">
//                   {formik.touched.occupation && formik.errors.occupation ? (
//                     <div className="text-danger">
//                       {formik.errors.occupation}
//                     </div>
//                   ) : null}
//                 </div>
//               </div>
//             </div>
//             {/* End: Edit Row */}
//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="datE_BIRTH"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   BIRTH DATE
//                 </label>
//                 <input
//                   type="date"
//                   id="datE_BIRTH"
//                   className="form-control form-control-lg form-control-solid "
//                   name="datE_BIRTH"
//                   placeholder="Enter Birth Date"
//                   value={
//                     formik.values?.datE_BIRTH
//                       ? formik.values.datE_BIRTH.split("T")[0]
//                       : ""
//                   }
//                   onChange={formik.handleChange}
//                 />
//               </div>

//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="sex"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   GENDER
//                 </label>
//                 <select
//                   id="sex"
//                   className="form-select form-select-lg form-select-solid"
//                   name="sex"
//                   onChange={formik.handleChange}
//                 >
//                   <option value={formik.values?.sex}>
//                     {formik.values?.sex === "M" ? "MALE" : "FEMALE"}
//                   </option>
//                   <option value="M">MALE</option>
//                   <option value="F">FEMALE</option>
//                   <option value="F">OTHER</option>
//                 </select>
//               </div>
//               <div className="row mb-1">
//                 <div className="col-lg-6">
//                   {formik.touched.datE_BIRTH && formik.errors.datE_BIRTH ? (
//                     <div className="text-danger">
//                       {formik.errors.datE_BIRTH}
//                     </div>
//                   ) : null}
//                 </div>
//                 <div className="col-lg-6">
//                   {formik.touched.sex && formik.errors.sex ? (
//                     <div className="text-danger">{formik.errors.sex}</div>
//                   ) : null}
//                 </div>
//               </div>
//             </div>
//             {/* End: Edit Row */}
//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//             <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="physicaL_STATUS"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   PHYSICAL STATUS
//                 </label>
//                 <select
//                   id="physicaL_STATUS"
//                   className="form-select form-select-lg form-select-solid"
//                   name="physicaL_STATUS"
//                   onChange={formik.handleChange}
//                 >
//                 <option value={formik.values?.physicaL_STATUS}>
//                   {formik.values?.physicaL_STATUS === "N" ? "ACTIVE" : formik.values?.adopted === "Y" ? "DISABLED" : ""}
//                 </option>
//                 <option value="N">ACTIVE</option>
//                 <option value="Y">DISABLED</option>
//                 <option value="O">PREFER NOT TO DESCRIBE</option>
//                 </select>
//               </div>

//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="qlF_CODE"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   QUALIFICATION
//                 </label>
//                 <select
//                   id="qlF_CODE"
//                   className="form-select form-select-lg form-select-solid"
//                   name="qlF_CODE"
//                   onChange={formik.handleChange}
//                 >
//                   <option value={formik.values?.qlF_CODE}>
//                     {formik.values?.qlF_CODE}
//                   </option>
//                   {qualifications.map((qual) => (
//                     <option value={qual.qlF_CODE}>{qual.qualification}</option>
//                   ))}
//                 </select>
//               </div>
//               <div className="row mb-1">
//                 <div className="col-lg-6">
//                   {formik.touched.physicaL_STATUS && formik.errors.physicaL_STATUS ? (
//                     <div className="text-danger">
//                       {formik.errors.physicaL_STATUS}
//                     </div>
//                   ) : null}
//                 </div>
//                 <div className="col-lg-6">
//                   {formik.touched.qlF_CODE && formik.errors.qlF_CODE ? (
//                     <div className="text-danger">{formik.errors.qlF_CODE}</div>
//                   ) : null}
//                 </div>
//               </div>
//             </div>
//             {/* End: Edit Row */}
//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//             <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="adopted"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   ADOPTED ?
//                 </label>
//                 <select
//                   id="adopted"
//                   className="form-select form-select-lg form-select-solid"
//                   name="adopted"
//                   onChange={formik.handleChange}
//                 >
//                   <option value={formik.values?.adopted}>
//                     {formik.values?.adopted === "Y" ? "YES" : formik.values?.adopted === "N" ? "NO" : ""}
//                   </option>
//                   <option value="Y">YES</option>
//                   <option value="N">NO</option>
//                 </select>
//               </div>
//               <div className="row mb-1">
//                 <div className="col-lg-6">
//                   {formik.touched.adopted && formik.errors.adopted ? (
//                     <div className="text-danger">
//                       {formik.errors.adopted}
//                     </div>
//                   ) : null}
//                 </div>
//               </div>
//             </div>
//             {/* End: Edit Row */}
//           </div>

//           {/* Logic for Submit */}
//           <div className="card-footer d-flex justify-content-center py-6 px-9">
//             <button
//               type="button"
//               className="btn btn-light me-2"
//               onClick={handleClose}
//             >
//               <span className="indicator-label">Discard</span>
//             </button>

//             <button
//               type="submit"
//               className="btn btn-primary"
//               disabled={//formik.isSubmitting ||
//                  !formik.isValid}
//             >
//               <span className="indicator-label">Submit</span>
//             </button>
//           </div>
//         </form>
//       </div>
//     </div>
//   );
// };

// export default EditFamilyDetails;

import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  employeeIdForView,
  fMIdForUpdate,
  setFMIdForUpdate,
} from "../../GlobalQuery";
import { KTIcon, Mode, QUERIES } from "../../../../_metronic/helpers";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
import { useListView } from "../../apps/transaction-master/master-list/core/ListViewProvider";
import clsx from "clsx";
import { useMutation, useQueryClient } from "react-query";
import {
  insertTransaction,
  insertTransactionV2,
} from "../../apps/transaction-master/master-list/core/_requests";
import { useQueryResponse } from "../../apps/employee/employee-list/core/QueryResponseProvider";

interface Qualification {
  qualification: string;
  qlF_CODE: string;
}

type FieldDetail = {
  olD_KEY: string;
  olD_VALUE: string;
  neW_KEY: string;
  neW_VALUE: string;
  FILE_PATH: string;
};

type FamilyMember = {
  emP_CODE: string;
  seQ_NO: string;
  membeR_NAME: string;
  datE_BIRTH: string;
  relation: string;
  occupation: string;

  sex: string;
  sex_val: string;

  dependent: string;
  dependent_val: string;

  physicaL_STATUS: string;
  physicaL_STATUS_val: string;

  qlF_CODE: string;
  qlF_CODE_val: string;

  adopted: string;
  adopted_val: string;

  chG_USER: string;
  chG_DATE: string;
  chG_TERM: string;
  CHG_REASON: string;

  fieldNames?: {
    EMP_CODE: FieldDetail;
    SEQ_NO: FieldDetail;
    MEMBER_NAME: FieldDetail;
    DATE_BIRTH: FieldDetail;
    RELATION: FieldDetail;
    OCCUPATION: FieldDetail;
    SEX: FieldDetail;
    DEPENDENT: FieldDetail;
    PHYSICAL_STATUS: FieldDetail;
    QLF_CODE: FieldDetail;
    ADOPTED: FieldDetail;
    CHG_USER: FieldDetail;
    CHG_DATE: FieldDetail;
    CHG_TERM: FieldDetail;
    CHG_REASON: FieldDetail;
  };
};

const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const EditFamilyDetails: React.FC = () => {
  const { currentUser } = useAuth();
  const [privateIP, setPrivateIP] = useState();

  const { itemIdForUpdate, setItemIdForUpdate, formMode } = useListView();
  const [empCodeTxForView, setEmpCodeTxForView] = useState();
  const [seqNo, setSeqNo] = useState(fMIdForUpdate);

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();
  const navigate = useNavigate();

  const [image] = useState<string>("media/avatars/User.png");
  // const [pdf, setPdf] = useState<string>("");

  const [familyMember, setFamilyMember] = useState<FamilyMember>();
  const [familyMemberTx, setFamilyMemberTx] = useState<FamilyMember>();

  const [qualifications, setQualifications] = useState<Qualification[]>([]);
  const [genders] = useState([
    { code: "F", descr: "FEMALE" },
    { code: "M", descr: "MALE" },
  ]);

  const [yesNos] = useState([
    { code: "Y", descr: "YES" },
    { code: "N", descr: "NO" },
  ]);

  const [physicalStatuses] = useState([
    { code: "Y", descr: "ACTIVE" },
    { code: "N", descr: "DIFFRENTLY ABLED" },
  ]);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching Ip Address:", error));

    itemIdForUpdate && fetchFamilyMemberTx();
    seqNo !== 0 && fetchFamilyMember();
  }, [empCodeTxForView, seqNo, itemIdForUpdate]);

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/ddl_GetQualification`)
      .then((response) => {
        setQualifications(response.data.qualifications);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const fetchFamilyMember = () => {
    axios
      .get(
        `${EMPLOYEE_URL}/GetFamilyMemberByID?empCode=${employeeIdForView}&seqNo=${seqNo}`
      )
      .then((response) => {
        setFamilyMember(response.data);
      })
      .catch((error) => {
        console.error("Error fetching BASIC details:", error);
      });
  };

  const fetchFamilyMemberTx = () => {
    axios
      .get(`${MASTERTX_URL}/GetTransactionByID?tranId=${itemIdForUpdate}`)
      .then((response) => {
        setEmpCodeTxForView(
          response.data.transactions[0].fieldNames?.EMP_CODE?.neW_VALUE
        );
        setSeqNo(response.data.transactions[0].fieldNames?.SEQ_NO?.olD_VALUE);
        setFamilyMemberTx(response.data.transactions[0]);
      });
  };

  useEffect(() => {
    formik.setValues({
      emP_CODE:
        familyMemberTx?.fieldNames?.EMP_CODE?.neW_KEY ||
        familyMember?.emP_CODE ||
        employeeIdForView ||
        "",
      emP_CODE_O:
        familyMemberTx?.fieldNames?.EMP_CODE?.olD_KEY ||
        familyMember?.emP_CODE ||
        employeeIdForView ||
        "",

      seQ_NO: familyMemberTx?.fieldNames?.SEQ_NO?.neW_KEY || "",
      seQ_NO_O:
        familyMemberTx?.fieldNames?.SEQ_NO?.olD_KEY ||
        familyMember?.seQ_NO ||
        "",

      membeR_NAME:
        familyMemberTx?.fieldNames?.MEMBER_NAME?.neW_KEY ||
        familyMember?.membeR_NAME ||
        "",
      membeR_NAME_O:
        familyMemberTx?.fieldNames?.MEMBER_NAME?.olD_KEY ||
        familyMember?.membeR_NAME ||
        "",

      datE_BIRTH:
        familyMemberTx?.fieldNames?.DATE_BIRTH?.neW_KEY ||
        familyMember?.datE_BIRTH ||
        "",
      datE_BIRTH_O:
        familyMemberTx?.fieldNames?.DATE_BIRTH?.olD_KEY ||
        familyMember?.datE_BIRTH ||
        "",

      relation:
        familyMemberTx?.fieldNames?.RELATION?.neW_KEY ||
        familyMember?.relation ||
        "",
      relation_O:
        familyMemberTx?.fieldNames?.RELATION?.olD_KEY ||
        familyMember?.relation ||
        "",

      occupation:
        familyMemberTx?.fieldNames?.OCCUPATION?.neW_KEY ||
        familyMember?.occupation ||
        "",
      occupation_O:
        familyMemberTx?.fieldNames?.OCCUPATION?.olD_KEY ||
        familyMember?.occupation ||
        "",

      sex: familyMemberTx?.fieldNames?.SEX?.neW_KEY || familyMember?.sex || "",
      sex_O:
        familyMemberTx?.fieldNames?.SEX?.olD_KEY || familyMember?.sex || "",

      sex_val:
        familyMemberTx?.fieldNames?.SEX?.neW_VALUE || familyMember?.sex === "M"
          ? "MALE"
          : familyMember?.sex === "F"
          ? "FEMALE"
          : "",
      sex_val_O:
        familyMemberTx?.fieldNames?.SEX?.olD_VALUE || familyMember?.sex === "M"
          ? "MALE"
          : familyMember?.sex === "F"
          ? "FEMALE"
          : "",

      dependent:
        familyMemberTx?.fieldNames?.DEPENDENT?.neW_KEY ||
        familyMember?.dependent ||
        "",
      dependent_O:
        familyMemberTx?.fieldNames?.DEPENDENT?.olD_KEY ||
        familyMember?.dependent ||
        "",

      dependent_val:
        familyMemberTx?.fieldNames?.DEPENDENT?.neW_VALUE ||
        familyMember?.dependent === "Y"
          ? "YES"
          : familyMember?.dependent === "N"
          ? "NO"
          : "",
      dependent_val_O:
        familyMemberTx?.fieldNames?.DEPENDENT?.olD_VALUE ||
        familyMember?.dependent === "Y"
          ? "YES"
          : familyMember?.dependent === "N"
          ? "NO"
          : "",

      physicaL_STATUS:
        familyMemberTx?.fieldNames?.PHYSICAL_STATUS?.neW_KEY ||
        familyMember?.physicaL_STATUS ||
        "",
      physicaL_STATUS_O:
        familyMemberTx?.fieldNames?.PHYSICAL_STATUS?.olD_KEY ||
        familyMember?.physicaL_STATUS ||
        "",

      physicaL_STATUS_val:
        familyMemberTx?.fieldNames?.PHYSICAL_STATUS?.neW_VALUE ||
        familyMember?.physicaL_STATUS === "Y"
          ? "ACTIVE"
          : familyMember?.physicaL_STATUS === "N"
          ? "DISABLED"
          : "",
      physicaL_STATUS_val_O:
        familyMemberTx?.fieldNames?.PHYSICAL_STATUS?.olD_VALUE ||
        familyMember?.physicaL_STATUS === "Y"
          ? "ACTIVE"
          : familyMember?.physicaL_STATUS === "N"
          ? "DISABLED"
          : "",

      qlF_CODE:
        familyMemberTx?.fieldNames?.QLF_CODE?.neW_KEY ||
        familyMember?.qlF_CODE ||
        "",
      qlF_CODE_O:
        familyMemberTx?.fieldNames?.QLF_CODE?.olD_KEY ||
        familyMember?.qlF_CODE ||
        "",

      qlF_CODE_val:
        familyMemberTx?.fieldNames?.QLF_CODE?.neW_VALUE ||
        familyMember?.qlF_CODE ||
        "",
      qlF_CODE_val_O:
        // familyMemberTx?.fieldNames?.QLF_CODE?.olD_VALUE ||
        qualifications.find(
          (qual) => qual.qlF_CODE === familyMember?.qlF_CODE
        )?.qualification ||
        "",

      adopted:
        familyMemberTx?.fieldNames?.ADOPTED?.neW_KEY ||
        familyMember?.adopted ||
        "",
      adopted_O:
        familyMemberTx?.fieldNames?.ADOPTED?.olD_KEY ||
        familyMember?.adopted ||
        "",

      adopted_val:
        familyMemberTx?.fieldNames?.ADOPTED?.neW_VALUE ||
        familyMember?.adopted === "Y"
          ? "YES"
          : familyMember?.dependent === "N"
          ? "NO"
          : "",
      adopted_val_O:
        familyMemberTx?.fieldNames?.ADOPTED?.olD_VALUE ||
        familyMember?.adopted === "Y"
          ? "YES"
          : familyMember?.dependent === "N"
          ? "NO"
          : "",

      chG_USER:
        familyMemberTx?.fieldNames?.CHG_USER?.neW_KEY ||
        familyMember?.chG_USER ||
        "",
      chG_USER_O:
        familyMemberTx?.fieldNames?.CHG_USER?.olD_KEY ||
        familyMember?.chG_USER ||
        "",

      chG_DATE:
        familyMemberTx?.fieldNames?.CHG_DATE?.neW_KEY ||
        familyMember?.chG_DATE ||
        "",
      chG_DATE_O:
        familyMemberTx?.fieldNames?.CHG_DATE?.olD_KEY ||
        familyMember?.chG_DATE ||
        "",

      chG_TERM:
        familyMemberTx?.fieldNames?.CHG_TERM?.neW_KEY ||
        familyMember?.chG_TERM ||
        "",
      chG_TERM_O:
        familyMemberTx?.fieldNames?.CHG_TERM?.olD_KEY ||
        familyMember?.chG_TERM ||
        "",

      CHG_REASON:
        fMIdForUpdate !== 0 || formMode === Mode.EDIT
          ? ""
          : familyMember?.CHG_REASON,
    });
  }, [familyMember, familyMemberTx]);

  const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    formik.setFieldValue("emP_IMAGE", e.target.files?.[0]);
    const file = e.target.files?.[0];
    if (file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        const base64String = reader.result as string;
        formik.setFieldValue("emP_IMAGE", base64String);
      };
      reader.readAsDataURL(file);
    }
  };

  const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (file) {
      const reader = new FileReader();

      reader.onloadend = () => {
        const base64String = reader.result;
        console.log(base64String);
        formik.setFieldValue("datE_BIRTH_FILE", base64String);
      };
      reader.readAsDataURL(file);
    }
  };

  const validationSchema = Yup.object().shape({
    membeR_NAME: Yup.string().required("Member name is required"),
    datE_BIRTH: Yup.string().required("Birth date is required"),
    sex: Yup.string().required("Gender is required"),
    relation: Yup.string().required("Realation with employee is required"),
    occupation: Yup.string().required("Occupation is required"),
    dependent: Yup.string().required("Dependent status is required"),
    physicaL_STATUS: Yup.string().required("Physical status is required"),
    qlF_CODE: Yup.string().required("Qualification is required"),
    adopted: Yup.string().required("Adoption status is required"),
    CHG_REASON: Yup.string().when([], {
      is: () => fMIdForUpdate !== 0 || formMode === Mode.EDIT,
      then: (schema) =>
        schema
          .trim()
          .required("Change Reason is required")
          .min(5, "Minimum 5 characters"),
      otherwise: (schema) => schema.notRequired(),
    }),
    // datE_BIRTH_FILE: Yup.string().when([], {
    //   is: () => familyMember?.datE_BIRTH !== formik.values.datE_BIRTH,
    //   then: (schema) => schema.required("Proof attachment is required"),
    //   otherwise: (schema) => schema.notRequired(),
    // }),
  });

  const createTx = useMutation((payload: any) => insertTransactionV2(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        handleClose();
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formik = useFormik({
    initialValues: {
      emP_CODE: "",
      emP_CODE_O: "",

      seQ_NO: "",
      seQ_NO_O: "",

      membeR_NAME: "",
      membeR_NAME_O: "",

      datE_BIRTH: "",
      datE_BIRTH_O: "",

      relation: "",
      relation_O: "",

      occupation: "",
      occupation_O: "",

      sex: "",
      sex_O: "",

      sex_val: "",
      sex_val_O: "",

      dependent: "",
      dependent_O: "",

      dependent_val: "",
      dependent_val_O: "",

      physicaL_STATUS: "",
      physicaL_STATUS_O: "",

      physicaL_STATUS_val: "",
      physicaL_STATUS_val_O: "",

      qlF_CODE: "",
      qlF_CODE_O: "",

      qlF_CODE_val: "",
      qlF_CODE_val_O: "",

      adopted: "",
      adopted_O: "",

      adopted_val: "",
      adopted_val_O: "",

      chG_USER: "",
      chG_USER_O: "",

      chG_DATE: new Date().toString(),
      chG_DATE_O: "",

      chG_TERM: "",
      chG_TERM_O: "",

      CHG_REASON: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values, formik) => {
      values.chG_USER = currentUser?.id?.toString() || "";
      values.chG_TERM = privateIP || "";
      const payload = {
        values,

        TABLENAME: "EMPLOYEE_FAMILY",
        KEYVAR: "",
        WFLEVEL: 1,
        CONF: "N",
        SITECODE: "S0001",
        EMP: currentUser?.id?.toString() || "",
        FILEPATH: null,
        FILE: null,
        FOLDERPATH: null,
        FORMNAME: "Family Details",
        tX_CHG_USER: currentUser?.id?.toString() || "",
        tX_CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        STRTRANID: itemIdForUpdate ? itemIdForUpdate : null,
      };

      createTx.mutate(payload);
      formik.setSubmitting(false);
    },
  });

  const handleClose = () => {
    setFMIdForUpdate(0);
    itemIdForUpdate ? setItemIdForUpdate(undefined) : navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">
            {fMIdForUpdate !== 0 || formMode === Mode.EDIT ? "Edit" : "Add"}{" "}
            Family Member Details
          </h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative d-flex flex-column align-items-center">
              <div className="d-flex justify-content-center align-items-center mb-3 position-relative">
                <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative d-flex position-relative">
                  
                  <img
                    src={formik.values.emP_IMAGE}
                    alt={formik.errors.emP_IMAGE || "NO Image Chosen"}
                    className="max-width-100 mh-100 faded-image border border-dashed border-gray-300 rounded p-5"
                    onClick={() =>
                      // document.getElementById("upload-button").click()
                      {
                        const uploadButton =
                          document.getElementById("upload-button");
                        if (uploadButton) {
                          uploadButton.click();
                        } else {
                          console.error("Upload button not found.");
                        }
                      }
                    }
                  />
                  <label
                    htmlFor="upload-button"
                    className="btn btn-sm position-absolute top-0 end-0"
                    style={{ transform: "translate(50%, -50%)" }} // Adjust the position slightly
                  >
                    <KTIcon iconName="pencil" className="fs-2x text-dark" />
                  </label>
                  <input
                    id="upload-button"
                    type="file"
                    accept="image/*"
                    onChange={handleImageChange}
                    style={{ display: "none" }}
                    alt="/media/avatars/User.png"
                  />
                </div>
              </div>
              <style>
                {`
                  .faded-image {
                    opacity: 0.5;
                    transition: opacity 0.3s;
                  }
                  .faded-image:hover {
                    opacity: 1;
                  }
                `}
              </style>
            </div> */}

            {/* Begin: final outer Edit Row */}
            <div className="row mb-1">
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="membeR_NAME"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    MEMBER NAME:
                  </label>
                  <input
                    // className="form-select form-select-lg form-select-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          familyMemberTx?.fieldNames?.MEMBER_NAME?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.membeR_NAME &&
                          formik.errors.membeR_NAME,
                      },
                      {
                        "is-valid":
                          formik.touched.membeR_NAME &&
                          !formik.errors.membeR_NAME,
                      }
                    )}
                    {...(familyMemberTx?.fieldNames?.MEMBER_NAME && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        familyMemberTx?.fieldNames?.MEMBER_NAME?.olD_VALUE ===
                        null
                          ? "-"
                          : familyMemberTx?.fieldNames?.MEMBER_NAME?.olD_VALUE
                      }`,
                    })}
                    name="membeR_NAME"
                    placeholder="Enter family member name"
                    type="text"
                    value={formik.values?.membeR_NAME.toUpperCase()}
                    // {...formik.getFieldProps("membeR_NAME")}
                    onChange={formik.handleChange}
                  />
                </div>
                {formik.touched.membeR_NAME && formik.errors.membeR_NAME && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.membeR_NAME}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="relation"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    RELATION:
                  </label>
                  <input
                    type="text"
                    id="relation"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          familyMemberTx?.fieldNames?.RELATION?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.relation && formik.errors.relation,
                      },
                      {
                        "is-valid":
                          formik.touched.relation && !formik.errors.relation,
                      }
                    )}
                    {...(familyMemberTx?.fieldNames?.RELATION && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        familyMemberTx?.fieldNames?.RELATION?.olD_VALUE === null
                          ? "-"
                          : familyMemberTx?.fieldNames?.RELATION?.olD_VALUE
                      }`,
                    })}
                    name="relation"
                    placeholder="Enter relation with employee"
                    value={formik.values.relation?.toUpperCase()}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.relation && formik.errors.relation && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.relation}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="qlF_CODE"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    QUALIFICATION:
                  </label>
                  <select
                    id="qlF_CODE"
                    // className="form-select form-select-lg form-select-solid "
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          familyMemberTx?.fieldNames?.QLF_CODE?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.qlF_CODE && formik.errors.qlF_CODE,
                      },
                      {
                        "is-valid":
                          formik.touched.qlF_CODE && !formik.errors.qlF_CODE,
                      }
                    )}
                    {...(familyMemberTx?.fieldNames?.QLF_CODE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        familyMemberTx?.fieldNames?.QLF_CODE?.olD_VALUE === null
                          ? "-"
                          : familyMemberTx?.fieldNames?.QLF_CODE?.olD_VALUE
                      }`,
                    })}
                    aria-placeholder="Enter qlF_CODE"
                    // value={formik.values.qlF_CODE}
                    {...formik.getFieldProps("qlF_CODE")}
                    name="qlF_CODE"
                    onChange={(e) => {
                      const selectedValue = e.target.value;
                      const selectedOption = qualifications.find(
                        (qual) => qual.qlF_CODE === selectedValue
                      );

                      formik.setFieldValue("qlF_CODE", selectedValue);
                      formik.setFieldValue(
                        "qlF_CODE_val",
                        selectedOption?.qualification || ""
                      );
                    }}
                  >
                    <option value=""></option>
                    {qualifications.map((qual) => (
                      <option key={qual.qlF_CODE} value={qual.qlF_CODE}>
                        {qual.qualification}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.qlF_CODE && formik.errors.qlF_CODE && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.qlF_CODE}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="occupation"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    OCCUPATION:
                  </label>
                  <input
                    type="text"
                    id="occupation"
                    name="occupation"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          familyMemberTx?.fieldNames?.OCCUPATION?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.occupation && formik.errors.occupation,
                      },
                      {
                        "is-valid":
                          formik.touched.occupation &&
                          !formik.errors.occupation,
                      }
                    )}
                    {...(familyMemberTx?.fieldNames?.OCCUPATION && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        familyMemberTx?.fieldNames?.OCCUPATION?.olD_VALUE ===
                        null
                          ? "-"
                          : familyMemberTx?.fieldNames?.OCCUPATION?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter occupation"
                    value={formik.values.occupation.toUpperCase()}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.occupation && formik.errors.occupation && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.occupation}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="datE_BIRTH"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    BIRTH DATE:
                  </label>
                  <input
                    type="date"
                    id="datE_BIRTH"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid me-2",
                      {
                        "bg-light-warning":
                          familyMemberTx?.fieldNames?.DATE_BIRTH?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.datE_BIRTH && formik.errors.datE_BIRTH,
                      },
                      {
                        "is-valid":
                          formik.touched.datE_BIRTH &&
                          !formik.errors.datE_BIRTH,
                      }
                    )}
                    {...(familyMemberTx?.fieldNames?.DATE_BIRTH && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        familyMemberTx?.fieldNames?.DATE_BIRTH?.olD_VALUE ===
                        null
                          ? "-"
                          : familyMemberTx?.fieldNames?.DATE_BIRTH?.olD_VALUE
                      }`,
                    })}
                    name="datE_BIRTH"
                    placeholder="Enter Birth Date"
                    // value={formik.values.datE_BIRTH?.toUpperCase()}
                    value={
                      formik.values?.datE_BIRTH
                        ? new Date(formik.values?.datE_BIRTH)
                            .toISOString()
                            .split("T")[0]
                        : ""
                    }
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {/* <input
                    className={clsx(
                      "ps-2 form-control form-control-lg",
                      {
                        "bg-light-warning":
                          familyMemberTx?.fieldNames?.DATE_BIRTH?.FILE_PATH,
                      },
                      {
                        "is-invalid":
                          formik.touched.datE_BIRTH_FILE &&
                          formik.errors.datE_BIRTH_FILE,
                      },
                      {
                        "is-valid":
                          formik.touched.datE_BIRTH_FILE &&
                          !formik.errors.datE_BIRTH_FILE,
                      }
                    )}
                    name="datE_BIRTH_FILE"
                    type="file"
                    id="datE_BIRTH_FILE"
                    onChange={handlePdfChange}
                    accept="application/pdf"
                  /> */}
                </div>
                <>
                  {/* {formik.touched.datE_BIRTH_FILE &&
                    formik.errors.datE_BIRTH_FILE && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert">
                          {formik.errors.datE_BIRTH_FILE}
                        </span>
                      </div>
                    )} */}
                  {formik.touched.datE_BIRTH && formik.errors.datE_BIRTH && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.datE_BIRTH}</span>
                    </div>
                  )}
                </>
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="sex"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    GENDER:
                  </label>
                  <select
                    id="sex"
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          familyMemberTx?.fieldNames?.SEX?.neW_VALUE,
                      },
                      {
                        "is-invalid": formik.touched.sex && formik.errors.sex,
                      },
                      {
                        "is-valid": formik.touched.sex && !formik.errors.sex,
                      }
                    )}
                    {...(familyMemberTx?.fieldNames?.SEX && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        familyMemberTx?.fieldNames?.SEX?.olD_VALUE === null
                          ? "-"
                          : familyMemberTx?.fieldNames?.SEX?.olD_VALUE
                      }`,
                    })}
                    area-placeholder="Enter DD Payable Bank"
                    // value={formik.values.sex?.toUpperCase()}
                    // onChange={formik.handleChange}
                    {...formik.getFieldProps("sex")}
                    name="sex"
                    onChange={(e) => {
                      const selectedValue = e.target.value;
                      const selectedOption = genders.find(
                        (sex) => sex.code === selectedValue
                      );

                      formik.setFieldValue("sex", selectedValue);
                      formik.setFieldValue(
                        "sex_val",
                        selectedOption?.descr.toUpperCase() || ""
                      );
                    }}
                    onBlur={formik.handleBlur}
                  >
                    <option value=""></option>
                    {genders.map((sex) => (
                      <option key={sex.code} value={sex.code}>
                        {sex.descr}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.sex && formik.errors.sex && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.sex}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="physicaL_STATUS"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PHYSICAL STATUS:
                  </label>

                  <select
                    id="physicaL_STATUS"
                    // className="form-select form-select-lg form-select-solid"
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          familyMemberTx?.fieldNames?.PHYSICAL_STATUS
                            ?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.physicaL_STATUS &&
                          formik.errors.physicaL_STATUS,
                      },
                      {
                        "is-valid":
                          formik.touched.physicaL_STATUS &&
                          !formik.errors.physicaL_STATUS,
                      }
                    )}
                    {...(familyMemberTx?.fieldNames?.PHYSICAL_STATUS && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        familyMemberTx?.fieldNames?.PHYSICAL_STATUS
                          ?.olD_VALUE === null
                          ? "-"
                          : familyMemberTx?.fieldNames?.PHYSICAL_STATUS
                              ?.olD_VALUE
                      }`,
                    })}
                    aria-placeholder="Enter Payment Type"
                    // value={formik.values.physicaL_STATUS}
                    // onChange={formik.handleChange}
                    {...formik.getFieldProps("physicaL_STATUS")}
                    onChange={(e) => {
                      const selectedValue = e.target.value;
                      const selectedOption = physicalStatuses.find(
                        (status) => status.code === selectedValue
                      );

                      formik.setFieldValue("physicaL_STATUS", selectedValue);
                      formik.setFieldValue(
                        "physicaL_STATUS_val",
                        selectedOption?.descr.toUpperCase() || ""
                      );
                    }}
                    name="physicaL_STATUS"
                  >
                    <option value=""></option>
                    {physicalStatuses.map((status) => (
                      <option key={status.code} value={status.code}>
                        {status.descr}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.physicaL_STATUS &&
                  formik.errors.physicaL_STATUS && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.physicaL_STATUS}</span>
                    </div>
                  )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="dependent"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    IS DEPENDENT ?:
                  </label>
                  <select
                    id="dependent"
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          familyMemberTx?.fieldNames?.DEPENDENT?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.dependent && formik.errors.dependent,
                      },
                      {
                        "is-valid":
                          formik.touched.dependent && !formik.errors.dependent,
                      }
                    )}
                    {...(familyMemberTx?.fieldNames?.DEPENDENT && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        familyMemberTx?.fieldNames?.DEPENDENT?.olD_VALUE ===
                        null
                          ? "-"
                          : familyMemberTx?.fieldNames?.DEPENDENT?.olD_VALUE
                      }`,
                    })}
                    area-placeholder="Enter DD Payable Bank"
                    // value={formik.values.dependent?.toUpperCase()}
                    // onChange={formik.handleChange}
                    {...formik.getFieldProps("dependent")}
                    name="dependent"
                    onChange={(e) => {
                      const selectedValue = e.target.value;
                      const selectedOption = yesNos.find(
                        (dependency) => dependency.code === selectedValue
                      );

                      formik.setFieldValue("dependent", selectedValue);
                      formik.setFieldValue(
                        "dependent_val",
                        selectedOption?.descr.toUpperCase() || ""
                      );
                    }}
                    onBlur={formik.handleBlur}
                  >
                    <option value=""></option>
                    {yesNos.map((dependency) => (
                      <option key={dependency.code} value={dependency.code}>
                        {dependency.descr}
                      </option>
                    ))}
                  </select>
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.dependent && formik.errors.dependent && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.dependent}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="adopted"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    ADOPTION STATUS:
                  </label>
                  <select
                    id="adopted"
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          familyMemberTx?.fieldNames?.ADOPTED?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.adopted && formik.errors.adopted,
                      },
                      {
                        "is-valid":
                          formik.touched.adopted && !formik.errors.adopted,
                      }
                    )}
                    {...(familyMemberTx?.fieldNames?.ADOPTED && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        familyMemberTx?.fieldNames?.ADOPTED?.olD_VALUE === null
                          ? "-"
                          : familyMemberTx?.fieldNames?.ADOPTED?.olD_VALUE
                      }`,
                    })}
                    area-placeholder="Enter DD Payable Bank"
                    // value={formik.values.adopted?.toUpperCase()}
                    // onChange={formik.handleChange}
                    {...formik.getFieldProps("adopted")}
                    name="adopted"
                    onChange={(e) => {
                      const selectedValue = e.target.value;
                      const selectedOption = yesNos.find(
                        (adopted) => adopted.code === selectedValue
                      );

                      formik.setFieldValue("adopted", selectedValue);
                      formik.setFieldValue(
                        "adopted_val",
                        selectedOption?.descr.toUpperCase() || ""
                      );
                    }}
                    onBlur={formik.handleBlur}
                  >
                    <option value=""></option>
                    {yesNos.map((adopted) => (
                      <option key={adopted.code} value={adopted.code}>
                        {adopted.descr}
                      </option>
                    ))}
                  </select>
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.adopted && formik.errors.adopted && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.adopted}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              {seqNo !== 0 && (
                <div className="col-lg-6">
                  <div className="d-flex align-items-center mb-1">
                    <label
                      htmlFor="CHG_REASON"
                      className="col-form-label  fw-bold fs-6 me-2"
                      style={{ minWidth: "150px" }}
                    >
                      CHANGE REASON:
                    </label>
                    <input
                      type="text"
                      id="CHG_REASON"
                      // className="form-control form-control-lg form-control-solid"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            formik.touched.CHG_REASON &&
                            formik.errors.CHG_REASON,
                        },
                        {
                          "is-valid":
                            formik.touched.CHG_REASON &&
                            !formik.errors.CHG_REASON,
                        }
                      )}
                      name="CHG_REASON"
                      placeholder="Enter Change Reason"
                      value={formik.values.CHG_REASON?.toUpperCase()}
                      //
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                  {formik.touched.CHG_REASON && formik.errors.CHG_REASON && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.CHG_REASON}</span>
                    </div>
                  )}
                </div>
              )}
              {/* End: Edit Row */}
            </div>
            {/* end: final outer Edit Row */}
          </div>

          {/* Logic for Submit */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default EditFamilyDetails;
