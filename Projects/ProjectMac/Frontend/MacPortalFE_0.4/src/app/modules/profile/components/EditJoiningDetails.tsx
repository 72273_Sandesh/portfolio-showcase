import { Content } from "../../../../_metronic/layout/components/content";
import JoiningDetailsEdit from "./JoiningDetailsEdit";

export function EditJoiningDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <JoiningDetailsEdit />
      </div>
    </Content>
  );
}
