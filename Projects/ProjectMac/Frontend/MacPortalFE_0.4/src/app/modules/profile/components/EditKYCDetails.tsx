import { Content } from "../../../../_metronic/layout/components/content";
import KYCDetailsEdit from "./KYCDetailsEdit";


export function EditKYCDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <KYCDetailsEdit />
      </div>
    </Content>
  );
}
