// // import React, { useState, useEffect } from "react";
// // import axios from "axios";
// // import { useNavigate } from "react-router-dom";
// // import { useFormik, Formik, Form, Field } from "formik";
// // import * as Yup from "yup";
// // import { employeeIdForView } from "../../GlobalQuery";
// // import { toast } from "react-toastify";

// // const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// // const EditLanguages: React.FC = () => {
// //   const navigate = useNavigate();
// //   const [languages, setLanguages] = useState([]);

// //   useEffect(() => {
// //     axios
// //       .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPLANG`)
// //       .then((response) => {
// //         setLanguages(response.data);
// //       })
// //       .catch((error) => {
// //         console.error("Error fetching basic details:", error);
// //       });
// //   }, []);

// //   const validationSchema = Yup.object().shape({
// //     languages: Yup.array().of(
// //       Yup.object().shape({
// //         language: Yup.string().required("Language is required"),
// //         worK_PROFICIENCY: Yup.string(),
// //         read: Yup.string(),
// //         write: Yup.string(),
// //         speak: Yup.string(),
// //       })
// //     ),
// //   });

// //   const handleSubmit = (values) => {
// //     console.log("Submitting form data:", values.languages);

// //     axios
// //       .post("your_backend_endpoint", {
// //         languages: values.languages,
// //       })
// //       .then((response) => {
// //         console.log("Post successful!", response.data);
// //         toast.success("Languages Edited Successfully");
// //         navigate(-1);
// //       })
// //       .catch((error) => {
// //         console.error("Error posting data:", error);
// //         toast.error("Error While Editing languages");
// //       });
// //   };

// //   const handleClose = () => {
// //     navigate(-1);
// //   };

// //   return (
// //     <div className="card mb-1 mb-xl-2">
// //       <div
// //         className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
// //         role="button"
// //         data-bs-target="#kt_account_profile_details"
// //         aria-expanded="true"
// //         aria-controls="kt_account_profile_details"
// //       >
// //         <div className="card-title m-0">
// //           <h3 className="fw-bolder m-0">Edit Language Details</h3>
// //         </div>
// //         <button
// //           type="button"
// //           className="btn-close"
// //           aria-label="Close"
// //           onClick={handleClose}
// //           style={{ marginRight: "10px", cursor: "pointer" }}
// //         ></button>
// //       </div>

// //       <div id="kt_account_profile_details" className="collapse show">
// //         <Formik
// //           initialValues={{ languages }}
// //           validationSchema={validationSchema}
// //           onSubmit={handleSubmit}
// //           enableReinitialize
// //         >
// //           {({ values}) => (
// //             <Form className="form">
// //               <div className="card-body border-top p-9">
// //                 {values.languages.map((language, index) => (
// //                   <div key={index} className="mb-3">
// //                     <h4 className="fw-bold">Language {index + 1}</h4>
// //                     <div className="row mb-1">
// //                       <div className="col-lg-6 d-flex align-items-center mb-1">
// //                         <label
// //                           htmlFor={`languages.${index}.language`}
// //                           className="col-form-label required fw-bold fs-6 me-2"
// //                           style={{ minWidth: "150px" }}
// //                         >
// //                           Language Name:
// //                         </label>
// //                         <Field
// //                           type="text"
// //                           id={`languages.${index}.language`}
// //                           className="form-control form-control-lg form-control-solid bg-light-danger"
// //                           name={`languages.${index}.language`}
// //                           placeholder="Enter language name"
// //                           readOnly
// //                         />
// //                       </div>
// //                       <div className="col-lg-6 d-flex align-items-center mb-1">
// //                         <label
// //                           htmlFor={`languages.${index}.worK_PROFICIENCY`}
// //                           className="col-form-label required fw-bold fs-6 me-2"
// //                           style={{ minWidth: "150px" }}
// //                         >
// //                           Work Proficiency:
// //                         </label>
// //                         <Field
// //                           type="text"
// //                           id={`languages.${index}.worK_PROFICIENCY`}
// //                           className="form-control form-control-lg form-control-solid"
// //                           name={`languages.${index}.worK_PROFICIENCY`}
// //                           placeholder="Enter work proficiency"
// //                         />
// //                       </div>
// //                     </div>
// //                     <div className="row mb-1">
// //                       <div className="col-lg-6 d-flex align-items-center mb-1">
// //                         <label
// //                           htmlFor={`languages.${index}.read`}
// //                           className="col-form-label required fw-bold fs-6 me-2"
// //                           style={{ minWidth: "150px" }}
// //                         >
// //                           Read:
// //                         </label>
// //                         <Field
// //                           type="text"
// //                           id={`languages.${index}.read`}
// //                           className="form-control form-control-lg form-control-solid"
// //                           name={`languages.${index}.read`}
// //                           placeholder="Enter read proficiency"
// //                         />
// //                       </div>
// //                       <div className="col-lg-6 d-flex align-items-center mb-1">
// //                         <label
// //                           htmlFor={`languages.${index}.write`}
// //                           className="col-form-label required fw-bold fs-6 me-2"
// //                           style={{ minWidth: "150px" }}
// //                         >
// //                           Write:
// //                         </label>
// //                         <Field
// //                           type="text"
// //                           id={`languages.${index}.write`}
// //                           className="form-control form-control-lg form-control-solid"
// //                           name={`languages.${index}.write`}
// //                           placeholder="Enter write proficiency"
// //                         />
// //                       </div>

// //                     </div>
// //                     <div className="row mb-1">
// //                       <div className="col-lg-6 d-flex align-items-center mb-1">
// //                         <label
// //                           htmlFor={`languages.${index}.speak`}
// //                           className="col-form-label required fw-bold fs-6 me-2"
// //                           style={{ minWidth: "150px" }}
// //                         >
// //                           Speak:
// //                         </label>
// //                         <Field
// //                           type="text"
// //                           id={`languages.${index}.speak`}
// //                           className="form-control form-control-lg form-control-solid"
// //                           name={`languages.${index}.speak`}
// //                           placeholder="Enter speak proficiency"
// //                         />
// //                       </div>
// //                     </div>
// //                   </div>
// //                 ))}
// //               </div>

// //               <div className="card-footer d-flex justify-content-center py-6 px-9">
// //                 <button
// //                   type="button"
// //                   className="btn btn-light me-2"
// //                   onClick={handleClose}
// //                 >
// //                   <span className="indicator-label">Discard</span>
// //                 </button>

// //                 <button
// //                   type="submit"
// //                   className="btn btn-primary"
// //                   disabled={!languages.length}
// //                 >
// //                   <span className="indicator-label">Submit</span>
// //                 </button>
// //               </div>
// //             </Form>
// //           )}
// //         </Formik>
// //       </div>
// //     </div>
// //   );
// // };

// // export default EditLanguages;

// // import React, { useState, useEffect } from "react";
// // import axios from "axios";
// // import { useNavigate } from "react-router-dom";
// // import { Formik, Form, Field } from "formik";
// // import * as Yup from "yup";
// // import { employeeIdForView } from "../../GlobalQuery";
// // import { toast } from "react-toastify";

// // const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// // const EditLanguages: React.FC = () => {
// //   const navigate = useNavigate();
// //   const [languages, setLanguages] = useState([]);

// //   useEffect(() => {
// //     axios
// //       .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPLANG`)
// //       .then((response) => {
// //         const languagesData = response.data.map((lang) => ({
// //           ...lang,
// //           read: lang.read === "Y",
// //           write: lang.write === "Y",
// //           speak: lang.speak === "Y",
// //         }));
// //         setLanguages(languagesData);
// //       })
// //       .catch((error) => {
// //         console.error("Error fetching basic details:", error);
// //       });
// //   }, []);

// //   const validationSchema = Yup.object().shape({
// //     languages: Yup.array().of(
// //       Yup.object().shape({
// //         language: Yup.string().required("Language is required"),
// //         worK_PROFICIENCY: Yup.string(),
// //         read: Yup.bool(),
// //         write: Yup.bool(),
// //         speak: Yup.bool(),
// //       })
// //     ),
// //   });

// //   const handleSubmit = (values) => {
// //     const formattedValues = values.languages.map((lang) => ({
// //       ...lang,
// //       read: lang.read ? "Y" : "N",
// //       write: lang.write ? "Y" : "N",
// //       speak: lang.speak ? "Y" : "N",
// //     }));

// //     axios
// //       .post("your_backend_endpoint", {
// //         languages: formattedValues,
// //       })
// //       .then((response) => {
// //         console.log("Post successful!", response.data);
// //         toast.success("Languages Edited Successfully");
// //         navigate(-1);
// //       })
// //       .catch((error) => {
// //         console.error("Error posting data:", error);
// //         toast.error("Error While Editing languages");
// //       });
// //   };

// //   const handleClose = () => {
// //     navigate(-1);
// //   };

// //   return (
// //     <div className="card mb-1 mb-xl-2">
// //       <div
// //         className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
// //         role="button"
// //         data-bs-target="#kt_account_profile_details"
// //         aria-expanded="true"
// //         aria-controls="kt_account_profile_details"
// //       >
// //         <div className="card-title m-0">
// //           <h3 className="fw-bolder m-0">Edit Language Details</h3>
// //         </div>
// //         <button
// //           type="button"
// //           className="btn-close"
// //           aria-label="Close"
// //           onClick={handleClose}
// //           style={{ marginRight: "10px", cursor: "pointer" }}
// //         ></button>
// //       </div>

// //       <div id="kt_account_profile_details" className="collapse show">
// //         <Formik
// //           initialValues={{ languages }}
// //           validationSchema={validationSchema}
// //           onSubmit={handleSubmit}
// //           enableReinitialize
// //         >
// //           {({ values }) => (
// //             <Form className="form">
// //               <div className="card-body border-top p-9">
// //                 {values.languages.map((language, index) => (
// //                   <div key={index} className="mb-3">
// //                     <h4 className="fw-bold">Language {index + 1}</h4>
// //                     <div className="row mb-1">
// //                       <div className="col-lg-6 d-flex align-items-center mb-1">
// //                         <label
// //                           htmlFor={`languages.${index}.language`}
// //                           className="col-form-label required fw-bold fs-6 me-2"
// //                           style={{ minWidth: "150px" }}
// //                         >
// //                           Language Name:
// //                         </label>
// //                         <Field
// //                           type="text"
// //                           id={`languages.${index}.language`}
// //                           className="form-control form-control-lg form-control-solid bg-light-danger"
// //                           name={`languages.${index}.language`}
// //                           placeholder="Enter language name"
// //                           readOnly
// //                         />
// //                       </div>
// //                       <div className="col-lg-6 d-flex align-items-center mb-1">
// //                         <label
// //                           htmlFor={`languages.${index}.worK_PROFICIENCY`}
// //                           className="col-form-label required fw-bold fs-6 me-2"
// //                           style={{ minWidth: "150px" }}
// //                         >
// //                           Work Proficiency:
// //                         </label>
// //                         <Field
// //                           type="text"
// //                           id={`languages.${index}.worK_PROFICIENCY`}
// //                           className="form-control form-control-lg form-control-solid"
// //                           name={`languages.${index}.worK_PROFICIENCY`}
// //                           placeholder="Enter work proficiency"
// //                         />
// //                       </div>
// //                     </div>
// //                     <div className="row mb-1 form-check form-check-custom form-check-solid me-5">
// //                       <div className="col-lg-4 d-flex align-items-center mb-1">
// //                         <label
// //                           htmlFor={`languages.${index}.read`}
// //                           className="col-form-label required fw-bold fs-6 me-2"
// //                           style={{ minWidth: "150px" }}
// //                         >
// //                           Read:
// //                         </label>
// //                         <Field
// //                           type="checkbox"
// //                           id={`languages.${index}.read`}
// //                           className="form-check-input"
// //                           name={`languages.${index}.read`}
// //                         />
// //                       </div>
// //                       <div className="col-lg-4 d-flex align-items-center mb-1">
// //                         <label
// //                           htmlFor={`languages.${index}.write`}
// //                           className="col-form-label required fw-bold fs-6 me-2"
// //                           style={{ minWidth: "150px" }}
// //                         >
// //                           Write:
// //                         </label>
// //                         <Field
// //                           type="checkbox"
// //                           id={`languages.${index}.write`}
// //                           className="form-check-input"
// //                           name={`languages.${index}.write`}
// //                         />
// //                       </div>
// //                       <div className="col-lg-4 d-flex align-items-center mb-1">
// //                         <label
// //                           htmlFor={`languages.${index}.speak`}
// //                           className="col-form-label required fw-bold fs-6 me-2"
// //                           style={{ minWidth: "150px" }}
// //                         >
// //                           Speak:
// //                         </label>
// //                         <Field
// //                           type="checkbox"
// //                           id={`languages.${index}.speak`}
// //                           className="form-check-input"
// //                           name={`languages.${index}.speak`}
// //                         />
// //                       </div>
// //                     </div>
// //                   </div>
// //                 ))}
// //               </div>

// //               <div className="card-footer d-flex justify-content-center py-6 px-9">
// //                 <button
// //                   type="button"
// //                   className="btn btn-light me-2"
// //                   onClick={handleClose}
// //                 >
// //                   <span className="indicator-label">Discard</span>
// //                 </button>

// //                 <button
// //                   type="submit"
// //                   className="btn btn-primary"
// //                   disabled={!languages.length}
// //                 >
// //                   <span className="indicator-label">Submit</span>
// //                 </button>
// //               </div>
// //             </Form>
// //           )}
// //         </Formik>
// //       </div>
// //     </div>
// //   );
// // };

// // export default EditLanguages;

// import React, { useState, useEffect } from "react";
// import axios from "axios";
// import { useNavigate } from "react-router-dom";
// import { Formik, Form, Field, ErrorMessage } from "formik";
// import * as Yup from "yup";
// import { employeeIdForView } from "../../GlobalQuery";
// import { toast } from "react-toastify";

// const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// interface Response {
//   responseStatus: string;
//   message: string;
// }
// const EditLanguages: React.FC = () => {
//   const navigate = useNavigate();
//   const [languages, setLanguages] = useState([]);

//   useEffect(() => {
//     axios
//       .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPLANG`)
//       .then((response) => {
//         const languagesData = response.data.map((lang) => ({
//           ...lang,
//           read: lang.read === "Y",
//           write: lang.write === "Y",
//           speak: lang.speak === "Y",
//         }));
//         setLanguages(languagesData);
//       })
//       .catch((error) => {
//         console.error("Error fetching basic details:", error);
//       });
//   }, []);

//   const validationSchema = Yup.object().shape({
//     languages: Yup.array().of(
//       Yup.object().shape({
//         language: Yup.string().required("Language is required"),
//         worK_PROFICIENCY: Yup.string(),
//         read: Yup.bool(),
//         write: Yup.bool(),
//         speak: Yup.bool(),
//       })
//     ),
//   });

//   const handleSubmit = (values) => {
//     const formattedValues = values.languages.map((lang) => ({
//       ...lang,
//       read: lang.read ? "Y" : "N",
//       write: lang.write ? "Y" : "N",
//       speak: lang.speak ? "Y" : "N",
//     }));
  
//     axios
//       .post<Response>("your_backend_endpoint", {
//         languages: formattedValues,
//       })
//       .then((response) => {
//         console.log("Post successful!", response.data);
  
//         if (response.data.responseStatus === "E") {
//           toast.error(
//             <div>
//               <h4 className="alert-heading">Failed to submit Form...</h4>
//               <p className="alert alert-danger" role="alert">
//                 <h6>{Array.isArray(response.data.message) ? response.data.message.join("\n") : response.data.message}</h6>
//               </p>
//             </div>
//           );
//         } else {
//           toast.success(
//             <div>
//               <h4 className="alert-heading">Successfully Submitted Form...</h4>
//               <p className="alert alert-success" role="alert">
//                 <h6>{Array.isArray(response.data.message) ? response.data.message.join("\n") : response.data.message}</h6>
//               </p>
//             </div>
//           );
//         }
  
//         navigate(-1);
//       })
//       .catch((error) => {
//         console.error("Error posting data:", error);
//         toast.error(
//           <div>
//             <h6 className="alert-heading">Error Posting Data</h6>
//             <p className="alert alert-danger" role="alert">
//               <h6>{error.message}</h6>
//             </p>
//           </div>
//         );
//       });
//   };
  

//   const handleClose = () => {
//     navigate(-1);
//   };

//   return (
//     <div className="card mb-1 mb-xl-2">
//       <div
//         className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
//         role="button"
//         data-bs-target="#kt_account_profile_details"
//         aria-expanded="true"
//         aria-controls="kt_account_profile_details"
//       >
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Edit Language Details</h3>
//         </div>
//         <button
//           type="button"
//           className="btn-close"
//           aria-label="Close"
//           onClick={handleClose}
//           style={{ marginRight: "10px", cursor: "pointer" }}
//         ></button>
//       </div>

//       <div id="kt_account_profile_details" className="collapse show">
//         <Formik
//           initialValues={{ languages }}
//           validationSchema={validationSchema}
//           onSubmit={handleSubmit}
//           enableReinitialize
//         >
//           {({ values }) => (
//             <Form className="form" placeholder="">
//               <div className="card-body border-top p-9">
//                 {values.languages.map((language, index) => (
//                   <div key={index} className="mb-3">
//                     <h4 className="fw-bold">Language {index + 1}</h4>
//                     <div className="row mb-1">
//                       <div className="col-lg-6 d-flex align-items-center mb-1">
//                         <label
//                           htmlFor={`languages.${index}.language`}
//                           className="col-form-label  fw-bold fs-6 me-2"
//                           style={{ minWidth: "150px" }}
//                         >
//                           Language Name:
//                         </label>
//                         <Field
//                           type="text"
//                           id={`languages.${index}.language`}
//                           className="form-control form-control-lg form-control-solid bg-light-danger"
//                           style={{ cursor: "not-allowed" }}
//                           name={`languages.${index}.language`}
//                           placeholder="Enter language name"
//                           readOnly
//                         />
//                         <ErrorMessage
//                           name={`languages.${index}.language`}
//                           component="div"
//                           className="text-danger"
//                         />
//                       </div>
//                       <div className="col-lg-6 d-flex align-items-center mb-1">
//                         <label
//                           htmlFor={`languages.${index}.worK_PROFICIENCY`}
//                           className="col-form-label required fw-bold fs-6 me-2"
//                           style={{ minWidth: "150px" }}
//                         >
//                           Work Proficiency:
//                         </label>
//                         <Field
//                           type="text"
//                           id={`languages.${index}.worK_PROFICIENCY`}
//                           className="form-control form-control-lg form-control-solid"
//                           name={`languages.${index}.worK_PROFICIENCY`}
//                           placeholder="Enter work proficiency"
//                         />
//                         <ErrorMessage
//                           name={`languages.${index}.worK_PROFICIENCY`}
//                           component="div"
//                           className="text-danger"
//                         />
//                       </div>
//                     </div>
//                     <div className="row mb-1 form-check form-check-custom form-check-solid me-5">
//                       <div className="col-lg-4 d-flex align-items-center mb-1">
//                         <label
//                           htmlFor={`languages.${index}.read`}
//                           className="col-form-label required fw-bold fs-6 me-2"
//                           style={{ minWidth: "150px" }}
//                         >
//                           Read:
//                         </label>
//                         <Field
//                           type="checkbox"
//                           id={`languages.${index}.read`}
//                           className="form-check-input"
//                           name={`languages.${index}.read`}
//                         />
//                       </div>
//                       <div className="col-lg-4 d-flex align-items-center mb-1">
//                         <label
//                           htmlFor={`languages.${index}.write`}
//                           className="col-form-label required fw-bold fs-6 me-2"
//                           style={{ minWidth: "150px" }}
//                         >
//                           Write:
//                         </label>
//                         <Field
//                           type="checkbox"
//                           id={`languages.${index}.write`}
//                           className="form-check-input"
//                           name={`languages.${index}.write`}
//                         />
//                       </div>
//                       <div className="col-lg-4 d-flex align-items-center mb-1">
//                         <label
//                           htmlFor={`languages.${index}.speak`}
//                           className="col-form-label required fw-bold fs-6 me-2"
//                           style={{ minWidth: "150px" }}
//                         >
//                           Speak:
//                         </label>
//                         <Field
//                           type="checkbox"
//                           id={`languages.${index}.speak`}
//                           className="form-check-input"
//                           name={`languages.${index}.speak`}
//                         />
//                       </div>
//                     </div>
//                   </div>
//                 ))}
//               </div>

//               <div className="card-footer d-flex justify-content-center py-6 px-9">
//                 <button
//                   type="button"
//                   className="btn btn-light me-2"
//                   onClick={handleClose}
//                 >
//                   <span className="indicator-label">Discard</span>
//                 </button>

//                 <button
//                   type="submit"
//                   className="btn btn-primary"
//                   disabled={!languages.length}
//                 >
//                   <span className="indicator-label">Submit</span>
//                 </button>
//               </div>
//             </Form>
//           )}
//         </Formik>
//       </div>
//     </div>
//   );
// };

// export default EditLanguages;
import {FC} from 'react'

const EditLanguages: FC = () => {
  return (<div >
          <h1>This Page is under Development...</h1>
          <h3>please contact to admin</h3>
          </div>)
}

export {EditLanguages}