import { Content } from "../../../../_metronic/layout/components/content";
import { EditLanguages } from "./EditLanguages";


export function EditLanguagesWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <EditLanguages />
      </div>
    </Content>
  );
}
