import { Content } from "../../../../_metronic/layout/components/content";
import PermanentDetailsEdit from "./PermanentDetailsEdit";

export function EditPermanentDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <PermanentDetailsEdit />
      </div>
    </Content>
  );
}
