import { Content } from "../../../../_metronic/layout/components/content";
import PersonalDetailsEdit from "./PersonalDetailsEdit";


export function EditPersonalDetails() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
        <PersonalDetailsEdit />
      </div>
    </Content>
  );
}
