// import React, { useState, useEffect } from "react";
// import axios from "axios";
// import { useNavigate } from "react-router-dom";
// import { useFormik } from "formik";
// import * as Yup from "yup";
// import { employeeIdForView } from "../../GlobalQuery";
// import { toast } from "react-toastify";
// import { useAuth } from "../../auth";
// const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
// type states = {
//   statE_CODE: string;
//   descr: string;
// };

// const EmergencyDetailsEdit: React.FC = () => {
//   const [employee, setEmployee] = useState<any>({});
//   const navigate = useNavigate();
//   const [states, setStates] = useState<states[]>([]);
//   const [pdf, setPdf] = useState<File | null>(null);
//   const { currentUser } = useAuth();

//   useEffect(() => {
//     axios
//       .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
//       .then((response) => {
//         setEmployee(response.data[0]);
//         // Update formik's initial values after fetching employee data
//         formik.setValues({
//           contacT_PERS: response.data[0].contacT_PERS || "",
//           contacT_ADD1: response.data[0].contacT_ADD1 || "",
//           contacT_ADD2: response.data[0].contacT_ADD2 || "",
//           contacT_ADD3: response.data[0].contacT_ADD3 || "",
//           contacT_TEL: response.data[0].contacT_TEL || "",
//           contacT_STATE: response.data[0].contacT_STATE || "",
//           contacT_CITY: response.data[0].contacT_CITY || "",
//           contacT_PIN: response.data[0].contacT_PIN || "",
//           pdf: response.data[0].pdf || pdf,
//         });
//       })
//       .catch((error) => {
//         console.error("Error fetching basic details:", error);
//       });

//     axios
//       .get(`${EMPLOYEE_URL}/ddl_GetStates`)
//       .then((response) => {
//         setStates(response.data.states);
//       })
//       .catch((error) => {
//         console.error("Error fetching basic details:", error);
//       });
//   }, []);

//   const validationSchema = Yup.object().shape({
//     contacT_PERS: Yup.string().required("Contact Person is required"),
//     contacT_ADD1: Yup.string().required("Line 1 is required"),
//     contacT_ADD2: Yup.string().required("Line 2 is required"),
//     contacT_ADD3: Yup.string().required("Line 3 is required"),
//     contacT_TEL: Yup.string().required("Contact Telephone is required"),
//     contacT_STATE: Yup.string().required("Contact State is required"),
//     contacT_CITY: Yup.string().required("Contact City is required"),
//     contacT_PIN: Yup.string().required("Zip Code is required"),
//   });

//   const formik = useFormik({
//     initialValues: {
//       contacT_PERS: "",
//       contacT_ADD1: "",
//       contacT_ADD2: "",
//       contacT_ADD3: "",
//       contacT_TEL: "",
//       contacT_STATE: "",
//       contacT_CITY: "",
//       contacT_PIN: "",
//       pdf: null,
//     },
//     validationSchema: validationSchema,
//     onSubmit: (values, formik) => {
//       console.log("Submitting form data:", values);
//       const formData = new FormData();

//       Object.keys(values).forEach((key) => {
//         if (key === "pdf" && pdf) {
//           formData.append(key, pdf);
//         } else {
//           formData.append(key, values[key as keyof typeof values] as string);
//         }
//       });

//       axios
//         .put(
//           `${EMPLOYEE_URL}/UpdateEmployeeEmergencyAddressDetails/${employeeIdForView}`,
//           {
//             ...values,
//             chG_DATE: new Date(),
//             chG_USER: currentUser?.id,
//             chG_TERM: "TERM",
//             // pdf: pdf,
//           }
//         )
//         .then((response) => {
//           console.log("Basic Details Posted successfully!", response.data);
//           const { responseStatus, message } = response.data.data;
//           if (responseStatus === "E") {
//             formik.setSubmitting(false);
//             toast.error(
//               <div>
//                 <h4 className="alert-heading">Failed to submit Form...</h4>
//                 <p className="alert alert-danger" role="alert">
//                   <h6>
//                     {Array.isArray(message) ? message.join("\n") : message}
//                   </h6>
//                 </p>
//               </div>
//             );
//           } else {
//             toast.success(
//               <div>
//                 <h4 className="alert-heading">
//                   Successfully Submitted Form...
//                 </h4>
//                 <p className="alert alert-success" role="alert">
//                   <h6>
//                     {Array.isArray(message) ? message.join("\n") : message}
//                   </h6>
//                 </p>
//               </div>
//             );
//             setEmployee({
//               contacT_PERS: "",
//               contacT_ADD1: "",
//               contacT_ADD2: "",
//               contacT_ADD3: "",
//               contacT_TEL: "",
//               contacT_STATE: "",
//               contacT_CITY: "",
//               contacT_PIN: "",
//             });
//             formik.resetForm();
//             navigate(-1);
//           }
//         })
//         .catch((error) => {
//           console.error("Error posting data:", error);
//           toast.error(
//             <div>
//               <h6 className="alert-heading">Error Posting Data</h6>
//               <p className="alert alert-danger" role="alert">
//                 <h6>{error.message}</h6>
//               </p>
//             </div>
//           );

//           formik.setSubmitting(false);
//         });

//     },
//   });
//   const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
//     const file = e.target.files?.[0];
//     if (file) {
//       setPdf(file);
//     }
//   };
//   const handleClose = () => {
//     navigate(-1);
//   };

//   return (
//     <div className="card mb-1 mb-xl-2">
//       <div
//         className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
//         role="button"
//         data-bs-target="#kt_account_profile_details"
//         aria-expanded="true"
//         aria-controls="kt_account_profile_details"
//       >
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Edit Emergency Details</h3>
//         </div>
//         <button
//           type="button"
//           className="btn-close"
//           aria-label="Close"
//           onClick={handleClose}
//           style={{ marginRight: "10px", cursor: "pointer" }}
//         ></button>
//       </div>

//       <div id="kt_account_profile_details" className="collapse show">
//         <form onSubmit={formik.handleSubmit} className="form">
//           <div className="card-body border-top p-9">
//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="contacT_PERS"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   CONTACT PERSON :
//                 </label>
//                 <input
//                   type="text"
//                   id="contacT_PERS"
//                   className="form-control form-control-lg form-control-solid"
//                   name="contacT_PERS"
//                   placeholder="Enter Contact Person"
//                   value={formik.values.contacT_PERS}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="contacT_ADD1"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   EMERGENCY ADDRESS :
//                 </label>
//                 <input
//                   type="text"
//                   id="contacT_ADD1"
//                   className="form-control form-control-lg form-control-solid"
//                   name="contacT_ADD1"
//                   placeholder="Line 1"
//                   value={formik.values.contacT_ADD1}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>

//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.contacT_PERS && formik.errors.contacT_PERS ? (
//                   <div className="text-danger">
//                     {formik.errors.contacT_PERS}
//                   </div>
//                 ) : null}
//               </div>
//               <div className="col-lg-6">
//                 {formik.touched.contacT_ADD1 && formik.errors.contacT_ADD1 ? (
//                   <div className="text-danger">
//                     {formik.errors.contacT_ADD1}
//                   </div>
//                 ) : null}
//               </div>
//             </div>

//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="cuR_PIN"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   ATTACH DOCUMENT :
//                 </label>
//                 <input
//                   name="birtH_DATE"
//                   className="form-control form-control-lg flex-grow-1"
//                   type="file"
//                   id="pdf"
//                   onChange={handlePdfChange}
//                   accept="application/pdf"
//                   required
//                 />
//                 {formik.touched.pdf && formik.errors.pdf ? (
//                   <div className="text-danger">{formik.errors.pdf}</div>
//                 ) : null}
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="contacT_ADD2"
//                   className="col-form-label fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   &nbsp;
//                 </label>
//                 <input
//                   type="text"
//                   id="contacT_ADD2"
//                   className="form-control form-control-lg form-control-solid"
//                   name="contacT_ADD2"
//                   placeholder="Line 2"
//                   value={formik.values.contacT_ADD2}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>

//             <div className="row mb-1">
//               <div className="col-lg-6"></div>
//               <div className="col-lg-6">
//                 {formik.touched.contacT_ADD2 && formik.errors.contacT_ADD2 ? (
//                   <div className="text-danger">
//                     {formik.errors.contacT_ADD2}
//                   </div>
//                 ) : null}
//               </div>
//             </div>

//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1"></div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="contacT_ADD3"
//                   className="col-form-label fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   &nbsp;
//                 </label>
//                 <input
//                   type="text"
//                   id="contacT_ADD3"
//                   className="form-control form-control-lg form-control-solid"
//                   name="contacT_ADD3"
//                   placeholder="Line 3"
//                   value={formik.values.contacT_ADD3}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>

//             <div className="row mb-1">
//               <div className="col-lg-6"></div>
//               <div className="col-lg-6">
//                 {formik.touched.contacT_ADD3 && formik.errors.contacT_ADD3 ? (
//                   <div className="text-danger">
//                     {formik.errors.contacT_ADD3}
//                   </div>
//                 ) : null}
//               </div>
//             </div>
//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="contacT_TEL"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   EMERGENCY TELEPHONE :
//                 </label>
//                 <input
//                   type="text"
//                   id="contacT_TEL"
//                   className="form-control form-control-lg form-control-solid"
//                   name="contacT_TEL"
//                   placeholder="Enter contact telephone"
//                   value={formik.values?.contacT_TEL}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   {" "}
//                   STATE :
//                 </label>
//                 <select
//                   id="contacT_STATE"
//                   className="form-select form-select-lg form-select-solid"
//                   name="contacT_STATE"
//                   onChange={formik.handleChange}
//                 >
//                   <option value={formik.values?.contacT_STATE}>
//                     {formik.values?.contacT_STATE}
//                   </option>

//                   {states.map((state) => (
//                     <option key={state.statE_CODE} value={state.statE_CODE}>
//                       {state.descr}
//                     </option>
//                   ))}
//                 </select>
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.contacT_TEL && formik.errors.contacT_TEL ? (
//                   <div className="text-danger">{formik.errors.contacT_TEL}</div>
//                 ) : null}
//               </div>

//               <div className="col-lg-6">
//                 {formik.touched.contacT_STATE &&
//                 formik.errors.contacT_STATE ? (
//                   <div className="text-danger">
//                     {formik.errors.contacT_STATE}
//                   </div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="contacT_CITY"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   CITY:
//                 </label>
//                 <input
//                   type="text"
//                   id="contacT_CITY"
//                   className="form-control form-control-lg form-control-solid"
//                   name="contacT_CITY"
//                   placeholder="Enter contact city"
//                   value={formik.values?.contacT_CITY}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   {" "}
//                   ZIP CODE :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   name="contacT_PIN"
//                   placeholder="Enter zip code"
//                   value={formik.values.contacT_PIN?.toUpperCase()}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.contacT_CITY && formik.errors.contacT_CITY ? (
//                   <div className="text-danger">
//                     {formik.errors.contacT_CITY}
//                   </div>
//                 ) : null}
//               </div>

//               <div className="col-lg-6">
//                 {formik.touched.contacT_PIN && formik.errors.contacT_PIN ? (
//                   <div className="text-danger">{formik.errors.contacT_PIN}</div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}
//           </div>

//           {/* Logic for Submit is HERE */}
//           <div className="card-footer d-flex justify-content-center py-6 px-9">
//             <button
//               type="button"
//               className="btn btn-light me-2"
//               onClick={handleClose}
//             >
//               <span className="indicator-label">Discard</span>
//             </button>

//             <button
//               type="submit"
//               className="btn btn-primary"
//               disabled={formik.isSubmitting || !formik.isValid}
//             >
//               <span className="indicator-label">Submit</span>
//             </button>
//           </div>
//         </form>
//       </div>
//     </div>
//   );
// };

// export default EmergencyDetailsEdit;

import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { Mode, QUERIES } from "../../../../_metronic/helpers";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
import { useListView } from "../../apps/transaction-master/master-list/core/ListViewProvider";
import clsx from "clsx";
import { useMutation, useQueryClient } from "react-query";
import { insertTransactionV2 } from "../../apps/transaction-master/master-list/core/_requests";
import { useQueryResponse } from "../../apps/employee/employee-list/core/QueryResponseProvider";

type FieldDetail = {
  olD_KEY: string;
  olD_VALUE: string;
  neW_KEY: string;
  neW_VALUE: string;
  FILE_PATH: string;
};

type EmergencyDetails = {
  contacT_PERS: "",
  contacT_ADD1: "",
  contacT_ADD2: "",
  contacT_ADD3: "",
  contacT_TEL: "",
  contacT_STATE: "",
  contacT_CITY: "",
  contacT_PIN: "",
  contacT_ADD1_FILE: null,
  contacT_STATE_DESCR: string;
  CHG_REASON: string;

  fieldNames?: {
    CONTACT_PERS: FieldDetail;
    CONTACT_ADD1: FieldDetail;
    CONTACT_ADD2: FieldDetail;
    CONTACT_ADD3: FieldDetail;
    CONTACT_TEL: FieldDetail;
    CONTACT_STATE: FieldDetail;
    CONTACT_CITY: FieldDetail;
    CONTACT_PIN: FieldDetail;
    CONTACT_ADD1_FILE: FieldDetail; 
    CHG_REASON: FieldDetail;
  };
};

interface States {
  statE_CODE: string;
  descr: string;
  counT_CODE: string;
}

const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const EmergencyDetailsEdit: React.FC = () => {
  const { currentUser } = useAuth();
  const [privateIP, setPrivateIP] = useState();

  const { itemIdForUpdate, setItemIdForUpdate, formMode } = useListView();
  const [empCodeTxForView, setEmpCodeTxForView] = useState();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  const [states, setStates] = useState<States[]>([]);

  // const [pdf, setPdf] = useState<string>("");

  const [emergencyDetails, setEmergencyDetails] = useState<EmergencyDetails>();
  const [emergencyDetailsTx, setEmergencyDetailsTx] =
    useState<EmergencyDetails>();

  // const [genders] = useState([
  //   { code: "F", descr: "FEMALE" },
  //   { code: "M", descr: "MALE" },
  // ]);

  // const [religions] = useState([
  //   { code: "HINDU", descr: "HINDU" },
  //   { code: "MUSLIM", descr: "MUSLIM" },
  //   { code: "SIKH", descr: "SIKH" },
  //   { code: "CHRISTIAN", descr: "CHRISTIAN" },
  //   { code: "BAUDDHA", descr: "BAUDDHA" },
  //   { code: "JAIN", descr: "JAIN" },
  //   { code: "PARASI", descr: "PARASI" },
  // ]);

  // const [bloodGrps] = useState([
  //   { code: "A+", descr: "A+" },
  //   { code: "A-", descr: "A-" },
  //   { code: "B+", descr: "B+" },
  //   { code: "B-", descr: "B-" },
  //   { code: "AB+", descr: "AB+" },
  //   { code: "AB-", descr: "AB-" },
  //   { code: "O+", descr: "O+" },
  //   { code: "O-", descr: "O-" },
  // ]);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching Ip Address:", error));

    itemIdForUpdate && fetchEmergencyDetailsTx();
    fetchEmergencyDetails();
  }, [empCodeTxForView]);

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/ddl_GetStates`)
      .then((response) => {
        setStates(response.data.states);
      })
      .catch((error) => {
        console.error("Error fetching state dropdown:", error);
      });
  }, []);

  const fetchEmergencyDetails = () => {
    axios
      .get(
        `${EMPLOYEE_URL}/${
          employeeIdForView ? employeeIdForView : empCodeTxForView
        }/EMPPER`
      )
      .then((response) => {
        setEmergencyDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching Present contact details:", error);
      });
  };

  const fetchEmergencyDetailsTx = () => {
    axios
      .get(`${MASTERTX_URL}/GetTransactionByID?tranId=${itemIdForUpdate}`)
      .then((response) => {
        setEmpCodeTxForView(
          response.data.transactions[0].fieldNames?.EMP_CODE?.neW_VALUE
        );

        setEmergencyDetailsTx(response.data.transactions[0]);
      });
  };

  useEffect(() => {
    formik.setValues({
      contacT_PERS:
        emergencyDetailsTx?.fieldNames?.CONTACT_PERS?.neW_KEY ||
        emergencyDetails?.contacT_PERS ||
        "",
        contacT_PERS_O:
        emergencyDetailsTx?.fieldNames?.CONTACT_PERS?.neW_KEY ||
        emergencyDetails?.contacT_PERS ||
        "",

        contacT_ADD1:   emergencyDetailsTx?.fieldNames?.CONTACT_ADD1?.neW_KEY ||
        emergencyDetails?.contacT_ADD1 ||
        "",
        contacT_ADD1_O:   emergencyDetailsTx?.fieldNames?.CONTACT_ADD1?.neW_KEY ||
        emergencyDetails?.contacT_ADD1 ||
        "",
        
        contacT_ADD2:  emergencyDetailsTx?.fieldNames?.CONTACT_ADD2?.neW_KEY ||
        emergencyDetails?.contacT_ADD2 ||
        "",
        contacT_ADD2_O:   emergencyDetailsTx?.fieldNames?.CONTACT_ADD2?.neW_KEY ||
        emergencyDetails?.contacT_ADD2 ||
        "",
        
        contacT_ADD3: emergencyDetailsTx?.fieldNames?.CONTACT_ADD3?.neW_KEY ||
        emergencyDetails?.contacT_ADD3 ||
        "",
        contacT_ADD3_O: emergencyDetailsTx?.fieldNames?.CONTACT_ADD3?.neW_KEY ||
        emergencyDetails?.contacT_ADD3 ||
        "",
        
        contacT_TEL: emergencyDetailsTx?.fieldNames?.CONTACT_TEL?.neW_KEY ||
        emergencyDetails?.contacT_TEL ||
        "",
        contacT_TEL_O: emergencyDetailsTx?.fieldNames?.CONTACT_TEL?.neW_KEY ||
        emergencyDetails?.contacT_TEL ||
        "",
        
        contacT_STATE: emergencyDetailsTx?.fieldNames?.CONTACT_STATE?.neW_KEY ||
        emergencyDetails?.contacT_STATE ||
        "",
        contacT_STATE_val:
        emergencyDetailsTx?.fieldNames?.CONTACT_STATE?.neW_VALUE ||
        emergencyDetails?.contacT_STATE_DESCR ||
        "",
        contacT_STATE_O: emergencyDetailsTx?.fieldNames?.CONTACT_STATE?.neW_KEY ||
        emergencyDetails?.contacT_STATE ||
        "",
        contacT_STATE_val_O:
        emergencyDetailsTx?.fieldNames?.CONTACT_STATE?.olD_VALUE ||
        emergencyDetails?.contacT_STATE_DESCR ||
        "",
         
        contacT_CITY:emergencyDetailsTx?.fieldNames?.CONTACT_CITY?.neW_KEY ||
        emergencyDetails?.contacT_CITY ||
        "",
        contacT_CITY_O:emergencyDetailsTx?.fieldNames?.CONTACT_CITY?.neW_KEY ||
        emergencyDetails?.contacT_CITY ||
        "",
     
        contacT_PIN: emergencyDetailsTx?.fieldNames?.CONTACT_PIN?.neW_KEY ||
        emergencyDetails?.contacT_PIN ||
        "",
        contacT_PIN_O: emergencyDetailsTx?.fieldNames?.CONTACT_PIN?.neW_KEY ||
        emergencyDetails?.contacT_PIN ||
        "",

        contacT_ADD1_FILE: emergencyDetailsTx?.fieldNames?.CONTACT_ADD1?.FILE_PATH ||  "",

        CHG_REASON: formMode === Mode.EDIT ? "" : emergencyDetails?.CHG_REASON,

    });
  }, [emergencyDetails]);

  // const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
  //   formik.setFieldValue("emP_IMAGE", e.target.files?.[0]);
  //   const file = e.target.files?.[0];
  //   if (file) {
  //     const reader = new FileReader();
  //     reader.onloadend = () => {
  //       const base64String = reader.result as string;
  //       formik.setFieldValue("emP_IMAGE", base64String);
  //     };
  //     reader.readAsDataURL(file);
  //   }
  // };

  const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (file) {
      const reader = new FileReader();

      reader.onloadend = () => {
        const base64String = reader.result;
        console.log(base64String);
        formik.setFieldValue("contacT_ADD1_FILE", base64String);
      };
      reader.readAsDataURL(file);
    }
  };

  const validationSchema = Yup.object().shape({
    contacT_ADD1: Yup.string().required("Line 1 is required"),
    contacT_ADD2: Yup.string().required("Line 2 is required"),
    contacT_ADD3: Yup.string().required("Line 3 is required"),
    contacT_PERS: Yup.string().required("Contact person is required"),
    contacT_TEL: Yup.string().required("Telephone is required"),
    contacT_STATE: Yup.string().required("State is required"),
    contacT_CITY: Yup.string().required("City is required"),
    contacT_PIN: Yup.string()
      .required("Pincode is required")
      .test(
        "len",
        "Pincode must be exactly 6 numbers",
        (value) => value && value.length === 6
      ),
    CHG_REASON: Yup.string().required("Change Reason is required"),
    contacT_ADD1_FILE: Yup.string().when([], {
      is: () =>
        emergencyDetails?.contacT_ADD1 !== formik.values.contacT_ADD1 ||
        emergencyDetails?.contacT_ADD2 !== formik.values.contacT_ADD2 ||
        emergencyDetails?.contacT_ADD3 !== formik.values.contacT_ADD3 ||
        emergencyDetails?.contacT_STATE !== formik.values.contacT_STATE ||
        emergencyDetails?.contacT_CITY !== formik.values.contacT_CITY ||
        emergencyDetails?.contacT_PIN !== formik.values.contacT_PIN,
      then: (schema) => schema.required("Proof attachment is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
  });

  const createTx = useMutation((payload: any) => insertTransactionV2(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        handleClose();
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formik = useFormik({
    initialValues: {
      contacT_PERS: "",
      contacT_PERS_O: "",
      
      contacT_ADD1: "",
      contacT_ADD1_O: "",
      
      contacT_ADD2: "",
      contacT_ADD2_O: "",
      
      contacT_ADD3: "",
      contacT_ADD3_O: "",
      
      contacT_TEL: "",
      contacT_TEL_O: "",
      
      contacT_STATE: "",
      contacT_STATE_O: "",
      
      contacT_CITY: "",
      contacT_CITY_O: "",
      
      contacT_PIN: "",
      contacT_PIN_O: "",

      contacT_ADD1_FILE: "",

      CHG_REASON: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values, formik) => {
      const payload = {
        values,

        TABLENAME: "EMPLOYEE_EMERGENCY",
        KEYVAR: "",
        WFLEVEL: 1,
        CONF: "N",
        SITECODE: "S0001",
        EMP: currentUser?.id?.toString() || "",
        FILEPATH: null,
        FILE: null,
        FOLDERPATH: null,
        FORMNAME: "Emergency Contact Details",
        tX_CHG_USER: currentUser?.id?.toString() || "",
        tX_CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        STRTRANID: itemIdForUpdate ? itemIdForUpdate : null,
      };

      createTx.mutate(payload);
      formik.setSubmitting(false);
    },
  });

  const handleClose = () => {
    itemIdForUpdate ? setItemIdForUpdate(undefined) : navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Emergency Contact</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: final outer Edit Row */}
            <div className="row mb-1">
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="contacT_PERS"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CONTACT PERSON:
                  </label>
                  <input
                    type="text"
                    id="contacT_PERS"
                    name="contacT_PERS"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          emergencyDetailsTx?.fieldNames?.CONTACT_PERS?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.contacT_PERS && formik.errors.contacT_PERS,
                      },
                      {
                        "is-valid":
                          formik.touched.contacT_PERS && !formik.errors.contacT_PERS,
                      }
                    )}
                    {...(emergencyDetailsTx?.fieldNames?.CONTACT_PERS && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        emergencyDetailsTx?.fieldNames?.CONTACT_PERS?.olD_VALUE ===
                        null
                          ? "-"
                          : emergencyDetailsTx?.fieldNames?.CONTACT_PERS?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter name of contact person"
                    value={formik.values.contacT_PERS}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.contacT_PERS && formik.errors.contacT_PERS && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.contacT_PERS}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="contacT_ADD1"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CONTACT ADDRESS:
                  </label>
                  <input
                    type="text"
                    id="contacT_ADD1"
                    name="contacT_ADD1"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          emergencyDetailsTx?.fieldNames?.CONTACT_ADD1?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.contacT_ADD1 && formik.errors.contacT_ADD1,
                      },
                      {
                        "is-valid":
                          formik.touched.contacT_ADD1 && !formik.errors.contacT_ADD1,
                      }
                    )}
                    {...(emergencyDetailsTx?.fieldNames?.CONTACT_ADD1 && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        emergencyDetailsTx?.fieldNames?.CONTACT_ADD1?.olD_VALUE ===
                        null
                          ? "-"
                          : emergencyDetailsTx?.fieldNames?.CONTACT_ADD1?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter line 1 address"
                    value={formik.values.contacT_ADD1}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.contacT_ADD1 && formik.errors.contacT_ADD1 && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.contacT_ADD1}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}
              

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="contacT_ADD1_FILE"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    ATTACH FILE:
                  </label>
                  <input
                    name="contacT_ADD1_FILE"
                    // className="form-control form-control-lg flex-grow-1"
                    className={clsx(
                      "form-control form-control-solid flex-grow-1",
                      {
                        "bg-light-warning":
                          emergencyDetailsTx?.fieldNames?.CONTACT_ADD1?.FILE_PATH,
                      },
                      {
                        "is-invalid": formik.touched.contacT_ADD1_FILE && formik.errors.contacT_ADD1_FILE,
                      },
                      {
                        "is-valid": formik.touched.contacT_ADD1_FILE && !formik.errors.contacT_ADD1_FILE,
                      }
                    )}
                    {...(emergencyDetailsTx?.fieldNames?.CONTACT_ADD1.FILE_PATH && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        emergencyDetailsTx?.fieldNames?.CONTACT_ADD1.FILE_PATH === null
                          ? "-"
                          : emergencyDetailsTx?.fieldNames?.CONTACT_ADD1.FILE_PATH
                      }`,
                    })}
                    type="file"
                    id="contacT_ADD1_FILE"
                    placeholder="Attach proof document"
                    onChange={handlePdfChange}
                    accept="application/pdf"
                    // required
                  />
                </div>
                {formik.touched.contacT_ADD1_FILE && formik.errors.contacT_ADD1_FILE && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.contacT_ADD1_FILE}</span>
                  </div>
                )}
              </div>
               {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="contacT_ADD2"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    &nbsp;
                  </label>
                  <input
                    type="text"
                    id="contacT_ADD2"
                    name="contacT_ADD2"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          emergencyDetailsTx?.fieldNames?.CONTACT_ADD2?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.contacT_ADD2 && formik.errors.contacT_ADD2,
                      },
                      {
                        "is-valid":
                          formik.touched.contacT_ADD2 && !formik.errors.contacT_ADD2,
                      }
                    )}
                    {...(emergencyDetailsTx?.fieldNames?.CONTACT_ADD2 && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        emergencyDetailsTx?.fieldNames?.CONTACT_ADD2?.olD_VALUE ===
                        null
                          ? "-"
                          : emergencyDetailsTx?.fieldNames?.CONTACT_ADD2?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Line 2 address"
                    value={formik.values.contacT_ADD2}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.contacT_ADD2 && formik.errors.contacT_ADD2 && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.contacT_ADD2}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              


              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="peR_ADD3"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    &nbsp;
                  </label>
                  {/* <input
                    type="text"
                    id="peR_ADD3"
                    name="peR_ADD3"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          permenentDetailsTx?.fieldNames?.PER_ADD3?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.peR_ADD3 && formik.errors.peR_ADD3,
                      },
                      {
                        "is-valid":
                          formik.touched.peR_ADD3 && !formik.errors.peR_ADD3,
                      }
                    )}
                    {...(permenentDetailsTx?.fieldNames?.PER_ADD3 && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        permenentDetailsTx?.fieldNames?.PER_ADD3?.olD_VALUE ===
                        null
                          ? "-"
                          : permenentDetailsTx?.fieldNames?.PER_ADD3?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Line 3 address"
                    value={formik.values.peR_ADD3}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  /> */}
                </div>
                {/* {formik.touched.peR_ADD3 && formik.errors.peR_ADD3 && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.peR_ADD3}</span>
                  </div>
                )} */}
              </div>
              {/* End: Edit Row */}

               {/* Begin: Edit Row */}
               <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="contacT_ADD3"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    &nbsp;
                  </label>
                  <input
                    type="text"
                    id="contacT_ADD3"
                    name="contacT_ADD3"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          emergencyDetailsTx?.fieldNames?.CONTACT_ADD3?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.contacT_ADD3 && formik.errors.contacT_ADD3,
                      },
                      {
                        "is-valid":
                          formik.touched.contacT_ADD3 && !formik.errors.contacT_ADD3,
                      }
                    )}
                    {...(emergencyDetailsTx?.fieldNames?.CONTACT_ADD3 && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        emergencyDetailsTx?.fieldNames?.CONTACT_ADD3?.olD_VALUE ===
                        null
                          ? "-"
                          : emergencyDetailsTx?.fieldNames?.CONTACT_ADD3?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Line 3 address"
                    value={formik.values.contacT_ADD3}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.contacT_ADD3 && formik.errors.contacT_ADD3 && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.contacT_ADD3}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

                {/* Begin: Edit Row */}
                <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="contacT_TEL"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                     TELEPHONE:
                  </label>
                  <input
                    type="text"
                    id="contacT_TEL"
                    name="contacT_TEL"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          emergencyDetailsTx?.fieldNames?.CONTACT_TEL?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.contacT_TEL && formik.errors.contacT_TEL,
                      },
                      {
                        "is-valid":
                          formik.touched.contacT_TEL && !formik.errors.contacT_TEL,
                      }
                    )}
                    {...(emergencyDetailsTx?.fieldNames?.CONTACT_TEL && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        emergencyDetailsTx?.fieldNames?.CONTACT_TEL?.olD_VALUE ===
                        null
                          ? "-"
                          : emergencyDetailsTx?.fieldNames?.CONTACT_TEL?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter telephone"
                    value={formik.values.contacT_TEL}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.contacT_TEL && formik.errors.contacT_TEL && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.contacT_TEL}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

                {/* Begin: Edit Row */}
                <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="contacT_STATE"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    STATE:
                  </label>
                
                  <select
                    id="contacT_STATE"
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          emergencyDetailsTx?.fieldNames?.CONTACT_STATE?.neW_KEY,
                      },
                      {
                        "is-invalid":
                          formik.touched.contacT_STATE && formik.errors.contacT_STATE,
                      },
                      {
                        "is-valid":
                          formik.touched.contacT_STATE && !formik.errors.contacT_STATE,
                      }
                    )}
                    {...(emergencyDetailsTx?.fieldNames?.CONTACT_STATE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        emergencyDetailsTx?.fieldNames?.CONTACT_STATE?.olD_VALUE ===
                        null
                          ? "-"
                          : emergencyDetailsTx?.fieldNames?.CONTACT_STATE?.olD_VALUE
                      }`,
                    })}
                    name="contacT_STATE"
                    aria-placeholder="Select State"
                    value={formik.values.contacT_STATE}
                    // onChange={formik.handleChange}
                    onChange={(e) => {
                      const selectedValue = e.target.value;
                      const selectedOption = states.find(
                        (bankName) => bankName.statE_CODE === selectedValue
                      );

                      formik.setFieldValue("contacT_STATE", selectedValue);
                      formik.setFieldValue(
                        "contacT_STATE_val",
                        selectedOption?.descr.toUpperCase() || ""
                      );
                    }}
                  >
                    <option value=""></option>
                    {states.map((e) => (
                      <option key={e.statE_CODE} value={e.statE_CODE}>
                        {e.descr.toUpperCase()}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.contacT_STATE && formik.errors.contacT_STATE && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.contacT_STATE}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}







              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="contacT_CITY"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                     CITY:
                  </label>
                  <input
                    type="text"
                    id="contacT_CITY"
                    name="contacT_CITY"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          emergencyDetailsTx?.fieldNames?.CONTACT_CITY?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.contacT_CITY && formik.errors.contacT_CITY,
                      },
                      {
                        "is-valid":
                          formik.touched.contacT_CITY && !formik.errors.contacT_CITY,
                      }
                    )}
                    {...(emergencyDetailsTx?.fieldNames?.CONTACT_CITY && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        emergencyDetailsTx?.fieldNames?.CONTACT_CITY?.olD_VALUE ===
                        null
                          ? "-"
                          : emergencyDetailsTx?.fieldNames?.CONTACT_CITY?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter city name"
                    value={formik.values.contacT_CITY}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.contacT_CITY && formik.errors.contacT_CITY && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.contacT_CITY}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

                {/* Begin: Edit Row */}
                <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="contacT_PIN"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PIN CODE:
                  </label>
                  <input
                    type="text"
                    id="contacT_PIN"
                    name="contacT_PIN"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          emergencyDetailsTx?.fieldNames?.CONTACT_PIN?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.contacT_PIN && formik.errors.contacT_PIN,
                      },
                      {
                        "is-valid":
                          formik.touched.contacT_PIN && !formik.errors.contacT_PIN,
                      }
                    )}
                    {...(emergencyDetailsTx?.fieldNames?.CONTACT_PIN && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        emergencyDetailsTx?.fieldNames?.CONTACT_PIN?.olD_VALUE ===
                        null
                          ? "-"
                          : emergencyDetailsTx?.fieldNames?.CONTACT_PIN?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Pincode"
                    value={formik.values.contacT_PIN}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.contacT_PIN && formik.errors.contacT_PIN && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.contacT_PIN}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}



            
              {/* Begin: Edit Row */}

              <div className="col-lg-12">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="CHG_REASON"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CHANGE REASON:
                  </label>
                  <input
                    type="text"
                    id="CHG_REASON"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "is-invalid":
                          formik.touched.CHG_REASON && formik.errors.CHG_REASON,
                      },
                      {
                        "is-valid":
                          formik.touched.CHG_REASON &&
                          !formik.errors.CHG_REASON,
                      }
                    )}
                    name="CHG_REASON"
                    placeholder="Enter Change Reason"
                    value={formik.values.CHG_REASON?.toUpperCase()}
                    //
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.CHG_REASON && formik.errors.CHG_REASON && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.CHG_REASON}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}
            </div>
            {/* end: final outer Edit Row */}
          </div>

          {/* Logic for Submit */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default EmergencyDetailsEdit;
