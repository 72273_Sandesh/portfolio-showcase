// import React, { useState, useEffect } from "react";
// import axios from "axios";
// import { useNavigate } from "react-router-dom";
// import { useFormik } from "formik";
// import * as Yup from "yup";
// import { employeeIdForView } from "../../GlobalQuery";
// import { toast } from "react-toastify";
// const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// interface Response {
//   responseStatus: string;
//   message: string;
// }

// const JoiningDetailsEdit: React.FC = () => {
//   const [employee, setEmployee] = useState<any>({});
//   const navigate = useNavigate();

//   useEffect(() => {
//     axios
//       .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
//       .then((response) => {
//         setEmployee(response.data[0]);
//         // Update formik's initial values after fetching employee data
//         formik.setValues({
//           datE_JOIN: response.data[0].datE_JOIN || "",
//           joineD_AS: response.data[0].joineD_AS.toUpperCase() || "",
//           joininG_CTC: response.data[0].joininG_CTC || "",
//           reporT_TO: response.data[0].reporT_TO.toUpperCase() || "",
//           employmenT_TYPE: response.data[0].employmenT_TYPE || "",
//           datE_CONF: response.data[0].datE_CONF || "",
//           probatioN_PRD: response.data[0].probatioN_PRD || "",
//           conF_EXT: response.data[0].conF_EXT || "",
//           traininG_PRD: response.data[0].traininG_PRD || "",
//           probatioN_DATE: response.data[0].probatioN_DATE || "",
//           proB_EXT: response.data[0].proB_EXT || "",
//           noticE_PRD: response.data[0].noticE_PRD || "",
//         });
//       })
//       .catch((error) => {
//         console.error("Error fetching basic details:", error);
//       });
//   }, []);

//   const validationSchema = Yup.object().shape({
//     datE_JOIN: Yup.date().required("Date of join is required"),
//     // joineD_AS: Yup.string().required("Joined as is required"),
//     // joininG_CTC: Yup.number().required("Joining CTC is required"),
//     // reporT_TO: Yup.string().required("Report To is required"),
//     // employmenT_TYPE: Yup.string().required("Employment Type is required"),
//     // datE_CONF: Yup.date().required("Date of Confirmation is required"),
//     // probatioN_PRD: Yup.number()
//     //   // .required("Joining CTC is required")
//     //   .positive("Joining CTC must be positive"), // Optional: Ensure positive value
//     //  // Optional: Ensure non-negative value
//     //   conF_EXT: Yup.number()
//     //   // .required("Current CTC is required")
//     //   .positive("Current CTC must be positive") ,// Optional: Ensure positive value
//     //   // Optional: Ensure non-negative value
//     //   proB_EXT: Yup.number()
//     // .required("Current CTC is required")
//     // .positive("Current CTC must be positive") ,
//     // traininG_PRD: Yup.number()
//     // .required("Macleods Experience is required")
//     // .positive("Macleods Experience must be positive"), // Optional: Ensure positive value
//     // probatioN_DATE: Yup.date().required("Probation Date is required"),
//     // Optional: Ensure positive value
//     // emaiL_ID_OFF: Yup.string().email("Invalid email format").required("Official Email is required"),

//     probatioN_PRD: Yup.date().required("Confirmation months is required"),
//     conF_EXT: Yup.date().required("Confirmation extension is required"),
//     traininG_PRD: Yup.date().required("Probation months is required"),
//     proB_EXT: Yup.date().required("Probation extension is required"),
//   });

//   const formik = useFormik({
//     initialValues: {
//       datE_JOIN: "",
//       joineD_AS: "",
//       joininG_CTC: "",
//       reporT_TO: "",
//       employmenT_TYPE: "",
//       datE_CONF: "",
//       probatioN_PRD: "",
//       conF_EXT: "",
//       traininG_PRD: "",
//       probatioN_DATE: "",
//       proB_EXT: "",
//       noticE_PRD: "",
//     },
//     validationSchema: validationSchema,
//     // onSubmit: (values) => {
//     //   console.log('Submitting form data:', values);
//     //   // Implement your submit logic here, e.g., send updated data to the server

//     //   setEmployee({ sitE_NAME: '', depT_NAME: '' ,designation:'',section:""});
//     //   formik.resetForm();// Upon successful submission, clear the form
//     // },

//     onSubmit: (values, formik) => {
//       console.log("Submitting form data:", values);

//       axios
//         .post<Response>("your_backend_endpoint", values)
//         .then((response) => {
//           console.log("Joining Details Posted successfully!", response.data);
//           if (response.data.responseStatus === "E") {
//             toast.error(
//               <div>
//                 <h4 className="alert-heading">Failed to submit Form...</h4>
//                 <p className="alert alert-danger" role="alert">
//                   <h6>
//                     {Array.isArray(response.data.message)
//                       ? response.data.message.join("\n")
//                       : response.data.message}
//                   </h6>
//                 </p>
//               </div>
//             );
//           } else {
//             toast.success(
//               <div>
//                 <h4 className="alert-heading">
//                   Successfully Submitted Form...
//                 </h4>
//                 <p className="alert alert-success" role="alert">
//                   <h6>
//                     {Array.isArray(response.data.message)
//                       ? response.data.message.join("\n")
//                       : response.data.message}
//                   </h6>
//                 </p>
//               </div>
//             );

//             // Clear employee data and reset form
//             setEmployee({
//               datE_JOIN: "",
//               joineD_AS: "",
//               joininG_CTC: "",
//               reporT_TO: "",
//               employmenT_TYPE: "",
//               datE_CONF: "",
//               probatioN_PRD: "",
//               conF_EXT: "",
//               traininG_PRD: "",
//               probatioN_DATE: "",
//               proB_EXT: "",
//               noticE_PRD: "",
//             });
//             formik.resetForm();
//             navigate(-1); // Redirect to previous page or desired location
//           }
//         })
//         .catch((error) => {
//           console.error("Error posting data:", error);
//           toast.error(
//             <div>
//               <h6 className="alert-heading">Error Posting Data</h6>
//               <p className="alert alert-danger" role="alert">
//                 <h6>{error.message}</h6>
//               </p>
//             </div>
//           );
//         });
//     },
//   });

//   const handleClose = () => {
//     navigate(-1);
//   };

//   return (
//     <div className="card mb-1 mb-xl-2">
//       <div
//         className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
//         role="button"
//         data-bs-target="#kt_account_profile_details"
//         aria-expanded="true"
//         aria-controls="kt_account_profile_details"
//       >
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Edit Joining Details</h3>
//         </div>
//         <button
//           type="button"
//           className="btn-close"
//           aria-label="Close"
//           onClick={handleClose}
//           style={{ marginRight: "10px", cursor: "pointer" }}
//         ></button>
//       </div>

//       <div id="kt_account_profile_details" className="collapse show">
//         <form onSubmit={formik.handleSubmit} className="form">
//           <div className="card-body border-top p-9">
//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="datE_JOIN"
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   JOINING DATE :
//                 </label>
//                 <input
//                   type="date"
//                   id="datE_JOIN"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="datE_JOIN"
//                   placeholder="Enter date of joining"
//                   value={
//                     formik.values?.datE_JOIN
//                       ? new Date(formik.values?.datE_JOIN)
//                           .toISOString()
//                           .split("T")[0]
//                       : ""
//                   }
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   {" "}
//                   JOINED AS :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="joineD_AS"
//                   placeholder="Enter joinned as"
//                   value={formik.values.joineD_AS?.toUpperCase()}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.datE_JOIN && formik.errors.datE_JOIN ? (
//                   <div className="text-danger">{formik.errors.datE_JOIN}</div>
//                 ) : null}
//               </div>

//               <div className="col-lg-6">
//                 {formik.touched.joineD_AS && formik.errors.joineD_AS ? (
//                   <div className="text-danger">{formik.errors.joineD_AS}</div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="joininG_CTC"
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   JOINING CTC :
//                 </label>
//                 <input
//                   type="text"
//                   id="joininG_CTC"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="joininG_CTC"
//                   placeholder="Enter date of joining"
//                   value={formik.values?.joininG_CTC}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   {" "}
//                   REPORT TO :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   name="reporT_TO"
//                   placeholder="Enter joinned as"
//                   value={formik.values.reporT_TO?.toUpperCase()}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.joininG_CTC && formik.errors.joininG_CTC ? (
//                   <div className="text-danger">{formik.errors.joininG_CTC}</div>
//                 ) : null}
//               </div>

//               <div className="col-lg-6">
//                 {formik.touched.reporT_TO && formik.errors.reporT_TO ? (
//                   <div className="text-danger">{formik.errors.reporT_TO}</div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="employmenT_TYPE"
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   EMPLOYMENT TYPE :
//                 </label>
//                 <input
//                   type="text"
//                   id="employmenT_TYPE"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="employmenT_TYPE"
//                   placeholder="Enter employment type"
//                   value={formik.values?.employmenT_TYPE}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="noticE_PRD"
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   NOTICE PERIOD(Days) :
//                 </label>

//                 <input
//                   type="number"
//                   id="noticE_PRD"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="noticE_PRD"
//                   placeholder="Enter employment type"
//                   value={formik.values?.noticE_PRD}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.employmenT_TYPE &&
//                 formik.errors.employmenT_TYPE ? (
//                   <div className="text-danger">
//                     {formik.errors.employmenT_TYPE}
//                   </div>
//                 ) : null}
//               </div>

//               <div className="col-lg-6">
//                 {formik.touched.noticE_PRD && formik.errors.noticE_PRD ? (
//                   <div className="text-danger">{formik.errors.noticE_PRD}</div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-4 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   PROBATION DATE :
//                 </label>
//                 <input
//                   type="date"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="probatioN_DATE"
//                   placeholder="Enter probation date"
//                   value={
//                     formik.values?.probatioN_DATE
//                       ? new Date(formik.values?.probatioN_DATE)
//                           .toISOString()
//                           .split("T")[0]
//                       : ""
//                   }
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>

//               <div className="col-lg-4 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="traininG_PRD"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   PROBATION MONTHS :
//                 </label>
//                 <input
//                   type="number"
//                   id="traininG_PRD"
//                   className="form-control form-control-lg form-control-solid"
//                   name="traininG_PRD"
//                   placeholder="Enter probation months"
//                   value={formik.values?.traininG_PRD}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                   min={1}
//                   max={12}
//                 />
//               </div>

//               <div className="col-lg-4 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="proB_EXT"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   PROBATION EXTENSION :
//                 </label>
//                 <input
//                   type="number"
//                   id="proB_EXT"
//                   className="form-control form-control-lg form-control-solid"
//                   name="proB_EXT"
//                   placeholder="Enter probation extension"
//                   value={formik.values?.proB_EXT}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-4">
//                 {formik.touched.probatioN_DATE &&
//                 formik.errors.probatioN_DATE ? (
//                   <div className="text-danger">
//                     {formik.errors.probatioN_DATE}
//                   </div>
//                 ) : null}
//               </div>
//               <div className="col-lg-4">
//                 {formik.touched.traininG_PRD && formik.errors.traininG_PRD ? (
//                   <div className="text-danger">
//                     {formik.errors.traininG_PRD}
//                   </div>
//                 ) : null}
//               </div>
//               <div className="col-lg-4">
//                 {formik.touched.proB_EXT && formik.errors.proB_EXT ? (
//                   <div className="text-danger">{formik.errors.proB_EXT}</div>
//                 ) : null}
//               </div>
//             </div>
//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-4 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   CONFIRMATION DATE :
//                 </label>
//                 <input
//                   type="date"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="datE_CONF"
//                   placeholder="Select confirmation date"
//                   value={
//                     formik.values?.datE_CONF
//                       ? new Date(formik.values?.datE_CONF)
//                           .toISOString()
//                           .split("T")[0]
//                       : ""
//                   }
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-4 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="probatioN_PRD"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   CONFIRMATION MONTHS :
//                 </label>
//                 <input
//                   type="number"
//                   id="probatioN_PRD"
//                   className="form-control form-control-lg form-control-solid"
//                   name="probatioN_PRD"
//                   placeholder="Enter confirmation months"
//                   value={formik.values?.probatioN_PRD}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                   min={1}
//                   max={12}
//                 />
//               </div>
//               <div className="col-lg-4 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   CONFIRMATION EXTENSION :
//                 </label>
//                 <input
//                   type="number"
//                   className="form-control form-control-lg form-control-solid"
//                   name="conF_EXT"
//                   placeholder="Enter confirmation extension"
//                   value={formik.values?.conF_EXT}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-4">
//                 {formik.touched.datE_CONF && formik.errors.datE_CONF ? (
//                   <div className="text-danger">{formik.errors.datE_CONF}</div>
//                 ) : null}
//               </div>
//               <div className="col-lg-4">
//                 {formik.touched.probatioN_PRD && formik.errors.probatioN_PRD ? (
//                   <div className="text-danger">
//                     {formik.errors.probatioN_PRD}
//                   </div>
//                 ) : null}
//               </div>
//               <div className="col-lg-4">
//                 {formik.touched.conF_EXT && formik.errors.conF_EXT ? (
//                   <div className="text-danger">{formik.errors.conF_EXT}</div>
//                 ) : null}
//               </div>
//             </div>
//             {/* End: Edit Row */}
//           </div>

//           {/* Logic for Submit is HERE */}
//           <div className="card-footer d-flex justify-content-center py-6 px-9">
//             <button
//               type="button"
//               className="btn btn-light me-2"
//               onClick={handleClose}
//             >
//               <span className="indicator-label">Discard</span>
//             </button>

//             <button
//               type="submit"
//               className="btn btn-primary"
//               disabled={formik.isSubmitting || !formik.isValid}
//             >
//               <span className="indicator-label">Submit</span>
//             </button>
//           </div>
//         </form>
//       </div>
//     </div>
//   );
// };

// export default JoiningDetailsEdit;
import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { KTIcon, Mode, QUERIES } from "../../../../_metronic/helpers";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
import { useListView } from "../../apps/transaction-master/master-list/core/ListViewProvider";
import clsx from "clsx";
import { useMutation, useQueryClient } from "react-query";
import {
  insertTransaction,
  insertTransactionV2,
} from "../../apps/transaction-master/master-list/core/_requests";
import { useQueryResponse } from "../../apps/employee/employee-list/core/QueryResponseProvider";

type FieldDetail = {
  olD_KEY: string;
  olD_VALUE: string;
  neW_KEY: string;
  neW_VALUE: string;
  FILE_PATH: string;
};

type JoiningDetails = {
  emP_CODE: string;
  datE_JOIN: string;
  joineD_AS: string;
  joininG_CTC: string;
  reporT_TO: string;
  employmenT_TYPE: string;
  datE_CONF: string;
  probatioN_PRD: string;
  conF_EXT: string;
  traininG_PRD: string;
  probatioN_DATE: string;
  proB_EXT: string;
  noticE_PRD: string;
  cHG_REASON: string;

  fieldNames?: {
    EMP_CODE: FieldDetail;
    DATE_JOIN: FieldDetail;
    JOINED_AS: FieldDetail;
    JOINING_CTC: FieldDetail;
    REPORT_TO: FieldDetail;
    EMPLOYMENT_TYPE: FieldDetail;
    DATE_CONF: FieldDetail;
    PROBATION_PRD: FieldDetail;
    CONF_EXT: FieldDetail;
    TRAINING_PRD: FieldDetail;
    PROBATION_DATE: FieldDetail;
    PROB_EXT: FieldDetail;
    NOTICE_PRD: FieldDetail;
    CHG_REASON: FieldDetail;
  };
};

const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const JoiningDetailsEdit: React.FC = () => {
  const { currentUser } = useAuth();
  const [privateIP, setPrivateIP] = useState();

  const { itemIdForUpdate, setItemIdForUpdate, formMode } = useListView();
  const [empCodeTxForView, setEmpCodeTxForView] = useState();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();
  const navigate = useNavigate();

  const [joiningDetails, setJoiningDetails] = useState<JoiningDetails>();
  const [joiningDetailsTx, setJoiningDetailsTx] = useState<JoiningDetails>();

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));

    itemIdForUpdate && fetchJoiningDetailsTx();
    fetchJoiningDetails();
  }, [empCodeTxForView]);

  const fetchJoiningDetails = () => {
    axios
      .get(
        `${EMPLOYEE_URL}/${
          employeeIdForView ? employeeIdForView : empCodeTxForView
        }/EMPPER`
      )
      .then((response) => {
        setJoiningDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching BASIC details:", error);
      });
  };

  const fetchJoiningDetailsTx = () => {
    axios
      .get(`${MASTERTX_URL}/GetTransactionByID?tranId=${itemIdForUpdate}`)
      .then((response) => {
        setEmpCodeTxForView(
          response.data.transactions[0].fieldNames?.EMP_CODE?.neW_VALUE
        );

        setJoiningDetailsTx(response.data.transactions[0]);
      });
  };

  useEffect(() => {
    formik.setValues({
      emP_CODE:
        joiningDetailsTx?.fieldNames?.EMP_CODE?.neW_KEY ||
        joiningDetails?.emP_CODE ||
        "",

      emP_CODE_O:
        joiningDetailsTx?.fieldNames?.EMP_CODE?.neW_KEY ||
        joiningDetails?.emP_CODE ||
        "",

      datE_JOIN:
        joiningDetailsTx?.fieldNames?.DATE_JOIN?.neW_KEY ||
        joiningDetails?.datE_JOIN ||
        "",

      datE_JOIN_O:
        joiningDetailsTx?.fieldNames?.DATE_JOIN?.neW_KEY ||
        joiningDetails?.datE_JOIN ||
        "",

      joineD_AS:
        joiningDetailsTx?.fieldNames?.JOINED_AS?.neW_KEY ||
        joiningDetails?.joineD_AS ||
        "",
      joineD_AS_O:
        joiningDetailsTx?.fieldNames?.JOINED_AS?.neW_KEY ||
        joiningDetails?.joineD_AS ||
        "",

      joininG_CTC:
        joiningDetailsTx?.fieldNames?.JOINING_CTC?.neW_KEY.toString() ||
        joiningDetails?.joininG_CTC.toString() ||
        "",
      joininG_CTC_O:
        joiningDetailsTx?.fieldNames?.JOINING_CTC?.neW_KEY.toString() ||
        joiningDetails?.joininG_CTC.toString() ||
        "",

      reporT_TO:
        joiningDetailsTx?.fieldNames?.REPORT_TO?.neW_KEY ||
        joiningDetails?.reporT_TO ||
        "",
      reporT_TO_O:
        joiningDetailsTx?.fieldNames?.REPORT_TO?.neW_KEY ||
        joiningDetails?.reporT_TO ||
        "",

      employmenT_TYPE:
        joiningDetailsTx?.fieldNames?.EMPLOYMENT_TYPE?.neW_KEY ||
        joiningDetails?.employmenT_TYPE ||
        "",
      employmenT_TYPE_O:
        joiningDetailsTx?.fieldNames?.EMPLOYMENT_TYPE?.neW_KEY ||
        joiningDetails?.employmenT_TYPE ||
        "",

      datE_CONF:
        joiningDetailsTx?.fieldNames?.DATE_CONF?.neW_KEY ||
        joiningDetails?.datE_CONF ||
        "",
      datE_CONF_O:
        joiningDetailsTx?.fieldNames?.DATE_CONF?.neW_KEY ||
        joiningDetails?.datE_CONF ||
        "",

      probatioN_PRD:
        joiningDetailsTx?.fieldNames?.PROBATION_PRD?.neW_KEY.toString() ||
        joiningDetails?.probatioN_PRD.toString() ||
        "",
      probatioN_PRD_O:
        joiningDetailsTx?.fieldNames?.PROBATION_PRD?.neW_KEY ||
        joiningDetails?.probatioN_PRD ||
        "",

      conF_EXT:
        joiningDetailsTx?.fieldNames?.CONF_EXT?.neW_KEY.toString() ||
        joiningDetails?.conF_EXT.toString() ||
        "",
      conF_EXT_O:
        joiningDetailsTx?.fieldNames?.CONF_EXT?.neW_KEY ||
        joiningDetails?.conF_EXT ||
        "",

      traininG_PRD:
        joiningDetailsTx?.fieldNames?.TRAINING_PRD?.neW_KEY.toString() ||
        joiningDetails?.traininG_PRD.toString() ||
        "",
      traininG_PRD_O:
        joiningDetailsTx?.fieldNames?.TRAINING_PRD?.neW_KEY ||
        joiningDetails?.traininG_PRD ||
        "",

      probatioN_DATE:
        joiningDetailsTx?.fieldNames?.PROBATION_DATE?.neW_KEY ||
        joiningDetails?.probatioN_DATE ||
        "",
      probatioN_DATE_O:
        joiningDetailsTx?.fieldNames?.PROBATION_DATE?.neW_KEY ||
        joiningDetails?.probatioN_DATE ||
        "",

      proB_EXT:
        joiningDetailsTx?.fieldNames?.PROB_EXT?.neW_KEY.toString() ||
        joiningDetails?.proB_EXT.toString() ||
        "",
      proB_EXT_O:
        joiningDetailsTx?.fieldNames?.PROB_EXT?.neW_KEY ||
        joiningDetails?.proB_EXT ||
        "",

      noticE_PRD:
        joiningDetailsTx?.fieldNames?.NOTICE_PRD?.neW_KEY.toString() ||
        joiningDetails?.noticE_PRD.toString() ||
        "",
      noticE_PRD_O:
        joiningDetailsTx?.fieldNames?.NOTICE_PRD?.neW_KEY.toString() ||
        joiningDetails?.noticE_PRD.toString() ||
        "",
      CHG_REASON: formMode === Mode.EDIT ? "" : joiningDetails?.cHG_REASON,
    });
  }, [joiningDetails]);

  const validationSchema = Yup.object().shape({
    datE_JOIN: Yup.string().required("Date of joining is required"),
    joineD_AS: Yup.string().required("Joinned as field is required"),
    joininG_CTC: Yup.string().required("Joining CTC is required"),
    reporT_TO: Yup.string().required("Report to is required"),
    employmenT_TYPE: Yup.string().required("Employmet type is required"),
    datE_CONF: Yup.string().required("Date of confirmation is required"),
    probatioN_PRD: Yup.string().required("Probation period is required"),
    conF_EXT: Yup.string().required("Confirmation extension is required"),
    traininG_PRD: Yup.string().required("Traning period is required"),
    probatioN_DATE: Yup.string().required("Probation date is required"),
    proB_EXT: Yup.string().required("Probation extension is required"),
    noticE_PRD: Yup.string().required("Notice period is required"),
  });

  const createTx = useMutation((payload: any) => insertTransactionV2(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        handleClose();
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formik = useFormik({
    initialValues: {
      emP_CODE: "",
      emP_CODE_O: "",

      datE_JOIN: "",
      datE_JOIN_O: "",

      joineD_AS: "",
      joineD_AS_O: "",

      joininG_CTC: "",
      joininG_CTC_O: "",

      reporT_TO: "",
      reporT_TO_O: "",

      employmenT_TYPE: "",
      employmenT_TYPE_O: "",

      datE_CONF: "",
      datE_CONF_O: "",

      probatioN_PRD: "",
      probatioN_PRD_O: "",

      conF_EXT: "",
      conF_EXT_O: "",

      traininG_PRD: "",
      traininG_PRD_O: "",

      probatioN_DATE: "",
      probatioN_DATE_O: "",

      proB_EXT: "",
      proB_EXT_O: "",

      noticE_PRD: "",
      noticE_PRD_O: "",

      CHG_REASON: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values, formik) => {
      const payload = {
        values,

        TABLENAME: "EMPLOYEE_JOINING",
        KEYVAR: "",
        WFLEVEL: 1,
        CONF: "N",
        SITECODE: "S0001",
        EMP: currentUser?.id?.toString() || "",
        FILEPATH: null,
        FILE: null,
        FOLDERPATH: null,
        FORMNAME: "Joining Details",
        tX_CHG_USER: currentUser?.id?.toString() || "",
        tX_CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        STRTRANID: itemIdForUpdate ? itemIdForUpdate : "",
      };

      createTx.mutate(payload);
      formik.setSubmitting(false);
    },
  });

  const handleClose = () => {
    itemIdForUpdate ? setItemIdForUpdate(undefined) : navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Joining Details</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: final outer Edit Row */}
            <div className="row mb-1">
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="datE_JOIN"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    JOINING DATE:
                  </label>
                  <input
                    type="date"
                    id="datE_JOIN"
                    name="datE_JOIN"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          joiningDetailsTx?.fieldNames?.DATE_JOIN?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.datE_JOIN && formik.errors.datE_JOIN,
                      },
                      {
                        "is-valid":
                          formik.touched.datE_JOIN && !formik.errors.datE_JOIN,
                      }
                    )}
                    {...(joiningDetailsTx?.fieldNames?.DATE_JOIN && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        joiningDetailsTx?.fieldNames?.DATE_JOIN?.olD_VALUE ===
                        null
                          ? "-"
                          : joiningDetailsTx?.fieldNames?.DATE_JOIN?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter joining date"
                    // value={formik.values.datE_JOIN}
                    value={
                      formik.values?.datE_JOIN
                        ? new Date(formik.values?.datE_JOIN)
                            .toISOString()
                            .split("T")[0]
                        : ""
                    }
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.datE_JOIN && formik.errors.datE_JOIN && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.datE_JOIN}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="joineD_AS"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    JOINNED AS:
                  </label>
                  <input
                    type="text"
                    id="joineD_AS"
                    name="joineD_AS"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          joiningDetailsTx?.fieldNames?.JOINED_AS?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.joineD_AS && formik.errors.joineD_AS,
                      },
                      {
                        "is-valid":
                          formik.touched.joineD_AS && !formik.errors.joineD_AS,
                      }
                    )}
                    {...(joiningDetailsTx?.fieldNames?.JOINED_AS && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        joiningDetailsTx?.fieldNames?.JOINED_AS?.olD_VALUE ===
                        null
                          ? "-"
                          : joiningDetailsTx?.fieldNames?.JOINED_AS?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter joinned as field"
                    value={formik.values.joineD_AS}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.joineD_AS && formik.errors.joineD_AS && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.joineD_AS}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="joininG_CTC"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    JOINING CTC:
                  </label>
                  <input
                    type="number"
                    id="joininG_CTC"
                    name="joininG_CTC"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          joiningDetailsTx?.fieldNames?.JOINING_CTC?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.joininG_CTC &&
                          formik.errors.joininG_CTC,
                      },
                      {
                        "is-valid":
                          formik.touched.joininG_CTC &&
                          !formik.errors.joininG_CTC,
                      }
                    )}
                    {...(joiningDetailsTx?.fieldNames?.JOINING_CTC && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        joiningDetailsTx?.fieldNames?.JOINING_CTC?.olD_VALUE ===
                        null
                          ? "-"
                          : joiningDetailsTx?.fieldNames?.JOINING_CTC?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter joining CTC"
                    value={formik.values.joininG_CTC}
                    // onChange={formik.handleChange}
                    onChange={(e) =>
                      formik.setFieldValue(
                        "joininG_CTC",
                        e.target.value.toString()
                      )
                    }
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.joininG_CTC && formik.errors.joininG_CTC && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.joininG_CTC}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="reporT_TO"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    REPORT TO:
                  </label>
                  <input
                    type="text"
                    id="reporT_TO"
                    name="reporT_TO"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          joiningDetailsTx?.fieldNames?.REPORT_TO?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.reporT_TO && formik.errors.reporT_TO,
                      },
                      {
                        "is-valid":
                          formik.touched.reporT_TO && !formik.errors.reporT_TO,
                      }
                    )}
                    {...(joiningDetailsTx?.fieldNames?.REPORT_TO && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        joiningDetailsTx?.fieldNames?.REPORT_TO?.olD_VALUE ===
                        null
                          ? "-"
                          : joiningDetailsTx?.fieldNames?.REPORT_TO?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Report to field"
                    value={formik.values.reporT_TO}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.reporT_TO && formik.errors.reporT_TO && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.reporT_TO}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="employmenT_TYPE"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    EMPLOYMENT TYPE:
                  </label>
                  <input
                    type="text"
                    id="employmenT_TYPE"
                    name="employmenT_TYPE"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          joiningDetailsTx?.fieldNames?.EMPLOYMENT_TYPE
                            ?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.employmenT_TYPE &&
                          formik.errors.employmenT_TYPE,
                      },
                      {
                        "is-valid":
                          formik.touched.employmenT_TYPE &&
                          !formik.errors.employmenT_TYPE,
                      }
                    )}
                    {...(joiningDetailsTx?.fieldNames?.EMPLOYMENT_TYPE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        joiningDetailsTx?.fieldNames?.EMPLOYMENT_TYPE
                          ?.olD_VALUE === null
                          ? "-"
                          : joiningDetailsTx?.fieldNames?.EMPLOYMENT_TYPE
                              ?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Employment type"
                    value={formik.values.employmenT_TYPE}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.employmenT_TYPE &&
                  formik.errors.employmenT_TYPE && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.employmenT_TYPE}</span>
                    </div>
                  )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="noticE_PRD"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    NOTICE PERIOD:
                  </label>
                  <input
                    type="number"
                    id="noticE_PRD"
                    name="noticE_PRD"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          joiningDetailsTx?.fieldNames?.NOTICE_PRD?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.noticE_PRD && formik.errors.noticE_PRD,
                      },
                      {
                        "is-valid":
                          formik.touched.noticE_PRD &&
                          !formik.errors.noticE_PRD,
                      }
                    )}
                    {...(joiningDetailsTx?.fieldNames?.NOTICE_PRD && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        joiningDetailsTx?.fieldNames?.NOTICE_PRD?.olD_VALUE ===
                        null
                          ? "-"
                          : joiningDetailsTx?.fieldNames?.NOTICE_PRD?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Notice period"
                    value={formik.values.noticE_PRD}
                    onChange={(e) =>
                      formik.setFieldValue(
                        "noticE_PRD",
                        e.target.value.toString()
                      )
                    }
                    // onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  <span className="input-group-text">Days</span>
                </div>

                {formik.touched.noticE_PRD && formik.errors.noticE_PRD && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.noticE_PRD}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-4">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="probatioN_DATE"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PROBATION DATE:
                  </label>
                  <input
                    type="date"
                    id="probatioN_DATE"
                    name="probatioN_DATE"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          joiningDetailsTx?.fieldNames?.PROBATION_DATE
                            ?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.probatioN_DATE &&
                          formik.errors.probatioN_DATE,
                      },
                      {
                        "is-valid":
                          formik.touched.probatioN_DATE &&
                          !formik.errors.probatioN_DATE,
                      }
                    )}
                    {...(joiningDetailsTx?.fieldNames?.PROBATION_DATE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        joiningDetailsTx?.fieldNames?.PROBATION_DATE
                          ?.olD_VALUE === null
                          ? "-"
                          : joiningDetailsTx?.fieldNames?.PROBATION_DATE
                              ?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter probation date"
                    // value={formik.values.probatioN_DATE}
                    value={
                      formik.values?.probatioN_DATE
                        ? new Date(formik.values?.probatioN_DATE)
                            .toISOString()
                            .split("T")[0]
                        : ""
                    }
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.probatioN_DATE &&
                  formik.errors.probatioN_DATE && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.probatioN_DATE}</span>
                    </div>
                  )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-4">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="traininG_PRD"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PROBATION MONTHS:
                  </label>
                  <input
                    type="number"
                    id="traininG_PRD"
                    name="traininG_PRD"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          joiningDetailsTx?.fieldNames?.TRAINING_PRD?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.traininG_PRD &&
                          formik.errors.traininG_PRD,
                      },
                      {
                        "is-valid":
                          formik.touched.traininG_PRD &&
                          !formik.errors.traininG_PRD,
                      }
                    )}
                    {...(joiningDetailsTx?.fieldNames?.TRAINING_PRD && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        joiningDetailsTx?.fieldNames?.TRAINING_PRD
                          ?.olD_VALUE === null
                          ? "-"
                          : joiningDetailsTx?.fieldNames?.TRAINING_PRD
                              ?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter probation months"
                    value={formik.values.traininG_PRD}
                    // onChange={formik.handleChange}
                    onChange={(e) =>
                      formik.setFieldValue(
                        "traininG_PRD",
                        e.target.value.toString()
                      )
                    }
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.traininG_PRD && formik.errors.traininG_PRD && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.traininG_PRD}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-4">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="proB_EXT"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PROBATION EXTENSION:
                  </label>
                  <input
                    type="number"
                    id="proB_EXT"
                    name="proB_EXT"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          joiningDetailsTx?.fieldNames?.PROB_EXT?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.proB_EXT && formik.errors.proB_EXT,
                      },
                      {
                        "is-valid":
                          formik.touched.proB_EXT && !formik.errors.proB_EXT,
                      }
                    )}
                    {...(joiningDetailsTx?.fieldNames?.PROB_EXT && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        joiningDetailsTx?.fieldNames?.PROB_EXT?.olD_VALUE ===
                        null
                          ? "-"
                          : joiningDetailsTx?.fieldNames?.PROB_EXT?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter probation months"
                    value={formik.values.proB_EXT}
                    // onChange={formik.handleChange}
                    onChange={(e) =>
                      formik.setFieldValue(
                        "proB_EXT",
                        e.target.value.toString()
                      )
                    }
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.proB_EXT && formik.errors.proB_EXT && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.proB_EXT}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-4">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="datE_CONF"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CONFIRMATION DATE:
                  </label>
                  <input
                    type="date"
                    id="datE_CONF"
                    name="datE_CONF"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          joiningDetailsTx?.fieldNames?.DATE_CONF?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.datE_CONF && formik.errors.datE_CONF,
                      },
                      {
                        "is-valid":
                          formik.touched.datE_CONF && !formik.errors.datE_CONF,
                      }
                    )}
                    {...(joiningDetailsTx?.fieldNames?.DATE_CONF && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        joiningDetailsTx?.fieldNames?.DATE_CONF?.olD_VALUE ===
                        null
                          ? "-"
                          : joiningDetailsTx?.fieldNames?.DATE_CONF?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter date of confirmation"
                    // value={formik.values.datE_CONF}
                    value={
                      formik.values?.datE_CONF
                        ? new Date(formik.values?.datE_CONF)
                            .toISOString()
                            .split("T")[0]
                        : ""
                    }
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.datE_CONF && formik.errors.datE_CONF && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.datE_CONF}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-4">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="probatioN_PRD"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CONFIRMATION MONTHS:
                  </label>
                  <input
                    type="number"
                    id="probatioN_PRD"
                    name="probatioN_PRD"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          joiningDetailsTx?.fieldNames?.PROBATION_PRD
                            ?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.probatioN_PRD &&
                          formik.errors.probatioN_PRD,
                      },
                      {
                        "is-valid":
                          formik.touched.probatioN_PRD &&
                          !formik.errors.probatioN_PRD,
                      }
                    )}
                    {...(joiningDetailsTx?.fieldNames?.PROBATION_PRD && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        joiningDetailsTx?.fieldNames?.PROBATION_PRD
                          ?.olD_VALUE === null
                          ? "-"
                          : joiningDetailsTx?.fieldNames?.PROBATION_PRD
                              ?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter probation months"
                    value={formik.values.probatioN_PRD}
                    // onChange={formik.handleChange}
                    onChange={(e) =>
                      formik.setFieldValue(
                        "probatioN_PRD",
                        e.target.value.toString()
                      )
                    }
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.probatioN_PRD &&
                  formik.errors.probatioN_PRD && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.probatioN_PRD}</span>
                    </div>
                  )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-4">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="conF_EXT"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CONFIRMATION EXTENSION:
                  </label>
                  <input
                    type="number"
                    id="conF_EXT"
                    name="conF_EXT"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          joiningDetailsTx?.fieldNames?.CONF_EXT?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.conF_EXT && formik.errors.conF_EXT,
                      },
                      {
                        "is-valid":
                          formik.touched.conF_EXT && !formik.errors.conF_EXT,
                      }
                    )}
                    {...(joiningDetailsTx?.fieldNames?.CONF_EXT && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        joiningDetailsTx?.fieldNames?.CONF_EXT?.olD_VALUE ===
                        null
                          ? "-"
                          : joiningDetailsTx?.fieldNames?.CONF_EXT?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter confirmation extension"
                    value={formik.values.conF_EXT}
                    // onChange={formik.handleChange}
                    onChange={(e) =>
                      formik.setFieldValue(
                        "conF_EXT",
                        e.target.value.toString()
                      )
                    }
                    onBlur={formik.handleBlur}
                  />
                  {/* <span className="input-group-text">.00</span> */}
                </div>

                {formik.touched.conF_EXT && formik.errors.conF_EXT && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.conF_EXT}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}
            </div>
            {/* end: final outer Edit Row */}
          </div>

          {/* Logic for Submit */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default JoiningDetailsEdit;
