import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { KTIcon } from "../../../../_metronic/helpers";
import { employeeIdForView } from "../../GlobalQuery";
import { ToastContainer } from "react-toastify";
import { InternalMode , ThemeModeComponent } from "../../../../_metronic/assets/ts/layout";

export function KYC() {
  const [basicDetails, setBasicDetails] = useState<any>([]);
  const [isCollapsed, setIsCollapsed] = useState(false);
 const currentMode: InternalMode = ThemeModeComponent.getCurrentMode();
  


const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

  useEffect(() => {
    // Fetch KYC details from the backend API
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
      .then((response) => {
        setBasicDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching KYC details:", error);
      });
  }, []);

  // const toggleCollapse = () => {
  //   setIsCollapsed((prev) => !prev); // Toggle the isCollapsed state
  // };

  return (
    <div className={`card`}
    style={(currentMode === 'dark' ? {background: "#0F1014"} : { background: "#F9F9FA"})}
    >
       <div className={"rounded " + (currentMode === 'dark' ? "bg-light" : "bg-white")}>
      <div
        className="card-header bg-light-primary cursor-pointer ">
        {/* <div className="card-title m-0">
          <h3 className="fw-bolder m-0">KYC Details</h3>
        </div> */}

        <div className="card-title m-0">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                KYC Details
                </h3>
              </div>
        <div className="d-flex justify-content-end">
          <Link
            to="/crafted/pages/profile/EditKYCDetails"
            className="btn btn-sm btn-primary me-3 align-self-center"
          >
            Edit
          </Link>

          {/* <button
          className={`btn btn-sm mt-5 btn-light btn-icon rounded-circle ${
            isCollapsed ? "" : "active"
          }`}
          aria-label={isCollapsed ? "Expand" : "Collapse"}
        >
          <KTIcon
            iconName={isCollapsed ? "arrow-down" : "arrow-up"}
            className={`fs-3 ${isCollapsed ? "text-dark" : "text-dark"}`}
          />
        </button> */}
        </div>
      </div>

      {!isCollapsed && ( // Render card body only if not collapsed
        <div className="card-body p-9">
          {/* Begin:first Row     */}
          <div className="row">
            <div className="col-lg-6 mb-6">
              <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                NAME ON AADHAR :{" "}
              </label>
              <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                {/* {basicDetails.emP_CODE} */}
                {/* make all changes in this format */}
                {basicDetails?.aadhaR_EMP_NAME ? basicDetails?.aadhaR_EMP_NAME.toUpperCase() : ""}
              </span>
            </div>
            <div className="col-lg-6 mb-6">
              <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                AADHAR NO. :{" "}
              </label>
              <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                {basicDetails?.aadhaaR_NO}
              </span>
            </div>
          </div>
          {/* End:first Row     */}

          {/* Begin:first Row     */}
          <div className="row">
            <div className="col-lg-6 mb-6">
              <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                NAME ON PAN :{" "}
              </label>
              <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                {basicDetails?.paN_EMP_NAME ? basicDetails?.paN_EMP_NAME.toUpperCase() : ""}
              </span>
            </div>
            <div className="col-lg-6 mb-6">
              <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">PAN NO. : </label>
              <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                {/* {basicDetails.gendeR_DESCR} */}
              </span>
            </div>
          </div>
          {/* End:first Row     */}

          {/* Begin:first Row     */}
          <div className="row">
            <div className="col-lg-6 mb-6">
              <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                VOTER CARD NO. :{" "}
              </label>
              <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                {basicDetails?.electioN_CARD_NO}
              </span>
            </div>
            <div className="col-lg-6 mb-6">
              <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                DRIVING LICENSE NO. :{" "}
              </label>
              <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                {basicDetails?.driV_LIC_NO}
              </span>
            </div>
          </div>
          {/* End:first Row     */}
          {/* Begin:first Row     */}
          <div className="row">
            <div className="col-lg-6 mb-6">
              <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                PASSPORT NO :{" "}
              </label>
              <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                {basicDetails?.passporT_NO}
              </span>
            </div>
            <div className="col-lg-6 mb-6">
              <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                PASSPORT ISSUE PLACE:{" "}
              </label>
              <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                {basicDetails?.passporT_ISS_PLACE}
              </span>
            </div>
          </div>
          {/* End:first Row     */}

          {/* Begin:first Row     */}
          <div className="row">
            <div className="col-lg-6 mb-6">
              <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                PASSPORT ISSUE DATE :{" "}
              </label>
              <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                {basicDetails?.passporT_ISS_DATE}
              </span>
            </div>
            <div className="col-lg-6 mb-6">
              <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                PASSPORT EXP DATE :{" "}
              </label>
              <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                {basicDetails?.passporT_EXP_DATE}
              </span>
            </div>
          </div>
          {/* End:first Row     */}
        </div>
      )}
      </div>
    </div>
  );
}
