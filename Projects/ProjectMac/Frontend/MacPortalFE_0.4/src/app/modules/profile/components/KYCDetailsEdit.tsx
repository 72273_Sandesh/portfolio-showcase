// import React, { useState, useEffect } from "react";
// import axios from "axios";
// import { useNavigate } from "react-router-dom";
// import { useFormik } from "formik";
// import * as Yup from "yup";
// import { employeeIdForView } from "../../GlobalQuery";
// import { toast } from "react-toastify";
// import { useAuth } from "../../auth";
// const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// const KYCDetailsEdit: React.FC = () => {
//   const [employee, setEmployee] = useState<any>({});
//   const navigate = useNavigate();
//   const [pdf, setPdf] = useState<File | null>(null);
//   const { currentUser } = useAuth();

//   useEffect(() => {
//     axios
//       .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
//       .then((response) => {
//         setEmployee(response.data[0]);
//         formik.setValues({
//           aadhaR_EMP_NAME: response.data[0].aadhaR_EMP_NAME.toUpperCase() || "",
//           aadhaaR_NO: response.data[0].aadhaaR_NO || "",
//           paN_EMP_NAME: response.data[0].paN_EMP_NAME || "",
//           electioN_CARD_NO: response.data[0].electioN_CARD_NO || "",
//           driV_LIC_NO: response.data[0].driV_LIC_NO || "",
//           passporT_NO: response.data[0].passporT_NO || "",
//           passporT_ISS_PLACE: response.data[0].passporT_ISS_PLACE || "",
//           passporT_ISS_DATE: response.data[0].passporT_ISS_DATE || "",
//           passporT_EXP_DATE: response.data[0].passporT_EXP_DATE || "",
//           paN_NO: response.data[0].paN_NO || "",
//           pdf: null,
//         });
//       })
//       .catch((error) => {
//         console.error("Error fetching basic details:", error);
//       });
//   }, []);

//   const validationSchema = Yup.object().shape({
//     aadhaR_EMP_NAME: Yup.string().required("Name on aadhaar is required"),

//     paN_EMP_NAME: Yup.string().required("Name on PAN is required"),

//     passporT_ISS_DATE: Yup.string().required("Passport Issue Date is required"),
//     passporT_EXP_DATE: Yup.string().required(
//       "Passport Expiration Date is required"
//     ),
//   });

//   const formik = useFormik({
//     initialValues: {
//       aadhaR_EMP_NAME: "",
//       aadhaaR_NO: "",
//       paN_EMP_NAME: "",
//       electioN_CARD_NO: "",
//       driV_LIC_NO: "",
//       passporT_NO: "",
//       passporT_ISS_PLACE: "",
//       passporT_ISS_DATE: "",
//       passporT_EXP_DATE: "",
//       paN_NO: "",
//       pdf: null,
//     },
//     validationSchema: validationSchema,
//     onSubmit: (values, formik) => {
//       console.log("Submitting form data:", values);
//       const formData = new FormData();

//       Object.keys(values).forEach((key) => {
//         if (key === "pdf" && pdf) {
//           formData.append(key, pdf);
//         } else {
//           formData.append(key, values[key as keyof typeof values] as string);
//         }
//       });

//       axios
//         .put(`${EMPLOYEE_URL}/UpdateEmployeeKYCDetails/${employeeIdForView}`, {
//           ...values,
//           chG_DATE: new Date(),
//           chG_USER: currentUser?.id, //const { currentUser } = useAuth();
//           chG_TERM: "TERM",
//           // pdf: pdf,
//         })
//         .then((response) => {
//           console.log("Basic Details Posted successfully!", response.data);
//           const { responseStatus, message } = response.data.data;
//           if (responseStatus === "E") {
//             formik.setSubmitting(false);
//             toast.error(
//               <div>
//                 <h4 className="alert-heading">Failed to submit Form...</h4>
//                 <p className="alert alert-danger" role="alert">
//                   <h6>
//                     {Array.isArray(message) ? message.join("\n") : message}
//                   </h6>
//                 </p>
//               </div>
//             );
//           } else {
//             toast.success(
//               <div>
//                 <h4 className="alert-heading">
//                   Successfully Submitted Form...
//                 </h4>
//                 <p className="alert alert-success" role="alert">
//                   <h6>
//                     {Array.isArray(message) ? message.join("\n") : message}
//                   </h6>
//                 </p>
//               </div>
//             );

//             formik.resetForm();
//             navigate(-1);
//           }
//         })
//         .catch((error) => {
//           console.error("Error posting data:", error);
//           toast.error(
//             <div>
//               <h6 className="alert-heading">Error Posting Data</h6>
//               <p className="alert alert-danger" role="alert">
//                 <h6>{error.message}</h6>
//               </p>
//             </div>
//           );

//           formik.setSubmitting(false);
//         });
//     },
//   });

//   const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
//     const file = e.target.files?.[0];
//     if (file) {
//       setPdf(file);
//     }
//   };

//   const handleClose = () => {
//     navigate(-1);
//   };

//   return (
//     <div className="card mb-1 mb-xl-2">
//       <div
//         className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
//         role="button"
//         data-bs-target="#kt_account_profile_details"
//         aria-expanded="true"
//         aria-controls="kt_account_profile_details"
//       >
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Edit KYC Details</h3>
//         </div>
//         <button
//           type="button"
//           className="btn-close"
//           aria-label="Close"
//           onClick={handleClose}
//           style={{ marginRight: "10px", cursor: "pointer" }}
//         ></button>
//       </div>

//       <div id="kt_account_profile_details" className="collapse show">
//         <form onSubmit={formik.handleSubmit} className="form">
//           <div className="card-body border-top p-9">
//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               {/* <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="aadhaR_EMP_NAME"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   Name on Aadhaar:
//                 </label>
//                 <input
//                   type="text"
//                   id="aadhaR_EMP_NAME"
//                   className="form-control form-control-lg form-control-solid"
//                   name="aadhaR_EMP_NAME"
//                   placeholder="Enter name on AADHAAR"
//                   value={formik.values?.aadhaR_EMP_NAME}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div> */}
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="aadhaR_EMP_NAME"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   NAME ON ADHAR :
//                 </label>
//                 <input
//                   name="aadhaR_EMP_NAME"
//                   type="text"
//                   className="form-control form-control-lg form-control-solid me-2"
//                   placeholder="Enter Name on Aadhaar"
//                   value={formik.values?.aadhaR_EMP_NAME}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//                 <input
//                   name="birtH_DATE"
//                   className="form-control form-control-lg flex-grow-1"
//                   type="file"
//                   id="pdf"
//                   onChange={handlePdfChange}
//                   accept="application/pdf"
//                   required
//                 />
//                 {formik.touched.pdf && formik.errors.pdf ? (
//                   <div className="text-danger">{formik.errors.pdf}</div>
//                 ) : null}
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   {" "}
//                   ADHAR NO :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="aadhaaR_NO"
//                   placeholder="Enter Aadhaar No"
//                   value={formik.values.aadhaaR_NO?.toUpperCase()}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.aadhaR_EMP_NAME &&
//                 formik.errors.aadhaR_EMP_NAME ? (
//                   <div className="text-danger">
//                     {formik.errors.aadhaR_EMP_NAME}
//                   </div>
//                 ) : null}
//               </div>

//               <div className="col-lg-6">
//                 {formik.touched.aadhaaR_NO && formik.errors.aadhaaR_NO ? (
//                   <div className="text-danger">{formik.errors.aadhaaR_NO}</div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               {/* <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="paN_EMP_NAME"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   Name on PAN:
//                 </label>
//                 <input
//                   type="text"
//                   id="paN_EMP_NAME"
//                   className="form-control form-control-lg form-control-solid"
//                   name="paN_EMP_NAME"
//                   placeholder="Enter name on PAN"
//                   value={formik.values?.paN_EMP_NAME}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div> */}
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="paN_EMP_NAME"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   NAME ON PAN :
//                 </label>
//                 <input
//                   name="paN_EMP_NAME"
//                   type="text"
//                   className="form-control form-control-lg form-control-solid me-2"
//                   placeholder="Enter Name on PAN"
//                   value={formik.values?.paN_EMP_NAME}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//                 <input
//                   name="birtH_DATE"
//                   className="form-control form-control-lg flex-grow-1"
//                   type="file"
//                   id="pdf"
//                   onChange={handlePdfChange}
//                   accept="application/pdf"
//                   required
//                 />
//                 {formik.touched.pdf && formik.errors.pdf ? (
//                   <div className="text-danger">{formik.errors.pdf}</div>
//                 ) : null}
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   {" "}
//                   PAN NO. :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="paN_NO"
//                   placeholder="Enter PAN No."
//                   value={formik.values?.paN_NO}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.paN_EMP_NAME && formik.errors.paN_EMP_NAME ? (
//                   <div className="text-danger">
//                     {formik.errors.paN_EMP_NAME}
//                   </div>
//                 ) : null}
//               </div>

//               <div className="col-lg-6">
//                 {formik.touched.paN_NO && formik.errors.paN_NO ? (
//                   <div className="text-danger">{formik.errors.paN_NO}</div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="electioN_CARD_NO"
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   VOTER CARD NO. :
//                 </label>
//                 <input
//                   type="text"
//                   id="electioN_CARD_NO"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="electioN_CARD_NO"
//                   placeholder="Enter voter card No."
//                   value={formik.values?.electioN_CARD_NO}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   {" "}
//                   DRIVING LICENCE NO. :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="driV_LIC_NO"
//                   placeholder="Enter Driving License No."
//                   value={formik.values?.driV_LIC_NO?.toUpperCase()}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.electioN_CARD_NO &&
//                 formik.errors.electioN_CARD_NO ? (
//                   <div className="text-danger">
//                     {formik.errors.electioN_CARD_NO}
//                   </div>
//                 ) : null}
//               </div>

//               <div className="col-lg-6">
//                 {formik.touched.driV_LIC_NO && formik.errors.driV_LIC_NO ? (
//                   <div className="text-danger">{formik.errors.driV_LIC_NO}</div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="passporT_NO"
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   PASSPORT NO. :
//                 </label>
//                 <input
//                   type="text"
//                   id="passporT_NO"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="passporT_NO"
//                   placeholder="Enter passport No."
//                   value={formik.values?.passporT_NO}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   {" "}
//                   PASSPORT ISSUE PLACE :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="passporT_ISS_PLACE"
//                   placeholder="Enter passport issue place"
//                   value={formik.values?.passporT_ISS_PLACE?.toUpperCase()}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.passporT_NO && formik.errors.passporT_NO ? (
//                   <div className="text-danger">{formik.errors.passporT_NO}</div>
//                 ) : null}
//               </div>

//               <div className="col-lg-6">
//                 {formik.touched.passporT_ISS_PLACE &&
//                 formik.errors.passporT_ISS_PLACE ? (
//                   <div className="text-danger">
//                     {formik.errors.passporT_ISS_PLACE}
//                   </div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="passporT_ISS_DATE"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   PASSPORT ISSUE DATE :
//                 </label>
//                 <input
//                   type="date"
//                   id="passporT_ISS_DATE"
//                   className="form-control form-control-lg form-control-solid"
//                   name="passporT_ISS_DATE"
//                   placeholder="Enter passport issue date"
//                   value={
//                     formik.values?.passporT_ISS_DATE
//                       ? new Date(formik.values?.passporT_ISS_DATE)
//                           .toISOString()
//                           .split("T")[0]
//                       : ""
//                   }
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   {" "}
//                   PASSPORT EXPIRATION DATE :
//                 </label>
//                 <input
//                   type="date"
//                   className="form-control form-control-lg form-control-solid"
//                   name="passporT_EXP_DATE"
//                   placeholder="Enter passport expiration date"
//                   value={
//                     formik.values?.passporT_EXP_DATE
//                       ? new Date(formik.values?.passporT_EXP_DATE)
//                           .toISOString()
//                           .split("T")[0]
//                       : ""
//                   }
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.passporT_ISS_DATE &&
//                 formik.errors.passporT_ISS_DATE ? (
//                   <div className="text-danger">
//                     {formik.errors.passporT_ISS_DATE}
//                   </div>
//                 ) : null}
//               </div>

//               <div className="col-lg-6">
//                 {formik.touched.passporT_EXP_DATE &&
//                 formik.errors.passporT_EXP_DATE ? (
//                   <div className="text-danger">
//                     {formik.errors.passporT_EXP_DATE}
//                   </div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}
//           </div>

//           {/* Logic for Submit is HERE */}
//           <div className="card-footer d-flex justify-content-center py-6 px-9">
//             <button
//               type="button"
//               className="btn btn-light me-2"
//               onClick={handleClose}
//             >
//               <span className="indicator-label">Discard</span>
//             </button>

//             <button
//               type="submit"
//               className="btn btn-primary"
//               disabled={formik.isSubmitting || !formik.isValid}
//             >
//               <span className="indicator-label">Submit</span>
//             </button>
//           </div>
//         </form>
//       </div>
//     </div>
//   );
// };

// export default KYCDetailsEdit;

import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
import { useListView } from "../../apps/transaction-master/master-list/core/ListViewProvider";
import clsx from "clsx";
import { createTransaction } from "../../apps/transaction-master/master-list/core/_models";
import { Mode, QUERIES } from "../../../../_metronic/helpers";
import { useQueryResponse } from "../../apps/employee/employee-list/core/QueryResponseProvider";
import { useMutation, useQueryClient } from "react-query";
import { insertTransactionV2 } from "../../apps/transaction-master/master-list/core/_requests";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

type FieldDetail = {
  olD_KEY: string;
  olD_VALUE: string;
  neW_KEY: string;
  neW_VALUE: string;
  FILE_PATH: string;
};

type WorkAndPayroll = {
  emP_CODE: string;
  aadhaaR_NO: string;
  aadhaR_EMP_NAME: string;
  paN_NO: string;
  paN_EMP_NAME: string;
  electioN_CARD_NO: string;
  driV_LIC_NO: string;
  passporT_NO: string;
  passporT_ISS_PLACE: string;
  passporT_ISS_DATE: string;
  passporT_EXP_DATE: string;
  chG_REASON: string;

  fieldNames?: {
    EMP_CODE: FieldDetail;
    AADHAAR_NO: FieldDetail;
    AADHAR_EMP_NAME: FieldDetail;
    PAN_NO: FieldDetail;
    PAN_EMP_NAME: FieldDetail;
    ELECTION_CARD_NO: FieldDetail;
    DRIV_LIC_NO: FieldDetail;
    PASSPORT_NO: FieldDetail;
    PASSPORT_ISS_PLACE: FieldDetail;
    PASSPORT_ISS_DATE: FieldDetail;
    PASSPORT_EXP_DATE: FieldDetail;
    CHG_REASON: FieldDetail;
  };
};

const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const KYCDetailsEdit: React.FC = () => {
  const [kycDetails, setKycDetails] = useState<WorkAndPayroll>();
  const [kycDetailsTx, setKycDetailsTx] = useState<WorkAndPayroll>();

  const navigate = useNavigate();

  const { currentUser } = useAuth();
  const [privateIP, setPrivateIP] = useState();

  const { itemIdForUpdate, setItemIdForUpdate, formMode } = useListView();
  const [empCodeTxForView, setEmpCodeTxForView] = useState();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();

  useEffect(() => {
    // itemIdForUpdate !== undefined ?
    itemIdForUpdate && fetchKYCDetailsTx();
    // :
    fetchKYCDetails();
  }, [empCodeTxForView]);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const fetchKYCDetails = () => {
    axios
      .get(
        `${EMPLOYEE_URL}/${
          employeeIdForView ? employeeIdForView : empCodeTxForView
        }/EMPPER`
      )
      .then((response) => {
        setKycDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching BASIC details:", error);
      });
  };

  const fetchKYCDetailsTx = () => {
    axios
      .get(`${MASTERTX_URL}/GetTransactionByID?tranId=${itemIdForUpdate}`)
      .then((response) => {
        setKycDetailsTx(response.data.transactions[0]);
        setEmpCodeTxForView(
          response.data.transactions[0].fieldNames?.EMP_CODE?.neW_VALUE
        );
      });
  };

  useEffect(() => {
    formik.setValues({
      emP_CODE:
        kycDetailsTx?.fieldNames?.EMP_CODE?.neW_KEY ||
        kycDetails?.emP_CODE ||
        "",
      emP_CODE_O:
        kycDetailsTx?.fieldNames?.EMP_CODE?.neW_KEY ||
        kycDetails?.emP_CODE ||
        "",

      aadhaaR_NO:
        kycDetailsTx?.fieldNames?.AADHAAR_NO?.neW_KEY ||
        kycDetails?.aadhaaR_NO ||
        "",
      aadhaaR_NO_O:
        kycDetailsTx?.fieldNames?.AADHAAR_NO?.neW_KEY ||
        kycDetails?.aadhaaR_NO ||
        "",

      aadhaR_EMP_NAME:
        kycDetailsTx?.fieldNames?.AADHAR_EMP_NAME?.neW_KEY ||
        kycDetails?.aadhaR_EMP_NAME ||
        "",
      aadhaR_EMP_NAME_O:
        kycDetailsTx?.fieldNames?.AADHAR_EMP_NAME?.neW_KEY ||
        kycDetails?.aadhaR_EMP_NAME ||
        "",

      paN_NO:
        kycDetailsTx?.fieldNames?.PAN_NO?.neW_KEY || kycDetails?.paN_NO || "",
      paN_NO_O:
        kycDetailsTx?.fieldNames?.PAN_NO?.neW_KEY || kycDetails?.paN_NO || "",

      paN_EMP_NAME:
        kycDetailsTx?.fieldNames?.PAN_EMP_NAME?.neW_KEY ||
        kycDetails?.paN_EMP_NAME ||
        "",
      paN_EMP_NAME_O:
        kycDetailsTx?.fieldNames?.PAN_EMP_NAME?.neW_KEY ||
        kycDetails?.paN_EMP_NAME ||
        "",

      electioN_CARD_NO:
        kycDetailsTx?.fieldNames?.ELECTION_CARD_NO?.neW_KEY ||
        kycDetails?.electioN_CARD_NO ||
        "",
      electioN_CARD_NO_O:
        kycDetailsTx?.fieldNames?.ELECTION_CARD_NO?.neW_KEY ||
        kycDetails?.electioN_CARD_NO ||
        "",

      driV_LIC_NO:
        kycDetailsTx?.fieldNames?.DRIV_LIC_NO?.neW_KEY ||
        kycDetails?.driV_LIC_NO ||
        "",
      driV_LIC_NO_O:
        kycDetailsTx?.fieldNames?.DRIV_LIC_NO?.neW_KEY ||
        kycDetails?.driV_LIC_NO ||
        "",

      passporT_NO:
        kycDetailsTx?.fieldNames?.PASSPORT_NO?.neW_KEY ||
        kycDetails?.passporT_NO ||
        "",
      passporT_NO_O:
        kycDetailsTx?.fieldNames?.PASSPORT_NO?.neW_KEY ||
        kycDetails?.passporT_NO ||
        "",

      passporT_ISS_PLACE:
        kycDetailsTx?.fieldNames?.PASSPORT_ISS_PLACE?.neW_KEY ||
        kycDetails?.passporT_ISS_PLACE ||
        "",
      passporT_ISS_PLACE_O:
        kycDetailsTx?.fieldNames?.PASSPORT_ISS_PLACE?.neW_KEY ||
        kycDetails?.passporT_ISS_PLACE ||
        "",

      passporT_ISS_DATE:
        kycDetailsTx?.fieldNames?.PASSPORT_ISS_DATE?.neW_KEY ||
        kycDetails?.passporT_ISS_DATE ||
        "",
      passporT_ISS_DATE_O:
        kycDetailsTx?.fieldNames?.PASSPORT_ISS_DATE?.neW_KEY ||
        kycDetails?.passporT_ISS_DATE ||
        "",

      passporT_EXP_DATE:
        kycDetailsTx?.fieldNames?.PASSPORT_EXP_DATE?.neW_KEY ||
        kycDetails?.passporT_EXP_DATE ||
        "",
      passporT_EXP_DATE_O:
        kycDetailsTx?.fieldNames?.PASSPORT_EXP_DATE?.neW_KEY ||
        kycDetails?.passporT_EXP_DATE ||
        "",

      chG_REASON: formMode === Mode.EDIT ? "" : kycDetails?.chG_REASON,

      // pdf:
      //   kycDetailsTx?.fieldNames?.BANK_ACCT?.FILE_PATH ||
      //   kycDetails?.pdf ||
      //   "",
    });
  }, [kycDetails, kycDetailsTx]);

  const validationSchema = Yup.object().shape({
    aadhaaR_NO: Yup.string().required("Adhar Number is required"),
    aadhaR_EMP_NAME: Yup.string().required("Name on Adhar is required"),
    // aadhaR_EMP_NAME_FILE: Yup.string().required("Proof attachment is required"),
    paN_NO: Yup.string().required("PAN number is required"),
    paN_EMP_NAME: Yup.string().required("Name on PAN is required"),
    electioN_CARD_NO: Yup.string().required("Voter ID number is required"),
    driV_LIC_NO: Yup.string().required("Payment Type is required"),
    passporT_NO: Yup.string().required("Passport Number is required"),
    passporT_ISS_PLACE: Yup.string().required(
      "Passport Issue Place is required"
    ),
    passporT_ISS_DATE: Yup.string().required("Passport Issue Date is required"),
    passporT_EXP_DATE: Yup.string().required(
      "Passport Expiry Date is required"
    ),
    chG_REASON: Yup.string().required("Change Reason is required"),
    aadhaR_EMP_NAME_FILE: Yup.string().when([], {
      is: () => kycDetails?.aadhaR_EMP_NAME !== formik.values.aadhaR_EMP_NAME,
      then: (schema) => schema.required("Proof attachment is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    paN_EMP_NAME_FILE: Yup.string().when([], {
      is: () => kycDetails?.paN_EMP_NAME !== formik.values.paN_EMP_NAME,
      then: (schema) => schema.required("Proof attachment is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
  });

  const createTx = useMutation((payload: any) => insertTransactionV2(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        handleClose();
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  // const formattedDate = (date: Date) => {
  //   return new Date(date).toLocaleString("en-GB", {
  //     day: "2-digit",
  //     month: "2-digit",
  //     year: "numeric",
  //     hour: "2-digit",
  //     minute: "2-digit",
  //     second: "2-digit",
  //   });
  // };

  const formik = useFormik({
    initialValues: {
      emP_CODE: "",
      emP_CODE_O: "",

      aadhaaR_NO: "",
      aadhaaR_NO_O: "",

      aadhaR_EMP_NAME: "",
      aadhaR_EMP_NAME_O: "",
      aadhaR_EMP_NAME_FILE: "",

      paN_NO: "",
      paN_NO_O: "",

      paN_EMP_NAME: "",
      paN_EMP_NAME_O: "",
      paN_EMP_NAME_FILE: "",

      electioN_CARD_NO: "",
      electioN_CARD_NO_O: "",

      driV_LIC_NO: "",
      driV_LIC_NO_O: "",

      passporT_NO: "",
      passporT_NO_O: "",

      passporT_ISS_PLACE: "",
      passporT_ISS_PLACE_O: "",

      passporT_ISS_DATE: "",
      passporT_ISS_DATE_O: "",

      passporT_EXP_DATE: "",
      passporT_EXP_DATE_O: "",

      chG_REASON: "",
    },
    validationSchema: validationSchema,

    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      const payload = {
        values,

        TABLENAME: "EMPLOYEE_KYC",
        KEYVAR: "",
        WFLEVEL: 1,
        CONF: "N",
        SITECODE: "S0001",
        EMP: currentUser?.id?.toString() || "",
        FILEPATH: null,
        FILE: null,
        FOLDERPATH: null,
        FORMNAME: "KYC Details",
        tX_CHG_USER: currentUser?.id?.toString() || "",
        tX_CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        STRTRANID: itemIdForUpdate ? itemIdForUpdate : null,
      };
      createTx.mutate(payload);

      setSubmitting(false);
    },
  });

  const handlePdfChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    fieldName: string
  ) => {
    const file = e.target.files?.[0];
    if (file) {
      const reader = new FileReader();

      reader.onloadend = () => {
        const base64String = reader.result;
        formik.setFieldValue(fieldName, base64String);
      };

      reader.readAsDataURL(file);
    }
  };

  // const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
  //   const file = e.target.files?.[0];
  //   if (file) {
  //     formik.setFieldValue("pdf", file);
  //     formData.append("pdf", file);
  //   }
  // };

  const handleClose = () => {
    itemIdForUpdate ? setItemIdForUpdate(undefined) : navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit KYC Details</h3>
        </div>

        <div>
          {/* <button
            type="button"
            className="btn btn-light-danger me-10"
            onClick={handleOpenModal}
          >
            Change Bank Details
          </button>
          {isModalOpen && <BankDetailsEditModal onClose={handleCloseModal} />} */}

          <button
            type="button"
            className="btn-close"
            aria-label="Close"
            onClick={handleClose}
            style={{ marginRight: "10px", cursor: "pointer" }}
          ></button>
        </div>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: final outer Edit Row */}
            <div className="row mb-1">
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="aadhaaR_NO"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    ADHAR NO.:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="number"
                      id="aadhaaR_NO"
                      name="aadhaaR_NO"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            kycDetailsTx?.fieldNames?.AADHAAR_NO?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.aadhaaR_NO &&
                            formik.errors.aadhaaR_NO,
                        },
                        {
                          "is-valid":
                            formik.touched.aadhaaR_NO &&
                            !formik.errors.aadhaaR_NO,
                        }
                      )}
                      {...(kycDetailsTx?.fieldNames?.AADHAAR_NO && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          kycDetailsTx?.fieldNames?.AADHAAR_NO?.olD_VALUE ===
                          null
                            ? "-"
                            : kycDetailsTx?.fieldNames?.AADHAAR_NO?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter Adhar Number"
                      value={formik.values.aadhaaR_NO}
                      onChange={(e) =>
                        formik.setFieldValue("aadhaaR_NO", e.target.value)
                      }
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                {formik.touched.aadhaaR_NO && formik.errors.aadhaaR_NO && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.aadhaaR_NO}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="aadhaR_EMP_NAME"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    NAME ON ADHAR:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="text"
                      id="aadhaR_EMP_NAME"
                      name="aadhaR_EMP_NAME"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            kycDetailsTx?.fieldNames?.AADHAR_EMP_NAME
                              ?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.aadhaR_EMP_NAME &&
                            formik.errors.aadhaR_EMP_NAME,
                        },
                        {
                          "is-valid":
                            formik.touched.aadhaR_EMP_NAME &&
                            !formik.errors.aadhaR_EMP_NAME,
                        }
                      )}
                      {...(kycDetailsTx?.fieldNames?.AADHAR_EMP_NAME && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          kycDetailsTx?.fieldNames?.AADHAR_EMP_NAME
                            ?.olD_VALUE === null
                            ? "-"
                            : kycDetailsTx?.fieldNames?.AADHAR_EMP_NAME
                                ?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter Name on Adhar"
                      value={formik.values.aadhaR_EMP_NAME.toUpperCase()}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                    <input
                      name="aadhaR_EMP_NAME_FILE"
                      // className="form-control form-control-lg input-group-text flex-grow-1"
                      className={clsx(
                        "form-control input-group-text flex-grow-1",
                        {
                          "is-invalid":
                            formik.touched.aadhaR_EMP_NAME_FILE &&
                            formik.errors.aadhaR_EMP_NAME_FILE,
                        },
                        {
                          "is-valid":
                            formik.touched.aadhaR_EMP_NAME_FILE &&
                            !formik.errors.aadhaR_EMP_NAME_FILE,
                        }
                      )}
                      type="file"
                      id="pdf"
                      onChange={(e) => {
                        handlePdfChange(e, "aadhaR_EMP_NAME_FILE");
                      }}
                      accept="application/pdf"
                    />
                  </div>
                </div>
                {formik.touched.aadhaR_EMP_NAME &&
                  formik.errors.aadhaR_EMP_NAME && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.aadhaR_EMP_NAME}</span>
                    </div>
                  )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="paN_NO"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PAN NO.:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="text"
                      id="paN_NO"
                      name="paN_NO"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            kycDetailsTx?.fieldNames?.PAN_NO?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.paN_NO && formik.errors.paN_NO,
                        },
                        {
                          "is-valid":
                            formik.touched.paN_NO && !formik.errors.paN_NO,
                        }
                      )}
                      {...(kycDetailsTx?.fieldNames?.PAN_NO && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          kycDetailsTx?.fieldNames?.PAN_NO?.olD_VALUE === null
                            ? "-"
                            : kycDetailsTx?.fieldNames?.PAN_NO?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter PAN Number"
                      value={formik.values.paN_NO}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                {formik.touched.paN_NO && formik.errors.paN_NO && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.paN_NO}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="paN_EMP_NAME"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    NAME ON PAN:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="text"
                      id="paN_EMP_NAME"
                      name="paN_EMP_NAME"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            kycDetailsTx?.fieldNames?.PAN_EMP_NAME?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.paN_EMP_NAME &&
                            formik.errors.paN_EMP_NAME,
                        },
                        {
                          "is-valid":
                            formik.touched.paN_EMP_NAME &&
                            !formik.errors.paN_EMP_NAME,
                        }
                      )}
                      {...(kycDetailsTx?.fieldNames?.PAN_EMP_NAME && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          kycDetailsTx?.fieldNames?.PAN_EMP_NAME?.olD_VALUE ===
                          null
                            ? "-"
                            : kycDetailsTx?.fieldNames?.PAN_EMP_NAME?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter Name on PAN"
                      value={formik.values.paN_EMP_NAME.toUpperCase()}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                    <input
                      name="birtH_DATE"
                      className="form-control form-control-lg input-group-text flex-grow-1"
                      type="file"
                      id="pdf"
                      onChange={(e) => {
                        handlePdfChange(e, "paN_EMP_NAME_FILE");
                      }}
                      accept="application/pdf"
                    />
                  </div>
                </div>
                {formik.touched.paN_EMP_NAME && formik.errors.paN_EMP_NAME && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.paN_EMP_NAME}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="electioN_CARD_NO"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    VOTER CARD NO.:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="number"
                      id="electioN_CARD_NO"
                      name="electioN_CARD_NO"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            kycDetailsTx?.fieldNames?.ELECTION_CARD_NO
                              ?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.electioN_CARD_NO &&
                            formik.errors.electioN_CARD_NO,
                        },
                        {
                          "is-valid":
                            formik.touched.electioN_CARD_NO &&
                            !formik.errors.electioN_CARD_NO,
                        }
                      )}
                      {...(kycDetailsTx?.fieldNames?.ELECTION_CARD_NO && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          kycDetailsTx?.fieldNames?.ELECTION_CARD_NO
                            ?.olD_VALUE === null
                            ? "-"
                            : kycDetailsTx?.fieldNames?.ELECTION_CARD_NO
                                ?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter Voter Card Number"
                      value={formik.values.electioN_CARD_NO}
                      onChange={(e) =>
                        formik.setFieldValue("electioN_CARD_NO", e.target.value)
                      }
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                {formik.touched.electioN_CARD_NO &&
                  formik.errors.electioN_CARD_NO && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.electioN_CARD_NO}</span>
                    </div>
                  )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="driV_LIC_NO"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    DRIVING LICENCE NO.:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="number"
                      id="driV_LIC_NO"
                      name="driV_LIC_NO"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            kycDetailsTx?.fieldNames?.DRIV_LIC_NO?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.driV_LIC_NO &&
                            formik.errors.driV_LIC_NO,
                        },
                        {
                          "is-valid":
                            formik.touched.driV_LIC_NO &&
                            !formik.errors.driV_LIC_NO,
                        }
                      )}
                      {...(kycDetailsTx?.fieldNames?.DRIV_LIC_NO && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          kycDetailsTx?.fieldNames?.DRIV_LIC_NO?.olD_VALUE ===
                          null
                            ? "-"
                            : kycDetailsTx?.fieldNames?.DRIV_LIC_NO?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter Driving Licence Number"
                      value={formik.values.driV_LIC_NO}
                      onChange={(e) =>
                        formik.setFieldValue("driV_LIC_NO", e.target.value)
                      }
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                {formik.touched.driV_LIC_NO && formik.errors.driV_LIC_NO && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.driV_LIC_NO}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="paN_NO"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PASSPORT NO.:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="number"
                      id="passporT_NO"
                      name="passporT_NO"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            kycDetailsTx?.fieldNames?.PASSPORT_NO?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.passporT_NO &&
                            formik.errors.passporT_NO,
                        },
                        {
                          "is-valid":
                            formik.touched.passporT_NO &&
                            !formik.errors.passporT_NO,
                        }
                      )}
                      {...(kycDetailsTx?.fieldNames?.PASSPORT_NO && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          kycDetailsTx?.fieldNames?.PASSPORT_NO?.olD_VALUE ===
                          null
                            ? "-"
                            : kycDetailsTx?.fieldNames?.PASSPORT_NO?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter Passport Number"
                      value={formik.values.passporT_NO}
                      onChange={(e) =>
                        formik.setFieldValue("passporT_NO", e.target.value)
                      }
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                {formik.touched.passporT_NO && formik.errors.passporT_NO && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.passporT_NO}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="passporT_ISS_PLACE"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PASSPORT ISSUE PLACE:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="text"
                      id="passporT_ISS_PLACE"
                      name="passporT_ISS_PLACE"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            kycDetailsTx?.fieldNames?.PASSPORT_ISS_PLACE
                              ?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.passporT_ISS_PLACE &&
                            formik.errors.passporT_ISS_PLACE,
                        },
                        {
                          "is-valid":
                            formik.touched.passporT_ISS_PLACE &&
                            !formik.errors.passporT_ISS_PLACE,
                        }
                      )}
                      {...(kycDetailsTx?.fieldNames?.PASSPORT_ISS_PLACE && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          kycDetailsTx?.fieldNames?.PASSPORT_ISS_PLACE
                            ?.olD_VALUE === null
                            ? "-"
                            : kycDetailsTx?.fieldNames?.PASSPORT_ISS_PLACE
                                ?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter Passport issue Place"
                      value={formik.values.passporT_ISS_PLACE.toUpperCase()}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                {formik.touched.passporT_ISS_PLACE &&
                  formik.errors.passporT_ISS_PLACE && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">
                        {formik.errors.passporT_ISS_PLACE}
                      </span>
                    </div>
                  )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="passporT_ISS_DATE"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PASSPORT ISSUE DATE:
                  </label>
                  <input
                    type="date"
                    id="passporT_ISS_DATE"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          kycDetailsTx?.fieldNames?.PASSPORT_ISS_DATE
                            ?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.passporT_ISS_DATE &&
                          formik.errors.passporT_ISS_DATE,
                      },
                      {
                        "is-valid":
                          formik.touched.passporT_ISS_DATE &&
                          !formik.errors.passporT_ISS_DATE,
                      }
                    )}
                    {...(kycDetailsTx?.fieldNames?.PASSPORT_ISS_DATE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        kycDetailsTx?.fieldNames?.PASSPORT_ISS_DATE
                          ?.olD_VALUE === null
                          ? "-"
                          : kycDetailsTx?.fieldNames?.PASSPORT_ISS_DATE
                              ?.olD_VALUE
                      }`,
                    })}
                    name="passporT_ISS_DATE"
                    placeholder="Enter Gratuity Date"
                    // value={formik.values.passporT_ISS_DATE?.toUpperCase()}
                    value={
                      formik.values?.passporT_ISS_DATE
                        ? new Date(formik.values?.passporT_ISS_DATE)
                            .toISOString()
                            .split("T")[0]
                        : ""
                    }
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.passporT_ISS_DATE &&
                  formik.errors.passporT_ISS_DATE && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">
                        {formik.errors.passporT_ISS_DATE}
                      </span>
                    </div>
                  )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="passporT_EXP_DATE"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PASSPORT EXPIRATION DATE:
                  </label>
                  <input
                    type="date"
                    id="passporT_EXP_DATE"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          kycDetailsTx?.fieldNames?.PASSPORT_EXP_DATE
                            ?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.passporT_EXP_DATE &&
                          formik.errors.passporT_EXP_DATE,
                      },
                      {
                        "is-valid":
                          formik.touched.passporT_EXP_DATE &&
                          !formik.errors.passporT_EXP_DATE,
                      }
                    )}
                    {...(kycDetailsTx?.fieldNames?.PASSPORT_EXP_DATE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        kycDetailsTx?.fieldNames?.PASSPORT_EXP_DATE
                          ?.olD_VALUE === null
                          ? "-"
                          : kycDetailsTx?.fieldNames?.PASSPORT_EXP_DATE
                              ?.olD_VALUE
                      }`,
                    })}
                    name="passporT_EXP_DATE"
                    placeholder="Enter Gratuity Date"
                    // value={formik.values.passporT_EXP_DATE?.toUpperCase()}
                    value={
                      formik.values?.passporT_EXP_DATE
                        ? new Date(formik.values?.passporT_EXP_DATE)
                            .toISOString()
                            .split("T")[0]
                        : ""
                    }
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.passporT_EXP_DATE &&
                  formik.errors.passporT_EXP_DATE && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">
                        {formik.errors.passporT_EXP_DATE}
                      </span>
                    </div>
                  )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="chG_REASON"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CHANGE REASON:
                  </label>
                  <input
                    type="text"
                    id="chG_REASON"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "is-invalid":
                          formik.touched.chG_REASON && formik.errors.chG_REASON,
                      },
                      {
                        "is-valid":
                          formik.touched.chG_REASON &&
                          !formik.errors.chG_REASON,
                      }
                    )}
                    name="chG_REASON"
                    placeholder="Enter Change Reason"
                    value={formik.values.chG_REASON?.toUpperCase()}
                    //
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.chG_REASON && formik.errors.chG_REASON && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.chG_REASON}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}
            </div>
            {/* end: final outer Edit Row */}

            {/* Logic for Submit is HERE : start */}
            <div className="card-footer d-flex justify-content-center py-6 px-9">
              <button
                type="button"
                className="btn btn-light me-2"
                onClick={handleClose}
              >
                <span className="indicator-label">Discard</span>
              </button>

              <button
                type="submit"
                className="btn btn-primary"
                disabled={formik.isSubmitting}
              >
                <span className="indicator-label">Submit</span>
              </button>
            </div>
            {/* Logic for Submit is HERE : end */}
          </div>
        </form>
      </div>
    </div>
  );
};

export default KYCDetailsEdit;
