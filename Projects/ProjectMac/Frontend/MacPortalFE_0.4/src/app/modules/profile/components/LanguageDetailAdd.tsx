// import { useState, FC } from "react";
// import { useFormik } from "formik";
// import * as Yup from "yup";
// import axios from "axios";
// import { useNavigate } from "react-router-dom";
// import { toast } from "react-toastify";

// // Define the form validation schema
// const profileDetailsSchema = Yup.object().shape({
//   languageName: Yup.string().required("Language Name is required"),
//   // You can add more validation rules for other form fields here
// });

// const LanguageDetailsAdd: FC = () => {
//   const [selectedLanguages, setSelectedLanguages] = useState<string[]>([]);
//   const [loading, setLoading] = useState(false);
//   const [proficiency, setProficiency] = useState(0);
//   const navigate = useNavigate();

//   const handleClose = () => {
//     // navigate(-1);
//     navigate("/crafted/pages/profile/overview");
//   };

//   const progressBarTextStyles = {
//     marginTop: "0.5rem",
//     fontSize: "1rem",
//     fontWeight: "bold",
//   };

//   const handleSubmit = async (values: { languageName: string }) => {
//     setLoading(true);

//     try {
//       const dataToSend = {
//         languageName: values.languageName,
//         selectedLanguages: selectedLanguages,
//         proficiency: proficiency,
//       };

//       // Make a POST request to your backend API endpoint using Axios
//       await axios.post("https://dev-jp95n6u72z4gzu9.api.raw-labs.com/your/endpoint/5", dataToSend);

//       setLoading(false);
//       toast.success("Succesfully submitted add language form")
//       navigate("/success"); // Redirect to success page upon successful submission

//     } catch (error) {
//       console.error("Error submitting add language details form:", error);
//       // toast.error("Error while submitting add language details form")
//       toast.error(<div >
//         <h6 className="alert-heading">{error}</h6>
//         <p className="alert alert-danger" role="alert"><h6>Error Message</h6></p>
//       </div>);
      
//       setLoading(false);
//     }
//     formik.resetForm();
//     navigate(-1); 
//   };

//   const formik = useFormik({
//     initialValues: {
//       languageName: "",
//     },
//     validationSchema: profileDetailsSchema,
//     onSubmit: handleSubmit,
//   });

//   const handleLanguageToggle = (language: string) => {
//     setSelectedLanguages((prevSelected) =>
//       prevSelected.includes(language)
//         ? prevSelected.filter((lang) => lang !== language)
//         : [...prevSelected, language]
//     );
//   };

//   return (
//     <div className="card mb-1 mb-xl-2">
//       <div className="card-header border-0 d-flex justify-content-between align-items-center cursor-pointer" role="button" onClick={handleClose}>
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Add Language Details</h3>
//         </div>
//         <button type="button" className="btn-close" aria-label="Close" onClick={handleClose} style={{ marginRight: "10px", cursor: "pointer" }}></button>
//       </div>

//       <div className="collapse show" id="kt_account_profile_details">
//         <form onSubmit={formik.handleSubmit} noValidate className="form">
//           <div className="card-body border-top p-9">
//             {/* Language Name input */}
//             <div className="row mb-1">
//               <div>
//                 <label className="col-form-label required fw-bold fs-6">Language Name:</label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   placeholder="Language Name"
//                   {...formik.getFieldProps("languageName")}
//                 />
//                 {formik.touched.languageName && formik.errors.languageName && (
//                   <div className="fv-plugins-message-container">
//                     <div className="fv-help-block">{formik.errors.languageName}</div>
//                   </div>
//                 )}
//               </div>
//             </div>

//             {/* Language Skills checkboxes */}
//             <div className="row mb-1 mt-4">
//               <label className="col-form-label required fw-bold fs-6">Language Skills</label>
//               <div className="language-checkboxes d-flex align-items-center mt-3">
//                 {["Read", "Write", "Speak"].map((skill) => (
//                   <div
//                     key={skill}
//                     className="form-check form-check-custom form-check-solid me-5"
//                     style={{
//                       borderColor: selectedLanguages.includes(skill) ? "green" : "initial",
//                     }}
//                   >
//                     <input
//                       className="form-check-input"
//                       type="checkbox"
//                       id={`${skill}Checkbox`}
//                       checked={selectedLanguages.includes(skill)}
//                       onChange={() => handleLanguageToggle(skill)}
//                       style={{ width: "16px", height: "16px" }}
//                     />
//                     <label className="form-check-label fw-bold" htmlFor={`${skill}Checkbox`}>
//                       {skill}
//                     </label>
//                   </div>
//                 ))}
//               </div>
//             </div>

//             {/* Language Proficiency range input */}
//             <div className="mt-9">
//               <label className="col-form-label required fw-bold fs-6">Language Proficiency:</label>
//               <input
//                 type="range"
//                 min="0"
//                 max="100"
//                 step="1"
//                 value={proficiency}
//                 onChange={(e) => setProficiency(parseInt(e.target.value))}
//                 style={{ width: "100%" }}
//               />
//               <div style={{ ...progressBarTextStyles, marginTop: "8px" }}>Proficiency: {proficiency}%</div>
//             </div>
//           </div>

//           {/* Form submission button */}
//           <div className="card-footer d-flex justify-content-center py-6 px-9">
//             <button type="submit" className="btn btn-primary" disabled={loading}>
//               {!loading ? (
//                 "Save Changes"
//               ) : (
//                 <span className="indicator-progress" style={{ display: "block" }}>
//                   Please wait... <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
//                 </span>
//               )}
//             </button>
//           </div>
//         </form>
//       </div>
//     </div>
//   );
// };

// export { LanguageDetailsAdd };
import {FC} from 'react'

const LanguageDetailsAdd: FC = () => {
  return (<div >
          <h1>This Page is under Development...</h1>
          <h3>please contact to admin</h3>
          </div>)
}

export {LanguageDetailsAdd}