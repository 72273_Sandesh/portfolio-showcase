// import { useEffect, useState } from "react";
// import { Link } from "react-router-dom";
// import axios from "axios";


// export function PayrollDetails() {
//   const [basicDetails, setBasicDetails] = useState([]);
//   const [isCollapsed, setIsCollapsed] = useState(true);

//   useEffect(() => {
//     // Fetch Payroll details from the backend API
//     axios
//       .get("https://localhost:44360/api/Employee/H003333/EMPPER")
//       .then((response) => {
//         setBasicDetails(response.data[0]);
//       })
//       .catch((error) => {
//         console.error("Error fetching Payroll details:", error);
//       });
//   }, []);

//   const toggleCollapse = () => {
//     setIsCollapsed(!isCollapsed);
//   };

//   return (
//     <div className={`card`} id="Payroll Details">
//       <div
//         className="card-header cursor-pointer"
//         role="button"
//         onClick={toggleCollapse}
//         aria-expanded={!isCollapsed}
//         aria-controls="kt_account_profile_details"
//       >
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Payroll Details</h3>
//         </div>
//         <Link
//           to="/crafted/pages/profile/EditBasicDetails"
//           className="btn btn-sm btn-primary me-3 align-self-center"
//         >
//           Edit 
//         </Link>
//       </div>

//       <div className={`collapse ${isCollapsed ? '' : 'show'} card-body p-9`}>
//         {/* Begin:first Row */}
//         <div className="row">
//           {/* Begin:first Row     */}
//           <div className="row">
//             <div className="col-lg-6 mb-6">
//               <label className="col-lg-4 fw-bold text-muted">
//                 BASIC PER MONTH :{" "}
//               </label>
//               <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//             </div>
//             <div className="col-lg-6 mb-6">
//               <label className="col-lg-4 fw-bold text-muted">
//                 CTC PER MONTH :{" "}
//               </label>
//               <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//             </div>
//           </div>
//           {/* End:first Row     */}

//           {/* Begin:first Row     */}
//           <div className="row">
//             <div className="col-lg-6 mb-6">
//               <label className="col-lg-4 fw-bold text-muted">
//                 PAY MODE  :{" "}
//               </label>
//               <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.paymenT_MODE}
//                 </span>
//             </div>
//             <div className="col-lg-6 mb-6">
//               <label className="col-lg-4 fw-bold text-muted">PAY TYPE : </label>
//               <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//             </div>
//           </div>
//           {/* End:first Row     */}

//           {/* Begin:first Row     */}
//           <div className="row">
//             <div className="col-lg-6 mb-6">
//               <label className="col-lg-4 fw-bold text-muted">
//                 {" "}
//                 BANK CODE  :{" "}
//               </label>
//               <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.banK_CODE}
//                 </span>
//             </div>
//             <div className="col-lg-6 mb-6">
//               <label className="col-lg-4 fw-bold text-muted">BANK IFSC CODE  : </label>
//               <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.banK_IFSC}
//                 </span>
//             </div>
//           </div>
//           {/* End:first Row     */}
//           {/* Begin:first Row     */}
//           <div className="row">
//             <div className="col-lg-6 mb-6">
//               <label className="col-lg-4 fw-bold text-muted">
//                 BANK A/C NAME :{" "}
//               </label>
//               <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.banK_NAME}
//                 </span>
//             </div>
//             <div className="col-lg-6 mb-6">
//               <label className="col-lg-4 fw-bold text-muted">
//                 BANK A/C NO. :{" "}
//               </label>
//               <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.banK_ACCT}
//                 </span>
//             </div>
//           </div>
//           {/* End:first Row     */}

//           {/* Begin:first Row     */}
//           <div className="row">
//             <div className="col-lg-6 mb-6">
//               <label className="col-lg-4 fw-bold text-muted">DD PAY BANK : </label>
//               <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//             </div>
//             <div className="col-lg-6 mb-6">
//               <label className="col-lg-4 fw-bold text-muted">
//                 OLD PAYROLL ID :{" "}
//               </label>
//               <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//             </div>
//           </div>
//           {/* End:first Row     */}

//           {/* Begin:first Row     */}
//           <div className="row">
//             <div className="col-lg-6 mb-6">
//               <label className="col-lg-4 fw-bold text-muted">
//                 SALARY WITHHELD :{" "}
//               </label>
//               <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//             </div>
//             <div className="col-lg-6 mb-6">
//               <label className="col-lg-4 fw-bold text-muted">
//                 STATION CODE  :{" "}
//               </label>
//               <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//             </div>
//           </div>
//           {/* End:first Row     */}
//         </div>
//         {/* End:first Row */}
//       </div>
//     </div>
//   );
// }

import {FC} from 'react'

const PayrollDetails: FC = () => {
  return (<div >
          <h1>This Page is under Development...</h1>
          <h3>please contact to admin</h3>
          </div>)
}

export {PayrollDetails}
