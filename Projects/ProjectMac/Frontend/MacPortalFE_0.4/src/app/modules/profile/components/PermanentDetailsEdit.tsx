// import React, { useState, useEffect } from "react";
// import axios from "axios";
// import { useNavigate } from "react-router-dom";
// import { useFormik } from "formik";
// import * as Yup from "yup";
// import { employeeIdForView } from "../../GlobalQuery";
// import { toast } from "react-toastify";
// import { useAuth } from "../../auth";
// const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// type states = {
//   statE_CODE: string;
//   descr: string;
// };

// const PermanentDetailsEdit: React.FC = () => {
//   const [employee, setEmployee] = useState<any>({});
//   const [states, setStates] = useState<states[]>([]);
//   const [pdf, setPdf] = useState<File | null>(null);
//   const { currentUser } = useAuth();

//   const navigate = useNavigate();

//   useEffect(() => {
//     axios
//       .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
//       .then((response) => {
//         setEmployee(response.data[0]);
//         // Update formik's initial values after fetching employee data
//         formik.setValues({
//           peR_ADD1: response.data[0].peR_ADD1 || "",
//           peR_ADD2: response.data[0].peR_ADD2 || "",
//           peR_ADD3: response.data[0].peR_ADD3 || "",

//           peR_TEL: response.data[0].peR_TEL || "",

//           peR_CITY: response.data[0].peR_CITY || "",
//           peR_STATE: response.data[0].peR_STATE || "",
//           peR_STATE_DESCR: response.data[0].peR_STATE_DESCR || "",
//           peR_PIN: response.data[0].peR_PIN || "",
//           pdf:  response.data[0].pdf || pdf,
//         });
//       })
//       .catch((error) => {
//         console.error("Error fetching basic details:", error);
//       });

//     axios
//       .get(`${EMPLOYEE_URL}/ddl_GetStates`)
//       .then((response) => {
//         setStates(response.data.states);
//       })
//       .catch((error) => {
//         console.error("Error fetching basic details:", error);
//       });
//   }, []);

//   const validationSchema = Yup.object().shape({
//     peR_ADD1: Yup.string().required("Line 1 is required"),
//     peR_ADD2: Yup.string().required("Line 2 is required"),
//     peR_ADD3: Yup.string().required("Line 3 is required"),

//     peR_TEL: Yup.string().required("Telephone is required"),

//     peR_CITY: Yup.string().required("Permanent City is required"),
//     peR_STATE: Yup.string().required("Permanent State is required"),
//     peR_PIN: Yup.string().required("Zip code is required"),
//   });

//   const formik = useFormik({
//     initialValues: {
//       peR_ADD1: "",
//       peR_ADD2: "",
//       peR_ADD3: "",

//       peR_TEL: "",

//       peR_CITY: "",
//       peR_STATE: "",
//       peR_STATE_DESCR: "",
//       peR_PIN: "",
//       pdf:null
//     },
//     validationSchema: validationSchema,
//     onSubmit: (values, formik) => {
//       console.log("Submitting form data:", values);
//       // const formData = new FormData();

//       // Object.keys(values).forEach((key) => {
//       //   if (key === "pdf" && pdf) {
//       //     formData.append(key, pdf);
//       //   } else {
//       //     formData.append(key, values[key as keyof typeof values] as string);
//       //   }
//       // });

//       axios
//         .put(
//           `${EMPLOYEE_URL}/UpdateEmployeePermanentAddressDetails/${employeeIdForView}`,
//           {
//             ...values,
//             chG_DATE: new Date(),
//             chG_USER: currentUser?.id, //const { currentUser } = useAuth();
//             chG_TERM: "TERM",
//             // pdf: pdf,
//           }
//         )
//         .then((response) => {
//           console.log("Basic Details Posted successfully!", response.data);
//           const { responseStatus, message } = response.data.data;
//           if (responseStatus === "E") {
//             formik.setSubmitting(false);
//             toast.error(
//               <div>
//                 <h4 className="alert-heading">Failed to submit Form...</h4>
//                 <p className="alert alert-danger" role="alert">
//                   <h6>
//                     {Array.isArray(message) ? message.join("\n") : message}
//                   </h6>
//                 </p>
//               </div>
//             );
//           } else {
//             toast.success(
//               <div>
//                 <h4 className="alert-heading">
//                   Successfully Submitted Form...
//                 </h4>
//                 <p className="alert alert-success" role="alert">
//                   <h6>
//                     {Array.isArray(message) ? message.join("\n") : message}
//                   </h6>
//                 </p>
//               </div>
//             );

//             formik.resetForm();
//             navigate(-1);
//           }
//         })
//         .catch((error) => {
//           console.error("Error posting data:", error);
//           toast.error(
//             <div>
//               <h6 className="alert-heading">Error Posting Data</h6>
//               <p className="alert alert-danger" role="alert">
//                 <h6>{error.message}</h6>
//               </p>
//             </div>
//           );

//           formik.setSubmitting(false);
//         });
//     },
//   });
//   const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
//     const file = e.target.files?.[0];
//     if (file) {
//       setPdf(file);
//     }
//   };
//   const handleClose = () => {
//     navigate(-1);
//   };

//   return (
//     <div className="card mb-1 mb-xl-2">
//       <div
//         className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
//         role="button"
//         data-bs-target="#kt_account_profile_details"
//         aria-expanded="true"
//         aria-controls="kt_account_profile_details"
//       >
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Edit Permanent Details</h3>
//         </div>
//         <button
//           type="button"
//           className="btn-close"
//           aria-label="Close"
//           onClick={handleClose}
//           style={{ marginRight: "10px", cursor: "pointer" }}
//         ></button>
//       </div>

//       <div id="kt_account_profile_details" className="collapse show">
//         <form onSubmit={formik.handleSubmit} className="form">
//           <div className="card-body border-top p-9">
//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="peR_ADD1"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   PERMANENT ADDRESS :
//                 </label>
//                 <input
//                   type="text"
//                   id="peR_ADD1"
//                   className="form-control form-control-lg form-control-solid"
//                   name="peR_ADD1"
//                   placeholder="Line 1"
//                   value={formik.values.peR_ADD1}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="peR_TEL"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   TELEPHONE :
//                 </label>
//                 <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   name="peR_TEL"
//                   placeholder="Telephone 1"
//                   value={formik.values.peR_TEL}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.peR_ADD1 && formik.errors.peR_ADD1 ? (
//                   <div className="text-danger">{formik.errors.peR_ADD1}</div>
//                 ) : null}
//               </div>

//               <div className="col-lg-6">
//                 {formik.touched.peR_TEL && formik.errors.peR_TEL ? (
//                   <div className="text-danger">{formik.errors.peR_TEL}</div>
//                 ) : null}
//               </div>
//             </div>

//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="cuR_ADD2"
//                   className="col-form-label fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   &nbsp;
//                 </label>
//                 <input
//                   type="text"
//                   id="peR_ADD2"
//                   className="form-control form-control-lg form-control-solid"
//                   name="peR_ADD2"
//                   placeholder="Line 2"
//                   value={formik.values.peR_ADD2}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.peR_ADD2 && formik.errors.peR_ADD2 ? (
//                   <div className="text-danger">{formik.errors.peR_ADD2}</div>
//                 ) : null}{" "}
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="peR_ADD3"
//                   className="col-form-label fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   &nbsp;
//                 </label>
//                 <input
//                   type="text"
//                   id="peR_ADD3"
//                   className="form-control form-control-lg form-control-solid"
//                   name="peR_ADD3"
//                   placeholder="Line 3"
//                   value={formik.values.peR_ADD3}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.peR_ADD3 && formik.errors.peR_ADD3 ? (
//                   <div className="text-danger">{formik.errors.peR_ADD3}</div>
//                 ) : null}
//               </div>
//             </div>
//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="peR_CITY"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   PERMANENT CITY :
//                 </label>
//                 <input
//                   type="text"
//                   id="peR_CITY"
//                   className="form-control form-control-lg form-control-solid"
//                   name="peR_CITY"
//                   placeholder="Enter permanent city"
//                   value={formik.values?.peR_CITY}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   {" "}
//                   STATE :
//                 </label>
//                 {/* <input
//                   type="text"
//                   className="form-control form-control-lg form-control-solid"
//                   name="peR_STATE"
//                   placeholder="Enter permanent state"
//                   value={formik.values.peR_STATE?.toUpperCase()}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 /> */}
//                 <select
//                   id="peR_STATE_DESCR"
//                   className="form-select form-select-lg form-select-solid"
//                   name="peR_STATE_DESCR"
//                   onChange={formik.handleChange}
//                 >
//                   <option value={formik.values?.peR_STATE}>
//                     {formik.values?.peR_STATE_DESCR}
//                   </option>

//                   {states.map((state) => (
//                     <option key={state.statE_CODE} value={state.statE_CODE}>
//                       {state.descr}
//                     </option>
//                   ))}
//                 </select>
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.peR_CITY && formik.errors.peR_CITY ? (
//                   <div className="text-danger">{formik.errors.peR_CITY}</div>
//                 ) : null}
//               </div>

//               <div className="col-lg-6">
//                 {formik.touched.peR_STATE && formik.errors.peR_STATE ? (
//                   <div className="text-danger">{formik.errors.peR_STATE}</div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="peR_PIN"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   ZIP CODE :
//                 </label>
//                 <input
//                   type="text"
//                   id="peR_PIN"
//                   className="form-control form-control-lg form-control-solid"
//                   name="peR_PIN"
//                   placeholder="Enter zip code"
//                   value={formik.values?.peR_PIN}
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="cuR_PIN"
//                   className="col-form-label required fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   ATTACH DOCUMENT :
//                 </label>
//                 <input
//                   name="birtH_DATE"
//                   className="form-control form-control-lg flex-grow-1"
//                   type="file"
//                   id="pdf"
//                   onChange={handlePdfChange}
//                   accept="application/pdf"
//                   required
//                 />
//                 {formik.touched.pdf && formik.errors.pdf ? (
//                   <div className="text-danger">{formik.errors.pdf}</div>
//                 ) : null}
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.peR_PIN && formik.errors.peR_PIN ? (
//                   <div className="text-danger">{formik.errors.peR_PIN}</div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}
//           </div>

//           {/* Logic for Submit is HERE */}
//           <div className="card-footer d-flex justify-content-center py-6 px-9">
//             <button
//               type="button"
//               className="btn btn-light me-2"
//               onClick={handleClose}
//             >
//               <span className="indicator-label">Discard</span>
//             </button>
//             <button
//               type="submit"
//               className="btn btn-primary"
//               disabled={formik.isSubmitting || !formik.isValid}
//             >
//               <span className="indicator-label">Submit</span>
//             </button>
//           </div>
//         </form>
//       </div>
//     </div>
//   );
// };

// export default PermanentDetailsEdit;




import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { Mode, QUERIES } from "../../../../_metronic/helpers";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
import { useListView } from "../../apps/transaction-master/master-list/core/ListViewProvider";
import clsx from "clsx";
import { useMutation, useQueryClient } from "react-query";
import { insertTransactionV2 } from "../../apps/transaction-master/master-list/core/_requests";
import { useQueryResponse } from "../../apps/employee/employee-list/core/QueryResponseProvider";

type FieldDetail = {
  olD_KEY: string;
  olD_VALUE: string;
  neW_KEY: string;
  neW_VALUE: string;
  FILE_PATH: string;
};

type PermenentDetails = {
  peR_ADD1: string;
  peR_ADD2: string;
  peR_ADD3: string;
  peR_TEL: string;
  peR_CITY: string;
  peR_STATE: string;
  peR_STATE_val: string;
  peR_PIN: string;
  peR_ADD1_FILE: string;
  peR_STATE_DESCR:string;
  CHG_REASON: string;

  fieldNames?: {
    EMP_CODE: FieldDetail;
    PER_ADD1: FieldDetail;
    PER_ADD2: FieldDetail;
    PER_ADD3: FieldDetail;
    PER_TEL: FieldDetail;
    PER_CITY: FieldDetail;
    PER_STATE: FieldDetail;
    PER_STATE_val: FieldDetail;
    PER_PIN: FieldDetail;
    PER_ADD1_FILE: FieldDetail;
    CHG_REASON: FieldDetail;
  };
};

interface States {
  statE_CODE: string;
  descr: string;
  counT_CODE: string;
}

const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const PermanentDetailsEdit: React.FC = () => {
  const { currentUser } = useAuth();
  const [privateIP, setPrivateIP] = useState();

  const { itemIdForUpdate, setItemIdForUpdate, formMode } = useListView();
  const [empCodeTxForView, setEmpCodeTxForView] = useState();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  const [states, setStates] = useState<States[]>([]);
  
  // const [pdf, setPdf] = useState<string>("");

  const [permenentDetails, setPermenentDetails] = useState<PermenentDetails>();
  const [permenentDetailsTx, setPermenentDetailsTx] = useState<PermenentDetails>();

  // const [genders] = useState([
  //   { code: "F", descr: "FEMALE" },
  //   { code: "M", descr: "MALE" },
  // ]);

  // const [religions] = useState([
  //   { code: "HINDU", descr: "HINDU" },
  //   { code: "MUSLIM", descr: "MUSLIM" },
  //   { code: "SIKH", descr: "SIKH" },
  //   { code: "CHRISTIAN", descr: "CHRISTIAN" },
  //   { code: "BAUDDHA", descr: "BAUDDHA" },
  //   { code: "JAIN", descr: "JAIN" },
  //   { code: "PARASI", descr: "PARASI" },
  // ]);

  // const [bloodGrps] = useState([
  //   { code: "A+", descr: "A+" },
  //   { code: "A-", descr: "A-" },
  //   { code: "B+", descr: "B+" },
  //   { code: "B-", descr: "B-" },
  //   { code: "AB+", descr: "AB+" },
  //   { code: "AB-", descr: "AB-" },
  //   { code: "O+", descr: "O+" },
  //   { code: "O-", descr: "O-" },
  // ]);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching Ip Address:", error));

    itemIdForUpdate && fetchPermenentDetailsTx();
    fetchPermenentDetails();
  }, [empCodeTxForView]);

  useEffect(() => {
    axios
      .get(`${EMPLOYEE_URL}/ddl_GetStates`)
      .then((response) => {
        setStates(response.data.states);
      })
      .catch((error) => {
        console.error("Error fetching state dropdown:", error);
      });
  }, []);

  const fetchPermenentDetails = () => {
    axios
      .get(
        `${EMPLOYEE_URL}/${
          employeeIdForView ? employeeIdForView : empCodeTxForView
        }/EMPPER`
      )
      .then((response) => {
        setPermenentDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching Present contact details:", error);
      });
  };

  const fetchPermenentDetailsTx = () => {
    axios
      .get(`${MASTERTX_URL}/GetTransactionByID?tranId=${itemIdForUpdate}`)
      .then((response) => {
        setEmpCodeTxForView(
          response.data.transactions[0].fieldNames?.EMP_CODE?.neW_VALUE
        );

        setPermenentDetailsTx(response.data.transactions[0]);
      });
  };

  useEffect(() => {
    formik.setValues({
      peR_ADD1:
        permenentDetailsTx?.fieldNames?.PER_ADD1?.neW_KEY ||
        permenentDetails?.peR_ADD1 ||
        "",
      peR_ADD1_O:
        permenentDetailsTx?.fieldNames?.PER_ADD1?.neW_KEY ||
        permenentDetails?.peR_ADD1 ||
        "",

      peR_ADD2:
        permenentDetailsTx?.fieldNames?.PER_ADD2?.neW_KEY ||
        permenentDetails?.peR_ADD2 ||
        "",
      peR_ADD2_O:
        permenentDetailsTx?.fieldNames?.PER_ADD2?.neW_KEY ||
        permenentDetails?.peR_ADD2 ||
        "",

      peR_ADD3:
        permenentDetailsTx?.fieldNames?.PER_ADD3?.neW_KEY ||
        permenentDetails?.peR_ADD3 ||
        "",
      peR_ADD3_O:
        permenentDetailsTx?.fieldNames?.PER_ADD3?.neW_KEY ||
        permenentDetails?.peR_ADD3 ||
        "",

      peR_TEL:
        permenentDetailsTx?.fieldNames?.PER_TEL?.neW_KEY ||
        permenentDetails?.peR_TEL ||
        "",
      peR_TEL_O:
        permenentDetailsTx?.fieldNames?.PER_TEL?.neW_KEY ||
        permenentDetails?.peR_TEL ||
        "",

      peR_CITY:
        permenentDetailsTx?.fieldNames?.PER_CITY?.neW_KEY ||
        permenentDetails?.peR_CITY ||
        "",
      peR_CITY_O:
        permenentDetailsTx?.fieldNames?.PER_CITY?.neW_KEY ||
        permenentDetails?.peR_CITY ||
        "",

      peR_STATE:
        permenentDetailsTx?.fieldNames?.PER_STATE?.neW_KEY ||
        permenentDetails?.peR_STATE ||
        "",

      peR_STATE_val:
        permenentDetailsTx?.fieldNames?.PER_STATE?.neW_VALUE ||
        permenentDetails?.peR_STATE_DESCR ||
        "",

      peR_STATE_O:
        permenentDetailsTx?.fieldNames?.PER_STATE?.olD_KEY ||
        permenentDetails?.peR_STATE ||
        "",
      peR_STATE_val_O:
        permenentDetailsTx?.fieldNames?.PER_STATE?.olD_VALUE ||
        permenentDetails?.peR_STATE_DESCR ||
        "",

      peR_PIN:
        permenentDetailsTx?.fieldNames?.PER_PIN?.neW_KEY ||
        permenentDetails?.peR_PIN ||
        "",
      peR_PIN_O:
        permenentDetailsTx?.fieldNames?.PER_PIN?.neW_KEY ||
        permenentDetails?.peR_PIN ||
        "",

      peR_ADD1_FILE:  permenentDetailsTx?.fieldNames?.PER_ADD1?.FILE_PATH ||
      "",

      CHG_REASON: formMode === Mode.EDIT ? "" : permenentDetails?.CHG_REASON,

    });
  }, [permenentDetails]);

  // const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
  //   formik.setFieldValue("emP_IMAGE", e.target.files?.[0]);
  //   const file = e.target.files?.[0];
  //   if (file) {
  //     const reader = new FileReader();
  //     reader.onloadend = () => {
  //       const base64String = reader.result as string;
  //       formik.setFieldValue("emP_IMAGE", base64String);
  //     };
  //     reader.readAsDataURL(file);
  //   }
  // };

  const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (file) {
      const reader = new FileReader();

      reader.onloadend = () => {
        const base64String = reader.result;
        console.log(base64String);
        formik.setFieldValue("peR_ADD1_FILE", base64String);
      };
      reader.readAsDataURL(file);
    }
  };

  const validationSchema = Yup.object().shape({
    peR_ADD1: Yup.string().required("Line 1 is required"),
    peR_ADD2: Yup.string().required("Line 2 is required"),
    peR_ADD3: Yup.string().required("Line 3 is required"),
    peR_TEL: Yup.string().required("Telephone  is required"),
    peR_CITY: Yup.string().required("city is required"),
    peR_STATE: Yup.string().required("state is required"),
    peR_PIN: Yup.string().required("Pincode is required").test("len","Pincode must be exactly 6 numbers",(value) => value && value.length === 6
  ),
    CHG_REASON: Yup.string().required("Change Reason is required"),
    peR_ADD1_FILE: Yup.string().when([], {
      is: () =>
        permenentDetails?.peR_ADD1 !== formik.values.peR_ADD1 ||
        permenentDetails?.peR_ADD2 !== formik.values.peR_ADD2 ||
        permenentDetails?.peR_ADD3 !== formik.values.peR_ADD3 ||
        permenentDetails?.peR_STATE !== formik.values.peR_STATE ||
        permenentDetails?.peR_CITY !== formik.values.peR_CITY ||
        permenentDetails?.peR_PIN !== formik.values.peR_PIN ,
      then: (schema) => schema.required("Proof attachment is required"),
      otherwise: (schema) => schema.notRequired(),
    }),

  });

  const createTx = useMutation((payload: any) => insertTransactionV2(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        handleClose();
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formik = useFormik({
    initialValues: {
      peR_ADD1: "",
      peR_ADD1_O: "",

      peR_ADD2: "",
      peR_ADD2_O: "",

      peR_ADD3: "",
      peR_ADD3_O: "",

      peR_TEL: "",
      peR_TEL_O: "",

      peR_CITY: "",
      peR_CITY_O: "",
      
      peR_STATE: "",
      peR_STATE_O: "",

      peR_STATE_val: "",
      peR_STATE_val_O: "",
      
      peR_PIN: "",
      peR_PIN_O: "",
    
      peR_ADD1_FILE: "",

      CHG_REASON: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values, formik) => {
      const payload = {
        values,

        TABLENAME: "EMPLOYEE_PERMANENT",
        KEYVAR: "",
        WFLEVEL: 1,
        CONF: "N",
        SITECODE: "S0001",
        EMP: currentUser?.id?.toString() || "",
        FILEPATH: null,
        FILE: null,
        FOLDERPATH: null,
        FORMNAME: "Permanent Contact Details",
        tX_CHG_USER: currentUser?.id?.toString() || "",
        tX_CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        STRTRANID: itemIdForUpdate ? itemIdForUpdate : null,
      };

      createTx.mutate(payload);
      formik.setSubmitting(false);
    },
  });

  const handleClose = () => {
    itemIdForUpdate ? setItemIdForUpdate(undefined) : navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Permanent Contact</h3>
        </div>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={handleClose}
          style={{ marginRight: "10px", cursor: "pointer" }}
        ></button>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: final outer Edit Row */}
            <div className="row mb-1">
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="peR_ADD1"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PERMANENT ADDRESS:
                  </label>
                  <input
                    type="text"
                    id="peR_ADD1"
                    name="peR_ADD1"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          permenentDetailsTx?.fieldNames?.PER_ADD1?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.peR_ADD1 && formik.errors.peR_ADD1,
                      },
                      {
                        "is-valid":
                          formik.touched.peR_ADD1 && !formik.errors.peR_ADD1,
                      }
                    )}
                    {...(permenentDetailsTx?.fieldNames?.PER_ADD1 && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        permenentDetailsTx?.fieldNames?.PER_ADD1?.olD_VALUE ===
                        null
                          ? "-"
                          : permenentDetailsTx?.fieldNames?.PER_ADD1?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Line 1 address"
                    value={formik.values.peR_ADD1}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.peR_ADD1 && formik.errors.peR_ADD1 && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.peR_ADD1}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="peR_TEL"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PERMANENT CONTACT:
                  </label>
                  <input
                    type="text"
                    id="peR_TEL"
                    name="peR_TEL"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          permenentDetailsTx?.fieldNames?.PER_TEL?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.peR_TEL && formik.errors.peR_TEL,
                      },
                      {
                        "is-valid":
                          formik.touched.peR_TEL && !formik.errors.peR_TEL,
                      }
                    )}
                    {...(permenentDetailsTx?.fieldNames?.PER_TEL && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        permenentDetailsTx?.fieldNames?.PER_TEL?.olD_VALUE ===
                        null
                          ? "-"
                          : permenentDetailsTx?.fieldNames?.PER_TEL?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Telephone 1"
                    value={formik.values.peR_TEL}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.peR_TEL && formik.errors.peR_TEL && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.peR_TEL}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

               {/* Begin: Edit Row */}
               <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="peR_ADD2"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    &nbsp;
                  </label>
                  <input
                    type="text"
                    id="peR_ADD2"
                    name="peR_ADD2"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          permenentDetailsTx?.fieldNames?.PER_ADD2?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.peR_ADD2 && formik.errors.peR_ADD2,
                      },
                      {
                        "is-valid":
                          formik.touched.peR_ADD2 && !formik.errors.peR_ADD2,
                      }
                    )}
                    {...(permenentDetailsTx?.fieldNames?.PER_ADD2 && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        permenentDetailsTx?.fieldNames?.PER_ADD2?.olD_VALUE ===
                        null
                          ? "-"
                          : permenentDetailsTx?.fieldNames?.PER_ADD2?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Line 2 address"
                    value={formik.values.peR_ADD2}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.peR_ADD2 && formik.errors.peR_ADD2 && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.peR_ADD2}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="peR_ADD2"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    &nbsp;
                  </label>
                  {/* <input
                    type="text"
                    id="peR_ADD2"
                    name="peR_ADD2"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          permenentDetailsTx?.fieldNames?.PER_ADD2?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.peR_ADD2 && formik.errors.peR_ADD2,
                      },
                      {
                        "is-valid":
                          formik.touched.peR_ADD2 && !formik.errors.peR_ADD2,
                      }
                    )}
                    {...(permenentDetailsTx?.fieldNames?.PER_ADD2 && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        permenentDetailsTx?.fieldNames?.PER_ADD2?.olD_VALUE ===
                        null
                          ? "-"
                          : permenentDetailsTx?.fieldNames?.PER_ADD2?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Line 2 address"
                    value={formik.values.peR_ADD2}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  /> */}
                </div>
                {/* {formik.touched.peR_ADD2 && formik.errors.peR_ADD2 && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.peR_ADD2}</span>
                  </div>
                )} */}
              </div>
              {/* End: Edit Row */}

             

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="peR_ADD3"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    &nbsp;
                  </label>
                  <input
                    type="text"
                    id="peR_ADD3"
                    name="peR_ADD3"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          permenentDetailsTx?.fieldNames?.PER_ADD3?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.peR_ADD3 && formik.errors.peR_ADD3,
                      },
                      {
                        "is-valid":
                          formik.touched.peR_ADD3 && !formik.errors.peR_ADD3,
                      }
                    )}
                    {...(permenentDetailsTx?.fieldNames?.PER_ADD3 && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        permenentDetailsTx?.fieldNames?.PER_ADD3?.olD_VALUE ===
                        null
                          ? "-"
                          : permenentDetailsTx?.fieldNames?.PER_ADD3?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Line 3 address"
                    value={formik.values.peR_ADD3}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.peR_ADD3 && formik.errors.peR_ADD3 && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.peR_ADD3}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

             

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="peR_CITY"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PERMANENT CITY:
                  </label>
                  <input
                    type="text"
                    id="peR_CITY"
                    name="peR_CITY"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          permenentDetailsTx?.fieldNames?.PER_CITY?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.peR_CITY && formik.errors.peR_CITY,
                      },
                      {
                        "is-valid":
                          formik.touched.peR_CITY && !formik.errors.peR_CITY,
                      }
                    )}
                    {...(permenentDetailsTx?.fieldNames?.PER_CITY && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        permenentDetailsTx?.fieldNames?.PER_CITY?.olD_VALUE ===
                        null
                          ? "-"
                          : permenentDetailsTx?.fieldNames?.PER_CITY?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter city"
                    value={formik.values.peR_CITY}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.peR_CITY && formik.errors.peR_CITY && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.peR_CITY}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="peR_STATE"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PERMANENT STATE:
                  </label>
                  {/* <input
                    type="text"
                    id="cuR_STATE"
                    name="cuR_STATE"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          presentDetailsTx?.fieldNames?.CUR_STATE?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.cuR_STATE && formik.errors.cuR_STATE,
                      },
                      {
                        "is-valid":
                          formik.touched.cuR_STATE && !formik.errors.cuR_STATE,
                      }
                    )}
                    {...(presentDetailsTx?.fieldNames?.CUR_STATE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        presentDetailsTx?.fieldNames?.CUR_STATE?.olD_VALUE ===
                        null
                          ? "-"
                          : presentDetailsTx?.fieldNames?.CUR_STATE?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter state"
                    value={formik.values.cuR_STATE}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  /> */}
                  <select
                    id="peR_STATE"
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          permenentDetailsTx?.fieldNames?.PER_STATE?.neW_KEY,
                      },
                      {
                        "is-invalid":
                          formik.touched.peR_STATE && formik.errors.peR_STATE,
                      },
                      {
                        "is-valid":
                          formik.touched.peR_STATE && !formik.errors.peR_STATE,
                      }
                    )}
                    {...(permenentDetailsTx?.fieldNames?.PER_STATE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        permenentDetailsTx?.fieldNames?.PER_STATE?.olD_VALUE ===
                        null
                          ? "-"
                          : permenentDetailsTx?.fieldNames?.PER_STATE?.olD_VALUE
                      }`,
                    })}
                    name="peR_STATE"
                    aria-placeholder="Select State"
                    value={formik.values.peR_STATE}
                    // onChange={formik.handleChange}
                    onChange={(e) => {
                      const selectedValue = e.target.value;
                      const selectedOption = states.find(
                        (bankName) =>
                          bankName.statE_CODE === selectedValue
                      );

                      formik.setFieldValue("peR_STATE", selectedValue);
                      formik.setFieldValue(
                        "peR_STATE_val",
                        selectedOption?.descr.toUpperCase() || ""
                      );
                    }}
                  >
                    <option value=""></option>
                    {states.map((e) => (
                      <option
                        key={e.statE_CODE}
                        value={e.statE_CODE}
                      >
                        {e.descr.toUpperCase()}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.peR_STATE && formik.errors.peR_STATE && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.peR_STATE}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="peR_PIN"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PIN CODE:
                  </label>
                  <input
                    type="text"
                    id="peR_PIN"
                    name="peR_PIN"
                    // className="form-control"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          permenentDetailsTx?.fieldNames?.PER_PIN?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.peR_PIN && formik.errors.peR_PIN,
                      },
                      {
                        "is-valid":
                          formik.touched.peR_PIN && !formik.errors.peR_PIN,
                      }
                    )}
                    {...(permenentDetailsTx?.fieldNames?.PER_PIN && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        permenentDetailsTx?.fieldNames?.PER_PIN?.olD_VALUE ===
                        null
                          ? "-"
                          : permenentDetailsTx?.fieldNames?.PER_PIN?.olD_VALUE
                      }`,
                    })}
                    placeholder="Enter Pincode"
                    value={formik.values.peR_PIN}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.peR_PIN && formik.errors.peR_PIN && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.peR_PIN}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="peR_ADD1_FILE"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    ATTACH FILE:
                  </label>
                  <input
                    name="peR_ADD1_FILE"
                    // className="form-control form-control-lg flex-grow-1"
                    className={clsx(
                      "form-control form-control-solid flex-grow-1",
                      {
                        "bg-light-warning":
                          permenentDetailsTx?.fieldNames?.PER_ADD1?.FILE_PATH,
                      },
                      {
                        "is-invalid": formik.touched.peR_ADD1_FILE && formik.errors.peR_ADD1_FILE,
                      },
                      {
                        "is-valid": formik.touched.peR_ADD1_FILE && !formik.errors.peR_ADD1_FILE,
                      }
                    )}
                    {...(permenentDetailsTx?.fieldNames?.PER_ADD1.FILE_PATH && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        permenentDetailsTx?.fieldNames?.PER_ADD1.FILE_PATH === null
                          ? "-"
                          : permenentDetailsTx?.fieldNames?.PER_ADD1.FILE_PATH
                      }`,
                    })}
                    type="file"
                    id="peR_ADD1_FILE"
                    placeholder="Attach proof document"
                    onChange={handlePdfChange}
                    accept="application/pdf"
                    // required
                  />
                </div>
                {formik.touched.peR_ADD1_FILE && formik.errors.peR_ADD1_FILE && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.peR_ADD1_FILE}</span>
                  </div>
                )}
              </div>

              {/* Begin: Edit Row */}

              <div className="col-lg-12">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="CHG_REASON"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CHANGE REASON:
                  </label>
                  <input
                    type="text"
                    id="CHG_REASON"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "is-invalid":
                          formik.touched.CHG_REASON && formik.errors.CHG_REASON,
                      },
                      {
                        "is-valid":
                          formik.touched.CHG_REASON &&
                          !formik.errors.CHG_REASON,
                      }
                    )}
                    name="CHG_REASON"
                    placeholder="Enter Change Reason"
                    value={formik.values.CHG_REASON?.toUpperCase()}
                    //
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.CHG_REASON && formik.errors.CHG_REASON && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.CHG_REASON}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}
            </div>
            {/* end: final outer Edit Row */}
          </div>

          {/* Logic for Submit */}
          <div className="card-footer d-flex justify-content-center py-6 px-9">
            <button
              type="button"
              className="btn btn-light me-2"
              onClick={handleClose}
            >
              <span className="indicator-label">Discard</span>
            </button>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={formik.isSubmitting}
            >
              <span className="indicator-label">Submit</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default PermanentDetailsEdit;
