
import { Content } from "../../../../_metronic/layout/components/content";
import { PostJoiningDocs } from "../../../../_metronic/partials/widgets/tables/PostJoiningDocs";

export function PostJoiningDocsWrapper() {
  return (
    <Content>
      <div className="mb-5 mb-xl-8">
      <PostJoiningDocs className='mb-5 mb-xl-8' />
      </div>
    </Content>
  );
}
