// import { useEffect, useState } from "react";
// import { Link } from "react-router-dom";
// import axios from "axios";



// export function PresentDetails() {
//   const [basicDetails, setBasicDetails] = useState([]);
//   const [isCollapsed, setIsCollapsed] = useState(true);

//   useEffect(() => {
//     // Fetch Present details from the backend API
//     axios
//       .get("https://localhost:44360/api/Employee/H003333/EMPPER")
//       .then((response) => {
//         setBasicDetails(response.data[0]);
//       })
//       .catch((error) => {
//         console.error("Error fetching Present details:", error);
//       });
//   }, []);

//   const toggleCollapse = () => {
//     setIsCollapsed(!isCollapsed);
//   };

//   return (
//     <div className={`card`} id="Present Details">
//       <div
//         className="card-header cursor-pointer"
//         role="button"
//         onClick={toggleCollapse}
//         aria-expanded={!isCollapsed}
//         aria-controls="kt_account_profile_details"
//       >
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Present Details</h3>
//         </div>
//         <Link
//           to="/crafted/pages/profile/EditBasicDetails"
//           className="btn btn-sm btn-primary me-3 align-self-center"
//         >
//           Edit 
//         </Link>
//       </div>

//       <div className={`collapse ${isCollapsed ? '' : 'show'} card-body p-9`}>
//         {/* Begin:first Row */}
//         <div className="row">
//           {/* Begin:first Row     */}
//             <div className="row">
//               <div
//                 className="col-lg-6 mb-6"
//                 style={{
//                   display: "flex",
//                   flexDirection: "row",
//                   alignItems: "flex-start",
//                 }}
//               >
//                 <label className="col-lg-4 fw-bold text-muted">
//                   PRESENT ADDRESS :
//                 </label>
//                 <div
//                   style={{
//                     display: "flex",
//                     flexDirection: "column",
//                     alignItems: "flex-start",
//                   }}
//                 >
//                   <span
//                     className="fw-bold fs-6 text-gray-900 text-hover-primary"
//                     style={{ marginBottom: "-14px" }}
//                   >
//                     2132 A WARD DHOTRI LANE
//                   </span>
//                   <br />
//                   <span
//                     className="fw-bold fs-6 text-gray-900 text-hover-primary"
//                     style={{ marginBottom: "-14px" }}
//                   >
//                     SHKRUWAR PETH GANGAWESH
//                   </span>
//                   <br />
//                   <span
//                     className="fw-bold fs-6 text-gray-900 text-hover-primary"
//                     style={{ marginBottom: "-14px" }}
//                   >
//                     KARVEER KOLHAPUR 416010
//                   </span>
//                   <br />
//                 </div>
//               </div>

//               <div
//                 className="col-lg-6 mb-6"
//                 style={{
//                   display: "flex",
//                   flexDirection: "row",
//                   alignItems: "flex-start",
//                 }}
//               >
//                 <label className="col-lg-4 fw-bold text-muted">
//                   TELEPHONE :
//                 </label>
//                 <div
//                   style={{
//                     display: "flex",
//                     flexDirection: "column",
//                     alignItems: "flex-start",
//                   }}
//                 >
//                   <span
//                     className="fw-bold fs-6 text-gray-900 text-hover-primary"
//                     style={{ marginBottom: "-14px" }}
//                   >
//                     +0232
//                   </span>
//                   <br />
//                   <span
//                     className="fw-bold fs-6 text-gray-900 text-hover-primary"
//                     style={{ marginBottom: "-14px" }}
//                   >
//                     5122 3162
//                   </span>
//                   <br />
//                   <span
//                     className="fw-bold fs-6 text-gray-900 text-hover-primary"
//                     style={{ marginBottom: "-14px" }}
//                   >
//                     4182 4172
//                   </span>
//                   <br />
//                 </div>
//               </div>
//             </div>
//             {/* End:first Row     */}

//             {/* Begin:first Row     */}
//             <div className="row">
//               <div className="col-lg-6 mb-6">
//                 <label className="col-lg-4 fw-bold text-muted">
//                   MOBILE NO :{" "}
//                 </label>
//                 <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//               </div>
//               <div className="col-lg-6 mb-6">
//                 <label className="col-lg-4 fw-bold text-muted">STATE : </label>
//                 <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//               </div>
//             </div>
//             {/* End:first Row     */}

//             {/* Begin:first Row     */}
//             <div className="row">
//               <div className="col-lg-6 mb-6">
//                 <label className="col-lg-4 fw-bold text-muted">
//                   EMAIL(OFFICE) :{" "}
//                 </label>
//                 <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//               </div>
//               <div className="col-lg-6 mb-6">
//                 <label className="col-lg-4 fw-bold text-muted">CITY : </label>
//                 <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//               </div>
//             </div>

//             <div className="row">
//               <div className="col-lg-6 mb-6">
//                 <label className="col-lg-4 fw-bold text-muted">
//                   EMAIL(PERSONAL) :{" "}
//                 </label>
//                 <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//               </div>
//               <div className="col-lg-6 mb-6">
//                 <label className="col-lg-4 fw-bold text-muted">PIN : </label>
//                 <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//               </div>
//             </div>

//             <div className="row">
//               <div className="col-lg-6 mb-6">
//                 <label className="col-lg-4 fw-bold text-muted">
//                   SMS DOMAIN :{" "}
//                 </label>
//                 <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
//                   {basicDetails.contacT_CODE}
//                 </span>
//               </div>
//             </div>
//             {/* End:first Row     */}
//         </div>
//         {/* End:first Row */}
//       </div>
//     </div>
//   );
// }

import {FC} from 'react'

const PresentDetails: FC = () => {
  return (<div >
          <h1>This Page is under Development...</h1>
          <h3>please contact to admin</h3>
          </div>)
}

export {PresentDetails}
