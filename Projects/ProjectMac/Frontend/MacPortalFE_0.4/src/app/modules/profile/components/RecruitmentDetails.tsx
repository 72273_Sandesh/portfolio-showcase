import React, { useEffect, useState } from "react";
import { employeeIdForView } from "../../GlobalQuery";
import { InternalMode , ThemeModeComponent } from "../../../../_metronic/assets/ts/layout";
import axios from "axios";

type FamilyDetail = {
  panel:string;
  starT_DATE:string;
  source:string;
  status:string;
  commentS_PANEL:string;
  remarks:string;
  
};

export function Recruitment() {
  const [basicDetails, setBasicDetails] = useState<any>([]);
  const [isMinimized, setIsMinimized] = useState(false); // Default state: card is minimized
  const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;
  const currentMode: InternalMode = ThemeModeComponent.getCurrentMode();

  useEffect(() => {
    // Fetch basic details from the backend API
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/CANDIDATE_DET`)
      .then((response) => {
        setBasicDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching basic details:", error);
      });
  }, []);

  const [familyDetails, setFamilyDetails] = useState<FamilyDetail[]>([]);

  useEffect(() => {
    // Fetch family details from API
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/INTERVIEW_DET`)
      .then((response) => {
        setFamilyDetails(response.data);
      })
      .catch((error) => {
        console.error("Error fetching family details:", error);
      });
  }, []);

  const [offerDetails, setOfferDetails] = useState<any>([]);

  useEffect(() => {
    // Fetch family details from API
    axios
      .get(`${EMPLOYEE_URL}/${employeeIdForView}/eMp0F2r`)
      .then((response) => {
        setOfferDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching family details:", error);
      });
  }, []);

  // const toggleMinimize = () => {
  //   setIsMinimized(!isMinimized); // Toggle minimized/maximized state
  // };

  return (
    <div
      className={`card`}
      id="Employment Details"
      style={(currentMode === 'dark' ? {background: "#0F1014"} : { background: "#F9F9FA"})}
    >
      {/* <div
        className="card-header d-flex justify-content-between align-items-center cursor-pointer"
        // onClick={toggleMinimize}
      > */}
      {/* <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Recruitment History</h3>
        </div> */}
      {/* <Link
          to="/crafted/pages/profile/EditBasicDetails"
          className="btn btn-sm btn-primary me-3 align-self-center"
        >
          Edit
        </Link> */}
      {/* <button
            className={`btn btn-sm mt-3 btn-light btn-icon rounded-circle ${
              isMinimized ? "" : "active"
            }`}
           
            aria-label={isMinimized ?"Expand":  "Collapse" }
          >
            <KTIcon
              iconName={isMinimized ? "arrow-down" : "arrow-up"}
              className={`fs-3 ${isMinimized ? "text-dark" : "text-dark"}`}
            />
          </button> */}
      {/* </div> */}

      {!isMinimized && (
        <div className="cardrow g-2 mb-2 g-xxl-2">
          <div className={"rounded " + (currentMode === 'dark' ? "bg-light" : "bg-white")}>
            <div className="card-header bg-light-primary d-flex justify-content-between align-items-left cursor-pointer">
              <div className="card-title m-0">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                  Candidate Details
                </h3>
              </div>

              {/* <Link
                to="/crafted/pages/profile/EditCandidateDetails"
                //src/app/modules/profile/components/EditBasicDetails.tsx
                className="btn btn-sm btn-primary me-3 align-self-center"
              >
                Edit
              </Link> */}
            </div>
            <div className="card-body p-9">
              {/* Begin:first Row */}
              <div className="row">
                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      CANDIDATE NAME :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.candidatE_NAME}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      LOCATION :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.sitE_DESCR}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      EXPERIENCE :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.toT_EXP}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      RESUME RECEIVED DATE :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {/* {basicDetails?.receiveD_DATE} */}
                      {basicDetails?.receiveD_DATE
                        ? new Date(
                            basicDetails?.receiveD_DATE
                          ).toLocaleDateString("en-GB")
                        : ""}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      {" "}
                      CURRENT COMPANY :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.curR_COMPANY}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      CURRENT DEPARTMENT :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.curR_DEPARTMENT}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}
                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      CURRENT DESIGNATION :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {basicDetails?.curR_DESIGNATION}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      CURRENT CTC :{" "}
                    </label>
                    <span className="badge badge-light-success fs-6  text-hover-primary">
                      {basicDetails?.curR_CTC}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}
              </div>
              {/* End:first Row */}
            </div>
          </div>
        </div>
      )}

      {!isMinimized && (
        <div className="cardrow g-2 mb-2 g-xxl-2">
          <div className={"rounded " + (currentMode === 'dark' ? "bg-light" : "bg-white")}>
            <div>
              {/* begin::Header */}
              <div className="card-header bg-light-primary border-0 pt-5">
                <h3 className="card-title align-items-start flex-column">
                 
                  <div className="card-title m-0">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                Interview Details
                </h3>
              </div>
               
                  <span className="text-muted mt-1 fw-semibold fs-7">
                    {familyDetails.length} Records
                  </span>
                </h3>
                <div className="card-toolbar">
                  {/* <Link
                    to="/crafted/pages/profile/AddInterviewDetails"
                    className="btn btn-sm btn-primary me-3 align-self-center"
                  >
                    Add
                  </Link>
                  <Link
                    to="/crafted/pages/profile/EditInterviewDetails"
                    className="btn btn-sm btn-primary me-3 align-self-center"
                  >
                    Edit
                  </Link> */}
                </div>
              </div>
              {/* end::Header */}
              {/* begin::Body */}
              <div className="card-body py-3">
                <div className="table-responsive">
                  <table className="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                    {/* begin::Table head */}
                    <thead>
                      <tr className="fw-bold text-muted">
                        <th className="w-25px">Sr.NO.</th>
                        <th className="min-w-150px">Interview Panel</th>
                        <th className="min-w-120px">Date</th>
                        <th className="min-w-140px">Source </th>
                        <th className="min-w-120px">Status </th>
                        <th className="min-w-120px">Panel Remarks</th>
                        <th className="min-w-120px">Remarks</th>
                        {/* <th className="min-w-120px">Pension</th> */}

                        {/* <th className="min-w-100px text-end">Actions</th> */}
                      </tr>
                    </thead>
                    {/* end::Table head */}
                    <tbody>
                      {familyDetails.map((familyDetail, index) => (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>
                            <div className="d-flex align-items-center">
                              <div className="d-flex justify-content-start flex-column">
                                <a
                                  href="#"
                                  className="text-gray-900 fw-bold text-hover-primary fs-6"
                                >
                                  {familyDetail?.panel}
                                </a>
                                {/* <span className="fw-bolder d-flex align-items-right">
                          <div className="badge badge-light-success fw-bolder d-flex align-items-right">
                            {" "}
                            {familyDetail.sex === "M" ? "Male" : "Female"}
                          </div>
                          <div className="badge badge-light-info fw-bolder d-flex align-items-right">
                            {familyDetail.relation}
                          </div>
                        </span> */}
                              </div>
                            </div>
                          </td>
                          {/* <td>
                    <span className='fw-bolder d-flex align-items-right'>
                      <div className='badge badge-light-success fw-bolder d-flex align-items-right'>{familyDetail.datE_BIRTH}</div>
                    </span>
                  </td> */}

                          <td>
                            <span className="fw-bolder d-flex align-items-right">
                              <div className="fw-bolder d-flex align-items-right">
                                {/* Convert date format */}
                                {familyDetail?.starT_DATE
                                  ? new Date(
                                      familyDetail?.starT_DATE
                                    ).toLocaleDateString("en-GB")
                                  : ""}
                              </div>
                            </span>
                          </td>

                          <td>
                            <a
                              href="#"
                              className="text-gray-900 fw-bold text-hover-primary d-block fs-6"
                            >
                              {familyDetail?.source}
                            </a>
                          </td>

                          <td>
                            <div className="badge badge-light-danger fw-bolder align-items-right">
                              {familyDetail?.status}
                            </div>
                          </td>

                          <td>
                            <a
                              href="#"
                              className="text-gray-900 fw-bold text-hover-primary d-block fs-6"
                            >
                              {familyDetail?.commentS_PANEL}
                            </a>
                          </td>

                          <td>
                            <a
                              href="#"
                              className="text-gray-900 fw-bold text-hover-primary d-block fs-6"
                            >
                              {familyDetail?.remarks}
                            </a>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>

              {/* end::Body */}
            </div>
          </div>
        </div>
      )}

      {!isMinimized && (
        <div className="cardrow g-2 mb-2 g-xxl-2">
          <div className={"rounded " + (currentMode === 'dark' ? "bg-light" : "bg-white")}>
            <div className="card-header bg-light-primary d-flex justify-content-between align-items-left cursor-pointer">
              <div className="card-title m-0 ">
                <h3 className="fw-bolder m-0 text-gray-800 fs-6">
                  Offer Details
                </h3>
              </div>
              {/* <Link
                to="/crafted/pages/profile/EditOfferDetails"
                className="btn btn-sm btn-primary me-3 align-self-center"
              >
                Edit
              </Link> */}
            </div>
            <div className="card-body p-9">
              {/* Begin:first Row */}
              <div className="row">
                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      PREVIOUS CTC :{" "}
                    </label>
                    <span className="badge badge-light-success fs-6  text-hover-primary">
                      {offerDetails?.curR_CTC}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      PREVIOUS DESIGNATION :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {offerDetails?.curR_DESIGNATION}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      OFFERED CTC :{" "}
                    </label>
                    <span className="badge badge-light-success fs-6  text-hover-primary">
                      {offerDetails?.offereD_CTC}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      OFFERED DESIGNATION :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {offerDetails?.offereD_DESIGNATION}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      {" "}
                      EXPECTED JOIN DATE:{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {/* {offerDetails.exP_DOJ} */}
                      {offerDetails?.exP_DOJ
                        ? new Date(offerDetails?.exP_DOJ).toLocaleDateString(
                            "en-GB"
                          )
                        : ""}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      DEVIATION IN JOINING :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {offerDetails?.deV_JOIN_DAY}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      VACANCY :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {offerDetails?.vaC_NO}
                    </span>
                  </div>
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      OFFER ID :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {offerDetails?.offeR_ID}
                    </span>
                  </div>
                </div>
                {/* End:first Row     */}

                {/* Begin:first Row     */}
                <div className="row">
                  <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      OFFER DATE :{" "}
                    </label>
                    <span className="fw-bold fs-6 text-gray-900 text-hover-primary">
                      {/* {offerDetails?.contacT_CODE} */}

                      {offerDetails?.offeR_DATE
                        ? new Date(offerDetails?.offeR_DATE).toLocaleDateString(
                            "en-GB"
                          )
                        : ""}
                    </span>
                  </div>
                  {/* <div className="col-lg-6 mb-6">
                    <label style={{ minWidth: "150px" }} className="col-lg-4 fw-bold text-muted">
                      SALARY OFFERED :{" "}
                    </label>
                    <span className="badge badge-light-success fw-bold fs-6 text-hover-primary">
                      {offerDetails?.offereD_CTC}
                    </span>
                  </div> */}
                </div>
                {/* End:first Row     */}
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
