// import React, { useState, useEffect } from "react";
// import axios from "axios";
// import { useNavigate } from "react-router-dom";
// import { useFormik } from "formik";
// import * as Yup from "yup";
// import { employeeIdForView } from "../../GlobalQuery";
// import { toast } from "react-toastify";
// const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

// interface Response {
//   responseStatus:string;
//   message:string;
// }

// const StatutoryDetailsEdit: React.FC = () => {
//   const [employee, setEmployee] = useState<any>({});
//   const navigate = useNavigate();

//   useEffect(() => {
//     axios
//       .get(`${EMPLOYEE_URL}/${employeeIdForView}/EMPPER`)
//       .then((response) => {
//         setEmployee(response.data[0]);
//         // Update formik's initial values after fetching employee data
//         formik.setValues({
//           //   induction: response.data[0].induction || "",
//           pF_UN_NO: response.data[0].pF_UN_NO || "",
//           pF_NO: response.data[0].pF_NO || "",
//           fpF_NO: response.data[0].fpF_NO || "",
//           esiC_NO: response.data[0].esiC_NO || "",
//         });
//       })
//       .catch((error) => {
//         console.error("Error fetching basic details:", error);
//       });
//   }, []);

//   const validationSchema = Yup.object().shape({
//     // pF_UN_NO: Yup.string().required("PF UN No required"),
//     // pF_NO: Yup.string().required("PF No required"),
//     // fpF_NO: Yup.string().required("FPF No required"),
//     // esiC_NO: Yup.string().required("ESIC No required"),
//   });

//   const formik = useFormik({
//     initialValues: {
//       pF_UN_NO: "",
//       pF_NO: "",
//       fpF_NO: "",
//       esiC_NO: "",
//     },
//     validationSchema: validationSchema,
//     // onSubmit: (values) => {
//     //   console.log('Submitting form data:', values);
//     //   // Implement your submit logic here, e.g., send updated data to the server

//     //   setEmployee({ sitE_NAME: '', depT_NAME: '' ,designation:'',section:""});
//     //   formik.resetForm();// Upon successful submission, clear the form
//     // },

//     onSubmit: (values) => {
//       console.log("Submitting form data:", values);

//       axios
//         .post<Response>("your_backend_endpoint", values)
//         .then((response) => {
//           console.log("Post successful!", response.data);
//           if (response.data.responseStatus === "E") {
//             toast.error(
//               <div>
//                 <h4 className="alert-heading">Failed to submit Form...</h4>
//                 <p className="alert alert-danger" role="alert">
//                   <h6>{Array.isArray(response.data.message) ? response.data.message.join("\n") : response.data.message}</h6>
//                 </p>
//               </div>
//             );
//           } else {
//             toast.success(
//               <div>
//                 <h4 className="alert-heading">Successfully Submitted Form...</h4>
//                 <p className="alert alert-success" role="alert">
//                   <h6>{Array.isArray(response.data.message) ? response.data.message.join("\n") : response.data.message}</h6>
//                 </p>
//               </div>
//             );
//           }
//         })
//         .catch((error) => {
//           console.error("Error posting data:", error);
//           toast.error(
//             <div>
//               <h6 className="alert-heading">Error Posting Data</h6>
//               <p className="alert alert-danger" role="alert">
//                 <h6>{error.message}</h6>
//               </p>
//             </div>
//           );
//         });

//       setEmployee({
//         pF_UN_NO: "",
//         pF_NO: "",
//         fpF_NO: "",
//         esiC_NO: "",
//       });
//       formik.resetForm();
//       navigate(-1);
//     },

//   });

//   const handleClose = () => {
//     navigate(-1);
//   };

//   return (
//     <div className="card mb-1 mb-xl-2">
//       <div
//         className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
//         role="button"
//         data-bs-target="#kt_account_profile_details"
//         aria-expanded="true"
//         aria-controls="kt_account_profile_details"
//       >
//         <div className="card-title m-0">
//           <h3 className="fw-bolder m-0">Edit Statutory Details</h3>
//         </div>
//         <button
//           type="button"
//           className="btn-close"
//           aria-label="Close"
//           onClick={handleClose}
//           style={{ marginRight: "10px", cursor: "pointer" }}
//         ></button>
//       </div>

//       <div id="kt_account_profile_details" className="collapse show">
//         <form onSubmit={formik.handleSubmit} className="form">
//           <div className="card-body border-top p-9">
//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="pF_UN_NO"
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   PF UAN NO.:
//                 </label>
//                 <input
//                   type="text"
//                   id="pF_UN_NO"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="pF_UN_NO"
//                   placeholder="Enter PF UAN NO"
//                   value={formik.values?.pF_UN_NO}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="pF_NO"
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   PF NO.:
//                 </label>
//                 <input
//                   type="text"
//                   id="pF_NO"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="pF_NO"
//                   placeholder="Enter PF NO"
//                   value={formik.values.pF_NO?.toUpperCase()}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.pF_UN_NO && formik.errors.pF_UN_NO ? (
//                   <div className="text-danger">{formik.errors.pF_UN_NO}</div>
//                 ) : null}
//               </div>
//               <div className="col-lg-6">
//                 {formik.touched.pF_NO && formik.errors.pF_NO ? (
//                   <div className="text-danger">{formik.errors.pF_NO}</div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}

//             {/* Begin: Edit Row */}
//             <div className="row mb-1">
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="esiC_NO"
//                   className="col-form-label  fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   ESIC NO :
//                 </label>
//                 <input
//                   type="text"
//                   id="esiC_NO"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="esiC_NO"
//                   placeholder="Enter ESIC NO Code"
//                   value={formik.values?.esiC_NO}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//               <div className="col-lg-6 d-flex align-items-center mb-1">
//                 <label
//                   htmlFor="fpF_NO"
//                   className="col-form-label fw-bold fs-6 me-2"
//                   style={{ minWidth: "150px" }}
//                 >
//                   FPF NO :
//                 </label>
//                 <input
//                   type="text"
//                   id="fpF_NO"
//                   className="form-control form-control-lg form-control-solid bg-light-danger"
//                   style={{ cursor: "not-allowed" }}
//                   name="fpF_NO"
//                   placeholder="Enter Full Name"
//                   value={formik.values.fpF_NO?.toUpperCase()}
//                   readOnly
//                   onChange={formik.handleChange}
//                   onBlur={formik.handleBlur}
//                 />
//               </div>
//             </div>
//             <div className="row mb-1">
//               <div className="col-lg-6">
//                 {formik.touched.esiC_NO && formik.errors.esiC_NO ? (
//                   <div className="text-danger">{formik.errors.esiC_NO}</div>
//                 ) : null}
//               </div>
//               <div className="col-lg-6">
//                 {formik.touched.fpF_NO && formik.errors.fpF_NO ? (
//                   <div className="text-danger">{formik.errors.fpF_NO}</div>
//                 ) : null}
//               </div>
//             </div>

//             {/* End: Edit Row */}
//           </div>

//           {/* Logic for Submit is HERE */}
//           <div className="card-footer d-flex justify-content-center py-6 px-9">
//             <button
//               type="button"
//               className="btn btn-light me-2"
//               onClick={handleClose}
//             >
//               <span className="indicator-label">Discard</span>
//             </button>

//             <button
//               type="submit"
//               className="btn btn-primary"
//               disabled={formik.isSubmitting || !formik.isValid}
//             >
//               <span className="indicator-label">Submit</span>
//             </button>
//           </div>
//         </form>
//       </div>
//     </div>
//   );
// };
// export default StatutoryDetailsEdit;

import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
import { useListView } from "../../apps/transaction-master/master-list/core/ListViewProvider";
import clsx from "clsx";
import { Mode, QUERIES } from "../../../../_metronic/helpers";
import { useQueryResponse } from "../../apps/employee/employee-list/core/QueryResponseProvider";
import { useMutation, useQueryClient } from "react-query";
import { insertTransactionV2 } from "../../apps/transaction-master/master-list/core/_requests";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

type FieldDetail = {
  olD_KEY: string;
  olD_VALUE: string;
  neW_KEY: string;
  neW_VALUE: string;
  FILE_PATH: string;
};

type StatutoryDetails = {
  emP_CODE: string;
  pF_UN_NO: string;
  pF_NO: string;
  fpF_NO: string;
  esiC_NO: string;
  chG_REASON: string;

  fieldNames?: {
    EMP_CODE: FieldDetail;
    PF_UN_NO: FieldDetail;
    PF_NO: FieldDetail;
    FPF_NO: FieldDetail;
    ESIC_NO: FieldDetail;
    CHG_REASON: FieldDetail;
  };
};

const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const StatutoryDetailsEdit: React.FC = () => {
  const [statutoryDetails, setStatutoryDetails] = useState<StatutoryDetails>();
  const [statutoryDetailsTx, setStatutoryDetailsTx] =
    useState<StatutoryDetails>();

  const navigate = useNavigate();

  const { currentUser } = useAuth();
  const [privateIP, setPrivateIP] = useState();

  const { itemIdForUpdate, setItemIdForUpdate, formMode } = useListView();
  const [empCodeTxForView, setEmpCodeTxForView] = useState();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();

  useEffect(() => {
    // itemIdForUpdate !== undefined ?
    itemIdForUpdate && fetchStatutoryDetailsTx();
    // :
    fetchStatutoryDetails();
  }, [empCodeTxForView]);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const fetchStatutoryDetails = () => {
    axios
      .get(
        `${EMPLOYEE_URL}/${
          employeeIdForView ? employeeIdForView : empCodeTxForView
        }/EMPPER`
      )
      .then((response) => {
        setStatutoryDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching BASIC details:", error);
      });
  };

  const fetchStatutoryDetailsTx = () => {
    axios
      .get(`${MASTERTX_URL}/GetTransactionByID?tranId=${itemIdForUpdate}`)
      .then((response) => {
        setStatutoryDetailsTx(response.data.transactions[0]);
        setEmpCodeTxForView(
          response.data.transactions[0].fieldNames?.EMP_CODE?.neW_VALUE
        );
      });
  };

  useEffect(() => {
    formik.setValues({
      emP_CODE:
        statutoryDetailsTx?.fieldNames?.EMP_CODE?.neW_KEY ||
        statutoryDetails?.emP_CODE ||
        "",
      emP_CODE_O:
        statutoryDetailsTx?.fieldNames?.EMP_CODE?.neW_KEY ||
        statutoryDetails?.emP_CODE ||
        "",

      pF_UN_NO:
        statutoryDetailsTx?.fieldNames?.PF_UN_NO?.neW_KEY ||
        statutoryDetails?.pF_UN_NO ||
        "",
      pF_UN_NO_O:
        statutoryDetailsTx?.fieldNames?.PF_UN_NO?.neW_KEY ||
        statutoryDetails?.pF_UN_NO ||
        "",

      pF_NO:
        statutoryDetailsTx?.fieldNames?.PF_NO?.neW_KEY ||
        statutoryDetails?.pF_NO ||
        "",
      pF_NO_O:
        statutoryDetailsTx?.fieldNames?.PF_NO?.neW_KEY ||
        statutoryDetails?.pF_NO ||
        "",

      fpF_NO:
        statutoryDetailsTx?.fieldNames?.FPF_NO?.neW_KEY ||
        statutoryDetails?.fpF_NO ||
        "",
      fpF_NO_O:
        statutoryDetailsTx?.fieldNames?.FPF_NO?.neW_KEY ||
        statutoryDetails?.fpF_NO ||
        "",

      esiC_NO:
        statutoryDetailsTx?.fieldNames?.ESIC_NO?.neW_KEY ||
        statutoryDetails?.esiC_NO ||
        "",
      esiC_NO_O:
        statutoryDetailsTx?.fieldNames?.ESIC_NO?.neW_KEY ||
        statutoryDetails?.esiC_NO ||
        "",

      chG_REASON: formMode === Mode.EDIT ? "" : statutoryDetails?.chG_REASON,
    });
  }, [statutoryDetails, statutoryDetailsTx]);

  const validationSchema = Yup.object().shape({
    pF_UN_NO: Yup.string().required("PF UAN Number is required"),
    pF_NO: Yup.string().required("PF number is required"),
    fpF_NO: Yup.string().required("FPF number is required"),
    esiC_NO: Yup.string().required("ESIC number is required"),

    chG_REASON: Yup.string().required("Change Reason is required"),
  });

  const createTx = useMutation((payload: any) => insertTransactionV2(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        handleClose();
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formik = useFormik({
    initialValues: {
      emP_CODE: "",
      emP_CODE_O: "",

      pF_UN_NO: "",
      pF_UN_NO_O: "",

      pF_NO: "",
      pF_NO_O: "",

      fpF_NO: "",
      fpF_NO_O: "",

      esiC_NO: "",
      esiC_NO_O: "",

      chG_REASON: "",
    },
    validationSchema: validationSchema,

    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      const payload = {
        values,

        TABLENAME: "EMPLOYEE_STATUTORY",
        KEYVAR: "",
        WFLEVEL: 1,
        CONF: "N",
        SITECODE: "S0001",
        EMP: currentUser?.id?.toString() || "",
        FILEPATH: null,
        FILE: null,
        FOLDERPATH: null,
        FORMNAME: "STATUTORY Details",
        tX_CHG_USER: currentUser?.id?.toString() || "",
        CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        STRTRANID: itemIdForUpdate ? itemIdForUpdate : null,
      };
      createTx.mutate(payload);

      setSubmitting(false);
    },
  });

  // const handlePdfChange = (
  //   e: React.ChangeEvent<HTMLInputElement>,
  //   fieldName: string
  // ) => {
  //   const file = e.target.files?.[0];
  //   if (file) {
  //     const reader = new FileReader();

  //     reader.onloadend = () => {
  //       const base64String = reader.result;
  //       formik.setFieldValue(fieldName, base64String);
  //     };

  //     reader.readAsDataURL(file);
  //   }
  // };

  // const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
  //   const file = e.target.files?.[0];
  //   if (file) {
  //     formik.setFieldValue("pdf", file);
  //     formData.append("pdf", file);
  //   }
  // };

  const handleClose = () => {
    itemIdForUpdate ? setItemIdForUpdate(undefined) : navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit KYC Details</h3>
        </div>

        <div>
          <button
            type="button"
            className="btn-close"
            aria-label="Close"
            onClick={handleClose}
            style={{ marginRight: "10px", cursor: "pointer" }}
          ></button>
        </div>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: final outer Edit Row */}
            <div className="row mb-1">
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="pF_UN_NO"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PF UAN NO.:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="number"
                      id="pF_UN_NO"
                      name="pF_UN_NO"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            statutoryDetailsTx?.fieldNames?.PF_UN_NO?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.pF_UN_NO && formik.errors.pF_UN_NO,
                        },
                        {
                          "is-valid":
                            formik.touched.pF_UN_NO && !formik.errors.pF_UN_NO,
                        }
                      )}
                      {...(statutoryDetailsTx?.fieldNames?.PF_UN_NO && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          statutoryDetailsTx?.fieldNames?.PF_UN_NO
                            ?.olD_VALUE === null
                            ? "-"
                            : statutoryDetailsTx?.fieldNames?.PF_UN_NO
                                ?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter PF UAN Number"
                      value={formik.values.pF_UN_NO}
                      onChange={(e) =>
                        formik.setFieldValue("pF_UN_NO", e.target.value)
                      }
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                {formik.touched.pF_UN_NO && formik.errors.pF_UN_NO && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.pF_UN_NO}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="pF_NO"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    PF NO.:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="number"
                      id="pF_NO"
                      name="pF_NO"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            statutoryDetailsTx?.fieldNames?.PF_NO?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.pF_NO && formik.errors.pF_NO,
                        },
                        {
                          "is-valid":
                            formik.touched.pF_NO && !formik.errors.pF_NO,
                        }
                      )}
                      {...(statutoryDetailsTx?.fieldNames?.PF_NO && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          statutoryDetailsTx?.fieldNames?.PF_NO?.olD_VALUE ===
                          null
                            ? "-"
                            : statutoryDetailsTx?.fieldNames?.PF_NO?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter PF Number"
                      value={formik.values.pF_NO}
                      onChange={(e) =>
                        formik.setFieldValue("pF_NO", e.target.value)
                      }
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                {formik.touched.pF_NO && formik.errors.pF_NO && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.pF_NO}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="esiC_NO"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    ESIC NO.:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="number"
                      id="esiC_NO"
                      name="esiC_NO"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            statutoryDetailsTx?.fieldNames?.ESIC_NO?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.esiC_NO && formik.errors.esiC_NO,
                        },
                        {
                          "is-valid":
                            formik.touched.esiC_NO && !formik.errors.esiC_NO,
                        }
                      )}
                      {...(statutoryDetailsTx?.fieldNames?.ESIC_NO && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          statutoryDetailsTx?.fieldNames?.ESIC_NO?.olD_VALUE ===
                          null
                            ? "-"
                            : statutoryDetailsTx?.fieldNames?.ESIC_NO?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter ESIC Number"
                      value={formik.values.esiC_NO}
                      onChange={(e) =>
                        formik.setFieldValue("esiC_NO", e.target.value)
                      }
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                {formik.touched.esiC_NO && formik.errors.esiC_NO && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.esiC_NO}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="fpF_NO"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    FPF NO.:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="number"
                      id="fpF_NO"
                      name="fpF_NO"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            statutoryDetailsTx?.fieldNames?.FPF_NO?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.fpF_NO && formik.errors.fpF_NO,
                        },
                        {
                          "is-valid":
                            formik.touched.fpF_NO && !formik.errors.fpF_NO,
                        }
                      )}
                      {...(statutoryDetailsTx?.fieldNames?.FPF_NO && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          statutoryDetailsTx?.fieldNames?.FPF_NO?.olD_VALUE ===
                          null
                            ? "-"
                            : statutoryDetailsTx?.fieldNames?.FPF_NO?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter FPF Number"
                      value={formik.values.fpF_NO}
                      onChange={(e) =>
                        formik.setFieldValue("fpF_NO", e.target.value)
                      }
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                {formik.touched.fpF_NO && formik.errors.fpF_NO && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.fpF_NO}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="chG_REASON"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CHANGE REASON:
                  </label>
                  <input
                    type="text"
                    id="chG_REASON"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "is-invalid":
                          formik.touched.chG_REASON && formik.errors.chG_REASON,
                      },
                      {
                        "is-valid":
                          formik.touched.chG_REASON &&
                          !formik.errors.chG_REASON,
                      }
                    )}
                    name="chG_REASON"
                    placeholder="Enter Change Reason"
                    value={formik.values.chG_REASON?.toUpperCase()}
                    //
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.chG_REASON && formik.errors.chG_REASON && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.chG_REASON}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}
            </div>
            {/* end: final outer Edit Row */}

            {/* Logic for Submit is HERE : start */}
            <div className="card-footer d-flex justify-content-center py-6 px-9">
              <button
                type="button"
                className="btn btn-light me-2"
                onClick={handleClose}
              >
                <span className="indicator-label">Discard</span>
              </button>

              <button
                type="submit"
                className="btn btn-primary"
                disabled={formik.isSubmitting}
              >
                <span className="indicator-label">Submit</span>
              </button>
            </div>
            {/* Logic for Submit is HERE : end */}
          </div>
        </form>
      </div>
    </div>
  );
};

export default StatutoryDetailsEdit;
