import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { employeeIdForView } from "../../GlobalQuery";
import { toast } from "react-toastify";
import { useAuth } from "../../auth";
import { useListView } from "../../apps/transaction-master/master-list/core/ListViewProvider";
import clsx from "clsx";
import { createTransaction } from "../../apps/transaction-master/master-list/core/_models";
import { Mode, QUERIES } from "../../../../_metronic/helpers";
import { useQueryResponse } from "../../apps/employee/employee-list/core/QueryResponseProvider";
import { useMutation, useQueryClient } from "react-query";
import { insertTransaction } from "../../apps/transaction-master/master-list/core/_requests";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

type FieldDetail = {
  olD_KEY: string;
  olD_VALUE: string;
  neW_KEY: string;
  neW_VALUE: string;
  FILE_PATH: string;
};

type WorkAndPayroll = {
  EMP_CODE: string;
  WORK_SITE: string;
  GRATUITY_DATE: string;
  NOTICE_PRD: string;
  BASIC: string;
  CURR_CTC: string;
  PAY_MODE: string;
  PAY_TYPE: string;
  BANK_CODE: string;
  BANK_IFSC: string;
  BANK_ACCT: string;
  DD_PAYABLE_BANK: string;
  pdf: string;
  CHG_REASON: string | undefined;

  emP_CODE: string;
  worK_SITE: string;
  gratuitY_DATE: string;
  noticE_PRD: string;
  basic: string;
  curR_CTC: string;
  paY_MODE: string;
  paY_TYPE: string;
  banK_CODE: string;
  banK_IFSC: string;
  banK_ACCT: string;
  dD_PAYABLE_BANK: string;

  paymenT_MODE: string;
  paymenT_TYPE: string;
  banK_NAME: string;
  sitE_NAME: string;

  fieldNames?: {
    EMP_CODE: FieldDetail;
    WORK_SITE: FieldDetail;
    GRATUITY_DATE: FieldDetail;
    NOTICE_PRD: FieldDetail;
    BASIC: FieldDetail;
    CURR_CTC: FieldDetail;
    PAY_MODE: FieldDetail;
    PAY_TYPE: FieldDetail;
    BANK_CODE: FieldDetail;
    BANK_IFSC: FieldDetail;
    BANK_ACCT: FieldDetail;
    DD_PAYABLE_BANK: FieldDetail;
    pdf: FieldDetail;
    CHG_REASON: FieldDetail;
  };
};

interface WorkSiteDropdownItem {
  siteCode: string;
  description: string;
  areaCode: string;
  address: string;
  financialEntity: string;
}
interface BankDetailsDropdown {
  banK_CODE: string;
  banK_NAME: string;
  banK_TYPE: string;
}

const MASTERTX_URL = import.meta.env.VITE_MASTER_API_URL;
const EMPLOYEE_URL = import.meta.env.VITE_EMPLOYEE_API_URL;

const WorkPayrollDetailsEdit: React.FC = () => {
  const [workPayrollDetails, setWorkPayrollDetails] =
    useState<WorkAndPayroll>();
  const [workPayrollTx, setWorkPayrollTx] = useState<WorkAndPayroll>();
  const navigate = useNavigate();
  const [drpdwnEmpSites, setDrpdwnEmpSites] = useState<WorkSiteDropdownItem[]>(
    []
  );
  const [bankNameDropdown, setBankNameDropdown] = useState<
    BankDetailsDropdown[]
  >([]);
  const [payModes] = useState([
    { code: "C", descr: "CASH" },
    { code: "Q", descr: "CHEQUE" },
    { code: "D", descr: "DEMAND DRAFT" },
    { code: "T", descr: "TRANSFER" },
  ]);
  const [payTypes] = useState([
    { code: "S", descr: "SALARY" },
    { code: "W", descr: "WAGES" },
  ]);
  const { currentUser } = useAuth();
  const [privateIP, setPrivateIP] = useState();

  const { itemIdForUpdate, setItemIdForUpdate, formMode } = useListView();
  const [empCodeTxForView, setEmpCodeTxForView] = useState();

  const { query } = useQueryResponse();
  const queryClient = useQueryClient();

  useEffect(() => {
    axios
      .get(
        `${EMPLOYEE_URL}/EMP_WORK_PAYROLL_site/${
          employeeIdForView ? employeeIdForView : empCodeTxForView
        }`
      )
      .then((response) => {
        setDrpdwnEmpSites(response.data);
      })
      .catch((error) => {
        console.error("Error fetching EmpSite details:", error);
      });

    axios
      .get(`${EMPLOYEE_URL}/ddl_GetBank`)
      .then((response) => {
        setBankNameDropdown(response.data.banks);
      })
      .catch((error) =>
        console.error("Error fetching Bank Names data:", error)
      );

    // itemIdForUpdate !== undefined ?
    itemIdForUpdate && fetchBankDetailsTx();
    // :
    fetchBankDetails();
  }, [empCodeTxForView]);

  useEffect(() => {
    axios
      .get(`https://localhost:44360/api/Ip/GetPrivateIpAddress`)
      .then((response) => {
        setPrivateIP(response.data);
      })
      .catch((error) => console.error("Error fetching combined data:", error));
  }, []);

  const fetchBankDetails = () => {
    axios
      .get(
        `${EMPLOYEE_URL}/${
          employeeIdForView ? employeeIdForView : empCodeTxForView
        }/EMPPER`
      )
      .then((response) => {
        setWorkPayrollDetails(response.data[0]);
      })
      .catch((error) => {
        console.error("Error fetching BASIC details:", error);
      });
  };

  const fetchBankDetailsTx = () => {
    axios
      .get(`${MASTERTX_URL}/GetTransactionByID?tranId=${itemIdForUpdate}`)
      .then((response) => {
        setWorkPayrollTx(response.data.transactions[0]);
        setEmpCodeTxForView(
          response.data.transactions[0].fieldNames?.EMP_CODE?.neW_VALUE
        );
      });
  };

  useEffect(() => {
    formik.setValues({
      EMP_CODE:
        workPayrollTx?.fieldNames?.EMP_CODE?.neW_KEY ||
        workPayrollDetails?.emP_CODE ||
        "",

      WORK_SITE:
        workPayrollTx?.fieldNames?.WORK_SITE?.neW_KEY ||
        workPayrollDetails?.worK_SITE ||
        "",

      GRATUITY_DATE:
        workPayrollTx?.fieldNames?.GRATUITY_DATE?.neW_KEY ||
        workPayrollDetails?.gratuitY_DATE ||
        "",

      NOTICE_PRD:
        workPayrollTx?.fieldNames?.NOTICE_PRD?.neW_KEY ||
        workPayrollDetails?.noticE_PRD ||
        "",

      BASIC:
        workPayrollTx?.fieldNames?.BASIC?.neW_KEY ||
        workPayrollDetails?.basic ||
        "",

      CURR_CTC:
        workPayrollTx?.fieldNames?.CURR_CTC?.neW_KEY ||
        workPayrollDetails?.curR_CTC ||
        "",

      PAY_MODE:
        workPayrollTx?.fieldNames?.PAY_MODE?.neW_KEY ||
        workPayrollDetails?.paY_MODE ||
        "",

      PAY_TYPE:
        workPayrollTx?.fieldNames?.PAY_TYPE?.neW_KEY ||
        workPayrollDetails?.paY_TYPE ||
        "",

      BANK_CODE:
        workPayrollTx?.fieldNames?.BANK_CODE?.neW_KEY ||
        workPayrollDetails?.banK_CODE.trim() ||
        "",

      BANK_IFSC:
        workPayrollTx?.fieldNames?.BANK_IFSC?.neW_KEY ||
        workPayrollDetails?.banK_IFSC ||
        "",

      BANK_ACCT:
        workPayrollTx?.fieldNames?.BANK_ACCT?.neW_KEY ||
        workPayrollDetails?.banK_ACCT ||
        "",

      DD_PAYABLE_BANK:
        workPayrollTx?.fieldNames?.DD_PAYABLE_BANK?.neW_KEY ||
        workPayrollDetails?.dD_PAYABLE_BANK ||
        "",

      pdf:
        workPayrollTx?.fieldNames?.BANK_ACCT?.FILE_PATH ||
        workPayrollDetails?.pdf ||
        "",

      CHG_REASON: formMode === Mode.EDIT ? "" : workPayrollDetails?.CHG_REASON,

      paymenT_MODE:
        workPayrollTx?.fieldNames?.PAY_MODE?.neW_VALUE ||
        workPayrollDetails?.paymenT_MODE ||
        "",

      paymenT_TYPE:
        workPayrollTx?.fieldNames?.PAY_TYPE?.neW_VALUE ||
        workPayrollDetails?.paY_TYPE === "S"
          ? "SALARY"
          : "WAGES",

      banK_NAME:
        workPayrollTx?.fieldNames?.BANK_CODE?.neW_VALUE ||
        workPayrollDetails?.banK_NAME ||
        "",

      sitE_NAME:
        workPayrollTx?.fieldNames?.WORK_SITE?.neW_VALUE ||
        workPayrollDetails?.sitE_NAME ||
        "",
    });
  }, [workPayrollDetails, workPayrollTx]);

  const validationSchema = Yup.object().shape({
    WORK_SITE: Yup.string().required("Work Site is required"),
    GRATUITY_DATE: Yup.string().required("Gratuity Date is required"),
    NOTICE_PRD: Yup.string().required("Notice Peroid is required"),
    BASIC: Yup.string().required("BASIC Salary is required"),
    CURR_CTC: Yup.string().required("Current CTC is required"),
    PAY_MODE: Yup.string().required("Payment Mode is required"),
    PAY_TYPE: Yup.string().required("Payment Type is required"),
    BANK_CODE: Yup.string().required("Bank Name is required"),
    BANK_IFSC: Yup.string().required("Bank IFSC is required"),
    BANK_ACCT: Yup.string().required("Bank Account is required"),
    DD_PAYABLE_BANK: Yup.string().required("DD Payable Bank is required"),
    // pdf: Yup.string().required("pdf is required"),
    pdf: Yup.string().when([], {
      is: () =>
        workPayrollDetails?.banK_CODE.trim() !== formik.values.BANK_CODE ||
        workPayrollDetails?.banK_IFSC !== formik.values.BANK_IFSC ||
        workPayrollDetails?.banK_ACCT !== formik.values.BANK_ACCT.toString(),
      then: (schema) => schema.required("Proof attachment is required"),
      otherwise: (schema) => schema.notRequired(),
    }),
    CHG_REASON: Yup.string().required("Change Reason is required"),

    // pdf: Yup.string().when([], {
    //   is: () =>
    //     workPayrollDetails?.banK_CODE.trim() !== formik.values.BANK_CODE.trim() ||
    //   workPayrollTx?.fieldNames?.BANK_CODE.neW_KEY.trim() !== formik.values.BANK_CODE.trim()||

    //     workPayrollDetails?.banK_IFSC !== formik.values.BANK_IFSC ||
    //     workPayrollTx?.fieldNames?.BANK_IFSC.neW_KEY !== formik.values.BANK_IFSC.toString()||

    //     workPayrollDetails?.banK_ACCT !== formik.values.BANK_ACCT.toString()||
    //     workPayrollTx?.fieldNames?.BANK_ACCT.neW_KEY !== formik.values.BANK_ACCT.toString(),
    //   then: (schema) => schema.required("Proof attachment is required"),
    //   otherwise: (schema) => schema.notRequired(),
    // }),
  });

  const createTx = useMutation((payload: any) => insertTransaction(payload), {
    onSuccess: (response: any) => {
      const { responseStatus, message } = response;

      if (responseStatus === "E") {
        toast.error(
          <div>
            <h4 className="alert-heading">Error</h4>

            <p className="alert alert-danger" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      } else {
        queryClient.invalidateQueries([
          `${QUERIES.MASTER_DETAILS_LIST}-${query}`,
        ]);

        setItemIdForUpdate(undefined);
        handleClose();
        toast.success(
          <div>
            <h4 className="alert-heading">Success</h4>
            <p className="alert alert-success" role="alert">
              <h6>{Array.isArray(message) ? message.join("\n") : message}</h6>
            </p>
          </div>
        );
      }
    },
    onError: (response: any) => {
      toast.error(
        <div>
          <h4 className="alert-heading">Error</h4>

          <p className="alert alert-danger" role="alert">
            <h6>{response.message}</h6>
          </p>
        </div>
      );
    },
  });

  const formattedDate = (date: Date) => {
    return new Date(date).toLocaleString("en-GB", {
      day: "2-digit",
      month: "2-digit",
      year: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    });
  };

  const formik = useFormik({
    initialValues: {
      EMP_CODE: "",
      WORK_SITE: "",
      GRATUITY_DATE: "",
      NOTICE_PRD: "",
      BASIC: "",
      CURR_CTC: "",
      PAY_MODE: "",
      PAY_TYPE: "",
      BANK_CODE: "",
      BANK_IFSC: "",
      BANK_ACCT: "",
      DD_PAYABLE_BANK: "",
      pdf: "",
      CHG_REASON: "",
      paymenT_MODE: "",
      banK_NAME: "",
      sitE_NAME: "",
      paymenT_TYPE: "",
    },
    validationSchema: validationSchema,

    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);

      const payload: createTransaction = {
        tableName: "EMPLOYEE_PAYROLL",
        keyVar: "",
        wfLevel: 1,
        conf: "N",
        siteCode: "S0001",
        emp: currentUser?.id?.toString() || "",
        filePath: null,
        file: null,
        strTranId: itemIdForUpdate ? itemIdForUpdate : null,
        folderPath: null,
        tX_CHG_USER: currentUser?.id?.toString() || "",
        tX_CHG_TERM: privateIP,
        tX_CHG_DATE: new Date(),
        remarks: values.CHG_REASON || "NEW EMPLOYEE PAYROLL ADDED",

        fieldUpdates: [
          {
            SEQ_NO: "1",
            FIELD_NAME: "EMP_CODE",
            FIELD_DESCRIPTION: "EMPLOYEE CODE",
            OLD_KEY:
              workPayrollDetails?.emP_CODE ||
              workPayrollTx?.fieldNames?.EMP_CODE?.olD_KEY ||
              null,
            OLD_VALUE:
              workPayrollDetails?.emP_CODE ||
              workPayrollTx?.fieldNames?.EMP_CODE?.olD_VALUE ||
              null,
            NEW_KEY:
              workPayrollDetails?.emP_CODE ||
              workPayrollTx?.fieldNames?.EMP_CODE?.olD_VALUE ||
              null,
            NEW_VALUE:
              workPayrollDetails?.emP_CODE ||
              workPayrollTx?.fieldNames?.EMP_CODE?.olD_VALUE ||
              null,
            FILE_PATH: "",
          },
          ...((itemIdForUpdate !== undefined &&
            workPayrollTx?.fieldNames?.WORK_SITE?.olD_KEY ===
              values.WORK_SITE) ||
          workPayrollDetails?.worK_SITE !== values.WORK_SITE
            ? [
                {
                  SEQ_NO: "2",
                  FIELD_NAME: "WORK_SITE",
                  FIELD_DESCRIPTION: "WORK SITE",
                  OLD_KEY:
                    workPayrollDetails?.worK_SITE ||
                    workPayrollTx?.fieldNames?.WORK_SITE?.olD_KEY ||
                    "",
                  OLD_VALUE:
                    workPayrollDetails?.sitE_NAME ||
                    workPayrollTx?.fieldNames?.WORK_SITE?.olD_VALUE ||
                    "",
                  NEW_KEY: values.WORK_SITE,
                  NEW_VALUE: values.sitE_NAME,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            workPayrollTx?.fieldNames?.GRATUITY_DATE?.olD_KEY ===
              values.GRATUITY_DATE) ||
          workPayrollDetails?.gratuitY_DATE !== values.GRATUITY_DATE
            ? [
                {
                  SEQ_NO: "3",
                  FIELD_NAME: "GRATUITY_DATE",
                  FIELD_DESCRIPTION: "GRATUITY DATE",
                  OLD_KEY:
                    workPayrollDetails?.gratuitY_DATE ||
                    workPayrollTx?.fieldNames?.GRATUITY_DATE?.olD_KEY ||
                    "",
                  OLD_VALUE:
                    (workPayrollDetails?.gratuitY_DATE &&
                      formattedDate(workPayrollDetails.gratuitY_DATE)) ||
                    (workPayrollTx?.fieldNames?.GRATUITY_DATE?.olD_VALUE &&
                      formattedDate(
                        workPayrollDetails?.gratuitY_DATE ||
                          workPayrollTx?.fieldNames?.GRATUITY_DATE?.olD_VALUE
                      )) ||
                    "",
                  NEW_KEY: values.GRATUITY_DATE,
                  NEW_VALUE: formattedDate(values.GRATUITY_DATE),
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            workPayrollTx?.fieldNames?.NOTICE_PRD?.olD_KEY.toString() ===
              values.NOTICE_PRD.toString()) ||
          workPayrollDetails?.noticE_PRD.toString() !==
            values.NOTICE_PRD.toString()
            ? [
                {
                  SEQ_NO: "4",
                  FIELD_NAME: "NOTICE_PRD",
                  FIELD_DESCRIPTION: "NOTICE PERIOD",
                  OLD_KEY:
                    workPayrollDetails?.noticE_PRD.toString() ||
                    workPayrollTx?.fieldNames?.NOTICE_PRD?.olD_KEY ||
                    "",
                  OLD_VALUE:
                    workPayrollDetails?.noticE_PRD.toString() ||
                    workPayrollTx?.fieldNames?.NOTICE_PRD?.olD_VALUE ||
                    "",
                  NEW_KEY: values.NOTICE_PRD.toString(),
                  NEW_VALUE: values.NOTICE_PRD.toString(),
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            workPayrollTx?.fieldNames?.BASIC?.olD_KEY.toString() ===
              values.BASIC.toString()) ||
          workPayrollDetails?.basic.toString() !== values.BASIC.toString()
            ? [
                {
                  SEQ_NO: "5",
                  FIELD_NAME: "BASIC",
                  FIELD_DESCRIPTION: "BASIC PER MONTH",
                  OLD_KEY:
                    workPayrollDetails?.basic.toString() ||
                    workPayrollTx?.fieldNames?.BASIC?.olD_KEY ||
                    "",
                  OLD_VALUE:
                    workPayrollDetails?.basic.toString() ||
                    workPayrollTx?.fieldNames?.BASIC?.olD_VALUE ||
                    "",
                  NEW_KEY: values.BASIC.toString(),
                  NEW_VALUE: values.BASIC.toString(),
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            workPayrollTx?.fieldNames?.CURR_CTC?.olD_KEY.toString() ===
              values.CURR_CTC.toString()) ||
          workPayrollDetails?.curR_CTC.toString() !== values.CURR_CTC.toString()
            ? [
                {
                  SEQ_NO: "6",
                  FIELD_NAME: "CURR_CTC",
                  FIELD_DESCRIPTION: "CTC PER MONTH",
                  OLD_KEY:
                    workPayrollDetails?.curR_CTC.toString() ||
                    workPayrollTx?.fieldNames?.CURR_CTC?.olD_KEY ||
                    "",
                  OLD_VALUE:
                    workPayrollDetails?.curR_CTC.toString() ||
                    workPayrollTx?.fieldNames?.CURR_CTC?.olD_VALUE ||
                    "",
                  NEW_KEY: values.CURR_CTC.toString(),
                  NEW_VALUE: values.CURR_CTC.toString(),
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            workPayrollTx?.fieldNames?.PAY_MODE?.olD_KEY === values.PAY_MODE) ||
          workPayrollDetails?.paY_MODE !== values.PAY_MODE
            ? [
                {
                  SEQ_NO: "7",
                  FIELD_NAME: "PAY_MODE",
                  FIELD_DESCRIPTION: "MODE OF PAYMENT",
                  OLD_KEY:
                    workPayrollDetails?.paY_MODE ||
                    workPayrollTx?.fieldNames?.PAY_MODE?.olD_KEY ||
                    "",
                  OLD_VALUE:
                    workPayrollDetails?.paymenT_MODE ||
                    workPayrollTx?.fieldNames?.PAY_MODE?.olD_VALUE ||
                    "",
                  NEW_KEY: values.PAY_MODE,
                  NEW_VALUE: values.paymenT_MODE,
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            workPayrollTx?.fieldNames?.PAY_TYPE?.olD_KEY === values.PAY_TYPE) ||
          workPayrollDetails?.paY_TYPE !== values.PAY_TYPE
            ? [
                {
                  SEQ_NO: "8",
                  FIELD_NAME: "PAY_TYPE",
                  FIELD_DESCRIPTION: "TYPE OF PAYMENT",
                  OLD_KEY:
                    workPayrollDetails?.paY_TYPE ||
                    workPayrollTx?.fieldNames?.PAY_TYPE?.olD_KEY ||
                    "",
                  OLD_VALUE:
                    workPayrollDetails?.paY_TYPE === "S"
                      ? "SALARY"
                      : "WAGES" ||
                        workPayrollTx?.fieldNames?.PAY_TYPE?.olD_VALUE ||
                        "",
                  NEW_KEY: values.PAY_TYPE,
                  NEW_VALUE: values.PAY_TYPE === "S" ? "SALARY" : "WAGES",
                  FILE_PATH: "",
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            workPayrollTx?.fieldNames?.BANK_CODE?.olD_KEY ===
              values.BANK_CODE) ||
          workPayrollDetails?.banK_CODE.trim() !== values.BANK_CODE
            ? [
                {
                  SEQ_NO: "9",
                  FIELD_NAME: "BANK_CODE",
                  FIELD_DESCRIPTION: "BANK NAME",
                  OLD_KEY:
                    workPayrollDetails?.banK_CODE.trim() ||
                    workPayrollTx?.fieldNames?.BANK_CODE?.olD_KEY ||
                    "",
                  OLD_VALUE:
                    workPayrollDetails?.banK_NAME ||
                    workPayrollTx?.fieldNames?.BANK_CODE?.olD_VALUE ||
                    "",
                  NEW_KEY: values.BANK_CODE,
                  NEW_VALUE: formik.values.banK_NAME,
                  FILE_PATH: values.pdf,
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            workPayrollTx?.fieldNames?.BANK_IFSC?.olD_KEY ===
              values.BANK_IFSC) ||
          workPayrollDetails?.banK_IFSC !== values.BANK_IFSC
            ? [
                {
                  SEQ_NO: "10",
                  FIELD_NAME: "BANK_IFSC",
                  FIELD_DESCRIPTION: "IFSC CODE",
                  OLD_KEY:
                    workPayrollDetails?.banK_IFSC ||
                    workPayrollTx?.fieldNames?.BANK_IFSC?.olD_KEY ||
                    "",
                  OLD_VALUE:
                    workPayrollDetails?.banK_IFSC ||
                    workPayrollTx?.fieldNames?.BANK_IFSC?.olD_VALUE ||
                    "",
                  NEW_KEY: values.BANK_IFSC,
                  NEW_VALUE: values.BANK_IFSC,
                  FILE_PATH: values.pdf,
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            workPayrollTx?.fieldNames?.BANK_ACCT?.olD_KEY ===
              values.BANK_ACCT.toString()) ||
          workPayrollDetails?.banK_ACCT !== values.BANK_ACCT.toString()
            ? [
                {
                  SEQ_NO: "11",
                  FIELD_NAME: "BANK_ACCT",
                  FIELD_DESCRIPTION: "ACCOUNT NUMBER",
                  OLD_KEY:
                    workPayrollDetails?.banK_ACCT.toString() ||
                    workPayrollTx?.fieldNames?.BANK_ACCT?.olD_KEY ||
                    "",
                  OLD_VALUE:
                    workPayrollDetails?.banK_ACCT.toString() ||
                    workPayrollTx?.fieldNames?.BANK_ACCT?.olD_VALUE ||
                    "",
                  NEW_KEY: values.BANK_ACCT.toString(),
                  NEW_VALUE: values.BANK_ACCT.toString(),
                  FILE_PATH: formik.values.pdf,
                },
              ]
            : []),
          ...((itemIdForUpdate !== undefined &&
            workPayrollTx?.fieldNames?.DD_PAYABLE_BANK?.olD_KEY ===
              values.DD_PAYABLE_BANK) ||
          workPayrollDetails?.dD_PAYABLE_BANK !== values.DD_PAYABLE_BANK
            ? [
                {
                  SEQ_NO: "12",
                  FIELD_NAME: "DD_PAYABLE_BANK",
                  FIELD_DESCRIPTION: "DD PAYABLE BANK",
                  OLD_KEY:
                    workPayrollDetails?.dD_PAYABLE_BANK ||
                    workPayrollTx?.fieldNames?.DD_PAYABLE_BANK?.olD_KEY ||
                    "",
                  OLD_VALUE:
                    workPayrollDetails?.dD_PAYABLE_BANK ||
                    workPayrollTx?.fieldNames?.DD_PAYABLE_BANK?.olD_VALUE ||
                    "",
                  NEW_KEY: values.DD_PAYABLE_BANK,
                  NEW_VALUE: values.DD_PAYABLE_BANK,
                  FILE_PATH: "",
                },
              ]
            : []),
          {
            SEQ_NO: "13",
            FIELD_NAME: "CHG_REASON",
            FIELD_DESCRIPTION: "CHANGE REASON",
            OLD_KEY:
              workPayrollDetails?.CHG_REASON ||
              workPayrollTx?.fieldNames?.CHG_REASON?.olD_KEY ||
              "",
            OLD_VALUE:
              workPayrollDetails?.CHG_REASON ||
              workPayrollTx?.fieldNames?.CHG_REASON?.olD_KEY ||
              "",
            NEW_KEY: values.CHG_REASON || "NEW EMPLOYEE PAYROLL ADDED",
            NEW_VALUE: values.CHG_REASON || "NEW EMPLOYEE PAYROLL ADDED",
            FILE_PATH: "",
          },
        ],
      };
      createTx.mutate(payload);

      setSubmitting(false);
    },
  });

  const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (file) {
      const reader = new FileReader();

      reader.onloadend = () => {
        const base64String = reader.result;
        console.log(base64String);
        formik.setFieldValue("pdf", base64String);
      };

      reader.readAsDataURL(file);
    }
  };

  // const handlePdfChange = (e: React.ChangeEvent<HTMLInputElement>) => {
  //   const file = e.target.files?.[0];
  //   if (file) {
  //     formik.setFieldValue("pdf", file);
  //     formData.append("pdf", file);
  //   }
  // };

  const handleClose = () => {
    itemIdForUpdate ? setItemIdForUpdate(undefined) : navigate(-1);
  };

  return (
    <div className="card mb-1 mb-xl-2">
      <div
        className="m-1 rounded bg-light-primary card-header border-0 d-flex justify-content-between align-items-center cursor-pointer"
        role="button"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Edit Work & Payroll Details</h3>
        </div>

        <div>
          {/* <button
            type="button"
            className="btn btn-light-danger me-10"
            onClick={handleOpenModal}
          >
            Change Bank Details
          </button>
          {isModalOpen && <BankDetailsEditModal onClose={handleCloseModal} />} */}

          <button
            type="button"
            className="btn-close"
            aria-label="Close"
            onClick={handleClose}
            style={{ marginRight: "10px", cursor: "pointer" }}
          ></button>
        </div>
      </div>

      <div id="kt_account_profile_details" className="collapse show">
        <form onSubmit={formik.handleSubmit} className="form">
          <div className="card-body border-top p-9">
            {/* Begin: final outer Edit Row */}
            <div className="row mb-1">
              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="WORK_SITE"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    WORK SITE:
                  </label>
                  <select
                    name="WORK_SITE"
                    // className="form-select form-select-lg form-select-solid"
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          workPayrollTx?.fieldNames?.WORK_SITE?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.WORK_SITE && formik.errors.WORK_SITE,
                      },
                      {
                        "is-valid":
                          formik.touched.WORK_SITE && !formik.errors.WORK_SITE,
                      }
                    )}
                    {...(workPayrollTx?.fieldNames?.WORK_SITE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        workPayrollTx?.fieldNames?.WORK_SITE?.olD_VALUE === null
                          ? "-"
                          : workPayrollTx?.fieldNames?.WORK_SITE?.olD_VALUE
                      }`,
                    })}
                    value={formik.values?.WORK_SITE}
                    // {...formik.getFieldProps("WORK_SITE")}
                    // onChange={formik.handleChange}
                    onChange={(e) => {
                      const selectedValue = e.target.value;
                      const selectedOption = drpdwnEmpSites.find(
                        (EmpSite) => EmpSite.siteCode === selectedValue
                      );

                      formik.setFieldValue("WORK_SITE", selectedValue);
                      formik.setFieldValue(
                        "sitE_NAME",
                        selectedOption?.description || ""
                      );
                    }}
                    aria-placeholder="Enter Work Site"
                  >
                    <option value=""></option>
                    {drpdwnEmpSites.map((EmpSite) => (
                      <option key={EmpSite.siteCode} value={EmpSite.siteCode}>
                        {EmpSite.description}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.WORK_SITE && formik.errors.WORK_SITE && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.WORK_SITE}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="GRATUITY_DATE"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    GRATUITY DATE:
                  </label>
                  <input
                    type="date"
                    id="GRATUITY_DATE"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          workPayrollTx?.fieldNames?.GRATUITY_DATE?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.GRATUITY_DATE &&
                          formik.errors.GRATUITY_DATE,
                      },
                      {
                        "is-valid":
                          formik.touched.GRATUITY_DATE &&
                          !formik.errors.GRATUITY_DATE,
                      }
                    )}
                    {...(workPayrollTx?.fieldNames?.GRATUITY_DATE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        workPayrollTx?.fieldNames?.GRATUITY_DATE?.olD_VALUE ===
                        null
                          ? "-"
                          : workPayrollTx?.fieldNames?.GRATUITY_DATE?.olD_VALUE
                      }`,
                    })}
                    name="GRATUITY_DATE"
                    placeholder="Enter Gratuity Date"
                    // value={formik.values.GRATUITY_DATE?.toUpperCase()}
                    value={
                      formik.values?.GRATUITY_DATE
                        ? new Date(formik.values?.GRATUITY_DATE)
                            .toISOString()
                            .split("T")[0]
                        : ""
                    }
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.GRATUITY_DATE &&
                  formik.errors.GRATUITY_DATE && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.GRATUITY_DATE}</span>
                    </div>
                  )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="BASIC"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    BASIC PER MONTH:
                  </label>

                  <div className="input-group input-group-solid">
                    <span className="input-group-text">INR</span>
                    <input
                      type="number"
                      id="BASIC"
                      name="BASIC"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            workPayrollTx?.fieldNames?.BASIC?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.BASIC && formik.errors.BASIC,
                        },
                        {
                          "is-valid":
                            formik.touched.BASIC && !formik.errors.BASIC,
                        }
                      )}
                      {...(workPayrollTx?.fieldNames?.BASIC && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          workPayrollTx?.fieldNames?.BASIC?.olD_VALUE === null
                            ? "-"
                            : workPayrollTx?.fieldNames?.BASIC?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter BASIC Salary"
                      value={formik.values.BASIC}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                    {/* <span className="input-group-text">.00</span> */}
                  </div>
                </div>
                {formik.touched.BASIC && formik.errors.BASIC && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.BASIC}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="CURR_CTC"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CTC PER MONTH:
                  </label>
                  <div className="input-group input-group-solid">
                    <span className="input-group-text">INR</span>
                    <input
                      type="number"
                      id="CURR_CTC"
                      name="CURR_CTC"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            workPayrollTx?.fieldNames?.CURR_CTC?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.CURR_CTC && formik.errors.CURR_CTC,
                        },
                        {
                          "is-valid":
                            formik.touched.CURR_CTC && !formik.errors.CURR_CTC,
                        }
                      )}
                      {...(workPayrollTx?.fieldNames?.CURR_CTC && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          workPayrollTx?.fieldNames?.CURR_CTC?.olD_VALUE ===
                          null
                            ? "-"
                            : workPayrollTx?.fieldNames?.CURR_CTC?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter CTC "
                      value={formik.values.CURR_CTC}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                    {/* <span className="input-group-text">.00</span> */}
                  </div>
                </div>
                {formik.touched.CURR_CTC && formik.errors.CURR_CTC && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.CURR_CTC}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="NOTICE_PRD"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    NOTICE PERIOD:
                  </label>
                  <div className="input-group input-group-solid">
                    <input
                      type="number"
                      id="NOTICE_PRD"
                      name="NOTICE_PRD"
                      // className="form-control"
                      className={clsx(
                        "form-control",
                        {
                          "bg-light-warning":
                            workPayrollTx?.fieldNames?.NOTICE_PRD?.neW_VALUE,
                        },
                        {
                          "is-invalid":
                            formik.touched.NOTICE_PRD &&
                            formik.errors.NOTICE_PRD,
                        },
                        {
                          "is-valid":
                            formik.touched.NOTICE_PRD &&
                            !formik.errors.NOTICE_PRD,
                        }
                      )}
                      {...(workPayrollTx?.fieldNames?.NOTICE_PRD && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          workPayrollTx?.fieldNames?.NOTICE_PRD?.olD_VALUE ===
                          null
                            ? "-"
                            : workPayrollTx?.fieldNames?.NOTICE_PRD?.olD_VALUE
                        }`,
                      })}
                      placeholder="Enter Notice Period"
                      value={formik.values.NOTICE_PRD}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                    <span className="input-group-text">Days</span>
                  </div>
                </div>
                {formik.touched.NOTICE_PRD && formik.errors.NOTICE_PRD && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.NOTICE_PRD}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="DD_PAYABLE_BANK"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    DD PAY BANK:
                  </label>
                  <input
                    type="text"
                    id="DD_PAYABLE_BANK"
                    // className="form-control form-control-lg form-control-solid"

                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          workPayrollTx?.fieldNames?.DD_PAYABLE_BANK?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.DD_PAYABLE_BANK &&
                          formik.errors.DD_PAYABLE_BANK,
                      },
                      {
                        "is-valid":
                          formik.touched.DD_PAYABLE_BANK &&
                          !formik.errors.DD_PAYABLE_BANK,
                      }
                    )}
                    {...(workPayrollTx?.fieldNames?.DD_PAYABLE_BANK && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        workPayrollTx?.fieldNames?.DD_PAYABLE_BANK
                          ?.olD_VALUE === null
                          ? "-"
                          : workPayrollTx?.fieldNames?.DD_PAYABLE_BANK
                              ?.olD_VALUE
                      }`,
                    })}
                    name="DD_PAYABLE_BANK"
                    placeholder="Enter DD Payable Bank"
                    value={formik.values.DD_PAYABLE_BANK?.toUpperCase()}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.DD_PAYABLE_BANK &&
                  formik.errors.DD_PAYABLE_BANK && (
                    <div className="fv-plugins-message-container text-danger">
                      <span role="alert">{formik.errors.DD_PAYABLE_BANK}</span>
                    </div>
                  )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="PAY_MODE"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    MODE OF PAYMENT:
                  </label>
                  <select
                    id="PAY_MODE"
                    // className="form-select form-select-lg form-select-solid "
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          workPayrollTx?.fieldNames?.PAY_MODE?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.PAY_MODE && formik.errors.PAY_MODE,
                      },
                      {
                        "is-valid":
                          formik.touched.PAY_MODE && !formik.errors.PAY_MODE,
                      }
                    )}
                    {...(workPayrollTx?.fieldNames?.PAY_MODE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        workPayrollTx?.fieldNames?.PAY_MODE?.olD_VALUE === null
                          ? "-"
                          : workPayrollTx?.fieldNames?.PAY_MODE?.olD_VALUE
                      }`,
                    })}
                    name="PAY_MODE"
                    aria-placeholder="Enter Mode of Payment"
                    value={formik.values.PAY_MODE}
                    onChange={(e) => {
                      const selectedValue = e.target.value;
                      const selectedOption = payModes.find(
                        (mode) => mode.code === selectedValue
                      );

                      formik.setFieldValue("PAY_MODE", selectedValue);
                      formik.setFieldValue(
                        "paymenT_MODE",
                        selectedOption?.descr || ""
                      );
                    }}
                  >
                    <option value=""></option>
                    {payModes.map((mode) => (
                      <option key={mode.code} value={mode.code}>
                        {mode.descr}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.PAY_MODE && formik.errors.PAY_MODE && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.PAY_MODE}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="PAY_TYPE"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    TYPE OF PAYMENT:
                  </label>

                  <select
                    id="PAY_TYPE"
                    // className="form-select form-select-lg form-select-solid"
                    className={clsx(
                      "form-select form-select-lg form-select-solid",
                      {
                        "bg-light-warning":
                          workPayrollTx?.fieldNames?.PAY_TYPE?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.PAY_TYPE && formik.errors.PAY_TYPE,
                      },
                      {
                        "is-valid":
                          formik.touched.PAY_TYPE && !formik.errors.PAY_TYPE,
                      }
                    )}
                    {...(workPayrollTx?.fieldNames?.PAY_TYPE && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        workPayrollTx?.fieldNames?.PAY_TYPE?.olD_VALUE === null
                          ? "-"
                          : workPayrollTx?.fieldNames?.PAY_TYPE?.olD_VALUE
                      }`,
                    })}
                    name="PAY_TYPE"
                    aria-placeholder="Enter Payment Type"
                    value={formik.values.PAY_TYPE}
                    // onChange={formik.handleChange}
                    onChange={(e) => {
                      const selectedValue = e.target.value;
                      const selectedOption = payTypes.find(
                        (mode) => mode.code === selectedValue
                      );

                      formik.setFieldValue("PAY_TYPE", selectedValue);
                      formik.setFieldValue(
                        "paymenT_TYPE",
                        selectedOption?.descr || ""
                      );
                    }}
                  >
                    <option value=""></option>
                    {payTypes.map((mode) => (
                      <option key={mode.code} value={mode.code}>
                        {mode.descr}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.PAY_TYPE && formik.errors.PAY_TYPE && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.PAY_TYPE}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="BANK_CODE"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    BANK NAME:
                  </label>
                  {/* {workPayrollTx?.fieldNames?.BANK_CODE && (<OverlayTrigger
                    placement="top"
                    delay={{ show: 250, hide: 400 }}
                    overlay={(props) => (
                      <Tooltip id="button-tooltip" {...props}>
                       OLD VALUE : {workPayrollTx?.fieldNames?.BANK_CODE?.olD_VALUE}
                      </Tooltip>
                    )}
                  > */}
                    <select
                      id="BANK_CODE"
                      // className="form-select form-select-lg form-select-solid "
                      className={clsx(
                        "form-select form-select-lg form-select-solid",
                        {
                          "bg-light-warning":
                            workPayrollTx?.fieldNames?.BANK_CODE?.neW_KEY,
                        },
                        {
                          "is-invalid":
                            formik.touched.BANK_CODE && formik.errors.BANK_CODE,
                        },
                        {
                          "is-valid":
                            formik.touched.BANK_CODE &&
                            !formik.errors.BANK_CODE,
                        }
                      )}
                      {...(workPayrollTx?.fieldNames?.BANK_CODE && {
                        "data-bs-toggle": "tooltip",
                        "data-bs-placement": "top",
                        title: `OLD VALUE : ${
                          workPayrollTx?.fieldNames?.BANK_CODE?.olD_VALUE === null
                            ? "-"
                            : workPayrollTx?.fieldNames?.BANK_CODE?.olD_VALUE
                        }`,
                      })}
                      name="BANK_CODE"
                      aria-placeholder="Enter Bank Name"
                      value={formik.values.BANK_CODE}
                      // onChange={formik.handleChange}
                      onChange={(e) => {
                        const selectedValue = e.target.value;
                        const selectedOption = bankNameDropdown.find(
                          (bankName) =>
                            bankName.banK_CODE.trim() === selectedValue
                        );

                        formik.setFieldValue("BANK_CODE", selectedValue.trim());
                        formik.setFieldValue(
                          "banK_NAME",
                          selectedOption?.banK_NAME.toUpperCase() || ""
                        );
                      }}
                    >
                      <option value=""></option>
                      {bankNameDropdown.map((bankName) => (
                        <option
                          key={bankName.banK_CODE}
                          value={bankName.banK_CODE.trim()}
                        >
                          {bankName.banK_NAME.toUpperCase()}
                        </option>
                      ))}
                    </select>
                  {/* </OverlayTrigger>)} */}
                </div>
                {formik.touched.BANK_CODE && formik.errors.BANK_CODE && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.BANK_CODE}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="BANK_ACCT"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    ACCOUNT NO.:
                  </label>
                  <input
                    type="number"
                    id="BANK_ACCT"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          workPayrollTx?.fieldNames?.BANK_ACCT?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.BANK_ACCT && formik.errors.BANK_ACCT,
                      },
                      {
                        "is-valid":
                          formik.touched.BANK_ACCT && !formik.errors.BANK_ACCT,
                      }
                    )}
                    {...(workPayrollTx?.fieldNames?.BANK_ACCT && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        workPayrollTx?.fieldNames?.BANK_ACCT?.olD_VALUE === null
                          ? "-"
                          : workPayrollTx?.fieldNames?.BANK_ACCT?.olD_VALUE
                      }`,
                    })}
                    // style={{ cursor: "not-allowed" }}
                    name="BANK_ACCT"
                    placeholder="Enter Bank A/C Number"
                    value={formik.values?.BANK_ACCT}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.BANK_ACCT && formik.errors.BANK_ACCT && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.BANK_ACCT}</span>
                  </div>
                )}
              </div>
              {/* end: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="BANK_IFSC"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    IFSC CODE:
                  </label>
                  <input
                    type="text"
                    id="BANK_IFSC"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "bg-light-warning":
                          workPayrollTx?.fieldNames?.BANK_IFSC?.neW_VALUE,
                      },
                      {
                        "is-invalid":
                          formik.touched.BANK_IFSC && formik.errors.BANK_IFSC,
                      },
                      {
                        "is-valid":
                          formik.touched.BANK_IFSC && !formik.errors.BANK_IFSC,
                      }
                    )}
                    {...(workPayrollTx?.fieldNames?.BANK_IFSC && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        workPayrollTx?.fieldNames?.BANK_IFSC?.olD_VALUE === null
                          ? "-"
                          : workPayrollTx?.fieldNames?.BANK_IFSC?.olD_VALUE
                      }`,
                    })}
                    // style={{ cursor: "not-allowed" }}
                    name="BANK_IFSC"
                    placeholder="Enter Bank IFSC Code"
                    value={formik.values?.BANK_IFSC}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.BANK_IFSC && formik.errors.BANK_IFSC && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.BANK_IFSC}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}

              {/* Begin: Edit Row */}
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="pdf"
                    className="col-form-label required fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    ATTACH FILE:
                  </label>
                  <input
                    name="pdf"
                    // className="form-control form-control-lg flex-grow-1"
                    className={clsx(
                      "form-control form-control-solid flex-grow-1",
                      {
                        "bg-light-warning":
                          workPayrollTx?.fieldNames?.pdf?.neW_VALUE,
                      },
                      {
                        "is-invalid": formik.touched.pdf && formik.errors.pdf,
                      },
                      {
                        "is-valid": formik.touched.pdf && !formik.errors.pdf,
                      }
                    )}
                    {...(workPayrollTx?.fieldNames?.pdf && {
                      "data-bs-toggle": "tooltip",
                      "data-bs-placement": "top",
                      title: `OLD VALUE : ${
                        workPayrollTx?.fieldNames?.pdf?.olD_VALUE === null
                          ? "-"
                          : workPayrollTx?.fieldNames?.pdf?.olD_VALUE
                      }`,
                    })}
                    type="file"
                    id="pdf"
                    placeholder="Attach proof document"
                    onChange={handlePdfChange}
                    accept="application/pdf"
                    // required
                  />
                </div>
                {formik.touched.pdf && formik.errors.pdf && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.pdf}</span>
                  </div>
                )}
              </div>
              <div className="col-lg-6">
                <div className="d-flex align-items-center mb-1">
                  <label
                    htmlFor="CHG_REASON"
                    className="col-form-label  fw-bold fs-6 me-2"
                    style={{ minWidth: "150px" }}
                  >
                    CHANGE REASON:
                  </label>
                  <input
                    type="text"
                    id="CHG_REASON"
                    // className="form-control form-control-lg form-control-solid"
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "is-invalid":
                          formik.touched.CHG_REASON && formik.errors.CHG_REASON,
                      },
                      {
                        "is-valid":
                          formik.touched.CHG_REASON &&
                          !formik.errors.CHG_REASON,
                      }
                    )}
                    name="CHG_REASON"
                    placeholder="Enter Change Reason"
                    value={formik.values.CHG_REASON?.toUpperCase()}
                    //
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
                {formik.touched.CHG_REASON && formik.errors.CHG_REASON && (
                  <div className="fv-plugins-message-container text-danger">
                    <span role="alert">{formik.errors.CHG_REASON}</span>
                  </div>
                )}
              </div>
              {/* End: Edit Row */}
            </div>
            {/* end: final outer Edit Row */}

            {/* Logic for Submit is HERE : start */}
            <div className="card-footer d-flex justify-content-center py-6 px-9">
              <button
                type="button"
                className="btn btn-light me-2"
                onClick={handleClose}
              >
                <span className="indicator-label">Discard</span>
              </button>

              <button
                type="submit"
                className="btn btn-primary"
                disabled={formik.isSubmitting}
              >
                <span className="indicator-label">Submit</span>
              </button>
            </div>
            {/* Logic for Submit is HERE : end */}
          </div>
        </form>
      </div>
    </div>
  );
};

export default WorkPayrollDetailsEdit;
