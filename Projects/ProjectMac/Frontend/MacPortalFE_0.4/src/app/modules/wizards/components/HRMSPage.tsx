// import React, { useState } from 'react';
// import './HRMSPage.css'; // Import CSS file for additional styling

// interface Employee {
//   id: number;
//   name: string;
//   location: string;
//   department: string;
//   cadre: string;
// }

// const HRMSPage: React.FC = () => {
//   const [selectedLocation, setSelectedLocation] = useState<string | null>(null);
//   const [selectedDepartment, setSelectedDepartment] = useState<string | null>(null);
//   const [selectedCadre, setSelectedCadre] = useState<string | null>(null);
//   const [totalEmployees, setTotalEmployees] = useState<number>(0);
//   const [specificEmployee, setSpecificEmployee] = useState<string>('');
//   const [message, setMessage] = useState<string>('');

//   // Dummy data for demonstration
//   const employees: Employee[] = [
//     { id: 1, name: 'John Doe', location: 'New York', department: 'HR', cadre: 'Manager' },
//     { id: 2, name: 'Jane Smith', location: 'Los Angeles', department: 'Finance', cadre: 'Analyst' },
//     // Add more dummy data as needed
//   ];

//   const handleSendClick = () => {
//     // Implement sending functionality here
//     console.log('Sending message:', message);
//   };

//   return (
//     <div className="container mt-5">
//       <div className="card">
//         <div className="card-header">
//           <h2 className="text-center">SMS Broadcast</h2>
//         </div>
//         <div className="card-body">
//           <div className="row mb-3">
//             <div className="col">
//               <select className="form-select" value={selectedLocation || ''} onChange={(e) => setSelectedLocation(e.target.value)}>
//                 <option value="">Select Location</option>
//                 {/* Populate options dynamically based on available locations */}
//                 {/* You may want to extract location options from your employee data */}
//                 <option value="New York">New York</option>
//                 <option value="Los Angeles">Los Angeles</option>
//                 {/* Add more location options as needed */}
//               </select>
//             </div>
//             <div className="col">
//               <select className="form-select" value={selectedDepartment || ''} onChange={(e) => setSelectedDepartment(e.target.value)}>
//                 <option value="">Select Department</option>
//                 {/* Populate options dynamically based on available departments */}
//                 {/* You may want to extract department options from your employee data */}
//                 <option value="HR">HR</option>
//                 <option value="Finance">Finance</option>
//                 {/* Add more department options as needed */}
//               </select>
//             </div>
//             <div className="col">
//               <select className="form-select" value={selectedCadre || ''} onChange={(e) => setSelectedCadre(e.target.value)}>
//                 <option value="">Select Cadre</option>
//                 {/* Populate options dynamically based on available cadres */}
//                 {/* You may want to extract cadre options from your employee data */}
//                 <option value="Manager">Manager</option>
//                 <option value="Analyst">Analyst</option>
//                 {/* Add more cadre options as needed */}
//               </select>
//             </div>
//           </div>
//           <div className="row mb-3">
//             <div className="col">
//               <input className="form-control" type="number" value={totalEmployees} onChange={(e) => setTotalEmployees(parseInt(e.target.value))} placeholder="Total Employees" />
//             </div>
//             <div className="col">
//               <input className="form-control" type="text" value={specificEmployee} onChange={(e) => setSpecificEmployee(e.target.value)} placeholder="Specific Employee Name" />
//             </div>
//           </div>
//           <div className="row mb-3">
//             <div className="col">
//               <textarea className="form-control" value={message} onChange={(e) => setMessage(e.target.value)} rows={4} placeholder="Message"></textarea>
//             </div>
//           </div>
//           <div className="row">
//             <div className="col text-center">
//               <button className="btn btn-primary" onClick={handleSendClick}>Send</button>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default HRMSPage;

// import React, { useState } from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';

// const HRMSPage: React.FC = () => {
//   const [pageId, setPageId] = useState<string>('');

//   const handleClick = (id: string) => {
//     setPageId(id);
//     const element = document.getElementById(id);
//     if (element) {
//       window.scrollTo({
//         top: element.offsetTop,
//         behavior: 'smooth',
//       });
//     }
//   };

//   return (
//     <div className="App">
//       <nav className="navbar navbar-dark bg-dark navbar-fixed-top">
//         <div className="container-fluid">
//           <div className="navbar-header">
//             <a className="navbar-brand" href="#">
//               Company Name
//             </a>
//           </div>
//           <ul className="nav navbar-nav">
//             <li>
//               <a
//                 href="#"
//                 className="nav-link"
//                 onClick={() => handleClick('home')}
//               >
//                 Home
//               </a>
//             </li>
//             <li>
//               <a
//                 href="#"
//                 className="nav-link"
//                 onClick={() => handleClick('services')}
//               >
//                 Services
//               </a>
//             </li>
//             <li>
//               <a
//                 href="#"
//                 className="nav-link"
//                 onClick={() => handleClick('about')}
//               >
//                 About
//               </a>
//             </li>
//             <li>
//               <a
//                 href="#"
//                 className="nav-link"
//                 onClick={() => handleClick('contact')}
//               >
//                 Contact
//               </a>
//             </li>
//           </ul>
//         </div>
//       </nav>

//       <div className="container-fluid section" id="home">
//         <h1>Welcome to our website!</h1>
//         <p>This is the Home section.</p>
//       </div>
//       <div className="container-fluid section" id="services">
//         <h1>Our Services</h1>
//         <p>Explore our range of services.</p>
//       </div>
//       <div className="container-fluid section" id="about">
//         <h1>About Us</h1>
//         <p>Learn more about our company.</p>
//       </div>
//       <div className="container-fluid section" id="contact">
//         <h1>Contact Us</h1>
//         <p>Get in touch with us.</p>
//       </div>
//     </div>
//   );
// };

// export default HRMSPage;


// import React, { useState, useRef } from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';

// const HRMSPage: React.FC = () => {
//   const homeRef = useRef<HTMLDivElement>(null);
//   const servicesRef = useRef<HTMLDivElement>(null);
//   const aboutRef = useRef<HTMLDivElement>(null);
//   const contactRef = useRef<HTMLDivElement>(null);

//   const scrollToRef = (ref: React.RefObject<HTMLDivElement>) => {
//     if (ref.current) {
//       window.scrollTo({
//         top: ref.current.offsetTop,
//         behavior: 'smooth',
//       });
//     }
//   };

//   return (
//     <div className="App">
//       <nav className="navbar navbar-dark bg-dark navbar-fixed-top">
//         <div className="container-fluid">
//           <div className="navbar-header">
//             <a className="navbar-brand" href="#">
//               Company Name
//             </a>
//           </div>
//           <ul className="nav navbar-nav">
//             <li>
//               <a
//                 href="#"
//                 className="nav-link"
//                 onClick={() => scrollToRef(homeRef)}
//               >
//                 Home
//               </a>
//             </li>
//             <li>
//               <a
//                 href="#"
//                 className="nav-link"
//                 onClick={() => scrollToRef(servicesRef)}
//               >
//                 Services
//               </a>
//             </li>
//             <li>
//               <a
//                 href="#"
//                 className="nav-link"
//                 onClick={() => scrollToRef(aboutRef)}
//               >
//                 About
//               </a>
//             </li>
//             <li>
//               <a
//                 href="#"
//                 className="nav-link"
//                 onClick={() => scrollToRef(contactRef)}
//               >
//                 Contact
//               </a>
//             </li>
//           </ul>
//         </div>
//       </nav>

//       <div className="container-fluid section" ref={homeRef}>
//         <h1>Welcome to our website!</h1>
//         <p>This is the Home section.We hereby inform you with deep regret about demise of Mr Mubinuddin Shaikh (H002788) on 10th January, 2024 due to Cardiac Arrest and Mr Soumen Sharma (F029153) on 15th January, 2024 due to Cardiac Attack.
// Mr Mubinuddin Shaikh was associated with us as General Manager in Safety & Environment Control department at our Sarigam plant since 01stAugust, 2007 and Mr Soumen Sharma was associated with us as PSE in Procare AHT division at our Siliguri HQ since 28thAugust’2023, whose valuable contribution in Macleods will always be remembered.
// We pray to God for their family to give them strength to go through this difficult time. As per practice, we will be contributing towards the welfare of their family.
// The deduction towards this contribution will be made from employees’ salary in the month of March ’2024. The deduction will be as per the slab of Benevolent Contribution Scheme.
// </p>
// <p>This is the Home section.We hereby inform you with deep regret about demise of Mr Mubinuddin Shaikh (H002788) on 10th January, 2024 due to Cardiac Arrest and Mr Soumen Sharma (F029153) on 15th January, 2024 due to Cardiac Attack.
// Mr Mubinuddin Shaikh was associated with us as General Manager in Safety & Environment Control department at our Sarigam plant since 01stAugust, 2007 and Mr Soumen Sharma was associated with us as PSE in Procare AHT division at our Siliguri HQ since 28thAugust’2023, whose valuable contribution in Macleods will always be remembered.
// We pray to God for their family to give them strength to go through this difficult time. As per practice, we will be contributing towards the welfare of their family.
// The deduction towards this contribution will be made from employees’ salary in the month of March ’2024. The deduction will be as per the slab of Benevolent Contribution Scheme.
// </p>
// <p>This is the Home section.We hereby inform you with deep regret about demise of Mr Mubinuddin Shaikh (H002788) on 10th January, 2024 due to Cardiac Arrest and Mr Soumen Sharma (F029153) on 15th January, 2024 due to Cardiac Attack.
// Mr Mubinuddin Shaikh was associated with us as General Manager in Safety & Environment Control department at our Sarigam plant since 01stAugust, 2007 and Mr Soumen Sharma was associated with us as PSE in Procare AHT division at our Siliguri HQ since 28thAugust’2023, whose valuable contribution in Macleods will always be remembered.
// We pray to God for their family to give them strength to go through this difficult time. As per practice, we will be contributing towards the welfare of their family.
// The deduction towards this contribution will be made from employees’ salary in the month of March ’2024. The deduction will be as per the slab of Benevolent Contribution Scheme.
// </p>
//       </div>
//       <div className="container-fluid section" ref={servicesRef}>
//         <h1>Our Services</h1>
//         <p>Explore our range of servicesWe hereby inform you with deep regret about demise of Mr Mubinuddin Shaikh (H002788) on 10th January, 2024 due to Cardiac Arrest and Mr Soumen Sharma (F029153) on 15th January, 2024 due to Cardiac Attack.
// Mr Mubinuddin Shaikh was associated with us as General Manager in Safety & Environment Control department at our Sarigam plant since 01stAugust, 2007 and Mr Soumen Sharma was associated with us as PSE in Procare AHT division at our Siliguri HQ since 28thAugust’2023, whose valuable contribution in Macleods will always be remembered.
// We pray to God for their family to give them strength to go through this difficult time. As per practice, we will be contributing towards the welfare of their family.
// The deduction towards this contribution will be made from employees’ salary in the month of March ’2024. The deduction will be as per the slab of Benevolent Contribution Scheme.
// .</p>
// <p>This is the Home section.We hereby inform you with deep regret about demise of Mr Mubinuddin Shaikh (H002788) on 10th January, 2024 due to Cardiac Arrest and Mr Soumen Sharma (F029153) on 15th January, 2024 due to Cardiac Attack.
// Mr Mubinuddin Shaikh was associated with us as General Manager in Safety & Environment Control department at our Sarigam plant since 01stAugust, 2007 and Mr Soumen Sharma was associated with us as PSE in Procare AHT division at our Siliguri HQ since 28thAugust’2023, whose valuable contribution in Macleods will always be remembered.
// We pray to God for their family to give them strength to go through this difficult time. As per practice, we will be contributing towards the welfare of their family.
// The deduction towards this contribution will be made from employees’ salary in the month of March ’2024. The deduction will be as per the slab of Benevolent Contribution Scheme.
// </p>
// <p>This is the Home section.We hereby inform you with deep regret about demise of Mr Mubinuddin Shaikh (H002788) on 10th January, 2024 due to Cardiac Arrest and Mr Soumen Sharma (F029153) on 15th January, 2024 due to Cardiac Attack.
// Mr Mubinuddin Shaikh was associated with us as General Manager in Safety & Environment Control department at our Sarigam plant since 01stAugust, 2007 and Mr Soumen Sharma was associated with us as PSE in Procare AHT division at our Siliguri HQ since 28thAugust’2023, whose valuable contribution in Macleods will always be remembered.
// We pray to God for their family to give them strength to go through this difficult time. As per practice, we will be contributing towards the welfare of their family.
// The deduction towards this contribution will be made from employees’ salary in the month of March ’2024. The deduction will be as per the slab of Benevolent Contribution Scheme.
// </p>
//       </div>
//       <div className="container-fluid section" ref={aboutRef}>
//         <h1>About Us</h1>
//         <p>Learn more about our company.We hereby inform you with deep regret about demise of Mr Mubinuddin Shaikh (H002788) on 10th January, 2024 due to Cardiac Arrest and Mr Soumen Sharma (F029153) on 15th January, 2024 due to Cardiac Attack.
// Mr Mubinuddin Shaikh was associated with us as General Manager in Safety & Environment Control department at our Sarigam plant since 01stAugust, 2007 and Mr Soumen Sharma was associated with us as PSE in Procare AHT division at our Siliguri HQ since 28thAugust’2023, whose valuable contribution in Macleods will always be remembered.
// We pray to God for their family to give them strength to go through this difficult time. As per practice, we will be contributing towards the welfare of their family.
// The deduction towards this contribution will be made from employees’ salary in the month of March ’2024. The deduction will be as per the slab of Benevolent Contribution Scheme.
// </p>
// <p>This is the Home section.We hereby inform you with deep regret about demise of Mr Mubinuddin Shaikh (H002788) on 10th January, 2024 due to Cardiac Arrest and Mr Soumen Sharma (F029153) on 15th January, 2024 due to Cardiac Attack.
// Mr Mubinuddin Shaikh was associated with us as General Manager in Safety & Environment Control department at our Sarigam plant since 01stAugust, 2007 and Mr Soumen Sharma was associated with us as PSE in Procare AHT division at our Siliguri HQ since 28thAugust’2023, whose valuable contribution in Macleods will always be remembered.
// We pray to God for their family to give them strength to go through this difficult time. As per practice, we will be contributing towards the welfare of their family.
// The deduction towards this contribution will be made from employees’ salary in the month of March ’2024. The deduction will be as per the slab of Benevolent Contribution Scheme.
// </p>
// <p>This is the Home section.We hereby inform you with deep regret about demise of Mr Mubinuddin Shaikh (H002788) on 10th January, 2024 due to Cardiac Arrest and Mr Soumen Sharma (F029153) on 15th January, 2024 due to Cardiac Attack.
// Mr Mubinuddin Shaikh was associated with us as General Manager in Safety & Environment Control department at our Sarigam plant since 01stAugust, 2007 and Mr Soumen Sharma was associated with us as PSE in Procare AHT division at our Siliguri HQ since 28thAugust’2023, whose valuable contribution in Macleods will always be remembered.
// We pray to God for their family to give them strength to go through this difficult time. As per practice, we will be contributing towards the welfare of their family.
// The deduction towards this contribution will be made from employees’ salary in the month of March ’2024. The deduction will be as per the slab of Benevolent Contribution Scheme.
// </p>
//       </div>
//       <div className="container-fluid section" ref={contactRef}>
//         <h1>Contact Us</h1>
//         <p>Get in touch with us.We hereby inform you with deep regret about demise of Mr Mubinuddin Shaikh (H002788) on 10th January, 2024 due to Cardiac Arrest and Mr Soumen Sharma (F029153) on 15th January, 2024 due to Cardiac Attack.
// Mr Mubinuddin Shaikh was associated with us as General Manager in Safety & Environment Control department at our Sarigam plant since 01stAugust, 2007 and Mr Soumen Sharma was associated with us as PSE in Procare AHT division at our Siliguri HQ since 28thAugust’2023, whose valuable contribution in Macleods will always be remembered.
// We pray to God for their family to give them strength to go through this difficult time. As per practice, we will be contributing towards the welfare of their family.
// The deduction towards this contribution will be made from employees’ salary in the month of March ’2024. The deduction will be as per the slab of Benevolent Contribution Scheme.
// </p>
// <p>This is the Home section.We hereby inform you with deep regret about demise of Mr Mubinuddin Shaikh (H002788) on 10th January, 2024 due to Cardiac Arrest and Mr Soumen Sharma (F029153) on 15th January, 2024 due to Cardiac Attack.
// Mr Mubinuddin Shaikh was associated with us as General Manager in Safety & Environment Control department at our Sarigam plant since 01stAugust, 2007 and Mr Soumen Sharma was associated with us as PSE in Procare AHT division at our Siliguri HQ since 28thAugust’2023, whose valuable contribution in Macleods will always be remembered.
// We pray to God for their family to give them strength to go through this difficult time. As per practice, we will be contributing towards the welfare of their family.
// The deduction towards this contribution will be made from employees’ salary in the month of March ’2024. The deduction will be as per the slab of Benevolent Contribution Scheme.
// </p>
// <p>This is the Home section.We hereby inform you with deep regret about demise of Mr Mubinuddin Shaikh (H002788) on 10th January, 2024 due to Cardiac Arrest and Mr Soumen Sharma (F029153) on 15th January, 2024 due to Cardiac Attack.
// Mr Mubinuddin Shaikh was associated with us as General Manager in Safety & Environment Control department at our Sarigam plant since 01stAugust, 2007 and Mr Soumen Sharma was associated with us as PSE in Procare AHT division at our Siliguri HQ since 28thAugust’2023, whose valuable contribution in Macleods will always be remembered.
// We pray to God for their family to give them strength to go through this difficult time. As per practice, we will be contributing towards the welfare of their family.
// The deduction towards this contribution will be made from employees’ salary in the month of March ’2024. The deduction will be as per the slab of Benevolent Contribution Scheme.
// </p>
//       </div>
//     </div>
//   );
// };

// export default HRMSPage;


// import React from 'react';
// import Navbar from './Navbar';
// import Content from './Content';

// const HRMSPage: React.FC = () => {
//   const scrollToRef = (ref: React.RefObject<HTMLDivElement>) => {
//     if (ref.current) {
//       window.scrollTo({
//         top: ref.current.offsetTop,
//         behavior: 'smooth',
//       });
//     }
//   };

//   return (
//     <div>
//       <Navbar scrollToRef={scrollToRef} />
//       <Content scrollToRef={scrollToRef} />
//     </div>
//   );
// };

// export default HRMSPage;

import React, { useState } from 'react';
import Navbar from './Navbar';
import PageBody from './Content';

const HRMSPage: React.FC = () => {
  const sections = ['Home', 'About', 'Services', 'Contact']; 

  const handleSectionClick = (section: string) => {
    const element = document.getElementById(section);
    if (element) {
      element.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <div>
      <Navbar sections={sections} handleSectionClick={handleSectionClick} />
      <PageBody sections={sections} />
    </div>
  );
};

export default HRMSPage;