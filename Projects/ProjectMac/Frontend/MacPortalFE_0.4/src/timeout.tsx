// import { useEffect, useState } from "react";
// import { useAuth } from "./app/modules/auth";

// export const Timeout = () => {
//   const { logout } = useAuth();
//   const [timeLeft, setTimeLeft] = useState(0);
//   const [timeSpan] = useState(30);

//   const checkForInActivity = () => {
//     const expireTime = localStorage.getItem("expireTime");

//     if (expireTime && parseInt(expireTime, 10) < Date.now()) {
//       // console.log("Logging Out...");
//       logout();
//     }
//   };

//   const updateExpireTime = () => {
//     const expireTime = Date.now() + 1000 * timeSpan * 60;
//     console.log("updateExpireTime() called...");
//     localStorage.setItem("expireTime", expireTime.toString());
//     setTimeLeft(timeSpan * 60);
//   };

//   useEffect(() => {
//     const interval = setInterval(() => {
//       checkForInActivity();
//       // console.log("checkForInActivity() called...");
//       const expireTime = localStorage.getItem("expireTime");
//       if (expireTime) {
//         const timeLeft = Math.max(parseInt(expireTime, 10) - Date.now(), 0);
//         setTimeLeft(Math.floor(timeLeft / 1000));
//       }
//     }, 1000);
//     return () => clearInterval(interval);
//   }, []);

//   useEffect(() => {
//     updateExpireTime();

//     const updateExpireTimeListener = () => updateExpireTime();

//     const elements = window.document.querySelectorAll(
//       "button, input, a, select, option"
//       +
//       ".menu, .btn, .menu-item"
//     );

//     elements.forEach((element) => {
//       element.addEventListener("click", updateExpireTimeListener);
//     });

//     return () => {
//       elements.forEach((element) => {
//         element.removeEventListener("click", updateExpireTimeListener);
//       });
//     };
//   }, []);

//   const formatTime = (seconds: number) => {
//     const minutes = Math.floor(seconds / 60);
//     const remainingSeconds = seconds % 60;
//     return `${String(minutes).padStart(2, "0")}:${String(
//       remainingSeconds
//     ).padStart(2, "0")}`;
//   };

//   return (
//     <div className="track-activity">
//       <span className="fw-bold border border-gray-300 rounded fs-6 text-gray-600 p-2 px-4">
//         {formatTime(timeLeft)}
//       </span>
//     </div>
//   );
// };



// import { useEffect, useState } from "react";
// import { useAuth } from "./app/modules/auth";

// export const useTimeout = (timeSpan = 30) => {
//   const { logout } = useAuth();
//   const [timeLeft, setTimeLeft] = useState(timeSpan * 60);

//   const checkForInActivity = () => {
//     const expireTime = localStorage.getItem("expireTime");

//     if (expireTime && parseInt(expireTime, 10) < Date.now()) {
//       logout();
//     }
//   };

//   const updateExpireTime = () => {
//     const expireTime = Date.now() + 1000 * timeSpan * 60;
//     localStorage.setItem("expireTime", expireTime.toString());
//     setTimeLeft(timeSpan * 60);
//   };

//   useEffect(() => {
//     const interval = setInterval(() => {
//       checkForInActivity();
//       const expireTime = localStorage.getItem("expireTime");
//       if (expireTime) {
//         const timeLeft = Math.max(parseInt(expireTime, 10) - Date.now(), 0);
//         setTimeLeft(Math.floor(timeLeft / 1000));
//       }
//     }, 1000);

//     return () => clearInterval(interval);
//   }, []);

//   useEffect(() => {
//     updateExpireTime();

//     const updateExpireTimeListener = () => updateExpireTime();

//     const elements = window.document.querySelectorAll(
//       "button, input, a, select, option, Link"
//       +
//       ".menu, .btn, .menu-item, .Link"
//     );

//     elements.forEach((element) => {
//       element.addEventListener("click", updateExpireTimeListener);
//     });

//     return () => {
//       elements.forEach((element) => {
//         element.removeEventListener("click", updateExpireTimeListener);
//       });
//     };
//   }, []);

//   return timeLeft;
// };




import { useEffect, useState } from "react";
import { useAuth } from "./app/modules/auth";
 import * as authHelper from "./../src/app/modules/auth/core/AuthHelpers";
import axios from "axios";
export const useTimeout = () => {
  const [timeSpan] = useState(5);
  const { currentUser,auth,saveAuth,logout } = useAuth();
  const [timeLeft, setTimeLeft] = useState(timeSpan * 60);

  const checkForInActivity = () => {
    const expireTime = localStorage.getItem("expireTime");

    if (expireTime && parseInt(expireTime, 10) < Date.now()) {
      logout();
    }
  };

  const updateExpireTime = () => {
    const expireTime = Date.now() + 1000 * timeSpan * 60;
    localStorage.setItem("expireTime", expireTime.toString());
    setTimeLeft(timeSpan * 60);
  };

  const attachEventListeners = () => {
    const updateExpireTimeListener = () => updateExpireTime();

    const elements = window.document.querySelectorAll(
     "button, input, a, select, option"
      +
      ".menu, .btn, .menu-item"
    );

    elements.forEach((element) => {
      element.removeEventListener("click", updateExpireTimeListener);
      element.addEventListener("click", updateExpireTimeListener);
    });
  };

  useEffect(() => {
    updateExpireTime();

    attachEventListeners(); // Initial attach

    const observer = new MutationObserver(() => {
      attachEventListeners(); // Reattach listeners when DOM changes
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true,
    });

    const interval = setInterval(() => {
      checkForInActivity();
      const expireTime = localStorage.getItem("expireTime");
      if (expireTime) {
        const timeLeft = Math.max(parseInt(expireTime, 10) - Date.now(), 0);
        setTimeLeft(Math.floor(timeLeft / 1000));
      }
    }, 1000);

    return () => {
      clearInterval(interval);
      observer.disconnect(); 
    };
  }, []);
  
  const refreshAuthToken = () => {
    console.log("refreshAuthToken() called...");
    if (auth) {
      const payload = {
        api_token: authHelper.getAuth()?.api_token,
        refreshToken: authHelper.getAuth()?.refreshToken,
      };
      axios
        .post("https://localhost:44360/api/Auth/RefreshToken", payload)
        .then((response) => {
          const { token, refreshToken } = response.data;
          if (token && refreshToken) {
            const updatedAuth = { ...auth, api_token: token, refreshToken };
            saveAuth(updatedAuth);
          }
        })
        .catch((error) => {
          console.error("Error refreshing token:", error);
        });
    }
  };

  useEffect(() => {
    console.log(" calling refreshAuthToken()...");
    if (currentUser !== undefined) {
      const intervalId = setInterval(refreshAuthToken, 4.9 * 60 * 1000);
      return () => clearInterval(intervalId);
    }
  }, []);

  return timeLeft;
};
