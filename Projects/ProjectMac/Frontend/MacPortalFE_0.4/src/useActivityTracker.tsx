import { useEffect } from 'react';
import { useInactivity } from './InactivityContext'; // Adjust the import based on your project structure

const useActivityTracker = () => {
  const { updateExpireTime } = useInactivity();

  useEffect(() => {
    const updateExpireTimeListener = () => updateExpireTime();

    const elements = document.querySelectorAll(
      "button, input, a, select, option"
      +
      ".menu, .btn, .menu-item"
    );

    elements.forEach((element) => {
      element.addEventListener("click", updateExpireTimeListener);
    });

    return () => {
      elements.forEach((element) => {
        element.removeEventListener("click", updateExpireTimeListener);
      });
    };
  }, [updateExpireTime]);
};

export default useActivityTracker;



    
