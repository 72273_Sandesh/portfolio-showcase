# Portfolio-Showcase

This repository is a comprehensive showcase of my Projects and Notes which I have gathered after lot of hard work and study. It includes a React front-end application, an ASP.NET CORE 8 back-end service, and other personal projects. Each project is organized into its own directory with detailed documentation.

## Projects

### Frontend React Application
- **Name**: HRMS
- **Location**: `portfolio-showcase/Projects/ProjectMac/Frontend`
- **Description**: This project is a single-page application built using React. It demonstrates my skills in modern front-end development, including state management, routing, and API integration.
- **Technologies**: React, Redux, React Router, Axios.
- **Setup Instructions**: See `frontend-react/README.md`

### Backend ASP.NET Service
- **Name**: HRMS
- **Location**: `portfolio-showcase/Projects/ProjectMac/Backend`
- **Description**: This project is a RESTful API service built using ASP.NET Core 8. It showcases my abilities in back-end development, including database interactions, authentication, and API design.
- **Technologies**: ASP.NET Core, Entity Framework, SQL Server
- **Setup Instructions**: See `backend-aspnet/README.md`

### Other Personal Projects
- **Name**: Portfolio Web
- **Location**: `portfolio-showcase/Projects/PortfolioWeb`
- **Description**: This project is a single-page application built using React. It demonstrates my Skills, Educational Background, Projects etc. in modern front-end development, including state management, routing, and SMTP integration.
- **Technologies**: React, Redux, React Router. 
- **Setup Instructions**: Each project has its own `README.md` file with detailed setup instructions.

## Documentation
- **Name**: UpSkill
- **Location**: `portfolio-showcase/UpSkill`
- **Contents**:
  - `ASP.NET CORE 8`: Project Architecture, Design Pattern, Folder Structure, Notes.
  - `REACT`: Project Architecture, Design Pattern, Folder Structure, Notes.
  - `JAVASCRIPT`: Project Architecture, Design Pattern, Folder Structure, Notes.
  - `GIT`: Centralized Source Management, Notes.

## How to Use This Repository

1. **Cloning the Repository**:
   ```bash
   git clone https://gitlab.com/SandeshPatankar/portfolio-showcase.git
